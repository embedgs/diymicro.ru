import string

def histogram(s):
	d = dict()
	for c in s:
		temp = d.get(c,0)
		d[c]=temp+1
	return d		

def process_file(filename):
	hist = dict()
	fp = open(filename)
	for line in fp:
		process_line(line, hist)
	return hist
	
def process_line(line, hist):
	for c in string.punctuation:
		line = line.replace(c, ' ')

	for word in line.split():
		word = word.strip(string.punctuation+string.whitespace+string.digits)
		word = word.lower()
		if (word not in string.whitespace):
			hist[word] = hist.get(word,0)+1		

def process_punctuation_file(filename):
	hist = dict()
	fp = open(filename)
	for line in fp:
		process_line_punctuation(line, hist)
	return hist

def process_line_punctuation(line, hist):
	for symbol in line:
		if (symbol in string.punctuation):
				hist[symbol]=hist.get(symbol,0)+1

def most_common(hist):
	t = []
	for key, value in hist.items():
		t.append((value, key))

	t.sort(reverse=True)
	return t	

def print_words_info(filename):
	mydict = process_file(filename)
	#for key, num in mydict.items():
	#		print(key, num, sep='\t')
	t = most_common(mydict)
	print('\r\n The file contains ', sum(mydict.values()), ' words')
	print('\r\n The file contains ', len(mydict), ' unique words')	
	print('The most common words are:')
	for freq, word in t[:10]:
		print(word, freq, sep='\t')

def print_punctuation_info(filename):
	my_punct_dict = process_punctuation_file(filename)
	for key, num in my_punct_dict.items():
		print(key, num, sep='\t')

	print('\r\n The file contains ', sum(my_punct_dict.values()), ' signs')
	print('\r\n The file contains ', len(my_punct_dict), ' unique signs')		

def most_common(hist):
	t = []
	for key, value in hist.items():
		t.append((value, key))

	t.sort(reverse=True)
	return t	



#print_punctuation_info()

print_words_info('hung_games.txt')

