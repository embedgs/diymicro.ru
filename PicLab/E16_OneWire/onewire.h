#ifndef _ONEWIRE_H
#define _ONEWIRE_H_

#define STATE TRISA2
#define PIN RA2


void TX(unsigned char cmd);
unsigned char RX();
unsigned int get_temp();


#endif