opt subtitle "HI-TECH Software Omniscient Code Generator (PRO mode) build 6446"

opt pagewidth 120

	opt pm

	processor	16F628A
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 7 "X:\Hi-tech\USART\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 7 "X:\Hi-tech\USART\main.c"
	dw 0x3F7F & 0x3FFB & 0x3FFF & 0x3FFF & 0x3FEE ;#
	FNCALL	_main,_getch
	FNROOT	_main
	global	_INTCON
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:
_INTCON	set	11
	global	_RCREG
_RCREG	set	26
	global	_RCSTA
_RCSTA	set	24
	global	_RB4
_RB4	set	52
	global	_RB5
_RB5	set	53
	global	_RB6
_RB6	set	54
	global	_RB7
_RB7	set	55
	global	_RCIF
_RCIF	set	101
	global	_SPBRG
_SPBRG	set	153
	global	_TXSTA
_TXSTA	set	152
	global	_TRISB1
_TRISB1	set	1073
	global	_TRISB2
_TRISB2	set	1074
	global	_TRISB4
_TRISB4	set	1076
	global	_TRISB5
_TRISB5	set	1077
	global	_TRISB6
_TRISB6	set	1078
	global	_TRISB7
_TRISB7	set	1079
	file	"usart.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	??_getch
??_getch:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 0 bytes @ 0x0
	global	??_main
??_main:	; 0 bytes @ 0x0
	global	?_getch
?_getch:	; 1 bytes @ 0x0
	global	main@input
main@input:	; 1 bytes @ 0x0
	ds	1
;;Data sizes: Strings 0, constant 0, data 0, bss 0, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      1       1
;; BANK0           80      0       0
;; BANK1           80      0       0
;; BANK2           48      0       0

;;
;; Pointer list with targets:



;;
;; Critical Paths under _main in COMMON
;;
;;   None.
;;
;; Critical Paths under _main in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 0, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 1     1      0      15
;;                                              0 COMMON     1     1      0
;;                              _getch
;; ---------------------------------------------------------------------------------
;; (1) _getch                                                0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 1
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _getch
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA              80      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      1       1       1        7.1%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       1       2        0.0%
;;BANK0               50      0       0       3        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;ABS                  0      0       0       4        0.0%
;;BITBANK0            50      0       0       5        0.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK2            30      0       0       8        0.0%
;;BANK2               30      0       0       9        0.0%
;;DATA                 0      0       0      10        0.0%

	global	_main
psect	maintext

;; *************** function _main *****************
;; Defined at:
;;		line 12 in file "X:\Hi-tech\USART\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  input           1    0[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 17F/0
;;		Unchanged: FFE80/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         1       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_getch
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"X:\Hi-tech\USART\main.c"
	line	12
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 8
; Regs used in _main: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	14
	
l1047:	
;main.c: 13: unsigned char input;
;main.c: 14: TRISB4 = 0; TRISB6 = 0; TRISB7 = 0; TRISB5 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	(1076/8)^080h,(1076)&7
	bcf	(1078/8)^080h,(1078)&7
	bcf	(1079/8)^080h,(1079)&7
	bcf	(1077/8)^080h,(1077)&7
	line	15
	
l1049:	
;main.c: 15: INTCON=0;
	clrf	(11)	;volatile
	line	17
	
l1051:	
;main.c: 17: TRISB1 = 1; TRISB2 = 1; SPBRG = ((int)(4000000L/(16UL * 9600) -1)); RCSTA = (0|0x90); TXSTA = (0x4|0|0x20);
	bsf	(1073/8)^080h,(1073)&7
	
l1053:	
	bsf	(1074/8)^080h,(1074)&7
	
l1055:	
	movlw	(019h)
	movwf	(153)^080h	;volatile
	
l1057:	
	movlw	(090h)
	bcf	status, 5	;RP0=0, select bank0
	movwf	(24)	;volatile
	
l1059:	
	movlw	(024h)
	bsf	status, 5	;RP0=1, select bank1
	movwf	(152)^080h	;volatile
	line	23
	
l1061:	
;main.c: 23: RB4 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	(52/8),(52)&7
	line	24
	
l1063:	
;main.c: 24: RB5 = 0;
	bcf	(53/8),(53)&7
	line	25
	
l1065:	
;main.c: 25: RB6 = 0;
	bcf	(54/8),(54)&7
	line	26
	
l1067:	
;main.c: 26: RB7 = 0;
	bcf	(55/8),(55)&7
	line	30
	
l1069:	
;main.c: 30: input = getch();
	fcall	_getch
	movwf	(main@input)
	line	32
;main.c: 32: switch (input) {
	goto	l1081
	line	33
	
l1071:	
	movlw	1<<((52)&7)
	xorwf	((52)/8),f
	line	34
;main.c: 34: break;
	goto	l1069
	line	35
	
l1073:	
	movlw	1<<((53)&7)
	xorwf	((53)/8),f
	line	36
;main.c: 36: break;
	goto	l1069
	line	37
	
l1075:	
	movlw	1<<((54)&7)
	xorwf	((54)/8),f
	line	38
;main.c: 38: break;
	goto	l1069
	line	39
	
l1077:	
	movlw	1<<((55)&7)
	xorwf	((55)/8),f
	line	40
;main.c: 40: break;
	goto	l1069
	line	32
	
l1081:	
	movf	(main@input),w
	; Switch size 1, requested type "space"
; Number of cases is 4, Range of values is 49 to 52
; switch strategies available:
; Name         Bytes Cycles
; simple_byte    13     7 (average)
; direct_byte    20    16 (fixed)
;	Chosen strategy is simple_byte

	xorlw	49^0	; case 49
	skipnz
	goto	l1071
	xorlw	50^49	; case 50
	skipnz
	goto	l1073
	xorlw	51^50	; case 51
	skipnz
	goto	l1075
	xorlw	52^51	; case 52
	skipnz
	goto	l1077
	goto	l1069

	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

psect	maintext
	line	47
	signat	_main,88
	global	_getch
psect	text21,local,class=CODE,delta=2
global __ptext21
__ptext21:

;; *************** function _getch *****************
;; Defined at:
;;		line 15 in file "X:\Hi-tech\USART\usart.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 17F/0
;;		Unchanged: FFE80/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text21
	file	"X:\Hi-tech\USART\usart.c"
	line	15
	global	__size_of_getch
	__size_of_getch	equ	__end_of_getch-_getch
	
_getch:	
	opt	stack 7
; Regs used in _getch: [wreg]
	line	17
	
l1041:	
	line	18
;usart.c: 17: while(!RCIF)
	
l666:	
	line	17
	btfss	(101/8),(101)&7
	goto	u11
	goto	u10
u11:
	goto	l666
u10:
	line	19
	
l1043:	
;usart.c: 19: return RCREG;
	movf	(26),w	;volatile
	line	20
	
l669:	
	return
	opt stack 0
GLOBAL	__end_of_getch
	__end_of_getch:
;; =============== function _getch ends ============

	signat	_getch,89
psect	text22,local,class=CODE,delta=2
global __ptext22
__ptext22:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
