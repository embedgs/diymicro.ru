opt subtitle "HI-TECH Software Omniscient Code Generator (PRO mode) build 6446"

opt pagewidth 120

	opt pm

	processor	16F628A
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 9 "X:\Hi-tech\I2C\softi2c.c"
	psect config,class=CONFIG,delta=2 ;#
# 9 "X:\Hi-tech\I2C\softi2c.c"
	dw 0x3FFB & 0x3FFF & 0x3FFF & 0x3F7F & 0x3FEE ;#
	FNCALL	_main,_i2c_start
	FNCALL	_main,_i2c_tx
	FNCALL	_main,_i2c_stop
	FNCALL	_i2c_tx,_i2c_delay
	FNCALL	_i2c_stop,_i2c_delay
	FNCALL	_i2c_start,_i2c_delay
	FNROOT	_main
	global	_CMCON
psect	text55,local,class=CODE,delta=2
global __ptext55
__ptext55:
_CMCON	set	31
	global	_RA1
_RA1	set	41
	global	_RB4
_RB4	set	52
	global	_TRISA0
_TRISA0	set	1064
	global	_TRISA1
_TRISA1	set	1065
	global	_TRISB4
_TRISB4	set	1076
	file	"soft_i2c.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_i2c_delay
?_i2c_delay:	; 0 bytes @ 0x0
	global	??_i2c_delay
??_i2c_delay:	; 0 bytes @ 0x0
	global	?_i2c_start
?_i2c_start:	; 0 bytes @ 0x0
	global	?_i2c_stop
?_i2c_stop:	; 0 bytes @ 0x0
	global	?_i2c_tx
?_i2c_tx:	; 1 bit 
	global	?_main
?_main:	; 0 bytes @ 0x0
	ds	1
	global	??_i2c_start
??_i2c_start:	; 0 bytes @ 0x1
	global	??_i2c_stop
??_i2c_stop:	; 0 bytes @ 0x1
	global	??_i2c_tx
??_i2c_tx:	; 0 bytes @ 0x1
	global	i2c_tx@d
i2c_tx@d:	; 1 bytes @ 0x1
	ds	1
	global	i2c_tx@x
i2c_tx@x:	; 1 bytes @ 0x2
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0x3
;;Data sizes: Strings 0, constant 0, data 0, bss 0, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      3       3
;; BANK0           80      0       0
;; BANK1           80      0       0
;; BANK2           48      0       0

;;
;; Pointer list with targets:



;;
;; Critical Paths under _main in COMMON
;;
;;   _main->_i2c_tx
;;   _i2c_tx->_i2c_delay
;;   _i2c_stop->_i2c_delay
;;   _i2c_start->_i2c_delay
;;
;; Critical Paths under _main in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 0, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 0     0      0      45
;;                          _i2c_start
;;                             _i2c_tx
;;                           _i2c_stop
;; ---------------------------------------------------------------------------------
;; (1) _i2c_tx                                               2     2      0      45
;;                                              1 COMMON     2     2      0
;;                          _i2c_delay
;; ---------------------------------------------------------------------------------
;; (1) _i2c_stop                                             0     0      0       0
;;                          _i2c_delay
;; ---------------------------------------------------------------------------------
;; (1) _i2c_start                                            0     0      0       0
;;                          _i2c_delay
;; ---------------------------------------------------------------------------------
;; (2) _i2c_delay                                            1     1      0       0
;;                                              0 COMMON     1     1      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _i2c_start
;;     _i2c_delay
;;   _i2c_tx
;;     _i2c_delay
;;   _i2c_stop
;;     _i2c_delay
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA              80      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      3       3       1       21.4%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       2       2        0.0%
;;BANK0               50      0       0       3        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;ABS                  0      0       0       4        0.0%
;;BITBANK0            50      0       0       5        0.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK2            30      0       0       8        0.0%
;;BANK2               30      0       0       9        0.0%
;;DATA                 0      0       0      10        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 99 in file "X:\Hi-tech\I2C\softi2c.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 60/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_i2c_start
;;		_i2c_tx
;;		_i2c_stop
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"X:\Hi-tech\I2C\softi2c.c"
	line	99
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 8
; Regs used in _main: [wreg+status,2+status,0+pclath+cstack]
	line	101
	
l803:	
;softi2c.c: 101: CMCON = 0x07;
	movlw	(07h)
	movwf	(31)	;volatile
	line	115
	
l805:	
;softi2c.c: 115: TRISB4 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	(1076/8)^080h,(1076)&7
	line	117
	
l807:	
;softi2c.c: 117: i2c_start();
	fcall	_i2c_start
	line	118
	
l809:	
;softi2c.c: 118: i2c_tx(0b11101000);
	movlw	(0E8h)
	fcall	_i2c_tx
	line	119
	
l811:	
;softi2c.c: 119: i2c_tx(0b00000110);
	movlw	(06h)
	fcall	_i2c_tx
	line	120
	
l813:	
;softi2c.c: 120: i2c_tx(0x00);
	movlw	(0)
	fcall	_i2c_tx
	line	121
	
l815:	
;softi2c.c: 121: i2c_tx(0x00);
	movlw	(0)
	fcall	_i2c_tx
	line	122
	
l817:	
;softi2c.c: 122: i2c_stop();
	fcall	_i2c_stop
	line	124
	
l819:	
;softi2c.c: 124: i2c_start();
	fcall	_i2c_start
	line	125
	
l821:	
;softi2c.c: 125: i2c_tx(0b11101000);
	movlw	(0E8h)
	fcall	_i2c_tx
	line	126
	
l823:	
;softi2c.c: 126: i2c_tx(0b00000010);
	movlw	(02h)
	fcall	_i2c_tx
	line	127
	
l825:	
;softi2c.c: 127: i2c_tx(0b10111110);
	movlw	(0BEh)
	fcall	_i2c_tx
	line	128
	
l827:	
;softi2c.c: 128: i2c_tx(0b10111110);
	movlw	(0BEh)
	fcall	_i2c_tx
	line	129
	
l829:	
;softi2c.c: 129: i2c_stop();
	fcall	_i2c_stop
	line	130
	
l831:	
;softi2c.c: 130: RB4 = 1;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bsf	(52/8),(52)&7
	line	131
;softi2c.c: 131: for (;;) {}
	
l360:	
	goto	l360
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

psect	maintext
	line	132
	signat	_main,88
	global	_i2c_tx
psect	text56,local,class=CODE,delta=2
global __ptext56
__ptext56:

;; *************** function _i2c_tx *****************
;; Defined at:
;;		line 44 in file "X:\Hi-tech\I2C\softi2c.c"
;; Parameters:    Size  Location     Type
;;  d               1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  d               1    1[COMMON] unsigned char 
;;  x               1    2[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/20
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         2       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         2       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_i2c_delay
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text56
	file	"X:\Hi-tech\I2C\softi2c.c"
	line	44
	global	__size_of_i2c_tx
	__size_of_i2c_tx	equ	__end_of_i2c_tx-_i2c_tx
	
_i2c_tx:	
	opt	stack 7
; Regs used in _i2c_tx: [wreg+status,2+status,0+pclath+cstack]
;i2c_tx@d stored from wreg
	line	47
	movwf	(i2c_tx@d)
	
l767:	
;softi2c.c: 45: char x;
;softi2c.c: 46: static bit b;
;softi2c.c: 47: for (x=0; x<8; x++) {
	clrf	(i2c_tx@x)
	line	48
	
l773:	
;softi2c.c: 48: i2c_delay();
	fcall	_i2c_delay
	line	49
	
l775:	
;softi2c.c: 49: if (d&0x80) TRISA1 = 1;
	btfss	(i2c_tx@d),(7)&7
	goto	u11
	goto	u10
u11:
	goto	l345
u10:
	
l777:	
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1065/8)^080h,(1065)&7
	goto	l779
	line	50
	
l345:	
;softi2c.c: 50: else TRISA1 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1065/8)^080h,(1065)&7
	line	51
	
l779:	
;softi2c.c: 51: i2c_delay();
	fcall	_i2c_delay
	line	52
	
l781:	
;softi2c.c: 52: TRISA0 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1064/8)^080h,(1064)&7
	line	53
	
l783:	
;softi2c.c: 53: d <<= 1;
	clrc
	rlf	(i2c_tx@d),f
	line	54
;softi2c.c: 54: i2c_delay();
	fcall	_i2c_delay
	line	55
	
l785:	
;softi2c.c: 55: TRISA0 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1064/8)^080h,(1064)&7
	line	47
	
l787:	
	incf	(i2c_tx@x),f
	
l789:	
	movlw	(08h)
	subwf	(i2c_tx@x),w
	skipc
	goto	u21
	goto	u20
u21:
	goto	l773
u20:
	line	57
	
l791:	
;softi2c.c: 56: }
;softi2c.c: 57: i2c_delay();
	fcall	_i2c_delay
	line	58
;softi2c.c: 58: i2c_delay();
	fcall	_i2c_delay
	line	59
	
l793:	
;softi2c.c: 59: TRISA1 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1065/8)^080h,(1065)&7
	line	60
	
l795:	
;softi2c.c: 60: TRISA0 = 1;
	bsf	(1064/8)^080h,(1064)&7
	line	61
;softi2c.c: 61: i2c_delay();
	fcall	_i2c_delay
	line	62
	
l797:	
;softi2c.c: 62: b = RA1;
	line	63
	
l799:	
;softi2c.c: 63: TRISA0 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1064/8)^080h,(1064)&7
	line	66
	
l347:	
	return
	opt stack 0
GLOBAL	__end_of_i2c_tx
	__end_of_i2c_tx:
;; =============== function _i2c_tx ends ============

	signat	_i2c_tx,4216
	global	_i2c_stop
psect	text57,local,class=CODE,delta=2
global __ptext57
__ptext57:

;; *************** function _i2c_stop *****************
;; Defined at:
;;		line 34 in file "X:\Hi-tech\I2C\softi2c.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/20
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_i2c_delay
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text57
	file	"X:\Hi-tech\I2C\softi2c.c"
	line	34
	global	__size_of_i2c_stop
	__size_of_i2c_stop	equ	__end_of_i2c_stop-_i2c_stop
	
_i2c_stop:	
	opt	stack 7
; Regs used in _i2c_stop: [wreg+status,2+status,0+pclath+cstack]
	line	35
	
l759:	
;softi2c.c: 35: TRISA1 = 0;
	bcf	(1065/8)^080h,(1065)&7
	line	36
	
l761:	
;softi2c.c: 36: i2c_delay();
	fcall	_i2c_delay
	line	37
	
l763:	
;softi2c.c: 37: TRISA0 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1064/8)^080h,(1064)&7
	line	38
;softi2c.c: 38: i2c_delay();
	fcall	_i2c_delay
	line	39
	
l765:	
;softi2c.c: 39: TRISA1 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1065/8)^080h,(1065)&7
	line	40
;softi2c.c: 40: i2c_delay();
	fcall	_i2c_delay
	line	41
	
l338:	
	return
	opt stack 0
GLOBAL	__end_of_i2c_stop
	__end_of_i2c_stop:
;; =============== function _i2c_stop ends ============

	signat	_i2c_stop,88
	global	_i2c_start
psect	text58,local,class=CODE,delta=2
global __ptext58
__ptext58:

;; *************** function _i2c_start *****************
;; Defined at:
;;		line 24 in file "X:\Hi-tech\I2C\softi2c.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/20
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_i2c_delay
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text58
	file	"X:\Hi-tech\I2C\softi2c.c"
	line	24
	global	__size_of_i2c_start
	__size_of_i2c_start	equ	__end_of_i2c_start-_i2c_start
	
_i2c_start:	
	opt	stack 7
; Regs used in _i2c_start: [wreg+status,2+status,0+pclath+cstack]
	line	25
	
l751:	
;softi2c.c: 25: TRISA0 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1064/8)^080h,(1064)&7
	line	26
;softi2c.c: 26: TRISA1 = 1;
	bsf	(1065/8)^080h,(1065)&7
	line	27
	
l753:	
;softi2c.c: 27: i2c_delay();
	fcall	_i2c_delay
	line	28
	
l755:	
;softi2c.c: 28: TRISA1 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1065/8)^080h,(1065)&7
	line	29
;softi2c.c: 29: i2c_delay();
	fcall	_i2c_delay
	line	30
	
l757:	
;softi2c.c: 30: TRISA0 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1064/8)^080h,(1064)&7
	line	31
	
l335:	
	return
	opt stack 0
GLOBAL	__end_of_i2c_start
	__end_of_i2c_start:
;; =============== function _i2c_start ends ============

	signat	_i2c_start,88
	global	_i2c_delay
psect	text59,local,class=CODE,delta=2
global __ptext59
__ptext59:

;; *************** function _i2c_delay *****************
;; Defined at:
;;		line 19 in file "X:\Hi-tech\I2C\softi2c.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/20
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          1       0       0       0
;;      Totals:         1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_i2c_start
;;		_i2c_stop
;;		_i2c_tx
;; This function uses a non-reentrant model
;;
psect	text59
	file	"X:\Hi-tech\I2C\softi2c.c"
	line	19
	global	__size_of_i2c_delay
	__size_of_i2c_delay	equ	__end_of_i2c_delay-_i2c_delay
	
_i2c_delay:	
	opt	stack 6
; Regs used in _i2c_delay: [wreg]
	line	20
	
l749:	
;softi2c.c: 20: _delay((unsigned long)((10)*(4000000/4000000.0)));
	opt asmopt_off
movlw	3
movwf	(??_i2c_delay+0)+0,f
u37:
decfsz	(??_i2c_delay+0)+0,f
	goto	u37
opt asmopt_on

	line	21
	
l332:	
	return
	opt stack 0
GLOBAL	__end_of_i2c_delay
	__end_of_i2c_delay:
;; =============== function _i2c_delay ends ============

	signat	_i2c_delay,88
psect	text60,local,class=CODE,delta=2
global __ptext60
__ptext60:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
