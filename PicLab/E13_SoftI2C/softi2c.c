#include <htc.h>
#define _XTAL_FREQ 4000000

#define SCL TRISA0
#define SDA TRISA1
#define SCL_IN RA0
#define SDA_IN RA1

__CONFIG(WDTDIS & MCLREN & UNPROTECT & LVPDIS & HS);

unsigned char time = 0;

void set_time_i2c (unsigned char speed)  //��������� �������� ��������
{
time = speed/18;
}

void i2c_delay (void) //�������� speed/18
{
__delay_us(10);
}

void i2c_start(void)  //����� ��������
{
SCL = 1;
SDA = 1;
i2c_delay();
SDA = 0;
i2c_delay();
SCL = 0;
}

void i2c_stop (void)
{
SDA = 0;
i2c_delay();
SCL = 1;
i2c_delay();
SDA = 1;
i2c_delay();
}

bit i2c_tx(unsigned char d)
{
char x;
static bit b;
   for (x=0; x<8; x++) {
       i2c_delay();
       if (d&0x80) SDA = 1;
       else SDA = 0;
       i2c_delay();
       SCL = 1;
       d <<= 1;
       i2c_delay();
       SCL = 0;
   }
i2c_delay();
i2c_delay();
SDA = 1;
SCL = 1;
i2c_delay();
b = SDA_IN;
SCL = 0;
return b;

}

unsigned char i2c_rx(unsigned char ack)
{
unsigned char x,d=0;
    SDA = 1;
   for (x=0; x<8;x++) {
    d <<= 1;
    do {
       i2c_delay();
       SCL = 1;
    } while (SCL_IN==0);
    i2c_delay();
    if (SDA_IN) d |= 1;
    SCL = 0;
    }
    i2c_delay();
    if (ack) SDA = 0;
       else SDA = 1;
    SCL = 1;
    i2c_delay();
    SCL = 0;
    
    
    return d;   
    
   

}



void main(void)
{

CMCON = 0x07;
/*
set_time_i2c(100);
i2c_delay();
i2c_start();
i2c_tx(0b01110110);
i2c_stop();

__delay_ms(100);
i2c_start();
i2c_rx(1);
i2c_stop();
*/

TRISB4 = 0;

i2c_start();
i2c_tx(0b11101000);
i2c_tx(0b00000110);
i2c_tx(0x00);
i2c_tx(0x00);
i2c_stop();

i2c_start();
i2c_tx(0b11101000);
i2c_tx(0b00000010);
i2c_tx(0b10111110);
i2c_tx(0b10111110);
i2c_stop();
RB4 = 1;
for (;;) {}	
}