opt subtitle "HI-TECH Software Omniscient Code Generator (PRO mode) build 5239"

opt pagewidth 120

	opt pm

	processor	16F628A
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 4 "X:\Hi-tech\PWM\pwm2.c"
	psect config,class=CONFIG,delta=2 ;#
# 4 "X:\Hi-tech\PWM\pwm2.c"
	dw 0x3FFB & 0x3FFF & 0x3FFF & 0x3F7F & 0x3FEE ;#
	FNROOT	_main
	FNCALL	_isr,_pwm
	FNCALL	intlevel1,_isr
	global	intlevel1
	FNROOT	intlevel1
	global	_CCP1CON
psect	text24,local,class=CODE,delta=2
global __ptext24
__ptext24:
_CCP1CON  equ     23
	global	_CCPR1H
_CCPR1H  equ     22
	global	_CCPR1L
_CCPR1L  equ     21
	global	_CMCON
_CMCON  equ     31
	global	_FSR
_FSR  equ     4
	global	_INDF
_INDF  equ     0
	global	_INTCON
_INTCON  equ     11
	global	_PCL
_PCL  equ     2
	global	_PCLATH
_PCLATH  equ     10
	global	_PIR1
_PIR1  equ     12
	global	_PORTA
_PORTA  equ     5
	global	_PORTB
_PORTB  equ     6
	global	_RCREG
_RCREG  equ     26
	global	_RCSTA
_RCSTA  equ     24
	global	_STATUS
_STATUS  equ     3
	global	_T1CON
_T1CON  equ     16
	global	_T2CON
_T2CON  equ     18
	global	_TMR0
_TMR0  equ     1
	global	_TMR1H
_TMR1H  equ     15
	global	_TMR1L
_TMR1L  equ     14
	global	_TMR2
_TMR2  equ     17
	global	_TXREG
_TXREG  equ     25
	global	_ADEN
_ADEN  equ     195
	global	_C1INV
_C1INV  equ     252
	global	_C1OUT
_C1OUT  equ     254
	global	_C2INV
_C2INV  equ     253
	global	_C2OUT
_C2OUT  equ     255
	global	_CARRY
_CARRY  equ     24
	global	_CCP1IF
_CCP1IF  equ     98
	global	_CCP1M0
_CCP1M0  equ     184
	global	_CCP1M1
_CCP1M1  equ     185
	global	_CCP1M2
_CCP1M2  equ     186
	global	_CCP1M3
_CCP1M3  equ     187
	global	_CCP1X
_CCP1X  equ     189
	global	_CCP1Y
_CCP1Y  equ     188
	global	_CIS
_CIS  equ     251
	global	_CM0
_CM0  equ     248
	global	_CM1
_CM1  equ     249
	global	_CM2
_CM2  equ     250
	global	_CMIF
_CMIF  equ     102
	global	_CREN
_CREN  equ     196
	global	_DC
_DC  equ     25
	global	_EEIF
_EEIF  equ     103
	global	_FERR
_FERR  equ     194
	global	_GIE
_GIE  equ     95
	global	_INTE
_INTE  equ     92
	global	_INTF
_INTF  equ     89
	global	_IRP
_IRP  equ     31
	global	_OERR
_OERR  equ     193
	global	_PD
_PD  equ     27
	global	_PEIE
_PEIE  equ     94
	global	_RA0
_RA0  equ     40
	global	_RA1
_RA1  equ     41
	global	_RA2
_RA2  equ     42
	global	_RA3
_RA3  equ     43
	global	_RA4
_RA4  equ     44
	global	_RA5
_RA5  equ     45
	global	_RA6
_RA6  equ     46
	global	_RA7
_RA7  equ     47
	global	_RB0
_RB0  equ     48
	global	_RB1
_RB1  equ     49
	global	_RB2
_RB2  equ     50
	global	_RB3
_RB3  equ     51
	global	_RB4
_RB4  equ     52
	global	_RB5
_RB5  equ     53
	global	_RB6
_RB6  equ     54
	global	_RB7
_RB7  equ     55
	global	_RBIE
_RBIE  equ     91
	global	_RBIF
_RBIF  equ     88
	global	_RCIF
_RCIF  equ     101
	global	_RP0
_RP0  equ     29
	global	_RP1
_RP1  equ     30
	global	_RX9
_RX9  equ     198
	global	_RX9D
_RX9D  equ     192
	global	_SPEN
_SPEN  equ     199
	global	_SREN
_SREN  equ     197
	global	_T0IE
_T0IE  equ     93
	global	_T0IF
_T0IF  equ     90
	global	_T1CKPS0
_T1CKPS0  equ     132
	global	_T1CKPS1
_T1CKPS1  equ     133
	global	_T1OSCEN
_T1OSCEN  equ     131
	global	_T1SYNC
_T1SYNC  equ     130
	global	_T2CKPS0
_T2CKPS0  equ     144
	global	_T2CKPS1
_T2CKPS1  equ     145
	global	_TMR1CS
_TMR1CS  equ     129
	global	_TMR1IF
_TMR1IF  equ     96
	global	_TMR1ON
_TMR1ON  equ     128
	global	_TMR2IF
_TMR2IF  equ     97
	global	_TMR2ON
_TMR2ON  equ     146
	global	_TO
_TO  equ     28
	global	_TOUTPS0
_TOUTPS0  equ     147
	global	_TOUTPS1
_TOUTPS1  equ     148
	global	_TOUTPS2
_TOUTPS2  equ     149
	global	_TOUTPS3
_TOUTPS3  equ     150
	global	_TXIF
_TXIF  equ     100
	global	_ZERO
_ZERO  equ     26
	global	_EEADR
_EEADR  equ     155
	global	_EECON1
_EECON1  equ     156
	global	_EECON2
_EECON2  equ     157
	global	_EEDATA
_EEDATA  equ     154
	global	_OPTION
_OPTION  equ     129
	global	_PCON
_PCON  equ     142
	global	_PIE1
_PIE1  equ     140
	global	_PR2
_PR2  equ     146
	global	_SPBRG
_SPBRG  equ     153
	global	_TRISA
_TRISA  equ     133
	global	_TRISB
_TRISB  equ     134
	global	_TXSTA
_TXSTA  equ     152
	global	_VRCON
_VRCON  equ     159
	global	_BOR
_BOR  equ     1136
	global	_BRGH
_BRGH  equ     1218
	global	_CCP1IE
_CCP1IE  equ     1122
	global	_CMIE
_CMIE  equ     1126
	global	_CSRC
_CSRC  equ     1223
	global	_EEIE
_EEIE  equ     1127
	global	_INTEDG
_INTEDG  equ     1038
	global	_OSCF
_OSCF  equ     1139
	global	_POR
_POR  equ     1137
	global	_PS0
_PS0  equ     1032
	global	_PS1
_PS1  equ     1033
	global	_PS2
_PS2  equ     1034
	global	_PSA
_PSA  equ     1035
	global	_RBPU
_RBPU  equ     1039
	global	_RCIE
_RCIE  equ     1125
	global	_RD
_RD  equ     1248
	global	_SYNC
_SYNC  equ     1220
	global	_T0CS
_T0CS  equ     1037
	global	_T0SE
_T0SE  equ     1036
	global	_TMR1IE
_TMR1IE  equ     1120
	global	_TMR2IE
_TMR2IE  equ     1121
	global	_TRISA0
_TRISA0  equ     1064
	global	_TRISA1
_TRISA1  equ     1065
	global	_TRISA2
_TRISA2  equ     1066
	global	_TRISA3
_TRISA3  equ     1067
	global	_TRISA4
_TRISA4  equ     1068
	global	_TRISA5
_TRISA5  equ     1069
	global	_TRISA6
_TRISA6  equ     1070
	global	_TRISA7
_TRISA7  equ     1071
	global	_TRISB0
_TRISB0  equ     1072
	global	_TRISB1
_TRISB1  equ     1073
	global	_TRISB2
_TRISB2  equ     1074
	global	_TRISB3
_TRISB3  equ     1075
	global	_TRISB4
_TRISB4  equ     1076
	global	_TRISB5
_TRISB5  equ     1077
	global	_TRISB6
_TRISB6  equ     1078
	global	_TRISB7
_TRISB7  equ     1079
	global	_TRMT
_TRMT  equ     1217
	global	_TX9
_TX9  equ     1222
	global	_TX9D
_TX9D  equ     1216
	global	_TXEN
_TXEN  equ     1221
	global	_TXIE
_TXIE  equ     1124
	global	_VR0
_VR0  equ     1272
	global	_VR1
_VR1  equ     1273
	global	_VR2
_VR2  equ     1274
	global	_VR3
_VR3  equ     1275
	global	_VREN
_VREN  equ     1279
	global	_VROE
_VROE  equ     1278
	global	_VRR
_VRR  equ     1277
	global	_WR
_WR  equ     1249
	global	_WREN
_WREN  equ     1250
	global	_WRERR
_WRERR  equ     1251
	file	"pwm.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initationation code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	??__delay
??__delay: ;@ 0x0
	global	?__delay
?__delay: ;@ 0x0
	global	??_main
??_main: ;@ 0x0
	global	?_main
?_main: ;@ 0x0
	global	__delay$0
__delay$0:	; 4 bytes @ 0x0
	ds	4
	global	??_pwm
??_pwm: ;@ 0x4
	ds	2
	global	?_pwm
?_pwm: ;@ 0x6
	global	??_isr
??_isr: ;@ 0x6
	ds	3
	global	?_isr
?_isr: ;@ 0x9
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	pwm@state
pwm@state:	; 1 bytes @ 0x0
	ds	1
	global	pwm@x
pwm@x:	; 1 bytes @ 0x1
	ds	1
;Data sizes: Strings 0, constant 0, data 0, bss 0, persistent 0 stack 0
;Auto spaces:   Size  Autos    Used
; COMMON          14      9       9
; BANK0           80      2       2
; BANK1           80      0       0
; BANK2           48      0       0


;Pointer list with targets:



;Main: autosize = 0, tempsize = 0, incstack = 0, save=0


;Call graph:                      Base Space Used Autos Args Refs Density
;_main                                                0    0    0   0.00
; Estimated maximum call depth 0
;_isr                                                 3    0   71   0.00
;                                    6 COMMO    3
;                _pwm
;  _pwm                                               4    0   71   0.00
;                                    4 COMMO    2
;                                    0 BANK0    2
;             __delay
;    __delay                                          0    4    0   0.00
;                                    0 COMMO    4
; Estimated maximum call depth 2
; Address spaces:

;Name               Size   Autos  Total    Cost      Usage
;BITCOMMON            E      0       0       0        0.0%
;NULL                 0      0       0       0        0.0%
;CODE                 0      0       0       0        0.0%
;COMMON               E      9       9       1       64.3%
;BITSFR0              0      0       0       1        0.0%
;SFR0                 0      0       0       1        0.0%
;BITSFR1              0      0       0       2        0.0%
;SFR1                 0      0       0       2        0.0%
;STACK                0      0       0       2        0.0%
;BANK0               50      2       2       3        2.5%
;BANK1               50      0       0       4        0.0%
;BITSFR3              0      0       0       4        0.0%
;SFR3                 0      0       0       4        0.0%
;BANK2               30      0       0       5        0.0%
;SFR2                 0      0       0       5        0.0%
;BITSFR2              0      0       0       5        0.0%
;ABS                  0      0       5       6        0.0%
;BITBANK0            50      0       0       7        0.0%
;BITBANK1            50      0       0       8        0.0%
;BITBANK2            30      0       0       9        0.0%
;DATA                 0      0       5      10        0.0%
;EEDATA              80      0       0    1000        0.0%

	global	_main
psect	maintext,local,class=CODE,delta=2
global __pmaintext
__pmaintext:

; *************** function _main *****************
; Defined at:
;		line 78 in file "X:\Hi-tech\PWM\pwm2.c"
; Parameters:    Size  Location     Type
;		None
; Auto vars:     Size  Location     Type
;		None
; Return value:  Size  Location     Type
;		None               void
; Registers used:
;		wreg, status,2
; Tracked objects:
;		On entry : 17F/0
;		On exit  : 17F/0
;		Unchanged: FFE80/0
; Data sizes:     COMMON   BANK0   BANK1   BANK2
;      Locals:         0       0       0       0
;      Temp:     0
;      Total:    0
; This function calls:
;		Nothing
; This function is called by:
;		Startup code after reset
; This function uses a non-reentrant model
; 
psect	maintext
	file	"X:\Hi-tech\PWM\pwm2.c"
	line	78
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
;pwm2.c: 78: void main() {
	
_main:	
	opt stack 5
; Regs used in _main: [wreg+status,2]
	line	80
	
l30000300:	
;pwm2.c: 80: PORTB = 0;
	clrf	(6)	;volatile
	
l30000301:	
	line	81
;pwm2.c: 81: TRISB = 0b00000111;
	movlw	(07h)
	bsf	status, 5	;RP0=1, select bank1
	movwf	(134)^080h	;volatile
	
l30000302:	
	line	82
;pwm2.c: 82: PR2 = 0b01111100;
	movlw	(07Ch)
	movwf	(146)^080h
	line	84
;pwm2.c: 84: CCPR1L = 0;
	bcf	status, 5	;RP0=0, select bank0
	clrf	(21)	;volatile
	
l30000303:	
	line	85
;pwm2.c: 85: CCP1X = 0;
	bcf	(189/8),(189)&7
	
l30000304:	
	line	86
;pwm2.c: 86: CCP1Y = 0;
	bcf	(188/8),(188)&7
	
l30000305:	
	line	88
;pwm2.c: 88: CCP1M3 = 1;
	bsf	(187/8),(187)&7
	
l30000306:	
	line	89
;pwm2.c: 89: CCP1M2 = 1;
	bsf	(186/8),(186)&7
	
l30000307:	
	line	90
;pwm2.c: 90: T2CKPS0 = 1; T2CKPS1 = 0;
	bsf	(144/8),(144)&7
	
l30000308:	
	bcf	(145/8),(145)&7
	
l30000309:	
	line	91
;pwm2.c: 91: TMR2ON = 1;
	bsf	(146/8),(146)&7
	
l30000310:	
	line	95
;pwm2.c: 95: GIE = 1;
	bsf	(95/8),(95)&7
	
l30000311:	
	line	96
;pwm2.c: 96: INTE = 1;
	bsf	(92/8),(92)&7
	
l27:	
	goto	l27
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
; =============== function _main ends ============

psect	maintext
	line	102
	signat	_main,88
	global	_isr
psect	text25,local,class=CODE,delta=2
global __ptext25
__ptext25:

; *************** function _isr *****************
; Defined at:
;		line 105 in file "X:\Hi-tech\PWM\pwm2.c"
; Parameters:    Size  Location     Type
;		None
; Auto vars:     Size  Location     Type
;		None
; Return value:  Size  Location     Type
;		None               void
; Registers used:
;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
; Tracked objects:
;		On entry : 0/0
;		On exit  : 60/0
;		Unchanged: 0/0
; Data sizes:     COMMON   BANK0   BANK1   BANK2
;      Locals:         3       0       0       0
;      Temp:     3
;      Total:    3
; This function calls:
;		_pwm
; This function is called by:
;		Interrupt level 1
; This function uses a non-reentrant model
; 
psect	text25
	file	"X:\Hi-tech\PWM\pwm2.c"
	line	105
	global	__size_of_isr
	__size_of_isr	equ	__end_of_isr-_isr
;pwm2.c: 105: void interrupt isr() {
	
_isr:	
	opt stack 8
; Regs used in _isr: [wreg-fsr0h+status,2+status,0+pclath+cstack]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+1
	movwf	saved_w
	movf	status,w
	movwf	(??_isr+0)
	movf	fsr0,w
	movwf	(??_isr+1)
	movf	pclath,w
	movwf	(??_isr+2)
	ljmp	_isr
psect	text25
	line	106
	
i1l30000362:	
;pwm2.c: 106: if (INTF) {
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(89/8),(89)&7
	goto	u20_21
	goto	u20_20
u20_21:
	goto	i1l30
u20_20:
	
i1l30000363:	
	line	107
;pwm2.c: 107: pwm();
	fcall	_pwm
	
i1l30:	
	movf	(??_isr+2),w
	movwf	pclath
	movf	(??_isr+1),w
	movwf	fsr0
	movf	(??_isr+0),w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_isr
	__end_of_isr:
; =============== function _isr ends ============

psect	text26,local,class=CODE,delta=2
global __ptext26
__ptext26:
	line	109
	signat	_isr,88
	global	_pwm

; *************** function _pwm *****************
; Defined at:
;		line 6 in file "X:\Hi-tech\PWM\pwm2.c"
; Parameters:    Size  Location     Type
;		None
; Auto vars:     Size  Location     Type
;  x               1    1[BANK0 ] unsigned char 
;  state           1    0[BANK0 ] unsigned char 
; Return value:  Size  Location     Type
;		None               void
; Registers used:
;		wreg, fsr0l, fsr0h, status,2, status,0
; Tracked objects:
;		On entry : 60/0
;		On exit  : 60/0
;		Unchanged: 0/0
; Data sizes:     COMMON   BANK0   BANK1   BANK2
;      Locals:         2       2       0       0
;      Temp:     2
;      Total:    4
; This function calls:
;		__delay
; This function is called by:
;		_isr
; This function uses a non-reentrant model
; 
psect	text26
	file	"X:\Hi-tech\PWM\pwm2.c"
	line	6
	global	__size_of_pwm
	__size_of_pwm	equ	__end_of_pwm-_pwm
;pwm2.c: 4: asm("\tpsect config,class=CONFIG,delta=2"); asm("\tdw ""0x3FFB & 0x3FFF & 0x3FFF & 0x3F7F & 0x3FEE");
;pwm2.c: 6: void pwm() {
	
_pwm:	
	opt stack 7
; Regs used in _pwm: [wreg-fsr0h+status,2+status,0]
	line	7
	
i1l30000314:	
;pwm2.c: 7: unsigned char x = 0;
	clrf	(pwm@x)
	
i1l30000315:	
	line	8
;pwm2.c: 8: unsigned char state = 1;
	clrf	(pwm@state)
	incf	(pwm@state),f
	goto	i1l30000361
	
i1l3:	
	line	11
;pwm2.c: 11: if (!RB0) {
	btfsc	(48/8),(48)&7
	goto	u11_21
	goto	u11_20
u11_21:
	goto	i1l30000321
u11_20:
	
i1l30000316:	
	line	12
;pwm2.c: 12: _delay((unsigned long)((100)*(4000000/4000.0)));
	movlw	130
movwf	(??_pwm+0+0+1),f
	movlw	221
movwf	(??_pwm+0+0),f
u21_27:
	decfsz	(??_pwm+0+0),f
	goto	u21_27
	decfsz	(??_pwm+0+0+1),f
	goto	u21_27
	nop2

	
i1l30000317:	
	line	13
;pwm2.c: 13: if (!RB0) {
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfsc	(48/8),(48)&7
	goto	u12_21
	goto	u12_20
u12_21:
	goto	i1l30000321
u12_20:
	
i1l30000318:	
	line	14
;pwm2.c: 14: if (x > 0) { x--;} else x = 0;
	movf	(pwm@x),w
	skipz
	goto	u13_20
	goto	i1l30000320
u13_20:
	
i1l30000319:	
	decf	(pwm@x),f
	goto	i1l30000321
	
i1l30000320:	
	clrf	(pwm@x)
	
i1l30000321:	
	line	18
;pwm2.c: 15: }
;pwm2.c: 16: }
;pwm2.c: 18: if (!RB1) {
	btfsc	(49/8),(49)&7
	goto	u14_21
	goto	u14_20
u14_21:
	goto	i1l30000327
u14_20:
	
i1l30000322:	
	line	19
;pwm2.c: 19: _delay((unsigned long)((100)*(4000000/4000.0)));
	movlw	130
movwf	(??_pwm+0+0+1),f
	movlw	221
movwf	(??_pwm+0+0),f
u22_27:
	decfsz	(??_pwm+0+0),f
	goto	u22_27
	decfsz	(??_pwm+0+0+1),f
	goto	u22_27
	nop2

	
i1l30000323:	
	line	20
;pwm2.c: 20: if (!RB1) {
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfsc	(49/8),(49)&7
	goto	u15_21
	goto	u15_20
u15_21:
	goto	i1l30000327
u15_20:
	
i1l30000324:	
	line	21
;pwm2.c: 21: if (x < 9) { x++;} else x = 8;
	movlw	(09h)
	subwf	(pwm@x),w
	skipnc
	goto	u16_21
	goto	u16_20
u16_21:
	goto	i1l30000326
u16_20:
	
i1l30000325:	
	incf	(pwm@x),f
	goto	i1l30000327
	
i1l30000326:	
	movlw	(08h)
	movwf	(pwm@x)
	
i1l30000327:	
	line	26
;pwm2.c: 23: }
;pwm2.c: 24: }
;pwm2.c: 26: if (!RB2) {
	btfsc	(50/8),(50)&7
	goto	u17_21
	goto	u17_20
u17_21:
	goto	i1l30000360
u17_20:
	
i1l30000328:	
	line	27
;pwm2.c: 27: _delay((unsigned long)((100)*(4000000/4000.0)));
	movlw	130
movwf	(??_pwm+0+0+1),f
	movlw	221
movwf	(??_pwm+0+0),f
u23_27:
	decfsz	(??_pwm+0+0),f
	goto	u23_27
	decfsz	(??_pwm+0+0+1),f
	goto	u23_27
	nop2

	
i1l30000329:	
	line	28
;pwm2.c: 28: if (!RB2) {
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfsc	(50/8),(50)&7
	goto	u18_21
	goto	u18_20
u18_21:
	goto	i1l30000360
u18_20:
	
i1l30000330:	
	line	29
;pwm2.c: 29: state = 0;
	clrf	(pwm@state)
	
i1l30000331:	
	line	30
;pwm2.c: 30: INTF = 0;
	bcf	(89/8),(89)&7
	goto	i1l30000360
	
i1l30000332:	
	line	36
;pwm2.c: 36: CCPR1L = 0;
	clrf	(21)	;volatile
	
i1l30000333:	
	line	37
;pwm2.c: 37: CCP1X = 0;
	bcf	(189/8),(189)&7
	
i1l30000334:	
	line	38
;pwm2.c: 38: CCP1Y = 0; break;
	bcf	(188/8),(188)&7
	goto	i1l30000361
	
i1l30000335:	
	line	40
;pwm2.c: 40: CCPR1L = 0b00001111;
	movlw	(0Fh)
	movwf	(21)	;volatile
	
i1l30000336:	
	line	41
;pwm2.c: 41: CCP1X = 1;
	bsf	(189/8),(189)&7
	goto	i1l30000334
	
i1l30000338:	
	line	44
;pwm2.c: 44: CCPR1L = 0b00011111;
	movlw	(01Fh)
	movwf	(21)	;volatile
	goto	i1l30000333
	
i1l30000341:	
	line	48
;pwm2.c: 48: CCPR1L = 0b00101110;
	movlw	(02Eh)
	movwf	(21)	;volatile
	
i1l30000342:	
	line	49
;pwm2.c: 49: CCP1X = 1;
	bsf	(189/8),(189)&7
	
i1l30000343:	
	line	50
;pwm2.c: 50: CCP1Y = 1; break;
	bsf	(188/8),(188)&7
	goto	i1l30000361
	
i1l30000344:	
	line	52
;pwm2.c: 52: CCPR1L = 0b00111110;
	movlw	(03Eh)
	movwf	(21)	;volatile
	
i1l30000345:	
	line	53
;pwm2.c: 53: CCP1X = 0;
	bcf	(189/8),(189)&7
	goto	i1l30000343
	
i1l30000347:	
	line	56
;pwm2.c: 56: CCPR1L = 0b01001110;
	movlw	(04Eh)
	movwf	(21)	;volatile
	goto	i1l30000333
	
i1l30000350:	
	line	60
;pwm2.c: 60: CCPR1L = 0b01011101;
	movlw	(05Dh)
	movwf	(21)	;volatile
	goto	i1l30000336
	
i1l30000353:	
	line	64
;pwm2.c: 64: CCPR1L = 0b01101101;
	movlw	(06Dh)
	movwf	(21)	;volatile
	goto	i1l30000345
	
i1l30000356:	
	line	68
;pwm2.c: 68: CCPR1L = 0b011111100;
	movlw	(0FCh)
	movwf	(21)	;volatile
	goto	i1l30000342
	
i1l30000360:	
	line	34
	movf	(pwm@x),w
		xorlw	0^0
	skipnz
	goto	i1l30000332
	xorlw	1^0
	skipnz
	goto	i1l30000335
	xorlw	2^1
	skipnz
	goto	i1l30000338
	xorlw	3^2
	skipnz
	goto	i1l30000341
	xorlw	4^3
	skipnz
	goto	i1l30000344
	xorlw	5^4
	skipnz
	goto	i1l30000347
	xorlw	6^5
	skipnz
	goto	i1l30000350
	xorlw	7^6
	skipnz
	goto	i1l30000353
	xorlw	8^7
	skipnz
	goto	i1l30000356
	goto	i1l30000361

	
i1l30000361:	
	line	10
	movf	(pwm@state),f
	skipz
	goto	u19_21
	goto	u19_20
u19_21:
	goto	i1l3
u19_20:
	
i1l1:	
	return
	opt stack 0
GLOBAL	__end_of_pwm
	__end_of_pwm:
; =============== function _pwm ends ============

psect	text27,local,class=CODE,delta=2
global __ptext27
__ptext27:
	line	76
	signat	_pwm,88
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	end
