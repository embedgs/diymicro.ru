#include <htc.h>
#define _XTAL_FREQ 4000000
 
__CONFIG(WDTDIS & UNPROTECT & MCLREN & LVPDIS & HS);

void pwm() {
unsigned char x = 0;
unsigned char state = 1;

while (state) {
if (!RB0) {
           __delay_ms(100);
           if (!RB0) {
                     if (x > 0) { x--;} else x = 0;
           }
}

if (!RB1) {
           __delay_ms(100);
           if (!RB1) {
                     if (x < 9) { x++;} else x = 8;       

           }
}

if (!RB2) {                               // ����� �� ���������� (� ������ ��������� ����� � �� ������)
           __delay_ms(100);
           if (!RB2) {
		         state = 0;
                 INTF = 0;     
           }
}

switch (x) {
case 0:                     // 0%
       CCPR1L = 0;                               
       CCP1X = 0;
       CCP1Y = 0;  break;
case 1:                     // 12.5%
       CCPR1L = 0b00001111;                               
       CCP1X = 1;
       CCP1Y = 0;  break;
case 2:                     // 25%
       CCPR1L = 0b00011111;                               
       CCP1X = 0;
       CCP1Y = 0;  break;    	 
case 3:                     // 37.5%
       CCPR1L = 0b00101110;                               
       CCP1X = 1;
       CCP1Y = 1;  break;    	 
case 4:                     // 50%
       CCPR1L = 0b00111110;                               
       CCP1X = 0;
       CCP1Y = 1;  break;   	 
case 5:                     // 62.5%
       CCPR1L = 0b01001110;                               
       CCP1X = 0;
       CCP1Y = 0;  break;   	 
case 6:                     // 75%
       CCPR1L = 0b01011101;                               
       CCP1X = 1;
       CCP1Y = 0;  break;
case 7:                     // 87.5%
       CCPR1L = 0b01101101;                               
       CCP1X = 0;
       CCP1Y = 1;  break;
case 8:                     // 100%
       CCPR1L = 0b011111100;                               
       CCP1X = 1;
       CCP1Y = 1;  break;

}


}
}

void main() {

PORTB = 0;
TRISB = 0b00000111; 
PR2 = 0b01111100;

CCPR1L = 0;                               // �� ��������� 0%
CCP1X = 0;
CCP1Y = 0;

CCP1M3 = 1;                               // �������� ���
CCP1M2 = 1;
T2CKPS0 = 1; T2CKPS1 = 0;                 // ������������ �� 4
TMR2ON = 1;                               // �������� 2 ������



GIE = 1;
INTE = 1;

for (;;) {
                                          // ������ ����������� �����
}
 
}


void interrupt isr() {                    // ���������� �� RB0      
if (INTF) {
pwm();
}
}
