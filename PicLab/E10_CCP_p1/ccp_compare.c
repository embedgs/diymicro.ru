#include <htc.h>
#define _XTAL_FREQ 4000000
 
__CONFIG(WDTDIS & UNPROTECT & MCLREN & LVPDIS);
 
void main() {
unsigned char x;
__delay_ms(100);
TRISB = 0x00;
PORTB = 0x00;
 
CCPR1H = 0b00000011; CCPR1L = 0b11101000; // 1000 in CCPR1
CCP1M3 = 1;
CCP1M2 = 0;
CCP1M1 = 1;
CCP1M0 = 0;
 
T1CKPS1 = 0;
T1CKPS0 = 0; // Prescaler = 0
 
T1OSCEN = 0;
TMR1CS = 0; // Fosc/4
TMR1ON = 1;
T1SYNC = 0; 
GIE = 1;
PEIE = 1;
CCP1IE = 1;
for (;;) {

}
 
}

void interrupt isr() {
if (CCP1IF) {
RB3 = !RB3;
TMR1H = 0x00; TMR1L = 0x00;
CCP1IF = 0;
}

}