opt subtitle "HI-TECH Software Omniscient Code Generator (PRO mode) build 5239"

opt pagewidth 120

	opt pm

	processor	16F628A
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 4 "X:\Hi-tech\CCP_Compare\ccp_compare.c"
	psect config,class=CONFIG,delta=2 ;#
# 4 "X:\Hi-tech\CCP_Compare\ccp_compare.c"
	dw 0x3FFB & 0x3FFF & 0x3FFF & 0x3F7F ;#
;COMMON:	_main->__delay
	FNROOT	_main
	FNCALL	intlevel1,_isr
	global	intlevel1
	FNROOT	intlevel1
	global	_CCP1CON
psect	text10,local,class=CODE,delta=2
global __ptext10
__ptext10:
_CCP1CON  equ     23
	global	_CCPR1H
_CCPR1H  equ     22
	global	_CCPR1L
_CCPR1L  equ     21
	global	_CMCON
_CMCON  equ     31
	global	_FSR
_FSR  equ     4
	global	_INDF
_INDF  equ     0
	global	_INTCON
_INTCON  equ     11
	global	_PCL
_PCL  equ     2
	global	_PCLATH
_PCLATH  equ     10
	global	_PIR1
_PIR1  equ     12
	global	_PORTA
_PORTA  equ     5
	global	_PORTB
_PORTB  equ     6
	global	_RCREG
_RCREG  equ     26
	global	_RCSTA
_RCSTA  equ     24
	global	_STATUS
_STATUS  equ     3
	global	_T1CON
_T1CON  equ     16
	global	_T2CON
_T2CON  equ     18
	global	_TMR0
_TMR0  equ     1
	global	_TMR1H
_TMR1H  equ     15
	global	_TMR1L
_TMR1L  equ     14
	global	_TMR2
_TMR2  equ     17
	global	_TXREG
_TXREG  equ     25
	global	_ADEN
_ADEN  equ     195
	global	_C1INV
_C1INV  equ     252
	global	_C1OUT
_C1OUT  equ     254
	global	_C2INV
_C2INV  equ     253
	global	_C2OUT
_C2OUT  equ     255
	global	_CARRY
_CARRY  equ     24
	global	_CCP1IF
_CCP1IF  equ     98
	global	_CCP1M0
_CCP1M0  equ     184
	global	_CCP1M1
_CCP1M1  equ     185
	global	_CCP1M2
_CCP1M2  equ     186
	global	_CCP1M3
_CCP1M3  equ     187
	global	_CCP1X
_CCP1X  equ     189
	global	_CCP1Y
_CCP1Y  equ     188
	global	_CIS
_CIS  equ     251
	global	_CM0
_CM0  equ     248
	global	_CM1
_CM1  equ     249
	global	_CM2
_CM2  equ     250
	global	_CMIF
_CMIF  equ     102
	global	_CREN
_CREN  equ     196
	global	_DC
_DC  equ     25
	global	_EEIF
_EEIF  equ     103
	global	_FERR
_FERR  equ     194
	global	_GIE
_GIE  equ     95
	global	_INTE
_INTE  equ     92
	global	_INTF
_INTF  equ     89
	global	_IRP
_IRP  equ     31
	global	_OERR
_OERR  equ     193
	global	_PD
_PD  equ     27
	global	_PEIE
_PEIE  equ     94
	global	_RA0
_RA0  equ     40
	global	_RA1
_RA1  equ     41
	global	_RA2
_RA2  equ     42
	global	_RA3
_RA3  equ     43
	global	_RA4
_RA4  equ     44
	global	_RA5
_RA5  equ     45
	global	_RA6
_RA6  equ     46
	global	_RA7
_RA7  equ     47
	global	_RB0
_RB0  equ     48
	global	_RB1
_RB1  equ     49
	global	_RB2
_RB2  equ     50
	global	_RB3
_RB3  equ     51
	global	_RB4
_RB4  equ     52
	global	_RB5
_RB5  equ     53
	global	_RB6
_RB6  equ     54
	global	_RB7
_RB7  equ     55
	global	_RBIE
_RBIE  equ     91
	global	_RBIF
_RBIF  equ     88
	global	_RCIF
_RCIF  equ     101
	global	_RP0
_RP0  equ     29
	global	_RP1
_RP1  equ     30
	global	_RX9
_RX9  equ     198
	global	_RX9D
_RX9D  equ     192
	global	_SPEN
_SPEN  equ     199
	global	_SREN
_SREN  equ     197
	global	_T0IE
_T0IE  equ     93
	global	_T0IF
_T0IF  equ     90
	global	_T1CKPS0
_T1CKPS0  equ     132
	global	_T1CKPS1
_T1CKPS1  equ     133
	global	_T1OSCEN
_T1OSCEN  equ     131
	global	_T1SYNC
_T1SYNC  equ     130
	global	_T2CKPS0
_T2CKPS0  equ     144
	global	_T2CKPS1
_T2CKPS1  equ     145
	global	_TMR1CS
_TMR1CS  equ     129
	global	_TMR1IF
_TMR1IF  equ     96
	global	_TMR1ON
_TMR1ON  equ     128
	global	_TMR2IF
_TMR2IF  equ     97
	global	_TMR2ON
_TMR2ON  equ     146
	global	_TO
_TO  equ     28
	global	_TOUTPS0
_TOUTPS0  equ     147
	global	_TOUTPS1
_TOUTPS1  equ     148
	global	_TOUTPS2
_TOUTPS2  equ     149
	global	_TOUTPS3
_TOUTPS3  equ     150
	global	_TXIF
_TXIF  equ     100
	global	_ZERO
_ZERO  equ     26
	global	_EEADR
_EEADR  equ     155
	global	_EECON1
_EECON1  equ     156
	global	_EECON2
_EECON2  equ     157
	global	_EEDATA
_EEDATA  equ     154
	global	_OPTION
_OPTION  equ     129
	global	_PCON
_PCON  equ     142
	global	_PIE1
_PIE1  equ     140
	global	_PR2
_PR2  equ     146
	global	_SPBRG
_SPBRG  equ     153
	global	_TRISA
_TRISA  equ     133
	global	_TRISB
_TRISB  equ     134
	global	_TXSTA
_TXSTA  equ     152
	global	_VRCON
_VRCON  equ     159
	global	_BOR
_BOR  equ     1136
	global	_BRGH
_BRGH  equ     1218
	global	_CCP1IE
_CCP1IE  equ     1122
	global	_CMIE
_CMIE  equ     1126
	global	_CSRC
_CSRC  equ     1223
	global	_EEIE
_EEIE  equ     1127
	global	_INTEDG
_INTEDG  equ     1038
	global	_OSCF
_OSCF  equ     1139
	global	_POR
_POR  equ     1137
	global	_PS0
_PS0  equ     1032
	global	_PS1
_PS1  equ     1033
	global	_PS2
_PS2  equ     1034
	global	_PSA
_PSA  equ     1035
	global	_RBPU
_RBPU  equ     1039
	global	_RCIE
_RCIE  equ     1125
	global	_RD
_RD  equ     1248
	global	_SYNC
_SYNC  equ     1220
	global	_T0CS
_T0CS  equ     1037
	global	_T0SE
_T0SE  equ     1036
	global	_TMR1IE
_TMR1IE  equ     1120
	global	_TMR2IE
_TMR2IE  equ     1121
	global	_TRISA0
_TRISA0  equ     1064
	global	_TRISA1
_TRISA1  equ     1065
	global	_TRISA2
_TRISA2  equ     1066
	global	_TRISA3
_TRISA3  equ     1067
	global	_TRISA4
_TRISA4  equ     1068
	global	_TRISA5
_TRISA5  equ     1069
	global	_TRISA6
_TRISA6  equ     1070
	global	_TRISA7
_TRISA7  equ     1071
	global	_TRISB0
_TRISB0  equ     1072
	global	_TRISB1
_TRISB1  equ     1073
	global	_TRISB2
_TRISB2  equ     1074
	global	_TRISB3
_TRISB3  equ     1075
	global	_TRISB4
_TRISB4  equ     1076
	global	_TRISB5
_TRISB5  equ     1077
	global	_TRISB6
_TRISB6  equ     1078
	global	_TRISB7
_TRISB7  equ     1079
	global	_TRMT
_TRMT  equ     1217
	global	_TX9
_TX9  equ     1222
	global	_TX9D
_TX9D  equ     1216
	global	_TXEN
_TXEN  equ     1221
	global	_TXIE
_TXIE  equ     1124
	global	_VR0
_VR0  equ     1272
	global	_VR1
_VR1  equ     1273
	global	_VR2
_VR2  equ     1274
	global	_VR3
_VR3  equ     1275
	global	_VREN
_VREN  equ     1279
	global	_VROE
_VROE  equ     1278
	global	_VRR
_VRR  equ     1277
	global	_WR
_WR  equ     1249
	global	_WREN
_WREN  equ     1250
	global	_WRERR
_WRERR  equ     1251
	file	"ccpcompare.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initationation code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?__delay
?__delay: ;@ 0x0
	global	??__delay
??__delay: ;@ 0x0
	global	__delay$0
__delay$0:	; 4 bytes @ 0x0
	ds	4
	global	??_main
??_main: ;@ 0x4
	ds	2
	global	??_isr
??_isr: ;@ 0x6
	global	?_main
?_main: ;@ 0x6
	ds	2
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	?_isr
?_isr: ;@ 0x0
;Data sizes: Strings 0, constant 0, data 0, bss 0, persistent 0 stack 0
;Auto spaces:   Size  Autos    Used
; COMMON          14      8       8
; BANK0           80      0       0
; BANK1           80      0       0
; BANK2           48      0       0


;Pointer list with targets:



;Main: autosize = 0, tempsize = 2, incstack = 0, save=0


;Call graph:                      Base Space Used Autos Args Refs Density
;_main                                                2    0    0   0.00
;                                    4 COMMO    2
;             __delay
;  __delay                                            0    4    0   0.00
;                                    0 COMMO    4
; Estimated maximum call depth 1
;_isr                                                 2    0    0   0.00
;                                    6 COMMO    2
; Estimated maximum call depth 0
; Address spaces:

;Name               Size   Autos  Total    Cost      Usage
;BITCOMMON            E      0       0       0        0.0%
;NULL                 0      0       0       0        0.0%
;CODE                 0      0       0       0        0.0%
;COMMON               E      8       8       1       57.1%
;BITSFR0              0      0       0       1        0.0%
;SFR0                 0      0       0       1        0.0%
;BITSFR1              0      0       0       2        0.0%
;SFR1                 0      0       0       2        0.0%
;STACK                0      0       0       2        0.0%
;BANK0               50      0       0       3        0.0%
;BANK1               50      0       0       4        0.0%
;BITSFR3              0      0       0       4        0.0%
;SFR3                 0      0       0       4        0.0%
;BANK2               30      0       0       5        0.0%
;SFR2                 0      0       0       5        0.0%
;BITSFR2              0      0       0       5        0.0%
;ABS                  0      0       8       6        0.0%
;BITBANK0            50      0       0       7        0.0%
;BITBANK1            50      0       0       8        0.0%
;BITBANK2            30      0       0       9        0.0%
;DATA                 0      0       8      10        0.0%
;EEDATA              80      0       0    1000        0.0%

	global	_main
psect	maintext,local,class=CODE,delta=2
global __pmaintext
__pmaintext:

; *************** function _main *****************
; Defined at:
;		line 6 in file "X:\Hi-tech\CCP_Compare\ccp_compare.c"
; Parameters:    Size  Location     Type
;		None
; Auto vars:     Size  Location     Type
;		None
; Return value:  Size  Location     Type
;		None               void
; Registers used:
;		wreg, status,2
; Tracked objects:
;		On entry : 17F/0
;		On exit  : 60/20
;		Unchanged: 0/0
; Data sizes:     COMMON   BANK0   BANK1   BANK2
;      Locals:         2       0       0       0
;      Temp:     2
;      Total:    2
; This function calls:
;		__delay
; This function is called by:
;		Startup code after reset
; This function uses a non-reentrant model
; 
psect	maintext
	file	"X:\Hi-tech\CCP_Compare\ccp_compare.c"
	line	6
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
;ccp_compare.c: 4: asm("\tpsect config,class=CONFIG,delta=2"); asm("\tdw ""0x3FFB & 0x3FFF & 0x3FFF & 0x3F7F");
;ccp_compare.c: 6: void main() {
	
_main:	
	opt stack 7
; Regs used in _main: [wreg+status,2]
	line	8
	
l30000251:	
;ccp_compare.c: 7: unsigned char x;
;ccp_compare.c: 8: _delay((unsigned long)((100)*(4000000/4000.0)));
	movlw	130
movwf	(??_main+0+0+1),f
	movlw	221
movwf	(??_main+0+0),f
u37:
	decfsz	(??_main+0+0),f
	goto	u37
	decfsz	(??_main+0+0+1),f
	goto	u37
	nop2

	
l30000252:	
	line	9
;ccp_compare.c: 9: TRISB = 0x00;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	clrf	(134)^080h	;volatile
	
l30000253:	
	line	10
;ccp_compare.c: 10: PORTB = 0x00;
	bcf	status, 5	;RP0=0, select bank0
	clrf	(6)	;volatile
	line	12
;ccp_compare.c: 12: CCPR1H = 0b00000011; CCPR1L = 0b11101000;
	movlw	(03h)
	movwf	(22)	;volatile
	movlw	(0E8h)
	movwf	(21)	;volatile
	
l30000254:	
	line	14
;ccp_compare.c: 14: CCP1M3 = 1;
	bsf	(187/8),(187)&7
	
l30000255:	
	line	15
;ccp_compare.c: 15: CCP1M2 = 0;
	bcf	(186/8),(186)&7
	
l30000256:	
	line	16
;ccp_compare.c: 16: CCP1M1 = 1;
	bsf	(185/8),(185)&7
	
l30000257:	
	line	17
;ccp_compare.c: 17: CCP1M0 = 0;
	bcf	(184/8),(184)&7
	
l30000258:	
	line	19
;ccp_compare.c: 19: T1CKPS1 = 0;
	bcf	(133/8),(133)&7
	
l30000259:	
	line	20
;ccp_compare.c: 20: T1CKPS0 = 0;
	bcf	(132/8),(132)&7
	
l30000260:	
	line	22
;ccp_compare.c: 22: T1OSCEN = 0;
	bcf	(131/8),(131)&7
	
l30000261:	
	line	23
;ccp_compare.c: 23: TMR1CS = 0;
	bcf	(129/8),(129)&7
	
l30000262:	
	line	24
;ccp_compare.c: 24: TMR1ON = 1;
	bsf	(128/8),(128)&7
	
l30000263:	
	line	25
;ccp_compare.c: 25: T1SYNC = 0;
	bcf	(130/8),(130)&7
	
l30000264:	
	line	26
;ccp_compare.c: 26: GIE = 1;
	bsf	(95/8),(95)&7
	
l30000265:	
	line	27
;ccp_compare.c: 27: PEIE = 1;
	bsf	(94/8),(94)&7
	
l30000266:	
	line	28
;ccp_compare.c: 28: CCP1IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bsf	(1122/8)^080h,(1122)&7
	
l2:	
	goto	l2
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
; =============== function _main ends ============

psect	maintext
	line	34
	signat	_main,88
	global	_isr
psect	text11,local,class=CODE,delta=2
global __ptext11
__ptext11:

; *************** function _isr *****************
; Defined at:
;		line 36 in file "X:\Hi-tech\CCP_Compare\ccp_compare.c"
; Parameters:    Size  Location     Type
;		None
; Auto vars:     Size  Location     Type
;		None
; Return value:  Size  Location     Type
;		None               void
; Registers used:
;		wreg, status,2
; Tracked objects:
;		On entry : 0/0
;		On exit  : 60/0
;		Unchanged: FFF9F/0
; Data sizes:     COMMON   BANK0   BANK1   BANK2
;      Locals:         2       0       0       0
;      Temp:     2
;      Total:    2
; This function calls:
;		Nothing
; This function is called by:
;		Interrupt level 1
; This function uses a non-reentrant model
; 
psect	text11
	file	"X:\Hi-tech\CCP_Compare\ccp_compare.c"
	line	36
	global	__size_of_isr
	__size_of_isr	equ	__end_of_isr-_isr
;ccp_compare.c: 36: void interrupt isr() {
	
_isr:	
	opt stack 7
; Regs used in _isr: [wreg+status,2]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+1
	movwf	saved_w
	movf	status,w
	movwf	(??_isr+0)
	movf	pclath,w
	movwf	(??_isr+1)
	ljmp	_isr
psect	text11
	line	37
	
i1l30000272:	
;ccp_compare.c: 37: if (CCP1IF) {
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(98/8),(98)&7
	goto	u2_21
	goto	u2_20
u2_21:
	goto	i1l5
u2_20:
	
i1l30000273:	
	line	38
;ccp_compare.c: 38: RB3 = !RB3;
	movlw	1<<((51)&7)
	xorwf	((51)/8),f
	
i1l30000274:	
	line	39
;ccp_compare.c: 39: TMR1H = 0x00; TMR1L = 0x00;
	clrf	(15)	;volatile
	
i1l30000275:	
	clrf	(14)	;volatile
	
i1l30000276:	
	line	40
;ccp_compare.c: 40: CCP1IF = 0;
	bcf	(98/8),(98)&7
	
i1l5:	
	movf	(??_isr+1),w
	movwf	pclath
	movf	(??_isr+0),w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_isr
	__end_of_isr:
; =============== function _isr ends ============

psect	text12,local,class=CODE,delta=2
global __ptext12
__ptext12:
	line	43
	signat	_isr,88
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	end
