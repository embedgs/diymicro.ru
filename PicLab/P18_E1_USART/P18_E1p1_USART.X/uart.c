#include <xc.h>

void InitUart()  //Initialization of UART 
{
   //PIC18F1230
    SPEN = 1;     //Serial Port enabled
    TRISA3 = 1;
    TRISA2 = 1;
    
    /*------BRG to 8 bit async -----------*/
    SYNC = 0;
    BRG16 = 0;
    BRGH = 0;
    /*------end of BRG -------------------*/

    /*-------BAUD rate coefficients-------
    BRG 0 0 0
    4M
    2.4K - 25
    9.6K - 6
    19.2 - 2
    57.6 - 0

    8M
    2.4K - 51
    9.6K - 12
    
    10M
    2.4K - 64
    9.6K - 15
    19.2K - 7
    --------------------------------------*/

    SPBRG = 12; //9615 baudrate with 8MHz
    


    TX9 = 0; //no need to use 9th bit
    TXEN = 1;

    //Reciever part
    RCIE = 1; //RX interrupts enable
    RX9 = 0;
    CREN = 1;

}//InitUart

char BusyUSART(void)
{
  if(!TXSTAbits.TRMT)  // Is the transmit shift register empty
    return 1;          // No, return FALSE
  return 0;            // Return TRUE
}


void writeByteUart(char tx_data)  //Writing of a single byte
{
  TXREG = tx_data;      // Write the data byte to the USART
}//writeByteUart

void writeDataUart( char *data)
{
  do
  {  // Transmit a byte
    while(BusyUSART());
    writeByteUart(*data);
  } while( *data++ );
}

void NumToUart(unsigned int Num)   //Number to uart procedure, max 5 digits
{

  unsigned int bignum = 10000;
  unsigned char numtemp = 5;

  if (!Num)
  {
      writeByteUart('0');         //If num is equal to 0, no need to do anything
      while(BusyUSART());         //wait till the buffer will be free
  }
  else 
  {
	  while(numtemp>0)             //determine how many digits in the number
	  {
	    if (Num/bignum)
	        break;
	    numtemp--;
	    bignum = bignum / 10;  
	  }  
	
	
	
	  for (unsigned char i = numtemp; i>0; i--)
	    {
	      writeByteUart( (Num - (Num/(bignum*10))*bignum*10 )/bignum + '0');         //pushing digits one by one from left to right
	      while(BusyUSART());                                                       //wait till buffer will be free
	      bignum = bignum/10;
	    }
   } 
}


char ReadUart()
{
    char data;   // Holds received data
    data = RCREG;                      // Read data

  return (data);                     // Return the received data

}