#include <stdio.h>
#include <htc.h>
#define _XTAL_FREQ 4000000
#include "usart.h"
#include "softi2c.h"

#define timeh1 RB4
#define timeh2 RB5
#define timem1 RB6
#define timem2 RB7


__CONFIG(LVPDIS & WDTDIS & MCLREN & UNPROTECT & HS);

	volatile unsigned char temphour = 0;
	volatile unsigned char tempmin = 0;

const unsigned char tempdigit[10]={
                               0b10110111,   //����
                               0b10000001,   //����
                               0b00111101,   //���
                               0b10101101,   //���
                               0b10001011,   //������
                               0b10101110,   //����
                               0b10111110,   //�����
                               0b10000101,   //����
                               0b10111111,   //������
                               0b10101111    //������
}; 


const unsigned char tempdigitvspoint[10]={
                               0b11110111,   //����
                               0b11000001,   //����
                               0b01111101,   //���
                               0b11101101,   //���
                               0b11001011,   //������
                               0b11101110,   //����
                               0b11111110,   //�����
                               0b11000101,   //����
                               0b11111111,   //������
                               0b11101111    //������
}; 


const unsigned char timedigit[10]={
                               0b11110101,   //����
                               0b00100100,   //����
                               0b10111001,   //���
                               0b10101101,   //���
                               0b01101100,   //������
                               0b11001101,   //����
                               0b11011101,  //�����
                               0b10100100,   //����
                               0b11111101,   //������
                               0b11101101    //������
}; 


const unsigned char timedigitvspoint[10]={
                               0b11110111,   //����
                               0b00100110,   //����
                               0b10111011,   //���
                               0b10101111,   //���
                               0b01101110,   //������
                               0b11001111,   //����
                               0b11011111,  //�����
                               0b10100110,   //����
                               0b11111111,   //������
                               0b11101111    //������
}; 



unsigned char BCDconv (unsigned char source);
unsigned char DCBconv (unsigned char source);
void SetMin(unsigned char minutes);
unsigned char ReadMin();
void SetHour(unsigned char hours);
unsigned char ReadHour();
void SetSeconds(unsigned char seconds);
unsigned char ReadSeconds();
void ShowTime();
void SetupTime();
void InitIO(void);
void Display(unsigned char time, unsigned char temperature);
void Displaypoint(unsigned char time, unsigned char temperature);
void DispAll(unsigned char hour, unsigned char min, unsigned char temperature);


void main(void){
	unsigned char input;

	CMCON = 0x07;
	TRISA = 0x00;
	TRISB4 = 0;
	TRISB5 = 0;
	TRISB6 = 0;
	TRISB7 = 0;
	INTCON=0;	// purpose of disabling the interrupts.
   	init_comms();	// set up the USART - settings defined in usart.h
    
	
    SetHour(15);
	SetMin(38);
	SetSeconds(23);
	
	ShowTime();
	InitIO();
	
	       T1CKPS1 = 1;
           T1CKPS0 = 1; // ������������ = 8
 
           T1OSCEN = 0; //��������� ���������� ���������
           TMR1CS = 0; // Fosc/4
           GIE = 0;   // ���������� ����������
           PEIE = 0;  // ���������� ���������
           TMR1IE = 1; // ���������� �� ������������ TMR1
           TMR1ON = 0; // �������� ������
 
           TMR1H = 0b11111110; TMR1L = 0x00;  //������������� ������� �� �������� 65024

	
   temphour = ReadHour();
   tempmin = ReadMin();
    
	
	
	for(;;){
	
	//DispAll(temphour, tempmin , 26);
	
	//__delay_ms(100);
 	
	input = getch();
	
	switch (input) {
                    case 49 : ShowTime();
					          break;
					case 50 : SetupTime();
                              break;					
    }	
	//if (input == 49) { 	SetupTime();}


	}
	

}


interrupt isr() {
 
if (TMR1IF) {
TMR1H = 0b11111110; TMR1L = 0x00;
   temphour = ReadHour();
   tempmin = ReadMin();
TMR1IF = 0;  //����� �����
}
 
}


unsigned char BCDconv (unsigned char source) {
  unsigned char temp_min=0;
  unsigned char temp_maj=0;
   temp_min = source&15;
   temp_maj = source >> 4;
   //printf("\r\n minor - %x", temp_min);
   //printf("\r\n major - %x", temp_maj);
   temp_maj *= 10;
   //printf("\r\n major - %x", temp_maj);
   
   return temp_maj+temp_min;
}

unsigned char DCBconv (unsigned char source) {
 unsigned char temp_min=0;
 unsigned char temp_maj=0;
 temp_maj = source/10 ;
 temp_min = source - temp_maj*10;
 temp_maj <<= 4;

 return temp_maj+temp_min;

}

void SetMin(unsigned char minutes) {        //���������� �������� � ������
 	i2c_start();            
    i2c_tx(0b11010000);
    i2c_tx(0x01);
    i2c_tx(DCBconv(minutes));
    i2c_stop();
}

unsigned char ReadMin() {
	unsigned char temp = 0;
	i2c_start();            //������ �������� �����
    i2c_tx(0b11010000);
	i2c_tx(0x01);
	i2c_start();
	i2c_tx(0b11010001);
	temp = i2c_rx(0);
	i2c_stop();

    return BCDconv(temp);
}

void SetHour(unsigned char hours) {        //���������� �������� � ����
 	i2c_start();            
    i2c_tx(0b11010000);
    i2c_tx(0x02);
    i2c_tx(DCBconv(hours));
    i2c_stop();
}

unsigned char ReadHour() {
	unsigned char temp = 0; 
	unsigned char temp2 = 0;
	i2c_start();            //������ �������� �����
    i2c_tx(0b11010000);
	i2c_tx(0x02);
	i2c_start();
	i2c_tx(0b11010001);
	temp = i2c_rx(0);
	i2c_stop();
    /*
	temp2 = temp&0b00001111;
	temp /= 10;
	temp = temp >> 4;
	temp = temp*10 + temp2;*/
	
	return BCDconv(temp);
}

void SetSeconds(unsigned char seconds) {        //���������� �������� � �������
 	i2c_start();            
    i2c_tx(0b11010000);
    i2c_tx(0x00);
    i2c_tx(DCBconv(seconds));
    i2c_stop();
}

unsigned char ReadSeconds() {
	unsigned char temp = 0;
	i2c_start();            //������ �������� �����
    i2c_tx(0b11010000);
	i2c_tx(0x00);
	i2c_start();
	i2c_tx(0b11010001);
	temp = i2c_rx(0);
	i2c_stop();

    return BCDconv(temp);
}

void ShowTime() {
    printf("\fTime - %d :", ReadHour());
    printf(" %d :", ReadMin());
	printf(" %d ", ReadSeconds());
	
	printf("\r\n*********************");
	
	printf("\r\n Menu");
	printf("\r\n 1 - Reload time");
	printf("\r\n 2 - Setup time");

}

void SetupTime() {
    unsigned char state = 1;
	unsigned char temp_min = 0;
	unsigned char temp_maj = 0;
	unsigned char temp = 0;
	
	while(state){
	     printf("\fEnter hours - ");
	     temp_maj = getch();
	     printf(" %c", temp_maj);
	     temp_min = getch();
	     printf("%c", temp_min);
	     temp = getch();
		 if (temp == 13) {
		                 temp_maj -= 48;
		                 temp_min -= 48;
	                     temp = temp_maj*10 + temp_min;
		                       if (temp < 24) {
						                      SetHour(temp);
		                                      state = 0;
											  }  										  
				   
						 }
    }
	state = 1;
	while(state){
	     printf("\fEnter minutes - ");
	     temp_maj = getch();
	     printf(" %c", temp_maj);
	     temp_min = getch();
	     printf("%c", temp_min);
	     temp = getch();
		 if (temp == 13) {
		                 temp_maj -= 48;
		                 temp_min -= 48;
	                     temp = temp_maj*10 + temp_min;
		                       if (temp < 61) {
						                      SetMin(temp);
		                                      state = 0;
											  }  						 
						 }
    }
	state = 1;
    while(state){
	     printf("\fEnter seconds - ");
	     temp_maj = getch();
	     printf(" %c", temp_maj);
	     temp_min = getch();
	     printf("%c", temp_min);
	     temp = getch();
		 if (temp == 13) {
		                 temp_maj -= 48;
		                 temp_min -= 48;
	                     temp = temp_maj*10 + temp_min;
		                       if (temp < 61) {
						                      SetSeconds(temp);
		                                      state = 0;
											  }  
						 }
    }

    ShowTime();

}

void InitIO(void) {
    i2c_start();
    i2c_tx(0b11101000);
    i2c_tx(0b00000110);
    i2c_tx(0x00);
    i2c_tx(0x00);
    i2c_stop();
}

void Display(unsigned char time, unsigned char temperature) {
    unsigned char timetemp = 0;
	unsigned char temptemp = 0;
	timetemp = time;
	temptemp = temperature;
	i2c_start();
    i2c_tx(0b11101000);
    i2c_tx(0b00000010);
    i2c_tx(timedigit[timetemp]);
    i2c_tx(tempdigit[temptemp]);
    i2c_stop();

}


void Displaypoint(unsigned char time, unsigned char temperature) {
    unsigned char timetemp = 0;
	unsigned char temptemp = 0;
	timetemp = time;
	temptemp = temperature;
	i2c_start();
    i2c_tx(0b11101000);
    i2c_tx(0b00000010);
    i2c_tx(timedigitvspoint[timetemp]);
    i2c_tx(tempdigitvspoint[temptemp]);
    i2c_stop();

}


void DispAll(unsigned char hour, unsigned char min, unsigned char temperature) {
     unsigned char tim = 0;
	 unsigned char tem = 0;
	 unsigned char buf = 0;
	 timeh1 = 0; timeh2 = 0; timem1 = 0; timem2 = 0;
	 tim = hour/10;
	 Display(tim, tem);
	 timeh1 = 1; timeh2 = 0; timem1 = 0; timem2 = 0;
	 __delay_ms(2);
	 timeh1 = 0; timeh2 = 0; timem1 = 0; timem2 = 0;
	 buf = tim*10;
	 tim = hour - buf;
	 Displaypoint(tim, tem);
	 timeh1 = 0; timeh2 = 1; timem1 = 0; timem2 = 0;
	 __delay_ms(2);

	 
	 timeh1 = 0; timeh2 = 0; timem1 = 0; timem2 = 0;
	 tim = min/10;
	 Display(tim, tem);
	 timeh1 = 0; timeh2 = 0; timem1 = 1; timem2 = 0;
	 __delay_ms(2);
	 timeh1 = 0; timeh2 = 0; timem1 = 0; timem2 = 0;
	 buf = tim*10;
	 tim = min-buf;
	 Display(tim, tem);
	 timeh1 = 0; timeh2 = 0; timem1 = 0; timem2 = 1;
	 __delay_ms(2);


}





