opt subtitle "HI-TECH Software Omniscient Code Generator (PRO mode) build 6446"

opt pagewidth 120

	opt pm

	processor	16F628A
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 13 "X:\Hi-tech\RTC_clock\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 13 "X:\Hi-tech\RTC_clock\main.c"
	dw 0x3F7F & 0x3FFB & 0x3FFF & 0x3FFF & 0x3FEE ;#
	FNCALL	_main,_SetHour
	FNCALL	_main,_SetMin
	FNCALL	_main,_SetSeconds
	FNCALL	_main,_ShowTime
	FNCALL	_main,_InitIO
	FNCALL	_main,_ReadHour
	FNCALL	_main,_ReadMin
	FNCALL	_main,_getch
	FNCALL	_main,_SetupTime
	FNCALL	_SetupTime,_printf
	FNCALL	_SetupTime,_getch
	FNCALL	_SetupTime,___bmul
	FNCALL	_SetupTime,_SetHour
	FNCALL	_SetupTime,_SetMin
	FNCALL	_SetupTime,_SetSeconds
	FNCALL	_SetupTime,_ShowTime
	FNCALL	_ShowTime,_ReadHour
	FNCALL	_ShowTime,_printf
	FNCALL	_ShowTime,_ReadMin
	FNCALL	_ShowTime,_ReadSeconds
	FNCALL	_ReadSeconds,_i2c_start
	FNCALL	_ReadSeconds,_i2c_tx
	FNCALL	_ReadSeconds,_i2c_rx
	FNCALL	_ReadSeconds,_i2c_stop
	FNCALL	_ReadSeconds,_BCDconv
	FNCALL	_ReadMin,_i2c_start
	FNCALL	_ReadMin,_i2c_tx
	FNCALL	_ReadMin,_i2c_rx
	FNCALL	_ReadMin,_i2c_stop
	FNCALL	_ReadMin,_BCDconv
	FNCALL	_ReadHour,_i2c_start
	FNCALL	_ReadHour,_i2c_tx
	FNCALL	_ReadHour,_i2c_rx
	FNCALL	_ReadHour,_i2c_stop
	FNCALL	_ReadHour,_BCDconv
	FNCALL	_InitIO,_i2c_start
	FNCALL	_InitIO,_i2c_tx
	FNCALL	_InitIO,_i2c_stop
	FNCALL	_SetSeconds,_i2c_start
	FNCALL	_SetSeconds,_i2c_tx
	FNCALL	_SetSeconds,_DCBconv
	FNCALL	_SetSeconds,_i2c_stop
	FNCALL	_SetMin,_i2c_start
	FNCALL	_SetMin,_i2c_tx
	FNCALL	_SetMin,_DCBconv
	FNCALL	_SetMin,_i2c_stop
	FNCALL	_SetHour,_i2c_start
	FNCALL	_SetHour,_i2c_tx
	FNCALL	_SetHour,_DCBconv
	FNCALL	_SetHour,_i2c_stop
	FNCALL	_i2c_rx,_i2c_delay
	FNCALL	_i2c_stop,_i2c_delay
	FNCALL	_i2c_tx,_i2c_delay
	FNCALL	_i2c_start,_i2c_delay
	FNCALL	_printf,_putch
	FNCALL	_printf,___lwdiv
	FNCALL	_printf,___lwmod
	FNCALL	_DCBconv,___lbdiv
	FNCALL	_DCBconv,___bmul
	FNROOT	_main
	FNCALL	_isr,i1_ReadHour
	FNCALL	_isr,i1_ReadMin
	FNCALL	i1_ReadHour,i1_i2c_start
	FNCALL	i1_ReadHour,i1_i2c_tx
	FNCALL	i1_ReadHour,i1_i2c_rx
	FNCALL	i1_ReadHour,i1_i2c_stop
	FNCALL	i1_ReadHour,i1_BCDconv
	FNCALL	i1_ReadMin,i1_i2c_start
	FNCALL	i1_ReadMin,i1_i2c_tx
	FNCALL	i1_ReadMin,i1_i2c_rx
	FNCALL	i1_ReadMin,i1_i2c_stop
	FNCALL	i1_ReadMin,i1_BCDconv
	FNCALL	i1_i2c_rx,i1_i2c_delay
	FNCALL	i1_i2c_tx,i1_i2c_delay
	FNCALL	i1_i2c_stop,i1_i2c_delay
	FNCALL	i1_i2c_start,i1_i2c_delay
	FNCALL	intlevel1,_isr
	global	intlevel1
	FNROOT	intlevel1
	global	_dpowers
psect	strings,class=STRING,delta=2
global __pstrings
__pstrings:
;	global	stringdir,stringtab,__stringbase
stringtab:
;	String table - string pointers are 1 byte each
stringcode:stringdir:
movlw high(stringdir)
movwf pclath
movf fsr,w
incf fsr
	addwf pc
__stringbase:
	retlw	0
psect	strings
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\lib\doprnt.c"
	line	350
_dpowers:
	retlw	01h
	retlw	0

	retlw	0Ah
	retlw	0

	retlw	064h
	retlw	0

	retlw	0E8h
	retlw	03h

	retlw	010h
	retlw	027h

	global	_dpowers
	global	_temphour
	global	_tempmin
	global	_CMCON
_CMCON	set	31
	global	_INTCON
_INTCON	set	11
	global	_RCREG
_RCREG	set	26
	global	_RCSTA
_RCSTA	set	24
	global	_TMR1H
_TMR1H	set	15
	global	_TMR1L
_TMR1L	set	14
	global	_TXREG
_TXREG	set	25
	global	_GIE
_GIE	set	95
	global	_PEIE
_PEIE	set	94
	global	_RA0
_RA0	set	40
	global	_RA1
_RA1	set	41
	global	_RCIF
_RCIF	set	101
	global	_T1CKPS0
_T1CKPS0	set	132
	global	_T1CKPS1
_T1CKPS1	set	133
	global	_T1OSCEN
_T1OSCEN	set	131
	global	_TMR1CS
_TMR1CS	set	129
	global	_TMR1IF
_TMR1IF	set	96
	global	_TMR1ON
_TMR1ON	set	128
	global	_TXIF
_TXIF	set	100
	global	_SPBRG
_SPBRG	set	153
	global	_TRISA
_TRISA	set	133
	global	_TXSTA
_TXSTA	set	152
	global	_TMR1IE
_TMR1IE	set	1120
	global	_TRISA0
_TRISA0	set	1064
	global	_TRISA1
_TRISA1	set	1065
	global	_TRISB1
_TRISB1	set	1073
	global	_TRISB2
_TRISB2	set	1074
	global	_TRISB4
_TRISB4	set	1076
	global	_TRISB5
_TRISB5	set	1077
	global	_TRISB6
_TRISB6	set	1078
	global	_TRISB7
_TRISB7	set	1079
	
STR_4:	
	retlw	13
	retlw	10
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	0
psect	strings
	
STR_6:	
	retlw	13
	retlw	10
	retlw	32	;' '
	retlw	49	;'1'
	retlw	32	;' '
	retlw	45	;'-'
	retlw	32	;' '
	retlw	82	;'R'
	retlw	101	;'e'
	retlw	108	;'l'
	retlw	111	;'o'
	retlw	97	;'a'
	retlw	100	;'d'
	retlw	32	;' '
	retlw	116	;'t'
	retlw	105	;'i'
	retlw	109	;'m'
	retlw	101	;'e'
	retlw	0
psect	strings
	
STR_14:	
	retlw	12
	retlw	69	;'E'
	retlw	110	;'n'
	retlw	116	;'t'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	32	;' '
	retlw	115	;'s'
	retlw	101	;'e'
	retlw	99	;'c'
	retlw	111	;'o'
	retlw	110	;'n'
	retlw	100	;'d'
	retlw	115	;'s'
	retlw	32	;' '
	retlw	45	;'-'
	retlw	32	;' '
	retlw	0
psect	strings
	
STR_11:	
	retlw	12
	retlw	69	;'E'
	retlw	110	;'n'
	retlw	116	;'t'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	32	;' '
	retlw	109	;'m'
	retlw	105	;'i'
	retlw	110	;'n'
	retlw	117	;'u'
	retlw	116	;'t'
	retlw	101	;'e'
	retlw	115	;'s'
	retlw	32	;' '
	retlw	45	;'-'
	retlw	32	;' '
	retlw	0
psect	strings
	
STR_7:	
	retlw	13
	retlw	10
	retlw	32	;' '
	retlw	50	;'2'
	retlw	32	;' '
	retlw	45	;'-'
	retlw	32	;' '
	retlw	83	;'S'
	retlw	101	;'e'
	retlw	116	;'t'
	retlw	117	;'u'
	retlw	112	;'p'
	retlw	32	;' '
	retlw	116	;'t'
	retlw	105	;'i'
	retlw	109	;'m'
	retlw	101	;'e'
	retlw	0
psect	strings
	
STR_8:	
	retlw	12
	retlw	69	;'E'
	retlw	110	;'n'
	retlw	116	;'t'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	32	;' '
	retlw	104	;'h'
	retlw	111	;'o'
	retlw	117	;'u'
	retlw	114	;'r'
	retlw	115	;'s'
	retlw	32	;' '
	retlw	45	;'-'
	retlw	32	;' '
	retlw	0
psect	strings
	
STR_1:	
	retlw	12
	retlw	84	;'T'
	retlw	105	;'i'
	retlw	109	;'m'
	retlw	101	;'e'
	retlw	32	;' '
	retlw	45	;'-'
	retlw	32	;' '
	retlw	37	;'%'
	retlw	100	;'d'
	retlw	32	;' '
	retlw	58	;':'
	retlw	0
psect	strings
	
STR_5:	
	retlw	13
	retlw	10
	retlw	32	;' '
	retlw	77	;'M'
	retlw	101	;'e'
	retlw	110	;'n'
	retlw	117	;'u'
	retlw	0
psect	strings
	
STR_3:	
	retlw	32	;' '
	retlw	37	;'%'
	retlw	100	;'d'
	retlw	32	;' '
	retlw	0
psect	strings
	
STR_9:	
	retlw	32	;' '
	retlw	37	;'%'
	retlw	99	;'c'
	retlw	0
psect	strings
STR_12	equ	STR_9+0
STR_15	equ	STR_9+0
STR_10	equ	STR_9+1
STR_13	equ	STR_9+1
STR_16	equ	STR_9+1
STR_2	equ	STR_1+7
	file	"rtcclock.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_temphour:
       ds      1

_tempmin:
       ds      1

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_SetHour
?_SetHour:	; 0 bytes @ 0x0
	global	?_SetMin
?_SetMin:	; 0 bytes @ 0x0
	global	?_SetSeconds
?_SetSeconds:	; 0 bytes @ 0x0
	global	?_ShowTime
?_ShowTime:	; 0 bytes @ 0x0
	global	?_InitIO
?_InitIO:	; 0 bytes @ 0x0
	global	?_SetupTime
?_SetupTime:	; 0 bytes @ 0x0
	global	?_i2c_start
?_i2c_start:	; 0 bytes @ 0x0
	global	?_i2c_tx
?_i2c_tx:	; 1 bit 
	global	?_i2c_stop
?_i2c_stop:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 0 bytes @ 0x0
	global	?_putch
?_putch:	; 0 bytes @ 0x0
	global	?_i2c_delay
?_i2c_delay:	; 0 bytes @ 0x0
	global	??i1_BCDconv
??i1_BCDconv:	; 0 bytes @ 0x0
	global	?i1_i2c_delay
?i1_i2c_delay:	; 0 bytes @ 0x0
	global	??i1_i2c_delay
??i1_i2c_delay:	; 0 bytes @ 0x0
	global	?i1_i2c_start
?i1_i2c_start:	; 0 bytes @ 0x0
	global	??i1_i2c_start
??i1_i2c_start:	; 0 bytes @ 0x0
	global	?i1_i2c_stop
?i1_i2c_stop:	; 0 bytes @ 0x0
	global	??i1_i2c_stop
??i1_i2c_stop:	; 0 bytes @ 0x0
	global	?i1_i2c_tx
?i1_i2c_tx:	; 1 bit 
	global	??i1_i2c_tx
??i1_i2c_tx:	; 0 bytes @ 0x0
	global	??i1_i2c_rx
??i1_i2c_rx:	; 0 bytes @ 0x0
	global	?_ReadHour
?_ReadHour:	; 1 bytes @ 0x0
	global	?_ReadMin
?_ReadMin:	; 1 bytes @ 0x0
	global	?_getch
?_getch:	; 1 bytes @ 0x0
	global	?_i2c_rx
?_i2c_rx:	; 1 bytes @ 0x0
	global	?_BCDconv
?_BCDconv:	; 1 bytes @ 0x0
	global	?_DCBconv
?_DCBconv:	; 1 bytes @ 0x0
	global	?_ReadSeconds
?_ReadSeconds:	; 1 bytes @ 0x0
	global	?i1_BCDconv
?i1_BCDconv:	; 1 bytes @ 0x0
	global	?i1_ReadMin
?i1_ReadMin:	; 1 bytes @ 0x0
	global	?i1_ReadHour
?i1_ReadHour:	; 1 bytes @ 0x0
	global	?i1_i2c_rx
?i1_i2c_rx:	; 1 bytes @ 0x0
	global	?_isr
?_isr:	; 2 bytes @ 0x0
	global	i1BCDconv@temp_min
i1BCDconv@temp_min:	; 1 bytes @ 0x0
	global	i1i2c_tx@d
i1i2c_tx@d:	; 1 bytes @ 0x0
	global	i1i2c_rx@ack
i1i2c_rx@ack:	; 1 bytes @ 0x0
	ds	1
	global	i1BCDconv@temp_maj
i1BCDconv@temp_maj:	; 1 bytes @ 0x1
	global	i1i2c_tx@x
i1i2c_tx@x:	; 1 bytes @ 0x1
	global	i1i2c_rx@d
i1i2c_rx@d:	; 1 bytes @ 0x1
	ds	1
	global	i1BCDconv@source
i1BCDconv@source:	; 1 bytes @ 0x2
	global	i1i2c_rx@x
i1i2c_rx@x:	; 1 bytes @ 0x2
	ds	1
	global	??i1_ReadMin
??i1_ReadMin:	; 0 bytes @ 0x3
	global	??i1_ReadHour
??i1_ReadHour:	; 0 bytes @ 0x3
	global	i1ReadMin@temp
i1ReadMin@temp:	; 1 bytes @ 0x3
	global	i1ReadHour@temp
i1ReadHour@temp:	; 1 bytes @ 0x3
	ds	1
	global	??_isr
??_isr:	; 0 bytes @ 0x4
	ds	2
	global	??_SetHour
??_SetHour:	; 0 bytes @ 0x6
	global	??_SetMin
??_SetMin:	; 0 bytes @ 0x6
	global	??_SetSeconds
??_SetSeconds:	; 0 bytes @ 0x6
	global	??_ShowTime
??_ShowTime:	; 0 bytes @ 0x6
	global	??_InitIO
??_InitIO:	; 0 bytes @ 0x6
	global	??_ReadHour
??_ReadHour:	; 0 bytes @ 0x6
	global	??_ReadMin
??_ReadMin:	; 0 bytes @ 0x6
	global	??_getch
??_getch:	; 0 bytes @ 0x6
	global	??_i2c_start
??_i2c_start:	; 0 bytes @ 0x6
	global	??_i2c_tx
??_i2c_tx:	; 0 bytes @ 0x6
	global	??_i2c_stop
??_i2c_stop:	; 0 bytes @ 0x6
	global	??_i2c_rx
??_i2c_rx:	; 0 bytes @ 0x6
	global	??_main
??_main:	; 0 bytes @ 0x6
	global	??_BCDconv
??_BCDconv:	; 0 bytes @ 0x6
	global	??_ReadSeconds
??_ReadSeconds:	; 0 bytes @ 0x6
	global	??_putch
??_putch:	; 0 bytes @ 0x6
	global	??_i2c_delay
??_i2c_delay:	; 0 bytes @ 0x6
	global	??___bmul
??___bmul:	; 0 bytes @ 0x6
	global	??___lwdiv
??___lwdiv:	; 0 bytes @ 0x6
	global	??___lwmod
??___lwmod:	; 0 bytes @ 0x6
	global	??___lbdiv
??___lbdiv:	; 0 bytes @ 0x6
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	?___bmul
?___bmul:	; 1 bytes @ 0x0
	global	?___lbdiv
?___lbdiv:	; 1 bytes @ 0x0
	global	?___lwdiv
?___lwdiv:	; 2 bytes @ 0x0
	global	BCDconv@temp_min
BCDconv@temp_min:	; 1 bytes @ 0x0
	global	putch@byte
putch@byte:	; 1 bytes @ 0x0
	global	___bmul@multiplicand
___bmul@multiplicand:	; 1 bytes @ 0x0
	global	___lbdiv@divisor
___lbdiv@divisor:	; 1 bytes @ 0x0
	global	___lwdiv@divisor
___lwdiv@divisor:	; 2 bytes @ 0x0
	ds	1
	global	BCDconv@temp_maj
BCDconv@temp_maj:	; 1 bytes @ 0x1
	global	___bmul@product
___bmul@product:	; 1 bytes @ 0x1
	global	___lbdiv@dividend
___lbdiv@dividend:	; 1 bytes @ 0x1
	ds	1
	global	BCDconv@source
BCDconv@source:	; 1 bytes @ 0x2
	global	___bmul@multiplier
___bmul@multiplier:	; 1 bytes @ 0x2
	global	___lbdiv@counter
___lbdiv@counter:	; 1 bytes @ 0x2
	global	___lwdiv@dividend
___lwdiv@dividend:	; 2 bytes @ 0x2
	ds	1
	global	___lbdiv@quotient
___lbdiv@quotient:	; 1 bytes @ 0x3
	ds	1
	global	??_DCBconv
??_DCBconv:	; 0 bytes @ 0x4
	global	___lwdiv@quotient
___lwdiv@quotient:	; 2 bytes @ 0x4
	ds	1
	global	DCBconv@temp_min
DCBconv@temp_min:	; 1 bytes @ 0x5
	ds	1
	global	DCBconv@source
DCBconv@source:	; 1 bytes @ 0x6
	global	___lwdiv@counter
___lwdiv@counter:	; 1 bytes @ 0x6
	ds	1
	global	?___lwmod
?___lwmod:	; 2 bytes @ 0x7
	global	DCBconv@temp_maj
DCBconv@temp_maj:	; 1 bytes @ 0x7
	global	___lwmod@divisor
___lwmod@divisor:	; 2 bytes @ 0x7
	ds	2
	global	___lwmod@dividend
___lwmod@dividend:	; 2 bytes @ 0x9
	ds	2
	global	___lwmod@counter
___lwmod@counter:	; 1 bytes @ 0xB
	ds	1
	global	?_printf
?_printf:	; 2 bytes @ 0xC
	ds	2
	global	??_printf
??_printf:	; 0 bytes @ 0xE
	ds	2
	global	printf@flag
printf@flag:	; 1 bytes @ 0x10
	ds	1
	global	printf@f
printf@f:	; 1 bytes @ 0x11
	ds	1
	global	printf@prec
printf@prec:	; 1 bytes @ 0x12
	ds	1
	global	printf@ap
printf@ap:	; 1 bytes @ 0x13
	ds	1
	global	printf@_val
printf@_val:	; 4 bytes @ 0x14
	ds	4
	global	printf@c
printf@c:	; 1 bytes @ 0x18
	ds	1
	global	i2c_tx@d
i2c_tx@d:	; 1 bytes @ 0x19
	global	i2c_rx@ack
i2c_rx@ack:	; 1 bytes @ 0x19
	ds	1
	global	i2c_tx@x
i2c_tx@x:	; 1 bytes @ 0x1A
	global	i2c_rx@d
i2c_rx@d:	; 1 bytes @ 0x1A
	ds	1
	global	SetMin@minutes
SetMin@minutes:	; 1 bytes @ 0x1B
	global	SetHour@hours
SetHour@hours:	; 1 bytes @ 0x1B
	global	SetSeconds@seconds
SetSeconds@seconds:	; 1 bytes @ 0x1B
	global	i2c_rx@x
i2c_rx@x:	; 1 bytes @ 0x1B
	ds	1
	global	ReadMin@temp
ReadMin@temp:	; 1 bytes @ 0x1C
	global	ReadHour@temp
ReadHour@temp:	; 1 bytes @ 0x1C
	global	ReadSeconds@temp
ReadSeconds@temp:	; 1 bytes @ 0x1C
	ds	1
	global	??_SetupTime
??_SetupTime:	; 0 bytes @ 0x1D
	ds	1
	global	SetupTime@state
SetupTime@state:	; 1 bytes @ 0x1E
	ds	1
	global	SetupTime@temp_min
SetupTime@temp_min:	; 1 bytes @ 0x1F
	ds	1
	global	SetupTime@temp_maj
SetupTime@temp_maj:	; 1 bytes @ 0x20
	ds	1
	global	SetupTime@temp
SetupTime@temp:	; 1 bytes @ 0x21
	ds	1
	global	main@input
main@input:	; 1 bytes @ 0x22
	ds	1
;;Data sizes: Strings 143, constant 10, data 0, bss 2, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      6       8
;; BANK0           80     35      35
;; BANK1           80      0       0
;; BANK2           48      0       0

;;
;; Pointer list with targets:

;; ?___lwdiv	unsigned int  size(1) Largest target is 0
;;
;; ?___lwmod	unsigned int  size(1) Largest target is 0
;;
;; printf@f	PTR const unsigned char  size(1) Largest target is 24
;;		 -> STR_16(CODE[3]), STR_15(CODE[4]), STR_14(CODE[18]), STR_13(CODE[3]), 
;;		 -> STR_12(CODE[4]), STR_11(CODE[18]), STR_10(CODE[3]), STR_9(CODE[4]), 
;;		 -> STR_8(CODE[16]), STR_7(CODE[18]), STR_6(CODE[19]), STR_5(CODE[8]), 
;;		 -> STR_4(CODE[24]), STR_3(CODE[5]), STR_2(CODE[6]), STR_1(CODE[13]), 
;;
;; ?_printf	int  size(1) Largest target is 0
;;
;; printf@ap	PTR void [1] size(1) Largest target is 2
;;		 -> ?_printf(BANK0[2]), 
;;
;; S674$_cp	PTR const unsigned char  size(1) Largest target is 0
;;
;; _val._str._cp	PTR const unsigned char  size(1) Largest target is 0
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   None.
;;
;; Critical Paths under _isr in COMMON
;;
;;   _isr->i1_ReadHour
;;   _isr->i1_ReadMin
;;   i1_ReadHour->i1_i2c_rx
;;   i1_ReadHour->i1_BCDconv
;;   i1_ReadMin->i1_i2c_rx
;;   i1_ReadMin->i1_BCDconv
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_SetupTime
;;   _ShowTime->_ReadHour
;;   _ShowTime->_ReadMin
;;   _ShowTime->_ReadSeconds
;;   _ReadSeconds->_i2c_rx
;;   _ReadMin->_i2c_rx
;;   _ReadHour->_i2c_rx
;;   _InitIO->_i2c_tx
;;   _SetSeconds->_i2c_tx
;;   _SetMin->_i2c_tx
;;   _SetHour->_i2c_tx
;;   _i2c_rx->_printf
;;   _i2c_stop->_printf
;;   _i2c_tx->_printf
;;   _i2c_start->_printf
;;   _printf->___lwmod
;;   _DCBconv->___lbdiv
;;   ___lwmod->___lwdiv
;;
;; Critical Paths under _isr in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _isr in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.
;;
;; Critical Paths under _isr in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 0, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 1     1      0   11705
;;                                             34 BANK0      1     1      0
;;                            _SetHour
;;                             _SetMin
;;                         _SetSeconds
;;                           _ShowTime
;;                             _InitIO
;;                           _ReadHour
;;                            _ReadMin
;;                              _getch
;;                          _SetupTime
;; ---------------------------------------------------------------------------------
;; (1) _SetupTime                                            5     5      0    6373
;;                                             29 BANK0      5     5      0
;;                             _printf
;;                              _getch
;;                             ___bmul
;;                            _SetHour
;;                             _SetMin
;;                         _SetSeconds
;;                           _ShowTime
;; ---------------------------------------------------------------------------------
;; (2) _ShowTime                                             0     0      0    2316
;;                           _ReadHour
;;                             _printf
;;                            _ReadMin
;;                        _ReadSeconds
;; ---------------------------------------------------------------------------------
;; (3) _ReadSeconds                                          1     1      0     420
;;                                             28 BANK0      1     1      0
;;                          _i2c_start
;;                             _i2c_tx
;;                             _i2c_rx
;;                           _i2c_stop
;;                            _BCDconv
;;                             _printf (ARG)
;; ---------------------------------------------------------------------------------
;; (3) _ReadMin                                              1     1      0     420
;;                                             28 BANK0      1     1      0
;;                          _i2c_start
;;                             _i2c_tx
;;                             _i2c_rx
;;                           _i2c_stop
;;                            _BCDconv
;;                             _printf (ARG)
;; ---------------------------------------------------------------------------------
;; (3) _ReadHour                                             2     2      0     420
;;                                             28 BANK0      1     1      0
;;                          _i2c_start
;;                             _i2c_tx
;;                             _i2c_rx
;;                           _i2c_stop
;;                            _BCDconv
;;                             _printf (ARG)
;; ---------------------------------------------------------------------------------
;; (1) _InitIO                                               0     0      0     102
;;                          _i2c_start
;;                             _i2c_tx
;;                           _i2c_stop
;; ---------------------------------------------------------------------------------
;; (2) _SetSeconds                                           1     1      0     680
;;                                             27 BANK0      1     1      0
;;                          _i2c_start
;;                             _i2c_tx
;;                            _DCBconv
;;                           _i2c_stop
;; ---------------------------------------------------------------------------------
;; (2) _SetMin                                               1     1      0     680
;;                                             27 BANK0      1     1      0
;;                          _i2c_start
;;                             _i2c_tx
;;                            _DCBconv
;;                           _i2c_stop
;; ---------------------------------------------------------------------------------
;; (2) _SetHour                                              1     1      0     680
;;                                             27 BANK0      1     1      0
;;                          _i2c_start
;;                             _i2c_tx
;;                            _DCBconv
;;                           _i2c_stop
;; ---------------------------------------------------------------------------------
;; (4) _i2c_rx                                               3     3      0     139
;;                                             25 BANK0      3     3      0
;;                          _i2c_delay
;;                             _printf (ARG)
;; ---------------------------------------------------------------------------------
;; (4) _i2c_stop                                             0     0      0       0
;;                          _i2c_delay
;;                             _printf (ARG)
;; ---------------------------------------------------------------------------------
;; (4) _i2c_tx                                               2     2      0     102
;;                                             25 BANK0      2     2      0
;;                          _i2c_delay
;;                             _printf (ARG)
;; ---------------------------------------------------------------------------------
;; (4) _i2c_start                                            0     0      0       0
;;                          _i2c_delay
;;                             _printf (ARG)
;; ---------------------------------------------------------------------------------
;; (5) _i2c_delay                                            0     0      0       0
;; ---------------------------------------------------------------------------------
;; (3) _printf                                              13    11      2    1056
;;                                             12 BANK0     13    11      2
;;                              _putch
;;                            ___lwdiv
;;                            ___lwmod
;;                          _i2c_delay (ARG)
;;                            _BCDconv (ARG)
;; ---------------------------------------------------------------------------------
;; (3) _DCBconv                                              4     4      0     547
;;                                              4 BANK0      4     4      0
;;                            ___lbdiv
;;                             ___bmul
;; ---------------------------------------------------------------------------------
;; (4) ___lbdiv                                              4     3      1     241
;;                                              0 BANK0      4     3      1
;; ---------------------------------------------------------------------------------
;; (4) ___lwmod                                              5     1      4     232
;;                                              7 BANK0      5     1      4
;;                            ___lwdiv (ARG)
;; ---------------------------------------------------------------------------------
;; (4) ___lwdiv                                              7     3      4     162
;;                                              0 BANK0      7     3      4
;; ---------------------------------------------------------------------------------
;; (4) ___bmul                                               3     2      1     136
;;                                              0 BANK0      3     2      1
;; ---------------------------------------------------------------------------------
;; (4) _putch                                                1     1      0      31
;;                                              0 BANK0      1     1      0
;; ---------------------------------------------------------------------------------
;; (4) _BCDconv                                              3     3      0     142
;;                                              0 BANK0      3     3      0
;; ---------------------------------------------------------------------------------
;; (2) _getch                                                0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 5
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (6) _isr                                                  2     2      0    1816
;;                                              4 COMMON     2     2      0
;;                         i1_ReadHour
;;                          i1_ReadMin
;; ---------------------------------------------------------------------------------
;; (7) i1_ReadHour                                           2     2      0     908
;;                                              3 COMMON     1     1      0
;;                        i1_i2c_start
;;                           i1_i2c_tx
;;                           i1_i2c_rx
;;                         i1_i2c_stop
;;                          i1_BCDconv
;; ---------------------------------------------------------------------------------
;; (7) i1_ReadMin                                            1     1      0     908
;;                                              3 COMMON     1     1      0
;;                        i1_i2c_start
;;                           i1_i2c_tx
;;                           i1_i2c_rx
;;                         i1_i2c_stop
;;                          i1_BCDconv
;; ---------------------------------------------------------------------------------
;; (8) i1_i2c_rx                                             3     3      0     302
;;                                              0 COMMON     3     3      0
;;                        i1_i2c_delay
;; ---------------------------------------------------------------------------------
;; (8) i1_i2c_tx                                             2     2      0     225
;;                                              0 COMMON     2     2      0
;;                        i1_i2c_delay
;; ---------------------------------------------------------------------------------
;; (8) i1_i2c_stop                                           0     0      0       0
;;                        i1_i2c_delay
;; ---------------------------------------------------------------------------------
;; (8) i1_i2c_start                                          0     0      0       0
;;                        i1_i2c_delay
;; ---------------------------------------------------------------------------------
;; (9) i1_i2c_delay                                          0     0      0       0
;; ---------------------------------------------------------------------------------
;; (8) i1_BCDconv                                            3     3      0     304
;;                                              0 COMMON     3     3      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 9
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _SetHour
;;     _i2c_start
;;       _i2c_delay
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;     _i2c_tx
;;       _i2c_delay
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;     _DCBconv
;;       ___lbdiv
;;       ___bmul
;;     _i2c_stop
;;       _i2c_delay
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;   _SetMin
;;     _i2c_start
;;       _i2c_delay
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;     _i2c_tx
;;       _i2c_delay
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;     _DCBconv
;;       ___lbdiv
;;       ___bmul
;;     _i2c_stop
;;       _i2c_delay
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;   _SetSeconds
;;     _i2c_start
;;       _i2c_delay
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;     _i2c_tx
;;       _i2c_delay
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;     _DCBconv
;;       ___lbdiv
;;       ___bmul
;;     _i2c_stop
;;       _i2c_delay
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;   _ShowTime
;;     _ReadHour
;;       _i2c_start
;;         _i2c_delay
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;       _i2c_tx
;;         _i2c_delay
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;       _i2c_rx
;;         _i2c_delay
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;       _i2c_stop
;;         _i2c_delay
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;       _BCDconv
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;     _printf
;;       _putch
;;       ___lwdiv
;;       ___lwmod
;;         ___lwdiv (ARG)
;;       _i2c_delay (ARG)
;;       _BCDconv (ARG)
;;     _ReadMin
;;       _i2c_start
;;         _i2c_delay
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;       _i2c_tx
;;         _i2c_delay
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;       _i2c_rx
;;         _i2c_delay
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;       _i2c_stop
;;         _i2c_delay
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;       _BCDconv
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;     _ReadSeconds
;;       _i2c_start
;;         _i2c_delay
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;       _i2c_tx
;;         _i2c_delay
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;       _i2c_rx
;;         _i2c_delay
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;       _i2c_stop
;;         _i2c_delay
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;       _BCDconv
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;   _InitIO
;;     _i2c_start
;;       _i2c_delay
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;     _i2c_tx
;;       _i2c_delay
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;     _i2c_stop
;;       _i2c_delay
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;   _ReadHour
;;     _i2c_start
;;       _i2c_delay
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;     _i2c_tx
;;       _i2c_delay
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;     _i2c_rx
;;       _i2c_delay
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;     _i2c_stop
;;       _i2c_delay
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;     _BCDconv
;;     _printf (ARG)
;;       _putch
;;       ___lwdiv
;;       ___lwmod
;;         ___lwdiv (ARG)
;;       _i2c_delay (ARG)
;;       _BCDconv (ARG)
;;   _ReadMin
;;     _i2c_start
;;       _i2c_delay
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;     _i2c_tx
;;       _i2c_delay
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;     _i2c_rx
;;       _i2c_delay
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;     _i2c_stop
;;       _i2c_delay
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;     _BCDconv
;;     _printf (ARG)
;;       _putch
;;       ___lwdiv
;;       ___lwmod
;;         ___lwdiv (ARG)
;;       _i2c_delay (ARG)
;;       _BCDconv (ARG)
;;   _getch
;;   _SetupTime
;;     _printf
;;       _putch
;;       ___lwdiv
;;       ___lwmod
;;         ___lwdiv (ARG)
;;       _i2c_delay (ARG)
;;       _BCDconv (ARG)
;;     _getch
;;     ___bmul
;;     _SetHour
;;       _i2c_start
;;         _i2c_delay
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;       _i2c_tx
;;         _i2c_delay
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;       _DCBconv
;;         ___lbdiv
;;         ___bmul
;;       _i2c_stop
;;         _i2c_delay
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;     _SetMin
;;       _i2c_start
;;         _i2c_delay
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;       _i2c_tx
;;         _i2c_delay
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;       _DCBconv
;;         ___lbdiv
;;         ___bmul
;;       _i2c_stop
;;         _i2c_delay
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;     _SetSeconds
;;       _i2c_start
;;         _i2c_delay
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;       _i2c_tx
;;         _i2c_delay
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;       _DCBconv
;;         ___lbdiv
;;         ___bmul
;;       _i2c_stop
;;         _i2c_delay
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;     _ShowTime
;;       _ReadHour
;;         _i2c_start
;;           _i2c_delay
;;           _printf (ARG)
;;             _putch
;;             ___lwdiv
;;             ___lwmod
;;               ___lwdiv (ARG)
;;             _i2c_delay (ARG)
;;             _BCDconv (ARG)
;;         _i2c_tx
;;           _i2c_delay
;;           _printf (ARG)
;;             _putch
;;             ___lwdiv
;;             ___lwmod
;;               ___lwdiv (ARG)
;;             _i2c_delay (ARG)
;;             _BCDconv (ARG)
;;         _i2c_rx
;;           _i2c_delay
;;           _printf (ARG)
;;             _putch
;;             ___lwdiv
;;             ___lwmod
;;               ___lwdiv (ARG)
;;             _i2c_delay (ARG)
;;             _BCDconv (ARG)
;;         _i2c_stop
;;           _i2c_delay
;;           _printf (ARG)
;;             _putch
;;             ___lwdiv
;;             ___lwmod
;;               ___lwdiv (ARG)
;;             _i2c_delay (ARG)
;;             _BCDconv (ARG)
;;         _BCDconv
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;       _printf
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _i2c_delay (ARG)
;;         _BCDconv (ARG)
;;       _ReadMin
;;         _i2c_start
;;           _i2c_delay
;;           _printf (ARG)
;;             _putch
;;             ___lwdiv
;;             ___lwmod
;;               ___lwdiv (ARG)
;;             _i2c_delay (ARG)
;;             _BCDconv (ARG)
;;         _i2c_tx
;;           _i2c_delay
;;           _printf (ARG)
;;             _putch
;;             ___lwdiv
;;             ___lwmod
;;               ___lwdiv (ARG)
;;             _i2c_delay (ARG)
;;             _BCDconv (ARG)
;;         _i2c_rx
;;           _i2c_delay
;;           _printf (ARG)
;;             _putch
;;             ___lwdiv
;;             ___lwmod
;;               ___lwdiv (ARG)
;;             _i2c_delay (ARG)
;;             _BCDconv (ARG)
;;         _i2c_stop
;;           _i2c_delay
;;           _printf (ARG)
;;             _putch
;;             ___lwdiv
;;             ___lwmod
;;               ___lwdiv (ARG)
;;             _i2c_delay (ARG)
;;             _BCDconv (ARG)
;;         _BCDconv
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;       _ReadSeconds
;;         _i2c_start
;;           _i2c_delay
;;           _printf (ARG)
;;             _putch
;;             ___lwdiv
;;             ___lwmod
;;               ___lwdiv (ARG)
;;             _i2c_delay (ARG)
;;             _BCDconv (ARG)
;;         _i2c_tx
;;           _i2c_delay
;;           _printf (ARG)
;;             _putch
;;             ___lwdiv
;;             ___lwmod
;;               ___lwdiv (ARG)
;;             _i2c_delay (ARG)
;;             _BCDconv (ARG)
;;         _i2c_rx
;;           _i2c_delay
;;           _printf (ARG)
;;             _putch
;;             ___lwdiv
;;             ___lwmod
;;               ___lwdiv (ARG)
;;             _i2c_delay (ARG)
;;             _BCDconv (ARG)
;;         _i2c_stop
;;           _i2c_delay
;;           _printf (ARG)
;;             _putch
;;             ___lwdiv
;;             ___lwmod
;;               ___lwdiv (ARG)
;;             _i2c_delay (ARG)
;;             _BCDconv (ARG)
;;         _BCDconv
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _i2c_delay (ARG)
;;           _BCDconv (ARG)
;;
;; _isr (ROOT)
;;   i1_ReadHour
;;     i1_i2c_start
;;       i1_i2c_delay
;;     i1_i2c_tx
;;       i1_i2c_delay
;;     i1_i2c_rx
;;       i1_i2c_delay
;;     i1_i2c_stop
;;       i1_i2c_delay
;;     i1_BCDconv
;;   i1_ReadMin
;;     i1_i2c_start
;;       i1_i2c_delay
;;     i1_i2c_tx
;;       i1_i2c_delay
;;     i1_i2c_rx
;;       i1_i2c_delay
;;     i1_i2c_stop
;;       i1_i2c_delay
;;     i1_BCDconv
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA              80      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      6       8       1       57.1%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       D       2        0.0%
;;BANK0               50     23      23       3       43.8%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;ABS                  0      0      2B       4        0.0%
;;BITBANK0            50      0       0       5        0.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK2            30      0       0       8        0.0%
;;BANK2               30      0       0       9        0.0%
;;DATA                 0      0      38      10        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 91 in file "X:\Hi-tech\RTC_clock\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  input           1   34[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 60/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       1       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       1       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels required when called:    9
;; This function calls:
;;		_SetHour
;;		_SetMin
;;		_SetSeconds
;;		_ShowTime
;;		_InitIO
;;		_ReadHour
;;		_ReadMin
;;		_getch
;;		_SetupTime
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"X:\Hi-tech\RTC_clock\main.c"
	line	91
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 8
; Regs used in _main: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	94
	
l3635:	
;main.c: 92: unsigned char input;
;main.c: 94: CMCON = 0x07;
	movlw	(07h)
	movwf	(31)	;volatile
	line	95
	
l3637:	
;main.c: 95: TRISA = 0x00;
	bsf	status, 5	;RP0=1, select bank1
	clrf	(133)^080h	;volatile
	line	96
	
l3639:	
;main.c: 96: TRISB4 = 0;
	bcf	(1076/8)^080h,(1076)&7
	line	97
	
l3641:	
;main.c: 97: TRISB5 = 0;
	bcf	(1077/8)^080h,(1077)&7
	line	98
	
l3643:	
;main.c: 98: TRISB6 = 0;
	bcf	(1078/8)^080h,(1078)&7
	line	99
	
l3645:	
;main.c: 99: TRISB7 = 0;
	bcf	(1079/8)^080h,(1079)&7
	line	100
	
l3647:	
;main.c: 100: INTCON=0;
	clrf	(11)	;volatile
	line	101
	
l3649:	
;main.c: 101: TRISB1 = 1; TRISB2 = 1; SPBRG = ((int)(4000000L/(16UL * 9600) -1)); RCSTA = (0|0x90); TXSTA = (0x4|0|0x20);
	bsf	(1073/8)^080h,(1073)&7
	
l3651:	
	bsf	(1074/8)^080h,(1074)&7
	movlw	(019h)
	movwf	(153)^080h	;volatile
	movlw	(090h)
	bcf	status, 5	;RP0=0, select bank0
	movwf	(24)	;volatile
	movlw	(024h)
	bsf	status, 5	;RP0=1, select bank1
	movwf	(152)^080h	;volatile
	line	104
	
l3653:	
;main.c: 104: SetHour(15);
	movlw	(0Fh)
	fcall	_SetHour
	line	105
	
l3655:	
;main.c: 105: SetMin(38);
	movlw	(026h)
	fcall	_SetMin
	line	106
	
l3657:	
;main.c: 106: SetSeconds(23);
	movlw	(017h)
	fcall	_SetSeconds
	line	108
	
l3659:	
;main.c: 108: ShowTime();
	fcall	_ShowTime
	line	109
	
l3661:	
;main.c: 109: InitIO();
	fcall	_InitIO
	line	111
	
l3663:	
;main.c: 111: T1CKPS1 = 1;
	bcf	status, 5	;RP0=0, select bank0
	bsf	(133/8),(133)&7
	line	112
	
l3665:	
;main.c: 112: T1CKPS0 = 1;
	bsf	(132/8),(132)&7
	line	114
	
l3667:	
;main.c: 114: T1OSCEN = 0;
	bcf	(131/8),(131)&7
	line	115
	
l3669:	
;main.c: 115: TMR1CS = 0;
	bcf	(129/8),(129)&7
	line	116
	
l3671:	
;main.c: 116: GIE = 0;
	bcf	(95/8),(95)&7
	line	117
	
l3673:	
;main.c: 117: PEIE = 0;
	bcf	(94/8),(94)&7
	line	118
	
l3675:	
;main.c: 118: TMR1IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bsf	(1120/8)^080h,(1120)&7
	line	119
	
l3677:	
;main.c: 119: TMR1ON = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	(128/8),(128)&7
	line	121
	
l3679:	
;main.c: 121: TMR1H = 0b11111110; TMR1L = 0x00;
	movlw	(0FEh)
	movwf	(15)	;volatile
	
l3681:	
	clrf	(14)	;volatile
	line	124
	
l3683:	
;main.c: 124: temphour = ReadHour();
	fcall	_ReadHour
	movwf	(_temphour)	;volatile
	line	125
	
l3685:	
;main.c: 125: tempmin = ReadMin();
	fcall	_ReadMin
	movwf	(_tempmin)	;volatile
	line	135
	
l3687:	
;main.c: 135: input = getch();
	fcall	_getch
	movwf	(main@input)
	line	137
;main.c: 137: switch (input) {
	goto	l3695
	line	138
	
l3689:	
	fcall	_ShowTime
	line	139
;main.c: 139: break;
	goto	l3687
	line	140
	
l3691:	
	fcall	_SetupTime
	line	141
;main.c: 141: break;
	goto	l3687
	line	137
	
l3695:	
	movf	(main@input),w
	; Switch size 1, requested type "space"
; Number of cases is 2, Range of values is 49 to 50
; switch strategies available:
; Name         Bytes Cycles
; simple_byte     7     4 (average)
; direct_byte    18    16 (fixed)
;	Chosen strategy is simple_byte

	xorlw	49^0	; case 49
	skipnz
	goto	l3689
	xorlw	50^49	; case 50
	skipnz
	goto	l3691
	goto	l3687

	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

psect	maintext
	line	149
	signat	_main,88
	global	_SetupTime
psect	text985,local,class=CODE,delta=2
global __ptext985
__ptext985:

;; *************** function _SetupTime *****************
;; Defined at:
;;		line 270 in file "X:\Hi-tech\RTC_clock\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  temp            1   33[BANK0 ] unsigned char 
;;  temp_maj        1   32[BANK0 ] unsigned char 
;;  temp_min        1   31[BANK0 ] unsigned char 
;;  state           1   30[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       4       0       0
;;      Temps:          0       1       0       0
;;      Totals:         0       5       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    8
;; This function calls:
;;		_printf
;;		_getch
;;		___bmul
;;		_SetHour
;;		_SetMin
;;		_SetSeconds
;;		_ShowTime
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text985
	file	"X:\Hi-tech\RTC_clock\main.c"
	line	270
	global	__size_of_SetupTime
	__size_of_SetupTime	equ	__end_of_SetupTime-_SetupTime
	
_SetupTime:	
	opt	stack 7
; Regs used in _SetupTime: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	271
	
l3557:	
;main.c: 271: unsigned char state = 1;
	clrf	(SetupTime@state)
	incf	(SetupTime@state),f
	line	272
	
l3559:	
	line	273
;main.c: 273: unsigned char temp_maj = 0;
	clrf	(SetupTime@temp_maj)
	line	274
;main.c: 274: unsigned char temp = 0;
	clrf	(SetupTime@temp)
	line	276
;main.c: 276: while(state){
	goto	l3583
	line	277
	
l3561:	
;main.c: 277: printf("\fEnter hours - ");
	movlw	((STR_8-__stringbase))&0ffh
	fcall	_printf
	line	278
	
l3563:	
;main.c: 278: temp_maj = getch();
	fcall	_getch
	movwf	(SetupTime@temp_maj)
	line	279
	
l3565:	
;main.c: 279: printf(" %c", temp_maj);
	movf	(SetupTime@temp_maj),w
	movwf	(?_printf)
	clrf	(?_printf+1)
	movlw	((STR_9-__stringbase))&0ffh
	fcall	_printf
	line	280
;main.c: 280: temp_min = getch();
	fcall	_getch
	movwf	(SetupTime@temp_min)
	line	281
	
l3567:	
;main.c: 281: printf("%c", temp_min);
	movf	(SetupTime@temp_min),w
	movwf	(?_printf)
	clrf	(?_printf+1)
	movlw	((STR_10-__stringbase))&0ffh
	fcall	_printf
	line	282
	
l3569:	
;main.c: 282: temp = getch();
	fcall	_getch
	movwf	(SetupTime@temp)
	line	283
	
l3571:	
;main.c: 283: if (temp == 13) {
	movf	(SetupTime@temp),w
	xorlw	0Dh
	skipz
	goto	u1211
	goto	u1210
u1211:
	goto	l3583
u1210:
	line	284
	
l3573:	
;main.c: 284: temp_maj -= 48;
	movlw	low(030h)
	subwf	(SetupTime@temp_maj),f
	line	285
;main.c: 285: temp_min -= 48;
	movlw	low(030h)
	subwf	(SetupTime@temp_min),f
	line	286
	
l3575:	
;main.c: 286: temp = temp_maj*10 + temp_min;
	movlw	(0Ah)
	movwf	(?___bmul)
	movf	(SetupTime@temp_maj),w
	fcall	___bmul
	movwf	(??_SetupTime+0)+0
	movf	(SetupTime@temp_min),w
	addwf	0+(??_SetupTime+0)+0,w
	movwf	(SetupTime@temp)
	line	287
	
l3577:	
;main.c: 287: if (temp < 24) {
	movlw	(018h)
	subwf	(SetupTime@temp),w
	skipnc
	goto	u1221
	goto	u1220
u1221:
	goto	l3583
u1220:
	line	288
	
l3579:	
;main.c: 288: SetHour(temp);
	movf	(SetupTime@temp),w
	fcall	_SetHour
	line	289
	
l3581:	
;main.c: 289: state = 0;
	bcf	status, 5	;RP0=0, select bank0
	clrf	(SetupTime@state)
	line	276
	
l3583:	
	movf	(SetupTime@state),f
	skipz
	goto	u1231
	goto	u1230
u1231:
	goto	l3561
u1230:
	
l411:	
	line	294
;main.c: 290: }
;main.c: 292: }
;main.c: 293: }
;main.c: 294: state = 1;
	clrf	(SetupTime@state)
	incf	(SetupTime@state),f
	line	295
;main.c: 295: while(state){
	goto	l3607
	line	296
	
l3585:	
;main.c: 296: printf("\fEnter minutes - ");
	movlw	((STR_11-__stringbase))&0ffh
	fcall	_printf
	line	297
	
l3587:	
;main.c: 297: temp_maj = getch();
	fcall	_getch
	movwf	(SetupTime@temp_maj)
	line	298
	
l3589:	
;main.c: 298: printf(" %c", temp_maj);
	movf	(SetupTime@temp_maj),w
	movwf	(?_printf)
	clrf	(?_printf+1)
	movlw	((STR_12-__stringbase))&0ffh
	fcall	_printf
	line	299
;main.c: 299: temp_min = getch();
	fcall	_getch
	movwf	(SetupTime@temp_min)
	line	300
	
l3591:	
;main.c: 300: printf("%c", temp_min);
	movf	(SetupTime@temp_min),w
	movwf	(?_printf)
	clrf	(?_printf+1)
	movlw	((STR_13-__stringbase))&0ffh
	fcall	_printf
	line	301
	
l3593:	
;main.c: 301: temp = getch();
	fcall	_getch
	movwf	(SetupTime@temp)
	line	302
	
l3595:	
;main.c: 302: if (temp == 13) {
	movf	(SetupTime@temp),w
	xorlw	0Dh
	skipz
	goto	u1241
	goto	u1240
u1241:
	goto	l3607
u1240:
	line	303
	
l3597:	
;main.c: 303: temp_maj -= 48;
	movlw	low(030h)
	subwf	(SetupTime@temp_maj),f
	line	304
;main.c: 304: temp_min -= 48;
	movlw	low(030h)
	subwf	(SetupTime@temp_min),f
	line	305
	
l3599:	
;main.c: 305: temp = temp_maj*10 + temp_min;
	movlw	(0Ah)
	movwf	(?___bmul)
	movf	(SetupTime@temp_maj),w
	fcall	___bmul
	movwf	(??_SetupTime+0)+0
	movf	(SetupTime@temp_min),w
	addwf	0+(??_SetupTime+0)+0,w
	movwf	(SetupTime@temp)
	line	306
	
l3601:	
;main.c: 306: if (temp < 61) {
	movlw	(03Dh)
	subwf	(SetupTime@temp),w
	skipnc
	goto	u1251
	goto	u1250
u1251:
	goto	l3607
u1250:
	line	307
	
l3603:	
;main.c: 307: SetMin(temp);
	movf	(SetupTime@temp),w
	fcall	_SetMin
	line	308
	
l3605:	
;main.c: 308: state = 0;
	bcf	status, 5	;RP0=0, select bank0
	clrf	(SetupTime@state)
	line	295
	
l3607:	
	movf	(SetupTime@state),f
	skipz
	goto	u1261
	goto	u1260
u1261:
	goto	l3585
u1260:
	
l416:	
	line	312
;main.c: 309: }
;main.c: 310: }
;main.c: 311: }
;main.c: 312: state = 1;
	clrf	(SetupTime@state)
	incf	(SetupTime@state),f
	line	313
;main.c: 313: while(state){
	goto	l3631
	line	314
	
l3609:	
;main.c: 314: printf("\fEnter seconds - ");
	movlw	((STR_14-__stringbase))&0ffh
	fcall	_printf
	line	315
	
l3611:	
;main.c: 315: temp_maj = getch();
	fcall	_getch
	movwf	(SetupTime@temp_maj)
	line	316
	
l3613:	
;main.c: 316: printf(" %c", temp_maj);
	movf	(SetupTime@temp_maj),w
	movwf	(?_printf)
	clrf	(?_printf+1)
	movlw	((STR_15-__stringbase))&0ffh
	fcall	_printf
	line	317
;main.c: 317: temp_min = getch();
	fcall	_getch
	movwf	(SetupTime@temp_min)
	line	318
	
l3615:	
;main.c: 318: printf("%c", temp_min);
	movf	(SetupTime@temp_min),w
	movwf	(?_printf)
	clrf	(?_printf+1)
	movlw	((STR_16-__stringbase))&0ffh
	fcall	_printf
	line	319
	
l3617:	
;main.c: 319: temp = getch();
	fcall	_getch
	movwf	(SetupTime@temp)
	line	320
	
l3619:	
;main.c: 320: if (temp == 13) {
	movf	(SetupTime@temp),w
	xorlw	0Dh
	skipz
	goto	u1271
	goto	u1270
u1271:
	goto	l3631
u1270:
	line	321
	
l3621:	
;main.c: 321: temp_maj -= 48;
	movlw	low(030h)
	subwf	(SetupTime@temp_maj),f
	line	322
;main.c: 322: temp_min -= 48;
	movlw	low(030h)
	subwf	(SetupTime@temp_min),f
	line	323
	
l3623:	
;main.c: 323: temp = temp_maj*10 + temp_min;
	movlw	(0Ah)
	movwf	(?___bmul)
	movf	(SetupTime@temp_maj),w
	fcall	___bmul
	movwf	(??_SetupTime+0)+0
	movf	(SetupTime@temp_min),w
	addwf	0+(??_SetupTime+0)+0,w
	movwf	(SetupTime@temp)
	line	324
	
l3625:	
;main.c: 324: if (temp < 61) {
	movlw	(03Dh)
	subwf	(SetupTime@temp),w
	skipnc
	goto	u1281
	goto	u1280
u1281:
	goto	l3631
u1280:
	line	325
	
l3627:	
;main.c: 325: SetSeconds(temp);
	movf	(SetupTime@temp),w
	fcall	_SetSeconds
	line	326
	
l3629:	
;main.c: 326: state = 0;
	bcf	status, 5	;RP0=0, select bank0
	clrf	(SetupTime@state)
	line	313
	
l3631:	
	movf	(SetupTime@state),f
	skipz
	goto	u1291
	goto	u1290
u1291:
	goto	l3609
u1290:
	line	331
	
l3633:	
;main.c: 327: }
;main.c: 328: }
;main.c: 329: }
;main.c: 331: ShowTime();
	fcall	_ShowTime
	line	333
	
l422:	
	return
	opt stack 0
GLOBAL	__end_of_SetupTime
	__end_of_SetupTime:
;; =============== function _SetupTime ends ============

	signat	_SetupTime,88
	global	_ShowTime
psect	text986,local,class=CODE,delta=2
global __ptext986
__ptext986:

;; *************** function _ShowTime *****************
;; Defined at:
;;		line 257 in file "X:\Hi-tech\RTC_clock\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 40/20
;;		On exit  : 60/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    7
;; This function calls:
;;		_ReadHour
;;		_printf
;;		_ReadMin
;;		_ReadSeconds
;; This function is called by:
;;		_main
;;		_SetupTime
;; This function uses a non-reentrant model
;;
psect	text986
	file	"X:\Hi-tech\RTC_clock\main.c"
	line	257
	global	__size_of_ShowTime
	__size_of_ShowTime	equ	__end_of_ShowTime-_ShowTime
	
_ShowTime:	
	opt	stack 6
; Regs used in _ShowTime: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	258
	
l3555:	
;main.c: 258: printf("\fTime - %d :", ReadHour());
	fcall	_ReadHour
	movwf	(?_printf)
	clrf	(?_printf+1)
	movlw	((STR_1-__stringbase))&0ffh
	fcall	_printf
	line	259
;main.c: 259: printf(" %d :", ReadMin());
	fcall	_ReadMin
	movwf	(?_printf)
	clrf	(?_printf+1)
	movlw	((STR_2-__stringbase))&0ffh
	fcall	_printf
	line	260
;main.c: 260: printf(" %d ", ReadSeconds());
	fcall	_ReadSeconds
	movwf	(?_printf)
	clrf	(?_printf+1)
	movlw	((STR_3-__stringbase))&0ffh
	fcall	_printf
	line	262
;main.c: 262: printf("\r\n*********************");
	movlw	((STR_4-__stringbase))&0ffh
	fcall	_printf
	line	264
;main.c: 264: printf("\r\n Menu");
	movlw	((STR_5-__stringbase))&0ffh
	fcall	_printf
	line	265
;main.c: 265: printf("\r\n 1 - Reload time");
	movlw	((STR_6-__stringbase))&0ffh
	fcall	_printf
	line	266
;main.c: 266: printf("\r\n 2 - Setup time");
	movlw	((STR_7-__stringbase))&0ffh
	fcall	_printf
	line	268
	
l404:	
	return
	opt stack 0
GLOBAL	__end_of_ShowTime
	__end_of_ShowTime:
;; =============== function _ShowTime ends ============

	signat	_ShowTime,88
	global	_ReadSeconds
psect	text987,local,class=CODE,delta=2
global __ptext987
__ptext987:

;; *************** function _ReadSeconds *****************
;; Defined at:
;;		line 244 in file "X:\Hi-tech\RTC_clock\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  temp            1   28[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       1       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       1       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    6
;; This function calls:
;;		_i2c_start
;;		_i2c_tx
;;		_i2c_rx
;;		_i2c_stop
;;		_BCDconv
;; This function is called by:
;;		_ShowTime
;; This function uses a non-reentrant model
;;
psect	text987
	file	"X:\Hi-tech\RTC_clock\main.c"
	line	244
	global	__size_of_ReadSeconds
	__size_of_ReadSeconds	equ	__end_of_ReadSeconds-_ReadSeconds
	
_ReadSeconds:	
	opt	stack 5
; Regs used in _ReadSeconds: [wreg+status,2+status,0+pclath+cstack]
	line	245
	
l3535:	
	line	246
	
l3537:	
;main.c: 246: i2c_start();
	fcall	_i2c_start
	line	247
	
l3539:	
;main.c: 247: i2c_tx(0b11010000);
	movlw	(0D0h)
	fcall	_i2c_tx
	line	248
	
l3541:	
;main.c: 248: i2c_tx(0x00);
	movlw	(0)
	fcall	_i2c_tx
	line	249
	
l3543:	
;main.c: 249: i2c_start();
	fcall	_i2c_start
	line	250
	
l3545:	
;main.c: 250: i2c_tx(0b11010001);
	movlw	(0D1h)
	fcall	_i2c_tx
	line	251
	
l3547:	
;main.c: 251: temp = i2c_rx(0);
	movlw	(0)
	fcall	_i2c_rx
	movwf	(ReadSeconds@temp)
	line	252
	
l3549:	
;main.c: 252: i2c_stop();
	fcall	_i2c_stop
	line	254
	
l3551:	
;main.c: 254: return BCDconv(temp);
	bcf	status, 5	;RP0=0, select bank0
	movf	(ReadSeconds@temp),w
	fcall	_BCDconv
	line	255
	
l401:	
	return
	opt stack 0
GLOBAL	__end_of_ReadSeconds
	__end_of_ReadSeconds:
;; =============== function _ReadSeconds ends ============

	signat	_ReadSeconds,89
	global	_ReadMin
psect	text988,local,class=CODE,delta=2
global __ptext988
__ptext988:

;; *************** function _ReadMin *****************
;; Defined at:
;;		line 196 in file "X:\Hi-tech\RTC_clock\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  temp            1   28[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       1       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       1       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    6
;; This function calls:
;;		_i2c_start
;;		_i2c_tx
;;		_i2c_rx
;;		_i2c_stop
;;		_BCDconv
;; This function is called by:
;;		_main
;;		_ShowTime
;; This function uses a non-reentrant model
;;
psect	text988
	file	"X:\Hi-tech\RTC_clock\main.c"
	line	196
	global	__size_of_ReadMin
	__size_of_ReadMin	equ	__end_of_ReadMin-_ReadMin
	
_ReadMin:	
	opt	stack 5
; Regs used in _ReadMin: [wreg+status,2+status,0+pclath+cstack]
	line	197
	
l3515:	
	line	198
	
l3517:	
;main.c: 198: i2c_start();
	fcall	_i2c_start
	line	199
	
l3519:	
;main.c: 199: i2c_tx(0b11010000);
	movlw	(0D0h)
	fcall	_i2c_tx
	line	200
	
l3521:	
;main.c: 200: i2c_tx(0x01);
	movlw	(01h)
	fcall	_i2c_tx
	line	201
	
l3523:	
;main.c: 201: i2c_start();
	fcall	_i2c_start
	line	202
	
l3525:	
;main.c: 202: i2c_tx(0b11010001);
	movlw	(0D1h)
	fcall	_i2c_tx
	line	203
	
l3527:	
;main.c: 203: temp = i2c_rx(0);
	movlw	(0)
	fcall	_i2c_rx
	movwf	(ReadMin@temp)
	line	204
	
l3529:	
;main.c: 204: i2c_stop();
	fcall	_i2c_stop
	line	206
	
l3531:	
;main.c: 206: return BCDconv(temp);
	bcf	status, 5	;RP0=0, select bank0
	movf	(ReadMin@temp),w
	fcall	_BCDconv
	line	207
	
l389:	
	return
	opt stack 0
GLOBAL	__end_of_ReadMin
	__end_of_ReadMin:
;; =============== function _ReadMin ends ============

	signat	_ReadMin,89
	global	_ReadHour
psect	text989,local,class=CODE,delta=2
global __ptext989
__ptext989:

;; *************** function _ReadHour *****************
;; Defined at:
;;		line 217 in file "X:\Hi-tech\RTC_clock\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  temp            1   28[BANK0 ] unsigned char 
;;  temp2           1    0        unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 40/0
;;		On exit  : 60/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       1       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       1       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    6
;; This function calls:
;;		_i2c_start
;;		_i2c_tx
;;		_i2c_rx
;;		_i2c_stop
;;		_BCDconv
;; This function is called by:
;;		_main
;;		_ShowTime
;; This function uses a non-reentrant model
;;
psect	text989
	file	"X:\Hi-tech\RTC_clock\main.c"
	line	217
	global	__size_of_ReadHour
	__size_of_ReadHour	equ	__end_of_ReadHour-_ReadHour
	
_ReadHour:	
	opt	stack 5
; Regs used in _ReadHour: [wreg+status,2+status,0+pclath+cstack]
	line	218
	
l3495:	
	line	220
	
l3497:	
;main.c: 220: i2c_start();
	fcall	_i2c_start
	line	221
	
l3499:	
;main.c: 221: i2c_tx(0b11010000);
	movlw	(0D0h)
	fcall	_i2c_tx
	line	222
	
l3501:	
;main.c: 222: i2c_tx(0x02);
	movlw	(02h)
	fcall	_i2c_tx
	line	223
	
l3503:	
;main.c: 223: i2c_start();
	fcall	_i2c_start
	line	224
	
l3505:	
;main.c: 224: i2c_tx(0b11010001);
	movlw	(0D1h)
	fcall	_i2c_tx
	line	225
	
l3507:	
;main.c: 225: temp = i2c_rx(0);
	movlw	(0)
	fcall	_i2c_rx
	movwf	(ReadHour@temp)
	line	226
	
l3509:	
;main.c: 226: i2c_stop();
	fcall	_i2c_stop
	line	233
	
l3511:	
;main.c: 233: return BCDconv(temp);
	bcf	status, 5	;RP0=0, select bank0
	movf	(ReadHour@temp),w
	fcall	_BCDconv
	line	234
	
l395:	
	return
	opt stack 0
GLOBAL	__end_of_ReadHour
	__end_of_ReadHour:
;; =============== function _ReadHour ends ============

	signat	_ReadHour,89
	global	_InitIO
psect	text990,local,class=CODE,delta=2
global __ptext990
__ptext990:

;; *************** function _InitIO *****************
;; Defined at:
;;		line 335 in file "X:\Hi-tech\RTC_clock\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    6
;; This function calls:
;;		_i2c_start
;;		_i2c_tx
;;		_i2c_stop
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text990
	file	"X:\Hi-tech\RTC_clock\main.c"
	line	335
	global	__size_of_InitIO
	__size_of_InitIO	equ	__end_of_InitIO-_InitIO
	
_InitIO:	
	opt	stack 7
; Regs used in _InitIO: [wreg+status,2+status,0+pclath+cstack]
	line	336
	
l3485:	
;main.c: 336: i2c_start();
	fcall	_i2c_start
	line	337
	
l3487:	
;main.c: 337: i2c_tx(0b11101000);
	movlw	(0E8h)
	fcall	_i2c_tx
	line	338
	
l3489:	
;main.c: 338: i2c_tx(0b00000110);
	movlw	(06h)
	fcall	_i2c_tx
	line	339
	
l3491:	
;main.c: 339: i2c_tx(0x00);
	movlw	(0)
	fcall	_i2c_tx
	line	340
	
l3493:	
;main.c: 340: i2c_tx(0x00);
	movlw	(0)
	fcall	_i2c_tx
	line	341
;main.c: 341: i2c_stop();
	fcall	_i2c_stop
	line	342
	
l425:	
	return
	opt stack 0
GLOBAL	__end_of_InitIO
	__end_of_InitIO:
;; =============== function _InitIO ends ============

	signat	_InitIO,88
	global	_SetSeconds
psect	text991,local,class=CODE,delta=2
global __ptext991
__ptext991:

;; *************** function _SetSeconds *****************
;; Defined at:
;;		line 236 in file "X:\Hi-tech\RTC_clock\main.c"
;; Parameters:    Size  Location     Type
;;  seconds         1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  seconds         1   27[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 40/20
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       1       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       1       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    6
;; This function calls:
;;		_i2c_start
;;		_i2c_tx
;;		_DCBconv
;;		_i2c_stop
;; This function is called by:
;;		_main
;;		_SetupTime
;; This function uses a non-reentrant model
;;
psect	text991
	file	"X:\Hi-tech\RTC_clock\main.c"
	line	236
	global	__size_of_SetSeconds
	__size_of_SetSeconds	equ	__end_of_SetSeconds-_SetSeconds
	
_SetSeconds:	
	opt	stack 6
; Regs used in _SetSeconds: [wreg+status,2+status,0+pclath+cstack]
;SetSeconds@seconds stored from wreg
	bcf	status, 5	;RP0=0, select bank0
	movwf	(SetSeconds@seconds)
	line	237
	
l3475:	
;main.c: 237: i2c_start();
	fcall	_i2c_start
	line	238
	
l3477:	
;main.c: 238: i2c_tx(0b11010000);
	movlw	(0D0h)
	fcall	_i2c_tx
	line	239
	
l3479:	
;main.c: 239: i2c_tx(0x00);
	movlw	(0)
	fcall	_i2c_tx
	line	240
	
l3481:	
;main.c: 240: i2c_tx(DCBconv(seconds));
	bcf	status, 5	;RP0=0, select bank0
	movf	(SetSeconds@seconds),w
	fcall	_DCBconv
	fcall	_i2c_tx
	line	241
	
l3483:	
;main.c: 241: i2c_stop();
	fcall	_i2c_stop
	line	242
	
l398:	
	return
	opt stack 0
GLOBAL	__end_of_SetSeconds
	__end_of_SetSeconds:
;; =============== function _SetSeconds ends ============

	signat	_SetSeconds,4216
	global	_SetMin
psect	text992,local,class=CODE,delta=2
global __ptext992
__ptext992:

;; *************** function _SetMin *****************
;; Defined at:
;;		line 188 in file "X:\Hi-tech\RTC_clock\main.c"
;; Parameters:    Size  Location     Type
;;  minutes         1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  minutes         1   27[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 40/20
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       1       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       1       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    6
;; This function calls:
;;		_i2c_start
;;		_i2c_tx
;;		_DCBconv
;;		_i2c_stop
;; This function is called by:
;;		_main
;;		_SetupTime
;; This function uses a non-reentrant model
;;
psect	text992
	file	"X:\Hi-tech\RTC_clock\main.c"
	line	188
	global	__size_of_SetMin
	__size_of_SetMin	equ	__end_of_SetMin-_SetMin
	
_SetMin:	
	opt	stack 6
; Regs used in _SetMin: [wreg+status,2+status,0+pclath+cstack]
;SetMin@minutes stored from wreg
	bcf	status, 5	;RP0=0, select bank0
	movwf	(SetMin@minutes)
	line	189
	
l3465:	
;main.c: 189: i2c_start();
	fcall	_i2c_start
	line	190
	
l3467:	
;main.c: 190: i2c_tx(0b11010000);
	movlw	(0D0h)
	fcall	_i2c_tx
	line	191
	
l3469:	
;main.c: 191: i2c_tx(0x01);
	movlw	(01h)
	fcall	_i2c_tx
	line	192
	
l3471:	
;main.c: 192: i2c_tx(DCBconv(minutes));
	bcf	status, 5	;RP0=0, select bank0
	movf	(SetMin@minutes),w
	fcall	_DCBconv
	fcall	_i2c_tx
	line	193
	
l3473:	
;main.c: 193: i2c_stop();
	fcall	_i2c_stop
	line	194
	
l386:	
	return
	opt stack 0
GLOBAL	__end_of_SetMin
	__end_of_SetMin:
;; =============== function _SetMin ends ============

	signat	_SetMin,4216
	global	_SetHour
psect	text993,local,class=CODE,delta=2
global __ptext993
__ptext993:

;; *************** function _SetHour *****************
;; Defined at:
;;		line 209 in file "X:\Hi-tech\RTC_clock\main.c"
;; Parameters:    Size  Location     Type
;;  hours           1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  hours           1   27[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 40/20
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       1       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       1       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    6
;; This function calls:
;;		_i2c_start
;;		_i2c_tx
;;		_DCBconv
;;		_i2c_stop
;; This function is called by:
;;		_main
;;		_SetupTime
;; This function uses a non-reentrant model
;;
psect	text993
	file	"X:\Hi-tech\RTC_clock\main.c"
	line	209
	global	__size_of_SetHour
	__size_of_SetHour	equ	__end_of_SetHour-_SetHour
	
_SetHour:	
	opt	stack 6
; Regs used in _SetHour: [wreg+status,2+status,0+pclath+cstack]
;SetHour@hours stored from wreg
	bcf	status, 5	;RP0=0, select bank0
	movwf	(SetHour@hours)
	line	210
	
l3455:	
;main.c: 210: i2c_start();
	fcall	_i2c_start
	line	211
	
l3457:	
;main.c: 211: i2c_tx(0b11010000);
	movlw	(0D0h)
	fcall	_i2c_tx
	line	212
	
l3459:	
;main.c: 212: i2c_tx(0x02);
	movlw	(02h)
	fcall	_i2c_tx
	line	213
	
l3461:	
;main.c: 213: i2c_tx(DCBconv(hours));
	bcf	status, 5	;RP0=0, select bank0
	movf	(SetHour@hours),w
	fcall	_DCBconv
	fcall	_i2c_tx
	line	214
	
l3463:	
;main.c: 214: i2c_stop();
	fcall	_i2c_stop
	line	215
	
l392:	
	return
	opt stack 0
GLOBAL	__end_of_SetHour
	__end_of_SetHour:
;; =============== function _SetHour ends ============

	signat	_SetHour,4216
	global	_i2c_rx
psect	text994,local,class=CODE,delta=2
global __ptext994
__ptext994:

;; *************** function _i2c_rx *****************
;; Defined at:
;;		line 58 in file "X:\Hi-tech\RTC_clock\softi2c.c"
;; Parameters:    Size  Location     Type
;;  ack             1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  ack             1   25[BANK0 ] unsigned char 
;;  x               1   27[BANK0 ] unsigned char 
;;  d               1   26[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/20
;;		On exit  : 60/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       3       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       3       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    5
;; This function calls:
;;		_i2c_delay
;; This function is called by:
;;		_ReadMin
;;		_ReadHour
;;		_ReadSeconds
;; This function uses a non-reentrant model
;;
psect	text994
	file	"X:\Hi-tech\RTC_clock\softi2c.c"
	line	58
	global	__size_of_i2c_rx
	__size_of_i2c_rx	equ	__end_of_i2c_rx-_i2c_rx
	
_i2c_rx:	
	opt	stack 4
; Regs used in _i2c_rx: [wreg+status,2+status,0+pclath+cstack]
;i2c_rx@ack stored from wreg
	bcf	status, 5	;RP0=0, select bank0
	movwf	(i2c_rx@ack)
	line	59
	
l3417:	
;softi2c.c: 59: unsigned char x,d=0;
	clrf	(i2c_rx@d)
	line	60
	
l3419:	
;softi2c.c: 60: TRISA1 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bsf	(1065/8)^080h,(1065)&7
	line	61
;softi2c.c: 61: for (x=0; x<8;x++) {
	bcf	status, 5	;RP0=0, select bank0
	clrf	(i2c_rx@x)
	
l1116:	
	line	62
;softi2c.c: 62: d <<= 1;
	clrc
	rlf	(i2c_rx@d),f
	line	64
	
l3425:	
;softi2c.c: 64: i2c_delay();
	fcall	_i2c_delay
	line	65
	
l3427:	
;softi2c.c: 65: TRISA0 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1064/8)^080h,(1064)&7
	line	66
	
l3429:	
;softi2c.c: 66: } while (RA0==0);
	bcf	status, 5	;RP0=0, select bank0
	btfss	(40/8),(40)&7
	goto	u1171
	goto	u1170
u1171:
	goto	l3425
u1170:
	line	67
	
l3431:	
;softi2c.c: 67: i2c_delay();
	fcall	_i2c_delay
	line	68
	
l3433:	
;softi2c.c: 68: if (RA1) d |= 1;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(41/8),(41)&7
	goto	u1181
	goto	u1180
u1181:
	goto	l1120
u1180:
	
l3435:	
	bsf	(i2c_rx@d)+(0/8),(0)&7
	
l1120:	
	line	69
;softi2c.c: 69: TRISA0 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	(1064/8)^080h,(1064)&7
	line	61
	
l3437:	
	bcf	status, 5	;RP0=0, select bank0
	incf	(i2c_rx@x),f
	
l3439:	
	movlw	(08h)
	subwf	(i2c_rx@x),w
	skipc
	goto	u1191
	goto	u1190
u1191:
	goto	l1116
u1190:
	line	71
	
l3441:	
;softi2c.c: 70: }
;softi2c.c: 71: i2c_delay();
	fcall	_i2c_delay
	line	72
	
l3443:	
;softi2c.c: 72: if (ack) TRISA1 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(i2c_rx@ack),w
	skipz
	goto	u1200
	goto	l1121
u1200:
	
l3445:	
	bsf	status, 5	;RP0=1, select bank1
	bcf	(1065/8)^080h,(1065)&7
	goto	l1122
	line	73
	
l1121:	
;softi2c.c: 73: else TRISA1 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bsf	(1065/8)^080h,(1065)&7
	
l1122:	
	line	74
;softi2c.c: 74: TRISA0 = 1;
	bsf	(1064/8)^080h,(1064)&7
	line	75
	
l3447:	
;softi2c.c: 75: i2c_delay();
	fcall	_i2c_delay
	line	76
	
l3449:	
;softi2c.c: 76: TRISA0 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1064/8)^080h,(1064)&7
	line	78
	
l3451:	
;softi2c.c: 78: return d;
	bcf	status, 5	;RP0=0, select bank0
	movf	(i2c_rx@d),w
	line	80
	
l1123:	
	return
	opt stack 0
GLOBAL	__end_of_i2c_rx
	__end_of_i2c_rx:
;; =============== function _i2c_rx ends ============

	signat	_i2c_rx,4217
	global	_i2c_stop
psect	text995,local,class=CODE,delta=2
global __ptext995
__ptext995:

;; *************** function _i2c_stop *****************
;; Defined at:
;;		line 20 in file "X:\Hi-tech\RTC_clock\softi2c.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 40/0
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    5
;; This function calls:
;;		_i2c_delay
;; This function is called by:
;;		_SetMin
;;		_ReadMin
;;		_SetHour
;;		_ReadHour
;;		_SetSeconds
;;		_ReadSeconds
;;		_InitIO
;; This function uses a non-reentrant model
;;
psect	text995
	file	"X:\Hi-tech\RTC_clock\softi2c.c"
	line	20
	global	__size_of_i2c_stop
	__size_of_i2c_stop	equ	__end_of_i2c_stop-_i2c_stop
	
_i2c_stop:	
	opt	stack 4
; Regs used in _i2c_stop: [status,2+status,0+pclath+cstack]
	line	21
	
l3407:	
;softi2c.c: 21: i2c_delay();
	fcall	_i2c_delay
	line	22
	
l3409:	
;softi2c.c: 22: TRISA1 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1065/8)^080h,(1065)&7
	line	23
	
l3411:	
;softi2c.c: 23: TRISA0 = 0;
	bcf	(1064/8)^080h,(1064)&7
	line	24
;softi2c.c: 24: i2c_delay();
	fcall	_i2c_delay
	line	25
	
l3413:	
;softi2c.c: 25: TRISA0 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1064/8)^080h,(1064)&7
	line	26
;softi2c.c: 26: i2c_delay();
	fcall	_i2c_delay
	line	27
	
l3415:	
;softi2c.c: 27: TRISA1 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1065/8)^080h,(1065)&7
	line	29
	
l1104:	
	return
	opt stack 0
GLOBAL	__end_of_i2c_stop
	__end_of_i2c_stop:
;; =============== function _i2c_stop ends ============

	signat	_i2c_stop,88
	global	_i2c_tx
psect	text996,local,class=CODE,delta=2
global __ptext996
__ptext996:

;; *************** function _i2c_tx *****************
;; Defined at:
;;		line 32 in file "X:\Hi-tech\RTC_clock\softi2c.c"
;; Parameters:    Size  Location     Type
;;  d               1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  d               1   25[BANK0 ] unsigned char 
;;  x               1   26[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 40/20
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       2       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       2       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    5
;; This function calls:
;;		_i2c_delay
;; This function is called by:
;;		_SetMin
;;		_ReadMin
;;		_SetHour
;;		_ReadHour
;;		_SetSeconds
;;		_ReadSeconds
;;		_InitIO
;; This function uses a non-reentrant model
;;
psect	text996
	file	"X:\Hi-tech\RTC_clock\softi2c.c"
	line	32
	global	__size_of_i2c_tx
	__size_of_i2c_tx	equ	__end_of_i2c_tx-_i2c_tx
	
_i2c_tx:	
	opt	stack 4
; Regs used in _i2c_tx: [wreg+status,2+status,0+pclath+cstack]
;i2c_tx@d stored from wreg
	line	34
	bcf	status, 5	;RP0=0, select bank0
	movwf	(i2c_tx@d)
	line	35
	
l3371:	
;softi2c.c: 33: char x;
;softi2c.c: 34: static bit b;
;softi2c.c: 35: for (x=0; x<8; x++) {
	clrf	(i2c_tx@x)
	line	36
	
l3377:	
;softi2c.c: 36: i2c_delay();
	fcall	_i2c_delay
	line	37
	
l3379:	
;softi2c.c: 37: if (d&0x80) TRISA1 = 1;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(i2c_tx@d),(7)&7
	goto	u1151
	goto	u1150
u1151:
	goto	l1111
u1150:
	
l3381:	
	bsf	status, 5	;RP0=1, select bank1
	bsf	(1065/8)^080h,(1065)&7
	goto	l3383
	line	38
	
l1111:	
;softi2c.c: 38: else TRISA1 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	(1065/8)^080h,(1065)&7
	line	39
	
l3383:	
;softi2c.c: 39: i2c_delay();
	fcall	_i2c_delay
	line	40
	
l3385:	
;softi2c.c: 40: TRISA0 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1064/8)^080h,(1064)&7
	line	41
	
l3387:	
;softi2c.c: 41: d <<= 1;
	bcf	status, 5	;RP0=0, select bank0
	clrc
	rlf	(i2c_tx@d),f
	line	42
;softi2c.c: 42: i2c_delay();
	fcall	_i2c_delay
	line	43
	
l3389:	
;softi2c.c: 43: TRISA0 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1064/8)^080h,(1064)&7
	line	35
	
l3391:	
	bcf	status, 5	;RP0=0, select bank0
	incf	(i2c_tx@x),f
	
l3393:	
	movlw	(08h)
	subwf	(i2c_tx@x),w
	skipc
	goto	u1161
	goto	u1160
u1161:
	goto	l3377
u1160:
	line	45
	
l3395:	
;softi2c.c: 44: }
;softi2c.c: 45: i2c_delay();
	fcall	_i2c_delay
	line	46
;softi2c.c: 46: i2c_delay();
	fcall	_i2c_delay
	line	47
	
l3397:	
;softi2c.c: 47: TRISA1 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1065/8)^080h,(1065)&7
	line	48
	
l3399:	
;softi2c.c: 48: TRISA0 = 1;
	bsf	(1064/8)^080h,(1064)&7
	line	49
;softi2c.c: 49: i2c_delay();
	fcall	_i2c_delay
	line	51
	
l3401:	
;softi2c.c: 51: b = RA1;
	line	52
	
l3403:	
;softi2c.c: 52: TRISA0 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1064/8)^080h,(1064)&7
	line	55
	
l1113:	
	return
	opt stack 0
GLOBAL	__end_of_i2c_tx
	__end_of_i2c_tx:
;; =============== function _i2c_tx ends ============

	signat	_i2c_tx,4216
	global	_i2c_start
psect	text997,local,class=CODE,delta=2
global __ptext997
__ptext997:

;; *************** function _i2c_start *****************
;; Defined at:
;;		line 10 in file "X:\Hi-tech\RTC_clock\softi2c.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 40/0
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    5
;; This function calls:
;;		_i2c_delay
;; This function is called by:
;;		_SetMin
;;		_ReadMin
;;		_SetHour
;;		_ReadHour
;;		_SetSeconds
;;		_ReadSeconds
;;		_InitIO
;; This function uses a non-reentrant model
;;
psect	text997
	file	"X:\Hi-tech\RTC_clock\softi2c.c"
	line	10
	global	__size_of_i2c_start
	__size_of_i2c_start	equ	__end_of_i2c_start-_i2c_start
	
_i2c_start:	
	opt	stack 4
; Regs used in _i2c_start: [status,2+status,0+pclath+cstack]
	line	11
	
l3363:	
;softi2c.c: 11: TRISA0 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bsf	(1064/8)^080h,(1064)&7
	line	12
;softi2c.c: 12: TRISA1 = 1;
	bsf	(1065/8)^080h,(1065)&7
	line	13
	
l3365:	
;softi2c.c: 13: i2c_delay();
	fcall	_i2c_delay
	line	14
	
l3367:	
;softi2c.c: 14: TRISA1 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1065/8)^080h,(1065)&7
	line	15
;softi2c.c: 15: i2c_delay();
	fcall	_i2c_delay
	line	16
	
l3369:	
;softi2c.c: 16: TRISA0 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1064/8)^080h,(1064)&7
	line	17
	
l1101:	
	return
	opt stack 0
GLOBAL	__end_of_i2c_start
	__end_of_i2c_start:
;; =============== function _i2c_start ends ============

	signat	_i2c_start,88
	global	_i2c_delay
psect	text998,local,class=CODE,delta=2
global __ptext998
__ptext998:

;; *************** function _i2c_delay *****************
;; Defined at:
;;		line 5 in file "X:\Hi-tech\RTC_clock\softi2c.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		None
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    4
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_i2c_start
;;		_i2c_stop
;;		_i2c_tx
;;		_i2c_rx
;; This function uses a non-reentrant model
;;
psect	text998
	file	"X:\Hi-tech\RTC_clock\softi2c.c"
	line	5
	global	__size_of_i2c_delay
	__size_of_i2c_delay	equ	__end_of_i2c_delay-_i2c_delay
	
_i2c_delay:	
	opt	stack 3
; Regs used in _i2c_delay: []
	line	6
	
l3361:	
;softi2c.c: 6: _delay((unsigned long)((5)*(4000000/4000000.0)));
		opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on
	opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on
	opt asmopt_off
	clrwdt
	opt asmopt_on

	line	7
	
l1098:	
	return
	opt stack 0
GLOBAL	__end_of_i2c_delay
	__end_of_i2c_delay:
;; =============== function _i2c_delay ends ============

	signat	_i2c_delay,88
	global	_printf
psect	text999,local,class=CODE,delta=2
global __ptext999
__ptext999:

;; *************** function _printf *****************
;; Defined at:
;;		line 461 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\lib\doprnt.c"
;; Parameters:    Size  Location     Type
;;  f               1    wreg     PTR const unsigned char 
;;		 -> STR_16(3), STR_15(4), STR_14(18), STR_13(3), 
;;		 -> STR_12(4), STR_11(18), STR_10(3), STR_9(4), 
;;		 -> STR_8(16), STR_7(18), STR_6(19), STR_5(8), 
;;		 -> STR_4(24), STR_3(5), STR_2(6), STR_1(13), 
;; Auto vars:     Size  Location     Type
;;  f               1   17[BANK0 ] PTR const unsigned char 
;;		 -> STR_16(3), STR_15(4), STR_14(18), STR_13(3), 
;;		 -> STR_12(4), STR_11(18), STR_10(3), STR_9(4), 
;;		 -> STR_8(16), STR_7(18), STR_6(19), STR_5(8), 
;;		 -> STR_4(24), STR_3(5), STR_2(6), STR_1(13), 
;;  _val            4   20[BANK0 ] struct .
;;  c               1   24[BANK0 ] char 
;;  ap              1   19[BANK0 ] PTR void [1]
;;		 -> ?_printf(2), 
;;  prec            1   18[BANK0 ] char 
;;  flag            1   16[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2   12[BANK0 ] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFE9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       2       0       0
;;      Locals:         0       9       0       0
;;      Temps:          0       2       0       0
;;      Totals:         0      13       0       0
;;Total ram usage:       13 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    5
;; This function calls:
;;		_putch
;;		___lwdiv
;;		___lwmod
;; This function is called by:
;;		_ShowTime
;;		_SetupTime
;; This function uses a non-reentrant model
;;
psect	text999
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\doprnt.c"
	line	461
	global	__size_of_printf
	__size_of_printf	equ	__end_of_printf-_printf
	
_printf:	
	opt	stack 5
; Regs used in _printf: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
;printf@f stored from wreg
	line	537
	movwf	(printf@f)
	
l3307:	
	movlw	(?_printf)&0ffh
	movwf	(printf@ap)
	line	540
	goto	l3359
	line	542
	
l3309:	
	movf	(printf@c),w
	xorlw	025h
	skipnz
	goto	u1081
	goto	u1080
u1081:
	goto	l3313
u1080:
	line	545
	
l3311:	
	movf	(printf@c),w
	fcall	_putch
	line	546
	goto	l3359
	line	552
	
l3313:	
	clrf	(printf@flag)
	line	638
	goto	l3323
	line	802
	
l3315:	
	movf	(printf@ap),w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(printf@c)
	
l3317:	
	incf	(printf@ap),f
	incf	(printf@ap),f
	line	812
	
l3319:	
	movf	(printf@c),w
	fcall	_putch
	line	813
	goto	l3359
	line	638
	
l3323:	
	movf	(printf@f),w
	incf	(printf@f),f
	movwf	fsr0
	fcall	stringdir
	movwf	(printf@c)
	; Switch size 1, requested type "space"
; Number of cases is 4, Range of values is 0 to 105
; switch strategies available:
; Name         Bytes Cycles
; simple_byte    13     7 (average)
; direct_byte   119    13 (fixed)
;	Chosen strategy is simple_byte

	xorlw	0^0	; case 0
	skipnz
	goto	l1154
	xorlw	99^0	; case 99
	skipnz
	goto	l3315
	xorlw	100^99	; case 100
	skipnz
	goto	l3325
	xorlw	105^100	; case 105
	skipnz
	goto	l3325
	goto	l3319

	line	1254
	
l3325:	
	movf	(printf@ap),w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(printf@_val)
	incf	fsr0,f
	movf	indf,w
	movwf	(printf@_val+1)
	
l3327:	
	incf	(printf@ap),f
	incf	(printf@ap),f
	line	1256
	
l3329:	
	btfss	(printf@_val+1),7
	goto	u1091
	goto	u1090
u1091:
	goto	l3335
u1090:
	line	1257
	
l3331:	
	movlw	(03h)
	iorwf	(printf@flag),f
	line	1258
	
l3333:	
	comf	(printf@_val),f
	comf	(printf@_val+1),f
	incf	(printf@_val),f
	skipnz
	incf	(printf@_val+1),f
	line	1300
	
l3335:	
	clrf	(printf@c)
	incf	(printf@c),f
	line	1301
	
l3339:	
	clrc
	rlf	(printf@c),w
	addlw	low((_dpowers-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(??_printf+0)+0
	fcall	stringdir
	movwf	(??_printf+0)+0+1
	movf	1+(??_printf+0)+0,w
	subwf	(printf@_val+1),w
	skipz
	goto	u1105
	movf	0+(??_printf+0)+0,w
	subwf	(printf@_val),w
u1105:
	skipnc
	goto	u1101
	goto	u1100
u1101:
	goto	l3343
u1100:
	goto	l3347
	line	1300
	
l3343:	
	incf	(printf@c),f
	
l3345:	
	movf	(printf@c),w
	xorlw	05h
	skipz
	goto	u1111
	goto	u1110
u1111:
	goto	l3339
u1110:
	line	1433
	
l3347:	
	movf	(printf@flag),w
	andlw	03h
	btfsc	status,2
	goto	u1121
	goto	u1120
u1121:
	goto	l3351
u1120:
	line	1434
	
l3349:	
	movlw	(02Dh)
	fcall	_putch
	line	1467
	
l3351:	
	movf	(printf@c),w
	movwf	(printf@prec)
	line	1469
	goto	l3357
	line	1484
	
l3353:	
	movlw	0Ah
	movwf	(?___lwmod)
	clrf	(?___lwmod+1)
	clrc
	rlf	(printf@prec),w
	addlw	low((_dpowers-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(?___lwdiv)
	fcall	stringdir
	movwf	(?___lwdiv+1)
	movf	(printf@_val+1),w
	movwf	1+(?___lwdiv)+02h
	movf	(printf@_val),w
	movwf	0+(?___lwdiv)+02h
	fcall	___lwdiv
	movf	(1+(?___lwdiv)),w
	movwf	1+(?___lwmod)+02h
	movf	(0+(?___lwdiv)),w
	movwf	0+(?___lwmod)+02h
	fcall	___lwmod
	movf	(0+(?___lwmod)),w
	addlw	030h
	movwf	(printf@c)
	line	1516
	
l3355:	
	movf	(printf@c),w
	fcall	_putch
	line	1469
	
l3357:	
	decf	(printf@prec),f
	incf	((printf@prec)),w
	skipz
	goto	u1131
	goto	u1130
u1131:
	goto	l3353
u1130:
	line	540
	
l3359:	
	movf	(printf@f),w
	incf	(printf@f),f
	movwf	fsr0
	fcall	stringdir
	movwf	(printf@c)
	movf	((printf@c)),f
	skipz
	goto	u1141
	goto	u1140
u1141:
	goto	l3309
u1140:
	line	1533
	
l1154:	
	return
	opt stack 0
GLOBAL	__end_of_printf
	__end_of_printf:
;; =============== function _printf ends ============

	signat	_printf,602
	global	_DCBconv
psect	text1000,local,class=CODE,delta=2
global __ptext1000
__ptext1000:

;; *************** function _DCBconv *****************
;; Defined at:
;;		line 177 in file "X:\Hi-tech\RTC_clock\main.c"
;; Parameters:    Size  Location     Type
;;  source          1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  source          1    6[BANK0 ] unsigned char 
;;  temp_maj        1    7[BANK0 ] unsigned char 
;;  temp_min        1    5[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       3       0       0
;;      Temps:          0       1       0       0
;;      Totals:         0       4       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    5
;; This function calls:
;;		___lbdiv
;;		___bmul
;; This function is called by:
;;		_SetMin
;;		_SetHour
;;		_SetSeconds
;; This function uses a non-reentrant model
;;
psect	text1000
	file	"X:\Hi-tech\RTC_clock\main.c"
	line	177
	global	__size_of_DCBconv
	__size_of_DCBconv	equ	__end_of_DCBconv-_DCBconv
	
_DCBconv:	
	opt	stack 5
; Regs used in _DCBconv: [wreg+status,2+status,0+pclath+cstack]
;DCBconv@source stored from wreg
	line	179
	movwf	(DCBconv@source)
	line	178
	
l3295:	
	line	179
;main.c: 179: unsigned char temp_maj=0;
	clrf	(DCBconv@temp_maj)
	line	180
	
l3297:	
;main.c: 180: temp_maj = source/10 ;
	movlw	(0Ah)
	movwf	(?___lbdiv)
	movf	(DCBconv@source),w
	fcall	___lbdiv
	movwf	(DCBconv@temp_maj)
	line	181
	
l3299:	
;main.c: 181: temp_min = source - temp_maj*10;
	movlw	(0F6h)
	movwf	(?___bmul)
	movf	(DCBconv@temp_maj),w
	fcall	___bmul
	movwf	(??_DCBconv+0)+0
	movf	(DCBconv@source),w
	addwf	0+(??_DCBconv+0)+0,w
	movwf	(DCBconv@temp_min)
	line	182
	
l3301:	
;main.c: 182: temp_maj <<= 4;
swapf	(DCBconv@temp_maj),f
	movlw	240
	andwf	(DCBconv@temp_maj),f

	line	184
	
l3303:	
;main.c: 184: return temp_maj+temp_min;
	movf	(DCBconv@temp_min),w
	addwf	(DCBconv@temp_maj),w
	line	186
	
l383:	
	return
	opt stack 0
GLOBAL	__end_of_DCBconv
	__end_of_DCBconv:
;; =============== function _DCBconv ends ============

	signat	_DCBconv,4217
	global	___lbdiv
psect	text1001,local,class=CODE,delta=2
global __ptext1001
__ptext1001:

;; *************** function ___lbdiv *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lbdiv.c"
;; Parameters:    Size  Location     Type
;;  dividend        1    wreg     unsigned char 
;;  divisor         1    0[BANK0 ] unsigned char 
;; Auto vars:     Size  Location     Type
;;  dividend        1    1[BANK0 ] unsigned char 
;;  quotient        1    3[BANK0 ] unsigned char 
;;  counter         1    2[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       1       0       0
;;      Locals:         0       3       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       4       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    4
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_DCBconv
;; This function uses a non-reentrant model
;;
psect	text1001
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lbdiv.c"
	line	5
	global	__size_of___lbdiv
	__size_of___lbdiv	equ	__end_of___lbdiv-___lbdiv
	
___lbdiv:	
	opt	stack 4
; Regs used in ___lbdiv: [wreg+status,2+status,0]
;___lbdiv@dividend stored from wreg
	line	9
	movwf	(___lbdiv@dividend)
	
l3271:	
	clrf	(___lbdiv@quotient)
	line	10
	
l3273:	
	movf	(___lbdiv@divisor),w
	skipz
	goto	u1040
	goto	l3291
u1040:
	line	11
	
l3275:	
	clrf	(___lbdiv@counter)
	incf	(___lbdiv@counter),f
	line	12
	goto	l3279
	
l1191:	
	line	13
	clrc
	rlf	(___lbdiv@divisor),f
	line	14
	
l3277:	
	incf	(___lbdiv@counter),f
	line	12
	
l3279:	
	btfss	(___lbdiv@divisor),(7)&7
	goto	u1051
	goto	u1050
u1051:
	goto	l1191
u1050:
	line	16
	
l1193:	
	line	17
	clrc
	rlf	(___lbdiv@quotient),f
	line	18
	
l3281:	
	movf	(___lbdiv@divisor),w
	subwf	(___lbdiv@dividend),w
	skipc
	goto	u1061
	goto	u1060
u1061:
	goto	l3287
u1060:
	line	19
	
l3283:	
	movf	(___lbdiv@divisor),w
	subwf	(___lbdiv@dividend),f
	line	20
	
l3285:	
	bsf	(___lbdiv@quotient)+(0/8),(0)&7
	line	22
	
l3287:	
	clrc
	rrf	(___lbdiv@divisor),f
	line	23
	
l3289:	
	decfsz	(___lbdiv@counter),f
	goto	u1071
	goto	u1070
u1071:
	goto	l1193
u1070:
	line	25
	
l3291:	
	movf	(___lbdiv@quotient),w
	line	26
	
l1196:	
	return
	opt stack 0
GLOBAL	__end_of___lbdiv
	__end_of___lbdiv:
;; =============== function ___lbdiv ends ============

	signat	___lbdiv,8313
	global	___lwmod
psect	text1002,local,class=CODE,delta=2
global __ptext1002
__ptext1002:

;; *************** function ___lwmod *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lwmod.c"
;; Parameters:    Size  Location     Type
;;  divisor         2    7[BANK0 ] unsigned int 
;;  dividend        2    9[BANK0 ] unsigned int 
;; Auto vars:     Size  Location     Type
;;  counter         1   11[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    7[BANK0 ] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       4       0       0
;;      Locals:         0       1       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       5       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    4
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_printf
;; This function uses a non-reentrant model
;;
psect	text1002
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lwmod.c"
	line	5
	global	__size_of___lwmod
	__size_of___lwmod	equ	__end_of___lwmod-___lwmod
	
___lwmod:	
	opt	stack 4
; Regs used in ___lwmod: [wreg+status,2+status,0]
	line	8
	
l3251:	
	movf	(___lwmod@divisor+1),w
	iorwf	(___lwmod@divisor),w
	skipnz
	goto	u1001
	goto	u1000
u1001:
	goto	l3267
u1000:
	line	9
	
l3253:	
	clrf	(___lwmod@counter)
	incf	(___lwmod@counter),f
	line	10
	goto	l3257
	line	11
	
l3255:	
	clrc
	rlf	(___lwmod@divisor),f
	rlf	(___lwmod@divisor+1),f
	line	12
	incf	(___lwmod@counter),f
	line	10
	
l3257:	
	btfss	(___lwmod@divisor+1),(15)&7
	goto	u1011
	goto	u1010
u1011:
	goto	l3255
u1010:
	line	15
	
l3259:	
	movf	(___lwmod@divisor+1),w
	subwf	(___lwmod@dividend+1),w
	skipz
	goto	u1025
	movf	(___lwmod@divisor),w
	subwf	(___lwmod@dividend),w
u1025:
	skipc
	goto	u1021
	goto	u1020
u1021:
	goto	l3263
u1020:
	line	16
	
l3261:	
	movf	(___lwmod@divisor),w
	subwf	(___lwmod@dividend),f
	movf	(___lwmod@divisor+1),w
	skipc
	decf	(___lwmod@dividend+1),f
	subwf	(___lwmod@dividend+1),f
	line	17
	
l3263:	
	clrc
	rrf	(___lwmod@divisor+1),f
	rrf	(___lwmod@divisor),f
	line	18
	
l3265:	
	decfsz	(___lwmod@counter),f
	goto	u1031
	goto	u1030
u1031:
	goto	l3259
u1030:
	line	20
	
l3267:	
	movf	(___lwmod@dividend+1),w
	movwf	(?___lwmod+1)
	movf	(___lwmod@dividend),w
	movwf	(?___lwmod)
	line	21
	
l1186:	
	return
	opt stack 0
GLOBAL	__end_of___lwmod
	__end_of___lwmod:
;; =============== function ___lwmod ends ============

	signat	___lwmod,8314
	global	___lwdiv
psect	text1003,local,class=CODE,delta=2
global __ptext1003
__ptext1003:

;; *************** function ___lwdiv *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lwdiv.c"
;; Parameters:    Size  Location     Type
;;  divisor         2    0[BANK0 ] unsigned int 
;;  dividend        2    2[BANK0 ] unsigned int 
;; Auto vars:     Size  Location     Type
;;  quotient        2    4[BANK0 ] unsigned int 
;;  counter         1    6[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       4       0       0
;;      Locals:         0       3       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       7       0       0
;;Total ram usage:        7 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    4
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_printf
;; This function uses a non-reentrant model
;;
psect	text1003
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lwdiv.c"
	line	5
	global	__size_of___lwdiv
	__size_of___lwdiv	equ	__end_of___lwdiv-___lwdiv
	
___lwdiv:	
	opt	stack 4
; Regs used in ___lwdiv: [wreg+status,2+status,0]
	line	9
	
l2583:	
	clrf	(___lwdiv@quotient)
	clrf	(___lwdiv@quotient+1)
	line	10
	
l2585:	
	movf	(___lwdiv@divisor+1),w
	iorwf	(___lwdiv@divisor),w
	skipnz
	goto	u571
	goto	u570
u571:
	goto	l2605
u570:
	line	11
	
l2587:	
	clrf	(___lwdiv@counter)
	incf	(___lwdiv@counter),f
	line	12
	goto	l2591
	line	13
	
l2589:	
	clrc
	rlf	(___lwdiv@divisor),f
	rlf	(___lwdiv@divisor+1),f
	line	14
	incf	(___lwdiv@counter),f
	line	12
	
l2591:	
	btfss	(___lwdiv@divisor+1),(15)&7
	goto	u581
	goto	u580
u581:
	goto	l2589
u580:
	line	17
	
l2593:	
	clrc
	rlf	(___lwdiv@quotient),f
	rlf	(___lwdiv@quotient+1),f
	line	18
	
l2595:	
	movf	(___lwdiv@divisor+1),w
	subwf	(___lwdiv@dividend+1),w
	skipz
	goto	u595
	movf	(___lwdiv@divisor),w
	subwf	(___lwdiv@dividend),w
u595:
	skipc
	goto	u591
	goto	u590
u591:
	goto	l2601
u590:
	line	19
	
l2597:	
	movf	(___lwdiv@divisor),w
	subwf	(___lwdiv@dividend),f
	movf	(___lwdiv@divisor+1),w
	skipc
	decf	(___lwdiv@dividend+1),f
	subwf	(___lwdiv@dividend+1),f
	line	20
	
l2599:	
	bsf	(___lwdiv@quotient)+(0/8),(0)&7
	line	22
	
l2601:	
	clrc
	rrf	(___lwdiv@divisor+1),f
	rrf	(___lwdiv@divisor),f
	line	23
	
l2603:	
	decfsz	(___lwdiv@counter),f
	goto	u601
	goto	u600
u601:
	goto	l2593
u600:
	line	25
	
l2605:	
	movf	(___lwdiv@quotient+1),w
	movwf	(?___lwdiv+1)
	movf	(___lwdiv@quotient),w
	movwf	(?___lwdiv)
	line	26
	
l1176:	
	return
	opt stack 0
GLOBAL	__end_of___lwdiv
	__end_of___lwdiv:
;; =============== function ___lwdiv ends ============

	signat	___lwdiv,8314
	global	___bmul
psect	text1004,local,class=CODE,delta=2
global __ptext1004
__ptext1004:

;; *************** function ___bmul *****************
;; Defined at:
;;		line 3 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\sources\bmul.c"
;; Parameters:    Size  Location     Type
;;  multiplier      1    wreg     unsigned char 
;;  multiplicand    1    0[BANK0 ] unsigned char 
;; Auto vars:     Size  Location     Type
;;  multiplier      1    2[BANK0 ] unsigned char 
;;  product         1    1[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       1       0       0
;;      Locals:         0       2       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       3       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    4
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_DCBconv
;;		_SetupTime
;; This function uses a non-reentrant model
;;
psect	text1004
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\bmul.c"
	line	3
	global	__size_of___bmul
	__size_of___bmul	equ	__end_of___bmul-___bmul
	
___bmul:	
	opt	stack 4
; Regs used in ___bmul: [wreg+status,2+status,0]
;___bmul@multiplier stored from wreg
	movwf	(___bmul@multiplier)
	line	4
	
l3235:	
	clrf	(___bmul@product)
	line	7
	
l3237:	
	btfss	(___bmul@multiplier),(0)&7
	goto	u981
	goto	u980
u981:
	goto	l3241
u980:
	line	8
	
l3239:	
	movf	(___bmul@multiplicand),w
	addwf	(___bmul@product),f
	line	9
	
l3241:	
	clrc
	rlf	(___bmul@multiplicand),f
	line	10
	
l3243:	
	clrc
	rrf	(___bmul@multiplier),f
	line	11
	
l3245:	
	movf	(___bmul@multiplier),f
	skipz
	goto	u991
	goto	u990
u991:
	goto	l3237
u990:
	line	12
	
l3247:	
	movf	(___bmul@product),w
	line	13
	
l1160:	
	return
	opt stack 0
GLOBAL	__end_of___bmul
	__end_of___bmul:
;; =============== function ___bmul ends ============

	signat	___bmul,8313
	global	_putch
psect	text1005,local,class=CODE,delta=2
global __ptext1005
__ptext1005:

;; *************** function _putch *****************
;; Defined at:
;;		line 7 in file "X:\Hi-tech\RTC_clock\usart.c"
;; Parameters:    Size  Location     Type
;;  byte            1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  byte            1    0[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       1       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       1       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    4
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_printf
;; This function uses a non-reentrant model
;;
psect	text1005
	file	"X:\Hi-tech\RTC_clock\usart.c"
	line	7
	global	__size_of_putch
	__size_of_putch	equ	__end_of_putch-_putch
	
_putch:	
	opt	stack 4
; Regs used in _putch: [wreg]
;putch@byte stored from wreg
	movwf	(putch@byte)
	line	9
	
l3231:	
	line	10
;usart.c: 9: while(!TXIF)
	
l759:	
	line	9
	btfss	(100/8),(100)&7
	goto	u971
	goto	u970
u971:
	goto	l759
u970:
	line	11
	
l3233:	
;usart.c: 11: TXREG = byte;
	movf	(putch@byte),w
	movwf	(25)	;volatile
	line	12
	
l762:	
	return
	opt stack 0
GLOBAL	__end_of_putch
	__end_of_putch:
;; =============== function _putch ends ============

	signat	_putch,4216
	global	_BCDconv
psect	text1006,local,class=CODE,delta=2
global __ptext1006
__ptext1006:

;; *************** function _BCDconv *****************
;; Defined at:
;;		line 164 in file "X:\Hi-tech\RTC_clock\main.c"
;; Parameters:    Size  Location     Type
;;  source          1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  source          1    2[BANK0 ] unsigned char 
;;  temp_maj        1    1[BANK0 ] unsigned char 
;;  temp_min        1    0[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       3       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       3       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    4
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_ReadMin
;;		_ReadHour
;;		_ReadSeconds
;; This function uses a non-reentrant model
;;
psect	text1006
	file	"X:\Hi-tech\RTC_clock\main.c"
	line	164
	global	__size_of_BCDconv
	__size_of_BCDconv	equ	__end_of_BCDconv-_BCDconv
	
_BCDconv:	
	opt	stack 4
; Regs used in _BCDconv: [wreg+status,2+status,0]
;BCDconv@source stored from wreg
	line	166
	movwf	(BCDconv@source)
	line	165
	
l3217:	
	line	166
;main.c: 166: unsigned char temp_maj=0;
	clrf	(BCDconv@temp_maj)
	line	167
	
l3219:	
;main.c: 167: temp_min = source&15;
	movf	(BCDconv@source),w
	movwf	(BCDconv@temp_min)
	
l3221:	
	movlw	(0Fh)
	andwf	(BCDconv@temp_min),f
	line	168
	
l3223:	
;main.c: 168: temp_maj = source >> 4;
	swapf	(BCDconv@source),w
	andlw	(0ffh shr 4) & 0ffh
	movwf	(BCDconv@temp_maj)
	line	171
	
l3225:	
;main.c: 171: temp_maj *= 10;
	clrc
	rlf	(BCDconv@temp_maj),w
	addwf	(BCDconv@temp_maj),f
	addwf	(BCDconv@temp_maj),f
	clrc
	rlf	(BCDconv@temp_maj),f
	line	174
	
l3227:	
;main.c: 174: return temp_maj+temp_min;
	movf	(BCDconv@temp_min),w
	addwf	(BCDconv@temp_maj),w
	line	175
	
l380:	
	return
	opt stack 0
GLOBAL	__end_of_BCDconv
	__end_of_BCDconv:
;; =============== function _BCDconv ends ============

	signat	_BCDconv,4217
	global	_getch
psect	text1007,local,class=CODE,delta=2
global __ptext1007
__ptext1007:

;; *************** function _getch *****************
;; Defined at:
;;		line 15 in file "X:\Hi-tech\RTC_clock\usart.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    4
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;;		_SetupTime
;; This function uses a non-reentrant model
;;
psect	text1007
	file	"X:\Hi-tech\RTC_clock\usart.c"
	line	15
	global	__size_of_getch
	__size_of_getch	equ	__end_of_getch-_getch
	
_getch:	
	opt	stack 6
; Regs used in _getch: [wreg]
	line	17
	
l2531:	
	line	18
;usart.c: 17: while(!RCIF)
	
l765:	
	line	17
	btfss	(101/8),(101)&7
	goto	u521
	goto	u520
u521:
	goto	l765
u520:
	line	19
	
l2533:	
;usart.c: 19: return RCREG;
	movf	(26),w	;volatile
	line	20
	
l768:	
	return
	opt stack 0
GLOBAL	__end_of_getch
	__end_of_getch:
;; =============== function _getch ends ============

	signat	_getch,89
	global	_isr
psect	text1008,local,class=CODE,delta=2
global __ptext1008
__ptext1008:

;; *************** function _isr *****************
;; Defined at:
;;		line 152 in file "X:\Hi-tech\RTC_clock\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  2  375[COMMON] int 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 60/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          2       0       0       0
;;      Totals:         2       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		i1_ReadHour
;;		i1_ReadMin
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	text1008
	file	"X:\Hi-tech\RTC_clock\main.c"
	line	152
	global	__size_of_isr
	__size_of_isr	equ	__end_of_isr-_isr
	
_isr:	
	opt	stack 2
; Regs used in _isr: [wreg+status,2+status,0+pclath+cstack]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+0
	movwf	saved_w
	movf	status,w
	movwf	(??_isr+0)
	movf	pclath,w
	movwf	(??_isr+1)
	ljmp	_isr
psect	text1008
	line	154
	
i1l2537:	
;main.c: 154: if (TMR1IF) {
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(96/8),(96)&7
	goto	u53_21
	goto	u53_20
u53_21:
	goto	i1l377
u53_20:
	line	155
	
i1l2539:	
;main.c: 155: TMR1H = 0b11111110; TMR1L = 0x00;
	movlw	(0FEh)
	movwf	(15)	;volatile
	
i1l2541:	
	clrf	(14)	;volatile
	line	156
	
i1l2543:	
;main.c: 156: temphour = ReadHour();
	fcall	i1_ReadHour
	movwf	(_temphour)	;volatile
	line	157
	
i1l2545:	
;main.c: 157: tempmin = ReadMin();
	fcall	i1_ReadMin
	movwf	(_tempmin)	;volatile
	line	158
	
i1l2547:	
;main.c: 158: TMR1IF = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	(96/8),(96)&7
	line	161
	
i1l377:	
	movf	(??_isr+1),w
	movwf	pclath
	movf	(??_isr+0),w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_isr
	__end_of_isr:
;; =============== function _isr ends ============

	signat	_isr,90
	global	i1_ReadHour
psect	text1009,local,class=CODE,delta=2
global __ptext1009
__ptext1009:

;; *************** function i1_ReadHour *****************
;; Defined at:
;;		line 217 in file "X:\Hi-tech\RTC_clock\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  ReadHour        1    3[COMMON] unsigned char 
;;  ReadHour        1    0        unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         1       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		i1_i2c_start
;;		i1_i2c_tx
;;		i1_i2c_rx
;;		i1_i2c_stop
;;		i1_BCDconv
;; This function is called by:
;;		_isr
;; This function uses a non-reentrant model
;;
psect	text1009
	file	"X:\Hi-tech\RTC_clock\main.c"
	line	217
	global	__size_ofi1_ReadHour
	__size_ofi1_ReadHour	equ	__end_ofi1_ReadHour-i1_ReadHour
	
i1_ReadHour:	
	opt	stack 1
; Regs used in i1_ReadHour: [wreg+status,2+status,0+pclath+cstack]
	line	218
	
i1l2687:	
;main.c: 219: unsigned char temp2 = 0;
	clrf	(i1ReadHour@temp)
	line	220
	
i1l2689:	
;main.c: 220: i2c_start();
	fcall	i1_i2c_start
	line	221
	
i1l2691:	
;main.c: 221: i2c_tx(0b11010000);
	movlw	(0D0h)
	fcall	i1_i2c_tx
	line	222
	
i1l2693:	
;main.c: 222: i2c_tx(0x02);
	movlw	(02h)
	fcall	i1_i2c_tx
	line	223
	
i1l2695:	
;main.c: 223: i2c_start();
	fcall	i1_i2c_start
	line	224
	
i1l2697:	
;main.c: 224: i2c_tx(0b11010001);
	movlw	(0D1h)
	fcall	i1_i2c_tx
	line	225
	
i1l2699:	
;main.c: 225: temp = i2c_rx(0);
	movlw	(0)
	fcall	i1_i2c_rx
	movwf	(i1ReadHour@temp)
	line	226
	
i1l2701:	
;main.c: 226: i2c_stop();
	fcall	i1_i2c_stop
	line	233
	
i1l2703:	
;main.c: 233: return BCDconv(temp);
	movf	(i1ReadHour@temp),w
	fcall	i1_BCDconv
	line	234
	
i1l395:	
	return
	opt stack 0
GLOBAL	__end_ofi1_ReadHour
	__end_ofi1_ReadHour:
;; =============== function i1_ReadHour ends ============

	signat	i1_ReadHour,89
	global	i1_ReadMin
psect	text1010,local,class=CODE,delta=2
global __ptext1010
__ptext1010:

;; *************** function i1_ReadMin *****************
;; Defined at:
;;		line 196 in file "X:\Hi-tech\RTC_clock\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  ReadMin         1    3[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/20
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         1       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		i1_i2c_start
;;		i1_i2c_tx
;;		i1_i2c_rx
;;		i1_i2c_stop
;;		i1_BCDconv
;; This function is called by:
;;		_isr
;; This function uses a non-reentrant model
;;
psect	text1010
	file	"X:\Hi-tech\RTC_clock\main.c"
	line	196
	global	__size_ofi1_ReadMin
	__size_ofi1_ReadMin	equ	__end_ofi1_ReadMin-i1_ReadMin
	
i1_ReadMin:	
	opt	stack 1
; Regs used in i1_ReadMin: [wreg+status,2+status,0+pclath+cstack]
	line	197
	
i1l2667:	
;main.c: 197: unsigned char temp = 0;
	clrf	(i1ReadMin@temp)
	line	198
	
i1l2669:	
;main.c: 198: i2c_start();
	fcall	i1_i2c_start
	line	199
	
i1l2671:	
;main.c: 199: i2c_tx(0b11010000);
	movlw	(0D0h)
	fcall	i1_i2c_tx
	line	200
	
i1l2673:	
;main.c: 200: i2c_tx(0x01);
	movlw	(01h)
	fcall	i1_i2c_tx
	line	201
	
i1l2675:	
;main.c: 201: i2c_start();
	fcall	i1_i2c_start
	line	202
	
i1l2677:	
;main.c: 202: i2c_tx(0b11010001);
	movlw	(0D1h)
	fcall	i1_i2c_tx
	line	203
	
i1l2679:	
;main.c: 203: temp = i2c_rx(0);
	movlw	(0)
	fcall	i1_i2c_rx
	movwf	(i1ReadMin@temp)
	line	204
	
i1l2681:	
;main.c: 204: i2c_stop();
	fcall	i1_i2c_stop
	line	206
	
i1l2683:	
;main.c: 206: return BCDconv(temp);
	movf	(i1ReadMin@temp),w
	fcall	i1_BCDconv
	line	207
	
i1l389:	
	return
	opt stack 0
GLOBAL	__end_ofi1_ReadMin
	__end_ofi1_ReadMin:
;; =============== function i1_ReadMin ends ============

	signat	i1_ReadMin,89
	global	i1_i2c_rx
psect	text1011,local,class=CODE,delta=2
global __ptext1011
__ptext1011:

;; *************** function i1_i2c_rx *****************
;; Defined at:
;;		line 58 in file "X:\Hi-tech\RTC_clock\softi2c.c"
;; Parameters:    Size  Location     Type
;;  i2c_rx          1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  i2c_rx          1    0[COMMON] unsigned char 
;;  i2c_rx          1    2[COMMON] unsigned char 
;;  i2c_rx          1    1[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/20
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         3       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         3       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		i1_i2c_delay
;; This function is called by:
;;		i1_ReadMin
;;		i1_ReadHour
;; This function uses a non-reentrant model
;;
psect	text1011
	file	"X:\Hi-tech\RTC_clock\softi2c.c"
	line	58
	global	__size_ofi1_i2c_rx
	__size_ofi1_i2c_rx	equ	__end_ofi1_i2c_rx-i1_i2c_rx
	
i1_i2c_rx:	
	opt	stack 0
; Regs used in i1_i2c_rx: [wreg+status,2+status,0+pclath+cstack]
;i1i2c_rx@ack stored from wreg
	movwf	(i1i2c_rx@ack)
	line	59
	
i1l2763:	
;softi2c.c: 59: unsigned char x,d=0;
	clrf	(i1i2c_rx@d)
	line	60
	
i1l2765:	
;softi2c.c: 60: TRISA1 = 1;
	bsf	(1065/8)^080h,(1065)&7
	line	61
;softi2c.c: 61: for (x=0; x<8;x++) {
	clrf	(i1i2c_rx@x)
	
i1l1116:	
	line	62
;softi2c.c: 62: d <<= 1;
	clrc
	rlf	(i1i2c_rx@d),f
	line	64
	
i1l2771:	
;softi2c.c: 64: i2c_delay();
	fcall	i1_i2c_delay
	line	65
	
i1l2773:	
;softi2c.c: 65: TRISA0 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1064/8)^080h,(1064)&7
	line	66
	
i1l2775:	
;softi2c.c: 66: } while (RA0==0);
	bcf	status, 5	;RP0=0, select bank0
	btfss	(40/8),(40)&7
	goto	u71_21
	goto	u71_20
u71_21:
	goto	i1l2771
u71_20:
	line	67
	
i1l2777:	
;softi2c.c: 67: i2c_delay();
	fcall	i1_i2c_delay
	line	68
	
i1l2779:	
;softi2c.c: 68: if (RA1) d |= 1;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(41/8),(41)&7
	goto	u72_21
	goto	u72_20
u72_21:
	goto	i1l1120
u72_20:
	
i1l2781:	
	bsf	(i1i2c_rx@d)+(0/8),(0)&7
	
i1l1120:	
	line	69
;softi2c.c: 69: TRISA0 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	(1064/8)^080h,(1064)&7
	line	61
	
i1l2783:	
	incf	(i1i2c_rx@x),f
	
i1l2785:	
	movlw	(08h)
	subwf	(i1i2c_rx@x),w
	skipc
	goto	u73_21
	goto	u73_20
u73_21:
	goto	i1l1116
u73_20:
	line	71
	
i1l2787:	
;softi2c.c: 70: }
;softi2c.c: 71: i2c_delay();
	fcall	i1_i2c_delay
	line	72
	
i1l2789:	
;softi2c.c: 72: if (ack) TRISA1 = 0;
	movf	(i1i2c_rx@ack),w
	skipz
	goto	u74_20
	goto	i1l1121
u74_20:
	
i1l2791:	
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1065/8)^080h,(1065)&7
	goto	i1l1122
	line	73
	
i1l1121:	
;softi2c.c: 73: else TRISA1 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1065/8)^080h,(1065)&7
	
i1l1122:	
	line	74
;softi2c.c: 74: TRISA0 = 1;
	bsf	(1064/8)^080h,(1064)&7
	line	75
	
i1l2793:	
;softi2c.c: 75: i2c_delay();
	fcall	i1_i2c_delay
	line	76
	
i1l2795:	
;softi2c.c: 76: TRISA0 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1064/8)^080h,(1064)&7
	line	78
	
i1l2797:	
;softi2c.c: 78: return d;
	movf	(i1i2c_rx@d),w
	line	80
	
i1l1123:	
	return
	opt stack 0
GLOBAL	__end_ofi1_i2c_rx
	__end_ofi1_i2c_rx:
;; =============== function i1_i2c_rx ends ============

	signat	i1_i2c_rx,89
	global	i1_i2c_tx
psect	text1012,local,class=CODE,delta=2
global __ptext1012
__ptext1012:

;; *************** function i1_i2c_tx *****************
;; Defined at:
;;		line 32 in file "X:\Hi-tech\RTC_clock\softi2c.c"
;; Parameters:    Size  Location     Type
;;  i2c_tx          1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  i2c_tx          1    0[COMMON] unsigned char 
;;  i2c_tx          1    1[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/20
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         2       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         2       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		i1_i2c_delay
;; This function is called by:
;;		i1_ReadMin
;;		i1_ReadHour
;; This function uses a non-reentrant model
;;
psect	text1012
	file	"X:\Hi-tech\RTC_clock\softi2c.c"
	line	32
	global	__size_ofi1_i2c_tx
	__size_ofi1_i2c_tx	equ	__end_ofi1_i2c_tx-i1_i2c_tx
	
i1_i2c_tx:	
	opt	stack 0
; Regs used in i1_i2c_tx: [wreg+status,2+status,0+pclath+cstack]
;i1i2c_tx@d stored from wreg
	line	35
	movwf	(i1i2c_tx@d)
	
i1l2727:	
;softi2c.c: 33: char x;
;softi2c.c: 34: static bit b;
;softi2c.c: 35: for (x=0; x<8; x++) {
	clrf	(i1i2c_tx@x)
	line	36
	
i1l2733:	
;softi2c.c: 36: i2c_delay();
	fcall	i1_i2c_delay
	line	37
	
i1l2735:	
;softi2c.c: 37: if (d&0x80) TRISA1 = 1;
	btfss	(i1i2c_tx@d),(7)&7
	goto	u69_21
	goto	u69_20
u69_21:
	goto	i1l1111
u69_20:
	
i1l2737:	
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1065/8)^080h,(1065)&7
	goto	i1l2739
	line	38
	
i1l1111:	
;softi2c.c: 38: else TRISA1 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1065/8)^080h,(1065)&7
	line	39
	
i1l2739:	
;softi2c.c: 39: i2c_delay();
	fcall	i1_i2c_delay
	line	40
	
i1l2741:	
;softi2c.c: 40: TRISA0 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1064/8)^080h,(1064)&7
	line	41
	
i1l2743:	
;softi2c.c: 41: d <<= 1;
	clrc
	rlf	(i1i2c_tx@d),f
	line	42
;softi2c.c: 42: i2c_delay();
	fcall	i1_i2c_delay
	line	43
	
i1l2745:	
;softi2c.c: 43: TRISA0 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1064/8)^080h,(1064)&7
	line	35
	
i1l2747:	
	incf	(i1i2c_tx@x),f
	
i1l2749:	
	movlw	(08h)
	subwf	(i1i2c_tx@x),w
	skipc
	goto	u70_21
	goto	u70_20
u70_21:
	goto	i1l2733
u70_20:
	line	45
	
i1l2751:	
;softi2c.c: 44: }
;softi2c.c: 45: i2c_delay();
	fcall	i1_i2c_delay
	line	46
;softi2c.c: 46: i2c_delay();
	fcall	i1_i2c_delay
	line	47
	
i1l2753:	
;softi2c.c: 47: TRISA1 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1065/8)^080h,(1065)&7
	line	48
	
i1l2755:	
;softi2c.c: 48: TRISA0 = 1;
	bsf	(1064/8)^080h,(1064)&7
	line	49
;softi2c.c: 49: i2c_delay();
	fcall	i1_i2c_delay
	line	51
	
i1l2757:	
;softi2c.c: 51: b = RA1;
	line	52
	
i1l2759:	
;softi2c.c: 52: TRISA0 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1064/8)^080h,(1064)&7
	line	55
	
i1l1113:	
	return
	opt stack 0
GLOBAL	__end_ofi1_i2c_tx
	__end_ofi1_i2c_tx:
;; =============== function i1_i2c_tx ends ============

	signat	i1_i2c_tx,88
	global	i1_i2c_stop
psect	text1013,local,class=CODE,delta=2
global __ptext1013
__ptext1013:

;; *************** function i1_i2c_stop *****************
;; Defined at:
;;		line 20 in file "X:\Hi-tech\RTC_clock\softi2c.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/20
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		i1_i2c_delay
;; This function is called by:
;;		i1_ReadMin
;;		i1_ReadHour
;; This function uses a non-reentrant model
;;
psect	text1013
	file	"X:\Hi-tech\RTC_clock\softi2c.c"
	line	20
	global	__size_ofi1_i2c_stop
	__size_ofi1_i2c_stop	equ	__end_ofi1_i2c_stop-i1_i2c_stop
	
i1_i2c_stop:	
	opt	stack 0
; Regs used in i1_i2c_stop: [status,2+status,0+pclath+cstack]
	line	21
	
i1l2717:	
;softi2c.c: 21: i2c_delay();
	fcall	i1_i2c_delay
	line	22
	
i1l2719:	
;softi2c.c: 22: TRISA1 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1065/8)^080h,(1065)&7
	line	23
	
i1l2721:	
;softi2c.c: 23: TRISA0 = 0;
	bcf	(1064/8)^080h,(1064)&7
	line	24
;softi2c.c: 24: i2c_delay();
	fcall	i1_i2c_delay
	line	25
	
i1l2723:	
;softi2c.c: 25: TRISA0 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1064/8)^080h,(1064)&7
	line	26
;softi2c.c: 26: i2c_delay();
	fcall	i1_i2c_delay
	line	27
	
i1l2725:	
;softi2c.c: 27: TRISA1 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1065/8)^080h,(1065)&7
	line	29
	
i1l1104:	
	return
	opt stack 0
GLOBAL	__end_ofi1_i2c_stop
	__end_ofi1_i2c_stop:
;; =============== function i1_i2c_stop ends ============

	signat	i1_i2c_stop,88
	global	i1_i2c_start
psect	text1014,local,class=CODE,delta=2
global __ptext1014
__ptext1014:

;; *************** function i1_i2c_start *****************
;; Defined at:
;;		line 10 in file "X:\Hi-tech\RTC_clock\softi2c.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 40/0
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		i1_i2c_delay
;; This function is called by:
;;		i1_ReadMin
;;		i1_ReadHour
;; This function uses a non-reentrant model
;;
psect	text1014
	file	"X:\Hi-tech\RTC_clock\softi2c.c"
	line	10
	global	__size_ofi1_i2c_start
	__size_ofi1_i2c_start	equ	__end_ofi1_i2c_start-i1_i2c_start
	
i1_i2c_start:	
	opt	stack 0
; Regs used in i1_i2c_start: [status,2+status,0+pclath+cstack]
	line	11
	
i1l2709:	
;softi2c.c: 11: TRISA0 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bsf	(1064/8)^080h,(1064)&7
	line	12
;softi2c.c: 12: TRISA1 = 1;
	bsf	(1065/8)^080h,(1065)&7
	line	13
	
i1l2711:	
;softi2c.c: 13: i2c_delay();
	fcall	i1_i2c_delay
	line	14
	
i1l2713:	
;softi2c.c: 14: TRISA1 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1065/8)^080h,(1065)&7
	line	15
;softi2c.c: 15: i2c_delay();
	fcall	i1_i2c_delay
	line	16
	
i1l2715:	
;softi2c.c: 16: TRISA0 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1064/8)^080h,(1064)&7
	line	17
	
i1l1101:	
	return
	opt stack 0
GLOBAL	__end_ofi1_i2c_start
	__end_ofi1_i2c_start:
;; =============== function i1_i2c_start ends ============

	signat	i1_i2c_start,88
	global	i1_i2c_delay
psect	text1015,local,class=CODE,delta=2
global __ptext1015
__ptext1015:

;; *************** function i1_i2c_delay *****************
;; Defined at:
;;		line 5 in file "X:\Hi-tech\RTC_clock\softi2c.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		None
;; Tracked objects:
;;		On entry : 0/20
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		i1_i2c_start
;;		i1_i2c_stop
;;		i1_i2c_tx
;;		i1_i2c_rx
;; This function uses a non-reentrant model
;;
psect	text1015
	file	"X:\Hi-tech\RTC_clock\softi2c.c"
	line	5
	global	__size_ofi1_i2c_delay
	__size_ofi1_i2c_delay	equ	__end_ofi1_i2c_delay-i1_i2c_delay
	
i1_i2c_delay:	
;; hardware stack exceeded
	opt	stack 0
; Regs used in i1_i2c_delay: []
	line	6
	
i1l2707:	
;softi2c.c: 6: _delay((unsigned long)((5)*(4000000/4000000.0)));
		opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on
	opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on
	opt asmopt_off
	clrwdt
	opt asmopt_on

	line	7
	
i1l1098:	
	return
	opt stack 0
GLOBAL	__end_ofi1_i2c_delay
	__end_ofi1_i2c_delay:
;; =============== function i1_i2c_delay ends ============

	signat	i1_i2c_delay,88
	global	i1_BCDconv
psect	text1016,local,class=CODE,delta=2
global __ptext1016
__ptext1016:

;; *************** function i1_BCDconv *****************
;; Defined at:
;;		line 164 in file "X:\Hi-tech\RTC_clock\main.c"
;; Parameters:    Size  Location     Type
;;  BCDconv         1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  BCDconv         1    2[COMMON] unsigned char 
;;  BCDconv         1    1[COMMON] unsigned char 
;;  BCDconv         1    0[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/20
;;		On exit  : 60/20
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         3       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         3       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		i1_ReadMin
;;		i1_ReadHour
;; This function uses a non-reentrant model
;;
psect	text1016
	file	"X:\Hi-tech\RTC_clock\main.c"
	line	164
	global	__size_ofi1_BCDconv
	__size_ofi1_BCDconv	equ	__end_ofi1_BCDconv-i1_BCDconv
	
i1_BCDconv:	
	opt	stack 0
; Regs used in i1_BCDconv: [wreg+status,2+status,0]
;i1BCDconv@source stored from wreg
	line	166
	movwf	(i1BCDconv@source)
	line	165
	
i1l2653:	
	line	166
;main.c: 166: unsigned char temp_maj=0;
	clrf	(i1BCDconv@temp_maj)
	line	167
	
i1l2655:	
;main.c: 167: temp_min = source&15;
	movf	(i1BCDconv@source),w
	movwf	(i1BCDconv@temp_min)
	
i1l2657:	
	movlw	(0Fh)
	andwf	(i1BCDconv@temp_min),f
	line	168
	
i1l2659:	
;main.c: 168: temp_maj = source >> 4;
	swapf	(i1BCDconv@source),w
	andlw	(0ffh shr 4) & 0ffh
	movwf	(i1BCDconv@temp_maj)
	line	171
	
i1l2661:	
;main.c: 171: temp_maj *= 10;
	clrc
	rlf	(i1BCDconv@temp_maj),w
	addwf	(i1BCDconv@temp_maj),f
	addwf	(i1BCDconv@temp_maj),f
	clrc
	rlf	(i1BCDconv@temp_maj),f
	line	174
	
i1l2663:	
;main.c: 174: return temp_maj+temp_min;
	movf	(i1BCDconv@temp_min),w
	addwf	(i1BCDconv@temp_maj),w
	line	175
	
i1l380:	
	return
	opt stack 0
GLOBAL	__end_ofi1_BCDconv
	__end_ofi1_BCDconv:
;; =============== function i1_BCDconv ends ============

	signat	i1_BCDconv,89
psect	text1017,local,class=CODE,delta=2
global __ptext1017
__ptext1017:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
