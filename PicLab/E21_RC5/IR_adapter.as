opt subtitle "HI-TECH Software Omniscient Code Generator (PRO mode) build 6446"

opt pagewidth 120

	opt pm

	processor	16F877A
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 12 "X:\Hi-tech\IR_adapter\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 12 "X:\Hi-tech\IR_adapter\main.c"
	dw 0x3FFB & 0x3FFF & 0x3F7F & 0x3FFE ;#
	FNCALL	_main,_printf
	FNCALL	_printf,_putch
	FNCALL	_printf,___lwdiv
	FNCALL	_printf,___lwmod
	FNROOT	_main
	FNCALL	_isr,_StartTimer
	FNCALL	intlevel1,_isr
	global	intlevel1
	FNROOT	intlevel1
	global	_dpowers
psect	strings,class=STRING,delta=2
global __pstrings
__pstrings:
;	global	stringdir,stringtab,__stringbase
stringtab:
;	String table - string pointers are 1 byte each
stringcode:stringdir:
movlw high(stringdir)
movwf pclath
movf fsr,w
incf fsr
	addwf pc
__stringbase:
	retlw	0
psect	strings
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\lib\doprnt.c"
	line	350
_dpowers:
	retlw	01h
	retlw	0

	retlw	0Ah
	retlw	0

	retlw	064h
	retlw	0

	retlw	0E8h
	retlw	03h

	retlw	010h
	retlw	027h

	global	_dpowers
	global	_IRbyte
	global	_bytecount
	global	_i
	global	_buffer
	global	_direction
	global	_RCSTA
_RCSTA	set	24
	global	_TMR1H
_TMR1H	set	15
	global	_TMR1L
_TMR1L	set	14
	global	_TXREG
_TXREG	set	25
	global	_GIE
_GIE	set	95
	global	_INTE
_INTE	set	92
	global	_INTF
_INTF	set	89
	global	_PEIE
_PEIE	set	94
	global	_RB0
_RB0	set	48
	global	_T1CKPS0
_T1CKPS0	set	132
	global	_T1CKPS1
_T1CKPS1	set	133
	global	_T1OSCEN
_T1OSCEN	set	131
	global	_TMR1CS
_TMR1CS	set	129
	global	_TMR1IF
_TMR1IF	set	96
	global	_TMR1ON
_TMR1ON	set	128
	global	_TXIF
_TXIF	set	100
	global	_SPBRG
_SPBRG	set	153
	global	_TXSTA
_TXSTA	set	152
	global	_INTEDG
_INTEDG	set	1038
	global	_TMR1IE
_TMR1IE	set	1120
	global	_TRISB0
_TRISB0	set	1072
	global	_TRISB5
_TRISB5	set	1077
	global	_TRISC6
_TRISC6	set	1086
	global	_TRISC7
_TRISC7	set	1087
	
STR_2:	
	retlw	13
	retlw	10
	retlw	80	;'P'
	retlw	114	;'r'
	retlw	101	;'e'
	retlw	115	;'s'
	retlw	115	;'s'
	retlw	101	;'e'
	retlw	100	;'d'
	retlw	32	;' '
	retlw	98	;'b'
	retlw	117	;'u'
	retlw	116	;'t'
	retlw	116	;'t'
	retlw	111	;'o'
	retlw	110	;'n'
	retlw	32	;' '
	retlw	105	;'i'
	retlw	115	;'s'
	retlw	32	;' '
	retlw	37	;'%'
	retlw	100	;'d'
	retlw	0
psect	strings
	
STR_1:	
	retlw	13
	retlw	10
	retlw	83	;'S'
	retlw	116	;'t'
	retlw	97	;'a'
	retlw	114	;'r'
	retlw	116	;'t'
	retlw	0
psect	strings
	file	"IR_adapter.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bitbssCOMMON,class=COMMON,bit,space=1
global __pbitbssCOMMON
__pbitbssCOMMON:
_buffer:
       ds      1

_direction:
       ds      1

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_i:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_IRbyte:
       ds      1

_bytecount:
       ds      1

; Clear objects allocated to BITCOMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbitbssCOMMON/8)+0)&07Fh
; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
	clrf	((__pbssBANK0)+1)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_putch
?_putch:	; 0 bytes @ 0x0
	global	?_StartTimer
?_StartTimer:	; 0 bytes @ 0x0
	global	??_StartTimer
??_StartTimer:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 0 bytes @ 0x0
	global	?_isr
?_isr:	; 0 bytes @ 0x0
	global	??_isr
??_isr:	; 0 bytes @ 0x0
	ds	3
	global	??_putch
??_putch:	; 0 bytes @ 0x3
	global	?___lwdiv
?___lwdiv:	; 2 bytes @ 0x3
	global	putch@byte
putch@byte:	; 1 bytes @ 0x3
	global	___lwdiv@divisor
___lwdiv@divisor:	; 2 bytes @ 0x3
	ds	2
	global	___lwdiv@dividend
___lwdiv@dividend:	; 2 bytes @ 0x5
	ds	2
	global	??___lwdiv
??___lwdiv:	; 0 bytes @ 0x7
	global	??___lwmod
??___lwmod:	; 0 bytes @ 0x7
	global	___lwmod@counter
___lwmod@counter:	; 1 bytes @ 0x7
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0x8
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	___lwdiv@counter
___lwdiv@counter:	; 1 bytes @ 0x0
	ds	1
	global	___lwdiv@quotient
___lwdiv@quotient:	; 2 bytes @ 0x1
	ds	2
	global	?___lwmod
?___lwmod:	; 2 bytes @ 0x3
	global	___lwmod@divisor
___lwmod@divisor:	; 2 bytes @ 0x3
	ds	2
	global	___lwmod@dividend
___lwmod@dividend:	; 2 bytes @ 0x5
	ds	2
	global	?_printf
?_printf:	; 2 bytes @ 0x7
	ds	2
	global	??_printf
??_printf:	; 0 bytes @ 0x9
	ds	2
	global	printf@ap
printf@ap:	; 1 bytes @ 0xB
	ds	1
	global	printf@flag
printf@flag:	; 1 bytes @ 0xC
	ds	1
	global	printf@f
printf@f:	; 1 bytes @ 0xD
	ds	1
	global	printf@prec
printf@prec:	; 1 bytes @ 0xE
	ds	1
	global	printf@_val
printf@_val:	; 4 bytes @ 0xF
	ds	4
	global	printf@c
printf@c:	; 1 bytes @ 0x13
	ds	1
;;Data sizes: Strings 31, constant 10, data 0, bss 3, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      8      10
;; BANK0           80     20      22
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?___lwdiv	unsigned int  size(1) Largest target is 0
;;
;; ?___lwmod	unsigned int  size(1) Largest target is 0
;;
;; printf@f	PTR const unsigned char  size(1) Largest target is 23
;;		 -> STR_2(CODE[23]), STR_1(CODE[8]), 
;;
;; ?_printf	int  size(1) Largest target is 0
;;
;; printf@ap	PTR void [1] size(1) Largest target is 2
;;		 -> ?_printf(BANK0[2]), 
;;
;; S614$_cp	PTR const unsigned char  size(1) Largest target is 0
;;
;; _val._str._cp	PTR const unsigned char  size(1) Largest target is 0
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   _printf->___lwmod
;;   ___lwmod->___lwdiv
;;
;; Critical Paths under _isr in COMMON
;;
;;   None.
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_printf
;;   _printf->___lwmod
;;   ___lwmod->___lwdiv
;;
;; Critical Paths under _isr in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _isr in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _isr in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.
;;
;; Critical Paths under _isr in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 0, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 1     1      0     985
;;                             _printf
;; ---------------------------------------------------------------------------------
;; (1) _printf                                              13    11      2     985
;;                                              7 BANK0     13    11      2
;;                              _putch
;;                            ___lwdiv
;;                            ___lwmod
;; ---------------------------------------------------------------------------------
;; (2) ___lwmod                                              5     1      4     159
;;                                              7 COMMON     1     1      0
;;                                              3 BANK0      4     0      4
;;                            ___lwdiv (ARG)
;; ---------------------------------------------------------------------------------
;; (2) ___lwdiv                                              7     3      4     241
;;                                              3 COMMON     4     0      4
;;                                              0 BANK0      3     3      0
;; ---------------------------------------------------------------------------------
;; (2) _putch                                                1     1      0      22
;;                                              3 COMMON     1     1      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (3) _isr                                                  3     3      0       0
;;                                              0 COMMON     3     3      0
;;                         _StartTimer
;; ---------------------------------------------------------------------------------
;; (4) _StartTimer                                           0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 4
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _printf
;;     _putch
;;     ___lwdiv
;;     ___lwmod
;;       ___lwdiv (ARG)
;;
;; _isr (ROOT)
;;   _StartTimer
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       1       0        7.1%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      8       A       1       71.4%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       6       2        0.0%
;;ABS                  0      0      20       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50     14      16       5       27.5%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;BANK3               60      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0      26      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 29 in file "X:\Hi-tech\IR_adapter\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  temp            1    0        unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 17F/20
;;		Unchanged: FFE80/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_printf
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"X:\Hi-tech\IR_adapter\main.c"
	line	29
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 8
; Regs used in _main: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	32
	
l2012:	
;main.c: 32: TRISB0 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bsf	(1072/8)^080h,(1072)&7
	line	33
;main.c: 33: TRISB5 = 1;
	bsf	(1077/8)^080h,(1077)&7
	line	34
;main.c: 34: buffer = 0;
	bcf	(_buffer/8),(_buffer)&7
	line	35
;main.c: 35: TRISC7 = 1; TRISC6 = 1; SPBRG = ((int)(4000000L/(16UL * 9600) -1)); RCSTA = (0|0x90); TXSTA = (0x4|0|0x20);
	bsf	(1087/8)^080h,(1087)&7
	bsf	(1086/8)^080h,(1086)&7
	
l2014:	
	movlw	(019h)
	movwf	(153)^080h	;volatile
	movlw	(090h)
	bcf	status, 5	;RP0=0, select bank0
	movwf	(24)	;volatile
	movlw	(024h)
	bsf	status, 5	;RP0=1, select bank1
	movwf	(152)^080h	;volatile
	line	36
	
l2016:	
;main.c: 36: printf("\r\nStart");
	movlw	((STR_1-__stringbase))&0ffh
	fcall	_printf
	line	37
	
l2018:	
;main.c: 37: IRbyte = 0;
	clrf	(_IRbyte)	;volatile
	line	38
	
l2020:	
;main.c: 38: bytecount = 0;
	clrf	(_bytecount)	;volatile
	line	39
	
l2022:	
;main.c: 39: direction = 0;
	bcf	(_direction/8),(_direction)&7
	line	40
	
l2024:	
;main.c: 40: INTEDG = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	(1038/8)^080h,(1038)&7
	line	41
	
l2026:	
;main.c: 41: GIE = 1;
	bsf	(95/8),(95)&7
	line	42
	
l2028:	
;main.c: 42: PEIE = 1;
	bsf	(94/8),(94)&7
	line	43
	
l2030:	
;main.c: 43: INTE = 1;
	bsf	(92/8),(92)&7
	line	46
	
l2032:	
;main.c: 45: {
;main.c: 46: if (bytecount>13)
	movlw	(0Eh)
	bcf	status, 5	;RP0=0, select bank0
	subwf	(_bytecount),w	;volatile
	skipc
	goto	u701
	goto	u700
u701:
	goto	l2032
u700:
	line	48
	
l2034:	
;main.c: 47: {
;main.c: 48: GIE = 0;
	bcf	(95/8),(95)&7
	line	49
	
l2036:	
;main.c: 49: printf("\r\nPressed button is %d", IRbyte);
	movf	(_IRbyte),w	;volatile
	movwf	(?_printf)
	clrf	(?_printf+1)
	movlw	((STR_2-__stringbase))&0ffh
	fcall	_printf
	line	51
	
l2038:	
;main.c: 51: IRbyte = 0;
	clrf	(_IRbyte)	;volatile
	line	52
	
l2040:	
;main.c: 52: bytecount=0;
	clrf	(_bytecount)	;volatile
	line	53
	
l2042:	
;main.c: 53: INTEDG = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	(1038/8)^080h,(1038)&7
	line	54
	
l2044:	
;main.c: 54: buffer = 0;
	bcf	(_buffer/8),(_buffer)&7
	line	55
	
l2046:	
;main.c: 55: GIE = 1;
	bsf	(95/8),(95)&7
	goto	l2032
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

psect	maintext
	line	74
	signat	_main,88
	global	_printf
psect	text177,local,class=CODE,delta=2
global __ptext177
__ptext177:

;; *************** function _printf *****************
;; Defined at:
;;		line 461 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\lib\doprnt.c"
;; Parameters:    Size  Location     Type
;;  f               1    wreg     PTR const unsigned char 
;;		 -> STR_2(23), STR_1(8), 
;; Auto vars:     Size  Location     Type
;;  f               1   13[BANK0 ] PTR const unsigned char 
;;		 -> STR_2(23), STR_1(8), 
;;  _val            4   15[BANK0 ] struct .
;;  c               1   19[BANK0 ] char 
;;  prec            1   14[BANK0 ] char 
;;  flag            1   12[BANK0 ] unsigned char 
;;  ap              1   11[BANK0 ] PTR void [1]
;;		 -> ?_printf(2), 
;; Return value:  Size  Location     Type
;;                  2    7[BANK0 ] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 15F/20
;;		On exit  : 17F/0
;;		Unchanged: FFE80/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       9       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0      13       0       0       0
;;Total ram usage:       13 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_putch
;;		___lwdiv
;;		___lwmod
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text177
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\doprnt.c"
	line	461
	global	__size_of_printf
	__size_of_printf	equ	__end_of_printf-_printf
	
_printf:	
	opt	stack 7
; Regs used in _printf: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
;printf@f stored from wreg
	line	537
	bcf	status, 5	;RP0=0, select bank0
	movwf	(printf@f)
	
l1964:	
	movlw	(?_printf)&0ffh
	movwf	(printf@ap)
	line	540
	goto	l2010
	line	542
	
l1966:	
	movf	(printf@c),w
	xorlw	025h
	skipnz
	goto	u631
	goto	u630
u631:
	goto	l1970
u630:
	line	545
	
l1968:	
	movf	(printf@c),w
	fcall	_putch
	line	546
	goto	l2010
	line	552
	
l1970:	
	clrf	(printf@flag)
	line	638
	
l1974:	
	movf	(printf@f),w
	incf	(printf@f),f
	movwf	fsr0
	fcall	stringdir
	movwf	(printf@c)
	; Switch size 1, requested type "space"
; Number of cases is 3, Range of values is 0 to 105
; switch strategies available:
; Name         Bytes Cycles
; simple_byte    10     6 (average)
;	Chosen strategy is simple_byte

	xorlw	0^0	; case 0
	skipnz
	goto	l1189
	xorlw	100^0	; case 100
	skipnz
	goto	l1976
	xorlw	105^100	; case 105
	skipnz
	goto	l1976
	goto	l2010

	line	1254
	
l1976:	
	movf	(printf@ap),w
	movwf	fsr0
	movf	indf,w
	movwf	(printf@_val)
	incf	fsr0,f
	movf	indf,w
	movwf	(printf@_val+1)
	
l1978:	
	incf	(printf@ap),f
	incf	(printf@ap),f
	line	1256
	
l1980:	
	btfss	(printf@_val+1),7
	goto	u641
	goto	u640
u641:
	goto	l1986
u640:
	line	1257
	
l1982:	
	movlw	(03h)
	iorwf	(printf@flag),f
	line	1258
	
l1984:	
	comf	(printf@_val),f
	comf	(printf@_val+1),f
	incf	(printf@_val),f
	skipnz
	incf	(printf@_val+1),f
	line	1300
	
l1986:	
	clrf	(printf@c)
	incf	(printf@c),f
	line	1301
	
l1990:	
	clrc
	rlf	(printf@c),w
	addlw	low((_dpowers-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(??_printf+0)+0
	fcall	stringdir
	movwf	(??_printf+0)+0+1
	movf	1+(??_printf+0)+0,w
	subwf	(printf@_val+1),w
	skipz
	goto	u655
	movf	0+(??_printf+0)+0,w
	subwf	(printf@_val),w
u655:
	skipnc
	goto	u651
	goto	u650
u651:
	goto	l1994
u650:
	goto	l1998
	line	1300
	
l1994:	
	incf	(printf@c),f
	
l1996:	
	movf	(printf@c),w
	xorlw	05h
	skipz
	goto	u661
	goto	u660
u661:
	goto	l1990
u660:
	line	1433
	
l1998:	
	movf	(printf@flag),w
	andlw	03h
	btfsc	status,2
	goto	u671
	goto	u670
u671:
	goto	l2002
u670:
	line	1434
	
l2000:	
	movlw	(02Dh)
	fcall	_putch
	line	1467
	
l2002:	
	movf	(printf@c),w
	movwf	(printf@prec)
	line	1469
	goto	l2008
	line	1484
	
l2004:	
	movlw	0Ah
	movwf	(?___lwmod)
	clrf	(?___lwmod+1)
	clrc
	rlf	(printf@prec),w
	addlw	low((_dpowers-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(?___lwdiv)
	fcall	stringdir
	movwf	(?___lwdiv+1)
	movf	(printf@_val+1),w
	movwf	1+(?___lwdiv)+02h
	movf	(printf@_val),w
	movwf	0+(?___lwdiv)+02h
	fcall	___lwdiv
	movf	(1+(?___lwdiv)),w
	movwf	1+(?___lwmod)+02h
	movf	(0+(?___lwdiv)),w
	movwf	0+(?___lwmod)+02h
	fcall	___lwmod
	movf	(0+(?___lwmod)),w
	addlw	030h
	movwf	(printf@c)
	line	1516
	
l2006:	
	movf	(printf@c),w
	fcall	_putch
	line	1469
	
l2008:	
	decf	(printf@prec),f
	incf	((printf@prec)),w
	skipz
	goto	u681
	goto	u680
u681:
	goto	l2004
u680:
	line	540
	
l2010:	
	movf	(printf@f),w
	incf	(printf@f),f
	movwf	fsr0
	fcall	stringdir
	movwf	(printf@c)
	movf	((printf@c)),f
	skipz
	goto	u691
	goto	u690
u691:
	goto	l1966
u690:
	line	1533
	
l1189:	
	return
	opt stack 0
GLOBAL	__end_of_printf
	__end_of_printf:
;; =============== function _printf ends ============

	signat	_printf,602
	global	___lwmod
psect	text178,local,class=CODE,delta=2
global __ptext178
__ptext178:

;; *************** function ___lwmod *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lwmod.c"
;; Parameters:    Size  Location     Type
;;  divisor         2    3[BANK0 ] unsigned int 
;;  dividend        2    5[BANK0 ] unsigned int 
;; Auto vars:     Size  Location     Type
;;  counter         1    7[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    3[BANK0 ] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 17F/0
;;		Unchanged: FFE80/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       4       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         1       4       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_printf
;; This function uses a non-reentrant model
;;
psect	text178
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lwmod.c"
	line	5
	global	__size_of___lwmod
	__size_of___lwmod	equ	__end_of___lwmod-___lwmod
	
___lwmod:	
	opt	stack 6
; Regs used in ___lwmod: [wreg+status,2+status,0]
	line	8
	
l1830:	
	movf	(___lwmod@divisor+1),w
	iorwf	(___lwmod@divisor),w
	skipnz
	goto	u471
	goto	u470
u471:
	goto	l1846
u470:
	line	9
	
l1832:	
	clrf	(___lwmod@counter)
	incf	(___lwmod@counter),f
	line	10
	goto	l1836
	line	11
	
l1834:	
	clrc
	rlf	(___lwmod@divisor),f
	rlf	(___lwmod@divisor+1),f
	line	12
	incf	(___lwmod@counter),f
	line	10
	
l1836:	
	btfss	(___lwmod@divisor+1),(15)&7
	goto	u481
	goto	u480
u481:
	goto	l1834
u480:
	line	15
	
l1838:	
	movf	(___lwmod@divisor+1),w
	subwf	(___lwmod@dividend+1),w
	skipz
	goto	u495
	movf	(___lwmod@divisor),w
	subwf	(___lwmod@dividend),w
u495:
	skipc
	goto	u491
	goto	u490
u491:
	goto	l1842
u490:
	line	16
	
l1840:	
	movf	(___lwmod@divisor),w
	subwf	(___lwmod@dividend),f
	movf	(___lwmod@divisor+1),w
	skipc
	decf	(___lwmod@dividend+1),f
	subwf	(___lwmod@dividend+1),f
	line	17
	
l1842:	
	clrc
	rrf	(___lwmod@divisor+1),f
	rrf	(___lwmod@divisor),f
	line	18
	
l1844:	
	decfsz	(___lwmod@counter),f
	goto	u501
	goto	u500
u501:
	goto	l1838
u500:
	line	20
	
l1846:	
	movf	(___lwmod@dividend+1),w
	movwf	(?___lwmod+1)
	movf	(___lwmod@dividend),w
	movwf	(?___lwmod)
	line	21
	
l1221:	
	return
	opt stack 0
GLOBAL	__end_of___lwmod
	__end_of___lwmod:
;; =============== function ___lwmod ends ============

	signat	___lwmod,8314
	global	___lwdiv
psect	text179,local,class=CODE,delta=2
global __ptext179
__ptext179:

;; *************** function ___lwdiv *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lwdiv.c"
;; Parameters:    Size  Location     Type
;;  divisor         2    3[COMMON] unsigned int 
;;  dividend        2    5[COMMON] unsigned int 
;; Auto vars:     Size  Location     Type
;;  quotient        2    1[BANK0 ] unsigned int 
;;  counter         1    0[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    3[COMMON] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 17F/0
;;		Unchanged: FFE80/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         4       0       0       0       0
;;      Locals:         0       3       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         4       3       0       0       0
;;Total ram usage:        7 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_printf
;; This function uses a non-reentrant model
;;
psect	text179
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lwdiv.c"
	line	5
	global	__size_of___lwdiv
	__size_of___lwdiv	equ	__end_of___lwdiv-___lwdiv
	
___lwdiv:	
	opt	stack 6
; Regs used in ___lwdiv: [wreg+status,2+status,0]
	line	9
	
l1938:	
	clrf	(___lwdiv@quotient)
	clrf	(___lwdiv@quotient+1)
	line	10
	
l1940:	
	movf	(___lwdiv@divisor+1),w
	iorwf	(___lwdiv@divisor),w
	skipnz
	goto	u591
	goto	u590
u591:
	goto	l1960
u590:
	line	11
	
l1942:	
	clrf	(___lwdiv@counter)
	incf	(___lwdiv@counter),f
	line	12
	goto	l1946
	line	13
	
l1944:	
	clrc
	rlf	(___lwdiv@divisor),f
	rlf	(___lwdiv@divisor+1),f
	line	14
	incf	(___lwdiv@counter),f
	line	12
	
l1946:	
	btfss	(___lwdiv@divisor+1),(15)&7
	goto	u601
	goto	u600
u601:
	goto	l1944
u600:
	line	17
	
l1948:	
	clrc
	rlf	(___lwdiv@quotient),f
	rlf	(___lwdiv@quotient+1),f
	line	18
	
l1950:	
	movf	(___lwdiv@divisor+1),w
	subwf	(___lwdiv@dividend+1),w
	skipz
	goto	u615
	movf	(___lwdiv@divisor),w
	subwf	(___lwdiv@dividend),w
u615:
	skipc
	goto	u611
	goto	u610
u611:
	goto	l1956
u610:
	line	19
	
l1952:	
	movf	(___lwdiv@divisor),w
	subwf	(___lwdiv@dividend),f
	movf	(___lwdiv@divisor+1),w
	skipc
	decf	(___lwdiv@dividend+1),f
	subwf	(___lwdiv@dividend+1),f
	line	20
	
l1954:	
	bsf	(___lwdiv@quotient)+(0/8),(0)&7
	line	22
	
l1956:	
	clrc
	rrf	(___lwdiv@divisor+1),f
	rrf	(___lwdiv@divisor),f
	line	23
	
l1958:	
	decfsz	(___lwdiv@counter),f
	goto	u621
	goto	u620
u621:
	goto	l1948
u620:
	line	25
	
l1960:	
	movf	(___lwdiv@quotient+1),w
	movwf	(?___lwdiv+1)
	movf	(___lwdiv@quotient),w
	movwf	(?___lwdiv)
	line	26
	
l1211:	
	return
	opt stack 0
GLOBAL	__end_of___lwdiv
	__end_of___lwdiv:
;; =============== function ___lwdiv ends ============

	signat	___lwdiv,8314
	global	_putch
psect	text180,local,class=CODE,delta=2
global __ptext180
__ptext180:

;; *************** function _putch *****************
;; Defined at:
;;		line 7 in file "X:\Hi-tech\IR_adapter\usart.c"
;; Parameters:    Size  Location     Type
;;  byte            1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  byte            1    3[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 17F/0
;;		Unchanged: FFE80/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         1       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_printf
;; This function uses a non-reentrant model
;;
psect	text180
	file	"X:\Hi-tech\IR_adapter\usart.c"
	line	7
	global	__size_of_putch
	__size_of_putch	equ	__end_of_putch-_putch
	
_putch:	
	opt	stack 6
; Regs used in _putch: [wreg]
;putch@byte stored from wreg
	movwf	(putch@byte)
	line	9
	
l1764:	
	line	10
;usart.c: 9: while(!TXIF)
	
l559:	
	line	9
	btfss	(100/8),(100)&7
	goto	u301
	goto	u300
u301:
	goto	l559
u300:
	line	11
	
l1766:	
;usart.c: 11: TXREG = byte;
	movf	(putch@byte),w
	movwf	(25)	;volatile
	line	12
	
l562:	
	return
	opt stack 0
GLOBAL	__end_of_putch
	__end_of_putch:
;; =============== function _putch ends ============

	signat	_putch,4216
	global	_isr
psect	text181,local,class=CODE,delta=2
global __ptext181
__ptext181:

;; *************** function _isr *****************
;; Defined at:
;;		line 77 in file "X:\Hi-tech\IR_adapter\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 40/0
;;		Unchanged: FFE00/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          3       0       0       0       0
;;      Totals:         3       0       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_StartTimer
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	text181
	file	"X:\Hi-tech\IR_adapter\main.c"
	line	77
	global	__size_of_isr
	__size_of_isr	equ	__end_of_isr-_isr
	
_isr:	
	opt	stack 5
; Regs used in _isr: [wreg+status,2+status,0+pclath+cstack]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+0
	movwf	saved_w
	movf	status,w
	movwf	(??_isr+1)
	movf	pclath,w
	movwf	(??_isr+2)
	ljmp	_isr
psect	text181
	line	78
	
i1l1774:	
;main.c: 78: if (INTF)
	btfss	(89/8),(89)&7
	goto	u31_21
	goto	u31_20
u31_21:
	goto	i1l1152
u31_20:
	line	81
	
i1l1776:	
;main.c: 79: {
;main.c: 81: if (bytecount<1)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(_bytecount),f
	skipz	;volatile
	goto	u32_21
	goto	u32_20
u32_21:
	goto	i1l1784
u32_20:
	line	83
	
i1l1778:	
;main.c: 82: {
;main.c: 83: direction = 1;
	bsf	(_direction/8),(_direction)&7
	line	84
;main.c: 84: INTEDG = 1;
	bsf	status, 5	;RP0=1, select bank1
	bsf	(1038/8)^080h,(1038)&7
	line	85
	
i1l1780:	
;main.c: 85: StartTimer();
	fcall	_StartTimer
	line	86
	
i1l1782:	
;main.c: 86: bytecount++;
	incf	(_bytecount),f	;volatile
	line	89
;main.c: 89: } else
	goto	i1l1792
	line	91
	
i1l1784:	
;main.c: 90: {
;main.c: 91: direction = !direction;
	movlw	1<<((_direction)&7)
	xorwf	((_direction)/8),f
	line	94
;main.c: 94: if (direction==buffer)
	btfsc	(_buffer/8),(_buffer)&7
	goto	u33_21
	goto	u33_20
u33_21:
	movlw	1
	goto	u33_22
u33_20:
	movlw	0
u33_22:
	movwf	(??_isr+0)+0
	btfsc	(_direction/8),(_direction)&7
	goto	u34_21
	goto	u34_20
u34_21:
	movlw	1
	goto	u34_22
u34_20:
	movlw	0
u34_22:
	xorwf	(??_isr+0)+0,w
	skipz
	goto	u35_21
	goto	u35_20
u35_21:
	goto	i1l1790
u35_20:
	line	96
	
i1l1786:	
;main.c: 95: {
;main.c: 96: StartTimer();
	fcall	_StartTimer
	line	97
	
i1l1788:	
;main.c: 97: bytecount++;
	incf	(_bytecount),f	;volatile
	line	101
	
i1l1790:	
;main.c: 99: }
;main.c: 101: INTEDG = direction;
	btfsc	(_direction/8),(_direction)&7
	goto	u36_21
	goto	u36_20
	
u36_21:
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1038/8)^080h,(1038)&7
	goto	u37_24
u36_20:
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1038/8)^080h,(1038)&7
u37_24:
	line	105
	
i1l1792:	
;main.c: 104: }
;main.c: 105: INTF = 0;
	bcf	(89/8),(89)&7
	line	107
;main.c: 107: }
	goto	i1l1159
	line	108
	
i1l1152:	
	line	109
;main.c: 108: else {
;main.c: 109: if (TMR1IF)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(96/8),(96)&7
	goto	u38_21
	goto	u38_20
u38_21:
	goto	i1l1159
u38_20:
	line	112
	
i1l1794:	
;main.c: 110: {
;main.c: 112: buffer = RB0;
	btfsc	(48/8),(48)&7
	goto	u39_21
	goto	u39_20
	
u39_21:
	bsf	(_buffer/8),(_buffer)&7
	goto	u40_24
u39_20:
	bcf	(_buffer/8),(_buffer)&7
u40_24:
	line	113
;main.c: 113: TMR1ON = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(128/8),(128)&7
	line	114
;main.c: 114: TMR1IF = 0;
	bcf	(96/8),(96)&7
	line	115
	
i1l1796:	
;main.c: 115: if ((bytecount>7)&&(bytecount<14))
	movlw	(08h)
	subwf	(_bytecount),w	;volatile
	skipc
	goto	u41_21
	goto	u41_20
u41_21:
	goto	i1l1159
u41_20:
	
i1l1798:	
	movlw	(0Eh)
	subwf	(_bytecount),w	;volatile
	skipnc
	goto	u42_21
	goto	u42_20
u42_21:
	goto	i1l1159
u42_20:
	line	117
	
i1l1800:	
;main.c: 116: {
;main.c: 117: IRbyte <<= 1;
	clrc
	rlf	(_IRbyte),f	;volatile
	line	118
	
i1l1802:	
;main.c: 118: IRbyte |= buffer;
	movlw	0
	btfsc	(_buffer/8),(_buffer)&7
	movlw	1
	iorwf	(_IRbyte),f	;volatile
	line	126
	
i1l1159:	
	movf	(??_isr+2),w
	movwf	pclath
	movf	(??_isr+1),w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_isr
	__end_of_isr:
;; =============== function _isr ends ============

	signat	_isr,88
	global	_StartTimer
psect	text182,local,class=CODE,delta=2
global __ptext182
__ptext182:

;; *************** function _StartTimer *****************
;; Defined at:
;;		line 16 in file "X:\Hi-tech\IR_adapter\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 40/20
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_isr
;; This function uses a non-reentrant model
;;
psect	text182
	file	"X:\Hi-tech\IR_adapter\main.c"
	line	16
	global	__size_of_StartTimer
	__size_of_StartTimer	equ	__end_of_StartTimer-_StartTimer
	
_StartTimer:	
	opt	stack 4
; Regs used in _StartTimer: [wreg]
	line	17
	
i1l1768:	
;main.c: 17: T1CKPS1 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	(133/8),(133)&7
	line	18
;main.c: 18: T1CKPS0 = 0;
	bcf	(132/8),(132)&7
	line	20
;main.c: 20: T1OSCEN = 0;
	bcf	(131/8),(131)&7
	line	21
;main.c: 21: TMR1CS = 0;
	bcf	(129/8),(129)&7
	line	22
;main.c: 22: TMR1IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bsf	(1120/8)^080h,(1120)&7
	line	23
	
i1l1770:	
;main.c: 23: TMR1H = 0b11111011; TMR1L = 0b00010110;
	movlw	(0FBh)
	bcf	status, 5	;RP0=0, select bank0
	movwf	(15)	;volatile
	movlw	(016h)
	movwf	(14)	;volatile
	line	25
	
i1l1772:	
;main.c: 25: TMR1ON = 1;
	bsf	(128/8),(128)&7
	line	26
	
i1l1142:	
	return
	opt stack 0
GLOBAL	__end_of_StartTimer
	__end_of_StartTimer:
;; =============== function _StartTimer ends ============

	signat	_StartTimer,88
psect	text183,local,class=CODE,delta=2
global __ptext183
__ptext183:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
