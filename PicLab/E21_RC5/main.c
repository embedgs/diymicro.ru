#include <htc.h>
#include <stdio.h>
#include "usart.h"

#define _XTAL_FREQ 4000000
#define IRpin RB0
volatile static bit direction; //1 - rising, 0 - falling
volatile unsigned char IRbyte;  //�������� ����
volatile bit buffer;	   	
volatile unsigned char bytecount;
volatile unsigned char i=0;
__CONFIG(WDTDIS & UNPROTECT & LVPDIS & HS);


void StartTimer()
{
	T1CKPS1 = 0;
	T1CKPS0 = 0; 
	
	T1OSCEN = 0; //��������� ���������� ���������
	TMR1CS = 0; // Fosc/4
	TMR1IE = 1; // ���������� �� ������������ TMR1
 	TMR1H = 0b11111011; TMR1L = 0b00010110;	   //1.257ms
	//TMR1H = 0b00000000; TMR1L = 0x00;
	TMR1ON = 1; // �������� ������
}

void main()
{
unsigned char temp = 0;

TRISB0 = 1;
TRISB5 = 1;
buffer = 0;
init_comms();
printf("\r\nStart");
IRbyte = 0;
bytecount = 0;
direction = 0;
INTEDG = 0;						//���� ���������� �� ������������ ������
GIE = 1;
PEIE = 1;
INTE = 1;
while(1)
{
	if (bytecount>13)
	{
		GIE = 0;
		printf("\r\nPressed button is %d", IRbyte);
				
				IRbyte = 0;
				bytecount=0;
				INTEDG = 0;
				buffer = 0;
		GIE = 1;		
	}
	
	
/*	if (!RB5) {
				__delay_ms(100);
				printf("\r\n i = %d", i);
				printf("\r\n count = %d", bytecount);
				printf("\r\n buffer = %d", IRbyte);
				i=0;
				IRbyte = 0;
				bytecount=0;
				INTEDG = 0;
				buffer = 0;
				}	
				*/
}


}

void interrupt isr()
{
	if (INTF)
	{
		
		if (bytecount<1) 
			{ 
			direction = 1;
			INTEDG = 1;
			StartTimer();
			bytecount++;
			
			
			} else 
				{
				direction = !direction;
				//if (bytecount<3) {
				//if (((direction)&&(buffer))||(!(direction)&&!(buffer))) 
				if (direction==buffer)
				{
			    	StartTimer();
					bytecount++;

				}
			//	}
				INTEDG = direction;
				
				
				}
		INTF = 0;		
		
		}
	else {
	if (TMR1IF)
		{
			
		buffer = IRpin;
		TMR1ON = 0;
		TMR1IF = 0;
		if ((bytecount>7)&&(bytecount<14))   //�������� ������ �������
			{
				IRbyte <<= 1;
				IRbyte |= buffer;
			}
			
		
	
		}
		}
			
} 
