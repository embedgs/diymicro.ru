 #include <xc.h>
#include "uart.h"
#include "adc.h"

#define _XTAL_FREQ 8000000 //The speed of your internal(or)external oscillator


// PIC18F1230 Configuration Bit Settings

// 'C' source line config statements

// CONFIG1H
#pragma config OSC = INTIO1     // Oscillator (Internal oscillator, port function on RA6 and RA7)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config IESO = OFF       // Internal/External Oscillator Switchover bit (Oscillator Switchover mode disabled)

// CONFIG2L
#pragma config PWRT = OFF       // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOR = BOHW       // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware only (SBOREN is disabled))
#pragma config BORV = 3         // Brown-out Reset Voltage bits (Minimum setting)

// CONFIG2H
#pragma config WDT = OFF        // Watchdog Timer Enable bit (WDT disabled (control is placed on the SWDTEN bit))
#pragma config WDTPS = 32768    // Watchdog Timer Postscale Select bits (1:32768)

// CONFIG3L
#pragma config PWMPIN = OFF     // PWM Output Pins Reset State Control bit (PWM outputs disabled upon Reset)
#pragma config LPOL = HIGH      // Low-Side Transistors Polarity bit (Even PWM Output Polarity Control bit) (PWM0, PWM2 and PWM4 are active-high (default))
#pragma config HPOL = HIGH      // High Side Transistors Polarity bit (Odd PWM Output Polarity Control bit) (PWM1, PWM3 and PWM5 are active-high (default))

// CONFIG3H
#pragma config FLTAMX = RA5     // FLTA Mux bit (FLTA input is muxed onto RA5)
#pragma config T1OSCMX = LOW    // T1OSO/T1CKI MUX bit (T1OSO/T1CKI pin resides on RB2)
#pragma config MCLRE = ON       // Master Clear Enable bit (MCLR pin enabled, RA5 input pin disabled)

// CONFIG4L
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable bit (Reset on stack overflow/underflow enabled)
#pragma config BBSIZ = BB256    // Boot Block Size Select bits (256 Words (512 Bytes) Boot Block size)
#pragma config XINST = OFF      // Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled)

// CONFIG5L
#pragma config CP0 = OFF        // Code Protection bit Block 0 (000400-0007FF) (Block 0 is not code-protected)
#pragma config CP1 = OFF        // Code Protection bit Block 1 (000800-000FFF) (Block 1 is not code-protected)

// CONFIG5H
#pragma config CPB = OFF        // Code Protection bit (Boot Block Memory Area) (Boot Block is not code-protected)
#pragma config CPD = OFF        // Code Protection bit (Data EEPROM) (Data EEPROM is not code-protected)

// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection bit Block 0 (000400-0007FF) (Block 0 is not write-protected)
#pragma config WRT1 = OFF       // Write Protection bit Block 1 (000800-000FFF) (Block 1 is not write-protected)

// CONFIG6H
#pragma config WRTC = OFF       // Write Protection bit (Configuration Registers) (Configuration registers are not write-protected)
#pragma config WRTB = OFF       // Write Protection bit (Boot Block Memory Area) (Boot Block is not write-protected)
#pragma config WRTD = OFF       // Write Protection bit (Data EEPROM) (Data EEPROM is not write-protected)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection bit Block 0 (000400-0007FF) (Block 0 is not protected from table reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection bit Block 1 (000800-000FFF) (Block 1 is not protected from table reads executed in other blocks)

// CONFIG7H
#pragma config EBTRB = OFF      // Table Read Protection bit (Boot Block Memory Area) (Boot Block is not protected from table reads executed in other blocks)


char rx_data_recieved = 0;
char tx_data_temp = 0;
const int sensor_n1_min[32] = {1010, 723, 581, 481, 396, 326, 268, 217, 174, 146, 118, 90, 71, 58, 39, 29, 27, 19, 10, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
const int sensor_n1_max[32] = {1023, 742, 606, 511, 428, 362, 306, 264, 223, 189, 162, 139, 113, 103, 91, 78, 67, 59, 50, 45, 42, 37, 33, 29, 30, 27, 28, 30, 32, 29, 22, 30};
const int sensor_n2_min[32] = {996, 851, 742, 674, 603, 554, 497, 452, 397, 359, 329, 300, 268, 245, 210, 170, 153, 128, 105, 98, 85, 57, 52, 34, 13, 20, 13, 1, 0, 0, 0, 0};
const int sensor_n2_max[32] = {1023, 896, 811, 731, 689, 642, 597, 566, 537, 503, 473, 448, 425, 402, 379, 355, 338, 321, 306, 290, 275, 263, 247, 233, 229, 219, 217, 209, 201, 191, 193, 179};
const int sensor_n3_min[32] = {994, 862, 792, 738, 684, 629, 589, 554, 523, 483, 458, 432, 399, 360, 344, 314, 288, 276, 256, 226, 213, 198, 171, 168, 155, 131, 127, 110, 104, 108, 82, 75};
const int sensor_n3_max[32] = {1023, 958, 892, 831, 788, 755, 719, 706, 676, 648, 633, 603, 579, 573, 551, 526, 523, 500, 487, 473, 462, 454, 439, 427, 424, 408, 403, 396, 381, 374, 364, 360};
const char delta_y = 30;
const char touch_trigger = 17;
volatile __bit tmr0flag = 0;
char i = 0;
char x = 0;
char touch_counter = 0;
int adc_data_temp = 0;
volatile __bit adc_conversion_start = 0;
volatile __bit adc_conversion_ended = 0;
volatile __bit timer0_event = 0;
volatile char samples_count = 0;
volatile char temp1[32];
volatile char temp2[32];
__bit sensor_n1_pressed = 0;
__bit sensor_n2_pressed = 0;
__bit sensor_n3_pressed = 0;
char sensor_n1_touches = 0;
char sensor_n2_touches = 0;
char sensor_n3_touches = 0;

void main() 
{
//INTOSC TO 8MHz
 IRCF2 = 1;
 IRCF1 = 1;
 IRCF0 = 1;
 SCS1 = 1;
//INTOSC setup    

 InitUart();
 writeDataUart((char *) "\r\ndiymicro.org\r\n");
 InitADC();
 ADIE = 0;
 
//launching the timer for polling (~1s)
 T0CS = 0;
 T016BIT = 0;
 PSA = 0;
 T0PS2 = 0;
 T0PS1 = 1;
 T0PS0 = 1;
 TMR0ON = 1;
 TMR0IF = 0;
 TMR0IE = 1;
//end of timer configuration

 
 IPEN = 0;
 GIE = 1;
 PEIE = 1;
 //adcSelChan(2);
 //adcAN2digitalOne();    
 //adcAN2analogIn();
 //GO = 1;
 samples_count = 0;
 adcAN1digitalOne();
 while(1)
 {
    if (timer0_event)       //time to check if sensor has been touched
    {
        TMR0IE = 0;
        TMR0ON = 0;
        timer0_event = 0;
        ADIE = 1;
        adcSelChan(0);
        adcAN0digitalOne();    
        adcAN0analogIn();
            
        GO = 1;
    }    

    //adc conversion ended, analizing outputs and restarting timer
    if (adc_conversion_ended)
    {
        ADIE = 0;
        adc_conversion_ended = 0;
        //zeroing all the service variables
        sensor_n1_pressed = 0;
        sensor_n2_pressed = 0;
        sensor_n3_pressed = 0;
        sensor_n1_touches = 0;
        sensor_n2_touches = 0;
        sensor_n3_touches = 0;

       x = 0;
              
              for(i=0;i<32;i++)
              {
                adc_data_temp = temp1[i];
                adc_data_temp = (adc_data_temp<<8) | temp2[i]; 
                if ((adc_data_temp>=sensor_n1_min[i])&&(adc_data_temp<=sensor_n1_max[i]))
                {
                     sensor_n1_touches++;   
                }//sensor n1 if
                    else
                    {
                        if ((adc_data_temp>=sensor_n2_min[i])&&(adc_data_temp<=sensor_n2_max[i]))
                        {
                            sensor_n2_touches++;   
                        }//if sensor n2
                            else
                            {
                                 if ((adc_data_temp>=sensor_n3_min[i])&&(adc_data_temp<=sensor_n3_max[i]))
                                    sensor_n3_touches++;
                            }//else 2
                    }//else 1

              } //for
       
              
         writeDataUart((char *) "\033c");
         writeDataUart((char *) "\r\nSummary\r\n");
         writeDataUart((char *) "\r\n n1\r\n");
         NumToUart(sensor_n1_touches);
         writeDataUart((char *) "\r\n n2\r\n");
         NumToUart(sensor_n2_touches);
         writeDataUart((char *) "\r\n n3\r\n");
         NumToUart(sensor_n3_touches);
         
        if (sensor_n1_touches>=touch_trigger)       //sensor touched event is detected
        {
            writeDataUart((char *) "\r\nSensor #1 is touched!\r\n");
           
        }//if #1
            else
            {
                  if (sensor_n2_touches>=touch_trigger)   
                  {
                        writeDataUart((char *) "\r\nSensor #2 is touched!\r\n");

                  }//if 2
                  else
                  {
                        if (sensor_n3_touches>=touch_trigger)
                            writeDataUart((char *) "\r\nSensor #3 is touched!\r\n");

                             
                  }//else 3
            }//else 1
        
        
        TMR0IE = 1;
        TMR0ON = 1;
    }
    //end of postprocessing the adc data

 }//while(1)

}//main()


void __interrupt(high_priority) HighISR(void)
{
    if (ADIF)
    {  
        temp1[samples_count] = ADRESH;
        temp2[samples_count] = ADRESL;        


          if (samples_count>=20)
          {
              /*writeDataUart((char *) "\033c");
              writeDataUart((char *) "Printing the data from ADC\r\n");
            
              for(i=0;i<32;i++)
              {
                writeDataUart((char *) "\r\n");
                adc_data_temp = temp1[i];
                adc_data_temp = (adc_data_temp<<8) | temp2[i]; 
                NumToUart(adc_data_temp);
              } 
             __delay_ms(200);__delay_ms(200);__delay_ms(200);__delay_ms(200);__delay_ms(200);  */
              samples_count=0;
              adc_conversion_ended = 1;
              //adcAN2digitalOne();    
              //adcAN2analogIn();
              //GO = 1;
          }else
          {
              samples_count++;
              GO = 1;
          }
        
        
        ADIF = 0;    
    }
    
    if (RCIF)
    {
       rx_data_recieved = ReadUart();
    }
   
    if (TMR0IF)
    {
        //adcAN0digitalOne();
        //adcAN0analogIn();
        timer0_event = 1;
        TMR0IF = 0;
    }

}