#include <xc.h>
#include "uart.h"
#include "adc.h"

#define _XTAL_FREQ 8000000 //The speed of your internal(or)external oscillator


// PIC18F1230 Configuration Bit Settings

// 'C' source line config statements

// CONFIG1H
#pragma config OSC = INTIO1     // Oscillator (Internal oscillator, port function on RA6 and RA7)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config IESO = OFF       // Internal/External Oscillator Switchover bit (Oscillator Switchover mode disabled)

// CONFIG2L
#pragma config PWRT = OFF       // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOR = BOHW       // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware only (SBOREN is disabled))
#pragma config BORV = 3         // Brown-out Reset Voltage bits (Minimum setting)

// CONFIG2H
#pragma config WDT = OFF        // Watchdog Timer Enable bit (WDT disabled (control is placed on the SWDTEN bit))
#pragma config WDTPS = 32768    // Watchdog Timer Postscale Select bits (1:32768)

// CONFIG3L
#pragma config PWMPIN = OFF     // PWM Output Pins Reset State Control bit (PWM outputs disabled upon Reset)
#pragma config LPOL = HIGH      // Low-Side Transistors Polarity bit (Even PWM Output Polarity Control bit) (PWM0, PWM2 and PWM4 are active-high (default))
#pragma config HPOL = HIGH      // High Side Transistors Polarity bit (Odd PWM Output Polarity Control bit) (PWM1, PWM3 and PWM5 are active-high (default))

// CONFIG3H
#pragma config FLTAMX = RA5     // FLTA Mux bit (FLTA input is muxed onto RA5)
#pragma config T1OSCMX = LOW    // T1OSO/T1CKI MUX bit (T1OSO/T1CKI pin resides on RB2)
#pragma config MCLRE = ON       // Master Clear Enable bit (MCLR pin enabled, RA5 input pin disabled)

// CONFIG4L
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable bit (Reset on stack overflow/underflow enabled)
#pragma config BBSIZ = BB256    // Boot Block Size Select bits (256 Words (512 Bytes) Boot Block size)
#pragma config XINST = OFF      // Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled)

// CONFIG5L
#pragma config CP0 = OFF        // Code Protection bit Block 0 (000400-0007FF) (Block 0 is not code-protected)
#pragma config CP1 = OFF        // Code Protection bit Block 1 (000800-000FFF) (Block 1 is not code-protected)

// CONFIG5H
#pragma config CPB = OFF        // Code Protection bit (Boot Block Memory Area) (Boot Block is not code-protected)
#pragma config CPD = OFF        // Code Protection bit (Data EEPROM) (Data EEPROM is not code-protected)

// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection bit Block 0 (000400-0007FF) (Block 0 is not write-protected)
#pragma config WRT1 = OFF       // Write Protection bit Block 1 (000800-000FFF) (Block 1 is not write-protected)

// CONFIG6H
#pragma config WRTC = OFF       // Write Protection bit (Configuration Registers) (Configuration registers are not write-protected)
#pragma config WRTB = OFF       // Write Protection bit (Boot Block Memory Area) (Boot Block is not write-protected)
#pragma config WRTD = OFF       // Write Protection bit (Data EEPROM) (Data EEPROM is not write-protected)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection bit Block 0 (000400-0007FF) (Block 0 is not protected from table reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection bit Block 1 (000800-000FFF) (Block 1 is not protected from table reads executed in other blocks)

// CONFIG7H
#pragma config EBTRB = OFF      // Table Read Protection bit (Boot Block Memory Area) (Boot Block is not protected from table reads executed in other blocks)


char rx_data_recieved = 0;
char tx_data_temp = 0;
const int sensor_n1_min[32] = {1007, 722, 587, 483, 396, 333, 284, 240, 198, 164, 133, 112, 94, 76, 61, 47, 37, 25, 18, 17, 11, 9, 5, 5, 2, 0, 1, 0, 0, 0, 0, 0};
const int sensor_n1_max[32] = {1009, 746, 615, 525, 443, 378, 328, 278, 239, 211, 187, 170, 149, 132, 119, 109, 104, 95, 89, 79, 71, 70, 72, 72, 70, 70, 68, 70, 72, 73, 74, 76};
const int sensor_n2_min[32] = {1000, 844, 752, 669, 615, 565, 518, 474, 438, 396, 365, 331, 308, 275, 253, 238, 213, 187, 159, 152, 149, 138, 114, 83, 76, 75, 69, 56, 59, 58, 36, 25};
const int sensor_n2_max[32] = {1015, 890, 792, 727, 676, 627, 584, 540, 507, 478, 453, 430, 400, 373, 347, 329, 312, 294, 285, 268, 251, 244, 231, 224, 213, 205, 194, 181, 166, 156, 149, 140};
const int sensor_n3_min[32] = {1021, 895, 797, 735, 680, 632, 593, 545, 514, 482, 458, 435, 405, 379, 351, 335, 317, 299, 289, 274, 226, 250, 235, 227, 220, 210, 200, 186, 170, 160, 155, 145};
const int sensor_n3_max[32] = {1023, 937, 873, 819, 770, 740, 718, 692, 683, 671, 646, 619, 596, 576, 557, 541, 535, 516, 506, 495, 468, 458, 447, 439, 421, 416, 411, 402, 393, 385, 376, 380};
const char delta_y = 30;
const char touch_trigger = 15;
volatile __bit tmr0flag = 0;
char i = 0;
char x = 0;
char touch_counter = 0;
int adc_data_temp = 0;
volatile __bit adc_conversion_start = 0;
volatile __bit adc_conversion_ended = 0;
volatile __bit timer0_event = 0;
volatile char samples_count = 0;
volatile char temp1[32];
volatile char temp2[32];
__bit sensor_n1_pressed = 0;
__bit sensor_n2_pressed = 0;
__bit sensor_n3_pressed = 0;
char sensor_n1_touches = 0;
char sensor_n2_touches = 0;
char sensor_n3_touches = 0;

void main() 
{
//INTOSC TO 8MHz
 IRCF2 = 1;
 IRCF1 = 1;
 IRCF0 = 1;
 SCS1 = 1;
//INTOSC setup    

 InitUart();
 writeDataUart((char *) "\r\ndiymicro.org\r\n");
 InitADC();
 ADIE = 0;
 
//launching the timer for polling (~1s)
 T0CS = 0;
 T016BIT = 0;
 PSA = 0;
 T0PS2 = 0;
 T0PS1 = 1;
 T0PS0 = 1;
 TMR0ON = 1;
 TMR0IF = 0;
 TMR0IE = 1;
//end of timer configuration

 
 IPEN = 0;
 GIE = 1;
 PEIE = 1;
 //adcSelChan(2);
 //adcAN2digitalOne();    
 //adcAN2analogIn();
 //GO = 1;
 samples_count = 0;
 adcAN1digitalOne();
 while(1)
 {
    if (timer0_event)       //time to check if sensor has been touched
    {
        TMR0IE = 0;
        TMR0ON = 0;
        timer0_event = 0;
        ADIE = 1;
        adcSelChan(0);
        adcAN0digitalOne();    
        adcAN0analogIn();
            
        GO = 1;
    }    

    //adc conversion ended, analizing outputs and restarting timer
    if (adc_conversion_ended)
    {
        ADIE = 0;
        adc_conversion_ended = 0;
        //zeroing all the service variables
        sensor_n1_pressed = 0;
        sensor_n2_pressed = 0;
        sensor_n3_pressed = 0;
        sensor_n1_touches = 0;
        sensor_n2_touches = 0;
        sensor_n3_touches = 0;

       x = 0;
              
              for(i=0;i<32;i++)
              {
                adc_data_temp = temp1[i];
                adc_data_temp = (adc_data_temp<<8) | temp2[i]; 
                if ((adc_data_temp>=sensor_n1_min[i])&&(adc_data_temp<=sensor_n1_max[i]))
                {
                     sensor_n1_touches++;   
                }//sensor n1 if
                    else
                    {
                        if ((adc_data_temp>=sensor_n2_min[i])&&(adc_data_temp<=sensor_n2_max[i]))
                        {
                            sensor_n2_touches++;   
                        }//if sensor n2
                            else
                            {
                                 if ((adc_data_temp>=sensor_n3_min[i])&&(adc_data_temp<=sensor_n3_max[i]))
                                    sensor_n3_touches++;
                            }//else 2
                    }//else 1

              } //for
       
              
         writeDataUart((char *) "\033c");
         writeDataUart((char *) "\r\nSummary\r\n");
         writeDataUart((char *) "\r\n n1\r\n");
         NumToUart(sensor_n1_touches);
         writeDataUart((char *) "\r\n n2\r\n");
         NumToUart(sensor_n2_touches);
         writeDataUart((char *) "\r\n n3\r\n");
         NumToUart(sensor_n3_touches);
         
        if (sensor_n1_touches>=touch_trigger)       //sensor touched event is detected
        {
            writeDataUart((char *) "\r\nSensor #1 is touched!\r\n");
           
        }//if #1
            else
            {
                  if (sensor_n2_touches>=touch_trigger)   
                  {
                        writeDataUart((char *) "\r\nSensor #2 is touched!\r\n");

                  }//if 2
                  else
                  {
                        if (sensor_n3_touches>=touch_trigger)
                            writeDataUart((char *) "\r\nSensor #3 is touched!\r\n");

                             
                  }//else 3
            }//else 1
        
        
        TMR0IE = 1;
        TMR0ON = 1;
    }
    //end of postprocessing the adc data

 }//while(1)

}//main()


void __interrupt(high_priority) HighISR(void)
{
    if (ADIF)
    {  
        temp1[samples_count] = ADRESH;
        temp2[samples_count] = ADRESL;        


          if (samples_count>=20)
          {
              /*writeDataUart((char *) "\033c");
              writeDataUart((char *) "Printing the data from ADC\r\n");
            
              for(i=0;i<32;i++)
              {
                writeDataUart((char *) "\r\n");
                adc_data_temp = temp1[i];
                adc_data_temp = (adc_data_temp<<8) | temp2[i]; 
                NumToUart(adc_data_temp);
              } 
             __delay_ms(200);__delay_ms(200);__delay_ms(200);__delay_ms(200);__delay_ms(200);  */
              samples_count=0;
              adc_conversion_ended = 1;
              //adcAN2digitalOne();    
              //adcAN2analogIn();
              //GO = 1;
          }else
          {
              samples_count++;
              GO = 1;
          }
        
        
        ADIF = 0;    
    }
    
    if (RCIF)
    {
       rx_data_recieved = ReadUart();
    }
   
    if (TMR0IF)
    {
        //adcAN0digitalOne();
        //adcAN0analogIn();
        timer0_event = 1;
        TMR0IF = 0;
    }

}