/* 
 * File:   main.c
 * Author: Sarge
 *
 * Created on 18 ??????? 2014 ?., 1:10
 */
#include <xc.h>
#include <plib/usart.h>
#include <plib/delays.h>


#define _XTAL_FREQ 12000000 //The speed of your internal(or)external oscillator

#pragma config WDTEN = OFF, LVP = OFF, FOSC = HS  

unsigned char UARTConfig = 0;
unsigned char baud = 0;
unsigned int Vmeas[128] = 0;
volatile bit ADCstatus = 0;
volatile unsigned char i = 0;

void Delay1us();          //1us delay for 12MHz xtall
void DelayUs(unsigned char Us);
void DelayMs(unsigned int Ms);
void NumToUart(unsigned int Num);  

/*
 * 
 */
void main()
{

 TRISC7 = 1;
 ANSEL = 0;                                                                   //here all analog buffers off
 ANSELH = 2;																  //turn on analog input for RC7(AN9)	
 ADCON0 = 0b00100100;														  //Channel AN9 select		
 ADCON2 = 0b10000100;														  //Right justification, clock = Fosc/4	
 ADON = 1;																	  //ADC is enabled!!!
 TRISB7 = 0; //TX pin set as output
 TRISB5 = 1; //RX pin set as input
 

 TRISC5 = 0;

// Vmeas = 0;



 UARTConfig = USART_TX_INT_OFF & USART_RX_INT_OFF & USART_ASYNCH_MODE & USART_EIGHT_BIT & USART_BRGH_HIGH ;    //прерывания выключены, асинхронный режим, 8 бит, высокоскоростной режим
 baud = 77;                 //Focs/(9600*16) - 1
 OpenUSART(UARTConfig,baud);
 

 putsUSART( (char *) "Welcome to Diymicro.ru\r\n" );

 ADIF = 0;
 ADIE = 1;
 PEIE = 1;
 GIE = 1;


 GO = 1;																	   //go, go, go!!! (:



 while(1)
 {
 		if ((ADCstatus)&&(i<128))
 		{
 			Vmeas[i] = (ADRESH << 8) | ADRESL;
 			//NumToUart(Vmeas);
 			//putsUSART( (char *) "\r\n" );
 			i++;
 			ADCstatus = 0;
 			GO = 1;
  		} else
  			{
  				if ((i == 128)&&(ADCstatus))
  				{
  					ADCstatus = 0;
  					for (i=0;i<128;i++)
  					{
  						 NumToUart(Vmeas[i]);	
  						 putsUSART( (char *) "\r\n" );
  					}
  				}
  			}


 }

}



void interrupt isr()
{
	if (ADIF)
	{
		ADCstatus = 1;
		ADIF = 0;
		

	}
}


void Delay1us()         //delay approx 1 us
{
  Delay1TCY();
  Delay1TCY();
  Delay1TCY();
}

void DelayUs(unsigned char Us)      //delay for a given number of microseconds
{
  for (unsigned char i = 0; i<Us; i++)
    Delay1us(); 
}

void DelayMs(unsigned int Ms)      //approx delay for a given number of miliseconds
{
  for (unsigned int i=0; i<Ms; i++)
    Delay1KTCYx(3);
}

void NumToUart(unsigned int Num)                                                //Число в уарт
{

  unsigned int bignum = 10000;
  unsigned char numtemp = 5;

  if (!Num)
  {
      WriteUSART('0');         //Выталкиеваем все разряды - от старшего к младшему
      while(BusyUSART());                                                       //Ждем пока освободится модуль иначе будут прострелы
  }
  else 
  {
	  while(numtemp>0)                                                             //Определяем сколько разрядов имеет наше число
	  {
	    if (Num/bignum)
	        break;
	    numtemp--;
	    bignum = bignum / 10;  
	  }  
	
	
	
	  for (unsigned char i = numtemp; i>0; i--)
	    {
	      WriteUSART( (Num - (Num/(bignum*10))*bignum*10 )/bignum + '0');         //Выталкиеваем все разряды - от старшего к младшему
	      while(BusyUSART());                                                       //Ждем пока освободится модуль иначе будут прострелы
	      bignum = bignum/10;
	    }
   } 
}