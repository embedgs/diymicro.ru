/*******************************************************************************
Diymicro specific additional functions, 
Sergey Grinko
sargein@gmail.com
*******************************************************************************/

#ifndef USB_CDC_DIYMICRO_H
#define USB_CDC_DIYMICRO_H

#include <stdbool.h>
#include <stddef.h>

#include "usb_device_cdc.h"

//Converting number to ASCII
void NumToUART(uint16_t number);




#endif
