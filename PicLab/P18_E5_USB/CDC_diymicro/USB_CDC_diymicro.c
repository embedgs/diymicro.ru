/*******************************************************************************
Diymicro specific additional functions, 
Sergey Grinko
sargein@gmail.com
*******************************************************************************/

#include "system.h"
#include <stdbool.h>
#include <stddef.h>

#include "usb_device_cdc.h"
#include "USB_CDC_diymicro.h"

//Converting number to ASCII
void NumToUART(uint16_t number)
{
	uint16_t bignum = 10000; //max divider for uint16
    uint16_t temp_Number = 0;
	uint8_t numtemp = 5;    //an amount of digits (max)
    static uint8_t temp_data[5];

	while (numtemp>0)       //checking how many digits number has
    {
        if (number/bignum)
            break;
        numtemp--;
        bignum = bignum/10;
    }

   for (uint8_t i = numtemp; i>0; i--)    
	{     
       if (i == numtemp)
           temp_data[numtemp-i]=number/bignum;
       else
       {
            temp_data[numtemp-i]=number/bignum - (number/bignum/10)*10;   
       }
           bignum = bignum/10;
       }
       
    for (uint8_t i =0; i<numtemp; i++)
    {
        temp_data[i]=temp_data[i]+'0';
    }
        if( USBUSARTIsTxTrfReady() == true)
            putUSBUSART(temp_data,numtemp);

}



