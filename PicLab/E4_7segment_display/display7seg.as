opt subtitle "HI-TECH Software Omniscient Code Generator (PRO mode) build 5239"

opt pagewidth 120

	opt pm

	processor	16F628A
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 8 "X:\Hi-tech\7segment_display\digit.c"
	psect config,class=CONFIG,delta=2 ;#
# 8 "X:\Hi-tech\7segment_display\digit.c"
	dw 0x3FFF & 0x3FFF & 0x3FFB ;#
;COMMON:	_main->_vpih
;COMMON:	_vpih->_tick
;COMMON:	_tick->__delay
	FNCALL	_main,_tick
	FNCALL	_main,_vpih
	FNCALL	_vpih,_tick
	FNROOT	_main
	global	_eight
psect	strings,class=CODE,delta=2,reloc=256
global __pstrings
__pstrings:
	global	stringdir,stringtab,__stringbase,stringjmp
stringtab:
;	String table - string pointers are 1 byte each
	movwf	(btemp)&07Fh
	btfss	(btemp)&07Fh,7
	goto	stringcode
	bcf	status,7
	btfsc	btemp&7Fh,0
	bsf	status,7
	movf	indf,w
	return
stringcode:
	movf	fsr,w
stringdir:
movwf btemp&07Fh
movlw high(stringdir)
movwf pclath
movf btemp&07Fh,w
stringjmp:
	addwf pc
__stringbase:
	retlw	0
psect	strings
	file	"X:\Hi-tech\7segment_display\digit.c"
	line	38
_eight:
	retlw	0
	retlw	01h
	retlw	01h
	retlw	01h
	retlw	01h
	retlw	01h
	retlw	01h
	retlw	01h
	global	_five
psect	strings
	file	"X:\Hi-tech\7segment_display\digit.c"
	line	35
_five:
	retlw	0
	retlw	01h
	retlw	01h
	retlw	0
	retlw	01h
	retlw	01h
	retlw	0
	retlw	01h
	global	_four
psect	strings
	file	"X:\Hi-tech\7segment_display\digit.c"
	line	34
_four:
	retlw	0
	retlw	01h
	retlw	01h
	retlw	0
	retlw	0
	retlw	01h
	retlw	01h
	retlw	0
	global	_nine
psect	strings
	file	"X:\Hi-tech\7segment_display\digit.c"
	line	39
_nine:
	retlw	0
	retlw	01h
	retlw	01h
	retlw	0
	retlw	01h
	retlw	01h
	retlw	01h
	retlw	01h
	global	_one
psect	strings
	file	"X:\Hi-tech\7segment_display\digit.c"
	line	31
_one:
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	01h
	retlw	01h
	retlw	0
	global	_seven
psect	strings
	file	"X:\Hi-tech\7segment_display\digit.c"
	line	37
_seven:
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	01h
	retlw	01h
	retlw	01h
	global	_six
psect	strings
	file	"X:\Hi-tech\7segment_display\digit.c"
	line	36
_six:
	retlw	0
	retlw	01h
	retlw	01h
	retlw	01h
	retlw	01h
	retlw	01h
	retlw	0
	retlw	01h
	global	_three
psect	strings
	file	"X:\Hi-tech\7segment_display\digit.c"
	line	33
_three:
	retlw	0
	retlw	01h
	retlw	0
	retlw	0
	retlw	01h
	retlw	01h
	retlw	01h
	retlw	01h
	global	_two
psect	strings
	file	"X:\Hi-tech\7segment_display\digit.c"
	line	32
_two:
	retlw	0
	retlw	01h
	retlw	0
	retlw	01h
	retlw	01h
	retlw	0
	retlw	01h
	retlw	01h
	global	_zero
psect	strings
	file	"X:\Hi-tech\7segment_display\digit.c"
	line	40
_zero:
	retlw	0
	retlw	0
	retlw	01h
	retlw	01h
	retlw	01h
	retlw	01h
	retlw	01h
	retlw	01h
	global	_eight
	global	_five
	global	_four
	global	_nine
	global	_one
	global	_seven
	global	_six
	global	_three
	global	_two
	global	_zero
	global	_CCP1CON
_CCP1CON  equ     23
	global	_CCPR1H
_CCPR1H  equ     22
	global	_CCPR1L
_CCPR1L  equ     21
	global	_CMCON
_CMCON  equ     31
	global	_FSR
_FSR  equ     4
	global	_INDF
_INDF  equ     0
	global	_INTCON
_INTCON  equ     11
	global	_PCL
_PCL  equ     2
	global	_PCLATH
_PCLATH  equ     10
	global	_PIR1
_PIR1  equ     12
	global	_PORTA
_PORTA  equ     5
	global	_PORTB
_PORTB  equ     6
	global	_RCREG
_RCREG  equ     26
	global	_RCSTA
_RCSTA  equ     24
	global	_STATUS
_STATUS  equ     3
	global	_T1CON
_T1CON  equ     16
	global	_T2CON
_T2CON  equ     18
	global	_TMR0
_TMR0  equ     1
	global	_TMR1H
_TMR1H  equ     15
	global	_TMR1L
_TMR1L  equ     14
	global	_TMR2
_TMR2  equ     17
	global	_TXREG
_TXREG  equ     25
	global	_ADEN
_ADEN  equ     195
	global	_C1INV
_C1INV  equ     252
	global	_C1OUT
_C1OUT  equ     254
	global	_C2INV
_C2INV  equ     253
	global	_C2OUT
_C2OUT  equ     255
	global	_CARRY
_CARRY  equ     24
	global	_CCP1IF
_CCP1IF  equ     98
	global	_CCP1M0
_CCP1M0  equ     184
	global	_CCP1M1
_CCP1M1  equ     185
	global	_CCP1M2
_CCP1M2  equ     186
	global	_CCP1M3
_CCP1M3  equ     187
	global	_CCP1X
_CCP1X  equ     189
	global	_CCP1Y
_CCP1Y  equ     188
	global	_CIS
_CIS  equ     251
	global	_CM0
_CM0  equ     248
	global	_CM1
_CM1  equ     249
	global	_CM2
_CM2  equ     250
	global	_CMIF
_CMIF  equ     102
	global	_CREN
_CREN  equ     196
	global	_DC
_DC  equ     25
	global	_EEIF
_EEIF  equ     103
	global	_FERR
_FERR  equ     194
	global	_GIE
_GIE  equ     95
	global	_INTE
_INTE  equ     92
	global	_INTF
_INTF  equ     89
	global	_IRP
_IRP  equ     31
	global	_OERR
_OERR  equ     193
	global	_PD
_PD  equ     27
	global	_PEIE
_PEIE  equ     94
	global	_RA0
_RA0  equ     40
	global	_RA1
_RA1  equ     41
	global	_RA2
_RA2  equ     42
	global	_RA3
_RA3  equ     43
	global	_RA4
_RA4  equ     44
	global	_RA5
_RA5  equ     45
	global	_RA6
_RA6  equ     46
	global	_RA7
_RA7  equ     47
	global	_RB0
_RB0  equ     48
	global	_RB1
_RB1  equ     49
	global	_RB2
_RB2  equ     50
	global	_RB3
_RB3  equ     51
	global	_RB4
_RB4  equ     52
	global	_RB5
_RB5  equ     53
	global	_RB6
_RB6  equ     54
	global	_RB7
_RB7  equ     55
	global	_RBIE
_RBIE  equ     91
	global	_RBIF
_RBIF  equ     88
	global	_RCIF
_RCIF  equ     101
	global	_RP0
_RP0  equ     29
	global	_RP1
_RP1  equ     30
	global	_RX9
_RX9  equ     198
	global	_RX9D
_RX9D  equ     192
	global	_SPEN
_SPEN  equ     199
	global	_SREN
_SREN  equ     197
	global	_T0IE
_T0IE  equ     93
	global	_T0IF
_T0IF  equ     90
	global	_T1CKPS0
_T1CKPS0  equ     132
	global	_T1CKPS1
_T1CKPS1  equ     133
	global	_T1OSCEN
_T1OSCEN  equ     131
	global	_T1SYNC
_T1SYNC  equ     130
	global	_T2CKPS0
_T2CKPS0  equ     144
	global	_T2CKPS1
_T2CKPS1  equ     145
	global	_TMR1CS
_TMR1CS  equ     129
	global	_TMR1IF
_TMR1IF  equ     96
	global	_TMR1ON
_TMR1ON  equ     128
	global	_TMR2IF
_TMR2IF  equ     97
	global	_TMR2ON
_TMR2ON  equ     146
	global	_TO
_TO  equ     28
	global	_TOUTPS0
_TOUTPS0  equ     147
	global	_TOUTPS1
_TOUTPS1  equ     148
	global	_TOUTPS2
_TOUTPS2  equ     149
	global	_TOUTPS3
_TOUTPS3  equ     150
	global	_TXIF
_TXIF  equ     100
	global	_ZERO
_ZERO  equ     26
	global	_EEADR
_EEADR  equ     155
	global	_EECON1
_EECON1  equ     156
	global	_EECON2
_EECON2  equ     157
	global	_EEDATA
_EEDATA  equ     154
	global	_OPTION
_OPTION  equ     129
	global	_PCON
_PCON  equ     142
	global	_PIE1
_PIE1  equ     140
	global	_PR2
_PR2  equ     146
	global	_SPBRG
_SPBRG  equ     153
	global	_TRISA
_TRISA  equ     133
	global	_TRISB
_TRISB  equ     134
	global	_TXSTA
_TXSTA  equ     152
	global	_VRCON
_VRCON  equ     159
	global	_BOR
_BOR  equ     1136
	global	_BRGH
_BRGH  equ     1218
	global	_CCP1IE
_CCP1IE  equ     1122
	global	_CMIE
_CMIE  equ     1126
	global	_CSRC
_CSRC  equ     1223
	global	_EEIE
_EEIE  equ     1127
	global	_INTEDG
_INTEDG  equ     1038
	global	_OSCF
_OSCF  equ     1139
	global	_POR
_POR  equ     1137
	global	_PS0
_PS0  equ     1032
	global	_PS1
_PS1  equ     1033
	global	_PS2
_PS2  equ     1034
	global	_PSA
_PSA  equ     1035
	global	_RBPU
_RBPU  equ     1039
	global	_RCIE
_RCIE  equ     1125
	global	_RD
_RD  equ     1248
	global	_SYNC
_SYNC  equ     1220
	global	_T0CS
_T0CS  equ     1037
	global	_T0SE
_T0SE  equ     1036
	global	_TMR1IE
_TMR1IE  equ     1120
	global	_TMR2IE
_TMR2IE  equ     1121
	global	_TRISA0
_TRISA0  equ     1064
	global	_TRISA1
_TRISA1  equ     1065
	global	_TRISA2
_TRISA2  equ     1066
	global	_TRISA3
_TRISA3  equ     1067
	global	_TRISA4
_TRISA4  equ     1068
	global	_TRISA5
_TRISA5  equ     1069
	global	_TRISA6
_TRISA6  equ     1070
	global	_TRISA7
_TRISA7  equ     1071
	global	_TRISB0
_TRISB0  equ     1072
	global	_TRISB1
_TRISB1  equ     1073
	global	_TRISB2
_TRISB2  equ     1074
	global	_TRISB3
_TRISB3  equ     1075
	global	_TRISB4
_TRISB4  equ     1076
	global	_TRISB5
_TRISB5  equ     1077
	global	_TRISB6
_TRISB6  equ     1078
	global	_TRISB7
_TRISB7  equ     1079
	global	_TRMT
_TRMT  equ     1217
	global	_TX9
_TX9  equ     1222
	global	_TX9D
_TX9D  equ     1216
	global	_TXEN
_TXEN  equ     1221
	global	_TXIE
_TXIE  equ     1124
	global	_VR0
_VR0  equ     1272
	global	_VR1
_VR1  equ     1273
	global	_VR2
_VR2  equ     1274
	global	_VR3
_VR3  equ     1275
	global	_VREN
_VREN  equ     1279
	global	_VROE
_VROE  equ     1278
	global	_VRR
_VRR  equ     1277
	global	_WR
_WR  equ     1249
	global	_WREN
_WREN  equ     1250
	global	_WRERR
_WRERR  equ     1251
	file	"display7seg.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initationation code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?__delay
?__delay: ;@ 0x0
	global	??__delay
??__delay: ;@ 0x0
	global	__delay$0
__delay$0:	; 4 bytes @ 0x0
	ds	4
	global	??_tick
??_tick: ;@ 0x4
	ds	1
	global	??_vpih
??_vpih: ;@ 0x5
	global	?_tick
?_tick: ;@ 0x5
	ds	1
	global	vpih@digit
vpih@digit:	; 1 bytes @ 0x6
	ds	1
	global	vpih@i
vpih@i:	; 1 bytes @ 0x7
	ds	1
	global	??_main
??_main: ;@ 0x8
	global	?_vpih
?_vpih: ;@ 0x8
	ds	2
	global	?_main
?_main: ;@ 0xA
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	main@i
main@i:	; 1 bytes @ 0x0
	ds	1
;Data sizes: Strings 0, constant 80, data 0, bss 0, persistent 0 stack 0
;Auto spaces:   Size  Autos    Used
; COMMON          14     10      10
; BANK0           80      1       1
; BANK1           80      0       0
; BANK2           48      0       0


;Pointer list with targets:

;vpih@digit	PTR unsigned char  size(1); Largest target is 8
;		 -> zero(CODE[8]), one(CODE[8]), two(CODE[8]), three(CODE[8]), 
;		 -> four(CODE[8]), five(CODE[8]), six(CODE[8]), seven(CODE[8]), 
;		 -> eight(CODE[8]), nine(CODE[8]), 


;Main: autosize = 0, tempsize = 2, incstack = 0, save=0


;Call graph:                      Base Space Used Autos Args Refs Density
;_main                                                3    0  408   0.00
;                                    8 COMMO    2
;                                    0 BANK0    1
;               _tick
;             __delay
;               _vpih
;  _vpih                                              3    0   67   0.00
;                                    5 COMMO    3
;               _tick
;    _tick                                            1    0    0   0.00
;                                    4 COMMO    1
;             __delay
;      __delay                                        0    4    0   0.00
;                                    0 COMMO    4
; Estimated maximum call depth 3
; Address spaces:

;Name               Size   Autos  Total    Cost      Usage
;BITCOMMON            E      0       0       0        0.0%
;NULL                 0      0       0       0        0.0%
;CODE                 0      0       0       0        0.0%
;COMMON               E      A       A       1       71.4%
;BITSFR0              0      0       0       1        0.0%
;SFR0                 0      0       0       1        0.0%
;BITSFR1              0      0       0       2        0.0%
;SFR1                 0      0       0       2        0.0%
;STACK                0      0       0       2        0.0%
;BANK0               50      1       1       3        1.3%
;BANK1               50      0       0       4        0.0%
;BITSFR3              0      0       0       4        0.0%
;SFR3                 0      0       0       4        0.0%
;BANK2               30      0       0       5        0.0%
;SFR2                 0      0       0       5        0.0%
;BITSFR2              0      0       0       5        0.0%
;ABS                  0      0       4       6        0.0%
;BITBANK0            50      0       0       7        0.0%
;BITBANK1            50      0       0       8        0.0%
;BITBANK2            30      0       0       9        0.0%
;DATA                 0      0       4      10        0.0%
;EEDATA              80      0       0    1000        0.0%

	global	_main
psect	maintext,local,class=CODE,delta=2
global __pmaintext
__pmaintext:

; *************** function _main *****************
; Defined at:
;		line 42 in file "X:\Hi-tech\7segment_display\digit.c"
; Parameters:    Size  Location     Type
;		None
; Auto vars:     Size  Location     Type
;  i               1    0[BANK0 ] unsigned char 
; Return value:  Size  Location     Type
;		None               void
; Registers used:
;		wreg, status,2, status,0, pclath, cstack
; Tracked objects:
;		On entry : 17F/0
;		On exit  : 60/0
;		Unchanged: 0/0
; Data sizes:     COMMON   BANK0   BANK1   BANK2
;      Locals:         2       1       0       0
;      Temp:     2
;      Total:    3
; This function calls:
;		_tick
;		__delay
;		_vpih
; This function is called by:
;		Startup code after reset
; This function uses a non-reentrant model
; 
psect	maintext
	file	"X:\Hi-tech\7segment_display\digit.c"
	line	42
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
;digit.c: 30: const char point[8] = {1,0,0,0,0,0,0,0};
;digit.c: 31: const char one[8] = {0,0,0,0,0,1,1,0};
;digit.c: 32: const char two[8] = {0,1,0,1,1,0,1,1};
;digit.c: 33: const char three[8] = {0,1,0,0,1,1,1,1};
;digit.c: 34: const char four[8] = {0,1,1,0,0,1,1,0};
;digit.c: 35: const char five[8] = {0,1,1,0,1,1,0,1};
;digit.c: 36: const char six[8] = {0,1,1,1,1,1,0,1};
;digit.c: 37: const char seven[8] = {0,0,0,0,0,1,1,1};
;digit.c: 38: const char eight[8] = {0,1,1,1,1,1,1,1};
;digit.c: 39: const char nine[8] = {0,1,1,0,1,1,1,1};
	
_main:	
	opt stack 7
; Regs used in _main: [wreg+status,2+status,0+pclath+cstack]
	
l30000353:	
	
l30000354:	
	line	45
;digit.c: 45: TRISB = 0b00000001;
	movlw	(01h)
	bsf	status, 5	;RP0=1, select bank1
	movwf	(134)^080h	;volatile
	
l30000355:	
	line	46
;digit.c: 46: RB3 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	(51/8),(51)&7
	
l30000356:	
	line	47
;digit.c: 47: RB1 = 0;
	bcf	(49/8),(49)&7
	
l30000357:	
	line	48
;digit.c: 48: tick();
	fcall	_tick
	
l30000358:	
	line	49
;digit.c: 49: RB1 = 1;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bsf	(49/8),(49)&7
	
l30000359:	
	line	51
;digit.c: 51: if (RB0 == 0) {
	btfsc	(48/8),(48)&7
	goto	u161
	goto	u160
u161:
	goto	l30000359
u160:
	
l30000360:	
	line	52
;digit.c: 52: _delay((unsigned long)((30)*(4000000/4000.0))); if (RB0 == 0) {
	movlw	39
movwf	(??_main+0+0+1),f
	movlw	245
movwf	(??_main+0+0),f
u317:
	decfsz	(??_main+0+0),f
	goto	u317
	decfsz	(??_main+0+0+1),f
	goto	u317

	
l30000361:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfsc	(48/8),(48)&7
	goto	u171
	goto	u170
u171:
	goto	l30000359
u170:
	
l30000362:	
	line	53
;digit.c: 53: vpih(zero);
	movlw	((_zero-__stringbase))&0ffh
	fcall	_vpih
	
l30000363:	
	line	54
;digit.c: 54: for(i=0;i<10;i++){ _delay((unsigned long)((100)*(4000000/4000.0))); }
	clrf	(main@i)
	
l30000366:	
	movlw	130
movwf	(??_main+0+0+1),f
	movlw	221
movwf	(??_main+0+0),f
u327:
	decfsz	(??_main+0+0),f
	goto	u327
	decfsz	(??_main+0+0+1),f
	goto	u327
	nop2

	
l30000367:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	incf	(main@i),f
	
l30000368:	
	movlw	(0Ah)
	subwf	(main@i),w
	skipc
	goto	u181
	goto	u180
u181:
	goto	l30000366
u180:
	
l30000369:	
	line	55
;digit.c: 55: vpih(one);
	movlw	((_one-__stringbase))&0ffh
	fcall	_vpih
	
l30000370:	
	line	56
;digit.c: 56: for(i=0;i<10;i++){ _delay((unsigned long)((100)*(4000000/4000.0))); }
	clrf	(main@i)
	
l30000373:	
	movlw	130
movwf	(??_main+0+0+1),f
	movlw	221
movwf	(??_main+0+0),f
u337:
	decfsz	(??_main+0+0),f
	goto	u337
	decfsz	(??_main+0+0+1),f
	goto	u337
	nop2

	
l30000374:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	incf	(main@i),f
	
l30000375:	
	movlw	(0Ah)
	subwf	(main@i),w
	skipc
	goto	u191
	goto	u190
u191:
	goto	l30000373
u190:
	
l30000376:	
	line	57
;digit.c: 57: vpih(two);
	movlw	((_two-__stringbase))&0ffh
	fcall	_vpih
	
l30000377:	
	line	58
;digit.c: 58: for(i=0;i<10;i++){ _delay((unsigned long)((100)*(4000000/4000.0))); }
	clrf	(main@i)
	
l30000380:	
	movlw	130
movwf	(??_main+0+0+1),f
	movlw	221
movwf	(??_main+0+0),f
u347:
	decfsz	(??_main+0+0),f
	goto	u347
	decfsz	(??_main+0+0+1),f
	goto	u347
	nop2

	
l30000381:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	incf	(main@i),f
	
l30000382:	
	movlw	(0Ah)
	subwf	(main@i),w
	skipc
	goto	u201
	goto	u200
u201:
	goto	l30000380
u200:
	
l30000383:	
	line	59
;digit.c: 59: vpih(three);
	movlw	((_three-__stringbase))&0ffh
	fcall	_vpih
	
l30000384:	
	line	60
;digit.c: 60: for(i=0;i<10;i++){ _delay((unsigned long)((100)*(4000000/4000.0))); }
	clrf	(main@i)
	
l30000387:	
	movlw	130
movwf	(??_main+0+0+1),f
	movlw	221
movwf	(??_main+0+0),f
u357:
	decfsz	(??_main+0+0),f
	goto	u357
	decfsz	(??_main+0+0+1),f
	goto	u357
	nop2

	
l30000388:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	incf	(main@i),f
	
l30000389:	
	movlw	(0Ah)
	subwf	(main@i),w
	skipc
	goto	u211
	goto	u210
u211:
	goto	l30000387
u210:
	
l30000390:	
	line	61
;digit.c: 61: vpih(four);
	movlw	((_four-__stringbase))&0ffh
	fcall	_vpih
	
l30000391:	
	line	62
;digit.c: 62: for(i=0;i<10;i++){ _delay((unsigned long)((100)*(4000000/4000.0))); }
	clrf	(main@i)
	
l30000394:	
	movlw	130
movwf	(??_main+0+0+1),f
	movlw	221
movwf	(??_main+0+0),f
u367:
	decfsz	(??_main+0+0),f
	goto	u367
	decfsz	(??_main+0+0+1),f
	goto	u367
	nop2

	
l30000395:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	incf	(main@i),f
	
l30000396:	
	movlw	(0Ah)
	subwf	(main@i),w
	skipc
	goto	u221
	goto	u220
u221:
	goto	l30000394
u220:
	
l30000397:	
	line	63
;digit.c: 63: vpih(five);
	movlw	((_five-__stringbase))&0ffh
	fcall	_vpih
	
l30000398:	
	line	64
;digit.c: 64: for(i=0;i<10;i++){ _delay((unsigned long)((100)*(4000000/4000.0))); }
	clrf	(main@i)
	
l30000401:	
	movlw	130
movwf	(??_main+0+0+1),f
	movlw	221
movwf	(??_main+0+0),f
u377:
	decfsz	(??_main+0+0),f
	goto	u377
	decfsz	(??_main+0+0+1),f
	goto	u377
	nop2

	
l30000402:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	incf	(main@i),f
	
l30000403:	
	movlw	(0Ah)
	subwf	(main@i),w
	skipc
	goto	u231
	goto	u230
u231:
	goto	l30000401
u230:
	
l30000404:	
	line	65
;digit.c: 65: vpih(six);
	movlw	((_six-__stringbase))&0ffh
	fcall	_vpih
	
l30000405:	
	line	66
;digit.c: 66: for(i=0;i<10;i++){ _delay((unsigned long)((100)*(4000000/4000.0))); }
	clrf	(main@i)
	
l30000408:	
	movlw	130
movwf	(??_main+0+0+1),f
	movlw	221
movwf	(??_main+0+0),f
u387:
	decfsz	(??_main+0+0),f
	goto	u387
	decfsz	(??_main+0+0+1),f
	goto	u387
	nop2

	
l30000409:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	incf	(main@i),f
	
l30000410:	
	movlw	(0Ah)
	subwf	(main@i),w
	skipc
	goto	u241
	goto	u240
u241:
	goto	l30000408
u240:
	
l30000411:	
	line	67
;digit.c: 67: vpih(seven);
	movlw	((_seven-__stringbase))&0ffh
	fcall	_vpih
	
l30000412:	
	line	68
;digit.c: 68: for(i=0;i<10;i++){ _delay((unsigned long)((100)*(4000000/4000.0))); }
	clrf	(main@i)
	
l30000415:	
	movlw	130
movwf	(??_main+0+0+1),f
	movlw	221
movwf	(??_main+0+0),f
u397:
	decfsz	(??_main+0+0),f
	goto	u397
	decfsz	(??_main+0+0+1),f
	goto	u397
	nop2

	
l30000416:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	incf	(main@i),f
	
l30000417:	
	movlw	(0Ah)
	subwf	(main@i),w
	skipc
	goto	u251
	goto	u250
u251:
	goto	l30000415
u250:
	
l30000418:	
	line	69
;digit.c: 69: vpih(eight);
	movlw	((_eight-__stringbase))&0ffh
	fcall	_vpih
	
l30000419:	
	line	70
;digit.c: 70: for(i=0;i<10;i++){ _delay((unsigned long)((100)*(4000000/4000.0))); }
	clrf	(main@i)
	
l30000422:	
	movlw	130
movwf	(??_main+0+0+1),f
	movlw	221
movwf	(??_main+0+0),f
u407:
	decfsz	(??_main+0+0),f
	goto	u407
	decfsz	(??_main+0+0+1),f
	goto	u407
	nop2

	
l30000423:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	incf	(main@i),f
	
l30000424:	
	movlw	(0Ah)
	subwf	(main@i),w
	skipc
	goto	u261
	goto	u260
u261:
	goto	l30000422
u260:
	
l30000425:	
	line	71
;digit.c: 71: vpih(nine);
	movlw	((_nine-__stringbase))&0ffh
	fcall	_vpih
	
l30000426:	
	line	72
;digit.c: 72: for(i=0;i<10;i++){ _delay((unsigned long)((100)*(4000000/4000.0))); }
	clrf	(main@i)
	
l30000429:	
	movlw	130
movwf	(??_main+0+0+1),f
	movlw	221
movwf	(??_main+0+0),f
u417:
	decfsz	(??_main+0+0),f
	goto	u417
	decfsz	(??_main+0+0+1),f
	goto	u417
	nop2

	
l30000430:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	incf	(main@i),f
	
l30000431:	
	movlw	(0Ah)
	subwf	(main@i),w
	skipc
	goto	u271
	goto	u270
u271:
	goto	l30000429
u270:
	goto	l30000359
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
; =============== function _main ends ============

psect	maintext
	line	84
	signat	_main,88
	global	_vpih
psect	text22,local,class=CODE,delta=2
global __ptext22
__ptext22:

; *************** function _vpih *****************
; Defined at:
;		line 20 in file "X:\Hi-tech\7segment_display\digit.c"
; Parameters:    Size  Location     Type
;  digit           1    wreg     PTR unsigned char 
;		 -> zero(8), one(8), two(8), three(8), 
;		 -> four(8), five(8), six(8), seven(8), 
;		 -> eight(8), nine(8), 
; Auto vars:     Size  Location     Type
;  digit           1    6[COMMON] PTR unsigned char 
;		 -> zero(8), one(8), two(8), three(8), 
;		 -> four(8), five(8), six(8), seven(8), 
;		 -> eight(8), nine(8), 
;  i               1    7[COMMON] unsigned char 
; Return value:  Size  Location     Type
;		None               void
; Registers used:
;		wreg, status,2, status,0, pclath, cstack
; Tracked objects:
;		On entry : 60/0
;		On exit  : 60/0
;		Unchanged: 0/0
; Data sizes:     COMMON   BANK0   BANK1   BANK2
;      Locals:         3       0       0       0
;      Temp:     1
;      Total:    3
; This function calls:
;		_tick
; This function is called by:
;		_main
; This function uses a non-reentrant model
; 
psect	text22
	file	"X:\Hi-tech\7segment_display\digit.c"
	line	20
	global	__size_of_vpih
	__size_of_vpih	equ	__end_of_vpih-_vpih
;digit.c: 20: void vpih(unsigned char digit[8]) {
	
_vpih:	
	opt stack 6
; Regs used in _vpih: [wreg+status,2+status,0+pclath+cstack]
;vpih@digit stored from wreg
	movwf	(vpih@digit)
	
l30000432:	
	
l30000433:	
	line	22
;digit.c: 22: RB7 = 0;
	bcf	(55/8),(55)&7
	line	23
;digit.c: 23: for (i=0;i<8;i++) {
	clrf	(vpih@i)
	
l30000436:	
	line	24
;digit.c: 24: RB3 = digit[i];
	movf	(vpih@digit),w
	addwf	(vpih@i),w
	FNCALL _vpih,stringtab
	fcall	stringdir
	movwf	(??_vpih+0+0)
	rrf	(??_vpih+0+0),w
	btfsc	status,0
	goto	u281
	goto	u280
	
u281:
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bsf	(51/8),(51)&7
	goto	u294
u280:
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(51/8),(51)&7
u294:
	
l30000437:	
	line	25
;digit.c: 25: tick();
	fcall	_tick
	
l30000438:	
	line	23
	incf	(vpih@i),f
	
l30000439:	
	movlw	(08h)
	subwf	(vpih@i),w
	skipc
	goto	u301
	goto	u300
u301:
	goto	l30000436
u300:
	
l4:	
	line	27
;digit.c: 26: }
;digit.c: 27: RB7 = 1;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bsf	(55/8),(55)&7
	
l2:	
	return
	opt stack 0
GLOBAL	__end_of_vpih
	__end_of_vpih:
; =============== function _vpih ends ============

psect	text23,local,class=CODE,delta=2
global __ptext23
__ptext23:
	line	28
	signat	_vpih,4216
	global	_tick

; *************** function _tick *****************
; Defined at:
;		line 12 in file "X:\Hi-tech\7segment_display\digit.c"
; Parameters:    Size  Location     Type
;		None
; Auto vars:     Size  Location     Type
;		None
; Return value:  Size  Location     Type
;		None               void
; Registers used:
;		wreg
; Tracked objects:
;		On entry : 60/0
;		On exit  : 0/0
;		Unchanged: 0/0
; Data sizes:     COMMON   BANK0   BANK1   BANK2
;      Locals:         1       0       0       0
;      Temp:     1
;      Total:    1
; This function calls:
;		__delay
; This function is called by:
;		_vpih
;		_main
; This function uses a non-reentrant model
; 
psect	text23
	file	"X:\Hi-tech\7segment_display\digit.c"
	line	12
	global	__size_of_tick
	__size_of_tick	equ	__end_of_tick-_tick
;pic16f62xa.h: 18: volatile unsigned char INDF @ 0x00;
;pic16f62xa.h: 19: volatile unsigned char TMR0 @ 0x01;
;pic16f62xa.h: 20: volatile unsigned char PCL @ 0x02;
;pic16f62xa.h: 21: volatile unsigned char STATUS @ 0x03;
;pic16f62xa.h: 22: unsigned char FSR @ 0x04;
;pic16f62xa.h: 23: volatile unsigned char PORTA @ 0x05;
;pic16f62xa.h: 24: volatile unsigned char PORTB @ 0x06;
;pic16f62xa.h: 25: volatile unsigned char PCLATH @ 0x0A;
;pic16f62xa.h: 26: volatile unsigned char INTCON @ 0x0B;
;pic16f62xa.h: 27: volatile unsigned char PIR1 @ 0x0C;
	
_tick:	
	opt stack 5
; Regs used in _tick: [wreg]
	line	13
	
l30000440:	
;digit.c: 13: RB2 = 1;
	bsf	(50/8),(50)&7
	
l30000441:	
	line	14
;digit.c: 14: _delay((unsigned long)((100)*(4000000/4000000.0)));
	movlw	33
movwf	(??_tick+0+0),f
u427:
decfsz	(??_tick+0+0),f
	goto	u427

	
l30000442:	
	line	15
;digit.c: 15: RB2 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(50/8),(50)&7
	line	16
;digit.c: 16: _delay((unsigned long)((100)*(4000000/4000000.0)));
	movlw	33
movwf	(??_tick+0+0),f
u437:
decfsz	(??_tick+0+0),f
	goto	u437

	
l1:	
	return
	opt stack 0
GLOBAL	__end_of_tick
	__end_of_tick:
; =============== function _tick ends ============

psect	text24,local,class=CODE,delta=2
global __ptext24
__ptext24:
	line	17
	signat	_tick,88
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	end
