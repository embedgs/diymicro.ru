#include <htc.h>

#define _XTAL_FREQ 4000000
#define CK RB2
#define RESET RB1
#define DATA RB3
#define TRIG RB7
__CONFIG(MCLREN & UNPROTECT & WDTDIS);



void tick() {											//Clock for shift register
CK = 1;
__delay_us(100);
CK = 0;
__delay_us(100);
}

void DelayS(unsigned char second)						//Delay n second function
{
	for (unsigned char i=0; i<second*10; i++)
		__delay_ms(100);
}

void vpih(unsigned char digit) {						//pushing data to shift register
unsigned char i=0;	
TRIG = 0;
for (i=0;i<8;i++) {
     
     DATA = digit & 0b00000001;
     digit >>= 1;
     tick();
     }     
TRIG = 1;
}  

const unsigned char digits[11] =						//constant array for numbers
{
0b11111100,								//0
0b01100000,								//1
0b11011010,								//2
0b11110010,								//3
0b01100110,								//4
0b10110110,								//5
0b10111110,								//6
0b11100000,								//7
0b11111110,								//8
0b11110110,								//9
0b00000001								//.
};


void main() {
unsigned char i=0;

TRISB = 0b00000001;
DATA = 0;
RESET = 0;
tick();
RESET = 1;
for (;;) {
  if (RB0 == 0) {
  __delay_ms(30); 
    if (RB0 == 0) 
    {  
		for (unsigned char i=0;i<11;i++)
		{	
				vpih(digits[i]); 
				DelayS(1);
		}		
    }
  }	

}


}