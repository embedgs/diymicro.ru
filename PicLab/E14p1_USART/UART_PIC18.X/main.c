#include <xc.h>
#include <plib/usart.h>

#define _XTAL_FREQ 12000000 //The speed of your internal(or)external oscillator

#pragma config WDTEN = OFF, LVP = OFF, FOSC = HS  

unsigned char UARTConfig = 0;
unsigned char baud = 0;
char txbyte = 'U';         //наш байт для пересылки
unsigned char Rxdata[10];

void NumToUart(unsigned int Num)                                                //Число в уарт
{

  unsigned int bignum = 10000;
  unsigned char numtemp = 5;

  while(numtemp>0)                                                             //Определяем сколько разрядов имеет наше число
  {
    if (Num/bignum)
        break;
    numtemp--;
    bignum = bignum / 10;  
  }  



  for (unsigned char i = numtemp; i>0; i--)
    {
      WriteUSART( (Num - (Num/(bignum*10))*bignum*10 )/bignum + '0');         //Выталкиеваем все разряды - от старшего к младшему
      while(BusyUSART());                                                       //Ждем пока освободится модуль иначе будут прострелы
      bignum = bignum/10;
    }

}

void main() 
{

 ANSEL = 0;                                                                   //Отключаем аналоговые буфера
 ANSELH = 0;

 TRISB7 = 0; //TX pin set as output
 TRISB5 = 1; //RX pin set as input
 TRISB6 = 1; //RX pin set as input

 UARTConfig = USART_TX_INT_OFF & USART_RX_INT_OFF & USART_ASYNCH_MODE & USART_EIGHT_BIT & USART_BRGH_HIGH ;    //прерывания выключены, асинхронный режим, 8 бит, высокоскоростной режим
 baud = 77;                 //Focs/(9600*16) - 1
 OpenUSART(UARTConfig,baud);


 WriteUSART(txbyte);        //Пишем наш байт
 putsUSART( (char *) "\r\nputs test\r\n" );
 putsUSART( (char *) "Welcome to Diymicro.ru\r\n" );
 putsUSART( (char *) "The number is " );
 NumToUart(1283);
 

 getsUSART(Rxdata, 10);
 while(BusyUSART());   
 putsUSART( (char *) "\r\nAll bytes have recieved, your message is - \r\n" );
 putsUSART(Rxdata);
 
 putsUSART( (char *) " \r\nNow, just print what you want :) \r\n" );

 while(1)
 {
      while(!DataRdyUSART());
      txbyte=ReadUSART();
      while(BusyUSART()); 
      if (txbyte)
        {
          WriteUSART(txbyte);
          while(BusyUSART());
        }

  }//while(1)

}//main()
