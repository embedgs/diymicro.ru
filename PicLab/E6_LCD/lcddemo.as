opt subtitle "HI-TECH Software Omniscient Code Generator (PRO mode) build 6446"

opt pagewidth 120

	opt pm

	processor	16F628A
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 5 "X:\Hi-tech\LCD\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 5 "X:\Hi-tech\LCD\main.c"
	dw 0x3FFB & 0x3FFF & 0x3FFF & 0x3F7F & 0x3FEE ;#
	FNCALL	_main,_lcd_init
	FNCALL	_main,_lcd_goto
	FNCALL	_main,_lcd_puts
	FNCALL	_lcd_init,_lcd_write
	FNCALL	_lcd_goto,_lcd_write
	FNCALL	_lcd_puts,_lcd_write
	FNROOT	_main
	global	_CMCON
psect	text59,local,class=CODE,delta=2
global __ptext59
__ptext59:
_CMCON	set	31
	global	_PORTB
_PORTB	set	6
	global	_RA1
_RA1	set	41
	global	_RA2
_RA2	set	42
	global	_RA3
_RA3	set	43
	global	_TRISA
_TRISA	set	133
	global	_TRISB
_TRISB	set	134
psect	strings,class=STRING,delta=2
global __pstrings
__pstrings:
;	global	stringdir,stringtab,__stringbase
stringtab:
;	String table - string pointers are 1 byte each
stringcode:stringdir:
movlw high(stringdir)
movwf pclath
movf fsr,w
incf fsr
	addwf pc
__stringbase:
	retlw	0
psect	strings
	
STR_2:	
	retlw	83	;'S'
	retlw	65	;'A'
	retlw	82	;'R'
	retlw	71	;'G'
	retlw	69	;'E'
	retlw	46	;'.'
	retlw	80	;'P'
	retlw	80	;'P'
	retlw	46	;'.'
	retlw	85	;'U'
	retlw	65	;'A'
	retlw	0
psect	strings
	
STR_1:	
	retlw	80	;'P'
	retlw	73	;'I'
	retlw	67	;'C'
	retlw	32	;' '
	retlw	109	;'m'
	retlw	97	;'a'
	retlw	110	;'n'
	retlw	117	;'u'
	retlw	97	;'a'
	retlw	108	;'l'
	retlw	0
psect	strings
	file	"lcddemo.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_lcd_write
?_lcd_write:	; 0 bytes @ 0x0
	global	??_lcd_write
??_lcd_write:	; 0 bytes @ 0x0
	global	?_lcd_puts
?_lcd_puts:	; 0 bytes @ 0x0
	global	?_lcd_goto
?_lcd_goto:	; 0 bytes @ 0x0
	global	?_lcd_init
?_lcd_init:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 0 bytes @ 0x0
	ds	1
	global	lcd_write@c
lcd_write@c:	; 1 bytes @ 0x1
	ds	1
	global	??_lcd_puts
??_lcd_puts:	; 0 bytes @ 0x2
	global	??_lcd_goto
??_lcd_goto:	; 0 bytes @ 0x2
	global	??_lcd_init
??_lcd_init:	; 0 bytes @ 0x2
	global	lcd_puts@s
lcd_puts@s:	; 1 bytes @ 0x2
	global	lcd_goto@pos
lcd_goto@pos:	; 1 bytes @ 0x2
	ds	2
	global	??_main
??_main:	; 0 bytes @ 0x4
;;Data sizes: Strings 23, constant 0, data 0, bss 0, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      4       4
;; BANK0           80      0       0
;; BANK1           80      0       0
;; BANK2           48      0       0

;;
;; Pointer list with targets:

;; lcd_puts@s	PTR const unsigned char  size(1) Largest target is 12
;;		 -> STR_2(CODE[12]), STR_1(CODE[11]), 
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   _main->_lcd_init
;;   _lcd_init->_lcd_write
;;   _lcd_goto->_lcd_write
;;   _lcd_puts->_lcd_write
;;
;; Critical Paths under _main in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 0, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 0     0      0      90
;;                           _lcd_init
;;                           _lcd_goto
;;                           _lcd_puts
;; ---------------------------------------------------------------------------------
;; (1) _lcd_init                                             3     3      0      15
;;                                              2 COMMON     2     2      0
;;                          _lcd_write
;; ---------------------------------------------------------------------------------
;; (1) _lcd_goto                                             1     1      0      30
;;                                              2 COMMON     1     1      0
;;                          _lcd_write
;; ---------------------------------------------------------------------------------
;; (1) _lcd_puts                                             1     1      0      45
;;                                              2 COMMON     1     1      0
;;                          _lcd_write
;; ---------------------------------------------------------------------------------
;; (2) _lcd_write                                            2     2      0      15
;;                                              0 COMMON     2     2      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _lcd_init
;;     _lcd_write
;;   _lcd_goto
;;     _lcd_write
;;   _lcd_puts
;;     _lcd_write
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA              80      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      4       4       1       28.6%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       2       2        0.0%
;;BANK0               50      0       0       3        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;ABS                  0      0       0       4        0.0%
;;BITBANK0            50      0       0       5        0.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK2            30      0       0       8        0.0%
;;BANK2               30      0       0       9        0.0%
;;DATA                 0      0       0      10        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 7 in file "X:\Hi-tech\LCD\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_lcd_init
;;		_lcd_goto
;;		_lcd_puts
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"X:\Hi-tech\LCD\main.c"
	line	7
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 8
; Regs used in _main: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	8
	
l1089:	
;main.c: 8: lcd_init();
	fcall	_lcd_init
	line	10
;main.c: 10: lcd_goto(0);
	movlw	(0)
	fcall	_lcd_goto
	line	13
	
l1091:	
;main.c: 13: lcd_puts("PIC manual");
	movlw	((STR_1-__stringbase))&0ffh
	fcall	_lcd_puts
	line	15
	
l1093:	
;main.c: 15: lcd_goto(0x40);
	movlw	(040h)
	fcall	_lcd_goto
	line	16
;main.c: 16: lcd_puts("SARGE.PP.UA");
	movlw	((STR_2-__stringbase))&0ffh
	fcall	_lcd_puts
	line	19
;main.c: 19: for(;;);
	
l676:	
	goto	l676
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

psect	maintext
	line	22
	signat	_main,88
	global	_lcd_init
psect	text60,local,class=CODE,delta=2
global __ptext60
__ptext60:

;; *************** function _lcd_init *****************
;; Defined at:
;;		line 87 in file "X:\Hi-tech\LCD\lcd.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  init_value      1    0        unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          2       0       0       0
;;      Totals:         2       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_lcd_write
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text60
	file	"X:\Hi-tech\LCD\lcd.c"
	line	87
	global	__size_of_lcd_init
	__size_of_lcd_init	equ	__end_of_lcd_init-_lcd_init
	
_lcd_init:	
	opt	stack 7
; Regs used in _lcd_init: [wreg+status,2+status,0+pclath+cstack]
	line	90
	
l1069:	
;lcd.c: 92: init_value = 2;
	movlw	(07h)
	movwf	(31)	;volatile
	line	93
	
l1071:	
;lcd.c: 93: TRISA=0;
	bsf	status, 5	;RP0=1, select bank1
	clrf	(133)^080h	;volatile
	line	94
	
l1073:	
;lcd.c: 94: TRISB=0;
	clrf	(134)^080h	;volatile
	line	95
	
l1075:	
;lcd.c: 95: RA1 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	(41/8),(41)&7
	line	96
	
l1077:	
;lcd.c: 96: RA3 = 0;
	bcf	(43/8),(43)&7
	line	97
	
l1079:	
;lcd.c: 97: RA2 = 0;
	bcf	(42/8),(42)&7
	line	98
;lcd.c: 98: PORTB = 0xFF;
	movlw	(0FFh)
	movwf	(6)	;volatile
	line	99
;lcd.c: 99: _delay((unsigned long)((40)*(4000000/4000.0)));
	opt asmopt_off
movlw	52
movwf	((??_lcd_init+0)+0+1),f
	movlw	241
movwf	((??_lcd_init+0)+0),f
u27:
	decfsz	((??_lcd_init+0)+0),f
	goto	u27
	decfsz	((??_lcd_init+0)+0+1),f
	goto	u27
	nop2
opt asmopt_on

	line	130
	
l1081:	
;lcd.c: 130: lcd_write(0b00111000);
	movlw	(038h)
	fcall	_lcd_write
	line	131
	
l1083:	
;lcd.c: 131: lcd_write(0b00001111);
	movlw	(0Fh)
	fcall	_lcd_write
	line	132
	
l1085:	
;lcd.c: 132: lcd_write(0b00000001);
	movlw	(01h)
	fcall	_lcd_write
	line	133
	
l1087:	
;lcd.c: 133: _delay((unsigned long)((2)*(4000000/4000.0)));
	opt asmopt_off
movlw	3
movwf	((??_lcd_init+0)+0+1),f
	movlw	151
movwf	((??_lcd_init+0)+0),f
u37:
	decfsz	((??_lcd_init+0)+0),f
	goto	u37
	decfsz	((??_lcd_init+0)+0+1),f
	goto	u37
	nop2
opt asmopt_on

	line	134
;lcd.c: 134: lcd_write(0b00000110);
	movlw	(06h)
	fcall	_lcd_write
	line	139
	
l345:	
	return
	opt stack 0
GLOBAL	__end_of_lcd_init
	__end_of_lcd_init:
;; =============== function _lcd_init ends ============

	signat	_lcd_init,88
	global	_lcd_goto
psect	text61,local,class=CODE,delta=2
global __ptext61
__ptext61:

;; *************** function _lcd_goto *****************
;; Defined at:
;;		line 79 in file "X:\Hi-tech\LCD\lcd.c"
;; Parameters:    Size  Location     Type
;;  pos             1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  pos             1    2[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         1       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_lcd_write
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text61
	file	"X:\Hi-tech\LCD\lcd.c"
	line	79
	global	__size_of_lcd_goto
	__size_of_lcd_goto	equ	__end_of_lcd_goto-_lcd_goto
	
_lcd_goto:	
	opt	stack 7
; Regs used in _lcd_goto: [wreg+status,2+status,0+pclath+cstack]
;lcd_goto@pos stored from wreg
	movwf	(lcd_goto@pos)
	line	80
	
l1065:	
;lcd.c: 80: RA1 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(41/8),(41)&7
	line	81
	
l1067:	
;lcd.c: 81: lcd_write(0x80+pos);
	movf	(lcd_goto@pos),w
	addlw	080h
	fcall	_lcd_write
	line	82
	
l342:	
	return
	opt stack 0
GLOBAL	__end_of_lcd_goto
	__end_of_lcd_goto:
;; =============== function _lcd_goto ends ============

	signat	_lcd_goto,4216
	global	_lcd_puts
psect	text62,local,class=CODE,delta=2
global __ptext62
__ptext62:

;; *************** function _lcd_puts *****************
;; Defined at:
;;		line 57 in file "X:\Hi-tech\LCD\lcd.c"
;; Parameters:    Size  Location     Type
;;  s               1    wreg     PTR const unsigned char 
;;		 -> STR_2(12), STR_1(11), 
;; Auto vars:     Size  Location     Type
;;  s               1    2[COMMON] PTR const unsigned char 
;;		 -> STR_2(12), STR_1(11), 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         1       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_lcd_write
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text62
	file	"X:\Hi-tech\LCD\lcd.c"
	line	57
	global	__size_of_lcd_puts
	__size_of_lcd_puts	equ	__end_of_lcd_puts-_lcd_puts
	
_lcd_puts:	
	opt	stack 7
; Regs used in _lcd_puts: [wreg-fsr0h+status,2+status,0+pclath+cstack]
;lcd_puts@s stored from wreg
	movwf	(lcd_puts@s)
	line	58
	
l1057:	
;lcd.c: 58: RA1 = 1;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bsf	(41/8),(41)&7
	line	59
;lcd.c: 59: while(*s)
	goto	l1063
	line	60
	
l1059:	
;lcd.c: 60: lcd_write(*s++);
	movf	(lcd_puts@s),w
	movwf	fsr0
	fcall	stringdir
	fcall	_lcd_write
	
l1061:	
	incf	(lcd_puts@s),f
	line	59
	
l1063:	
	movf	(lcd_puts@s),w
	movwf	fsr0
	fcall	stringdir
	iorlw	0
	skipz
	goto	u11
	goto	u10
u11:
	goto	l1059
u10:
	line	61
	
l336:	
	return
	opt stack 0
GLOBAL	__end_of_lcd_puts
	__end_of_lcd_puts:
;; =============== function _lcd_puts ends ============

	signat	_lcd_puts,4216
	global	_lcd_write
psect	text63,local,class=CODE,delta=2
global __ptext63
__ptext63:

;; *************** function _lcd_write *****************
;; Defined at:
;;		line 24 in file "X:\Hi-tech\LCD\lcd.c"
;; Parameters:    Size  Location     Type
;;  c               1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  c               1    1[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         1       0       0       0
;;      Temps:          1       0       0       0
;;      Totals:         2       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_lcd_puts
;;		_lcd_goto
;;		_lcd_init
;; This function uses a non-reentrant model
;;
psect	text63
	file	"X:\Hi-tech\LCD\lcd.c"
	line	24
	global	__size_of_lcd_write
	__size_of_lcd_write	equ	__end_of_lcd_write-_lcd_write
	
_lcd_write:	
	opt	stack 6
; Regs used in _lcd_write: [wreg]
;lcd_write@c stored from wreg
	line	35
	movwf	(lcd_write@c)
	
l1051:	
;lcd.c: 35: PORTB = c;
	movf	(lcd_write@c),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(6)	;volatile
	line	36
	
l1053:	
;lcd.c: 36: ((RA3 = 1),(RA3=0));
	bsf	(43/8),(43)&7
	
l1055:	
	bcf	(43/8),(43)&7
	line	37
;lcd.c: 37: _delay((unsigned long)((40)*(4000000/4000000.0)));
	opt asmopt_off
movlw	13
movwf	(??_lcd_write+0)+0,f
u47:
decfsz	(??_lcd_write+0)+0,f
	goto	u47
opt asmopt_on

	line	39
	
l327:	
	return
	opt stack 0
GLOBAL	__end_of_lcd_write
	__end_of_lcd_write:
;; =============== function _lcd_write ends ============

	signat	_lcd_write,4216
psect	text64,local,class=CODE,delta=2
global __ptext64
__ptext64:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
