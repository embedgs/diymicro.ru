/*
 *	LCD interface example
 *	Uses routines from delay.c
 *	This code will interface to a standard LCD controller
 *	like the Hitachi HD44780. It uses it in 4 bit mode, with
 *	the hardware connected as follows (the standard 14 pin 
 *	LCD connector is used):
 *	
 *	PORTD bits 0-3 are connected to the LCD data bits 4-7 (high nibble)
 *	PORTA bit 3 is connected to the LCD RS input (register select)
 *	PORTA bit 1 is connected to the LCD EN bit (enable)
 *	
 *	To use these routines, set up the port I/O (TRISA, TRISD) then
 *	call lcd_init(), then other routines as required.
 *	
 */

#include "lcd.h"

/* write a byte to the LCD in 4 bit mode */

void
lcd_write(unsigned char c)
{
	
	
	
	/*
	__delay_us(40);
	LCD_DATA = ( ( c >> 4 ) & 0x0F );
	LCD_STROBE();
	LCD_DATA = ( c & 0x0F );
	LCD_STROBE();
	*/
	LCD_DATA = c;
	LCD_STROBE();
	__delay_us(40);
	
}

/*
 * 	Clear and home the LCD
 */

void
lcd_clear(void)
{
	LCD_RS = 0;
	lcd_write(0x1);
	__delay_ms(2);
}

/* write a string of chars to the LCD */

void
lcd_puts(const char * s)
{
	LCD_RS = 1;	// write characters
	while(*s)
		lcd_write(*s++);
}

/* write one character to the LCD */

void
lcd_putch(char c)
{
	LCD_RS = 1;	// write characters
	lcd_write( c );
}


/*
 * Go to the specified position
 */

void
lcd_goto(unsigned char pos)
{
	LCD_RS = 0;
	lcd_write(0x80+pos);
}
	
/* initialise the LCD - put into 4 bit mode */
void
lcd_init()
{
	char init_value;

	CMCON  = 0x07;	// Disable analog pins on PORTA

	init_value = 2;
	TRISA=0;
	TRISB=0;
	LCD_RS = 0;
	LCD_EN = 0;
	LCD_RW = 0;
	LCD_DATA = 0xFF;
	__delay_ms(40);	// wait 15mSec after power applied,
	/*LCD_DATA	 = init_value;
	LCD_STROBE();
	__delay_ms(5);
	LCD_STROBE();
	__delay_us(200);
	LCD_STROBE();
	__delay_us(200);
	LCD_DATA = 2;	// Four bit mode
	LCD_STROBE();

	lcd_write(0x28); // Set interface length
	lcd_write(0xF); // Display On, Cursor On, Cursor Blink
	lcd_clear();	// Clear screen
	lcd_write(0x6); // Set entry Mode
	
lcd_write(0b00000010);  //�� �����
 
lcd_write(0b00101000);  //4 ������ �����

lcd_write(0b00001100);  //������ � ������

lcd_write(0b00000001);   

__delay_ms(2);

lcd_write(0b00000110);
	
	*/
//lcd_write(0b00000010);

lcd_write(0b00111000);
lcd_write(0b00001111);
lcd_write(0b00000001);
__delay_ms(2);
lcd_write(0b00000110);	
	
	
	
	
}
