#include <htc.h>
#include "lcd.h"
#define _XTAL_FREQ 4000000

__CONFIG(WDTDIS & UNPROTECT & MCLREN & LVPDIS & HS);

void main() {
    lcd_init();
	
	lcd_goto(0);	// select first line
	
	
	lcd_puts("PIC manual");
	
	lcd_goto(0x40);	// Select second line
	lcd_puts("SARGE.PP.UA"); 
    
	
	for(;;);


}