#include <htc.h>
#include <stdio.h>
#include "usart.h"
#include "rtc.h"
#include "i2c.h"

unsigned char BCDconv (unsigned char source) {
  unsigned char temp_min=0;
  unsigned char temp_maj=0;
   temp_min = source&15;
   temp_maj = source >> 4;
   //printf("\r\n minor - %x", temp_min);
   //printf("\r\n major - %x", temp_maj);
   temp_maj *= 10;
   //printf("\r\n major - %x", temp_maj);
   
   return temp_maj+temp_min;
}

unsigned char DCBconv (unsigned char source) {
 unsigned char temp_min=0;
 unsigned char temp_maj=0;
 temp_maj = source/10 ;
 temp_min = source - temp_maj*10;
 temp_maj <<= 4;

 return temp_maj+temp_min;

}

void SetMin(unsigned char minutes) {        //���������� �������� � ������
 	I2CStart();            
    I2CSend(0b11010000);
    I2CSend(0x01);
    I2CSend(DCBconv(minutes));
    I2CStop();
}

unsigned char ReadMin() {
	unsigned char temp = 0;
	I2CStart();            //������ �������� �����
    I2CSend(0b11010000);
	I2CSend(0x01);
	I2CRestart();
	I2CSend(0b11010001);
	temp = I2CRead();
	I2CNak();
	I2CStop();

    return BCDconv(temp);
}

void SetHour(unsigned char hours) {        //���������� �������� � ����
 	I2CStart();            
    I2CSend(0b11010000);
    I2CSend(0x02);
    I2CSend(DCBconv(hours));
    I2CStop();
}

unsigned char ReadHour() {
	unsigned char temp = 0; 
	I2CStart();            //������ �������� �����
    I2CSend(0b11010000);
	I2CSend(0x02);
	I2CRestart();
	I2CSend(0b11010001);
	
	temp = I2CRead();
	I2CNak();
	I2CStop();
    
	
	return BCDconv(temp);
}

void SetSeconds(unsigned char seconds) {        //���������� �������� � �������
 	I2CStart();            
    I2CSend(0b11010000);
    I2CSend(0x00);
    I2CSend(DCBconv(seconds));
    I2CStop();
}

unsigned char ReadSeconds() {
	unsigned char temp = 0;
	I2CStart();            //������ �������� �����
    I2CSend(0b11010000);
	I2CSend(0x00);
	I2CRestart();
	I2CSend(0b11010001);
	temp = I2CRead();
	I2CNak();
	I2CStop();

    return BCDconv(temp);
}

void ShowTime() {

    printf("\fTime - %d :", ReadHour());
	
    printf(" %d :", ReadMin());
	
	printf(" %d ", ReadSeconds());

	printf("\r\n*********************");
	
	printf("\r\n Menu");
	printf("\r\n 1 - Reload time");
	printf("\r\n 2 - Setup time");

}

void SetupTime() {
    unsigned char state = 1;
	unsigned char temp_min = 0;
	unsigned char temp_maj = 0;
	unsigned char temp = 0;
	
	while(state){
	     printf("\fEnter hours - ");
	     temp_maj = getch();
	     printf(" %c", temp_maj);
	     temp_min = getch();
	     printf("%c", temp_min);
	     temp = getch();
		 if (temp == 13) {
		                 temp_maj -= 48;
		                 temp_min -= 48;
	                     temp = temp_maj*10 + temp_min;
		                       if (temp < 24) {
						                      SetHour(temp);
		                                      state = 0;
											  }  										  
				   
						 }
    }
	
	
	state = 1;
	while(state){
	     printf("\fEnter minutes - ");
	     temp_maj = getch();
	     printf(" %c", temp_maj);
	     temp_min = getch();
	     printf("%c", temp_min);
	     temp = getch();
		 if (temp == 13) {
		                 temp_maj -= 48;
		                 temp_min -= 48;
	                     temp = temp_maj*10 + temp_min;
		                       if (temp < 61) {
						                      SetMin(temp);
		                                      state = 0;
											  }  						 
						 }
    }
	state = 1;
    while(state){
	     printf("\fEnter seconds - ");
	     temp_maj = getch();
	     printf(" %c", temp_maj);
	     temp_min = getch();
	     printf("%c", temp_min);
	     temp = getch();
		 if (temp == 13) {
		                 temp_maj -= 48;
		                 temp_min -= 48;
	                     temp = temp_maj*10 + temp_min;
		                       if (temp < 61) {
						                      SetSeconds(temp);
		                                      state = 0;
											  }  
						 }
    }

    ShowTime();

}