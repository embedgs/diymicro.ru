#include <htc.h>
#ifndef _LCD_H_
#define _LCD_H_

#ifndef _XTAL_FREQ
 // Unless specified elsewhere, 4MHz system frequency is assumed
 #define _XTAL_FREQ 4000000
#endif

#define	LCD_RS RD2
#define LCD_EN RD3

#define lcd4b RD4
#define lcd5b RD5
#define lcd6b RD6
#define lcd7b RD7


#define	lcd_cursor(x)	lcd_write(((x)&0x7F)|0x80)
#define    LCD_STROBE()    ((LCD_EN = 1),(LCD_EN=0))
#define testbit(data,bitno) ((data>>bitno)&0x01)

extern void lcd_write(unsigned char c);
extern void lcd_clear(void);
extern void lcd_puts(const char * s);
extern void lcd_putch(char c);
extern void lcd_goto(unsigned char pos);
extern void lcd_init();

#endif
