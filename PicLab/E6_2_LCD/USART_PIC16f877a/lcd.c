//#include <htc.h>
#include "lcd.h"

void
lcd_write(unsigned char c)
{
 __delay_us(40);

 if (testbit(c,4)) lcd4b = 1; else lcd4b = 0;
 if (testbit(c,5)) lcd5b = 1; else lcd5b = 0;
 if (testbit(c,6)) lcd6b = 1; else lcd6b = 0;
 if (testbit(c,7)) lcd7b = 1; else lcd7b = 0;
 LCD_STROBE();
 if (testbit(c,0)) lcd4b = 1; else lcd4b = 0;
 if (testbit(c,1)) lcd5b = 1; else lcd5b = 0;
 if (testbit(c,2)) lcd6b = 1; else lcd6b = 0;
 if (testbit(c,3)) lcd7b = 1; else lcd7b = 0;
 LCD_STROBE();
 
/*
 LCD_DATA = ( ( c >> 4 ) & 0x0F );
 LCD_STROBE();
 LCD_DATA = ( c & 0x0F );
 LCD_STROBE();  */
}


/*
 *     Clear and home the LCD
 */
 
void
lcd_clear(void)
{
 LCD_RS = 0;
 lcd_write(0x1);
 __delay_ms(2);
}
 
/* write a string of chars to the LCD */
 
void
lcd_puts(const char * s)
{
 LCD_RS = 1;    // write characters
 while(*s)
 lcd_write(*s++);
}
 

/* write one character to the LCD */
 
void
lcd_putch(char c)
{
 LCD_RS = 1;    // write characters
 lcd_write( c );
}
 
/*
 * Go to the specified position
 */
 
void
lcd_goto(unsigned char pos)
{
 LCD_RS = 0;
 lcd_write(0x80+pos);
}


/* initialise the LCD - put into 4 bit mode */
void
lcd_init()
{

 LCD_RS = 0;
 LCD_EN = 0;

 
 __delay_ms(15);    // wait 15mSec after power applied,
 lcd4b = 1;
 lcd5b = 1;
 lcd6b = 0;
 lcd7b = 0;
 LCD_STROBE();
 __delay_ms(5);
 LCD_STROBE();
 __delay_us(200);
 LCD_STROBE();
 __delay_us(200);
 lcd4b = 0;
 lcd5b = 1; 
 lcd6b = 0;
 lcd7b = 0;// Four bit mode
 LCD_STROBE();
 
 lcd_write(0x28); // Set interface length
 lcd_write(0xF); // Display On, Cursor On, Cursor Blink
 lcd_clear();    // Clear screen
 lcd_write(0x6); // Set entry Mode
}