opt subtitle "HI-TECH Software Omniscient Code Generator (PRO mode) build 6446"

opt pagewidth 120

	opt pm

	processor	16F877A
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 36 "X:\Hi-tech\USART_PIC16f877a\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 36 "X:\Hi-tech\USART_PIC16f877a\main.c"
	dw 0x3FFB & 0x3FFF & 0x3F7F & 0x3FFE ;#
	FNCALL	_main,_I2CInit
	FNCALL	_main,_lcd_init
	FNCALL	_main,_menu
	FNCALL	_main,_display_tt
	FNCALL	_main,_ShowTime
	FNCALL	_main,_SetupTime
	FNCALL	_main,_ReadHour
	FNCALL	_main,_printf
	FNCALL	_main,_ReadMin
	FNCALL	_main,_ReadSeconds
	FNCALL	_main,_printTemp
	FNCALL	_SetupTime,_printf
	FNCALL	_SetupTime,_getch
	FNCALL	_SetupTime,___bmul
	FNCALL	_SetupTime,_SetHour
	FNCALL	_SetupTime,_SetMin
	FNCALL	_SetupTime,_SetSeconds
	FNCALL	_SetupTime,_ShowTime
	FNCALL	_printTemp,_get_temp
	FNCALL	_printTemp,_printf
	FNCALL	_ShowTime,_ReadHour
	FNCALL	_ShowTime,_printf
	FNCALL	_ShowTime,_ReadMin
	FNCALL	_ShowTime,_ReadSeconds
	FNCALL	_display_tt,_get_temp
	FNCALL	_display_tt,_lcd_write
	FNCALL	_display_tt,_lcd_clear
	FNCALL	_display_tt,_ReadHour
	FNCALL	_display_tt,_lcd_goto
	FNCALL	_display_tt,___lbdiv
	FNCALL	_display_tt,_lcd_putch
	FNCALL	_display_tt,___bmul
	FNCALL	_display_tt,_ReadMin
	FNCALL	_menu,_lcd_clear
	FNCALL	_menu,_lcd_write
	FNCALL	_menu,_lcd_goto
	FNCALL	_menu,_lcd_puts
	FNCALL	_menu,_lcd_putch
	FNCALL	_lcd_init,_lcd_write
	FNCALL	_lcd_init,_lcd_clear
	FNCALL	_SetSeconds,_I2CStart
	FNCALL	_SetSeconds,_I2CSend
	FNCALL	_SetSeconds,_DCBconv
	FNCALL	_SetSeconds,_I2CStop
	FNCALL	_SetHour,_I2CStart
	FNCALL	_SetHour,_I2CSend
	FNCALL	_SetHour,_DCBconv
	FNCALL	_SetHour,_I2CStop
	FNCALL	_SetMin,_I2CStart
	FNCALL	_SetMin,_I2CSend
	FNCALL	_SetMin,_DCBconv
	FNCALL	_SetMin,_I2CStop
	FNCALL	_lcd_puts,_lcd_write
	FNCALL	_lcd_putch,_lcd_write
	FNCALL	_lcd_goto,_lcd_write
	FNCALL	_lcd_clear,_lcd_write
	FNCALL	_get_temp,_INIT
	FNCALL	_get_temp,_TX
	FNCALL	_get_temp,_printf
	FNCALL	_get_temp,_RX
	FNCALL	_get_temp,___wmul
	FNCALL	_get_temp,___awdiv
	FNCALL	_ReadSeconds,_I2CStart
	FNCALL	_ReadSeconds,_I2CSend
	FNCALL	_ReadSeconds,_I2CRestart
	FNCALL	_ReadSeconds,_I2CRead
	FNCALL	_ReadSeconds,_I2CNak
	FNCALL	_ReadSeconds,_I2CStop
	FNCALL	_ReadSeconds,_BCDconv
	FNCALL	_ReadMin,_I2CStart
	FNCALL	_ReadMin,_I2CSend
	FNCALL	_ReadMin,_I2CRestart
	FNCALL	_ReadMin,_I2CRead
	FNCALL	_ReadMin,_I2CNak
	FNCALL	_ReadMin,_I2CStop
	FNCALL	_ReadMin,_BCDconv
	FNCALL	_ReadHour,_I2CStart
	FNCALL	_ReadHour,_I2CSend
	FNCALL	_ReadHour,_I2CRestart
	FNCALL	_ReadHour,_I2CRead
	FNCALL	_ReadHour,_I2CNak
	FNCALL	_ReadHour,_I2CStop
	FNCALL	_ReadHour,_BCDconv
	FNCALL	_I2CRead,_I2CWait
	FNCALL	_I2CSend,_I2CWait
	FNCALL	_DCBconv,___lbdiv
	FNCALL	_DCBconv,___bmul
	FNCALL	_printf,_putch
	FNCALL	_printf,___lwdiv
	FNCALL	_printf,___lwmod
	FNROOT	_main
	FNCALL	intlevel1,_isr
	global	intlevel1
	FNROOT	intlevel1
	global	_temperature
psect	idataBANK0,class=CODE,space=0,delta=2
global __pidataBANK0
__pidataBANK0:
	file	"X:\Hi-tech\USART_PIC16f877a\main.c"
	line	40

;initializer for _temperature
	retlw	019h
	global	_digits
psect	stringtext,class=STRCODE,delta=2,reloc=256
global __pstringtext
__pstringtext:
;	global	stringtab,__stringbase
stringtab:
;	String table - string pointers are 2 bytes each
	btfsc	(btemp+1),7
	ljmp	stringcode
	bcf	status,7
	btfsc	(btemp+1),0
	bsf	status,7
	movf	indf,w
	incf fsr
skipnz
incf btemp+1
	return
stringcode:
	movf btemp+1,w
andlw 7Fh
movwf	pclath
	movf	fsr,w
incf fsr
skipnz
incf btemp+1
	movwf pc
__stringbase:
	file	"X:\Hi-tech\USART_PIC16f877a\main.c"
	line	22
_digits:
	retlw	030h
	retlw	031h
	retlw	032h
	retlw	033h
	retlw	034h
	retlw	035h
	retlw	036h
	retlw	037h
	retlw	038h
	retlw	039h
	global	_dpowers
psect	stringtext
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\lib\doprnt.c"
	line	350
_dpowers:
	retlw	01h
	retlw	0

	retlw	0Ah
	retlw	0

	retlw	064h
	retlw	0

	retlw	0E8h
	retlw	03h

	retlw	010h
	retlw	027h

	global	_digits
	global	_dpowers
	global	_flag
	global	_sign
	global	_temp_drob
	global	_update
	global	INIT@b
	global	get_temp@init
	global	_RCREG
_RCREG	set	26
	global	_RCSTA
_RCSTA	set	24
	global	_SSPBUF
_SSPBUF	set	19
	global	_SSPCON
_SSPCON	set	20
	global	_TMR2
_TMR2	set	17
	global	_TXREG
_TXREG	set	25
	global	_GIE
_GIE	set	95
	global	_PEIE
_PEIE	set	94
	global	_RC0
_RC0	set	56
	global	_RCIF
_RCIF	set	101
	global	_RD2
_RD2	set	66
	global	_RD3
_RD3	set	67
	global	_RD4
_RD4	set	68
	global	_RD5
_RD5	set	69
	global	_RD6
_RD6	set	70
	global	_RD7
_RD7	set	71
	global	_T2CKPS0
_T2CKPS0	set	144
	global	_T2CKPS1
_T2CKPS1	set	145
	global	_TMR2IF
_TMR2IF	set	97
	global	_TMR2ON
_TMR2ON	set	146
	global	_TOUTPS0
_TOUTPS0	set	147
	global	_TOUTPS1
_TOUTPS1	set	148
	global	_TOUTPS2
_TOUTPS2	set	149
	global	_TOUTPS3
_TOUTPS3	set	150
	global	_TXIF
_TXIF	set	100
	global	_PR2
_PR2	set	146
	global	_SPBRG
_SPBRG	set	153
	global	_SSPADD
_SSPADD	set	147
	global	_SSPCON2
_SSPCON2	set	145
	global	_SSPSTAT
_SSPSTAT	set	148
	global	_TRISB
_TRISB	set	134
	global	_TRISD
_TRISD	set	136
	global	_TXSTA
_TXSTA	set	152
	global	_ACKDT
_ACKDT	set	1165
	global	_ACKEN
_ACKEN	set	1164
	global	_BF
_BF	set	1184
	global	_PEN
_PEN	set	1162
	global	_RCEN
_RCEN	set	1163
	global	_RSEN
_RSEN	set	1161
	global	_SEN
_SEN	set	1160
	global	_TMR2IE
_TMR2IE	set	1121
	global	_TRISC0
_TRISC0	set	1080
	global	_TRISC3
_TRISC3	set	1083
	global	_TRISC4
_TRISC4	set	1084
	global	_TRISC6
_TRISC6	set	1086
	global	_TRISC7
_TRISC7	set	1087
	
STR_49:	
	retlw	13
	retlw	10
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	0
psect	stringtext
	
STR_51:	
	retlw	13
	retlw	10
	retlw	32	;' '
	retlw	49	;'1'
	retlw	32	;' '
	retlw	45	;'-'
	retlw	32	;' '
	retlw	82	;'R'
	retlw	101	;'e'
	retlw	108	;'l'
	retlw	111	;'o'
	retlw	97	;'a'
	retlw	100	;'d'
	retlw	32	;' '
	retlw	116	;'t'
	retlw	105	;'i'
	retlw	109	;'m'
	retlw	101	;'e'
	retlw	0
psect	stringtext
	
STR_59:	
	retlw	12
	retlw	69	;'E'
	retlw	110	;'n'
	retlw	116	;'t'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	32	;' '
	retlw	115	;'s'
	retlw	101	;'e'
	retlw	99	;'c'
	retlw	111	;'o'
	retlw	110	;'n'
	retlw	100	;'d'
	retlw	115	;'s'
	retlw	32	;' '
	retlw	45	;'-'
	retlw	32	;' '
	retlw	0
psect	stringtext
	
STR_56:	
	retlw	12
	retlw	69	;'E'
	retlw	110	;'n'
	retlw	116	;'t'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	32	;' '
	retlw	109	;'m'
	retlw	105	;'i'
	retlw	110	;'n'
	retlw	117	;'u'
	retlw	116	;'t'
	retlw	101	;'e'
	retlw	115	;'s'
	retlw	32	;' '
	retlw	45	;'-'
	retlw	32	;' '
	retlw	0
psect	stringtext
	
STR_52:	
	retlw	13
	retlw	10
	retlw	32	;' '
	retlw	50	;'2'
	retlw	32	;' '
	retlw	45	;'-'
	retlw	32	;' '
	retlw	83	;'S'
	retlw	101	;'e'
	retlw	116	;'t'
	retlw	117	;'u'
	retlw	112	;'p'
	retlw	32	;' '
	retlw	116	;'t'
	retlw	105	;'i'
	retlw	109	;'m'
	retlw	101	;'e'
	retlw	0
psect	stringtext
	
STR_53:	
	retlw	12
	retlw	69	;'E'
	retlw	110	;'n'
	retlw	116	;'t'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	32	;' '
	retlw	104	;'h'
	retlw	111	;'o'
	retlw	117	;'u'
	retlw	114	;'r'
	retlw	115	;'s'
	retlw	32	;' '
	retlw	45	;'-'
	retlw	32	;' '
	retlw	0
psect	stringtext
	
STR_46:	
	retlw	12
	retlw	84	;'T'
	retlw	105	;'i'
	retlw	109	;'m'
	retlw	101	;'e'
	retlw	32	;' '
	retlw	45	;'-'
	retlw	32	;' '
	retlw	37	;'%'
	retlw	100	;'d'
	retlw	32	;' '
	retlw	58	;':'
	retlw	0
psect	stringtext
	
STR_1:	
	retlw	13
	retlw	10
	retlw	49	;'1'
	retlw	48	;'0'
	retlw	49	;'1'
	retlw	48	;'0'
	retlw	37	;'%'
	retlw	100	;'d'
	retlw	0
psect	stringtext
	
STR_3:	
	retlw	13
	retlw	10
	retlw	49	;'1'
	retlw	48	;'0'
	retlw	50	;'2'
	retlw	48	;'0'
	retlw	37	;'%'
	retlw	100	;'d'
	retlw	0
psect	stringtext
	
STR_5:	
	retlw	13
	retlw	10
	retlw	49	;'1'
	retlw	48	;'0'
	retlw	51	;'3'
	retlw	48	;'0'
	retlw	37	;'%'
	retlw	100	;'d'
	retlw	0
psect	stringtext
	
STR_24:	
	retlw	51	;'3'
	retlw	46	;'.'
	retlw	84	;'T'
	retlw	114	;'r'
	retlw	101	;'e'
	retlw	98	;'b'
	retlw	108	;'l'
	retlw	101	;'e'
	retlw	0
psect	stringtext
	
STR_17:	
	retlw	49	;'1'
	retlw	46	;'.'
	retlw	86	;'V'
	retlw	111	;'o'
	retlw	108	;'l'
	retlw	117	;'u'
	retlw	109	;'m'
	retlw	101	;'e'
	retlw	0
psect	stringtext
	
STR_50:	
	retlw	13
	retlw	10
	retlw	32	;' '
	retlw	77	;'M'
	retlw	101	;'e'
	retlw	110	;'n'
	retlw	117	;'u'
	retlw	0
psect	stringtext
	
STR_2:	
	retlw	13
	retlw	10
	retlw	49	;'1'
	retlw	48	;'0'
	retlw	49	;'1'
	retlw	37	;'%'
	retlw	100	;'d'
	retlw	0
psect	stringtext
	
STR_4:	
	retlw	13
	retlw	10
	retlw	49	;'1'
	retlw	48	;'0'
	retlw	50	;'2'
	retlw	37	;'%'
	retlw	100	;'d'
	retlw	0
psect	stringtext
	
STR_6:	
	retlw	13
	retlw	10
	retlw	49	;'1'
	retlw	48	;'0'
	retlw	51	;'3'
	retlw	37	;'%'
	retlw	100	;'d'
	retlw	0
psect	stringtext
	
STR_9:	
	retlw	13
	retlw	10
	retlw	50	;'2'
	retlw	48	;'0'
	retlw	49	;'1'
	retlw	48	;'0'
	retlw	0
psect	stringtext
	
STR_8:	
	retlw	13
	retlw	10
	retlw	50	;'2'
	retlw	48	;'0'
	retlw	50	;'2'
	retlw	48	;'0'
	retlw	0
psect	stringtext
	
STR_13:	
	retlw	13
	retlw	10
	retlw	50	;'2'
	retlw	48	;'0'
	retlw	51	;'3'
	retlw	48	;'0'
	retlw	0
psect	stringtext
	
STR_36:	
	retlw	53	;'5'
	retlw	46	;'.'
	retlw	84	;'T'
	retlw	105	;'i'
	retlw	109	;'m'
	retlw	101	;'e'
	retlw	0
psect	stringtext
	
STR_30:	
	retlw	52	;'4'
	retlw	46	;'.'
	retlw	77	;'M'
	retlw	117	;'u'
	retlw	116	;'t'
	retlw	101	;'e'
	retlw	0
psect	stringtext
	
STR_18:	
	retlw	50	;'2'
	retlw	46	;'.'
	retlw	66	;'B'
	retlw	97	;'a'
	retlw	115	;'s'
	retlw	115	;'s'
	retlw	0
psect	stringtext
	
STR_42:	
	retlw	54	;'6'
	retlw	46	;'.'
	retlw	83	;'S'
	retlw	116	;'t'
	retlw	66	;'B'
	retlw	121	;'y'
	retlw	0
psect	stringtext
	
STR_11:	
	retlw	13
	retlw	10
	retlw	50	;'2'
	retlw	48	;'0'
	retlw	49	;'1'
	retlw	0
psect	stringtext
	
STR_10:	
	retlw	13
	retlw	10
	retlw	50	;'2'
	retlw	48	;'0'
	retlw	50	;'2'
	retlw	0
psect	stringtext
	
STR_14:	
	retlw	13
	retlw	10
	retlw	50	;'2'
	retlw	48	;'0'
	retlw	51	;'3'
	retlw	0
psect	stringtext
	
STR_16:	
	retlw	77	;'M'
	retlw	101	;'e'
	retlw	110	;'n'
	retlw	117	;'u'
	retlw	58	;':'
	retlw	0
psect	stringtext
	
STR_48:	
	retlw	32	;' '
	retlw	37	;'%'
	retlw	100	;'d'
	retlw	32	;' '
	retlw	0
psect	stringtext
	
STR_54:	
	retlw	32	;' '
	retlw	37	;'%'
	retlw	99	;'c'
	retlw	0
psect	stringtext
	
STR_7:	
	retlw	98	;'b'
	retlw	117	;'u'
	retlw	103	;'g'
	retlw	0
psect	stringtext
STR_45	equ	STR_42+0
STR_21	equ	STR_18+0
STR_23	equ	STR_18+0
STR_26	equ	STR_18+0
STR_33	equ	STR_30+0
STR_35	equ	STR_30+0
STR_38	equ	STR_30+0
STR_20	equ	STR_17+0
STR_39	equ	STR_36+0
STR_41	equ	STR_36+0
STR_44	equ	STR_36+0
STR_27	equ	STR_24+0
STR_29	equ	STR_24+0
STR_32	equ	STR_24+0
STR_12	equ	STR_1+6
STR_15	equ	STR_1+6
STR_57	equ	STR_54+0
STR_60	equ	STR_54+0
STR_55	equ	STR_54+1
STR_58	equ	STR_54+1
STR_61	equ	STR_54+1
STR_19	equ	STR_16+0
STR_22	equ	STR_16+0
STR_25	equ	STR_16+0
STR_28	equ	STR_16+0
STR_31	equ	STR_16+0
STR_34	equ	STR_16+0
STR_37	equ	STR_16+0
STR_40	equ	STR_16+0
STR_43	equ	STR_16+0
STR_47	equ	STR_46+7
	file	"usart_test.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bitbssCOMMON,class=COMMON,bit,space=1
global __pbitbssCOMMON
__pbitbssCOMMON:
INIT@b:
       ds      1

get_temp@init:
       ds      1

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_update:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_flag:
       ds      1

_sign:
       ds      1

_temp_drob:
       ds      1

psect	dataBANK0,class=BANK0,space=1
global __pdataBANK0
__pdataBANK0:
	file	"X:\Hi-tech\USART_PIC16f877a\main.c"
	line	40
_temperature:
       ds      1

; Clear objects allocated to BITCOMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbitbssCOMMON/8)+0)&07Fh
; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
	clrf	((__pbssBANK0)+1)&07Fh
	clrf	((__pbssBANK0)+2)&07Fh
; Initialize objects allocated to BANK0
	global __pidataBANK0
psect cinit,class=CODE,delta=2
	fcall	__pidataBANK0+0		;fetch initializer
	movwf	__pdataBANK0+0&07fh		
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_I2CInit
?_I2CInit:	; 0 bytes @ 0x0
	global	?_lcd_init
?_lcd_init:	; 0 bytes @ 0x0
	global	?_menu
?_menu:	; 0 bytes @ 0x0
	global	?_display_tt
?_display_tt:	; 0 bytes @ 0x0
	global	?_ShowTime
?_ShowTime:	; 0 bytes @ 0x0
	global	?_SetupTime
?_SetupTime:	; 0 bytes @ 0x0
	global	?_printTemp
?_printTemp:	; 0 bytes @ 0x0
	global	?_lcd_write
?_lcd_write:	; 0 bytes @ 0x0
	global	?_lcd_clear
?_lcd_clear:	; 0 bytes @ 0x0
	global	?_lcd_goto
?_lcd_goto:	; 0 bytes @ 0x0
	global	?_lcd_putch
?_lcd_putch:	; 0 bytes @ 0x0
	global	?_I2CStart
?_I2CStart:	; 0 bytes @ 0x0
	global	?_I2CSend
?_I2CSend:	; 0 bytes @ 0x0
	global	?_I2CStop
?_I2CStop:	; 0 bytes @ 0x0
	global	?_I2CRestart
?_I2CRestart:	; 0 bytes @ 0x0
	global	?_I2CNak
?_I2CNak:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 0 bytes @ 0x0
	global	??_isr
??_isr:	; 0 bytes @ 0x0
	global	?_INIT
?_INIT:	; 1 bit 
	global	?_TX
?_TX:	; 0 bytes @ 0x0
	global	?_get_temp
?_get_temp:	; 0 bytes @ 0x0
	global	?_putch
?_putch:	; 0 bytes @ 0x0
	global	?_SetMin
?_SetMin:	; 0 bytes @ 0x0
	global	?_SetHour
?_SetHour:	; 0 bytes @ 0x0
	global	?_SetSeconds
?_SetSeconds:	; 0 bytes @ 0x0
	global	?_I2CWait
?_I2CWait:	; 0 bytes @ 0x0
	global	?_ReadHour
?_ReadHour:	; 1 bytes @ 0x0
	global	?_ReadMin
?_ReadMin:	; 1 bytes @ 0x0
	global	?_ReadSeconds
?_ReadSeconds:	; 1 bytes @ 0x0
	global	?_I2CRead
?_I2CRead:	; 1 bytes @ 0x0
	global	?_RX
?_RX:	; 1 bytes @ 0x0
	global	?_getch
?_getch:	; 1 bytes @ 0x0
	global	?_BCDconv
?_BCDconv:	; 1 bytes @ 0x0
	global	?_DCBconv
?_DCBconv:	; 1 bytes @ 0x0
	global	?_isr
?_isr:	; 2 bytes @ 0x0
	ds	2
	global	??_I2CInit
??_I2CInit:	; 0 bytes @ 0x2
	global	??_lcd_write
??_lcd_write:	; 0 bytes @ 0x2
	global	??_I2CStart
??_I2CStart:	; 0 bytes @ 0x2
	global	??_I2CSend
??_I2CSend:	; 0 bytes @ 0x2
	global	??_I2CStop
??_I2CStop:	; 0 bytes @ 0x2
	global	??_I2CRestart
??_I2CRestart:	; 0 bytes @ 0x2
	global	??_I2CRead
??_I2CRead:	; 0 bytes @ 0x2
	global	??_I2CNak
??_I2CNak:	; 0 bytes @ 0x2
	global	??_INIT
??_INIT:	; 0 bytes @ 0x2
	global	??_TX
??_TX:	; 0 bytes @ 0x2
	global	??_RX
??_RX:	; 0 bytes @ 0x2
	global	??_putch
??_putch:	; 0 bytes @ 0x2
	global	??_getch
??_getch:	; 0 bytes @ 0x2
	global	??_BCDconv
??_BCDconv:	; 0 bytes @ 0x2
	global	??_I2CWait
??_I2CWait:	; 0 bytes @ 0x2
	global	?___lbdiv
?___lbdiv:	; 1 bytes @ 0x2
	global	?___wmul
?___wmul:	; 2 bytes @ 0x2
	global	?___lwdiv
?___lwdiv:	; 2 bytes @ 0x2
	global	putch@byte
putch@byte:	; 1 bytes @ 0x2
	global	BCDconv@temp_min
BCDconv@temp_min:	; 1 bytes @ 0x2
	global	I2CSend@dat
I2CSend@dat:	; 1 bytes @ 0x2
	global	I2CRead@temp
I2CRead@temp:	; 1 bytes @ 0x2
	global	___lbdiv@divisor
___lbdiv@divisor:	; 1 bytes @ 0x2
	global	___wmul@multiplier
___wmul@multiplier:	; 2 bytes @ 0x2
	global	___lwdiv@divisor
___lwdiv@divisor:	; 2 bytes @ 0x2
	ds	1
	global	??___lbdiv
??___lbdiv:	; 0 bytes @ 0x3
	global	TX@cmd
TX@cmd:	; 1 bytes @ 0x3
	global	RX@d
RX@d:	; 1 bytes @ 0x3
	global	BCDconv@temp_maj
BCDconv@temp_maj:	; 1 bytes @ 0x3
	global	lcd_write@c
lcd_write@c:	; 1 bytes @ 0x3
	global	___lbdiv@dividend
___lbdiv@dividend:	; 1 bytes @ 0x3
	ds	1
	global	??_lcd_clear
??_lcd_clear:	; 0 bytes @ 0x4
	global	??_lcd_goto
??_lcd_goto:	; 0 bytes @ 0x4
	global	??_lcd_putch
??_lcd_putch:	; 0 bytes @ 0x4
	global	?_lcd_puts
?_lcd_puts:	; 0 bytes @ 0x4
	global	TX@temp
TX@temp:	; 1 bytes @ 0x4
	global	RX@i
RX@i:	; 1 bytes @ 0x4
	global	BCDconv@source
BCDconv@source:	; 1 bytes @ 0x4
	global	lcd_putch@c
lcd_putch@c:	; 1 bytes @ 0x4
	global	lcd_goto@pos
lcd_goto@pos:	; 1 bytes @ 0x4
	global	___lbdiv@counter
___lbdiv@counter:	; 1 bytes @ 0x4
	global	lcd_puts@s
lcd_puts@s:	; 2 bytes @ 0x4
	global	___wmul@multiplicand
___wmul@multiplicand:	; 2 bytes @ 0x4
	global	___lwdiv@dividend
___lwdiv@dividend:	; 2 bytes @ 0x4
	ds	1
	global	TX@i
TX@i:	; 1 bytes @ 0x5
	global	___lbdiv@quotient
___lbdiv@quotient:	; 1 bytes @ 0x5
	ds	1
	global	??_lcd_init
??_lcd_init:	; 0 bytes @ 0x6
	global	??_menu
??_menu:	; 0 bytes @ 0x6
	global	??_lcd_puts
??_lcd_puts:	; 0 bytes @ 0x6
	global	??___wmul
??___wmul:	; 0 bytes @ 0x6
	global	??___lwdiv
??___lwdiv:	; 0 bytes @ 0x6
	global	?___bmul
?___bmul:	; 1 bytes @ 0x6
	global	___bmul@multiplicand
___bmul@multiplicand:	; 1 bytes @ 0x6
	global	___wmul@product
___wmul@product:	; 2 bytes @ 0x6
	global	___lwdiv@quotient
___lwdiv@quotient:	; 2 bytes @ 0x6
	ds	1
	global	??___bmul
??___bmul:	; 0 bytes @ 0x7
	global	menu@i
menu@i:	; 1 bytes @ 0x7
	global	___bmul@product
___bmul@product:	; 1 bytes @ 0x7
	ds	1
	global	??___awdiv
??___awdiv:	; 0 bytes @ 0x8
	global	___bmul@multiplier
___bmul@multiplier:	; 1 bytes @ 0x8
	global	___lwdiv@counter
___lwdiv@counter:	; 1 bytes @ 0x8
	ds	1
	global	??_DCBconv
??_DCBconv:	; 0 bytes @ 0x9
	global	??___lwmod
??___lwmod:	; 0 bytes @ 0x9
	global	___lwmod@counter
___lwmod@counter:	; 1 bytes @ 0x9
	ds	1
	global	??_ShowTime
??_ShowTime:	; 0 bytes @ 0xA
	global	??_ReadHour
??_ReadHour:	; 0 bytes @ 0xA
	global	??_ReadMin
??_ReadMin:	; 0 bytes @ 0xA
	global	??_ReadSeconds
??_ReadSeconds:	; 0 bytes @ 0xA
	global	??_SetMin
??_SetMin:	; 0 bytes @ 0xA
	global	??_SetHour
??_SetHour:	; 0 bytes @ 0xA
	global	??_SetSeconds
??_SetSeconds:	; 0 bytes @ 0xA
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	?___lwmod
?___lwmod:	; 2 bytes @ 0x0
	global	?___awdiv
?___awdiv:	; 2 bytes @ 0x0
	global	DCBconv@temp_min
DCBconv@temp_min:	; 1 bytes @ 0x0
	global	___lwmod@divisor
___lwmod@divisor:	; 2 bytes @ 0x0
	global	___awdiv@divisor
___awdiv@divisor:	; 2 bytes @ 0x0
	ds	1
	global	DCBconv@source
DCBconv@source:	; 1 bytes @ 0x1
	ds	1
	global	DCBconv@temp_maj
DCBconv@temp_maj:	; 1 bytes @ 0x2
	global	___lwmod@dividend
___lwmod@dividend:	; 2 bytes @ 0x2
	global	___awdiv@dividend
___awdiv@dividend:	; 2 bytes @ 0x2
	ds	1
	global	SetMin@minutes
SetMin@minutes:	; 1 bytes @ 0x3
	global	SetHour@hours
SetHour@hours:	; 1 bytes @ 0x3
	global	SetSeconds@seconds
SetSeconds@seconds:	; 1 bytes @ 0x3
	ds	1
	global	?_printf
?_printf:	; 2 bytes @ 0x4
	global	___awdiv@counter
___awdiv@counter:	; 1 bytes @ 0x4
	global	printf@f
printf@f:	; 2 bytes @ 0x4
	ds	1
	global	___awdiv@sign
___awdiv@sign:	; 1 bytes @ 0x5
	ds	1
	global	___awdiv@quotient
___awdiv@quotient:	; 2 bytes @ 0x6
	ds	2
	global	??_printf
??_printf:	; 0 bytes @ 0x8
	ds	2
	global	printf@flag
printf@flag:	; 1 bytes @ 0xA
	ds	1
	global	printf@prec
printf@prec:	; 1 bytes @ 0xB
	ds	1
	global	printf@ap
printf@ap:	; 1 bytes @ 0xC
	ds	1
	global	printf@_val
printf@_val:	; 4 bytes @ 0xD
	ds	4
	global	printf@c
printf@c:	; 1 bytes @ 0x11
	ds	1
	global	??_get_temp
??_get_temp:	; 0 bytes @ 0x12
	global	ReadMin@temp
ReadMin@temp:	; 1 bytes @ 0x12
	global	ReadHour@temp
ReadHour@temp:	; 1 bytes @ 0x12
	global	ReadSeconds@temp
ReadSeconds@temp:	; 1 bytes @ 0x12
	ds	1
	global	??_SetupTime
??_SetupTime:	; 0 bytes @ 0x13
	ds	1
	global	get_temp@temp1
get_temp@temp1:	; 1 bytes @ 0x14
	global	SetupTime@state
SetupTime@state:	; 1 bytes @ 0x14
	ds	1
	global	get_temp@temp2
get_temp@temp2:	; 1 bytes @ 0x15
	global	SetupTime@temp_min
SetupTime@temp_min:	; 1 bytes @ 0x15
	ds	1
	global	??_display_tt
??_display_tt:	; 0 bytes @ 0x16
	global	??_printTemp
??_printTemp:	; 0 bytes @ 0x16
	global	SetupTime@temp_maj
SetupTime@temp_maj:	; 1 bytes @ 0x16
	ds	1
	global	display_tt@d
display_tt@d:	; 1 bytes @ 0x17
	global	SetupTime@temp
SetupTime@temp:	; 1 bytes @ 0x17
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0x18
	ds	2
	global	main@input
main@input:	; 1 bytes @ 0x1A
	ds	2
	global	main@temp
main@temp:	; 1 bytes @ 0x1C
	ds	1
;;Data sizes: Strings 289, constant 20, data 1, bss 4, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14     10      12
;; BANK0           80     29      33
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?___lwdiv	unsigned int  size(1) Largest target is 0
;;
;; ?___lwmod	unsigned int  size(1) Largest target is 0
;;
;; ?___wmul	unsigned int  size(1) Largest target is 0
;;
;; ?___awdiv	int  size(1) Largest target is 0
;;
;; printf@f	PTR const unsigned char  size(2) Largest target is 24
;;		 -> STR_61(CODE[3]), STR_60(CODE[4]), STR_59(CODE[18]), STR_58(CODE[3]), 
;;		 -> STR_57(CODE[4]), STR_56(CODE[18]), STR_55(CODE[3]), STR_54(CODE[4]), 
;;		 -> STR_53(CODE[16]), STR_52(CODE[18]), STR_51(CODE[19]), STR_50(CODE[8]), 
;;		 -> STR_49(CODE[24]), STR_48(CODE[5]), STR_47(CODE[6]), STR_46(CODE[13]), 
;;		 -> STR_15(CODE[3]), STR_14(CODE[6]), STR_13(CODE[7]), STR_12(CODE[3]), 
;;		 -> STR_11(CODE[6]), STR_10(CODE[6]), STR_9(CODE[7]), STR_8(CODE[7]), 
;;		 -> STR_7(CODE[4]), STR_6(CODE[8]), STR_5(CODE[9]), STR_4(CODE[8]), 
;;		 -> STR_3(CODE[9]), STR_2(CODE[8]), STR_1(CODE[9]), 
;;
;; ?_printf	int  size(1) Largest target is 0
;;
;; printf@ap	PTR void [1] size(1) Largest target is 2
;;		 -> ?_printf(BANK0[2]), 
;;
;; S1680$_cp	PTR const unsigned char  size(1) Largest target is 0
;;
;; _val._str._cp	PTR const unsigned char  size(1) Largest target is 0
;;
;; lcd_puts@s	PTR const unsigned char  size(2) Largest target is 9
;;		 -> STR_45(CODE[7]), STR_44(CODE[7]), STR_43(CODE[6]), STR_42(CODE[7]), 
;;		 -> STR_41(CODE[7]), STR_40(CODE[6]), STR_39(CODE[7]), STR_38(CODE[7]), 
;;		 -> STR_37(CODE[6]), STR_36(CODE[7]), STR_35(CODE[7]), STR_34(CODE[6]), 
;;		 -> STR_33(CODE[7]), STR_32(CODE[9]), STR_31(CODE[6]), STR_30(CODE[7]), 
;;		 -> STR_29(CODE[9]), STR_28(CODE[6]), STR_27(CODE[9]), STR_26(CODE[7]), 
;;		 -> STR_25(CODE[6]), STR_24(CODE[9]), STR_23(CODE[7]), STR_22(CODE[6]), 
;;		 -> STR_21(CODE[7]), STR_20(CODE[9]), STR_19(CODE[6]), STR_18(CODE[7]), 
;;		 -> STR_17(CODE[9]), STR_16(CODE[6]), 
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   _menu->_lcd_clear
;;   _menu->_lcd_puts
;;   _lcd_init->_lcd_clear
;;   _SetSeconds->_DCBconv
;;   _SetHour->_DCBconv
;;   _SetMin->_DCBconv
;;   _lcd_puts->_lcd_write
;;   _lcd_putch->_lcd_write
;;   _lcd_goto->_lcd_write
;;   _lcd_clear->_lcd_write
;;   _DCBconv->___bmul
;;   _printf->___lwmod
;;   ___awdiv->___wmul
;;   ___lwmod->___lwdiv
;;   ___bmul->___lbdiv
;;
;; Critical Paths under _isr in COMMON
;;
;;   None.
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_display_tt
;;   _main->_SetupTime
;;   _main->_printTemp
;;   _printTemp->_get_temp
;;   _ShowTime->_ReadHour
;;   _ShowTime->_ReadMin
;;   _ShowTime->_ReadSeconds
;;   _display_tt->_get_temp
;;   _SetSeconds->_DCBconv
;;   _SetHour->_DCBconv
;;   _SetMin->_DCBconv
;;   _get_temp->_printf
;;   _ReadSeconds->_printf
;;   _ReadMin->_printf
;;   _ReadHour->_printf
;;   _printf->___lwmod
;;
;; Critical Paths under _isr in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _isr in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _isr in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.
;;
;; Critical Paths under _isr in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 2, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 8     8      0   16737
;;                                             24 BANK0      5     5      0
;;                            _I2CInit
;;                           _lcd_init
;;                               _menu
;;                         _display_tt
;;                           _ShowTime
;;                          _SetupTime
;;                           _ReadHour
;;                             _printf
;;                            _ReadMin
;;                        _ReadSeconds
;;                          _printTemp
;; ---------------------------------------------------------------------------------
;; (1) _SetupTime                                            5     5      0    5324
;;                                             19 BANK0      5     5      0
;;                             _printf
;;                              _getch
;;                             ___bmul
;;                            _SetHour
;;                             _SetMin
;;                         _SetSeconds
;;                           _ShowTime
;; ---------------------------------------------------------------------------------
;; (1) _printTemp                                            2     2      0    2824
;;                                             22 BANK0      2     2      0
;;                           _get_temp
;;                             _printf
;; ---------------------------------------------------------------------------------
;; (2) _ShowTime                                             0     0      0    1562
;;                           _ReadHour
;;                             _printf
;;                            _ReadMin
;;                        _ReadSeconds
;; ---------------------------------------------------------------------------------
;; (1) _display_tt                                           2     2      0    3769
;;                                             22 BANK0      2     2      0
;;                           _get_temp
;;                          _lcd_write
;;                          _lcd_clear
;;                           _ReadHour
;;                           _lcd_goto
;;                            ___lbdiv
;;                          _lcd_putch
;;                             ___bmul
;;                            _ReadMin
;; ---------------------------------------------------------------------------------
;; (1) _menu                                                 2     2      0     991
;;                                              6 COMMON     2     2      0
;;                          _lcd_clear
;;                          _lcd_write
;;                           _lcd_goto
;;                           _lcd_puts
;;                          _lcd_putch
;; ---------------------------------------------------------------------------------
;; (1) _lcd_init                                             2     2      0     352
;;                                              6 COMMON     2     2      0
;;                          _lcd_write
;;                          _lcd_clear
;; ---------------------------------------------------------------------------------
;; (2) _SetSeconds                                           1     1      0     609
;;                                              3 BANK0      1     1      0
;;                           _I2CStart
;;                            _I2CSend
;;                            _DCBconv
;;                            _I2CStop
;; ---------------------------------------------------------------------------------
;; (2) _SetHour                                              1     1      0     609
;;                                              3 BANK0      1     1      0
;;                           _I2CStart
;;                            _I2CSend
;;                            _DCBconv
;;                            _I2CStop
;; ---------------------------------------------------------------------------------
;; (2) _SetMin                                               1     1      0     609
;;                                              3 BANK0      1     1      0
;;                           _I2CStart
;;                            _I2CSend
;;                            _DCBconv
;;                            _I2CStop
;; ---------------------------------------------------------------------------------
;; (2) _lcd_puts                                             2     0      2     221
;;                                              4 COMMON     2     0      2
;;                          _lcd_write
;; ---------------------------------------------------------------------------------
;; (2) _lcd_putch                                            1     1      0     198
;;                                              4 COMMON     1     1      0
;;                          _lcd_write
;; ---------------------------------------------------------------------------------
;; (2) _lcd_goto                                             1     1      0     198
;;                                              4 COMMON     1     1      0
;;                          _lcd_write
;; ---------------------------------------------------------------------------------
;; (2) _lcd_clear                                            2     2      0     176
;;                                              4 COMMON     2     2      0
;;                          _lcd_write
;; ---------------------------------------------------------------------------------
;; (2) _get_temp                                             4     4      0    1850
;;                                             18 BANK0      4     4      0
;;                               _INIT
;;                                 _TX
;;                             _printf
;;                                 _RX
;;                             ___wmul
;;                            ___awdiv
;; ---------------------------------------------------------------------------------
;; (1) _ReadSeconds                                          1     1      0     196
;;                                             18 BANK0      1     1      0
;;                           _I2CStart
;;                            _I2CSend
;;                         _I2CRestart
;;                            _I2CRead
;;                             _I2CNak
;;                            _I2CStop
;;                            _BCDconv
;;                             _printf (ARG)
;; ---------------------------------------------------------------------------------
;; (1) _ReadMin                                              1     1      0     196
;;                                             18 BANK0      1     1      0
;;                           _I2CStart
;;                            _I2CSend
;;                         _I2CRestart
;;                            _I2CRead
;;                             _I2CNak
;;                            _I2CStop
;;                            _BCDconv
;;                             _printf (ARG)
;; ---------------------------------------------------------------------------------
;; (1) _ReadHour                                             1     1      0     196
;;                                             18 BANK0      1     1      0
;;                           _I2CStart
;;                            _I2CSend
;;                         _I2CRestart
;;                            _I2CRead
;;                             _I2CNak
;;                            _I2CStop
;;                            _BCDconv
;;                             _printf (ARG)
;; ---------------------------------------------------------------------------------
;; (2) _I2CRead                                              1     1      0      34
;;                                              2 COMMON     1     1      0
;;                            _I2CWait
;; ---------------------------------------------------------------------------------
;; (2) _I2CSend                                              1     1      0      31
;;                                              2 COMMON     1     1      0
;;                            _I2CWait
;; ---------------------------------------------------------------------------------
;; (3) _DCBconv                                              4     4      0     547
;;                                              9 COMMON     1     1      0
;;                                              0 BANK0      3     3      0
;;                            ___lbdiv
;;                             ___bmul
;; ---------------------------------------------------------------------------------
;; (3) _lcd_write                                            2     2      0     176
;;                                              2 COMMON     2     2      0
;; ---------------------------------------------------------------------------------
;; (3) _RX                                                   3     3      0      72
;;                                              2 COMMON     3     3      0
;; ---------------------------------------------------------------------------------
;; (3) _TX                                                   4     4      0      94
;;                                              2 COMMON     4     4      0
;; ---------------------------------------------------------------------------------
;; (3) _INIT                                                 1     1      0       0
;;                                              2 COMMON     1     1      0
;; ---------------------------------------------------------------------------------
;; (2) _printf                                              14    10      4     974
;;                                              4 BANK0     14    10      4
;;                              _putch
;;                            ___lwdiv
;;                            ___lwmod
;;                           _I2CStart (ARG)
;;                            _I2CSend (ARG)
;;                         _I2CRestart (ARG)
;;                            _I2CRead (ARG)
;;                             _I2CNak (ARG)
;;                            _I2CStop (ARG)
;;                            _BCDconv (ARG)
;; ---------------------------------------------------------------------------------
;; (3) ___awdiv                                              8     4      4     445
;;                                              0 BANK0      8     4      4
;;                             ___wmul (ARG)
;; ---------------------------------------------------------------------------------
;; (4) ___lbdiv                                              4     3      1     241
;;                                              2 COMMON     4     3      1
;; ---------------------------------------------------------------------------------
;; (3) ___lwmod                                              5     1      4     159
;;                                              9 COMMON     1     1      0
;;                                              0 BANK0      4     0      4
;;                            ___lwdiv (ARG)
;; ---------------------------------------------------------------------------------
;; (3) ___lwdiv                                              7     3      4     162
;;                                              2 COMMON     7     3      4
;; ---------------------------------------------------------------------------------
;; (3) ___wmul                                               6     2      4      92
;;                                              2 COMMON     6     2      4
;; ---------------------------------------------------------------------------------
;; (4) ___bmul                                               3     2      1     136
;;                                              6 COMMON     3     2      1
;;                            ___lbdiv (ARG)
;; ---------------------------------------------------------------------------------
;; (3) _I2CWait                                              0     0      0       0
;; ---------------------------------------------------------------------------------
;; (2) _I2CNak                                               0     0      0       0
;; ---------------------------------------------------------------------------------
;; (2) _I2CRestart                                           0     0      0       0
;; ---------------------------------------------------------------------------------
;; (2) _I2CStop                                              0     0      0       0
;; ---------------------------------------------------------------------------------
;; (2) _I2CStart                                             0     0      0       0
;; ---------------------------------------------------------------------------------
;; (2) _BCDconv                                              3     3      0      94
;;                                              2 COMMON     3     3      0
;; ---------------------------------------------------------------------------------
;; (2) _getch                                                0     0      0       0
;; ---------------------------------------------------------------------------------
;; (3) _putch                                                1     1      0      22
;;                                              2 COMMON     1     1      0
;; ---------------------------------------------------------------------------------
;; (1) _I2CInit                                              0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 4
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (6) _isr                                                  2     2      0       0
;;                                              0 COMMON     2     2      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 6
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _I2CInit
;;   _lcd_init
;;     _lcd_write
;;     _lcd_clear
;;       _lcd_write
;;   _menu
;;     _lcd_clear
;;       _lcd_write
;;     _lcd_write
;;     _lcd_goto
;;       _lcd_write
;;     _lcd_puts
;;       _lcd_write
;;     _lcd_putch
;;       _lcd_write
;;   _display_tt
;;     _get_temp
;;       _INIT
;;       _TX
;;       _printf
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _I2CStart (ARG)
;;         _I2CSend (ARG)
;;           _I2CWait
;;         _I2CRestart (ARG)
;;         _I2CRead (ARG)
;;           _I2CWait
;;         _I2CNak (ARG)
;;         _I2CStop (ARG)
;;         _BCDconv (ARG)
;;       _RX
;;       ___wmul
;;       ___awdiv
;;         ___wmul (ARG)
;;     _lcd_write
;;     _lcd_clear
;;       _lcd_write
;;     _ReadHour
;;       _I2CStart
;;       _I2CSend
;;         _I2CWait
;;       _I2CRestart
;;       _I2CRead
;;         _I2CWait
;;       _I2CNak
;;       _I2CStop
;;       _BCDconv
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _I2CStart (ARG)
;;         _I2CSend (ARG)
;;           _I2CWait
;;         _I2CRestart (ARG)
;;         _I2CRead (ARG)
;;           _I2CWait
;;         _I2CNak (ARG)
;;         _I2CStop (ARG)
;;         _BCDconv (ARG)
;;     _lcd_goto
;;       _lcd_write
;;     ___lbdiv
;;     _lcd_putch
;;       _lcd_write
;;     ___bmul
;;       ___lbdiv (ARG)
;;     _ReadMin
;;       _I2CStart
;;       _I2CSend
;;         _I2CWait
;;       _I2CRestart
;;       _I2CRead
;;         _I2CWait
;;       _I2CNak
;;       _I2CStop
;;       _BCDconv
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _I2CStart (ARG)
;;         _I2CSend (ARG)
;;           _I2CWait
;;         _I2CRestart (ARG)
;;         _I2CRead (ARG)
;;           _I2CWait
;;         _I2CNak (ARG)
;;         _I2CStop (ARG)
;;         _BCDconv (ARG)
;;   _ShowTime
;;     _ReadHour
;;       _I2CStart
;;       _I2CSend
;;         _I2CWait
;;       _I2CRestart
;;       _I2CRead
;;         _I2CWait
;;       _I2CNak
;;       _I2CStop
;;       _BCDconv
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _I2CStart (ARG)
;;         _I2CSend (ARG)
;;           _I2CWait
;;         _I2CRestart (ARG)
;;         _I2CRead (ARG)
;;           _I2CWait
;;         _I2CNak (ARG)
;;         _I2CStop (ARG)
;;         _BCDconv (ARG)
;;     _printf
;;       _putch
;;       ___lwdiv
;;       ___lwmod
;;         ___lwdiv (ARG)
;;       _I2CStart (ARG)
;;       _I2CSend (ARG)
;;         _I2CWait
;;       _I2CRestart (ARG)
;;       _I2CRead (ARG)
;;         _I2CWait
;;       _I2CNak (ARG)
;;       _I2CStop (ARG)
;;       _BCDconv (ARG)
;;     _ReadMin
;;       _I2CStart
;;       _I2CSend
;;         _I2CWait
;;       _I2CRestart
;;       _I2CRead
;;         _I2CWait
;;       _I2CNak
;;       _I2CStop
;;       _BCDconv
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _I2CStart (ARG)
;;         _I2CSend (ARG)
;;           _I2CWait
;;         _I2CRestart (ARG)
;;         _I2CRead (ARG)
;;           _I2CWait
;;         _I2CNak (ARG)
;;         _I2CStop (ARG)
;;         _BCDconv (ARG)
;;     _ReadSeconds
;;       _I2CStart
;;       _I2CSend
;;         _I2CWait
;;       _I2CRestart
;;       _I2CRead
;;         _I2CWait
;;       _I2CNak
;;       _I2CStop
;;       _BCDconv
;;       _printf (ARG)
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _I2CStart (ARG)
;;         _I2CSend (ARG)
;;           _I2CWait
;;         _I2CRestart (ARG)
;;         _I2CRead (ARG)
;;           _I2CWait
;;         _I2CNak (ARG)
;;         _I2CStop (ARG)
;;         _BCDconv (ARG)
;;   _SetupTime
;;     _printf
;;       _putch
;;       ___lwdiv
;;       ___lwmod
;;         ___lwdiv (ARG)
;;       _I2CStart (ARG)
;;       _I2CSend (ARG)
;;         _I2CWait
;;       _I2CRestart (ARG)
;;       _I2CRead (ARG)
;;         _I2CWait
;;       _I2CNak (ARG)
;;       _I2CStop (ARG)
;;       _BCDconv (ARG)
;;     _getch
;;     ___bmul
;;       ___lbdiv (ARG)
;;     _SetHour
;;       _I2CStart
;;       _I2CSend
;;         _I2CWait
;;       _DCBconv
;;         ___lbdiv
;;         ___bmul
;;           ___lbdiv (ARG)
;;       _I2CStop
;;     _SetMin
;;       _I2CStart
;;       _I2CSend
;;         _I2CWait
;;       _DCBconv
;;         ___lbdiv
;;         ___bmul
;;           ___lbdiv (ARG)
;;       _I2CStop
;;     _SetSeconds
;;       _I2CStart
;;       _I2CSend
;;         _I2CWait
;;       _DCBconv
;;         ___lbdiv
;;         ___bmul
;;           ___lbdiv (ARG)
;;       _I2CStop
;;     _ShowTime
;;       _ReadHour
;;         _I2CStart
;;         _I2CSend
;;           _I2CWait
;;         _I2CRestart
;;         _I2CRead
;;           _I2CWait
;;         _I2CNak
;;         _I2CStop
;;         _BCDconv
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _I2CStart (ARG)
;;           _I2CSend (ARG)
;;             _I2CWait
;;           _I2CRestart (ARG)
;;           _I2CRead (ARG)
;;             _I2CWait
;;           _I2CNak (ARG)
;;           _I2CStop (ARG)
;;           _BCDconv (ARG)
;;       _printf
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _I2CStart (ARG)
;;         _I2CSend (ARG)
;;           _I2CWait
;;         _I2CRestart (ARG)
;;         _I2CRead (ARG)
;;           _I2CWait
;;         _I2CNak (ARG)
;;         _I2CStop (ARG)
;;         _BCDconv (ARG)
;;       _ReadMin
;;         _I2CStart
;;         _I2CSend
;;           _I2CWait
;;         _I2CRestart
;;         _I2CRead
;;           _I2CWait
;;         _I2CNak
;;         _I2CStop
;;         _BCDconv
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _I2CStart (ARG)
;;           _I2CSend (ARG)
;;             _I2CWait
;;           _I2CRestart (ARG)
;;           _I2CRead (ARG)
;;             _I2CWait
;;           _I2CNak (ARG)
;;           _I2CStop (ARG)
;;           _BCDconv (ARG)
;;       _ReadSeconds
;;         _I2CStart
;;         _I2CSend
;;           _I2CWait
;;         _I2CRestart
;;         _I2CRead
;;           _I2CWait
;;         _I2CNak
;;         _I2CStop
;;         _BCDconv
;;         _printf (ARG)
;;           _putch
;;           ___lwdiv
;;           ___lwmod
;;             ___lwdiv (ARG)
;;           _I2CStart (ARG)
;;           _I2CSend (ARG)
;;             _I2CWait
;;           _I2CRestart (ARG)
;;           _I2CRead (ARG)
;;             _I2CWait
;;           _I2CNak (ARG)
;;           _I2CStop (ARG)
;;           _BCDconv (ARG)
;;   _ReadHour
;;     _I2CStart
;;     _I2CSend
;;       _I2CWait
;;     _I2CRestart
;;     _I2CRead
;;       _I2CWait
;;     _I2CNak
;;     _I2CStop
;;     _BCDconv
;;     _printf (ARG)
;;       _putch
;;       ___lwdiv
;;       ___lwmod
;;         ___lwdiv (ARG)
;;       _I2CStart (ARG)
;;       _I2CSend (ARG)
;;         _I2CWait
;;       _I2CRestart (ARG)
;;       _I2CRead (ARG)
;;         _I2CWait
;;       _I2CNak (ARG)
;;       _I2CStop (ARG)
;;       _BCDconv (ARG)
;;   _printf
;;     _putch
;;     ___lwdiv
;;     ___lwmod
;;       ___lwdiv (ARG)
;;     _I2CStart (ARG)
;;     _I2CSend (ARG)
;;       _I2CWait
;;     _I2CRestart (ARG)
;;     _I2CRead (ARG)
;;       _I2CWait
;;     _I2CNak (ARG)
;;     _I2CStop (ARG)
;;     _BCDconv (ARG)
;;   _ReadMin
;;     _I2CStart
;;     _I2CSend
;;       _I2CWait
;;     _I2CRestart
;;     _I2CRead
;;       _I2CWait
;;     _I2CNak
;;     _I2CStop
;;     _BCDconv
;;     _printf (ARG)
;;       _putch
;;       ___lwdiv
;;       ___lwmod
;;         ___lwdiv (ARG)
;;       _I2CStart (ARG)
;;       _I2CSend (ARG)
;;         _I2CWait
;;       _I2CRestart (ARG)
;;       _I2CRead (ARG)
;;         _I2CWait
;;       _I2CNak (ARG)
;;       _I2CStop (ARG)
;;       _BCDconv (ARG)
;;   _ReadSeconds
;;     _I2CStart
;;     _I2CSend
;;       _I2CWait
;;     _I2CRestart
;;     _I2CRead
;;       _I2CWait
;;     _I2CNak
;;     _I2CStop
;;     _BCDconv
;;     _printf (ARG)
;;       _putch
;;       ___lwdiv
;;       ___lwmod
;;         ___lwdiv (ARG)
;;       _I2CStart (ARG)
;;       _I2CSend (ARG)
;;         _I2CWait
;;       _I2CRestart (ARG)
;;       _I2CRead (ARG)
;;         _I2CWait
;;       _I2CNak (ARG)
;;       _I2CStop (ARG)
;;       _BCDconv (ARG)
;;   _printTemp
;;     _get_temp
;;       _INIT
;;       _TX
;;       _printf
;;         _putch
;;         ___lwdiv
;;         ___lwmod
;;           ___lwdiv (ARG)
;;         _I2CStart (ARG)
;;         _I2CSend (ARG)
;;           _I2CWait
;;         _I2CRestart (ARG)
;;         _I2CRead (ARG)
;;           _I2CWait
;;         _I2CNak (ARG)
;;         _I2CStop (ARG)
;;         _BCDconv (ARG)
;;       _RX
;;       ___wmul
;;       ___awdiv
;;         ___wmul (ARG)
;;     _printf
;;       _putch
;;       ___lwdiv
;;       ___lwmod
;;         ___lwdiv (ARG)
;;       _I2CStart (ARG)
;;       _I2CSend (ARG)
;;         _I2CWait
;;       _I2CRestart (ARG)
;;       _I2CRead (ARG)
;;         _I2CWait
;;       _I2CNak (ARG)
;;       _I2CStop (ARG)
;;       _BCDconv (ARG)
;;
;; _isr (ROOT)
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BANK3               60      0       0       9        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;BANK2               60      0       0      11        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR1                 0      0       0       2        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;CODE                 0      0       0       0        0.0%
;;DATA                 0      0      32      12        0.0%
;;ABS                  0      0      2D       3        0.0%
;;NULL                 0      0       0       0        0.0%
;;STACK                0      0       5       2        0.0%
;;BANK0               50     1D      21       5       41.3%
;;BITBANK0            50      0       0       4        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR0              0      0       0       1        0.0%
;;COMMON               E      A       C       1       85.7%
;;BITCOMMON            E      0       1       0        7.1%
;;EEDATA             100      0       0       0        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 56 in file "X:\Hi-tech\USART_PIC16f877a\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  temp            1   28[BANK0 ] unsigned char 
;;  i               1    0        unsigned char 
;;  input           1   26[BANK0 ] unsigned char 
;;  itemp           1    0        unsigned char 
;;  status          1    0        unsigned char 
;;  hr              1    0        unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 60/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       3       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0       5       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels required when called:    6
;; This function calls:
;;		_I2CInit
;;		_lcd_init
;;		_menu
;;		_display_tt
;;		_ShowTime
;;		_SetupTime
;;		_ReadHour
;;		_printf
;;		_ReadMin
;;		_ReadSeconds
;;		_printTemp
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"X:\Hi-tech\USART_PIC16f877a\main.c"
	line	56
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 8
; Regs used in _main: [wreg-fsr0h+status,2-btemp+1+pclath+cstack]
	line	59
	
l6348:	
	line	64
	
l6350:	
;main.c: 64: TRISB = 0x00;
	bsf	status, 5	;RP0=1, select bank1
	clrf	(134)^080h	;volatile
	line	65
;main.c: 65: TRISD = 0x00;
	clrf	(136)^080h	;volatile
	line	68
	
l6352:	
;main.c: 68: TRISC7 = 1; TRISC6 = 1; SPBRG = ((int)(4000000L/(16UL * 9600) -1)); RCSTA = (0|0x90); TXSTA = (0x4|0|0x20);
	bsf	(1087/8)^080h,(1087)&7
	
l6354:	
	bsf	(1086/8)^080h,(1086)&7
	
l6356:	
	movlw	(019h)
	movwf	(153)^080h	;volatile
	
l6358:	
	movlw	(090h)
	bcf	status, 5	;RP0=0, select bank0
	movwf	(24)	;volatile
	
l6360:	
	movlw	(024h)
	bsf	status, 5	;RP0=1, select bank1
	movwf	(152)^080h	;volatile
	line	69
	
l6362:	
;main.c: 69: I2CInit();
	fcall	_I2CInit
	line	70
	
l6364:	
;main.c: 70: lcd_init();
	fcall	_lcd_init
	line	72
	
l6366:	
;main.c: 72: menu(i);
	movlw	(01h)
	fcall	_menu
	line	74
	
l6368:	
;main.c: 74: TMR2 = 0x01;
	movlw	(01h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(17)	;volatile
	line	75
	
l6370:	
;main.c: 75: PR2 = 0xFF;
	movlw	(0FFh)
	bsf	status, 5	;RP0=1, select bank1
	movwf	(146)^080h	;volatile
	line	76
	
l6372:	
;main.c: 76: T2CKPS0 = 1; T2CKPS1 = 1;
	bcf	status, 5	;RP0=0, select bank0
	bsf	(144/8),(144)&7
	
l6374:	
	bsf	(145/8),(145)&7
	line	77
	
l6376:	
;main.c: 77: TOUTPS2 = 1; TOUTPS1 = 1; TOUTPS3 = 1; TOUTPS0 = 1;
	bsf	(149/8),(149)&7
	
l6378:	
	bsf	(148/8),(148)&7
	
l6380:	
	bsf	(150/8),(150)&7
	
l6382:	
	bsf	(147/8),(147)&7
	line	78
	
l6384:	
;main.c: 78: GIE = 1;
	bsf	(95/8),(95)&7
	line	79
	
l6386:	
;main.c: 79: PEIE = 1;
	bsf	(94/8),(94)&7
	line	80
	
l6388:	
;main.c: 80: TMR2IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bsf	(1121/8)^080h,(1121)&7
	line	81
	
l6390:	
;main.c: 81: TMR2ON = 1;
	bcf	status, 5	;RP0=0, select bank0
	bsf	(146/8),(146)&7
	line	88
	
l6392:	
;main.c: 88: if (flag == 2) {
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(_flag),w	;volatile
	xorlw	02h
	skipz
	goto	u2141
	goto	u2140
u2141:
	goto	l6444
u2140:
	line	89
	
l6394:	
;main.c: 89: GIE = 0;
	bcf	(95/8),(95)&7
	line	90
;main.c: 90: TMR2ON = 0;
	bcf	(146/8),(146)&7
	line	92
	
l6396:	
;main.c: 92: display_tt();
	fcall	_display_tt
	line	93
	
l6398:	
;main.c: 93: flag = 0;
	clrf	(_flag)	;volatile
	line	94
	
l6400:	
;main.c: 94: GIE = 1;
	bsf	(95/8),(95)&7
	line	95
	
l6402:	
;main.c: 95: TMR2ON = 1;}
	bsf	(146/8),(146)&7
	goto	l6444
	line	100
	
l6404:	
	fcall	_ShowTime
	line	101
;main.c: 101: break;
	goto	l6392
	line	102
	
l6406:	
	fcall	_SetupTime
	line	103
;main.c: 103: break;
	goto	l6392
	line	104
;main.c: 104: case 51 :
	
l611:	
	line	105
;main.c: 105: GIE = 0;
	bcf	(95/8),(95)&7
	line	106
	
l6408:	
;main.c: 106: temp = ReadHour();
	fcall	_ReadHour
	movwf	(main@temp)
	line	107
	
l6410:	
;main.c: 107: if (temp <10) printf("\r\n1010%d", temp); else printf("\r\n101%d", temp);
	movlw	(0Ah)
	subwf	(main@temp),w
	skipnc
	goto	u2151
	goto	u2150
u2151:
	goto	l6414
u2150:
	
l6412:	
	movlw	low(STR_1|8000h)
	movwf	(?_printf)
	movlw	high(STR_1|8000h)
	movwf	((?_printf))+1
	movf	(main@temp),w
	movwf	0+(?_printf)+02h
	clrf	1+(?_printf)+02h
	fcall	_printf
	goto	l6416
	
l6414:	
	movlw	low(STR_2|8000h)
	movwf	(?_printf)
	movlw	high(STR_2|8000h)
	movwf	((?_printf))+1
	movf	(main@temp),w
	movwf	0+(?_printf)+02h
	clrf	1+(?_printf)+02h
	fcall	_printf
	line	108
	
l6416:	
;main.c: 108: _delay((unsigned long)((20)*(4000000/4000.0)));
	opt asmopt_off
movlw	26
movwf	((??_main+0)+0+1),f
	movlw	248
movwf	((??_main+0)+0),f
u2187:
	decfsz	((??_main+0)+0),f
	goto	u2187
	decfsz	((??_main+0)+0+1),f
	goto	u2187
	clrwdt
opt asmopt_on

	line	109
	
l6418:	
;main.c: 109: temp = ReadMin();
	fcall	_ReadMin
	movwf	(main@temp)
	line	110
	
l6420:	
;main.c: 110: if (temp <10) printf("\r\n1020%d", temp); else printf("\r\n102%d", temp);
	movlw	(0Ah)
	subwf	(main@temp),w
	skipnc
	goto	u2161
	goto	u2160
u2161:
	goto	l6424
u2160:
	
l6422:	
	movlw	low(STR_3|8000h)
	movwf	(?_printf)
	movlw	high(STR_3|8000h)
	movwf	((?_printf))+1
	movf	(main@temp),w
	movwf	0+(?_printf)+02h
	clrf	1+(?_printf)+02h
	fcall	_printf
	goto	l6426
	
l6424:	
	movlw	low(STR_4|8000h)
	movwf	(?_printf)
	movlw	high(STR_4|8000h)
	movwf	((?_printf))+1
	movf	(main@temp),w
	movwf	0+(?_printf)+02h
	clrf	1+(?_printf)+02h
	fcall	_printf
	line	111
	
l6426:	
;main.c: 111: _delay((unsigned long)((20)*(4000000/4000.0)));
	opt asmopt_off
movlw	26
movwf	((??_main+0)+0+1),f
	movlw	248
movwf	((??_main+0)+0),f
u2197:
	decfsz	((??_main+0)+0),f
	goto	u2197
	decfsz	((??_main+0)+0+1),f
	goto	u2197
	clrwdt
opt asmopt_on

	line	112
	
l6428:	
;main.c: 112: temp = ReadSeconds();
	fcall	_ReadSeconds
	movwf	(main@temp)
	line	113
	
l6430:	
;main.c: 113: if (temp <10) printf("\r\n1030%d", temp); else printf("\r\n103%d", temp);
	movlw	(0Ah)
	subwf	(main@temp),w
	skipnc
	goto	u2171
	goto	u2170
u2171:
	goto	l6434
u2170:
	
l6432:	
	movlw	low(STR_5|8000h)
	movwf	(?_printf)
	movlw	high(STR_5|8000h)
	movwf	((?_printf))+1
	movf	(main@temp),w
	movwf	0+(?_printf)+02h
	clrf	1+(?_printf)+02h
	fcall	_printf
	goto	l6436
	
l6434:	
	movlw	low(STR_6|8000h)
	movwf	(?_printf)
	movlw	high(STR_6|8000h)
	movwf	((?_printf))+1
	movf	(main@temp),w
	movwf	0+(?_printf)+02h
	clrf	1+(?_printf)+02h
	fcall	_printf
	line	114
	
l6436:	
;main.c: 114: _delay((unsigned long)((20)*(4000000/4000.0)));
	opt asmopt_off
movlw	26
movwf	((??_main+0)+0+1),f
	movlw	248
movwf	((??_main+0)+0),f
u2207:
	decfsz	((??_main+0)+0),f
	goto	u2207
	decfsz	((??_main+0)+0+1),f
	goto	u2207
	clrwdt
opt asmopt_on

	line	115
;main.c: 115: break;
	goto	l6392
	line	116
	
l6438:	
	fcall	_printTemp
	line	117
;main.c: 117: break;
	goto	l6392
	line	119
	
l6440:	
	fcall	_display_tt
	line	120
;main.c: 120: break;
	goto	l6392
	line	99
	
l6444:	
	movf	(main@input),w
	; Switch size 1, requested type "space"
; Number of cases is 5, Range of values is 49 to 53
; switch strategies available:
; Name         Bytes Cycles
; simple_byte    16     9 (average)
; direct_byte    37    22 (fixed)
;	Chosen strategy is simple_byte

	xorlw	49^0	; case 49
	skipnz
	goto	l6404
	xorlw	50^49	; case 50
	skipnz
	goto	l6406
	xorlw	51^50	; case 51
	skipnz
	goto	l611
	xorlw	52^51	; case 52
	skipnz
	goto	l6438
	xorlw	53^52	; case 53
	skipnz
	goto	l6440
	goto	l6392

	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

psect	maintext
	line	127
	signat	_main,88
	global	_SetupTime
psect	text1215,local,class=CODE,delta=2
global __ptext1215
__ptext1215:

;; *************** function _SetupTime *****************
;; Defined at:
;;		line 115 in file "X:\Hi-tech\USART_PIC16f877a\rtc.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  temp            1   23[BANK0 ] unsigned char 
;;  temp_maj        1   22[BANK0 ] unsigned char 
;;  temp_min        1   21[BANK0 ] unsigned char 
;;  state           1   20[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFE9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       4       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       5       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    5
;; This function calls:
;;		_printf
;;		_getch
;;		___bmul
;;		_SetHour
;;		_SetMin
;;		_SetSeconds
;;		_ShowTime
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text1215
	file	"X:\Hi-tech\USART_PIC16f877a\rtc.c"
	line	115
	global	__size_of_SetupTime
	__size_of_SetupTime	equ	__end_of_SetupTime-_SetupTime
	
_SetupTime:	
	opt	stack 7
; Regs used in _SetupTime: [wreg-fsr0h+status,2-btemp+1+pclath+cstack]
	line	116
	
l6270:	
;rtc.c: 116: unsigned char state = 1;
	clrf	(SetupTime@state)
	incf	(SetupTime@state),f
	line	117
	
l6272:	
	line	118
;rtc.c: 118: unsigned char temp_maj = 0;
	clrf	(SetupTime@temp_maj)
	line	119
;rtc.c: 119: unsigned char temp = 0;
	clrf	(SetupTime@temp)
	line	121
;rtc.c: 121: while(state){
	goto	l6296
	line	122
	
l6274:	
;rtc.c: 122: printf("\fEnter hours - ");
	movlw	low(STR_53|8000h)
	movwf	(?_printf)
	movlw	high(STR_53|8000h)
	movwf	((?_printf))+1
	fcall	_printf
	line	123
	
l6276:	
;rtc.c: 123: temp_maj = getch();
	fcall	_getch
	movwf	(SetupTime@temp_maj)
	line	124
	
l6278:	
;rtc.c: 124: printf(" %c", temp_maj);
	movlw	low(STR_54|8000h)
	movwf	(?_printf)
	movlw	high(STR_54|8000h)
	movwf	((?_printf))+1
	movf	(SetupTime@temp_maj),w
	movwf	0+(?_printf)+02h
	clrf	1+(?_printf)+02h
	fcall	_printf
	line	125
;rtc.c: 125: temp_min = getch();
	fcall	_getch
	movwf	(SetupTime@temp_min)
	line	126
	
l6280:	
;rtc.c: 126: printf("%c", temp_min);
	movlw	low(STR_55|8000h)
	movwf	(?_printf)
	movlw	high(STR_55|8000h)
	movwf	((?_printf))+1
	movf	(SetupTime@temp_min),w
	movwf	0+(?_printf)+02h
	clrf	1+(?_printf)+02h
	fcall	_printf
	line	127
	
l6282:	
;rtc.c: 127: temp = getch();
	fcall	_getch
	movwf	(SetupTime@temp)
	line	128
	
l6284:	
;rtc.c: 128: if (temp == 13) {
	movf	(SetupTime@temp),w
	xorlw	0Dh
	skipz
	goto	u2051
	goto	u2050
u2051:
	goto	l6296
u2050:
	line	129
	
l6286:	
;rtc.c: 129: temp_maj -= 48;
	movlw	low(030h)
	subwf	(SetupTime@temp_maj),f
	line	130
;rtc.c: 130: temp_min -= 48;
	movlw	low(030h)
	subwf	(SetupTime@temp_min),f
	line	131
	
l6288:	
;rtc.c: 131: temp = temp_maj*10 + temp_min;
	movlw	(0Ah)
	movwf	(?___bmul)
	movf	(SetupTime@temp_maj),w
	fcall	___bmul
	movwf	(??_SetupTime+0)+0
	movf	(SetupTime@temp_min),w
	addwf	0+(??_SetupTime+0)+0,w
	movwf	(SetupTime@temp)
	line	132
	
l6290:	
;rtc.c: 132: if (temp < 24) {
	movlw	(018h)
	subwf	(SetupTime@temp),w
	skipnc
	goto	u2061
	goto	u2060
u2061:
	goto	l6296
u2060:
	line	133
	
l6292:	
;rtc.c: 133: SetHour(temp);
	movf	(SetupTime@temp),w
	fcall	_SetHour
	line	134
	
l6294:	
;rtc.c: 134: state = 0;
	bcf	status, 5	;RP0=0, select bank0
	clrf	(SetupTime@state)
	line	121
	
l6296:	
	movf	(SetupTime@state),f
	skipz
	goto	u2071
	goto	u2070
u2071:
	goto	l6274
u2070:
	
l1875:	
	line	141
;rtc.c: 135: }
;rtc.c: 137: }
;rtc.c: 138: }
;rtc.c: 141: state = 1;
	clrf	(SetupTime@state)
	incf	(SetupTime@state),f
	line	142
;rtc.c: 142: while(state){
	goto	l6320
	line	143
	
l6298:	
;rtc.c: 143: printf("\fEnter minutes - ");
	movlw	low(STR_56|8000h)
	movwf	(?_printf)
	movlw	high(STR_56|8000h)
	movwf	((?_printf))+1
	fcall	_printf
	line	144
	
l6300:	
;rtc.c: 144: temp_maj = getch();
	fcall	_getch
	movwf	(SetupTime@temp_maj)
	line	145
	
l6302:	
;rtc.c: 145: printf(" %c", temp_maj);
	movlw	low(STR_57|8000h)
	movwf	(?_printf)
	movlw	high(STR_57|8000h)
	movwf	((?_printf))+1
	movf	(SetupTime@temp_maj),w
	movwf	0+(?_printf)+02h
	clrf	1+(?_printf)+02h
	fcall	_printf
	line	146
;rtc.c: 146: temp_min = getch();
	fcall	_getch
	movwf	(SetupTime@temp_min)
	line	147
	
l6304:	
;rtc.c: 147: printf("%c", temp_min);
	movlw	low(STR_58|8000h)
	movwf	(?_printf)
	movlw	high(STR_58|8000h)
	movwf	((?_printf))+1
	movf	(SetupTime@temp_min),w
	movwf	0+(?_printf)+02h
	clrf	1+(?_printf)+02h
	fcall	_printf
	line	148
	
l6306:	
;rtc.c: 148: temp = getch();
	fcall	_getch
	movwf	(SetupTime@temp)
	line	149
	
l6308:	
;rtc.c: 149: if (temp == 13) {
	movf	(SetupTime@temp),w
	xorlw	0Dh
	skipz
	goto	u2081
	goto	u2080
u2081:
	goto	l6320
u2080:
	line	150
	
l6310:	
;rtc.c: 150: temp_maj -= 48;
	movlw	low(030h)
	subwf	(SetupTime@temp_maj),f
	line	151
;rtc.c: 151: temp_min -= 48;
	movlw	low(030h)
	subwf	(SetupTime@temp_min),f
	line	152
	
l6312:	
;rtc.c: 152: temp = temp_maj*10 + temp_min;
	movlw	(0Ah)
	movwf	(?___bmul)
	movf	(SetupTime@temp_maj),w
	fcall	___bmul
	movwf	(??_SetupTime+0)+0
	movf	(SetupTime@temp_min),w
	addwf	0+(??_SetupTime+0)+0,w
	movwf	(SetupTime@temp)
	line	153
	
l6314:	
;rtc.c: 153: if (temp < 61) {
	movlw	(03Dh)
	subwf	(SetupTime@temp),w
	skipnc
	goto	u2091
	goto	u2090
u2091:
	goto	l6320
u2090:
	line	154
	
l6316:	
;rtc.c: 154: SetMin(temp);
	movf	(SetupTime@temp),w
	fcall	_SetMin
	line	155
	
l6318:	
;rtc.c: 155: state = 0;
	bcf	status, 5	;RP0=0, select bank0
	clrf	(SetupTime@state)
	line	142
	
l6320:	
	movf	(SetupTime@state),f
	skipz
	goto	u2101
	goto	u2100
u2101:
	goto	l6298
u2100:
	
l1880:	
	line	159
;rtc.c: 156: }
;rtc.c: 157: }
;rtc.c: 158: }
;rtc.c: 159: state = 1;
	clrf	(SetupTime@state)
	incf	(SetupTime@state),f
	line	160
;rtc.c: 160: while(state){
	goto	l6344
	line	161
	
l6322:	
;rtc.c: 161: printf("\fEnter seconds - ");
	movlw	low(STR_59|8000h)
	movwf	(?_printf)
	movlw	high(STR_59|8000h)
	movwf	((?_printf))+1
	fcall	_printf
	line	162
	
l6324:	
;rtc.c: 162: temp_maj = getch();
	fcall	_getch
	movwf	(SetupTime@temp_maj)
	line	163
	
l6326:	
;rtc.c: 163: printf(" %c", temp_maj);
	movlw	low(STR_60|8000h)
	movwf	(?_printf)
	movlw	high(STR_60|8000h)
	movwf	((?_printf))+1
	movf	(SetupTime@temp_maj),w
	movwf	0+(?_printf)+02h
	clrf	1+(?_printf)+02h
	fcall	_printf
	line	164
;rtc.c: 164: temp_min = getch();
	fcall	_getch
	movwf	(SetupTime@temp_min)
	line	165
	
l6328:	
;rtc.c: 165: printf("%c", temp_min);
	movlw	low(STR_61|8000h)
	movwf	(?_printf)
	movlw	high(STR_61|8000h)
	movwf	((?_printf))+1
	movf	(SetupTime@temp_min),w
	movwf	0+(?_printf)+02h
	clrf	1+(?_printf)+02h
	fcall	_printf
	line	166
	
l6330:	
;rtc.c: 166: temp = getch();
	fcall	_getch
	movwf	(SetupTime@temp)
	line	167
	
l6332:	
;rtc.c: 167: if (temp == 13) {
	movf	(SetupTime@temp),w
	xorlw	0Dh
	skipz
	goto	u2111
	goto	u2110
u2111:
	goto	l6344
u2110:
	line	168
	
l6334:	
;rtc.c: 168: temp_maj -= 48;
	movlw	low(030h)
	subwf	(SetupTime@temp_maj),f
	line	169
;rtc.c: 169: temp_min -= 48;
	movlw	low(030h)
	subwf	(SetupTime@temp_min),f
	line	170
	
l6336:	
;rtc.c: 170: temp = temp_maj*10 + temp_min;
	movlw	(0Ah)
	movwf	(?___bmul)
	movf	(SetupTime@temp_maj),w
	fcall	___bmul
	movwf	(??_SetupTime+0)+0
	movf	(SetupTime@temp_min),w
	addwf	0+(??_SetupTime+0)+0,w
	movwf	(SetupTime@temp)
	line	171
	
l6338:	
;rtc.c: 171: if (temp < 61) {
	movlw	(03Dh)
	subwf	(SetupTime@temp),w
	skipnc
	goto	u2121
	goto	u2120
u2121:
	goto	l6344
u2120:
	line	172
	
l6340:	
;rtc.c: 172: SetSeconds(temp);
	movf	(SetupTime@temp),w
	fcall	_SetSeconds
	line	173
	
l6342:	
;rtc.c: 173: state = 0;
	bcf	status, 5	;RP0=0, select bank0
	clrf	(SetupTime@state)
	line	160
	
l6344:	
	movf	(SetupTime@state),f
	skipz
	goto	u2131
	goto	u2130
u2131:
	goto	l6322
u2130:
	line	178
	
l6346:	
;rtc.c: 174: }
;rtc.c: 175: }
;rtc.c: 176: }
;rtc.c: 178: ShowTime();
	fcall	_ShowTime
	line	180
	
l1886:	
	return
	opt stack 0
GLOBAL	__end_of_SetupTime
	__end_of_SetupTime:
;; =============== function _SetupTime ends ============

	signat	_SetupTime,88
	global	_printTemp
psect	text1216,local,class=CODE,delta=2
global __ptext1216
__ptext1216:

;; *************** function _printTemp *****************
;; Defined at:
;;		line 248 in file "X:\Hi-tech\USART_PIC16f877a\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0       2       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_get_temp
;;		_printf
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text1216
	file	"X:\Hi-tech\USART_PIC16f877a\main.c"
	line	248
	global	__size_of_printTemp
	__size_of_printTemp	equ	__end_of_printTemp-_printTemp
	
_printTemp:	
	opt	stack 7
; Regs used in _printTemp: [wreg-fsr0h+status,2-btemp+1+pclath+cstack]
	line	249
	
l6244:	
;main.c: 249: get_temp();
	fcall	_get_temp
	line	251
	
l6246:	
;main.c: 251: if (temperature<10) {
	movlw	(0Ah)
	subwf	(_temperature),w
	skipnc
	goto	u2011
	goto	u2010
u2011:
	goto	l6254
u2010:
	line	252
	
l6248:	
;main.c: 252: if (sign) printf("\r\n2020"); else printf("\r\n2010");
	movf	(_sign),w
	skipz
	goto	u2020
	goto	l6252
u2020:
	
l6250:	
	movlw	low(STR_8|8000h)
	movwf	(?_printf)
	movlw	high(STR_8|8000h)
	movwf	((?_printf))+1
	fcall	_printf
	goto	l661
	
l6252:	
	movlw	low(STR_9|8000h)
	movwf	(?_printf)
	movlw	high(STR_9|8000h)
	movwf	((?_printf))+1
	fcall	_printf
	goto	l661
	line	254
	
l6254:	
;main.c: 254: if (sign) printf("\r\n202"); else printf("\r\n201");
	movf	(_sign),w
	skipz
	goto	u2030
	goto	l6258
u2030:
	
l6256:	
	movlw	low(STR_10|8000h)
	movwf	(?_printf)
	movlw	high(STR_10|8000h)
	movwf	((?_printf))+1
	fcall	_printf
	goto	l661
	
l6258:	
	movlw	low(STR_11|8000h)
	movwf	(?_printf)
	movlw	high(STR_11|8000h)
	movwf	((?_printf))+1
	fcall	_printf
	line	255
	
l661:	
	line	256
;main.c: 255: }
;main.c: 256: printf("%d", temperature);
	movlw	low(STR_12|8000h)
	movwf	(?_printf)
	movlw	high(STR_12|8000h)
	movwf	((?_printf))+1
	movf	(_temperature),w
	movwf	0+(?_printf)+02h
	clrf	1+(?_printf)+02h
	fcall	_printf
	line	257
	
l6260:	
;main.c: 257: _delay((unsigned long)((20)*(4000000/4000.0)));
	opt asmopt_off
movlw	26
movwf	((??_printTemp+0)+0+1),f
	movlw	248
movwf	((??_printTemp+0)+0),f
u2217:
	decfsz	((??_printTemp+0)+0),f
	goto	u2217
	decfsz	((??_printTemp+0)+0+1),f
	goto	u2217
	clrwdt
opt asmopt_on

	line	258
	
l6262:	
;main.c: 258: if (temp_drob<10) printf("\r\n2030"); else printf("\r\n203");
	movlw	(0Ah)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	subwf	(_temp_drob),w
	skipnc
	goto	u2041
	goto	u2040
u2041:
	goto	l6266
u2040:
	
l6264:	
	movlw	low(STR_13|8000h)
	movwf	(?_printf)
	movlw	high(STR_13|8000h)
	movwf	((?_printf))+1
	fcall	_printf
	goto	l665
	
l6266:	
	movlw	low(STR_14|8000h)
	movwf	(?_printf)
	movlw	high(STR_14|8000h)
	movwf	((?_printf))+1
	fcall	_printf
	
l665:	
	line	261
;main.c: 261: printf("%d", temp_drob);
	movlw	low(STR_15|8000h)
	movwf	(?_printf)
	movlw	high(STR_15|8000h)
	movwf	((?_printf))+1
	movf	(_temp_drob),w
	movwf	0+(?_printf)+02h
	clrf	1+(?_printf)+02h
	fcall	_printf
	line	262
	
l6268:	
;main.c: 262: _delay((unsigned long)((20)*(4000000/4000.0)));
	opt asmopt_off
movlw	26
movwf	((??_printTemp+0)+0+1),f
	movlw	248
movwf	((??_printTemp+0)+0),f
u2227:
	decfsz	((??_printTemp+0)+0),f
	goto	u2227
	decfsz	((??_printTemp+0)+0+1),f
	goto	u2227
	clrwdt
opt asmopt_on

	line	263
	
l666:	
	return
	opt stack 0
GLOBAL	__end_of_printTemp
	__end_of_printTemp:
;; =============== function _printTemp ends ============

	signat	_printTemp,88
	global	_ShowTime
psect	text1217,local,class=CODE,delta=2
global __ptext1217
__ptext1217:

;; *************** function _ShowTime *****************
;; Defined at:
;;		line 99 in file "X:\Hi-tech\USART_PIC16f877a\rtc.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFE9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_ReadHour
;;		_printf
;;		_ReadMin
;;		_ReadSeconds
;; This function is called by:
;;		_main
;;		_SetupTime
;; This function uses a non-reentrant model
;;
psect	text1217
	file	"X:\Hi-tech\USART_PIC16f877a\rtc.c"
	line	99
	global	__size_of_ShowTime
	__size_of_ShowTime	equ	__end_of_ShowTime-_ShowTime
	
_ShowTime:	
	opt	stack 6
; Regs used in _ShowTime: [wreg-fsr0h+status,2-btemp+1+pclath+cstack]
	line	101
	
l6242:	
;rtc.c: 101: printf("\fTime - %d :", ReadHour());
	movlw	low(STR_46|8000h)
	movwf	(?_printf)
	movlw	high(STR_46|8000h)
	movwf	((?_printf))+1
	fcall	_ReadHour
	movwf	0+(?_printf)+02h
	clrf	1+(?_printf)+02h
	fcall	_printf
	line	103
;rtc.c: 103: printf(" %d :", ReadMin());
	movlw	low(STR_47|8000h)
	movwf	(?_printf)
	movlw	high(STR_47|8000h)
	movwf	((?_printf))+1
	fcall	_ReadMin
	movwf	0+(?_printf)+02h
	clrf	1+(?_printf)+02h
	fcall	_printf
	line	105
;rtc.c: 105: printf(" %d ", ReadSeconds());
	movlw	low(STR_48|8000h)
	movwf	(?_printf)
	movlw	high(STR_48|8000h)
	movwf	((?_printf))+1
	fcall	_ReadSeconds
	movwf	0+(?_printf)+02h
	clrf	1+(?_printf)+02h
	fcall	_printf
	line	107
;rtc.c: 107: printf("\r\n*********************");
	movlw	low(STR_49|8000h)
	movwf	(?_printf)
	movlw	high(STR_49|8000h)
	movwf	((?_printf))+1
	fcall	_printf
	line	109
;rtc.c: 109: printf("\r\n Menu");
	movlw	low(STR_50|8000h)
	movwf	(?_printf)
	movlw	high(STR_50|8000h)
	movwf	((?_printf))+1
	fcall	_printf
	line	110
;rtc.c: 110: printf("\r\n 1 - Reload time");
	movlw	low(STR_51|8000h)
	movwf	(?_printf)
	movlw	high(STR_51|8000h)
	movwf	((?_printf))+1
	fcall	_printf
	line	111
;rtc.c: 111: printf("\r\n 2 - Setup time");
	movlw	low(STR_52|8000h)
	movwf	(?_printf)
	movlw	high(STR_52|8000h)
	movwf	((?_printf))+1
	fcall	_printf
	line	113
	
l1868:	
	return
	opt stack 0
GLOBAL	__end_of_ShowTime
	__end_of_ShowTime:
;; =============== function _ShowTime ends ============

	signat	_ShowTime,88
	global	_display_tt
psect	text1218,local,class=CODE,delta=2
global __ptext1218
__ptext1218:

;; *************** function _display_tt *****************
;; Defined at:
;;		line 265 in file "X:\Hi-tech\USART_PIC16f877a\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  d               1   23[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       2       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_get_temp
;;		_lcd_write
;;		_lcd_clear
;;		_ReadHour
;;		_lcd_goto
;;		___lbdiv
;;		_lcd_putch
;;		___bmul
;;		_ReadMin
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text1218
	file	"X:\Hi-tech\USART_PIC16f877a\main.c"
	line	265
	global	__size_of_display_tt
	__size_of_display_tt	equ	__end_of_display_tt-_display_tt
	
_display_tt:	
	opt	stack 7
; Regs used in _display_tt: [wreg-fsr0h+status,2-btemp+1+pclath+cstack]
	line	268
	
l6186:	
;main.c: 266: unsigned char d;
;main.c: 268: get_temp();
	fcall	_get_temp
	line	269
	
l6188:	
;main.c: 269: RD2 = 0;
	bcf	(66/8),(66)&7
	line	270
	
l6190:	
;main.c: 270: lcd_write(0b00001100);
	movlw	(0Ch)
	fcall	_lcd_write
	line	271
	
l6192:	
;main.c: 271: _delay((unsigned long)((100)*(4000000/4000000.0)));
	opt asmopt_off
movlw	33
movwf	(??_display_tt+0)+0,f
u2237:
decfsz	(??_display_tt+0)+0,f
	goto	u2237
opt asmopt_on

	line	272
	
l6194:	
;main.c: 272: lcd_clear();
	fcall	_lcd_clear
	line	273
	
l6196:	
;main.c: 273: d = ReadHour();
	fcall	_ReadHour
	movwf	(display_tt@d)
	line	274
	
l6198:	
;main.c: 274: lcd_goto(0x05);
	movlw	(05h)
	fcall	_lcd_goto
	line	275
	
l6200:	
;main.c: 275: lcd_putch(digits[d/10]);
	movlw	(0Ah)
	movwf	(?___lbdiv)
	movf	(display_tt@d),w
	fcall	___lbdiv
	addlw	low(_digits|8000h)
	movwf	fsr0
	movlw	high(_digits|8000h)
	skipnc
	addlw	1
	movwf	btemp+1
	fcall	stringtab
	fcall	_lcd_putch
	line	276
	
l6202:	
;main.c: 276: d = d - ((d/10)*10);
	movlw	(0F6h)
	movwf	(?___bmul)
	movlw	(0Ah)
	movwf	(?___lbdiv)
	movf	(display_tt@d),w
	fcall	___lbdiv
	fcall	___bmul
	addwf	(display_tt@d),f
	line	277
	
l6204:	
;main.c: 277: lcd_putch(digits[d]);
	movf	(display_tt@d),w
	addlw	low(_digits|8000h)
	movwf	fsr0
	movlw	high(_digits|8000h)
	skipnc
	addlw	1
	movwf	btemp+1
	fcall	stringtab
	fcall	_lcd_putch
	line	278
	
l6206:	
	line	279
	
l6208:	
;main.c: 279: lcd_putch(d);
	movlw	(03Ah)
	fcall	_lcd_putch
	line	280
	
l6210:	
;main.c: 280: d = ReadMin();
	fcall	_ReadMin
	movwf	(display_tt@d)
	line	281
	
l6212:	
;main.c: 281: lcd_putch(digits[d/10]);
	movlw	(0Ah)
	movwf	(?___lbdiv)
	movf	(display_tt@d),w
	fcall	___lbdiv
	addlw	low(_digits|8000h)
	movwf	fsr0
	movlw	high(_digits|8000h)
	skipnc
	addlw	1
	movwf	btemp+1
	fcall	stringtab
	fcall	_lcd_putch
	line	282
	
l6214:	
;main.c: 282: d = d - ((d/10)*10);
	movlw	(0F6h)
	movwf	(?___bmul)
	movlw	(0Ah)
	movwf	(?___lbdiv)
	movf	(display_tt@d),w
	fcall	___lbdiv
	fcall	___bmul
	addwf	(display_tt@d),f
	line	283
	
l6216:	
;main.c: 283: lcd_putch(digits[d]);
	movf	(display_tt@d),w
	addlw	low(_digits|8000h)
	movwf	fsr0
	movlw	high(_digits|8000h)
	skipnc
	addlw	1
	movwf	btemp+1
	fcall	stringtab
	fcall	_lcd_putch
	line	284
	
l6218:	
;main.c: 284: lcd_goto(0x44);
	movlw	(044h)
	fcall	_lcd_goto
	line	285
	
l6220:	
;main.c: 285: if (!sign) lcd_putch(0b00101011); else lcd_putch(0b10110000);
	movf	(_sign),f
	skipz
	goto	u2001
	goto	u2000
u2001:
	goto	l6224
u2000:
	
l6222:	
	movlw	(02Bh)
	fcall	_lcd_putch
	goto	l670
	
l6224:	
	movlw	(0B0h)
	fcall	_lcd_putch
	
l670:	
	line	286
;main.c: 286: d=temperature/10;
	movlw	(0Ah)
	movwf	(?___lbdiv)
	movf	(_temperature),w
	fcall	___lbdiv
	movwf	(display_tt@d)
	line	287
	
l6226:	
;main.c: 287: lcd_putch(digits[d]);
	movf	(display_tt@d),w
	addlw	low(_digits|8000h)
	movwf	fsr0
	movlw	high(_digits|8000h)
	skipnc
	addlw	1
	movwf	btemp+1
	fcall	stringtab
	fcall	_lcd_putch
	line	288
	
l6228:	
;main.c: 288: d=temperature-((temperature/10)*10);
	movlw	(0F6h)
	movwf	(?___bmul)
	movlw	(0Ah)
	movwf	(?___lbdiv)
	movf	(_temperature),w
	fcall	___lbdiv
	fcall	___bmul
	movwf	(??_display_tt+0)+0
	movf	(_temperature),w
	addwf	0+(??_display_tt+0)+0,w
	movwf	(display_tt@d)
	line	289
;main.c: 289: lcd_putch(digits[d]);
	movf	(display_tt@d),w
	addlw	low(_digits|8000h)
	movwf	fsr0
	movlw	high(_digits|8000h)
	skipnc
	addlw	1
	movwf	btemp+1
	fcall	stringtab
	fcall	_lcd_putch
	line	290
	
l6230:	
	line	291
	
l6232:	
;main.c: 291: lcd_putch(d);
	movlw	(02Eh)
	fcall	_lcd_putch
	line	292
	
l6234:	
;main.c: 292: lcd_putch(digits[temp_drob]);
	movf	(_temp_drob),w
	addlw	low(_digits|8000h)
	movwf	fsr0
	movlw	high(_digits|8000h)
	skipnc
	addlw	1
	movwf	btemp+1
	fcall	stringtab
	fcall	_lcd_putch
	line	293
	
l6236:	
	line	294
;main.c: 294: lcd_putch(d);
	movlw	(0DFh)
	fcall	_lcd_putch
	line	295
	
l6238:	
	line	296
	
l6240:	
;main.c: 296: lcd_putch(d);
	movlw	(043h)
	fcall	_lcd_putch
	line	298
	
l671:	
	return
	opt stack 0
GLOBAL	__end_of_display_tt
	__end_of_display_tt:
;; =============== function _display_tt ends ============

	signat	_display_tt,88
	global	_menu
psect	text1219,local,class=CODE,delta=2
global __ptext1219
__ptext1219:

;; *************** function _menu *****************
;; Defined at:
;;		line 300 in file "X:\Hi-tech\USART_PIC16f877a\main.c"
;; Parameters:    Size  Location     Type
;;  i               1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  i               1    7[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_lcd_clear
;;		_lcd_write
;;		_lcd_goto
;;		_lcd_puts
;;		_lcd_putch
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text1219
	file	"X:\Hi-tech\USART_PIC16f877a\main.c"
	line	300
	global	__size_of_menu
	__size_of_menu	equ	__end_of_menu-_menu
	
_menu:	
	opt	stack 7
; Regs used in _menu: [wreg-fsr0h+status,2-btemp+1+pclath+cstack]
;menu@i stored from wreg
	movwf	(menu@i)
	line	301
	
l5426:	
;main.c: 301: lcd_clear();
	fcall	_lcd_clear
	line	302
	
l5428:	
;main.c: 302: RD2 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(66/8),(66)&7
	line	303
;main.c: 303: lcd_write(0b00001100);
	movlw	(0Ch)
	fcall	_lcd_write
	line	304
	
l5430:	
;main.c: 304: _delay((unsigned long)((100)*(4000000/4000000.0)));
	opt asmopt_off
movlw	33
movwf	(??_menu+0)+0,f
u2247:
decfsz	(??_menu+0)+0,f
	goto	u2247
opt asmopt_on

	line	305
;main.c: 305: switch (i) {
	goto	l5574
	line	307
	
l5432:	
;main.c: 307: lcd_goto(0x00);
	movlw	(0)
	fcall	_lcd_goto
	line	308
	
l5434:	
;main.c: 308: lcd_puts("Menu:");
	movlw	low(STR_16|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_16|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	309
	
l5436:	
;main.c: 309: lcd_goto(0x07);
	movlw	(07h)
	fcall	_lcd_goto
	line	310
	
l5438:	
;main.c: 310: lcd_putch(0b00111110);
	movlw	(03Eh)
	fcall	_lcd_putch
	line	311
	
l5440:	
;main.c: 311: lcd_goto(0x08);
	movlw	(08h)
	fcall	_lcd_goto
	line	312
;main.c: 312: lcd_puts("1.Volume");
	movlw	low(STR_17|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_17|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	313
	
l5442:	
;main.c: 313: lcd_goto(0x48);
	movlw	(048h)
	fcall	_lcd_goto
	line	314
	
l5444:	
;main.c: 314: lcd_puts("2.Bass");
	movlw	low(STR_18|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_18|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	315
;main.c: 315: break;
	goto	l686
	line	317
	
l5446:	
;main.c: 317: lcd_goto(0x00);
	movlw	(0)
	fcall	_lcd_goto
	line	318
	
l5448:	
;main.c: 318: lcd_puts("Menu:");
	movlw	low(STR_19|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_19|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	319
	
l5450:	
;main.c: 319: lcd_goto(0x08);
	movlw	(08h)
	fcall	_lcd_goto
	line	320
;main.c: 320: lcd_puts("1.Volume");
	movlw	low(STR_20|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_20|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	321
	
l5452:	
;main.c: 321: lcd_goto(0x47);
	movlw	(047h)
	fcall	_lcd_goto
	line	322
	
l5454:	
;main.c: 322: lcd_putch(0b00111110);
	movlw	(03Eh)
	fcall	_lcd_putch
	line	323
	
l5456:	
;main.c: 323: lcd_goto(0x48);
	movlw	(048h)
	fcall	_lcd_goto
	line	324
	
l5458:	
;main.c: 324: lcd_puts("2.Bass");
	movlw	low(STR_21|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_21|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	325
;main.c: 325: break;
	goto	l686
	line	327
	
l5460:	
;main.c: 327: lcd_goto(0x00);
	movlw	(0)
	fcall	_lcd_goto
	line	328
	
l5462:	
;main.c: 328: lcd_puts("Menu:");
	movlw	low(STR_22|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_22|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	329
	
l5464:	
;main.c: 329: lcd_goto(0x07);
	movlw	(07h)
	fcall	_lcd_goto
	line	330
	
l5466:	
;main.c: 330: lcd_putch(0b00111110);
	movlw	(03Eh)
	fcall	_lcd_putch
	line	331
	
l5468:	
;main.c: 331: lcd_goto(0x08);
	movlw	(08h)
	fcall	_lcd_goto
	line	332
;main.c: 332: lcd_puts("2.Bass");
	movlw	low(STR_23|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_23|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	333
	
l5470:	
;main.c: 333: lcd_goto(0x48);
	movlw	(048h)
	fcall	_lcd_goto
	line	334
	
l5472:	
;main.c: 334: lcd_puts("3.Treble");
	movlw	low(STR_24|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_24|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	335
;main.c: 335: break;
	goto	l686
	line	337
	
l5474:	
;main.c: 337: lcd_goto(0x00);
	movlw	(0)
	fcall	_lcd_goto
	line	338
	
l5476:	
;main.c: 338: lcd_puts("Menu:");
	movlw	low(STR_25|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_25|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	339
	
l5478:	
;main.c: 339: lcd_goto(0x08);
	movlw	(08h)
	fcall	_lcd_goto
	line	340
;main.c: 340: lcd_puts("2.Bass");
	movlw	low(STR_26|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_26|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	341
	
l5480:	
;main.c: 341: lcd_goto(0x47);
	movlw	(047h)
	fcall	_lcd_goto
	line	342
	
l5482:	
;main.c: 342: lcd_putch(0b00111110);
	movlw	(03Eh)
	fcall	_lcd_putch
	line	343
	
l5484:	
;main.c: 343: lcd_goto(0x48);
	movlw	(048h)
	fcall	_lcd_goto
	line	344
	
l5486:	
;main.c: 344: lcd_puts("3.Treble");
	movlw	low(STR_27|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_27|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	345
;main.c: 345: break;
	goto	l686
	line	347
	
l5488:	
;main.c: 347: lcd_goto(0x00);
	movlw	(0)
	fcall	_lcd_goto
	line	348
	
l5490:	
;main.c: 348: lcd_puts("Menu:");
	movlw	low(STR_28|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_28|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	349
	
l5492:	
;main.c: 349: lcd_goto(0x07);
	movlw	(07h)
	fcall	_lcd_goto
	line	350
	
l5494:	
;main.c: 350: lcd_putch(0b00111110);
	movlw	(03Eh)
	fcall	_lcd_putch
	line	351
	
l5496:	
;main.c: 351: lcd_goto(0x08);
	movlw	(08h)
	fcall	_lcd_goto
	line	352
;main.c: 352: lcd_puts("3.Treble");
	movlw	low(STR_29|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_29|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	353
	
l5498:	
;main.c: 353: lcd_goto(0x48);
	movlw	(048h)
	fcall	_lcd_goto
	line	354
	
l5500:	
;main.c: 354: lcd_puts("4.Mute");
	movlw	low(STR_30|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_30|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	355
;main.c: 355: break;
	goto	l686
	line	357
	
l5502:	
;main.c: 357: lcd_goto(0x00);
	movlw	(0)
	fcall	_lcd_goto
	line	358
	
l5504:	
;main.c: 358: lcd_puts("Menu:");
	movlw	low(STR_31|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_31|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	359
	
l5506:	
;main.c: 359: lcd_goto(0x08);
	movlw	(08h)
	fcall	_lcd_goto
	line	360
;main.c: 360: lcd_puts("3.Treble");
	movlw	low(STR_32|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_32|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	361
	
l5508:	
;main.c: 361: lcd_goto(0x47);
	movlw	(047h)
	fcall	_lcd_goto
	line	362
	
l5510:	
;main.c: 362: lcd_putch(0b00111110);
	movlw	(03Eh)
	fcall	_lcd_putch
	line	363
	
l5512:	
;main.c: 363: lcd_goto(0x48);
	movlw	(048h)
	fcall	_lcd_goto
	line	364
	
l5514:	
;main.c: 364: lcd_puts("4.Mute");
	movlw	low(STR_33|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_33|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	365
;main.c: 365: break;
	goto	l686
	line	367
	
l5516:	
;main.c: 367: lcd_goto(0x00);
	movlw	(0)
	fcall	_lcd_goto
	line	368
	
l5518:	
;main.c: 368: lcd_puts("Menu:");
	movlw	low(STR_34|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_34|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	369
	
l5520:	
;main.c: 369: lcd_goto(0x07);
	movlw	(07h)
	fcall	_lcd_goto
	line	370
	
l5522:	
;main.c: 370: lcd_putch(0b00111110);
	movlw	(03Eh)
	fcall	_lcd_putch
	line	371
	
l5524:	
;main.c: 371: lcd_goto(0x08);
	movlw	(08h)
	fcall	_lcd_goto
	line	372
;main.c: 372: lcd_puts("4.Mute");
	movlw	low(STR_35|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_35|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	373
	
l5526:	
;main.c: 373: lcd_goto(0x48);
	movlw	(048h)
	fcall	_lcd_goto
	line	374
	
l5528:	
;main.c: 374: lcd_puts("5.Time");
	movlw	low(STR_36|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_36|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	375
;main.c: 375: break;
	goto	l686
	line	377
	
l5530:	
;main.c: 377: lcd_goto(0x00);
	movlw	(0)
	fcall	_lcd_goto
	line	378
	
l5532:	
;main.c: 378: lcd_puts("Menu:");
	movlw	low(STR_37|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_37|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	379
	
l5534:	
;main.c: 379: lcd_goto(0x08);
	movlw	(08h)
	fcall	_lcd_goto
	line	380
;main.c: 380: lcd_puts("4.Mute");
	movlw	low(STR_38|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_38|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	381
	
l5536:	
;main.c: 381: lcd_goto(0x47);
	movlw	(047h)
	fcall	_lcd_goto
	line	382
	
l5538:	
;main.c: 382: lcd_putch(0b00111110);
	movlw	(03Eh)
	fcall	_lcd_putch
	line	383
	
l5540:	
;main.c: 383: lcd_goto(0x48);
	movlw	(048h)
	fcall	_lcd_goto
	line	384
	
l5542:	
;main.c: 384: lcd_puts("5.Time");
	movlw	low(STR_39|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_39|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	385
;main.c: 385: break;
	goto	l686
	line	387
	
l5544:	
;main.c: 387: lcd_goto(0x00);
	movlw	(0)
	fcall	_lcd_goto
	line	388
	
l5546:	
;main.c: 388: lcd_puts("Menu:");
	movlw	low(STR_40|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_40|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	389
	
l5548:	
;main.c: 389: lcd_goto(0x07);
	movlw	(07h)
	fcall	_lcd_goto
	line	390
	
l5550:	
;main.c: 390: lcd_putch(0b00111110);
	movlw	(03Eh)
	fcall	_lcd_putch
	line	391
	
l5552:	
;main.c: 391: lcd_goto(0x08);
	movlw	(08h)
	fcall	_lcd_goto
	line	392
;main.c: 392: lcd_puts("5.Time");
	movlw	low(STR_41|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_41|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	393
	
l5554:	
;main.c: 393: lcd_goto(0x48);
	movlw	(048h)
	fcall	_lcd_goto
	line	394
	
l5556:	
;main.c: 394: lcd_puts("6.StBy");
	movlw	low(STR_42|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_42|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	395
;main.c: 395: break;
	goto	l686
	line	397
	
l5558:	
;main.c: 397: lcd_goto(0x00);
	movlw	(0)
	fcall	_lcd_goto
	line	398
	
l5560:	
;main.c: 398: lcd_puts("Menu:");
	movlw	low(STR_43|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_43|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	399
	
l5562:	
;main.c: 399: lcd_goto(0x08);
	movlw	(08h)
	fcall	_lcd_goto
	line	400
;main.c: 400: lcd_puts("5.Time");
	movlw	low(STR_44|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_44|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	401
	
l5564:	
;main.c: 401: lcd_goto(0x47);
	movlw	(047h)
	fcall	_lcd_goto
	line	402
	
l5566:	
;main.c: 402: lcd_putch(0b00111110);
	movlw	(03Eh)
	fcall	_lcd_putch
	line	403
	
l5568:	
;main.c: 403: lcd_goto(0x48);
	movlw	(048h)
	fcall	_lcd_goto
	line	404
	
l5570:	
;main.c: 404: lcd_puts("6.StBy");
	movlw	low(STR_45|8000h)
	movwf	(?_lcd_puts)
	movlw	high(STR_45|8000h)
	movwf	((?_lcd_puts))+1
	fcall	_lcd_puts
	line	405
;main.c: 405: break;
	goto	l686
	line	305
	
l5574:	
	movf	(menu@i),w
	; Switch size 1, requested type "space"
; Number of cases is 10, Range of values is 1 to 10
; switch strategies available:
; Name         Bytes Cycles
; simple_byte    31    16 (average)
; direct_byte    52    22 (fixed)
;	Chosen strategy is simple_byte

	xorlw	1^0	; case 1
	skipnz
	goto	l5432
	xorlw	2^1	; case 2
	skipnz
	goto	l5446
	xorlw	3^2	; case 3
	skipnz
	goto	l5460
	xorlw	4^3	; case 4
	skipnz
	goto	l5474
	xorlw	5^4	; case 5
	skipnz
	goto	l5488
	xorlw	6^5	; case 6
	skipnz
	goto	l5502
	xorlw	7^6	; case 7
	skipnz
	goto	l5516
	xorlw	8^7	; case 8
	skipnz
	goto	l5530
	xorlw	9^8	; case 9
	skipnz
	goto	l5544
	xorlw	10^9	; case 10
	skipnz
	goto	l5558
	goto	l686

	line	407
	
l686:	
	return
	opt stack 0
GLOBAL	__end_of_menu
	__end_of_menu:
;; =============== function _menu ends ============

	signat	_menu,4216
	global	_lcd_init
psect	text1220,local,class=CODE,delta=2
global __ptext1220
__ptext1220:

;; *************** function _lcd_init *****************
;; Defined at:
;;		line 75 in file "X:\Hi-tech\USART_PIC16f877a\lcd.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/20
;;		On exit  : 60/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          2       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_lcd_write
;;		_lcd_clear
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text1220
	file	"X:\Hi-tech\USART_PIC16f877a\lcd.c"
	line	75
	global	__size_of_lcd_init
	__size_of_lcd_init	equ	__end_of_lcd_init-_lcd_init
	
_lcd_init:	
	opt	stack 7
; Regs used in _lcd_init: [wreg+status,2+status,0+pclath+cstack]
	line	77
	
l5382:	
;lcd.c: 77: RD2 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	(66/8),(66)&7
	line	78
;lcd.c: 78: RD3 = 0;
	bcf	(67/8),(67)&7
	line	81
	
l5384:	
;lcd.c: 81: _delay((unsigned long)((15)*(4000000/4000.0)));
	opt asmopt_off
movlw	20
movwf	((??_lcd_init+0)+0+1),f
	movlw	121
movwf	((??_lcd_init+0)+0),f
u2257:
	decfsz	((??_lcd_init+0)+0),f
	goto	u2257
	decfsz	((??_lcd_init+0)+0+1),f
	goto	u2257
	nop2
opt asmopt_on

	line	82
	
l5386:	
;lcd.c: 82: RD4 = 1;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bsf	(68/8),(68)&7
	line	83
	
l5388:	
;lcd.c: 83: RD5 = 1;
	bsf	(69/8),(69)&7
	line	84
	
l5390:	
;lcd.c: 84: RD6 = 0;
	bcf	(70/8),(70)&7
	line	85
	
l5392:	
;lcd.c: 85: RD7 = 0;
	bcf	(71/8),(71)&7
	line	86
	
l5394:	
;lcd.c: 86: ((RD3 = 1),(RD3=0));
	bsf	(67/8),(67)&7
	
l5396:	
	bcf	(67/8),(67)&7
	line	87
;lcd.c: 87: _delay((unsigned long)((5)*(4000000/4000.0)));
	opt asmopt_off
movlw	7
movwf	((??_lcd_init+0)+0+1),f
	movlw	125
movwf	((??_lcd_init+0)+0),f
u2267:
	decfsz	((??_lcd_init+0)+0),f
	goto	u2267
	decfsz	((??_lcd_init+0)+0+1),f
	goto	u2267
opt asmopt_on

	line	88
	
l5398:	
;lcd.c: 88: ((RD3 = 1),(RD3=0));
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bsf	(67/8),(67)&7
	
l5400:	
	bcf	(67/8),(67)&7
	line	89
;lcd.c: 89: _delay((unsigned long)((200)*(4000000/4000000.0)));
	opt asmopt_off
movlw	66
movwf	(??_lcd_init+0)+0,f
u2277:
decfsz	(??_lcd_init+0)+0,f
	goto	u2277
	clrwdt
opt asmopt_on

	line	90
	
l5402:	
;lcd.c: 90: ((RD3 = 1),(RD3=0));
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bsf	(67/8),(67)&7
	
l5404:	
	bcf	(67/8),(67)&7
	line	91
;lcd.c: 91: _delay((unsigned long)((200)*(4000000/4000000.0)));
	opt asmopt_off
movlw	66
movwf	(??_lcd_init+0)+0,f
u2287:
decfsz	(??_lcd_init+0)+0,f
	goto	u2287
	clrwdt
opt asmopt_on

	line	92
	
l5406:	
;lcd.c: 92: RD4 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(68/8),(68)&7
	line	93
	
l5408:	
;lcd.c: 93: RD5 = 1;
	bsf	(69/8),(69)&7
	line	94
	
l5410:	
;lcd.c: 94: RD6 = 0;
	bcf	(70/8),(70)&7
	line	95
	
l5412:	
;lcd.c: 95: RD7 = 0;
	bcf	(71/8),(71)&7
	line	96
	
l5414:	
;lcd.c: 96: ((RD3 = 1),(RD3=0));
	bsf	(67/8),(67)&7
	
l5416:	
	bcf	(67/8),(67)&7
	line	98
	
l5418:	
;lcd.c: 98: lcd_write(0x28);
	movlw	(028h)
	fcall	_lcd_write
	line	99
	
l5420:	
;lcd.c: 99: lcd_write(0xF);
	movlw	(0Fh)
	fcall	_lcd_write
	line	100
	
l5422:	
;lcd.c: 100: lcd_clear();
	fcall	_lcd_clear
	line	101
	
l5424:	
;lcd.c: 101: lcd_write(0x6);
	movlw	(06h)
	fcall	_lcd_write
	line	102
	
l3088:	
	return
	opt stack 0
GLOBAL	__end_of_lcd_init
	__end_of_lcd_init:
;; =============== function _lcd_init ends ============

	signat	_lcd_init,88
	global	_SetSeconds
psect	text1221,local,class=CODE,delta=2
global __ptext1221
__ptext1221:

;; *************** function _SetSeconds *****************
;; Defined at:
;;		line 77 in file "X:\Hi-tech\USART_PIC16f877a\rtc.c"
;; Parameters:    Size  Location     Type
;;  seconds         1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  seconds         1    3[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/20
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_I2CStart
;;		_I2CSend
;;		_DCBconv
;;		_I2CStop
;; This function is called by:
;;		_SetupTime
;; This function uses a non-reentrant model
;;
psect	text1221
	file	"X:\Hi-tech\USART_PIC16f877a\rtc.c"
	line	77
	global	__size_of_SetSeconds
	__size_of_SetSeconds	equ	__end_of_SetSeconds-_SetSeconds
	
_SetSeconds:	
	opt	stack 6
; Regs used in _SetSeconds: [wreg+status,2+status,0+pclath+cstack]
;SetSeconds@seconds stored from wreg
	movwf	(SetSeconds@seconds)
	line	78
	
l6176:	
;rtc.c: 78: I2CStart();
	fcall	_I2CStart
	line	79
	
l6178:	
;rtc.c: 79: I2CSend(0b11010000);
	movlw	(0D0h)
	fcall	_I2CSend
	line	80
	
l6180:	
;rtc.c: 80: I2CSend(0x00);
	movlw	(0)
	fcall	_I2CSend
	line	81
	
l6182:	
;rtc.c: 81: I2CSend(DCBconv(seconds));
	bcf	status, 5	;RP0=0, select bank0
	movf	(SetSeconds@seconds),w
	fcall	_DCBconv
	fcall	_I2CSend
	line	82
	
l6184:	
;rtc.c: 82: I2CStop();
	fcall	_I2CStop
	line	83
	
l1862:	
	return
	opt stack 0
GLOBAL	__end_of_SetSeconds
	__end_of_SetSeconds:
;; =============== function _SetSeconds ends ============

	signat	_SetSeconds,4216
	global	_SetHour
psect	text1222,local,class=CODE,delta=2
global __ptext1222
__ptext1222:

;; *************** function _SetHour *****************
;; Defined at:
;;		line 53 in file "X:\Hi-tech\USART_PIC16f877a\rtc.c"
;; Parameters:    Size  Location     Type
;;  hours           1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  hours           1    3[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/20
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_I2CStart
;;		_I2CSend
;;		_DCBconv
;;		_I2CStop
;; This function is called by:
;;		_SetupTime
;; This function uses a non-reentrant model
;;
psect	text1222
	file	"X:\Hi-tech\USART_PIC16f877a\rtc.c"
	line	53
	global	__size_of_SetHour
	__size_of_SetHour	equ	__end_of_SetHour-_SetHour
	
_SetHour:	
	opt	stack 6
; Regs used in _SetHour: [wreg+status,2+status,0+pclath+cstack]
;SetHour@hours stored from wreg
	movwf	(SetHour@hours)
	line	54
	
l6166:	
;rtc.c: 54: I2CStart();
	fcall	_I2CStart
	line	55
	
l6168:	
;rtc.c: 55: I2CSend(0b11010000);
	movlw	(0D0h)
	fcall	_I2CSend
	line	56
	
l6170:	
;rtc.c: 56: I2CSend(0x02);
	movlw	(02h)
	fcall	_I2CSend
	line	57
	
l6172:	
;rtc.c: 57: I2CSend(DCBconv(hours));
	bcf	status, 5	;RP0=0, select bank0
	movf	(SetHour@hours),w
	fcall	_DCBconv
	fcall	_I2CSend
	line	58
	
l6174:	
;rtc.c: 58: I2CStop();
	fcall	_I2CStop
	line	59
	
l1856:	
	return
	opt stack 0
GLOBAL	__end_of_SetHour
	__end_of_SetHour:
;; =============== function _SetHour ends ============

	signat	_SetHour,4216
	global	_SetMin
psect	text1223,local,class=CODE,delta=2
global __ptext1223
__ptext1223:

;; *************** function _SetMin *****************
;; Defined at:
;;		line 31 in file "X:\Hi-tech\USART_PIC16f877a\rtc.c"
;; Parameters:    Size  Location     Type
;;  minutes         1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  minutes         1    3[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/20
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_I2CStart
;;		_I2CSend
;;		_DCBconv
;;		_I2CStop
;; This function is called by:
;;		_SetupTime
;; This function uses a non-reentrant model
;;
psect	text1223
	file	"X:\Hi-tech\USART_PIC16f877a\rtc.c"
	line	31
	global	__size_of_SetMin
	__size_of_SetMin	equ	__end_of_SetMin-_SetMin
	
_SetMin:	
	opt	stack 6
; Regs used in _SetMin: [wreg+status,2+status,0+pclath+cstack]
;SetMin@minutes stored from wreg
	movwf	(SetMin@minutes)
	line	32
	
l6156:	
;rtc.c: 32: I2CStart();
	fcall	_I2CStart
	line	33
	
l6158:	
;rtc.c: 33: I2CSend(0b11010000);
	movlw	(0D0h)
	fcall	_I2CSend
	line	34
	
l6160:	
;rtc.c: 34: I2CSend(0x01);
	movlw	(01h)
	fcall	_I2CSend
	line	35
	
l6162:	
;rtc.c: 35: I2CSend(DCBconv(minutes));
	bcf	status, 5	;RP0=0, select bank0
	movf	(SetMin@minutes),w
	fcall	_DCBconv
	fcall	_I2CSend
	line	36
	
l6164:	
;rtc.c: 36: I2CStop();
	fcall	_I2CStop
	line	37
	
l1850:	
	return
	opt stack 0
GLOBAL	__end_of_SetMin
	__end_of_SetMin:
;; =============== function _SetMin ends ============

	signat	_SetMin,4216
	global	_lcd_puts
psect	text1224,local,class=CODE,delta=2
global __ptext1224
__ptext1224:

;; *************** function _lcd_puts *****************
;; Defined at:
;;		line 44 in file "X:\Hi-tech\USART_PIC16f877a\lcd.c"
;; Parameters:    Size  Location     Type
;;  s               2    4[COMMON] PTR const unsigned char 
;;		 -> STR_45(7), STR_44(7), STR_43(6), STR_42(7), 
;;		 -> STR_41(7), STR_40(6), STR_39(7), STR_38(7), 
;;		 -> STR_37(6), STR_36(7), STR_35(7), STR_34(6), 
;;		 -> STR_33(7), STR_32(9), STR_31(6), STR_30(7), 
;;		 -> STR_29(9), STR_28(6), STR_27(9), STR_26(7), 
;;		 -> STR_25(6), STR_24(9), STR_23(7), STR_22(6), 
;;		 -> STR_21(7), STR_20(9), STR_19(6), STR_18(7), 
;;		 -> STR_17(9), STR_16(6), 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         2       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_lcd_write
;; This function is called by:
;;		_menu
;; This function uses a non-reentrant model
;;
psect	text1224
	file	"X:\Hi-tech\USART_PIC16f877a\lcd.c"
	line	44
	global	__size_of_lcd_puts
	__size_of_lcd_puts	equ	__end_of_lcd_puts-_lcd_puts
	
_lcd_puts:	
	opt	stack 6
; Regs used in _lcd_puts: [wreg-fsr0h+status,2-btemp+1+pclath+cstack]
	line	45
	
l5344:	
;lcd.c: 45: RD2 = 1;
	bsf	(66/8),(66)&7
	line	46
;lcd.c: 46: while(*s)
	goto	l5350
	line	47
	
l5346:	
;lcd.c: 47: lcd_write(*s++);
	movf	(lcd_puts@s+1),w
	movwf	btemp+1
	movf	(lcd_puts@s),w
	movwf	fsr0
	fcall	stringtab
	fcall	_lcd_write
	
l5348:	
	incf	(lcd_puts@s),f
	skipnz
	incf	(lcd_puts@s+1),f
	line	46
	
l5350:	
	movf	(lcd_puts@s+1),w
	movwf	btemp+1
	movf	(lcd_puts@s),w
	movwf	fsr0
	fcall	stringtab
	iorlw	0
	skipz
	goto	u1501
	goto	u1500
u1501:
	goto	l5346
u1500:
	line	48
	
l3079:	
	return
	opt stack 0
GLOBAL	__end_of_lcd_puts
	__end_of_lcd_puts:
;; =============== function _lcd_puts ends ============

	signat	_lcd_puts,4216
	global	_lcd_putch
psect	text1225,local,class=CODE,delta=2
global __ptext1225
__ptext1225:

;; *************** function _lcd_putch *****************
;; Defined at:
;;		line 55 in file "X:\Hi-tech\USART_PIC16f877a\lcd.c"
;; Parameters:    Size  Location     Type
;;  c               1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  c               1    4[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         1       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_lcd_write
;; This function is called by:
;;		_display_tt
;;		_menu
;; This function uses a non-reentrant model
;;
psect	text1225
	file	"X:\Hi-tech\USART_PIC16f877a\lcd.c"
	line	55
	global	__size_of_lcd_putch
	__size_of_lcd_putch	equ	__end_of_lcd_putch-_lcd_putch
	
_lcd_putch:	
	opt	stack 6
; Regs used in _lcd_putch: [wreg+status,2+status,0+pclath+cstack]
;lcd_putch@c stored from wreg
	movwf	(lcd_putch@c)
	line	56
	
l5340:	
;lcd.c: 56: RD2 = 1;
	bsf	(66/8),(66)&7
	line	57
	
l5342:	
;lcd.c: 57: lcd_write( c );
	movf	(lcd_putch@c),w
	fcall	_lcd_write
	line	58
	
l3082:	
	return
	opt stack 0
GLOBAL	__end_of_lcd_putch
	__end_of_lcd_putch:
;; =============== function _lcd_putch ends ============

	signat	_lcd_putch,4216
	global	_lcd_goto
psect	text1226,local,class=CODE,delta=2
global __ptext1226
__ptext1226:

;; *************** function _lcd_goto *****************
;; Defined at:
;;		line 66 in file "X:\Hi-tech\USART_PIC16f877a\lcd.c"
;; Parameters:    Size  Location     Type
;;  pos             1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  pos             1    4[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 60/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         1       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_lcd_write
;; This function is called by:
;;		_display_tt
;;		_menu
;; This function uses a non-reentrant model
;;
psect	text1226
	file	"X:\Hi-tech\USART_PIC16f877a\lcd.c"
	line	66
	global	__size_of_lcd_goto
	__size_of_lcd_goto	equ	__end_of_lcd_goto-_lcd_goto
	
_lcd_goto:	
	opt	stack 6
; Regs used in _lcd_goto: [wreg+status,2+status,0+pclath+cstack]
;lcd_goto@pos stored from wreg
	movwf	(lcd_goto@pos)
	line	67
	
l5336:	
;lcd.c: 67: RD2 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(66/8),(66)&7
	line	68
	
l5338:	
;lcd.c: 68: lcd_write(0x80+pos);
	movf	(lcd_goto@pos),w
	addlw	080h
	fcall	_lcd_write
	line	69
	
l3085:	
	return
	opt stack 0
GLOBAL	__end_of_lcd_goto
	__end_of_lcd_goto:
;; =============== function _lcd_goto ends ============

	signat	_lcd_goto,4216
	global	_lcd_clear
psect	text1227,local,class=CODE,delta=2
global __ptext1227
__ptext1227:

;; *************** function _lcd_clear *****************
;; Defined at:
;;		line 34 in file "X:\Hi-tech\USART_PIC16f877a\lcd.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          2       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_lcd_write
;; This function is called by:
;;		_display_tt
;;		_menu
;;		_lcd_init
;; This function uses a non-reentrant model
;;
psect	text1227
	file	"X:\Hi-tech\USART_PIC16f877a\lcd.c"
	line	34
	global	__size_of_lcd_clear
	__size_of_lcd_clear	equ	__end_of_lcd_clear-_lcd_clear
	
_lcd_clear:	
	opt	stack 6
; Regs used in _lcd_clear: [wreg+status,2+status,0+pclath+cstack]
	line	35
	
l5330:	
;lcd.c: 35: RD2 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(66/8),(66)&7
	line	36
	
l5332:	
;lcd.c: 36: lcd_write(0x1);
	movlw	(01h)
	fcall	_lcd_write
	line	37
	
l5334:	
;lcd.c: 37: _delay((unsigned long)((2)*(4000000/4000.0)));
	opt asmopt_off
movlw	3
movwf	((??_lcd_clear+0)+0+1),f
	movlw	151
movwf	((??_lcd_clear+0)+0),f
u2297:
	decfsz	((??_lcd_clear+0)+0),f
	goto	u2297
	decfsz	((??_lcd_clear+0)+0+1),f
	goto	u2297
	nop2
opt asmopt_on

	line	38
	
l3073:	
	return
	opt stack 0
GLOBAL	__end_of_lcd_clear
	__end_of_lcd_clear:
;; =============== function _lcd_clear ends ============

	signat	_lcd_clear,88
	global	_get_temp
psect	text1228,local,class=CODE,delta=2
global __ptext1228
__ptext1228:

;; *************** function _get_temp *****************
;; Defined at:
;;		line 210 in file "X:\Hi-tech\USART_PIC16f877a\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  temp2           1   21[BANK0 ] unsigned char 
;;  temp1           1   20[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       2       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0       4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_INIT
;;		_TX
;;		_printf
;;		_RX
;;		___wmul
;;		___awdiv
;; This function is called by:
;;		_printTemp
;;		_display_tt
;; This function uses a non-reentrant model
;;
psect	text1228
	file	"X:\Hi-tech\USART_PIC16f877a\main.c"
	line	210
	global	__size_of_get_temp
	__size_of_get_temp	equ	__end_of_get_temp-_get_temp
	
_get_temp:	
	opt	stack 6
; Regs used in _get_temp: [wreg-fsr0h+status,2-btemp+1+pclath+cstack]
	line	214
	
l6112:	
;main.c: 211: static bit init;
;main.c: 212: unsigned char temp1;
;main.c: 213: unsigned char temp2;
;main.c: 214: init = INIT();
	fcall	_INIT
	bcf	(get_temp@init/8),(get_temp@init)&7
	btfss	status,0
	goto	u1955
	bsf	(get_temp@init/8),(get_temp@init)&7
u1955:

	line	215
	
l6114:	
;main.c: 215: if (!init) {
	btfsc	(get_temp@init/8),(get_temp@init)&7
	goto	u1961
	goto	u1960
u1961:
	goto	l6128
u1960:
	line	216
	
l6116:	
;main.c: 216: TX(0xCC);
	movlw	(0CCh)
	fcall	_TX
	line	217
;main.c: 217: TX(0x44);
	movlw	(044h)
	fcall	_TX
	line	218
	
l6118:	
;main.c: 218: _delay((unsigned long)((150)*(4000000/4000.0)));
	opt asmopt_off
movlw	195
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
movwf	((??_get_temp+0)+0+1),f
	movlw	205
movwf	((??_get_temp+0)+0),f
u2307:
	decfsz	((??_get_temp+0)+0),f
	goto	u2307
	decfsz	((??_get_temp+0)+0+1),f
	goto	u2307
opt asmopt_on

	line	219
	
l6120:	
;main.c: 219: _delay((unsigned long)((150)*(4000000/4000.0)));
	opt asmopt_off
movlw	195
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
movwf	((??_get_temp+0)+0+1),f
	movlw	205
movwf	((??_get_temp+0)+0),f
u2317:
	decfsz	((??_get_temp+0)+0),f
	goto	u2317
	decfsz	((??_get_temp+0)+0+1),f
	goto	u2317
opt asmopt_on

	line	220
	
l6122:	
;main.c: 220: _delay((unsigned long)((150)*(4000000/4000.0)));
	opt asmopt_off
movlw	195
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
movwf	((??_get_temp+0)+0+1),f
	movlw	205
movwf	((??_get_temp+0)+0),f
u2327:
	decfsz	((??_get_temp+0)+0),f
	goto	u2327
	decfsz	((??_get_temp+0)+0+1),f
	goto	u2327
opt asmopt_on

	line	221
	
l6124:	
;main.c: 221: _delay((unsigned long)((150)*(4000000/4000.0)));
	opt asmopt_off
movlw	195
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
movwf	((??_get_temp+0)+0+1),f
	movlw	205
movwf	((??_get_temp+0)+0),f
u2337:
	decfsz	((??_get_temp+0)+0),f
	goto	u2337
	decfsz	((??_get_temp+0)+0+1),f
	goto	u2337
opt asmopt_on

	line	222
	
l6126:	
;main.c: 222: _delay((unsigned long)((150)*(4000000/4000.0)));
	opt asmopt_off
movlw	195
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
movwf	((??_get_temp+0)+0+1),f
	movlw	205
movwf	((??_get_temp+0)+0),f
u2347:
	decfsz	((??_get_temp+0)+0),f
	goto	u2347
	decfsz	((??_get_temp+0)+0+1),f
	goto	u2347
opt asmopt_on

	line	223
;main.c: 223: } else printf("bug");
	goto	l6130
	
l6128:	
	movlw	low(STR_7|8000h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?_printf)
	movlw	high(STR_7|8000h)
	movwf	((?_printf))+1
	fcall	_printf
	line	224
	
l6130:	
;main.c: 224: init = INIT();
	fcall	_INIT
	bcf	(get_temp@init/8),(get_temp@init)&7
	btfss	status,0
	goto	u1975
	bsf	(get_temp@init/8),(get_temp@init)&7
u1975:

	line	225
	
l6132:	
;main.c: 225: if (!init) {
	btfsc	(get_temp@init/8),(get_temp@init)&7
	goto	u1981
	goto	u1980
u1981:
	goto	l6136
u1980:
	line	226
	
l6134:	
;main.c: 226: TX(0xCC);
	movlw	(0CCh)
	fcall	_TX
	line	227
;main.c: 227: TX(0xBE);
	movlw	(0BEh)
	fcall	_TX
	line	229
;main.c: 229: temp1 = RX();
	fcall	_RX
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(get_temp@temp1)
	line	230
;main.c: 230: temp2 = RX();
	fcall	_RX
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(get_temp@temp2)
	line	233
	
l6136:	
;main.c: 232: }
;main.c: 233: temp_drob = temp1 & 0b00001111;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(get_temp@temp1),w
	movwf	(_temp_drob)
	
l6138:	
	movlw	(0Fh)
	andwf	(_temp_drob),f
	line	234
	
l6140:	
;main.c: 234: temp_drob = ((temp_drob*6)+2)/10;
	movlw	0Ah
	movwf	(?___awdiv)
	clrf	(?___awdiv+1)
	movf	(_temp_drob),w
	movwf	(?___wmul)
	clrf	(?___wmul+1)
	movlw	06h
	movwf	0+(?___wmul)+02h
	clrf	1+(?___wmul)+02h
	fcall	___wmul
	movf	(0+(?___wmul)),w
	addlw	low(02h)
	movwf	0+(?___awdiv)+02h
	movf	(1+(?___wmul)),w
	skipnc
	addlw	1
	addlw	high(02h)
	movwf	1+0+(?___awdiv)+02h
	fcall	___awdiv
	movf	(0+(?___awdiv)),w
	movwf	(_temp_drob)
	line	235
	
l6142:	
;main.c: 235: temp1 >>= 4;
swapf	(get_temp@temp1),f
	movlw	15
	andwf	(get_temp@temp1),f

	line	236
	
l6144:	
;main.c: 236: sign = temp2 & 0x80;
	movf	(get_temp@temp2),w
	movwf	(_sign)
	
l6146:	
	movlw	(080h)
	andwf	(_sign),f
	line	237
;main.c: 237: temp2 <<= 4;
swapf	(get_temp@temp2),f
	movlw	240
	andwf	(get_temp@temp2),f

	line	238
	
l6148:	
;main.c: 238: temp2 &= 0b01110000;
	movlw	(070h)
	andwf	(get_temp@temp2),f
	line	239
	
l6150:	
;main.c: 239: temp2 |= temp1;
	movf	(get_temp@temp1),w
	iorwf	(get_temp@temp2),f
	line	241
;main.c: 241: if (sign) { temperature = 127-temp2;
	movf	(_sign),w
	skipz
	goto	u1990
	goto	l6154
u1990:
	
l6152:	
	movf	(get_temp@temp2),w
	sublw	07Fh
	movwf	(_temperature)
	line	242
;main.c: 242: temp_drob = 10 - temp_drob;
	movf	(_temp_drob),w
	sublw	0Ah
	movwf	(_temp_drob)
	line	243
;main.c: 243: } else temperature = temp2;
	goto	l655
	
l6154:	
	movf	(get_temp@temp2),w
	movwf	(_temperature)
	line	246
	
l655:	
	return
	opt stack 0
GLOBAL	__end_of_get_temp
	__end_of_get_temp:
;; =============== function _get_temp ends ============

	signat	_get_temp,88
	global	_ReadSeconds
psect	text1229,local,class=CODE,delta=2
global __ptext1229
__ptext1229:

;; *************** function _ReadSeconds *****************
;; Defined at:
;;		line 85 in file "X:\Hi-tech\USART_PIC16f877a\rtc.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  temp            1   18[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_I2CStart
;;		_I2CSend
;;		_I2CRestart
;;		_I2CRead
;;		_I2CNak
;;		_I2CStop
;;		_BCDconv
;; This function is called by:
;;		_main
;;		_ShowTime
;; This function uses a non-reentrant model
;;
psect	text1229
	file	"X:\Hi-tech\USART_PIC16f877a\rtc.c"
	line	85
	global	__size_of_ReadSeconds
	__size_of_ReadSeconds	equ	__end_of_ReadSeconds-_ReadSeconds
	
_ReadSeconds:	
	opt	stack 7
; Regs used in _ReadSeconds: [wreg+status,2+status,0+pclath+cstack]
	line	86
	
l6090:	
	line	87
	
l6092:	
;rtc.c: 87: I2CStart();
	fcall	_I2CStart
	line	88
	
l6094:	
;rtc.c: 88: I2CSend(0b11010000);
	movlw	(0D0h)
	fcall	_I2CSend
	line	89
	
l6096:	
;rtc.c: 89: I2CSend(0x00);
	movlw	(0)
	fcall	_I2CSend
	line	90
	
l6098:	
;rtc.c: 90: I2CRestart();
	fcall	_I2CRestart
	line	91
	
l6100:	
;rtc.c: 91: I2CSend(0b11010001);
	movlw	(0D1h)
	fcall	_I2CSend
	line	92
	
l6102:	
;rtc.c: 92: temp = I2CRead();
	fcall	_I2CRead
	bcf	status, 5	;RP0=0, select bank0
	movwf	(ReadSeconds@temp)
	line	93
	
l6104:	
;rtc.c: 93: I2CNak();
	fcall	_I2CNak
	line	94
	
l6106:	
;rtc.c: 94: I2CStop();
	fcall	_I2CStop
	line	96
	
l6108:	
;rtc.c: 96: return BCDconv(temp);
	bcf	status, 5	;RP0=0, select bank0
	movf	(ReadSeconds@temp),w
	fcall	_BCDconv
	line	97
	
l1865:	
	return
	opt stack 0
GLOBAL	__end_of_ReadSeconds
	__end_of_ReadSeconds:
;; =============== function _ReadSeconds ends ============

	signat	_ReadSeconds,89
	global	_ReadMin
psect	text1230,local,class=CODE,delta=2
global __ptext1230
__ptext1230:

;; *************** function _ReadMin *****************
;; Defined at:
;;		line 39 in file "X:\Hi-tech\USART_PIC16f877a\rtc.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  temp            1   18[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_I2CStart
;;		_I2CSend
;;		_I2CRestart
;;		_I2CRead
;;		_I2CNak
;;		_I2CStop
;;		_BCDconv
;; This function is called by:
;;		_main
;;		_display_tt
;;		_ShowTime
;; This function uses a non-reentrant model
;;
psect	text1230
	file	"X:\Hi-tech\USART_PIC16f877a\rtc.c"
	line	39
	global	__size_of_ReadMin
	__size_of_ReadMin	equ	__end_of_ReadMin-_ReadMin
	
_ReadMin:	
	opt	stack 7
; Regs used in _ReadMin: [wreg+status,2+status,0+pclath+cstack]
	line	40
	
l6068:	
	line	41
	
l6070:	
;rtc.c: 41: I2CStart();
	fcall	_I2CStart
	line	42
	
l6072:	
;rtc.c: 42: I2CSend(0b11010000);
	movlw	(0D0h)
	fcall	_I2CSend
	line	43
	
l6074:	
;rtc.c: 43: I2CSend(0x01);
	movlw	(01h)
	fcall	_I2CSend
	line	44
	
l6076:	
;rtc.c: 44: I2CRestart();
	fcall	_I2CRestart
	line	45
	
l6078:	
;rtc.c: 45: I2CSend(0b11010001);
	movlw	(0D1h)
	fcall	_I2CSend
	line	46
	
l6080:	
;rtc.c: 46: temp = I2CRead();
	fcall	_I2CRead
	bcf	status, 5	;RP0=0, select bank0
	movwf	(ReadMin@temp)
	line	47
	
l6082:	
;rtc.c: 47: I2CNak();
	fcall	_I2CNak
	line	48
	
l6084:	
;rtc.c: 48: I2CStop();
	fcall	_I2CStop
	line	50
	
l6086:	
;rtc.c: 50: return BCDconv(temp);
	bcf	status, 5	;RP0=0, select bank0
	movf	(ReadMin@temp),w
	fcall	_BCDconv
	line	51
	
l1853:	
	return
	opt stack 0
GLOBAL	__end_of_ReadMin
	__end_of_ReadMin:
;; =============== function _ReadMin ends ============

	signat	_ReadMin,89
	global	_ReadHour
psect	text1231,local,class=CODE,delta=2
global __ptext1231
__ptext1231:

;; *************** function _ReadHour *****************
;; Defined at:
;;		line 61 in file "X:\Hi-tech\USART_PIC16f877a\rtc.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  temp            1   18[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_I2CStart
;;		_I2CSend
;;		_I2CRestart
;;		_I2CRead
;;		_I2CNak
;;		_I2CStop
;;		_BCDconv
;; This function is called by:
;;		_main
;;		_display_tt
;;		_ShowTime
;; This function uses a non-reentrant model
;;
psect	text1231
	file	"X:\Hi-tech\USART_PIC16f877a\rtc.c"
	line	61
	global	__size_of_ReadHour
	__size_of_ReadHour	equ	__end_of_ReadHour-_ReadHour
	
_ReadHour:	
	opt	stack 7
; Regs used in _ReadHour: [wreg+status,2+status,0+pclath+cstack]
	line	62
	
l6046:	
	line	63
	
l6048:	
;rtc.c: 63: I2CStart();
	fcall	_I2CStart
	line	64
	
l6050:	
;rtc.c: 64: I2CSend(0b11010000);
	movlw	(0D0h)
	fcall	_I2CSend
	line	65
	
l6052:	
;rtc.c: 65: I2CSend(0x02);
	movlw	(02h)
	fcall	_I2CSend
	line	66
	
l6054:	
;rtc.c: 66: I2CRestart();
	fcall	_I2CRestart
	line	67
	
l6056:	
;rtc.c: 67: I2CSend(0b11010001);
	movlw	(0D1h)
	fcall	_I2CSend
	line	69
	
l6058:	
;rtc.c: 69: temp = I2CRead();
	fcall	_I2CRead
	bcf	status, 5	;RP0=0, select bank0
	movwf	(ReadHour@temp)
	line	70
	
l6060:	
;rtc.c: 70: I2CNak();
	fcall	_I2CNak
	line	71
	
l6062:	
;rtc.c: 71: I2CStop();
	fcall	_I2CStop
	line	74
	
l6064:	
;rtc.c: 74: return BCDconv(temp);
	bcf	status, 5	;RP0=0, select bank0
	movf	(ReadHour@temp),w
	fcall	_BCDconv
	line	75
	
l1859:	
	return
	opt stack 0
GLOBAL	__end_of_ReadHour
	__end_of_ReadHour:
;; =============== function _ReadHour ends ============

	signat	_ReadHour,89
	global	_I2CRead
psect	text1232,local,class=CODE,delta=2
global __ptext1232
__ptext1232:

;; *************** function _I2CRead *****************
;; Defined at:
;;		line 55 in file "X:\Hi-tech\USART_PIC16f877a\i2c.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  temp            1    2[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/20
;;		On exit  : 60/20
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         1       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_I2CWait
;; This function is called by:
;;		_ReadMin
;;		_ReadHour
;;		_ReadSeconds
;; This function uses a non-reentrant model
;;
psect	text1232
	file	"X:\Hi-tech\USART_PIC16f877a\i2c.c"
	line	55
	global	__size_of_I2CRead
	__size_of_I2CRead	equ	__end_of_I2CRead-_I2CRead
	
_I2CRead:	
	opt	stack 6
; Regs used in _I2CRead: [wreg+status,2+status,0+pclath+cstack]
	line	58
	
l6036:	
;i2c.c: 56: unsigned char temp;
;i2c.c: 58: RCEN = 1;
	bsf	(1163/8)^080h,(1163)&7
	line	59
;i2c.c: 59: while(!BF);
	
l2490:	
	btfss	(1184/8)^080h,(1184)&7
	goto	u1941
	goto	u1940
u1941:
	goto	l2490
u1940:
	line	60
	
l6038:	
;i2c.c: 60: temp = SSPBUF;
	bcf	status, 5	;RP0=0, select bank0
	movf	(19),w	;volatile
	movwf	(I2CRead@temp)
	line	61
	
l6040:	
;i2c.c: 61: I2CWait();
	fcall	_I2CWait
	line	62
	
l6042:	
;i2c.c: 62: return temp;
	movf	(I2CRead@temp),w
	line	63
	
l2493:	
	return
	opt stack 0
GLOBAL	__end_of_I2CRead
	__end_of_I2CRead:
;; =============== function _I2CRead ends ============

	signat	_I2CRead,89
	global	_I2CSend
psect	text1233,local,class=CODE,delta=2
global __ptext1233
__ptext1233:

;; *************** function _I2CSend *****************
;; Defined at:
;;		line 49 in file "X:\Hi-tech\USART_PIC16f877a\i2c.c"
;; Parameters:    Size  Location     Type
;;  dat             1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  dat             1    2[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 40/20
;;		On exit  : 60/20
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         1       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_I2CWait
;; This function is called by:
;;		_SetMin
;;		_ReadMin
;;		_SetHour
;;		_ReadHour
;;		_SetSeconds
;;		_ReadSeconds
;; This function uses a non-reentrant model
;;
psect	text1233
	file	"X:\Hi-tech\USART_PIC16f877a\i2c.c"
	line	49
	global	__size_of_I2CSend
	__size_of_I2CSend	equ	__end_of_I2CSend-_I2CSend
	
_I2CSend:	
	opt	stack 6
; Regs used in _I2CSend: [wreg+status,2+status,0+pclath+cstack]
;I2CSend@dat stored from wreg
	movwf	(I2CSend@dat)
	line	50
	
l6032:	
;i2c.c: 50: SSPBUF = dat;
	movf	(I2CSend@dat),w
	bcf	status, 5	;RP0=0, select bank0
	movwf	(19)	;volatile
	line	51
;i2c.c: 51: while(BF);
	
l2484:	
	bsf	status, 5	;RP0=1, select bank1
	btfsc	(1184/8)^080h,(1184)&7
	goto	u1931
	goto	u1930
u1931:
	goto	l2484
u1930:
	line	52
	
l6034:	
;i2c.c: 52: I2CWait();
	fcall	_I2CWait
	line	53
	
l2487:	
	return
	opt stack 0
GLOBAL	__end_of_I2CSend
	__end_of_I2CSend:
;; =============== function _I2CSend ends ============

	signat	_I2CSend,4216
	global	_DCBconv
psect	text1234,local,class=CODE,delta=2
global __ptext1234
__ptext1234:

;; *************** function _DCBconv *****************
;; Defined at:
;;		line 20 in file "X:\Hi-tech\USART_PIC16f877a\rtc.c"
;; Parameters:    Size  Location     Type
;;  source          1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  source          1    1[BANK0 ] unsigned char 
;;  temp_maj        1    2[BANK0 ] unsigned char 
;;  temp_min        1    0[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       3       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         1       3       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		___lbdiv
;;		___bmul
;; This function is called by:
;;		_SetMin
;;		_SetHour
;;		_SetSeconds
;; This function uses a non-reentrant model
;;
psect	text1234
	file	"X:\Hi-tech\USART_PIC16f877a\rtc.c"
	line	20
	global	__size_of_DCBconv
	__size_of_DCBconv	equ	__end_of_DCBconv-_DCBconv
	
_DCBconv:	
	opt	stack 5
; Regs used in _DCBconv: [wreg+status,2+status,0+pclath+cstack]
;DCBconv@source stored from wreg
	line	22
	movwf	(DCBconv@source)
	line	21
	
l6020:	
	line	22
;rtc.c: 22: unsigned char temp_maj=0;
	clrf	(DCBconv@temp_maj)
	line	23
	
l6022:	
;rtc.c: 23: temp_maj = source/10 ;
	movlw	(0Ah)
	movwf	(?___lbdiv)
	movf	(DCBconv@source),w
	fcall	___lbdiv
	movwf	(DCBconv@temp_maj)
	line	24
	
l6024:	
;rtc.c: 24: temp_min = source - temp_maj*10;
	movlw	(0F6h)
	movwf	(?___bmul)
	movf	(DCBconv@temp_maj),w
	fcall	___bmul
	movwf	(??_DCBconv+0)+0
	movf	(DCBconv@source),w
	addwf	0+(??_DCBconv+0)+0,w
	movwf	(DCBconv@temp_min)
	line	25
	
l6026:	
;rtc.c: 25: temp_maj <<= 4;
swapf	(DCBconv@temp_maj),f
	movlw	240
	andwf	(DCBconv@temp_maj),f

	line	27
	
l6028:	
;rtc.c: 27: return temp_maj+temp_min;
	movf	(DCBconv@temp_min),w
	addwf	(DCBconv@temp_maj),w
	line	29
	
l1847:	
	return
	opt stack 0
GLOBAL	__end_of_DCBconv
	__end_of_DCBconv:
;; =============== function _DCBconv ends ============

	signat	_DCBconv,4217
	global	_lcd_write
psect	text1235,local,class=CODE,delta=2
global __ptext1235
__ptext1235:

;; *************** function _lcd_write *****************
;; Defined at:
;;		line 6 in file "X:\Hi-tech\USART_PIC16f877a\lcd.c"
;; Parameters:    Size  Location     Type
;;  c               1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  c               1    3[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 60/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_display_tt
;;		_menu
;;		_lcd_clear
;;		_lcd_puts
;;		_lcd_putch
;;		_lcd_goto
;;		_lcd_init
;; This function uses a non-reentrant model
;;
psect	text1235
	file	"X:\Hi-tech\USART_PIC16f877a\lcd.c"
	line	6
	global	__size_of_lcd_write
	__size_of_lcd_write	equ	__end_of_lcd_write-_lcd_write
	
_lcd_write:	
	opt	stack 5
; Regs used in _lcd_write: [wreg+status,2+status,0]
;lcd_write@c stored from wreg
	movwf	(lcd_write@c)
	line	7
	
l5162:	
;lcd.c: 7: _delay((unsigned long)((40)*(4000000/4000000.0)));
	opt asmopt_off
movlw	13
movwf	(??_lcd_write+0)+0,f
u2357:
decfsz	(??_lcd_write+0)+0,f
	goto	u2357
opt asmopt_on

	line	9
	
l5164:	
;lcd.c: 9: if (((c>>4)&0x01)) RD4 = 1; else RD4 = 0;
	movf	(lcd_write@c),w
	movwf	(??_lcd_write+0)+0
	movlw	04h
u1315:
	clrc
	rrf	(??_lcd_write+0)+0,f
	addlw	-1
	skipz
	goto	u1315
	btfss	0+(??_lcd_write+0)+0,(0)&7
	goto	u1321
	goto	u1320
u1321:
	goto	l3054
u1320:
	
l5166:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bsf	(68/8),(68)&7
	goto	l5168
	
l3054:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(68/8),(68)&7
	line	10
	
l5168:	
;lcd.c: 10: if (((c>>5)&0x01)) RD5 = 1; else RD5 = 0;
	movf	(lcd_write@c),w
	movwf	(??_lcd_write+0)+0
	movlw	05h
u1335:
	clrc
	rrf	(??_lcd_write+0)+0,f
	addlw	-1
	skipz
	goto	u1335
	btfss	0+(??_lcd_write+0)+0,(0)&7
	goto	u1341
	goto	u1340
u1341:
	goto	l3056
u1340:
	
l5170:	
	bsf	(69/8),(69)&7
	goto	l5172
	
l3056:	
	bcf	(69/8),(69)&7
	line	11
	
l5172:	
;lcd.c: 11: if (((c>>6)&0x01)) RD6 = 1; else RD6 = 0;
	movf	(lcd_write@c),w
	movwf	(??_lcd_write+0)+0
	movlw	06h
u1355:
	clrc
	rrf	(??_lcd_write+0)+0,f
	addlw	-1
	skipz
	goto	u1355
	btfss	0+(??_lcd_write+0)+0,(0)&7
	goto	u1361
	goto	u1360
u1361:
	goto	l3058
u1360:
	
l5174:	
	bsf	(70/8),(70)&7
	goto	l5176
	
l3058:	
	bcf	(70/8),(70)&7
	line	12
	
l5176:	
;lcd.c: 12: if (((c>>7)&0x01)) RD7 = 1; else RD7 = 0;
	movf	(lcd_write@c),w
	movwf	(??_lcd_write+0)+0
	movlw	07h
u1375:
	clrc
	rrf	(??_lcd_write+0)+0,f
	addlw	-1
	skipz
	goto	u1375
	btfss	0+(??_lcd_write+0)+0,(0)&7
	goto	u1381
	goto	u1380
u1381:
	goto	l3060
u1380:
	
l5178:	
	bsf	(71/8),(71)&7
	goto	l3061
	
l3060:	
	bcf	(71/8),(71)&7
	
l3061:	
	line	13
;lcd.c: 13: ((RD3 = 1),(RD3=0));
	bsf	(67/8),(67)&7
	bcf	(67/8),(67)&7
	line	14
;lcd.c: 14: if (((c>>0)&0x01)) RD4 = 1; else RD4 = 0;
	btfss	(lcd_write@c),(0)&7
	goto	u1391
	goto	u1390
u1391:
	goto	l3062
u1390:
	
l5180:	
	bsf	(68/8),(68)&7
	goto	l5182
	
l3062:	
	bcf	(68/8),(68)&7
	line	15
	
l5182:	
;lcd.c: 15: if (((c>>1)&0x01)) RD5 = 1; else RD5 = 0;
	movf	(lcd_write@c),w
	movwf	(??_lcd_write+0)+0
	clrc
	rrf	(??_lcd_write+0)+0,f
	btfss	0+(??_lcd_write+0)+0,(0)&7
	goto	u1401
	goto	u1400
u1401:
	goto	l3064
u1400:
	
l5184:	
	bsf	(69/8),(69)&7
	goto	l5186
	
l3064:	
	bcf	(69/8),(69)&7
	line	16
	
l5186:	
;lcd.c: 16: if (((c>>2)&0x01)) RD6 = 1; else RD6 = 0;
	movf	(lcd_write@c),w
	movwf	(??_lcd_write+0)+0
	clrc
	rrf	(??_lcd_write+0)+0,f
	clrc
	rrf	(??_lcd_write+0)+0,f
	btfss	0+(??_lcd_write+0)+0,(0)&7
	goto	u1411
	goto	u1410
u1411:
	goto	l3066
u1410:
	
l5188:	
	bsf	(70/8),(70)&7
	goto	l5190
	
l3066:	
	bcf	(70/8),(70)&7
	line	17
	
l5190:	
;lcd.c: 17: if (((c>>3)&0x01)) RD7 = 1; else RD7 = 0;
	movf	(lcd_write@c),w
	movwf	(??_lcd_write+0)+0
	clrc
	rrf	(??_lcd_write+0)+0,f
	clrc
	rrf	(??_lcd_write+0)+0,f
	clrc
	rrf	(??_lcd_write+0)+0,f
	btfss	0+(??_lcd_write+0)+0,(0)&7
	goto	u1421
	goto	u1420
u1421:
	goto	l3068
u1420:
	
l5192:	
	bsf	(71/8),(71)&7
	goto	l3069
	
l3068:	
	bcf	(71/8),(71)&7
	
l3069:	
	line	18
;lcd.c: 18: ((RD3 = 1),(RD3=0));
	bsf	(67/8),(67)&7
	bcf	(67/8),(67)&7
	line	25
	
l3070:	
	return
	opt stack 0
GLOBAL	__end_of_lcd_write
	__end_of_lcd_write:
;; =============== function _lcd_write ends ============

	signat	_lcd_write,4216
	global	_RX
psect	text1236,local,class=CODE,delta=2
global __ptext1236
__ptext1236:

;; *************** function _RX *****************
;; Defined at:
;;		line 189 in file "X:\Hi-tech\USART_PIC16f877a\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1    4[COMMON] unsigned char 
;;  d               1    3[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         2       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         3       0       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_get_temp
;; This function uses a non-reentrant model
;;
psect	text1236
	file	"X:\Hi-tech\USART_PIC16f877a\main.c"
	line	189
	global	__size_of_RX
	__size_of_RX	equ	__end_of_RX-_RX
	
_RX:	
	opt	stack 5
; Regs used in _RX: [wreg+status,2+status,0]
	line	190
	
l5144:	
;main.c: 190: unsigned char d = 0;
	clrf	(RX@d)
	line	191
;main.c: 191: unsigned char i = 0;
	clrf	(RX@i)
	line	192
;main.c: 192: for (i=0;i<8;i++){
	clrf	(RX@i)
	
l642:	
	line	193
;main.c: 193: TRISC0 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1080/8)^080h,(1080)&7
	line	194
;main.c: 194: _delay((unsigned long)((6)*(4000000/4000000.0)));
		opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on
	opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on
	opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on

	line	195
;main.c: 195: TRISC0 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1080/8)^080h,(1080)&7
	line	196
;main.c: 196: _delay((unsigned long)((4)*(4000000/4000000.0)));
		opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on
	opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on

	line	197
;main.c: 197: d>>=1;
	clrc
	rrf	(RX@d),f
	line	198
;main.c: 198: if (RC0 == 1) d |= 0x80;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(56/8),(56)&7
	goto	u1291
	goto	u1290
u1291:
	goto	l5152
u1290:
	
l5150:	
	bsf	(RX@d)+(7/8),(7)&7
	line	200
	
l5152:	
;main.c: 200: _delay((unsigned long)((60)*(4000000/4000000.0)));
	opt asmopt_off
movlw	19
movwf	(??_RX+0)+0,f
u2367:
decfsz	(??_RX+0)+0,f
	goto	u2367
	nop2	;nop
opt asmopt_on

	line	192
	
l5154:	
	incf	(RX@i),f
	
l5156:	
	movlw	(08h)
	subwf	(RX@i),w
	skipc
	goto	u1301
	goto	u1300
u1301:
	goto	l642
u1300:
	line	206
	
l5158:	
;main.c: 204: }
;main.c: 206: return d;
	movf	(RX@d),w
	line	208
	
l645:	
	return
	opt stack 0
GLOBAL	__end_of_RX
	__end_of_RX:
;; =============== function _RX ends ============

	signat	_RX,89
	global	_TX
psect	text1237,local,class=CODE,delta=2
global __ptext1237
__ptext1237:

;; *************** function _TX *****************
;; Defined at:
;;		line 167 in file "X:\Hi-tech\USART_PIC16f877a\main.c"
;; Parameters:    Size  Location     Type
;;  cmd             1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  cmd             1    3[COMMON] unsigned char 
;;  i               1    5[COMMON] unsigned char 
;;  temp            1    4[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         3       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_get_temp
;; This function uses a non-reentrant model
;;
psect	text1237
	file	"X:\Hi-tech\USART_PIC16f877a\main.c"
	line	167
	global	__size_of_TX
	__size_of_TX	equ	__end_of_TX-_TX
	
_TX:	
	opt	stack 5
; Regs used in _TX: [wreg+status,2+status,0]
;TX@cmd stored from wreg
	line	169
	movwf	(TX@cmd)
	line	168
	
l5120:	
	line	169
;main.c: 169: unsigned char i = 0;
	clrf	(TX@i)
	line	170
	
l5122:	
;main.c: 170: temp = cmd;
	movf	(TX@cmd),w
	movwf	(TX@temp)
	line	171
	
l5124:	
;main.c: 171: for (i=0;i<8;i++) {
	clrf	(TX@i)
	
l635:	
	line	172
;main.c: 172: if (temp&0x01) {
	btfss	(TX@temp),(0)&7
	goto	u1271
	goto	u1270
u1271:
	goto	l637
u1270:
	line	173
	
l5128:	
;main.c: 173: TRISC0 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1080/8)^080h,(1080)&7
	line	174
;main.c: 174: _delay((unsigned long)((5)*(4000000/4000000.0)));
		opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on
	opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on
	opt asmopt_off
	clrwdt
	opt asmopt_on

	line	175
;main.c: 175: TRISC0 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1080/8)^080h,(1080)&7
	line	176
	
l5130:	
;main.c: 176: _delay((unsigned long)((70)*(4000000/4000000.0)));
	opt asmopt_off
movlw	23
movwf	(??_TX+0)+0,f
u2377:
decfsz	(??_TX+0)+0,f
	goto	u2377
opt asmopt_on

	line	177
;main.c: 177: } else {
	goto	l5138
	
l637:	
	line	178
;main.c: 178: TRISC0 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1080/8)^080h,(1080)&7
	line	179
	
l5132:	
;main.c: 179: _delay((unsigned long)((70)*(4000000/4000000.0)));
	opt asmopt_off
movlw	23
movwf	(??_TX+0)+0,f
u2387:
decfsz	(??_TX+0)+0,f
	goto	u2387
opt asmopt_on

	line	180
	
l5134:	
;main.c: 180: TRISC0 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1080/8)^080h,(1080)&7
	line	181
	
l5136:	
;main.c: 181: _delay((unsigned long)((5)*(4000000/4000000.0)));
		opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on
	opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on
	opt asmopt_off
	clrwdt
	opt asmopt_on

	line	184
	
l5138:	
;main.c: 183: }
;main.c: 184: temp >>= 1;
	clrc
	rrf	(TX@temp),f
	line	171
	
l5140:	
	incf	(TX@i),f
	
l5142:	
	movlw	(08h)
	subwf	(TX@i),w
	skipc
	goto	u1281
	goto	u1280
u1281:
	goto	l635
u1280:
	line	187
	
l639:	
	return
	opt stack 0
GLOBAL	__end_of_TX
	__end_of_TX:
;; =============== function _TX ends ============

	signat	_TX,4216
	global	_INIT
psect	text1238,local,class=CODE,delta=2
global __ptext1238
__ptext1238:

;; *************** function _INIT *****************
;; Defined at:
;;		line 153 in file "X:\Hi-tech\USART_PIC16f877a\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         1       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_get_temp
;; This function uses a non-reentrant model
;;
psect	text1238
	file	"X:\Hi-tech\USART_PIC16f877a\main.c"
	line	153
	global	__size_of_INIT
	__size_of_INIT	equ	__end_of_INIT-_INIT
	
_INIT:	
	opt	stack 5
; Regs used in _INIT: [wreg+status,2+status,0]
	line	155
	
l5104:	
;main.c: 154: static bit b;
;main.c: 155: TRISC0 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1080/8)^080h,(1080)&7
	line	156
;main.c: 156: TRISC0 = 0;
	bcf	(1080/8)^080h,(1080)&7
	line	157
	
l5106:	
;main.c: 157: _delay((unsigned long)((500)*(4000000/4000000.0)));
	opt asmopt_off
movlw	166
movwf	(??_INIT+0)+0,f
u2397:
decfsz	(??_INIT+0)+0,f
	goto	u2397
	clrwdt
opt asmopt_on

	line	158
	
l5108:	
;main.c: 158: TRISC0 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1080/8)^080h,(1080)&7
	line	159
;main.c: 159: _delay((unsigned long)((65)*(4000000/4000000.0)));
	opt asmopt_off
movlw	21
movwf	(??_INIT+0)+0,f
u2407:
decfsz	(??_INIT+0)+0,f
	goto	u2407
	clrwdt
opt asmopt_on

	line	160
	
l5110:	
;main.c: 160: b = RC0;
	bcf	(INIT@b/8),(INIT@b)&7
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(56/8),(56)&7
	goto	u1255
	bsf	(INIT@b/8),(INIT@b)&7
u1255:

	line	161
;main.c: 161: _delay((unsigned long)((450)*(4000000/4000000.0)));
	opt asmopt_off
movlw	149
movwf	(??_INIT+0)+0,f
u2417:
decfsz	(??_INIT+0)+0,f
	goto	u2417
	nop2	;nop
opt asmopt_on

	line	163
	
l5112:	
;main.c: 163: return b;
	btfsc	(INIT@b/8),(INIT@b)&7
	goto	u1261
	goto	u1260
u1261:
	goto	l5116
u1260:
	
l5114:	
	clrc
	
	goto	l632
	
l5116:	
	setc
	
	line	165
	
l632:	
	return
	opt stack 0
GLOBAL	__end_of_INIT
	__end_of_INIT:
;; =============== function _INIT ends ============

	signat	_INIT,88
	global	_printf
psect	text1239,local,class=CODE,delta=2
global __ptext1239
__ptext1239:

;; *************** function _printf *****************
;; Defined at:
;;		line 461 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\lib\doprnt.c"
;; Parameters:    Size  Location     Type
;;  f               2    4[BANK0 ] PTR const unsigned char 
;;		 -> STR_61(3), STR_60(4), STR_59(18), STR_58(3), 
;;		 -> STR_57(4), STR_56(18), STR_55(3), STR_54(4), 
;;		 -> STR_53(16), STR_52(18), STR_51(19), STR_50(8), 
;;		 -> STR_49(24), STR_48(5), STR_47(6), STR_46(13), 
;;		 -> STR_15(3), STR_14(6), STR_13(7), STR_12(3), 
;;		 -> STR_11(6), STR_10(6), STR_9(7), STR_8(7), 
;;		 -> STR_7(4), STR_6(8), STR_5(9), STR_4(8), 
;;		 -> STR_3(9), STR_2(8), STR_1(9), 
;; Auto vars:     Size  Location     Type
;;  _val            4   13[BANK0 ] struct .
;;  c               1   17[BANK0 ] char 
;;  ap              1   12[BANK0 ] PTR void [1]
;;		 -> ?_printf(2), 
;;  prec            1   11[BANK0 ] char 
;;  flag            1   10[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    4[BANK0 ] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFE9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       4       0       0       0
;;      Locals:         0       8       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0      14       0       0       0
;;Total ram usage:       14 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_putch
;;		___lwdiv
;;		___lwmod
;; This function is called by:
;;		_main
;;		_get_temp
;;		_printTemp
;;		_ShowTime
;;		_SetupTime
;; This function uses a non-reentrant model
;;
psect	text1239
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\doprnt.c"
	line	461
	global	__size_of_printf
	__size_of_printf	equ	__end_of_printf-_printf
	
_printf:	
	opt	stack 6
; Regs used in _printf: [wreg-fsr0h+status,2-btemp+1+pclath+cstack]
	line	537
	
l5966:	
	movlw	(?_printf+02h)&0ffh
	movwf	(printf@ap)
	line	540
	goto	l6018
	line	542
	
l5968:	
	movf	(printf@c),w
	xorlw	025h
	skipnz
	goto	u1861
	goto	u1860
u1861:
	goto	l5972
u1860:
	line	545
	
l5970:	
	movf	(printf@c),w
	fcall	_putch
	line	546
	goto	l6018
	line	552
	
l5972:	
	clrf	(printf@flag)
	line	638
	goto	l5982
	line	802
	
l5974:	
	movf	(printf@ap),w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(printf@c)
	
l5976:	
	incf	(printf@ap),f
	incf	(printf@ap),f
	line	812
	
l5978:	
	movf	(printf@c),w
	fcall	_putch
	line	813
	goto	l6018
	line	638
	
l5982:	
	movf	(printf@f+1),w
	movwf	(??_printf+0)+0+1
	movf	(printf@f),w
	movwf	(??_printf+0)+0
	incf	(printf@f),f
	skipnz
	incf	(printf@f+1),f
	movf	1+(??_printf+0)+0,w
	movwf	btemp+1
	movf	0+(??_printf+0)+0,w
	movwf	fsr0
	fcall	stringtab
	movwf	(printf@c)
	; Switch size 1, requested type "space"
; Number of cases is 4, Range of values is 0 to 105
; switch strategies available:
; Name         Bytes Cycles
; simple_byte    13     7 (average)
;	Chosen strategy is simple_byte

	xorlw	0^0	; case 0
	skipnz
	goto	l3119
	xorlw	99^0	; case 99
	skipnz
	goto	l5974
	xorlw	100^99	; case 100
	skipnz
	goto	l5984
	xorlw	105^100	; case 105
	skipnz
	goto	l5984
	goto	l5978

	line	1254
	
l5984:	
	movf	(printf@ap),w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(printf@_val)
	incf	fsr0,f
	movf	indf,w
	movwf	(printf@_val+1)
	
l5986:	
	incf	(printf@ap),f
	incf	(printf@ap),f
	line	1256
	
l5988:	
	btfss	(printf@_val+1),7
	goto	u1871
	goto	u1870
u1871:
	goto	l5994
u1870:
	line	1257
	
l5990:	
	movlw	(03h)
	iorwf	(printf@flag),f
	line	1258
	
l5992:	
	comf	(printf@_val),f
	comf	(printf@_val+1),f
	incf	(printf@_val),f
	skipnz
	incf	(printf@_val+1),f
	line	1300
	
l5994:	
	clrf	(printf@c)
	incf	(printf@c),f
	line	1301
	
l5998:	
	clrc
	rlf	(printf@c),w
	addlw	low(_dpowers|8000h)
	movwf	fsr0
	movlw	high(_dpowers|8000h)
	skipnc
	addlw	1
	movwf	btemp+1
	fcall	stringtab
	movwf	(??_printf+0)+0
	fcall	stringtab
	movwf	(??_printf+0)+0+1
	movf	1+(??_printf+0)+0,w
	subwf	(printf@_val+1),w
	skipz
	goto	u1885
	movf	0+(??_printf+0)+0,w
	subwf	(printf@_val),w
u1885:
	skipnc
	goto	u1881
	goto	u1880
u1881:
	goto	l6002
u1880:
	goto	l6006
	line	1300
	
l6002:	
	incf	(printf@c),f
	
l6004:	
	movf	(printf@c),w
	xorlw	05h
	skipz
	goto	u1891
	goto	u1890
u1891:
	goto	l5998
u1890:
	line	1433
	
l6006:	
	movf	(printf@flag),w
	andlw	03h
	btfsc	status,2
	goto	u1901
	goto	u1900
u1901:
	goto	l6010
u1900:
	line	1434
	
l6008:	
	movlw	(02Dh)
	fcall	_putch
	line	1467
	
l6010:	
	movf	(printf@c),w
	movwf	(printf@prec)
	line	1469
	goto	l6016
	line	1484
	
l6012:	
	movlw	0Ah
	movwf	(?___lwmod)
	clrf	(?___lwmod+1)
	clrc
	rlf	(printf@prec),w
	addlw	low(_dpowers|8000h)
	movwf	fsr0
	movlw	high(_dpowers|8000h)
	skipnc
	addlw	1
	movwf	btemp+1
	fcall	stringtab
	movwf	(?___lwdiv)
	fcall	stringtab
	movwf	(?___lwdiv+1)
	movf	(printf@_val+1),w
	movwf	1+(?___lwdiv)+02h
	movf	(printf@_val),w
	movwf	0+(?___lwdiv)+02h
	fcall	___lwdiv
	movf	(1+(?___lwdiv)),w
	movwf	1+(?___lwmod)+02h
	movf	(0+(?___lwdiv)),w
	movwf	0+(?___lwmod)+02h
	fcall	___lwmod
	movf	(0+(?___lwmod)),w
	addlw	030h
	movwf	(printf@c)
	line	1516
	
l6014:	
	movf	(printf@c),w
	fcall	_putch
	line	1469
	
l6016:	
	decf	(printf@prec),f
	incf	((printf@prec)),w
	skipz
	goto	u1911
	goto	u1910
u1911:
	goto	l6012
u1910:
	line	540
	
l6018:	
	movf	(printf@f+1),w
	movwf	(??_printf+0)+0+1
	movf	(printf@f),w
	movwf	(??_printf+0)+0
	incf	(printf@f),f
	skipnz
	incf	(printf@f+1),f
	movf	1+(??_printf+0)+0,w
	movwf	btemp+1
	movf	0+(??_printf+0)+0,w
	movwf	fsr0
	fcall	stringtab
	movwf	(printf@c)
	movf	((printf@c)),f
	skipz
	goto	u1921
	goto	u1920
u1921:
	goto	l5968
u1920:
	line	1533
	
l3119:	
	return
	opt stack 0
GLOBAL	__end_of_printf
	__end_of_printf:
;; =============== function _printf ends ============

	signat	_printf,602
	global	___awdiv
psect	text1240,local,class=CODE,delta=2
global __ptext1240
__ptext1240:

;; *************** function ___awdiv *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\sources\awdiv.c"
;; Parameters:    Size  Location     Type
;;  divisor         2    0[BANK0 ] int 
;;  dividend        2    2[BANK0 ] int 
;; Auto vars:     Size  Location     Type
;;  quotient        2    6[BANK0 ] int 
;;  sign            1    5[BANK0 ] unsigned char 
;;  counter         1    4[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       4       0       0       0
;;      Locals:         0       4       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       8       0       0       0
;;Total ram usage:        8 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_get_temp
;; This function uses a non-reentrant model
;;
psect	text1240
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\awdiv.c"
	line	5
	global	__size_of___awdiv
	__size_of___awdiv	equ	__end_of___awdiv-___awdiv
	
___awdiv:	
	opt	stack 5
; Regs used in ___awdiv: [wreg+status,2+status,0]
	line	9
	
l5922:	
	clrf	(___awdiv@sign)
	line	10
	
l5924:	
	btfss	(___awdiv@divisor+1),7
	goto	u1791
	goto	u1790
u1791:
	goto	l5930
u1790:
	line	11
	
l5926:	
	comf	(___awdiv@divisor),f
	comf	(___awdiv@divisor+1),f
	incf	(___awdiv@divisor),f
	skipnz
	incf	(___awdiv@divisor+1),f
	line	12
	
l5928:	
	clrf	(___awdiv@sign)
	incf	(___awdiv@sign),f
	line	14
	
l5930:	
	btfss	(___awdiv@dividend+1),7
	goto	u1801
	goto	u1800
u1801:
	goto	l5936
u1800:
	line	15
	
l5932:	
	comf	(___awdiv@dividend),f
	comf	(___awdiv@dividend+1),f
	incf	(___awdiv@dividend),f
	skipnz
	incf	(___awdiv@dividend+1),f
	line	16
	
l5934:	
	movlw	(01h)
	xorwf	(___awdiv@sign),f
	line	18
	
l5936:	
	clrf	(___awdiv@quotient)
	clrf	(___awdiv@quotient+1)
	line	19
	
l5938:	
	movf	(___awdiv@divisor+1),w
	iorwf	(___awdiv@divisor),w
	skipnz
	goto	u1811
	goto	u1810
u1811:
	goto	l5958
u1810:
	line	20
	
l5940:	
	clrf	(___awdiv@counter)
	incf	(___awdiv@counter),f
	line	21
	goto	l5944
	line	22
	
l5942:	
	clrc
	rlf	(___awdiv@divisor),f
	rlf	(___awdiv@divisor+1),f
	line	23
	incf	(___awdiv@counter),f
	line	21
	
l5944:	
	btfss	(___awdiv@divisor+1),(15)&7
	goto	u1821
	goto	u1820
u1821:
	goto	l5942
u1820:
	line	26
	
l5946:	
	clrc
	rlf	(___awdiv@quotient),f
	rlf	(___awdiv@quotient+1),f
	line	27
	
l5948:	
	movf	(___awdiv@divisor+1),w
	subwf	(___awdiv@dividend+1),w
	skipz
	goto	u1835
	movf	(___awdiv@divisor),w
	subwf	(___awdiv@dividend),w
u1835:
	skipc
	goto	u1831
	goto	u1830
u1831:
	goto	l5954
u1830:
	line	28
	
l5950:	
	movf	(___awdiv@divisor),w
	subwf	(___awdiv@dividend),f
	movf	(___awdiv@divisor+1),w
	skipc
	decf	(___awdiv@dividend+1),f
	subwf	(___awdiv@dividend+1),f
	line	29
	
l5952:	
	bsf	(___awdiv@quotient)+(0/8),(0)&7
	line	31
	
l5954:	
	clrc
	rrf	(___awdiv@divisor+1),f
	rrf	(___awdiv@divisor),f
	line	32
	
l5956:	
	decfsz	(___awdiv@counter),f
	goto	u1841
	goto	u1840
u1841:
	goto	l5946
u1840:
	line	34
	
l5958:	
	movf	(___awdiv@sign),w
	skipz
	goto	u1850
	goto	l5962
u1850:
	line	35
	
l5960:	
	comf	(___awdiv@quotient),f
	comf	(___awdiv@quotient+1),f
	incf	(___awdiv@quotient),f
	skipnz
	incf	(___awdiv@quotient+1),f
	line	36
	
l5962:	
	movf	(___awdiv@quotient+1),w
	movwf	(?___awdiv+1)
	movf	(___awdiv@quotient),w
	movwf	(?___awdiv)
	line	37
	
l3278:	
	return
	opt stack 0
GLOBAL	__end_of___awdiv
	__end_of___awdiv:
;; =============== function ___awdiv ends ============

	signat	___awdiv,8314
	global	___lbdiv
psect	text1241,local,class=CODE,delta=2
global __ptext1241
__ptext1241:

;; *************** function ___lbdiv *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lbdiv.c"
;; Parameters:    Size  Location     Type
;;  dividend        1    wreg     unsigned char 
;;  divisor         1    2[COMMON] unsigned char 
;; Auto vars:     Size  Location     Type
;;  dividend        1    3[COMMON] unsigned char 
;;  quotient        1    5[COMMON] unsigned char 
;;  counter         1    4[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         1       0       0       0       0
;;      Locals:         3       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_display_tt
;;		_DCBconv
;; This function uses a non-reentrant model
;;
psect	text1241
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lbdiv.c"
	line	5
	global	__size_of___lbdiv
	__size_of___lbdiv	equ	__end_of___lbdiv-___lbdiv
	
___lbdiv:	
	opt	stack 4
; Regs used in ___lbdiv: [wreg+status,2+status,0]
;___lbdiv@dividend stored from wreg
	line	9
	movwf	(___lbdiv@dividend)
	
l5898:	
	clrf	(___lbdiv@quotient)
	line	10
	
l5900:	
	movf	(___lbdiv@divisor),w
	skipz
	goto	u1750
	goto	l5918
u1750:
	line	11
	
l5902:	
	clrf	(___lbdiv@counter)
	incf	(___lbdiv@counter),f
	line	12
	goto	l5906
	
l3156:	
	line	13
	clrc
	rlf	(___lbdiv@divisor),f
	line	14
	
l5904:	
	incf	(___lbdiv@counter),f
	line	12
	
l5906:	
	btfss	(___lbdiv@divisor),(7)&7
	goto	u1761
	goto	u1760
u1761:
	goto	l3156
u1760:
	line	16
	
l3158:	
	line	17
	clrc
	rlf	(___lbdiv@quotient),f
	line	18
	
l5908:	
	movf	(___lbdiv@divisor),w
	subwf	(___lbdiv@dividend),w
	skipc
	goto	u1771
	goto	u1770
u1771:
	goto	l5914
u1770:
	line	19
	
l5910:	
	movf	(___lbdiv@divisor),w
	subwf	(___lbdiv@dividend),f
	line	20
	
l5912:	
	bsf	(___lbdiv@quotient)+(0/8),(0)&7
	line	22
	
l5914:	
	clrc
	rrf	(___lbdiv@divisor),f
	line	23
	
l5916:	
	decfsz	(___lbdiv@counter),f
	goto	u1781
	goto	u1780
u1781:
	goto	l3158
u1780:
	line	25
	
l5918:	
	movf	(___lbdiv@quotient),w
	line	26
	
l3161:	
	return
	opt stack 0
GLOBAL	__end_of___lbdiv
	__end_of___lbdiv:
;; =============== function ___lbdiv ends ============

	signat	___lbdiv,8313
	global	___lwmod
psect	text1242,local,class=CODE,delta=2
global __ptext1242
__ptext1242:

;; *************** function ___lwmod *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lwmod.c"
;; Parameters:    Size  Location     Type
;;  divisor         2    0[BANK0 ] unsigned int 
;;  dividend        2    2[BANK0 ] unsigned int 
;; Auto vars:     Size  Location     Type
;;  counter         1    9[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       4       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         1       4       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_printf
;; This function uses a non-reentrant model
;;
psect	text1242
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lwmod.c"
	line	5
	global	__size_of___lwmod
	__size_of___lwmod	equ	__end_of___lwmod-___lwmod
	
___lwmod:	
	opt	stack 5
; Regs used in ___lwmod: [wreg+status,2+status,0]
	line	8
	
l4962:	
	movf	(___lwmod@divisor+1),w
	iorwf	(___lwmod@divisor),w
	skipnz
	goto	u1031
	goto	u1030
u1031:
	goto	l4978
u1030:
	line	9
	
l4964:	
	clrf	(___lwmod@counter)
	incf	(___lwmod@counter),f
	line	10
	goto	l4968
	line	11
	
l4966:	
	clrc
	rlf	(___lwmod@divisor),f
	rlf	(___lwmod@divisor+1),f
	line	12
	incf	(___lwmod@counter),f
	line	10
	
l4968:	
	btfss	(___lwmod@divisor+1),(15)&7
	goto	u1041
	goto	u1040
u1041:
	goto	l4966
u1040:
	line	15
	
l4970:	
	movf	(___lwmod@divisor+1),w
	subwf	(___lwmod@dividend+1),w
	skipz
	goto	u1055
	movf	(___lwmod@divisor),w
	subwf	(___lwmod@dividend),w
u1055:
	skipc
	goto	u1051
	goto	u1050
u1051:
	goto	l4974
u1050:
	line	16
	
l4972:	
	movf	(___lwmod@divisor),w
	subwf	(___lwmod@dividend),f
	movf	(___lwmod@divisor+1),w
	skipc
	decf	(___lwmod@dividend+1),f
	subwf	(___lwmod@dividend+1),f
	line	17
	
l4974:	
	clrc
	rrf	(___lwmod@divisor+1),f
	rrf	(___lwmod@divisor),f
	line	18
	
l4976:	
	decfsz	(___lwmod@counter),f
	goto	u1061
	goto	u1060
u1061:
	goto	l4970
u1060:
	line	20
	
l4978:	
	movf	(___lwmod@dividend+1),w
	movwf	(?___lwmod+1)
	movf	(___lwmod@dividend),w
	movwf	(?___lwmod)
	line	21
	
l3151:	
	return
	opt stack 0
GLOBAL	__end_of___lwmod
	__end_of___lwmod:
;; =============== function ___lwmod ends ============

	signat	___lwmod,8314
	global	___lwdiv
psect	text1243,local,class=CODE,delta=2
global __ptext1243
__ptext1243:

;; *************** function ___lwdiv *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lwdiv.c"
;; Parameters:    Size  Location     Type
;;  divisor         2    2[COMMON] unsigned int 
;;  dividend        2    4[COMMON] unsigned int 
;; Auto vars:     Size  Location     Type
;;  quotient        2    6[COMMON] unsigned int 
;;  counter         1    8[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    2[COMMON] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         4       0       0       0       0
;;      Locals:         3       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         7       0       0       0       0
;;Total ram usage:        7 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_printf
;; This function uses a non-reentrant model
;;
psect	text1243
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lwdiv.c"
	line	5
	global	__size_of___lwdiv
	__size_of___lwdiv	equ	__end_of___lwdiv-___lwdiv
	
___lwdiv:	
	opt	stack 5
; Regs used in ___lwdiv: [wreg+status,2+status,0]
	line	9
	
l4936:	
	clrf	(___lwdiv@quotient)
	clrf	(___lwdiv@quotient+1)
	line	10
	
l4938:	
	movf	(___lwdiv@divisor+1),w
	iorwf	(___lwdiv@divisor),w
	skipnz
	goto	u991
	goto	u990
u991:
	goto	l4958
u990:
	line	11
	
l4940:	
	clrf	(___lwdiv@counter)
	incf	(___lwdiv@counter),f
	line	12
	goto	l4944
	line	13
	
l4942:	
	clrc
	rlf	(___lwdiv@divisor),f
	rlf	(___lwdiv@divisor+1),f
	line	14
	incf	(___lwdiv@counter),f
	line	12
	
l4944:	
	btfss	(___lwdiv@divisor+1),(15)&7
	goto	u1001
	goto	u1000
u1001:
	goto	l4942
u1000:
	line	17
	
l4946:	
	clrc
	rlf	(___lwdiv@quotient),f
	rlf	(___lwdiv@quotient+1),f
	line	18
	
l4948:	
	movf	(___lwdiv@divisor+1),w
	subwf	(___lwdiv@dividend+1),w
	skipz
	goto	u1015
	movf	(___lwdiv@divisor),w
	subwf	(___lwdiv@dividend),w
u1015:
	skipc
	goto	u1011
	goto	u1010
u1011:
	goto	l4954
u1010:
	line	19
	
l4950:	
	movf	(___lwdiv@divisor),w
	subwf	(___lwdiv@dividend),f
	movf	(___lwdiv@divisor+1),w
	skipc
	decf	(___lwdiv@dividend+1),f
	subwf	(___lwdiv@dividend+1),f
	line	20
	
l4952:	
	bsf	(___lwdiv@quotient)+(0/8),(0)&7
	line	22
	
l4954:	
	clrc
	rrf	(___lwdiv@divisor+1),f
	rrf	(___lwdiv@divisor),f
	line	23
	
l4956:	
	decfsz	(___lwdiv@counter),f
	goto	u1021
	goto	u1020
u1021:
	goto	l4946
u1020:
	line	25
	
l4958:	
	movf	(___lwdiv@quotient+1),w
	movwf	(?___lwdiv+1)
	movf	(___lwdiv@quotient),w
	movwf	(?___lwdiv)
	line	26
	
l3141:	
	return
	opt stack 0
GLOBAL	__end_of___lwdiv
	__end_of___lwdiv:
;; =============== function ___lwdiv ends ============

	signat	___lwdiv,8314
	global	___wmul
psect	text1244,local,class=CODE,delta=2
global __ptext1244
__ptext1244:

;; *************** function ___wmul *****************
;; Defined at:
;;		line 3 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\sources\wmul.c"
;; Parameters:    Size  Location     Type
;;  multiplier      2    2[COMMON] unsigned int 
;;  multiplicand    2    4[COMMON] unsigned int 
;; Auto vars:     Size  Location     Type
;;  product         2    6[COMMON] unsigned int 
;; Return value:  Size  Location     Type
;;                  2    2[COMMON] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         4       0       0       0       0
;;      Locals:         2       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         6       0       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_get_temp
;; This function uses a non-reentrant model
;;
psect	text1244
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\wmul.c"
	line	3
	global	__size_of___wmul
	__size_of___wmul	equ	__end_of___wmul-___wmul
	
___wmul:	
	opt	stack 5
; Regs used in ___wmul: [wreg+status,2+status,0]
	line	4
	
l4920:	
	clrf	(___wmul@product)
	clrf	(___wmul@product+1)
	line	7
	
l4922:	
	btfss	(___wmul@multiplier),(0)&7
	goto	u971
	goto	u970
u971:
	goto	l4926
u970:
	line	8
	
l4924:	
	movf	(___wmul@multiplicand),w
	addwf	(___wmul@product),f
	skipnc
	incf	(___wmul@product+1),f
	movf	(___wmul@multiplicand+1),w
	addwf	(___wmul@product+1),f
	line	9
	
l4926:	
	clrc
	rlf	(___wmul@multiplicand),f
	rlf	(___wmul@multiplicand+1),f
	line	10
	
l4928:	
	clrc
	rrf	(___wmul@multiplier+1),f
	rrf	(___wmul@multiplier),f
	line	11
	
l4930:	
	movf	((___wmul@multiplier+1)),w
	iorwf	((___wmul@multiplier)),w
	skipz
	goto	u981
	goto	u980
u981:
	goto	l4922
u980:
	line	12
	
l4932:	
	movf	(___wmul@product+1),w
	movwf	(?___wmul+1)
	movf	(___wmul@product),w
	movwf	(?___wmul)
	line	13
	
l3131:	
	return
	opt stack 0
GLOBAL	__end_of___wmul
	__end_of___wmul:
;; =============== function ___wmul ends ============

	signat	___wmul,8314
	global	___bmul
psect	text1245,local,class=CODE,delta=2
global __ptext1245
__ptext1245:

;; *************** function ___bmul *****************
;; Defined at:
;;		line 3 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\sources\bmul.c"
;; Parameters:    Size  Location     Type
;;  multiplier      1    wreg     unsigned char 
;;  multiplicand    1    6[COMMON] unsigned char 
;; Auto vars:     Size  Location     Type
;;  multiplier      1    8[COMMON] unsigned char 
;;  product         1    7[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         1       0       0       0       0
;;      Locals:         2       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         3       0       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_display_tt
;;		_DCBconv
;;		_SetupTime
;; This function uses a non-reentrant model
;;
psect	text1245
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\bmul.c"
	line	3
	global	__size_of___bmul
	__size_of___bmul	equ	__end_of___bmul-___bmul
	
___bmul:	
	opt	stack 4
; Regs used in ___bmul: [wreg+status,2+status,0]
;___bmul@multiplier stored from wreg
	movwf	(___bmul@multiplier)
	line	4
	
l5882:	
	clrf	(___bmul@product)
	line	7
	
l5884:	
	btfss	(___bmul@multiplier),(0)&7
	goto	u1731
	goto	u1730
u1731:
	goto	l5888
u1730:
	line	8
	
l5886:	
	movf	(___bmul@multiplicand),w
	addwf	(___bmul@product),f
	line	9
	
l5888:	
	clrc
	rlf	(___bmul@multiplicand),f
	line	10
	
l5890:	
	clrc
	rrf	(___bmul@multiplier),f
	line	11
	
l5892:	
	movf	(___bmul@multiplier),f
	skipz
	goto	u1741
	goto	u1740
u1741:
	goto	l5884
u1740:
	line	12
	
l5894:	
	movf	(___bmul@product),w
	line	13
	
l3125:	
	return
	opt stack 0
GLOBAL	__end_of___bmul
	__end_of___bmul:
;; =============== function ___bmul ends ============

	signat	___bmul,8313
	global	_I2CWait
psect	text1246,local,class=CODE,delta=2
global __ptext1246
__ptext1246:

;; *************** function _I2CWait *****************
;; Defined at:
;;		line 44 in file "X:\Hi-tech\USART_PIC16f877a\i2c.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 40/0
;;		On exit  : 60/20
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_I2CSend
;;		_I2CRead
;; This function uses a non-reentrant model
;;
psect	text1246
	file	"X:\Hi-tech\USART_PIC16f877a\i2c.c"
	line	44
	global	__size_of_I2CWait
	__size_of_I2CWait	equ	__end_of_I2CWait-_I2CWait
	
_I2CWait:	
	opt	stack 5
; Regs used in _I2CWait: [wreg+status,2]
	line	45
	
l5876:	
;i2c.c: 45: while ( ( SSPCON2 & 0x1F ) || ( SSPSTAT & 0x04 ) );
	
l5878:	
	bsf	status, 5	;RP0=1, select bank1
	movf	(145)^080h,w
	andlw	01Fh
	btfss	status,2
	goto	u1711
	goto	u1710
u1711:
	goto	l5878
u1710:
	
l5880:	
	btfsc	(148)^080h,(2)&7
	goto	u1721
	goto	u1720
u1721:
	goto	l5878
u1720:
	line	47
	
l2481:	
	return
	opt stack 0
GLOBAL	__end_of_I2CWait
	__end_of_I2CWait:
;; =============== function _I2CWait ends ============

	signat	_I2CWait,88
	global	_I2CNak
psect	text1247,local,class=CODE,delta=2
global __ptext1247
__ptext1247:

;; *************** function _I2CNak *****************
;; Defined at:
;;		line 38 in file "X:\Hi-tech\USART_PIC16f877a\i2c.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		None
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/20
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_ReadMin
;;		_ReadHour
;;		_ReadSeconds
;; This function uses a non-reentrant model
;;
psect	text1247
	file	"X:\Hi-tech\USART_PIC16f877a\i2c.c"
	line	38
	global	__size_of_I2CNak
	__size_of_I2CNak	equ	__end_of_I2CNak-_I2CNak
	
_I2CNak:	
	opt	stack 6
; Regs used in _I2CNak: []
	line	39
	
l4896:	
;i2c.c: 39: ACKDT = 1;
	bsf	status, 5	;RP0=1, select bank1
	bsf	(1165/8)^080h,(1165)&7
	line	40
;i2c.c: 40: ACKEN = 1;
	bsf	(1164/8)^080h,(1164)&7
	line	41
;i2c.c: 41: while(ACKEN);
	
l2472:	
	btfsc	(1164/8)^080h,(1164)&7
	goto	u921
	goto	u920
u921:
	goto	l2472
u920:
	line	42
	
l2475:	
	return
	opt stack 0
GLOBAL	__end_of_I2CNak
	__end_of_I2CNak:
;; =============== function _I2CNak ends ============

	signat	_I2CNak,88
	global	_I2CRestart
psect	text1248,local,class=CODE,delta=2
global __ptext1248
__ptext1248:

;; *************** function _I2CRestart *****************
;; Defined at:
;;		line 26 in file "X:\Hi-tech\USART_PIC16f877a\i2c.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		None
;; Tracked objects:
;;		On entry : 60/20
;;		On exit  : 60/20
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_ReadMin
;;		_ReadHour
;;		_ReadSeconds
;; This function uses a non-reentrant model
;;
psect	text1248
	file	"X:\Hi-tech\USART_PIC16f877a\i2c.c"
	line	26
	global	__size_of_I2CRestart
	__size_of_I2CRestart	equ	__end_of_I2CRestart-_I2CRestart
	
_I2CRestart:	
	opt	stack 6
; Regs used in _I2CRestart: []
	line	27
	
l4894:	
;i2c.c: 27: RSEN = 1;
	bsf	(1161/8)^080h,(1161)&7
	line	28
;i2c.c: 28: while(RSEN);
	
l2460:	
	btfsc	(1161/8)^080h,(1161)&7
	goto	u911
	goto	u910
u911:
	goto	l2460
u910:
	line	29
	
l2463:	
	return
	opt stack 0
GLOBAL	__end_of_I2CRestart
	__end_of_I2CRestart:
;; =============== function _I2CRestart ends ============

	signat	_I2CRestart,88
	global	_I2CStop
psect	text1249,local,class=CODE,delta=2
global __ptext1249
__ptext1249:

;; *************** function _I2CStop *****************
;; Defined at:
;;		line 20 in file "X:\Hi-tech\USART_PIC16f877a\i2c.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		None
;; Tracked objects:
;;		On entry : 60/20
;;		On exit  : 60/20
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_SetMin
;;		_ReadMin
;;		_SetHour
;;		_ReadHour
;;		_SetSeconds
;;		_ReadSeconds
;; This function uses a non-reentrant model
;;
psect	text1249
	file	"X:\Hi-tech\USART_PIC16f877a\i2c.c"
	line	20
	global	__size_of_I2CStop
	__size_of_I2CStop	equ	__end_of_I2CStop-_I2CStop
	
_I2CStop:	
	opt	stack 6
; Regs used in _I2CStop: []
	line	21
	
l5874:	
;i2c.c: 21: PEN = 1;
	bsf	(1162/8)^080h,(1162)&7
	line	22
;i2c.c: 22: while(PEN);
	
l2454:	
	btfsc	(1162/8)^080h,(1162)&7
	goto	u1701
	goto	u1700
u1701:
	goto	l2454
u1700:
	line	24
	
l2457:	
	return
	opt stack 0
GLOBAL	__end_of_I2CStop
	__end_of_I2CStop:
;; =============== function _I2CStop ends ============

	signat	_I2CStop,88
	global	_I2CStart
psect	text1250,local,class=CODE,delta=2
global __ptext1250
__ptext1250:

;; *************** function _I2CStart *****************
;; Defined at:
;;		line 14 in file "X:\Hi-tech\USART_PIC16f877a\i2c.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		None
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 60/20
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_SetMin
;;		_ReadMin
;;		_SetHour
;;		_ReadHour
;;		_SetSeconds
;;		_ReadSeconds
;; This function uses a non-reentrant model
;;
psect	text1250
	file	"X:\Hi-tech\USART_PIC16f877a\i2c.c"
	line	14
	global	__size_of_I2CStart
	__size_of_I2CStart	equ	__end_of_I2CStart-_I2CStart
	
_I2CStart:	
	opt	stack 6
; Regs used in _I2CStart: []
	line	15
	
l5872:	
;i2c.c: 15: SEN = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1160/8)^080h,(1160)&7
	line	16
;i2c.c: 16: while(SEN);
	
l2448:	
	btfsc	(1160/8)^080h,(1160)&7
	goto	u1691
	goto	u1690
u1691:
	goto	l2448
u1690:
	line	18
	
l2451:	
	return
	opt stack 0
GLOBAL	__end_of_I2CStart
	__end_of_I2CStart:
;; =============== function _I2CStart ends ============

	signat	_I2CStart,88
	global	_BCDconv
psect	text1251,local,class=CODE,delta=2
global __ptext1251
__ptext1251:

;; *************** function _BCDconv *****************
;; Defined at:
;;		line 7 in file "X:\Hi-tech\USART_PIC16f877a\rtc.c"
;; Parameters:    Size  Location     Type
;;  source          1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  source          1    4[COMMON] unsigned char 
;;  temp_maj        1    3[COMMON] unsigned char 
;;  temp_min        1    2[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         3       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         3       0       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_ReadMin
;;		_ReadHour
;;		_ReadSeconds
;; This function uses a non-reentrant model
;;
psect	text1251
	file	"X:\Hi-tech\USART_PIC16f877a\rtc.c"
	line	7
	global	__size_of_BCDconv
	__size_of_BCDconv	equ	__end_of_BCDconv-_BCDconv
	
_BCDconv:	
	opt	stack 6
; Regs used in _BCDconv: [wreg+status,2+status,0]
;BCDconv@source stored from wreg
	line	9
	movwf	(BCDconv@source)
	line	8
	
l4876:	
	line	9
;rtc.c: 9: unsigned char temp_maj=0;
	clrf	(BCDconv@temp_maj)
	line	10
	
l4878:	
;rtc.c: 10: temp_min = source&15;
	movf	(BCDconv@source),w
	movwf	(BCDconv@temp_min)
	
l4880:	
	movlw	(0Fh)
	andwf	(BCDconv@temp_min),f
	line	11
	
l4882:	
;rtc.c: 11: temp_maj = source >> 4;
	swapf	(BCDconv@source),w
	andlw	(0ffh shr 4) & 0ffh
	movwf	(BCDconv@temp_maj)
	line	14
	
l4884:	
;rtc.c: 14: temp_maj *= 10;
	clrc
	rlf	(BCDconv@temp_maj),w
	addwf	(BCDconv@temp_maj),f
	addwf	(BCDconv@temp_maj),f
	clrc
	rlf	(BCDconv@temp_maj),f
	line	17
	
l4886:	
;rtc.c: 17: return temp_maj+temp_min;
	movf	(BCDconv@temp_min),w
	addwf	(BCDconv@temp_maj),w
	line	18
	
l1844:	
	return
	opt stack 0
GLOBAL	__end_of_BCDconv
	__end_of_BCDconv:
;; =============== function _BCDconv ends ============

	signat	_BCDconv,4217
	global	_getch
psect	text1252,local,class=CODE,delta=2
global __ptext1252
__ptext1252:

;; *************** function _getch *****************
;; Defined at:
;;		line 15 in file "X:\Hi-tech\USART_PIC16f877a\usart.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_SetupTime
;; This function uses a non-reentrant model
;;
psect	text1252
	file	"X:\Hi-tech\USART_PIC16f877a\usart.c"
	line	15
	global	__size_of_getch
	__size_of_getch	equ	__end_of_getch-_getch
	
_getch:	
	opt	stack 6
; Regs used in _getch: [wreg]
	line	17
	
l4870:	
	line	18
;usart.c: 17: while(!RCIF)
	
l1263:	
	line	17
	btfss	(101/8),(101)&7
	goto	u881
	goto	u880
u881:
	goto	l1263
u880:
	line	19
	
l4872:	
;usart.c: 19: return RCREG;
	movf	(26),w	;volatile
	line	20
	
l1266:	
	return
	opt stack 0
GLOBAL	__end_of_getch
	__end_of_getch:
;; =============== function _getch ends ============

	signat	_getch,89
	global	_putch
psect	text1253,local,class=CODE,delta=2
global __ptext1253
__ptext1253:

;; *************** function _putch *****************
;; Defined at:
;;		line 7 in file "X:\Hi-tech\USART_PIC16f877a\usart.c"
;; Parameters:    Size  Location     Type
;;  byte            1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  byte            1    2[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         1       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_printf
;; This function uses a non-reentrant model
;;
psect	text1253
	file	"X:\Hi-tech\USART_PIC16f877a\usart.c"
	line	7
	global	__size_of_putch
	__size_of_putch	equ	__end_of_putch-_putch
	
_putch:	
	opt	stack 5
; Regs used in _putch: [wreg]
;putch@byte stored from wreg
	movwf	(putch@byte)
	line	9
	
l4866:	
	line	10
;usart.c: 9: while(!TXIF)
	
l1257:	
	line	9
	btfss	(100/8),(100)&7
	goto	u871
	goto	u870
u871:
	goto	l1257
u870:
	line	11
	
l4868:	
;usart.c: 11: TXREG = byte;
	movf	(putch@byte),w
	movwf	(25)	;volatile
	line	12
	
l1260:	
	return
	opt stack 0
GLOBAL	__end_of_putch
	__end_of_putch:
;; =============== function _putch ends ============

	signat	_putch,4216
	global	_I2CInit
psect	text1254,local,class=CODE,delta=2
global __ptext1254
__ptext1254:

;; *************** function _I2CInit *****************
;; Defined at:
;;		line 5 in file "X:\Hi-tech\USART_PIC16f877a\i2c.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 17F/20
;;		On exit  : 17F/20
;;		Unchanged: FFE80/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text1254
	file	"X:\Hi-tech\USART_PIC16f877a\i2c.c"
	line	5
	global	__size_of_I2CInit
	__size_of_I2CInit	equ	__end_of_I2CInit-_I2CInit
	
_I2CInit:	
	opt	stack 7
; Regs used in _I2CInit: [wreg]
	line	6
	
l4836:	
;i2c.c: 6: TRISC3 = 1;
	bsf	(1083/8)^080h,(1083)&7
	line	7
;i2c.c: 7: TRISC4 = 1;
	bsf	(1084/8)^080h,(1084)&7
	line	8
;i2c.c: 8: SSPSTAT |= 0x80;
	bsf	(148)^080h+(7/8),(7)&7	;volatile
	line	9
	
l4838:	
;i2c.c: 9: SSPCON = 0x28;
	movlw	(028h)
	bcf	status, 5	;RP0=0, select bank0
	movwf	(20)	;volatile
	line	10
;i2c.c: 10: SSPADD = 0x28;
	movlw	(028h)
	bsf	status, 5	;RP0=1, select bank1
	movwf	(147)^080h	;volatile
	line	11
	
l2445:	
	return
	opt stack 0
GLOBAL	__end_of_I2CInit
	__end_of_I2CInit:
;; =============== function _I2CInit ends ============

	signat	_I2CInit,88
	global	_isr
psect	text1255,local,class=CODE,delta=2
global __ptext1255
__ptext1255:

;; *************** function _isr *****************
;; Defined at:
;;		line 130 in file "X:\Hi-tech\USART_PIC16f877a\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  2  624[COMMON] int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          2       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	text1255
	file	"X:\Hi-tech\USART_PIC16f877a\main.c"
	line	130
	global	__size_of_isr
	__size_of_isr	equ	__end_of_isr-_isr
	
_isr:	
	opt	stack 2
; Regs used in _isr: [wreg+status,2+status,0]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+0
	movwf	saved_w
	movf	status,w
	movwf	(??_isr+0)
	movf	pclath,w
	movwf	(??_isr+1)
	ljmp	_isr
psect	text1255
	line	132
	
i1l4840:	
;main.c: 132: if (TMR2IF) {
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(97/8),(97)&7
	goto	u85_21
	goto	u85_20
u85_21:
	goto	i1l627
u85_20:
	line	133
	
i1l4842:	
;main.c: 133: update++;
	incf	(_update),f	;volatile
	line	135
	
i1l4844:	
;main.c: 135: if (update == 255) {
	movf	(_update),w	;volatile
	xorlw	0FFh
	skipz
	goto	u86_21
	goto	u86_20
u86_21:
	goto	i1l4850
u86_20:
	line	136
	
i1l4846:	
;main.c: 136: flag++;
	incf	(_flag),f	;volatile
	line	137
	
i1l4848:	
;main.c: 137: update = 0;
	clrf	(_update)	;volatile
	line	140
	
i1l4850:	
;main.c: 138: }
;main.c: 140: TMR2 = 0x01;
	movlw	(01h)
	movwf	(17)	;volatile
	line	141
	
i1l4852:	
;main.c: 141: T2CKPS0 = 1; T2CKPS1 = 1;
	bsf	(144/8),(144)&7
	
i1l4854:	
	bsf	(145/8),(145)&7
	line	142
	
i1l4856:	
;main.c: 142: TOUTPS2 = 1; TOUTPS1 = 1; TOUTPS3 = 1; TOUTPS0 = 1;
	bsf	(149/8),(149)&7
	
i1l4858:	
	bsf	(148/8),(148)&7
	
i1l4860:	
	bsf	(150/8),(150)&7
	
i1l4862:	
	bsf	(147/8),(147)&7
	line	144
	
i1l4864:	
;main.c: 144: TMR2IF = 0;
	bcf	(97/8),(97)&7
	line	147
	
i1l627:	
	movf	(??_isr+1),w
	movwf	pclath
	movf	(??_isr+0),w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_isr
	__end_of_isr:
;; =============== function _isr ends ============

	signat	_isr,90
psect	text1256,local,class=CODE,delta=2
global __ptext1256
__ptext1256:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
