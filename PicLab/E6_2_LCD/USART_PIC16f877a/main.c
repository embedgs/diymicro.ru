#include <htc.h>
#include <stdio.h>

#include "lcd.h"


#include "usart.h"
#include "i2c.h"
#include "rtc.h"



#define _XTAL_FREQ 4000000

#define STATE TRISC0
#define PIN RC0

#define out RD0

volatile unsigned char update = 0;
volatile unsigned char flag = 0;
const unsigned char digits[10] = {
                                  0b00110000,            //0
								  0b00110001,            //1
								  0b00110010,            //2
								  0b00110011,            //3
								  0b00110100,            //4
								  0b00110101,            //5
								  0b00110110,            //6
								  0b00110111,            //7
								  0b00111000,            //8
								  0b00111001,            //9
								  };
								  

__CONFIG(WDTDIS & UNPROTECT & LVPDIS & HS);



	char temperature = 25;
    unsigned char temp_drob = 0;
    unsigned char sign = 0;


static bit INIT(void);
void TX(unsigned char cmd);
unsigned char RX();	
void get_temp();
void printTemp();
void display_tt();
void menu(unsigned char i);
void pointer_time(unsigned char i);
display_digit(unsigned char digit, unsigned char pos);
	
void main()
{
unsigned char input;
unsigned char temp;
unsigned char i = 1;
unsigned char hr;
unsigned char status = 0;
unsigned char itemp = 0;
    //get_temp();
    TRISB = 0x00;
    TRISD = 0x00;
	
	
	init_comms();	// set up the USART - settings defined in usart.h
	I2CInit();	
    lcd_init();
   
    menu(i);  
	
TMR2 = 0x01; //стартуем с 1
PR2 = 0xFF;  //считаем по 255*16 мкс
T2CKPS0 = 1; T2CKPS1 = 1; //1 делитель не делит входную частоту
TOUTPS2 = 1; TOUTPS1 = 1; TOUTPS3 = 1; TOUTPS0 = 1; //2 делитель делит на 5
GIE = 1;   // глобальные прерывания
PEIE = 1;  // прерывания перефирии
TMR2IE = 1;
TMR2ON = 1; // Запуск таймера!



    
    while (1) {
	     
		if (flag == 2) { 
		               GIE = 0;
					   TMR2ON = 0;

					   display_tt(); 
		               flag = 0;
					   GIE = 1;
					   TMR2ON = 1;}
		//input = getch();
        
                      							
		switch (input) {
                    case 49 : ShowTime();
					          break;
					case 50 : SetupTime();
                              break;	
				    case 51 : 
					          GIE = 0; 
					          temp = ReadHour();
					          if (temp <10) printf("\r\n1010%d", temp); else printf("\r\n101%d", temp);
					          __delay_ms(20);
				              temp = ReadMin();
					          if (temp <10) printf("\r\n1020%d", temp); else printf("\r\n102%d", temp);
					          __delay_ms(20);
			               	 temp = ReadSeconds();
				 	          if (temp <10) printf("\r\n1030%d", temp); else printf("\r\n103%d", temp);
							  __delay_ms(20);
                              break;	
					case 52 : printTemp();
                              break;			  
                             			
                    case 53 : display_tt();										
                              break;
					
	
	            } 
       }  
	

}


interrupt isr() {

if (TMR2IF) {
update++;

if (update == 255) { 
                   flag++;
				   update = 0;
				   }
	
TMR2 = 0x01;					
T2CKPS0 = 1; T2CKPS1 = 1; //1 делитель не делит входную частоту
TOUTPS2 = 1; TOUTPS1 = 1; TOUTPS3 = 1; TOUTPS0 = 1; //2 делитель делит на 5

TMR2IF = 0;  //сброс флага
}
 
}




/* 1-wire functions */
static bit INIT(void){
static bit b;
STATE = 1;
STATE = 0;
__delay_us(500);
STATE = 1;
__delay_us(65);
b = PIN;
__delay_us(450);

return b;

}

void TX(unsigned char cmd){
unsigned char temp = 0;
unsigned char i = 0;
temp = cmd;
for (i=0;i<8;i++) {
                   if (temp&0x01) {
										STATE = 0;
										__delay_us(5);
										STATE = 1;				   
										__delay_us(70);
									  } else {
								         		STATE = 0;
												__delay_us(70);
												STATE = 1;
												__delay_us(5);
											
											  }
					temp >>= 1;						  
				   }

}

unsigned char RX() {
unsigned char d = 0;
unsigned char i = 0;
for (i=0;i<8;i++){
                  STATE = 0;
				 __delay_us(6);
				 STATE = 1;
				 __delay_us(4);
				 d>>=1;
				 if (PIN == 1) d |= 0x80; 
				//printf("\r\n %d", d);
				 __delay_us(60);	
				 
                
                 	 
				 }
				 
return d;				 

}

void get_temp() {
static bit init;
unsigned char temp1;
unsigned char temp2;
init = INIT();
if (!init) {
TX(0xCC);
TX(0x44);
__delay_ms(150);
__delay_ms(150);
__delay_ms(150);
__delay_ms(150);
__delay_ms(150); 
} else printf("bug");
init = INIT();
if (!init) {
TX(0xCC);
TX(0xBE);

temp1 = RX();
temp2 = RX();

}
temp_drob = temp1 & 0b00001111;    //Записываем дробную часть в отдельную переменную
temp_drob = ((temp_drob*6)+2)/10;              //Переводим в нужное дробное число
temp1 >>= 4;
sign = temp2 & 0x80;
temp2 <<= 4;
temp2 &= 0b01110000;
temp2 |= temp1;

if (sign) { temperature = 127-temp2; 
            temp_drob = 10 - temp_drob; 
           }   else temperature = temp2;


}

void printTemp() {                                                         
    get_temp();
	//printf("\r\ntemperatura -- ");
     if (temperature<10) {
                         if (sign) printf("\r\n2020"); else printf("\r\n2010");
	 } else {
	         if (sign) printf("\r\n202"); else printf("\r\n201");
	        }
    printf("%d", temperature);
	__delay_ms(20);
	if (temp_drob<10) printf("\r\n2030"); else printf("\r\n203");  
	                  
	
    printf("%d", temp_drob);
	__delay_ms(20);
}

void display_tt() {													//дежурная функция отображения информации на LCD
unsigned char d;

get_temp();                                                         //берем значение температуры
LCD_RS = 0;
lcd_write(0b00001100);
__delay_us(100);
lcd_clear();
d = ReadHour();														
lcd_goto(0x05);
lcd_putch(digits[d/10]);											//выводим значение часов	
d = d - ((d/10)*10);
lcd_putch(digits[d]);
d = 0b00111010;
lcd_putch(d);
d = ReadMin();
lcd_putch(digits[d/10]);  											//выводим значение минут
d = d - ((d/10)*10);
lcd_putch(digits[d]);
lcd_goto(0x44);
if (!sign) lcd_putch(0b00101011); else lcd_putch(0b10110000);       //знак температуры
d=temperature/10;
lcd_putch(digits[d]);												//целое значение температуры
d=temperature-((temperature/10)*10);
lcd_putch(digits[d]);
d = 0b00101110;
lcd_putch(d); 														//точка
lcd_putch(digits[temp_drob]);										//вывод дробного значения
d = 0b11011111;
lcd_putch(d); 														//градус
d = 0b01000011;
lcd_putch(d);														//С

}

void menu(unsigned char i) {
   lcd_clear();
   LCD_RS = 0;
   lcd_write(0b00001100);
   __delay_us(100);
   switch (i) {
               case 1 : 
	                  lcd_goto(0x00);
	                  lcd_puts("Menu:");
                      lcd_goto(0x07);
	                  lcd_putch(0b00111110);
	                  lcd_goto(0x08);
	                  lcd_puts("1.Volume");
	                  lcd_goto(0x48);
	                  lcd_puts("2.Bass");
	                  break;
	           case 2 : 
	                  lcd_goto(0x00);
	                  lcd_puts("Menu:");
	                  lcd_goto(0x08);
	                  lcd_puts("1.Volume");
                      lcd_goto(0x47);
	                  lcd_putch(0b00111110);
	                  lcd_goto(0x48);
	                  lcd_puts("2.Bass");
	                  break;			
               case 3 : 
	                  lcd_goto(0x00);
	                  lcd_puts("Menu:");
                      lcd_goto(0x07);
	                  lcd_putch(0b00111110);
	                  lcd_goto(0x08);
	                  lcd_puts("2.Bass");
	                  lcd_goto(0x48);
	                  lcd_puts("3.Treble");
	                  break;
	           case 4 : 
	                  lcd_goto(0x00);
	                  lcd_puts("Menu:");
	                  lcd_goto(0x08);
	                  lcd_puts("2.Bass");
                      lcd_goto(0x47);
	                  lcd_putch(0b00111110);
	                  lcd_goto(0x48);
	                  lcd_puts("3.Treble");
	                  break;	
               case 5 : 
	                  lcd_goto(0x00);
	                  lcd_puts("Menu:");
                      lcd_goto(0x07);
	                  lcd_putch(0b00111110);
	                  lcd_goto(0x08);
	                  lcd_puts("3.Treble");
	                  lcd_goto(0x48);
	                  lcd_puts("4.Mute");
	                  break;
	           case 6 : 
	                  lcd_goto(0x00);
	                  lcd_puts("Menu:");
	                  lcd_goto(0x08);
	                  lcd_puts("3.Treble");
                      lcd_goto(0x47);
	                  lcd_putch(0b00111110);
	                  lcd_goto(0x48);
	                  lcd_puts("4.Mute");
	                  break;	
               case 7 : 
	                  lcd_goto(0x00);
	                  lcd_puts("Menu:");
                      lcd_goto(0x07);
	                  lcd_putch(0b00111110);
	                  lcd_goto(0x08);
	                  lcd_puts("4.Mute");
	                  lcd_goto(0x48);
	                  lcd_puts("5.Time");
	                  break;
	           case 8 : 
	                  lcd_goto(0x00);
	                  lcd_puts("Menu:");
	                  lcd_goto(0x08);
	                  lcd_puts("4.Mute");
                      lcd_goto(0x47);
	                  lcd_putch(0b00111110);
	                  lcd_goto(0x48);
	                  lcd_puts("5.Time");
	                  break;	
               case 9 : 
	                  lcd_goto(0x00);
	                  lcd_puts("Menu:");
                      lcd_goto(0x07);
	                  lcd_putch(0b00111110);
	                  lcd_goto(0x08);
	                  lcd_puts("5.Time");
	                  lcd_goto(0x48);
	                  lcd_puts("6.StBy");
	                  break;
	           case 10 : 
	                  lcd_goto(0x00);
	                  lcd_puts("Menu:");
	                  lcd_goto(0x08);
	                  lcd_puts("5.Time");
                      lcd_goto(0x47);
	                  lcd_putch(0b00111110);
	                  lcd_goto(0x48);
	                  lcd_puts("6.StBy");
	                  break;							  
           	}
}

void pointer_time(unsigned char i) {
switch (i) {
case 1 : lcd_goto(0x00);
         lcd_putch(0b00111110);
		 lcd_goto(0x40);
         lcd_putch(0b00100000);
		 lcd_goto(0x09);
		 lcd_putch(0b00100000);
		 lcd_goto(0x49);
		 lcd_putch(0b00100000);
		 break;
case 2 : lcd_goto(0x00);
         lcd_putch(0b00100000);
		 lcd_goto(0x40);
         lcd_putch(0b00111110);
		 lcd_goto(0x09);
		 lcd_putch(0b00100000);
		 lcd_goto(0x49);
		 lcd_putch(0b00100000);
		 break;		 
case 3 : lcd_goto(0x00);
         lcd_putch(0b00100000);
		 lcd_goto(0x40);
         lcd_putch(0b00100000);
		 lcd_goto(0x09);
		 lcd_putch(0b00111110);
		 lcd_goto(0x49);
		 lcd_putch(0b00100000);
		 break;
case 4 : lcd_goto(0x00);
         lcd_putch(0b00100000);
		 lcd_goto(0x40);
         lcd_putch(0b00100000);
		 lcd_goto(0x09);
		 lcd_putch(0b00100000);
		 lcd_goto(0x49);
		 lcd_putch(0b00111110);
		 break;	
           }		 
}

display_digit(unsigned char digit, unsigned char pos){
                                                          lcd_goto(pos);
														  lcd_putch(digits[digit/10]);											//выводим значение часов	
                                                          digit = digit - ((digit/10)*10);
                                                          lcd_putch(digits[digit]);
}