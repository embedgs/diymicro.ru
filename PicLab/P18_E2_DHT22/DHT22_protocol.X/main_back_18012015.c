#include <xc.h>
#include <plib/usart.h>
#include <plib/delays.h>

#define _XTAL_FREQ 12000000 //The speed of your internal(or)external oscillator
#define State TRISC0
#define Humpin RC0

#pragma config WDTEN = OFF, LVP = OFF, FOSC = HS  

unsigned char UARTConfig = 0;
unsigned char baud = 0;
char txbyte = 'U';         //наш байт для пересылки
char rxbyte;
unsigned char Rxdata[10];

void NumToUart(unsigned int Num);  
void Delay1us();          //1us delay for 12MHz xtall
void DelayUs(unsigned char Us);
void DelayMs(unsigned int Ms);
void GetRHandTemp();

void main() 
{

 ANSEL = 0;                                                                   //Отключаем аналоговые буфера
 ANSELH = 0;

 TRISB7 = 0; //TX pin set as output
 TRISB5 = 1; //RX pin set as input
 TRISB6 = 1; //RX pin set as input
 State = 1;

 UARTConfig = USART_TX_INT_OFF & USART_RX_INT_OFF & USART_ASYNCH_MODE & USART_EIGHT_BIT & USART_BRGH_HIGH ;    //прерывания выключены, асинхронный режим, 8 бит, высокоскоростной режим
 baud = 77;                 //Focs/(9600*16) - 1
 OpenUSART(UARTConfig,baud);


 putsUSART( (char *) "Welcome to Diymicro.ru\r\n" );
 putsUSART( (char *) " \r\nNow, press 1 button for start measurement \r\n" );

 while(1)
 {

    while(!DataRdyUSART());
    rxbyte=ReadUSART();
    while(BusyUSART());

    NumToUart(rxbyte);
    if (rxbyte == 49) 
    {
      putsUSART( (char *) "Measuring has been started!\r\n" );
      State = 1;
      State = 0;
      Humpin = 0;
      DelayUs(20);         //Ждем около 20мс
      State = 1;
      DelayUs(40);         //Ждем 40мкс
      
      rxbyte = 0;
    }



  }//while(1)

}//main()


void NumToUart(unsigned int Num)                                                //Число в уарт
{

  unsigned int bignum = 10000;
  unsigned char numtemp = 5;

  while(numtemp>0)                                                             //Определяем сколько разрядов имеет наше число
  {
    if (Num/bignum)
        break;
    numtemp--;
    bignum = bignum / 10;  
  }  



  for (unsigned char i = numtemp; i>0; i--)
    {
      WriteUSART( (Num - (Num/(bignum*10))*bignum*10 )/bignum + '0');         //Выталкиеваем все разряды - от старшего к младшему
      while(BusyUSART());                                                       //Ждем пока освободится модуль иначе будут прострелы
      bignum = bignum/10;
    }

}


void Delay1us()         //delay approx 1 us
{
  Delay1TCY();
  Delay1TCY();
  Delay1TCY();
}

void DelayUs(unsigned char Us)      //delay for a given number of microseconds
{
  for (unsigned char i = 0; i<Us; i++)
    Delay1us(); 
}

void DelayMs(unsigned int Ms)      //approx delay for a given number of miliseconds
{
  for (unsigned int i=0; i<Ms; i++)
    Delay1KTCYx(3);
}


void GetRHandTemp()
{
  

    INTE = 0;
    DHTbyte[0] = 0;
    DHTbyte[1] = 0;
    DHTbyte[2] = 0;
    Humflag = 0;
    IsrCount = 0;
    


    State = 1;
    State = 0;
    HumPin = 0;
    __delay_ms(20);
    State = 1;
    HumPin = 1;
    __delay_us(40);
    State = 1;
    INTEDG = 0;
    INTE = 1;
    
    unsigned char tmrcount = 0;

    while (!Humflag)
    {
    

      if (TMR0>254)                 //попытка придумывания антизависаний
      {
        TMR0 = 0;
        tmrcount++;
        if (tmrcount>=254)
        {
          Humflag = 1;
          RHStatus = 0;
          
        }
      } 

      if (InteFlag)
      {
        INTE = 0;
        if (IsrCount==2) INTEDG = 1;

        if ((IsrCount > 2) && (IsrCount < 12))
        {
          //temp[IsrCount-3]=tempTmr0;
          DHTbyte[0]<<=1;
          if (tempTmr0>50) DHTbyte[0] |= 0b00000001;
          InitTimer0();
        }

        if ((IsrCount > 18) && (IsrCount < 28))
        {
          //temp[IsrCount-3]=tempTmr0;
          DHTbyte[1]<<=1;
          if (tempTmr0>50) DHTbyte[1] |= 0b00000001;
          InitTimer0();
        }

        if ((IsrCount > 34) && (IsrCount < 44))
        {
          //temp[IsrCount-3]=tempTmr0;
          DHTbyte[2]<<=1;
          if (tempTmr0>50) DHTbyte[2] |= 0b00000001;
          InitTimer0();
        }

        InteFlag = 0;
        INTE = 1;
      }//if (InteFlag)


      if (IsrCount>=43)
      {
        INTE = 0;
        Humflag = 1;
        if ((DHTbyte[2] == (DHTbyte[0]+DHTbyte[1]))&&(DHTbyte[2]))
        {
          //Humflag = 1;
          
          //Humflag = 1;
          IsrCount = 0;
          RHStatus = 1;
        
        } 
        //Humflag = 1;

      }


    }
}//GetRHandTemp
