#include <htc.h>
#ifndef _LCD_H_
#define _LCD_H_

#ifndef _XTAL_FREQ
 // Unless specified elsewhere, 4MHz system frequency is assumed
 #define _XTAL_FREQ 12000000
#endif

#define	LCD_RS RA1
#define LCD_EN RA2

#define lcd4b RB1
#define lcd5b RB2
#define lcd6b RB3
#define lcd7b RB4


#define	lcd_cursor(x)	lcd_write(((x)&0x7F)|0x80)
#define    LCD_STROBE()    ((LCD_EN = 1),(LCD_EN=0))
#define testbit(data,bitno) ((data>>bitno)&0x01)

extern void lcd_write(unsigned char c);
extern void lcd_clear(void);
extern void lcd_puts(const char * s);
extern void lcd_putch(char c);
extern void lcd_goto(unsigned char pos);
extern void lcd_init();

#endif
