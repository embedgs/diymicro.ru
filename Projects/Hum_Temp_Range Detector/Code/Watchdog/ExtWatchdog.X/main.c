#include <htc.h>
#include <stdio.h>
#include <stdlib.h>

__CONFIG(MCLRDIS & UNPROTECT & BORDIS & WDTDIS & PWRTEN & INTIO); 


#define _XTAL_FREQ 4000000

#define ResetPin GPIO4
#define Trig GPIO5

volatile bit TrigFlag;
volatile unsigned char TmrCount = 0;
void InitTimer1();

void main() 
{

TrigFlag = 0;
GIE = 0;
TRISIO = 32;

CMCON=0b00000111;
GPIO = 0x00;
IOCB5 = 1;

GPIE = 1;
GIE = 1;
PEIE = 1;
ResetPin = 1;
InitTimer1();
for(;;) 
{
	if (TmrCount>250)
	{
		ResetPin = 0;
		__delay_ms(200);
		__delay_ms(200);
		__delay_ms(200);
		__delay_ms(200);
		ResetPin = 1;

	}			
}			

}


void interrupt isr() {
GPIO;


if (GPIF)
{	
	if (Trig) 
		{
			TmrCount = 0;
			InitTimer1();
		}

	GPIF = 0;
}

if (TMR1IF)
{
	TmrCount++;
	InitTimer1();
	TMR1IF = 0;
}


}	

void InitTimer1()
{
	TMR1ON = 1;
	TMR1GE = 0;
	T1CKPS0 = 1;
	T1CKPS1 = 1;
	TMR1H = 0; TMR1L = 0;
	T1OSCEN = 0;
	TMR1CS = 0;
	TMR1IE = 1;
}
