#include <htc.h>
#include "lcd.h"

#define _XTAL_FREQ 12000000
#define State TRISB0
#define HumPin RB0

#define StRMeas TRISB7
#define RMeas TRISB0

__CONFIG(WDTDIS & UNPROTECT & LVPDIS & HS);

bit Humflag = 0;
//unsigned char temp[18] = 0;

volatile bit InteFlag = 0;
unsigned char DHTbyte[3];

volatile unsigned char IsrCount = 0;
volatile unsigned char tempTmr0 = 0;
const unsigned char digits[10] = {
                  0b00110000,            //0
				  0b00110001,            //1
				  0b00110010,            //2
				  0b00110011,            //3
				  0b00110100,            //4
				  0b00110101,            //5
				  0b00110110,            //6
				  0b00110111,            //7
				  0b00111000,            //8
				  0b00111001,            //9
				  };


display_digit(unsigned char digit, unsigned char pos);
void InitTimer0();
void GetRHandTemp();



void main() {
 State = 1;
 CMCON = 0x07;
 TRISA1 = 0;
 TRISA2 = 0;
 TRISA3 = 0;
 TRISA0 = 0;
 TRISB2 = 0;
 TRISB3 = 0;
 TRISB5 = 0;
 TRISB1 = 1;
 TRISB4 = 0;
 StRMeas = 0;
 RMeas = 0;
lcd_init();
lcd_goto(0x00);
lcd_puts("IsrCount = ");

 
T0CS = 0;
PSA = 0;
GIE = 1;
PEIE = 1;
INTEDG = 0;
HumPin = 1;
IsrCount = 0;
Humflag = 0;


for (;;) {



	if (!RB1)
	{
		__delay_ms(200);
		if (!RB1)
		{	
			//GetRHandTemp();
			StRMeas = 0;
			RMeas = 0;
			RMeas = 1;
			__delay_us(10)	;
			RMeas = 0;
			StRMeas = 1;

		}//if (!RB1) inner 
	}//if (!RB1) 
}


}

void interrupt isr()
{


if (INTF)
{
		tempTmr0 = TMR0;
		IsrCount++;
		InteFlag = 1;
        INTF = 0;
}

}


display_digit(unsigned char digit, unsigned char pos)
{
                                                        
    unsigned char TempDigit = 0;
    lcd_goto(pos);
    if (digit < 100)
    {
        lcd_goto(pos);
	    lcd_putch(digits[digit/10]);	
        digit = digit - ((digit/10)*10);
        lcd_putch(digits[digit]);
    } 
    	/*else
    	{
    		lcd_putch(digits[digit/100]);		   
    		TempDigit = digit - ((digit/100)*100);
  			lcd_putch(digits[TempDigit/10]);
  			TempDigit = TempDigit - ((TempDigit/10)*10);
  			lcd_putch(digits[TempDigit]);
            
    	}
    	*/
}

void InitTimer0()
{
	TMR0 = 0;
	OPTION = 0b10010001; 

}


void GetRHandTemp()
{
	INTE = 0;
		DHTbyte[0] = 0;
		DHTbyte[1] = 0;
		DHTbyte[2] = 0;
		Humflag = 0;
		//display_digit(IsrCount,0x40);
		IsrCount = 0;
		State = 1;
		State = 0;
		HumPin = 0;
		__delay_ms(20);
		State = 1;
		HumPin = 1;
		__delay_us(40);
		State = 1;
		INTEDG = 0;
		INTE = 1;
		

		while (!Humflag)
		{
		
			if (InteFlag)
			{
				INTE = 0;
				if (IsrCount==2) INTEDG = 1;

				if ((IsrCount > 2) && (IsrCount < 12))
				{
					//temp[IsrCount-3]=tempTmr0;
					DHTbyte[0]<<=1;
					if (tempTmr0>50) DHTbyte[0] |= 0b00000001;
					InitTimer0();
				}

				if ((IsrCount > 18) && (IsrCount < 28))
				{
					//temp[IsrCount-3]=tempTmr0;
					DHTbyte[1]<<=1;
					if (tempTmr0>50) DHTbyte[1] |= 0b00000001;
					InitTimer0();
				}

				if ((IsrCount > 34) && (IsrCount < 44))
				{
					//temp[IsrCount-3]=tempTmr0;
					DHTbyte[2]<<=1;
					if (tempTmr0>50) DHTbyte[2] |= 0b00000001;
					InitTimer0();
				}

				InteFlag = 0;
				INTE = 1;
			}//if (InteFlag)


			if (IsrCount>=43)
			{
				Humflag = 1;
				INTE = 0;
				lcd_clear();
				lcd_goto(0x00);
				lcd_puts("RH");
				lcd_goto(0x06);
				lcd_puts("Temp");
				display_digit(DHTbyte[0],0x40);
				display_digit(DHTbyte[1],0x46);
				lcd_goto(0x48);
				lcd_putch(0b11011111);								//градус
				lcd_putch(0b01000011);								//С
				//display_digit(DHTbyte[2],0x07);
				lcd_goto(0x0E);
				if (DHTbyte[2] == (DHTbyte[0]+DHTbyte[1])) 
					lcd_puts("ok");
				else
					lcd_puts("er");	
				//display_digit(IsrCount,0x40);
			}


		}
}//GetRHandTemp