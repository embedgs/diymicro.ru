/* ------------------------------------------------------//

Проект контроля присутствия человека, влажности и 



Автор: Гринько Сергей Николаевич
e-mail: sargein@gmail.com
site: http://diymicro.ru



---------------------------------------------------------*/

#include <htc.h>
#include "lcd.h"

#define _XTAL_FREQ 12000000
#define State TRISB0
#define HumPin RB0

#define StRMeas TRISB7
#define RMeas RB7
#define Recho RB6

#define HumSw RA4

__CONFIG(WDTDIS & UNPROTECT & LVPDIS & HS & MCLREN);

bit Humflag = 0;
//unsigned char temp[18] = 0;

volatile bit InteFlag = 0;
unsigned char DHTbyte[3];

volatile unsigned char IsrCount = 0;
volatile unsigned char tempTmr0 = 0;
volatile bit RBlowFlag = 0;
volatile bit RBhighFlag = 0;
volatile unsigned int tempTmr1 = 0;
unsigned int Range = 0;
unsigned char MeasCount;
volatile static bit RHStatus = 0;

const unsigned char digits[10] = {
                  0b00110000,            //0
				  0b00110001,            //1
				  0b00110010,            //2
				  0b00110011,            //3
				  0b00110100,            //4
				  0b00110101,            //5
				  0b00110110,            //6
				  0b00110111,            //7
				  0b00111000,            //8
				  0b00111001,            //9
				  };


display_digit(unsigned char digit, unsigned char pos);
void InitTimer0();
void InitTimer1();
void GetRHandTemp();
void GetRange();


void main() {
 State = 1;
 CMCON = 0x07;
 TRISA0 = 1;	 
 TRISA1 = 0;
 TRISA2 = 0;
 TRISA3 = 0;
 TRISA4 = 0;
 TRISB5 = 0;
 TRISB1 = 0;
 TRISB2 = 0;
 TRISB3 = 0;
 TRISB4 = 0;
 HumSw = 0;
 StRMeas = 0;
 RMeas = 0;
lcd_init();
lcd_goto(0x00);
lcd_puts("IsrCount = ");

 
T0CS = 0;
PSA = 0;
GIE = 1;
PEIE = 1;
INTEDG = 0;
HumPin = 1;
IsrCount = 0;
Humflag = 0;


for (;;) {




	if (!RA0)
	{
		__delay_ms(200);
		if (!RA0)
		{	
			RHStatus = 0;
			MeasCount = 0;
			//while(MeasCount != 3)
			//{	
			//__delay_ms(200);
			//__delay_ms(200);
			//MeasCount=3;
			//lcd_goto(MeasCount);
			//lcd_puts("*");

			GetRHandTemp();

			//if (MeasCount>=3)
			//	RHStatus = 1;
			//} 
			//if ((RHStatus)&&(MeasCount<3))

			if (RHStatus)
			{
					lcd_clear();
					lcd_goto(0x00);
					lcd_puts("RH");
					lcd_goto(0x06);
					lcd_puts("Temp");
					display_digit(DHTbyte[0],0x40);
					display_digit(DHTbyte[1],0x46);
					lcd_goto(0x48);
					lcd_putch(0b11011111);								//градус
					lcd_putch(0b01000011);								//С
			} 
				else 
					{
						__delay_ms(200);
						__delay_ms(200);
						GetRHandTemp();
						if (RHStatus)
						
						{
							lcd_clear();
							lcd_goto(0x00);
							lcd_puts("RH");
							lcd_goto(0x06);
							lcd_puts("Temp");
							display_digit(DHTbyte[0],0x40);
							display_digit(DHTbyte[1],0x46);
							lcd_goto(0x48);
							lcd_putch(0b11011111);								//градус
							lcd_putch(0b01000011);								//С
						} 
						else
						{
						lcd_clear();
						lcd_goto(0x00);
						lcd_puts("2 ERROR");
						}
					}	
			//GetRange();


		}//if (!RB1) inner 
	}//if (!RB1) 
}


}

void interrupt isr()
{


if (INTF)
{
		tempTmr0 = TMR0;
		IsrCount++;
		InteFlag = 1;
        INTF = 0;
} //if (INTF)


if (RBIF)
{
	
	if (!Recho)
	{
		
		/*TMR1ON = 0;
		tempTmr1 = TMR1H;
		tempTmr1 <<= 8;
		tempTmr1 = TMR1L;*/
		RBlowFlag = 1;
		RB5 = 0;
		
	
	}

	if (Recho)
	{
		//RBFlag = 0;
		//InitTimer1();
		RB5 = 1;
		RBhighFlag = 1;
	}

	RBIF = 0;
	
}//if (RBIF)

} //isr()


display_digit(unsigned char digit, unsigned char pos)
{
                                                        
    unsigned char TempDigit = 0;
    lcd_goto(pos);
    if (digit < 100)
    {
        lcd_goto(pos);
	    lcd_putch(digits[digit/10]);	
        digit = digit - ((digit/10)*10);
        lcd_putch(digits[digit]);
    } 
    	/*else
    	{
    		lcd_putch(digits[digit/100]);		   
    		TempDigit = digit - ((digit/100)*100);
  			lcd_putch(digits[TempDigit/10]);
  			TempDigit = TempDigit - ((TempDigit/10)*10);
  			lcd_putch(digits[TempDigit]);
            
    	}
    	*/
}

void InitTimer0()
{
	TMR0 = 0;
	OPTION = 0b10010001; 

}

void InitTimer1()
{
	T1CKPS1 = 0;
	T1CKPS0 = 0; // предделитель = 8
 	T1OSCEN = 0; //выключаем внутренний генератор
	TMR1CS = 0; // Fosc/4
	TMR1ON = 1; // включаем таймер
 	TMR1H = 0x00; TMR1L = 0x00;  //инициализация таймера со значения 65024
 	//RB5 = 1;
}

void GetRHandTemp()
{
	
		
		MeasCount = 0;
		INTE = 0;
		DHTbyte[0] = 0;
		DHTbyte[1] = 0;
		DHTbyte[2] = 0;
		Humflag = 0;
		IsrCount = 0;
		


		State = 1;
		State = 0;
		HumPin = 0;
		__delay_ms(20);
		State = 1;
		HumPin = 1;
		__delay_us(40);
		State = 1;
		INTEDG = 0;
		INTE = 1;
		


		while (!Humflag)
		{
		
			if (InteFlag)
			{
				INTE = 0;
				if (IsrCount==2) INTEDG = 1;

				if ((IsrCount > 2) && (IsrCount < 12))
				{
					//temp[IsrCount-3]=tempTmr0;
					DHTbyte[0]<<=1;
					if (tempTmr0>50) DHTbyte[0] |= 0b00000001;
					InitTimer0();
				}

				if ((IsrCount > 18) && (IsrCount < 28))
				{
					//temp[IsrCount-3]=tempTmr0;
					DHTbyte[1]<<=1;
					if (tempTmr0>50) DHTbyte[1] |= 0b00000001;
					InitTimer0();
				}

				if ((IsrCount > 34) && (IsrCount < 44))
				{
					//temp[IsrCount-3]=tempTmr0;
					DHTbyte[2]<<=1;
					if (tempTmr0>50) DHTbyte[2] |= 0b00000001;
					InitTimer0();
				}

				InteFlag = 0;
				INTE = 1;
			}//if (InteFlag)


			if (IsrCount>=43)
			{
				INTE = 0;
				Humflag = 1;
				if ((DHTbyte[2] == (DHTbyte[0]+DHTbyte[1]))&&(DHTbyte[2]))
				{
					//Humflag = 1;
					
					//Humflag = 1;
					IsrCount = 0;
					RHStatus = 1;
				
				}	
				//Humflag = 1;
					//lcd_puts("er");	
				//display_digit(IsrCount,0x40);
			}


		}
}//GetRHandTemp

void GetRange()
{
	tempTmr1 = 0;
	StRMeas = 0;
	RMeas = 0;
	RMeas = 1;
	__delay_us(10);
	RMeas = 0;
	StRMeas = 0;

	__delay_us(100);
	
	RBIE = 1;

	RBlowFlag = 0;

	while (!RBlowFlag)
	{
		if (RBhighFlag)
		{
			InitTimer1();
			RBhighFlag = 0;
		}
	};

	TMR1ON = 0;
	tempTmr1 = TMR1H;
	tempTmr1 <<= 8;
	tempTmr1 |= TMR1L;
	
	lcd_clear();
	lcd_goto(0x00);
	
	Range = tempTmr1/174 + 2;				//Поправочный коэффициент - 2 см
	lcd_puts("Range, cm");
	display_digit(Range, 0x40);
}