/*
 * File:   main.c
 * Author: Sergey Grinko
 * e-mail: sargein@gmail.com
 * 
 * site: diymicro.ru
 *
 * Created on 24 May 2018 ?., 23:05
 */


#include <xc.h>

#define _XTAL_FREQ 4000000

#define BAT1 RB1            //battery 1 enable (0), disable (1)
#define BAT2 RB2			//battery 2 enable (0), disable (1)
#define USB RA3				//Indication of USB connection
#define MOD1 RB0 			//Indication of mode #1
#define MOD2 RB5			//Indication of mode #2
#define LBO	 RA4			//Battery low indicator input


//__CONFIG(WDTDIS & UNPROTECT & LVPDIS & XT & MCLREN & BORDIS);
#pragma config LVP = OFF, FOSC = XT, WDTE = ON, BOREN = OFF, MCLRE = OFF 

volatile static bit bat_flag = 0;
volatile bit refresh_flag = 0;
static bit usb_connected = 0;
static bit lbo_happened = 0;
static bit bli_count = 0;
static bit mod_flag = 0;
static bit battery_need_charge = 0;
unsigned char battery_need_charge_count = 0;

void init();
void Refresh();
void pwm_stby();
void pwm25();
void pwm50();
void compare();
void checkMode();
void checkUSB();
void checkLBO();
void SOS();

void main() {

//Initial configuration
init();
//pwm50();
checkMode();

CLRWDT(); 
//compare();
    for (;;) 
        
    	{
    		
           if (refresh_flag==1) Refresh();

           if (battery_need_charge)
            {
            	pwm_stby();
            }// if (battery_need_charge) */
            	else
            	{
            		if (mod_flag==0) pwm25();
                		else pwm50();
            	}
           
    	} //for ;;
    
} // main

interrupt void isr()
{
	if (TMR1IF)
	{
        refresh_flag = 1;
        TMR1H = 0x00; TMR1L = 0x00; 
        TMR1IF = 0;
        GIE = 0;
        PEIE = 0;
    }    
}

//Fucntions section

void init()
{
        BAT1 = 0;           //battery 1 enabled
        BAT2 = 1;           //battery 2 disabled (inverse logic)    

        CMCON = 0x05;   
        TRISB1 = 0;
        TRISB2 = 0;
        TRISB0 = 1;
        TRISB5 = 1;
        TRISB3 = 0;
        TRISA4 = 1;
        TRISA3 = 1;
        TRISB7 = 0;
        
        refresh_flag = 0;
        usb_connected = 0;
        bli_count = 0;
        lbo_happened = 0;
        mod_flag = 0;
        battery_need_charge = 0;
        battery_need_charge_count = 0;

        
        T2CKPS0 = 1; T2CKPS1 = 0;       //Prescaler = 4
        PR2 = 0b01111100;               //Period of PWM = 500us >> 2KHz
        CCP1M3 = 1;                     //PWM enabling
        CCP1M2 = 1;                     //PWM enabling
        TMR2ON = 1;
        
        T1CKPS1 = 1;
        T1CKPS0 = 1;                    //prescaler for tmr1 = 8
         
        T1OSCEN = 0;                    //disable internal gen
        TMR1CS = 0;                     // Fosc/4
        
        
        TMR1H = 0x00; TMR1L = 0x00;     //full scale of timer1


        GIE = 1;                        //interrupts enable
        PEIE = 1;                       //peripheral interrupts enable
        TMR1IE = 1;                     // ?????????? ?? ???????????? TMR1
        TMR1ON = 1;                     // ???????? ??????
 
}

void Refresh()
{
   //RB7 = !RB7;
    CLRWDT();
    /*-----------Inputs checking-----------------------------------------*/
    checkMode();
    checkUSB();
    checkLBO();

    
            
    /*------------------End of input checking--------------------------------*/
    CLRWDT();
    refresh_flag = 0;
    GIE = 1;
    PEIE = 1;
     
}

void pwm_stby() {
 CCPR1L = 0b00000111;
 CCP1X = 0;
 CCP1Y = 0;  
} //pwm25()

void pwm25() {
 CCPR1L = 0b00011111;
 CCP1X = 0;
 CCP1Y = 0;  
} //pwm25()
 
void pwm50() {
 CCPR1L = 0b01101101;
 CCP1X = 0;
 CCP1Y = 1; 
} //pwm50()

void compare()
{
	RB7 = C2OUT;
}

void checkMode()
{
    if (!MOD1)
            {
                __delay_ms(100) ;
                    if (!MOD1)
                    {
                        mod_flag = 0;    

                    }//if (!MOD1) int
            }//if(!MOD1)
    
            if (!MOD2)
            {
                __delay_ms(100) ;
                    if (!MOD2)
                    {
                        mod_flag = 1;

                    }//if (!MOD2) int
            }//if(!MOD2)
}

void checkUSB()
{
    if ((!USB)&&(!usb_connected))
            {
                __delay_ms(100) ;
                    if (!USB)
                    {
                        BAT1 = 1;               //both input disabled
                        BAT2 = 1;   
                        usb_connected = 1;    
                        CLRWDT(); 
                    }//if (!USB) int
            }//if(!USB)
    if ((USB)&&(usb_connected))
    {
        __delay_ms(100) ;
            if (USB)
            {
                usb_connected = 0;
                init();
                CLRWDT();
            }
    }
         
}

void checkLBO()
{
         if ((!LBO)&&(!lbo_happened))
            {
                __delay_ms(100) ;
                    if (!LBO)
                    {
                        lbo_happened = 1;       //low battery occurs
                        if (!bli_count)         //if it happened first time
                        {
                            bli_count = 1;
                            if (C2OUT)          //if BAT2 > BAT1
                            {
                                 BAT1 = 1;      //changing battery
                                 BAT2 = 0;   
                            } else              //there is no good health battery anymore
                                    {
                                        battery_need_charge = 1;    //if BAT2 < BAT1
                                        battery_need_charge_count++;
                                    }    
                         } else 
                            {
                                battery_need_charge = 1;
                                battery_need_charge_count++;
                             }    

                      
                    }//if (!LBO) int
            }//if(!LBO)
          if ((LBO)&&(lbo_happened))
          {

            lbo_happened = 0;
            //battery_need_charge = 0;
          }
}

void SOS()
{
                CCP1M3 = 0;                     //PWM disabling
                CCP1M2 = 0;                     //PWM disabling
                TMR2ON = 0;
                CLRWDT();

                 RB3 = 1;                    
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 RB3 = 0;
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 RB3 = 1;
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 RB3 = 0;
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 RB3 = 1;
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 RB3 = 0;

                CLRWDT();    

                 RB3 = 1;                    
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                                  __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 RB3 = 0;
                                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 RB3 = 1;
                                  __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 RB3 = 0;
                                  __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 RB3 = 1;
                                  __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 RB3 = 0;
                 CLRWDT();   

                 RB3 = 1;                    
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 RB3 = 0;
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 RB3 = 1;
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 RB3 = 0;
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 RB3 = 1;
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 __delay_ms(100);
                 RB3 = 0;
                 CLRWDT();

}