#include <xc.h>
#include "ShiftReg.h"
#include "Delays.h"

void ShiftClk()
{
	Clk = 0;
	DelayUs(10);
	Clk = 1;
}

void ShiftReset()
{
	Rst = 0;
	ShiftClk();
	Rst = 1;
}

void DataPush(unsigned char DataForPush[])
{

	ShiftReset();
	for (unsigned char i=0; i<5; i++)														//Разворот байта задом наперед
	{
		for (unsigned char j=0; j<8; j++)
			{
				DataOut = (DataForPush[i]>>j) & 0b00000001;
				ShiftClk();
			}//for j
		//ShiftClk();	
	}//for i
	ShiftClk();	
}

unsigned char Bin2Term(unsigned char DecNum)				//Bin to Thermometer
{
	unsigned char temp;
	switch (DecNum)
	{
		case 0 : temp = 0;
				 break;
		case 1 : temp = 1;
				 break;
		case 2 : temp = 3;
				 break;
		case 3 : temp = 7;
				 break;
		case 4 : temp = 15;
				 break;
		case 5 : temp = 31;
				 break;	 				 				 	
		case 6 : temp = 63;
				 break;
		case 7 : temp = 127;
				 break;
		case 8 : temp = 255;
				 break;	
		case 9 : temp = 255;
			     break;		 			 
	}
	return temp;
}