#ifndef _SHIFTREG_H
#define _SHIFTREG_H


#define DataOut PORTCbits.RC4
#define Clk		PORTCbits.RC5
#define Rst	PORTCbits.RC2

void ShiftClk();
void ShiftReset();
void DataPush(unsigned char DataForPush[]);											//Выталкивание конкретного числа, находящегося в DataForPush, старшее число в массиве - старший байт
unsigned char Bin2Term(unsigned char DecNum);				//Bin to Thermometer



#endif