//?�?�?� ?�?�?�???�?�?�???? ?�?????�?�?�?�?? + ?�?�?�?????�?�?????� ?�???�?�?�?�???�?�???�?? ?�?�?�?�?? ?�?�???????�???�?� ?�?�?????�?�?�?� ?�?�?� ?�???????�?�?�?�??
//?????�?�?� - ?�?�?????�???� ?�?�?�???�??
//
//Encoder proccesing code + led driving thought shift registers 74hc595, uc - PIC18F14K50
//author - Grynko Sergey
//
//site - www.diymicro.ru, email - sargein@gmail.com, skype - sargein
//

/*
?� ???�?�?� ?�?�???�?????�???????� 5 ?�?�?????�?�?? ?�?�???�?�?�???� ?�?????�

mode = 0 - ?�?�?�???�?????????????� ?�?�?�?�?�?�?�?�?????�???� ?�???�?�?? ???? ?�???�?�?????? DataForPush
mode = 1 - ?�?�???�?�???? ?�???�???�??
mode = 2 - ?�?�???�?�???? ?�???�???�?? ?� ???????�?�?�?�?????�?�
mode = 4 - ?�?�?�?�?�?????�?????� ?�?�?�?�?�?�?????�?? dataForMode3 ???? ?�?????�?�?�???�?� ?�?�?????�???�?�
mode = 5 - ?�?�???�?�?�?�?�?�



There are five modes in this code

mode = 0 - Pushing to the output content of DataForPush array
mode = 1 - Running of single led
mode = 2 - Running of leds with accumulation (maximum number of running leds is 8) 
mode = 4 - Showing content of dataForMode3 at random position
mode = 5 - Rotationg speed meter
*/


#include <xc.h>
#include <plib/usart.h>
#include <plib/delays.h>
#include <stdlib.h> 
#include "Delays.h"
#include "ShiftReg.h"
#define _XTAL_FREQ 12000000 //The speed of your internal(or)external oscillator



#pragma config WDTEN = OFF, LVP = OFF, FOSC = HS  

unsigned char UARTConfig = 0;
unsigned char baud = 0;
unsigned char DataForPush[5];
volatile bit up;
volatile bit down;
volatile bit Update = 0;
volatile unsigned char TimeStep = 0;
volatile unsigned char EncData = 3;

unsigned char mode = 0;										// 0 - default mode, 4 bytes of code; 1 - run single led; 2 - run with accumulation; 3 - speed meter
unsigned char TempData = 0;
unsigned char dataForMode3 = 0;





void NumToUart(unsigned int Num);  
void InitTimer0();
void InitTimer1();



void main()
{
 DelayMs(400);
 unsigned char SecondModeCount = 0;							//variable for second mode
 unsigned char ishift = 7;									//temp variable again for second mode
 
 /*Variables for encoder*/
 unsigned char OldEncData = 3;                    		    //?�?�?�?�?????�?� ?�?�???�?�?� ???????�?�?????� ?�????????, ???????�?????�???????�?�?�?� ?� 3
 unsigned char upcount = 0;
 unsigned char downcount = 0;
 

 ANSEL = 0;                                                                   //?�?�???�?�?�???�?� ???????�?�???�???�?� ?�?�?�?�?�??
 ANSELH = 0;

 TRISC0 = 1;
 TRISC1 = 1;
 TRISC2 = 0;
 TRISC4 = 0;
 TRISC5 = 0;

 up = 0;
 down = 0;
 Rst = 1;
 Clk = 0;
 DataOut = 0;
 mode = 2;
 dataForMode3 = 9;

 UARTConfig = USART_TX_INT_OFF & USART_RX_INT_OFF & USART_ASYNCH_MODE & USART_EIGHT_BIT & USART_BRGH_HIGH ;    //?�?�?�?�?�?????????� ???�???�?�?�?�???�, ???�?????�?�?�?????�?? ?�?�?????�, 8 ?�???�, ???�?�?�???�?�???�?�?�?�?�???�?? ?�?�?????�
 baud = 77;                 //Focs/(9600*16) - 1
 OpenUSART(UARTConfig,baud);
 
 
 putsUSART( (char *) "MAC Welcome to Diymicro.ru!!!\r\n" );

 DataForPush[0] = 2;
 DataForPush[1] = 0;
 DataForPush[2] = 0;
 DataForPush[3] = 0;
 DataForPush[4] = 3;

 InitTimer0();

 GIE = 1;
 PEIE = 1;
 TMR0IE = 1;
 TMR1IE = 1; // ���������� �� ������������ TMR1
 TMR1ON = 1; // �������� ������

 InitTimer1();
 

 while(1)
 {
 	/*if (!PORTCbits.RC0)
 	{
 		DelayMs(100);
 		if (!PORTCbits.RC0)
 			up = 1;
 	}

 	if (!PORTCbits.RC1)
 	{
 		DelayMs(100);
 		if (!PORTCbits.RC1)
 			down = 1;
 	}*/


 if (OldEncData != EncData) 
  {                       //?�?�?�?? ???�???�?� ???????�?�?????� ?�?�?�???�???�?�?�?� ?�?� ?�?�???�?�???�
    switch (OldEncData) {
             case 0 : if (EncData == 1) {upcount++; downcount=0; }
                      if (EncData == 2) {downcount++; upcount = 0; }
                      break;
             case 1 : if (EncData == 3) {upcount++; downcount=0; }
                      if (EncData == 0) {downcount++; upcount = 0; }
                      break;
             case 2 : if (EncData == 0) {upcount++; downcount=0; }
                      if (EncData == 3) {downcount++; upcount = 0; }
                      break;
             case 3 : if (EncData == 2) {upcount++; downcount=0; }
                      if (EncData == 1) {downcount++; upcount = 0; }
                      break;
                       }
  OldEncData = EncData;                           //?�?�???�?�?�?� ???????�?�?????� = ?�?�???�?�?� ???????�?�?????�
  }


 if (Update)
 {
 	putsUSART((char *)"\r\n Update!");
 	NumToUart(upcount+downcount);
 	Update = 0;
 	upcount = 0;
 	downcount = 0;
 }


 	if ( (up) || (down) )			//Encoder event handler
 	{
 		if (mode == 0)				//?�?�?�?�?�?� ???�?????�?�?�???�?� ?�???�?�?�
 		{
 			 	if (up)
 				{
 					DataForPush[0] = 129;
 					up = 0;
 					DataPush(DataForPush);
 				}

 				if (down)
 				{
 					DataForPush[0] = 255;
 					down = 0;
 					DataPush(DataForPush);
 				}
 		}//mode 0
 		if (mode == 1)				//?�?�???�?�???? ?�???�???�??
 		{
 			    
 			 	if (up)
 				{
 					if (TempData == 40)
 						TempData = 1;
 					else
 						TempData++;
 					up = 0; 				
 				}

 				if (down)
 				{
 					
 					if (TempData == 1)
 						TempData = 40;
 					else
 						TempData--;
 					down = 0;
 				} 	

				if ((TempData>0)&&(TempData<=8))
				{
					 unsigned char powtemp = 1;
    				 for (unsigned char i = 0;i<=(7-TempData);i++)
					 		powtemp *= 2;

					 DataForPush[0] = 0;
					 DataForPush[1] = 0;
					 DataForPush[2] = 0;
					 DataForPush[3] = 0;
					 DataForPush[4] = powtemp;
					 powtemp = 1;

				}	
					else
					{
						if ((TempData>8)&&(TempData<=16))
						{
							 unsigned char powtemp = 1;
							 for (unsigned char i = 0;i<=(15-TempData);i++)
							 	powtemp *= 2;
							 DataForPush[0] = 0;
							 DataForPush[1] = 0;
							 DataForPush[2] = 0;
							 DataForPush[3] = powtemp;
							 DataForPush[4] = 0;
							 powtemp = 1;

						}	
						else
						{
							if ((TempData>16)&&(TempData<=24))
							{
								 unsigned char powtemp = 1;
								 for (unsigned char i = 0;i<=(23-TempData);i++)
								 	powtemp *= 2;
								 DataForPush[0] = 0;
								 DataForPush[1] = 0;
								 DataForPush[2] = powtemp;
								 DataForPush[3] = 0;
								 DataForPush[4] = 0;
								 powtemp = 1;

							}	
							else
							{
								if ((TempData>24)&&(TempData<=32))
								{
									 unsigned char powtemp = 1;
									 for (unsigned char i = 0;i<=(31-TempData);i++)
									 	powtemp *= 2;
									 DataForPush[0] = 0;
									 DataForPush[1] = powtemp;
									 DataForPush[2] = 0;
									 DataForPush[3] = 0;
									 DataForPush[4] = 0;
									 powtemp = 1;
	
								}
								else
								{
									if ((TempData>32)&&(TempData<=40))
									{
										 unsigned char powtemp = 1;
										 for (unsigned char i = 0;i<=(39-TempData);i++)
										 	powtemp *= 2;
										 DataForPush[0] = powtemp;
										 DataForPush[1] = 0;
										 DataForPush[2] = 0;
										 DataForPush[3] = 0;
										 DataForPush[4] = 0;
										 powtemp = 1;
		
									}
								}//else 4
							}//else 3
						}//else 2
					}//else 1

				DataPush(DataForPush);
 		}// mode 1
 		if (mode == 2)				//?�?�???�?�???? ?�???�???�?? ?� ???????�?�?�?�?????�?� ?�?� 8
 		{
 			 	if (up)
 				{
 					if (TempData == 40)
 					{
 						if (SecondModeCount < 7)
 							SecondModeCount++;
 						else 
 							SecondModeCount = 7;
 						TempData = 1;
 					}
 					else
 						TempData++;
 					up = 0; 

 				}

 				if (down)
 				{
 					
 					if (TempData == 1)
 					{
 						if (SecondModeCount > 0 )
 							SecondModeCount--;
 						else
 							SecondModeCount = 0;
 						TempData = 40;
 					}
 					else
 						TempData--;
 					down = 0;
 				} 	
 				//NumToUart(TempData);
 				//putsUSART( (char *) "\r\n" );	
 				unsigned char TempShift = Bin2Term(SecondModeCount);
 				
				if ((TempData>0)&&(TempData<=8))
				{
					 unsigned char powtemp = 1;
    				 for (unsigned char i = 0;i<=(7-TempData);i++)
					 		powtemp *= 2;

					 DataForPush[0] = 0;
					 DataForPush[1] = 0;
					 DataForPush[2] = 0;
					 if (powtemp <= TempShift)
						{
							DataForPush[3] = TempShift<<ishift;
							ishift--;
						}
					 else
					  	 {
					  		 DataForPush[3] = 0;	
					  		 ishift = 7;
					  	 }
					 DataForPush[4] = ((powtemp >> SecondModeCount) | ( (TempShift<<(8-SecondModeCount)) >> (TempData-1) ));
					 powtemp = 1;

				}	
				else
				{
						if ((TempData>8)&&(TempData<=16))
						{
							 unsigned char powtemp = 1;
							 for (unsigned char i = 0;i<=(15-TempData);i++)
							 	powtemp *= 2;
							 DataForPush[0] = 0;
							 DataForPush[1] = 0;
							 if (powtemp <= TempShift)
								{
									DataForPush[2] = TempShift<<ishift;
									ishift--;
								}
							 else
							  	 {
							  		 DataForPush[2] = 0;	
							  		 ishift = 7;
							  	 }							 
							 DataForPush[3] = ((powtemp >> SecondModeCount) | ( (TempShift<<(8-SecondModeCount)) >> (TempData-9) ));
							 DataForPush[4] = 0;
							 powtemp = 1;

						}	
						else
						{
							if ((TempData>16)&&(TempData<=24))
							{
								 unsigned char powtemp = 1;
								 for (unsigned char i = 0;i<=(23-TempData);i++)
								 	powtemp *= 2;
								 DataForPush[0] = 0;
								 if (powtemp <= TempShift)
									{
										DataForPush[1] = TempShift<<ishift;
										ishift--;
									}
								 else
								  	 {
								  		 DataForPush[1] = 0;	
								  		 ishift = 7;
								  	 }
								 DataForPush[2] = ((powtemp >> SecondModeCount) | ( (TempShift<<(8-SecondModeCount)) >> (TempData-17) ) );
								 DataForPush[3] = 0;
								 DataForPush[4] = 0;
								 powtemp = 1;

							}	
							else
							{
								if ((TempData>24)&&(TempData<=32))
								{
									 unsigned char powtemp = 1;
									 for (unsigned char i = 0;i<=(31-TempData);i++)
									 	powtemp *= 2;
										if (powtemp <= TempShift)
											{
												DataForPush[0] = TempShift<<ishift;
												ishift--;
											}
										 else
										  	 {
										  		 DataForPush[0] = 0;	
										  		 ishift = 7;
										  	 }
									 DataForPush[1] = ((powtemp >> SecondModeCount) | ( (TempShift<<(8-SecondModeCount)) >> (TempData-25) ));
									 DataForPush[2] = 0;
									 DataForPush[3] = 0;
									 DataForPush[4] = 0;
									 powtemp = 1;
	
								}
								else
								{
									if ((TempData>32)&&(TempData<=40))
									{
										 unsigned char powtemp = 1;
										 for (unsigned char i = 0;i<=(39-TempData);i++)
										 	powtemp *= 2;
										 DataForPush[0] = ((powtemp >> SecondModeCount) | ( (TempShift<<(8-SecondModeCount)) >> (TempData-33) ));
										 DataForPush[1] = 0;
										 DataForPush[2] = 0;
										 DataForPush[3] = 0;
										 if (powtemp <= TempShift)
											{
												DataForPush[4] = TempShift<<ishift;
												ishift--;
											}
										 else
										  	 {
										  		 DataForPush[4] = 0;	
										  		 ishift = 7;
										  	 }
										 powtemp = 1;
		
									}
								}//else 4
							}//else 3
						}//else 2
				}//else 1	

				DataPush(DataForPush);
 		}//mode 2
 		if (mode == 3)
 		{
 			
 			for (unsigned char i =0; i<5; i++)
 				DataForPush[i] = 0;

 			unsigned char ThirdModeTemp1, ThirdModeTemp2;
 			ThirdModeTemp1 = rand() % 4;
 			do
 			{
 				ThirdModeTemp2 = rand() % 4;
 			}while(ThirdModeTemp2 == ThirdModeTemp1);

 			if ((up) || (down))
 			{
 				DataForPush[ThirdModeTemp1] = Bin2Term(dataForMode3);
 				if (dataForMode3 > 8)
 					DataForPush[ThirdModeTemp2] = Bin2Term(1);
 				
 				
 				up = 0;
 				down = 0;
 			}

 			DataPush(DataForPush);
 		}

 	}//if ( (up) || (down) )




 }//while1

}//main()

void interrupt isr()
{
 if (TMR0IF)
  {
    EncData = PORTCbits.RC0 | (PORTCbits.RC1 << 1);
   
    InitTimer0();
    TMR0IF = 0;
  }

  if (TMR1IF)
  {
  	if (TimeStep < 6)
  		TimeStep++;
  	else
  	{
  		Update = 1;
  		TimeStep = 0;
  	}	
  	InitTimer1();
  	TMR1IF = 0;
  }
}


void NumToUart(unsigned int Num)                            //Number to uart
{
 
 
  unsigned int bignum = 10000;
  unsigned char numtemp = 5;
 
  if (!Num)
  {
      WriteUSART('0');         //?�?�?�???�?????�?????�?� ???�?� ?�?????�?�?�?� - ?�?� ?�?�???�?�?�???� ?? ?�?�???�?�?�?�?�
      while(BusyUSART());                                                       //?�?�?�?� ?�?�???? ?�?�???�?�?�?�???�?�?� ?�?�?�?�?�?� ???????�?� ?�?�?�?�?� ?�?�?�?�?�?�?�?�?�
  }
  else 
  {
    while(numtemp>0)                                                             //?�?�?�?�?�?�?�?�?�?� ?�???�?�?�???� ?�?????�?�?�?�?? ???�?�?�?� ?????�?� ?�???�?�?�
    {
      if (Num/bignum)
          break;
      numtemp--;
      bignum = bignum / 10;  
    }  
  
  
  
    for (unsigned char i = numtemp; i>0; i--)
      {
        WriteUSART( (Num - (Num/(bignum*10))*bignum*10 )/bignum + '0');         //?�?�?�???�?????�?????�?� ???�?� ?�?????�?�?�?� - ?�?� ?�?�???�?�?�???� ?? ?�?�???�?�?�?�?�
        while(BusyUSART());                                                       //?�?�?�?� ?�?�???? ?�?�???�?�?�?�???�?�?� ?�?�?�?�?�?� ???????�?� ?�?�?�?�?� ?�?�?�?�?�?�?�?�?�
        bignum = bignum/10;
      }
   } 
 
}
 
void InitTimer0()                                           //Timer0 initialization, 8bit mode, prescaler = 16 - period 1ms
{
  TMR0 = 67;                                                //188*16*333.33ns approx 1ms
  T0CON = 0b11010011;
 
} 

void InitTimer1()
{
	T1CKPS1 = 1;
	T1CKPS0 = 1; // ������������ = 8
 
	T1OSCEN = 0; //��������� ���������� ���������
	TMR1CS = 0; // Fosc/4

	TMR1H = 0; TMR1L = 0;
}
