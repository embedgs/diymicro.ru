#include <htc.h>


#define _XTAL_FREQ 1000000

__CONFIG(WDTEN & UNPROTECT & LVPDIS & INTIO & MCLREN & BORDIS);

const unsigned char *st1 = "KOMNATA TRISTA TRIDCATX TRI KOD NA BETONNOJ POVERHNOSTI";   //FALSE
const unsigned char *st2 = "KOMNATA STO ODINADCATX KOD NA OKNE";  //FALSE
const unsigned char *st3 = "WTOROJ 6TAV KOD NA TRUBE";    //TRUE
const unsigned char *st4 = "KOMNATA NOLX DVENADCATX KOD NA STENE";    //FALSE
const unsigned char *st5 = "KOMNATA TRISTA P8TNADCATX KOD NA POLU";   //TRUE
const unsigned char *st6 = "WTOROJ 6TAV KOD NA HUE";    //FALSE
const unsigned char *st7 = "KOMNATA NOLX P8TX KOD NA KIRPI4E";    //TRUE
const unsigned char *st8 = "TRETIJ 6TAV KOD NA BALKONE";    //TRUE
const unsigned char *st9 = "KOMNATA NOLX SEMNADCATX KOD NA POTOLKE";    //TRUE
const unsigned char *st10 = "KOMNATA TRISTA 5ESTNADCATX KOD NA BETONNOJ POVERHNOSTI";   //FALSE
const unsigned char *st11 = "KOMNATA NOLX TRI KOD NA VELEZNOJ POVERHNOSTI";   //FALSE
const unsigned char *st12 = "WTOROJ 6TAV KOD NA PUZYRE";    //TRUE
  
const unsigned char *st15 = "KOD PERWYJ STOLBEC";

void point();
void dash();
void SymbolCase(unsigned char ch);
void ShowString(unsigned char *st);


void main()
{
	unsigned char temp = 0;
   CMCON = 0x07;
	 TRISB = 0xFF;
         PSA = 1;
         PS2 = 1;
         PS1 = 1;
         PS0 = 1;

   TRISB7 = 0;
   TRISB6 = 0;
   POR = 0;
   BOR = 0;
   RB6 = 1;
	 __delay_ms(200);

   temp = PORTB;
   temp &= 0b00001111; 
        // RB7 = 1;
        // RB6 = 1;
        // RB5 = 1;
        // RB4 = 1;
     //ShowString(st1);
         CLRWDT();
   
             switch (temp)
               {
                      case 0 : ShowString(st1);
                               break;
                      case 1 : ShowString(st2);
                               break;
                      case 2 : ShowString(st3);
                               break;
  l,                        case 3 : ShowString(st4);
                               break;
                      case 4 : ShowString(st5);
                               break;
                      case 5 : ShowString(st6);
                               break;
                      case 6 : ShowString(st7);
                               break;
                      case 7 : ShowString(st8);
                               break;
                      case 8 : ShowString(st9);
                               break;
                      case 9 : ShowString(st10);
                               break;
                      case 10 : ShowString(st11);
                               break;
                      case 11 : ShowString(st12);
                               break;             
                      case 12 : RB7 = 1;
                               break;
                      case 13 : RB7 = 1;
                               break;
                      case 14 : RB7 = 1;
                               break;
                      case 15 : ShowString(st15);
                               break;
               } 
       
               CLRWDT();
               RB6 = 0;
               __delay_ms(200) ;

for (;;)
{};
             //point();
             //dash();
             //dash();


}


void point()
{
    
    RB7 = 1;
    for (unsigned i = 0; i<=4; i++)
    {
        __delay_ms(200);
        CLRWDT();
    }
    RB7 = 0;
    for (unsigned i = 0; i<=4; i++)
    {
       __delay_ms(200);
       CLRWDT();
    }

}


void dash()
{
    CLRWDT();
    RB7 = 1;
    for (unsigned i = 0; i<=19; i++)
    {    __delay_ms(200);
         CLRWDT();
    }
    RB7 = 0;
    for (unsigned i = 0; i<=4; i++)
    {
        __delay_ms(200);
         CLRWDT();
    }


}


void SymbolCase(unsigned char ch)          //Выборка символа и перевод в азбуку морзе
{
    switch (ch)
    {
        case 'A' : point();                // .- А
                   dash();
                   break;

        case 'B' : dash();                // -... Б
                   point();
                   point();
                   point();
                   break;     

        case 'W' : point();                // .-- В
                   dash();
                   dash();
                   break;    

        case 'G' : dash();                // --. Г
                   dash();
                   point();
                   break;          

        case 'D' : dash();                // -.. Д
                   point();
                   point();
                   break;    

        case 'E' : point();                // . E
                   break;               

        case 'V' : point();                // ...- Ж
                   point();
                   point();
                   dash();
                   break;       

        case 'Z' : dash();                // --.. З
                   dash();
                   point();
                   point();
                   break;        

        case 'I' : point();                // .. И
                   point();
                   break;        

        case 'J' : point();                // .--- Й
                   dash();
                   dash();
                   dash();
                   break;      

        case 'K' : dash();                // -.- K
                   point();
                   dash();
                   break;                                                                                                                                   

        case 'L' : point();                // .-.. Л
                   dash();
                   point();
                   point();
                   break;    

        case 'M' : dash();                // -- M
                   dash();
                   break;       

        case 'N' : dash();                // -. Н
                   point();
                   break;                                                             

        case 'O' : dash();                // --- O
                   dash();
                   dash();
                   break;     

        case 'P' : point();                // .--. П
                   dash();
                   dash();
                   point();
                   break;    

        case 'R' : point();                // .-. Р
                   dash();
                   point();
                   break;     

        case 'S' : point();                // ... С
                   point();
                   point();
                   break;         

        case 'T' : dash();                // - T
                   break;   

        case 'U' : point();                // ..- У
                   point();
                   dash();
                   break;     

        case 'F' : point();                // ..-. Ф
                   point();
                   dash();
                   point();
                   break;           

        case 'H' : point();                // .... Х
                   point();
                   point();
                   point();
                   break;                                                                                                                     

        case 'C' : dash();                // -.-. Ц
                   point();
                   dash();
                   point();
                   break;          

        case '4' : dash();                // ---. Ч
                   dash();
                   dash();
                   point();
                   break;        

        case '5' : dash();                // ---- Ш
                   dash();
                   dash();
                   dash();
                   break;                                                    

        case 'Q' : dash();                // --.- Щ
                   dash();
                   point();
                   dash();
                   break;                 

        case 'Y' : dash();                // -.-- Ы
                   point();
                   dash();
                   dash();
                   break;          

        case 'X' : dash();                // -..- Ь
                   point();
                   point();
                   dash();
                   break;      

        case '6' : point();                // ..-.. Э
                   point();
                   dash();
                   point();
                   point();
                   break;                                                                                              

        case '7' : point();                // ..-- Ю
                   point();
                   dash();
                   dash();
                   break;      

        case '8' : point();                // .-.- Я
                   dash();
                   point();
                   dash();
                   break;                                    


    }
}


void ShowString(unsigned char *st)
{
  while (*st)
         {

            SymbolCase(*st);    
            *st++;
            for (unsigned i = 0; i<=29; i++)
            {
                __delay_ms(200);
                 CLRWDT();
            }

         }
}