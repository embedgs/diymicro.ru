#include <htc.h>
#include <stdio.h>
#include "usart.h"

#define _XTAL_FREQ 4000000
#define ON RA0
#define IRpin RB0
#define FAN RA1

volatile static bit direction; 		//1 - rising, 0 - falling
volatile unsigned char IRbyte;  	//�������� ����
volatile bit togglebit;         	//��� ������������
volatile bit oldtogglebit; 		//�������� ������� �������� ����-������������	
volatile bit buffer;	   	
volatile unsigned char bytecount;
volatile unsigned char i=0;
volatile unsigned short long tick = 0;
volatile unsigned short long tickCount = 0;
volatile unsigned short long time = 0;

__CONFIG(MCLRDIS & LVPDIS & WDTDIS & UNPROTECT & XT);

void StartTimer(void);
void StartTimer2(void);



void main() {

unsigned int delta = 4596; 			//���������� ������ ��� ���������� ������� - 4596 ~ 5 �����

	
	FAN = 0;
	CMCON = 0x07;
	TRISA = 0x00;
	TRISB = 0b00001001;   // ���� � �� �����
	PORTB = 0x00;   // ��������� ��� ������ ����� �
	PORTA = 0x00;
	ON = 0;
	PORTB = 0x00;
	init_comms();
	GIE = 1;
	PEIE = 1;
	
	buffer = 0;
IRbyte = 0;
bytecount = 0;
direction = 0;
INTEDG = 0;		

	
	INTE = 1;
	printf("Starting done! \r\n");
	SLEEP();  //��� ���������, �������� ����� �� ����� ��� �������
        for (;;) 
	  {      // ��������� ����������� ����
	  

//---------------��������� �� ������---------------
	      if (bytecount>13)
		{
		GIE = 0;
		
		//printf("\r\nToggle is %d", togglebit);	
		
		if (oldtogglebit != togglebit)
		{
		printf("\r\nPressed button is %d", IRbyte);
		switch(IRbyte) 
		{
			case 31 : ON = !ON;
					  if (!ON) 
						{
							time = 0;
							FAN = 0; //���� ��������� ����������� ����������, �� ��������� ���� �����������
							PORTB &= 0b00001111;
							TMR2ON = 0; TMR2IE = 0;
							SLEEP();
						}	
				      break;
			case 15 : if ((time < 16)&&(ON)) 
						{
							FAN = 1;
							time++;
							unsigned char temp = time << 4;
							tickCount = delta*time;		//�������� 5 ����� * �� ������ ����������
							printf("Tickcount %d", tickCount);
							PORTB &= 0b00001111;
							PORTB |= temp;
							tick = 0;
							StartTimer2();
						} else if (ON)
							{
								time = 0;
								PORTB &= 0b00001111;
							}		
					  break;
			case 37 : time = 0;
				  
					  PORTB &= 0b00001111;
					  break;
			
			case 23	: if (ON) FAN = !FAN;
				    else FAN = 0;
				  break;
					  
					  
		}
		}
				IRbyte = 0;
				bytecount=0;
				INTEDG = 0;
				buffer = 0;
		oldtogglebit = togglebit;
		GIE = 1;		
		}
//---------------����� ��������� IR ������---------------

if ((tick%delta == 0)&&(tick>0))
{
	  unsigned char temp = (time - (tick/delta)) << 4;
	  PORTB &= 0b00001111;
	  PORTB |= temp;
	  
} 






	  }

}



interrupt isr() {

//-------------------IR decoding-------------------
if (INTF)
	{
		
		if (bytecount<1) 
			{ 
			direction = 1;
			INTEDG = 1;
			StartTimer();
			bytecount++;
			
			
			} else 
				{
				direction = !direction;
				//if (bytecount<3) {
				//if (((direction)&&(buffer))||(!(direction)&&!(buffer))) 
				if (direction==buffer)
				{
			    	StartTimer();
					bytecount++;

				}
			//	}
				INTEDG = direction;
				
				
				}
		INTF = 0;		
		
		}
	else {
	if (TMR1IF)
		{
			
		buffer = IRpin;
		TMR1ON = 0;
		TMR1IF = 0;
		if (bytecount==2) togglebit = buffer;
		if ((bytecount>7)&&(bytecount<14))   //�������� ������ �������
			{
				IRbyte <<= 1;
				IRbyte |= buffer;
			}
			
		
	
		}
}


if (TMR2IF) 
{
    GIE = 0;
    
    if (tick < tickCount) 
    {
      tick++;
	//	printf("\r\ntick %d", tick);
      
      TMR2IF = 0;
      StartTimer2();
    }  
	else 
	{
	    tick = 0;
	    FAN = 0;
	    ON = 0;
	    TMR2ON = 0;
	    TMR2IE = 0;
	    tickCount = 0;
		time = 0;
		PORTB &= 0b00001111;
	    
	}  
    TMR2IF = 0;
    GIE = 1;
  
}  


}
//--------------------End IR---------------------




/* ---------------------------------------------�������--------------------------------------------*/
void StartTimer(void)
{
	T1CKPS1 = 0;
	T1CKPS0 = 0; 
	
	T1OSCEN = 0; //��������� ���������� ���������
	TMR1CS = 0; // Fosc/4
	TMR1IE = 1; // ���������� �� ������������ TMR1
 	TMR1H = 0b11111011; TMR1L = 0b00010110;	   //1.257ms
	//TMR1H = 0b00000000; TMR1L = 0x00;
	TMR1ON = 1; // �������� ������
}

void StartTimer2(void)
{
		//printf("\r\nStart timer");
      TMR2 = 0x00; //�������� � 0
      PR2 = 0xFF;  //������� �� 255*16 ���
      T2CKPS0 = 1; T2CKPS1 = 1; //1 �������� ��������
      TOUTPS2 = 1; TOUTPS1 = 1; TOUTPS3 = 1; TOUTPS0 = 1; //2 �������� ��������
      TMR2IE = 1;
      TMR2ON = 1; // ������ �������!
}  
