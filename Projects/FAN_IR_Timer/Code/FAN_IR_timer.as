opt subtitle "HI-TECH Software Omniscient Code Generator (PRO mode) build 6446"

opt pagewidth 120

	opt pm

	processor	16F628A
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 21 "W:\PIC\Fan_IR_Timer\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 21 "W:\PIC\Fan_IR_Timer\main.c"
	dw 0x3FDF & 0x3F7F & 0x3FFB & 0x3FFF & 0x3FED ;#
	FNCALL	_main,_printf
	FNCALL	_main,___tmul
	FNCALL	_main,_StartTimer2
	FNCALL	_main,___ltmod
	FNCALL	_main,___ltdiv
	FNCALL	_printf,_putch
	FNCALL	_printf,___lwdiv
	FNCALL	_printf,___lwmod
	FNROOT	_main
	FNCALL	_isr,_StartTimer
	FNCALL	_isr,i1_StartTimer2
	FNCALL	intlevel1,_isr
	global	intlevel1
	FNROOT	intlevel1
	global	_dpowers
psect	strings,class=STRING,delta=2
global __pstrings
__pstrings:
;	global	stringdir,stringtab,__stringbase
stringtab:
;	String table - string pointers are 1 byte each
stringcode:stringdir:
movlw high(stringdir)
movwf pclath
movf fsr,w
incf fsr
	addwf pc
__stringbase:
	retlw	0
psect	strings
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\lib\doprnt.c"
	line	350
_dpowers:
	retlw	01h
	retlw	0

	retlw	0Ah
	retlw	0

	retlw	064h
	retlw	0

	retlw	0E8h
	retlw	03h

	retlw	010h
	retlw	027h

	global	_dpowers
	global	_tick
	global	_tickCount
	global	_time
	global	_IRbyte
	global	_bytecount
	global	_i
	global	_buffer
	global	_direction
	global	_oldtogglebit
	global	_togglebit
	global	_CMCON
_CMCON	set	31
	global	_PORTA
_PORTA	set	5
	global	_PORTB
_PORTB	set	6
	global	_RCSTA
_RCSTA	set	24
	global	_TMR1H
_TMR1H	set	15
	global	_TMR1L
_TMR1L	set	14
	global	_TMR2
_TMR2	set	17
	global	_TXREG
_TXREG	set	25
	global	_GIE
_GIE	set	95
	global	_INTE
_INTE	set	92
	global	_INTF
_INTF	set	89
	global	_PEIE
_PEIE	set	94
	global	_RA0
_RA0	set	40
	global	_RA1
_RA1	set	41
	global	_RB0
_RB0	set	48
	global	_T1CKPS0
_T1CKPS0	set	132
	global	_T1CKPS1
_T1CKPS1	set	133
	global	_T1OSCEN
_T1OSCEN	set	131
	global	_T2CKPS0
_T2CKPS0	set	144
	global	_T2CKPS1
_T2CKPS1	set	145
	global	_TMR1CS
_TMR1CS	set	129
	global	_TMR1IF
_TMR1IF	set	96
	global	_TMR1ON
_TMR1ON	set	128
	global	_TMR2IF
_TMR2IF	set	97
	global	_TMR2ON
_TMR2ON	set	146
	global	_TOUTPS0
_TOUTPS0	set	147
	global	_TOUTPS1
_TOUTPS1	set	148
	global	_TOUTPS2
_TOUTPS2	set	149
	global	_TOUTPS3
_TOUTPS3	set	150
	global	_TXIF
_TXIF	set	100
	global	_PR2
_PR2	set	146
	global	_SPBRG
_SPBRG	set	153
	global	_TRISA
_TRISA	set	133
	global	_TRISB
_TRISB	set	134
	global	_TXSTA
_TXSTA	set	152
	global	_INTEDG
_INTEDG	set	1038
	global	_TMR1IE
_TMR1IE	set	1120
	global	_TMR2IE
_TMR2IE	set	1121
	global	_TRISB1
_TRISB1	set	1073
	global	_TRISB2
_TRISB2	set	1074
	
STR_2:	
	retlw	13
	retlw	10
	retlw	80	;'P'
	retlw	114	;'r'
	retlw	101	;'e'
	retlw	115	;'s'
	retlw	115	;'s'
	retlw	101	;'e'
	retlw	100	;'d'
	retlw	32	;' '
	retlw	98	;'b'
	retlw	117	;'u'
	retlw	116	;'t'
	retlw	116	;'t'
	retlw	111	;'o'
	retlw	110	;'n'
	retlw	32	;' '
	retlw	105	;'i'
	retlw	115	;'s'
	retlw	32	;' '
	retlw	37	;'%'
	retlw	100	;'d'
	retlw	0
psect	strings
	
STR_1:	
	retlw	83	;'S'
	retlw	116	;'t'
	retlw	97	;'a'
	retlw	114	;'r'
	retlw	116	;'t'
	retlw	105	;'i'
	retlw	110	;'n'
	retlw	103	;'g'
	retlw	32	;' '
	retlw	100	;'d'
	retlw	111	;'o'
	retlw	110	;'n'
	retlw	101	;'e'
	retlw	33	;'!'
	retlw	32	;' '
	retlw	13
	retlw	10
	retlw	0
psect	strings
	
STR_3:	
	retlw	84	;'T'
	retlw	105	;'i'
	retlw	99	;'c'
	retlw	107	;'k'
	retlw	99	;'c'
	retlw	111	;'o'
	retlw	117	;'u'
	retlw	110	;'n'
	retlw	116	;'t'
	retlw	32	;' '
	retlw	37	;'%'
	retlw	100	;'d'
	retlw	0
psect	strings
	file	"FAN_IR_timer.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bitbssCOMMON,class=COMMON,bit,space=1
global __pbitbssCOMMON
__pbitbssCOMMON:
_buffer:
       ds      1

_direction:
       ds      1

_oldtogglebit:
       ds      1

_togglebit:
       ds      1

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_i:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_tick:
       ds      3

_tickCount:
       ds      3

_time:
       ds      3

_IRbyte:
       ds      1

_bytecount:
       ds      1

; Clear objects allocated to BITCOMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbitbssCOMMON/8)+0)&07Fh
; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
	clrf	((__pbssBANK0)+1)&07Fh
	clrf	((__pbssBANK0)+2)&07Fh
	clrf	((__pbssBANK0)+3)&07Fh
	clrf	((__pbssBANK0)+4)&07Fh
	clrf	((__pbssBANK0)+5)&07Fh
	clrf	((__pbssBANK0)+6)&07Fh
	clrf	((__pbssBANK0)+7)&07Fh
	clrf	((__pbssBANK0)+8)&07Fh
	clrf	((__pbssBANK0)+9)&07Fh
	clrf	((__pbssBANK0)+10)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_StartTimer2
?_StartTimer2:	; 0 bytes @ 0x0
	global	?_StartTimer
?_StartTimer:	; 0 bytes @ 0x0
	global	??_StartTimer
??_StartTimer:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 0 bytes @ 0x0
	global	??_isr
??_isr:	; 0 bytes @ 0x0
	global	?_putch
?_putch:	; 0 bytes @ 0x0
	global	?i1_StartTimer2
?i1_StartTimer2:	; 0 bytes @ 0x0
	global	??i1_StartTimer2
??i1_StartTimer2:	; 0 bytes @ 0x0
	global	?_isr
?_isr:	; 2 bytes @ 0x0
	ds	3
	global	??_StartTimer2
??_StartTimer2:	; 0 bytes @ 0x3
	global	??_putch
??_putch:	; 0 bytes @ 0x3
	global	?___lwdiv
?___lwdiv:	; 2 bytes @ 0x3
	global	?___ltdiv
?___ltdiv:	; 3 bytes @ 0x3
	global	?___ltmod
?___ltmod:	; 3 bytes @ 0x3
	global	?___tmul
?___tmul:	; 3 bytes @ 0x3
	global	putch@byte
putch@byte:	; 1 bytes @ 0x3
	global	___lwdiv@divisor
___lwdiv@divisor:	; 2 bytes @ 0x3
	global	___ltdiv@divisor
___ltdiv@divisor:	; 3 bytes @ 0x3
	global	___ltmod@divisor
___ltmod@divisor:	; 3 bytes @ 0x3
	global	___tmul@multiplier
___tmul@multiplier:	; 3 bytes @ 0x3
	ds	2
	global	___lwdiv@dividend
___lwdiv@dividend:	; 2 bytes @ 0x5
	ds	1
	global	___ltdiv@dividend
___ltdiv@dividend:	; 3 bytes @ 0x6
	global	___ltmod@dividend
___ltmod@dividend:	; 3 bytes @ 0x6
	global	___tmul@multiplicand
___tmul@multiplicand:	; 3 bytes @ 0x6
	ds	1
	global	??___lwdiv
??___lwdiv:	; 0 bytes @ 0x7
	global	??___lwmod
??___lwmod:	; 0 bytes @ 0x7
	global	___lwmod@counter
___lwmod@counter:	; 1 bytes @ 0x7
	ds	2
	global	??___ltdiv
??___ltdiv:	; 0 bytes @ 0x9
	global	??___ltmod
??___ltmod:	; 0 bytes @ 0x9
	global	??___tmul
??___tmul:	; 0 bytes @ 0x9
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	___ltmod@counter
___ltmod@counter:	; 1 bytes @ 0x0
	global	___lwdiv@quotient
___lwdiv@quotient:	; 2 bytes @ 0x0
	global	___ltdiv@quotient
___ltdiv@quotient:	; 3 bytes @ 0x0
	global	___tmul@product
___tmul@product:	; 3 bytes @ 0x0
	ds	2
	global	___lwdiv@counter
___lwdiv@counter:	; 1 bytes @ 0x2
	ds	1
	global	?___lwmod
?___lwmod:	; 2 bytes @ 0x3
	global	___ltdiv@counter
___ltdiv@counter:	; 1 bytes @ 0x3
	global	___lwmod@divisor
___lwmod@divisor:	; 2 bytes @ 0x3
	ds	2
	global	___lwmod@dividend
___lwmod@dividend:	; 2 bytes @ 0x5
	ds	2
	global	?_printf
?_printf:	; 2 bytes @ 0x7
	ds	3
	global	??_printf
??_printf:	; 0 bytes @ 0xA
	ds	2
	global	printf@ap
printf@ap:	; 1 bytes @ 0xC
	ds	1
	global	printf@flag
printf@flag:	; 1 bytes @ 0xD
	ds	1
	global	printf@f
printf@f:	; 1 bytes @ 0xE
	ds	1
	global	printf@prec
printf@prec:	; 1 bytes @ 0xF
	ds	1
	global	printf@_val
printf@_val:	; 4 bytes @ 0x10
	ds	4
	global	printf@c
printf@c:	; 1 bytes @ 0x14
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0x15
	ds	1
	global	main@temp
main@temp:	; 1 bytes @ 0x16
	ds	1
	global	main@temp_209
main@temp_209:	; 1 bytes @ 0x17
	ds	1
	global	main@delta
main@delta:	; 2 bytes @ 0x18
	ds	2
;;Data sizes: Strings 54, constant 10, data 0, bss 12, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      9      11
;; BANK0           80     26      37
;; BANK1           80      0       0
;; BANK2           48      0       0

;;
;; Pointer list with targets:

;; ?___lwdiv	unsigned int  size(1) Largest target is 0
;;
;; ?___lwmod	unsigned int  size(1) Largest target is 0
;;
;; ?___ltdiv	unsigned um size(1) Largest target is 0
;;
;; ?___ltmod	unsigned um size(1) Largest target is 0
;;
;; ?___tmul	unsigned um size(1) Largest target is 0
;;
;; printf@f	PTR const unsigned char  size(1) Largest target is 23
;;		 -> STR_3(CODE[13]), STR_2(CODE[23]), STR_1(CODE[18]), 
;;
;; ?_printf	int  size(1) Largest target is 0
;;
;; printf@ap	PTR void [1] size(1) Largest target is 2
;;		 -> ?_printf(BANK0[2]), 
;;
;; S397$_cp	PTR const unsigned char  size(1) Largest target is 0
;;
;; _val._str._cp	PTR const unsigned char  size(1) Largest target is 0
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   _main->___tmul
;;   _main->___ltmod
;;   _main->___ltdiv
;;   _printf->___lwmod
;;   ___lwmod->___lwdiv
;;
;; Critical Paths under _isr in COMMON
;;
;;   None.
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_printf
;;   _printf->___lwmod
;;   ___lwmod->___lwdiv
;;
;; Critical Paths under _isr in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _isr in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.
;;
;; Critical Paths under _isr in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 1, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 5     5      0    1255
;;                                             21 BANK0      5     5      0
;;                             _printf
;;                             ___tmul
;;                        _StartTimer2
;;                            ___ltmod
;;                            ___ltdiv
;; ---------------------------------------------------------------------------------
;; (1) _printf                                              14    11      3     729
;;                                              7 BANK0     14    11      3
;;                              _putch
;;                            ___lwdiv
;;                            ___lwmod
;; ---------------------------------------------------------------------------------
;; (1) ___tmul                                               9     3      6      92
;;                                              3 COMMON     6     0      6
;;                                              0 BANK0      3     3      0
;; ---------------------------------------------------------------------------------
;; (1) ___ltmod                                              7     1      6     159
;;                                              3 COMMON     6     0      6
;;                                              0 BANK0      1     1      0
;; ---------------------------------------------------------------------------------
;; (1) ___ltdiv                                             10     4      6     162
;;                                              3 COMMON     6     0      6
;;                                              0 BANK0      4     4      0
;; ---------------------------------------------------------------------------------
;; (2) ___lwmod                                              5     1      4     159
;;                                              7 COMMON     1     1      0
;;                                              3 BANK0      4     0      4
;;                            ___lwdiv (ARG)
;; ---------------------------------------------------------------------------------
;; (2) ___lwdiv                                              7     3      4     162
;;                                              3 COMMON     4     0      4
;;                                              0 BANK0      3     3      0
;; ---------------------------------------------------------------------------------
;; (2) _putch                                                1     1      0      22
;;                                              3 COMMON     1     1      0
;; ---------------------------------------------------------------------------------
;; (1) _StartTimer2                                          0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (3) _isr                                                  3     3      0       0
;;                                              0 COMMON     3     3      0
;;                         _StartTimer
;;                      i1_StartTimer2
;; ---------------------------------------------------------------------------------
;; (4) i1_StartTimer2                                        0     0      0       0
;; ---------------------------------------------------------------------------------
;; (4) _StartTimer                                           0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 4
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _printf
;;     _putch
;;     ___lwdiv
;;     ___lwmod
;;       ___lwdiv (ARG)
;;   ___tmul
;;   _StartTimer2
;;   ___ltmod
;;   ___ltdiv
;;
;; _isr (ROOT)
;;   _StartTimer
;;   i1_StartTimer2
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       1       0        7.1%
;;EEDATA              80      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      9       B       1       78.6%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       6       2        0.0%
;;BANK0               50     1A      25       3       46.3%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;ABS                  0      0      30       4        0.0%
;;BITBANK0            50      0       0       5        0.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK2            30      0       0       8        0.0%
;;BANK2               30      0       0       9        0.0%
;;DATA                 0      0      36      10        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 28 in file "W:\PIC\Fan_IR_Timer\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  temp            1   22[BANK0 ] unsigned char 
;;  temp            1   23[BANK0 ] unsigned char 
;;  delta           2   24[BANK0 ] unsigned int 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 60/0
;;		Unchanged: FFE00/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       4       0       0
;;      Temps:          0       1       0       0
;;      Totals:         0       5       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_printf
;;		___tmul
;;		_StartTimer2
;;		___ltmod
;;		___ltdiv
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"W:\PIC\Fan_IR_Timer\main.c"
	line	28
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 8
; Regs used in _main: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	30
	
l1852:	
;main.c: 30: unsigned int delta = 4596;
	movlw	low(011F4h)
	movwf	(main@delta)
	movlw	high(011F4h)
	movwf	((main@delta))+1
	line	33
	
l1854:	
;main.c: 33: RA1 = 0;
	bcf	(41/8),(41)&7
	line	34
;main.c: 34: CMCON = 0x07;
	movlw	(07h)
	movwf	(31)	;volatile
	line	35
	
l1856:	
;main.c: 35: TRISA = 0x00;
	bsf	status, 5	;RP0=1, select bank1
	clrf	(133)^080h	;volatile
	line	36
	
l1858:	
;main.c: 36: TRISB = 0b00001001;
	movlw	(09h)
	movwf	(134)^080h	;volatile
	line	37
;main.c: 37: PORTB = 0x00;
	bcf	status, 5	;RP0=0, select bank0
	clrf	(6)	;volatile
	line	38
;main.c: 38: PORTA = 0x00;
	clrf	(5)	;volatile
	line	39
	
l1860:	
;main.c: 39: RA0 = 0;
	bcf	(40/8),(40)&7
	line	40
;main.c: 40: PORTB = 0x00;
	clrf	(6)	;volatile
	line	41
	
l1862:	
;main.c: 41: TRISB1 = 1; TRISB2 = 1; SPBRG = ((int)(4000000L/(16UL * 9600) -1)); RCSTA = (0|0x90); TXSTA = (0x4|0|0x20);
	bsf	status, 5	;RP0=1, select bank1
	bsf	(1073/8)^080h,(1073)&7
	
l1864:	
	bsf	(1074/8)^080h,(1074)&7
	
l1866:	
	movlw	(019h)
	movwf	(153)^080h	;volatile
	
l1868:	
	movlw	(090h)
	bcf	status, 5	;RP0=0, select bank0
	movwf	(24)	;volatile
	
l1870:	
	movlw	(024h)
	bsf	status, 5	;RP0=1, select bank1
	movwf	(152)^080h	;volatile
	line	42
	
l1872:	
;main.c: 42: GIE = 1;
	bsf	(95/8),(95)&7
	line	43
	
l1874:	
;main.c: 43: PEIE = 1;
	bsf	(94/8),(94)&7
	line	45
	
l1876:	
;main.c: 45: buffer = 0;
	bcf	(_buffer/8),(_buffer)&7
	line	46
	
l1878:	
;main.c: 46: IRbyte = 0;
	bcf	status, 5	;RP0=0, select bank0
	clrf	(_IRbyte)	;volatile
	line	47
	
l1880:	
;main.c: 47: bytecount = 0;
	clrf	(_bytecount)	;volatile
	line	48
	
l1882:	
;main.c: 48: direction = 0;
	bcf	(_direction/8),(_direction)&7
	line	49
	
l1884:	
;main.c: 49: INTEDG = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	(1038/8)^080h,(1038)&7
	line	52
	
l1886:	
;main.c: 52: INTE = 1;
	bsf	(92/8),(92)&7
	line	53
	
l1888:	
;main.c: 53: printf("Starting done! \r\n");
	movlw	((STR_1-__stringbase))&0ffh
	fcall	_printf
	line	54
	
l1890:	
# 54 "W:\PIC\Fan_IR_Timer\main.c"
sleep ;#
psect	maintext
	line	60
	
l1892:	
;main.c: 56: {
;main.c: 60: if (bytecount>13)
	movlw	(0Eh)
	subwf	(_bytecount),w	;volatile
	skipc
	goto	u1021
	goto	u1020
u1021:
	goto	l1958
u1020:
	line	62
	
l1894:	
;main.c: 61: {
;main.c: 62: GIE = 0;
	bcf	(95/8),(95)&7
	line	66
	
l1896:	
;main.c: 66: if (oldtogglebit != togglebit)
	btfsc	(_togglebit/8),(_togglebit)&7
	goto	u1031
	goto	u1030
u1031:
	movlw	1
	goto	u1032
u1030:
	movlw	0
u1032:
	movwf	(??_main+0)+0
	btfsc	(_oldtogglebit/8),(_oldtogglebit)&7
	goto	u1041
	goto	u1040
u1041:
	movlw	1
	goto	u1042
u1040:
	movlw	0
u1042:
	xorwf	(??_main+0)+0,w
	skipnz
	goto	u1051
	goto	u1050
u1051:
	goto	l1948
u1050:
	line	68
	
l1898:	
;main.c: 67: {
;main.c: 68: printf("\r\nPressed button is %d", IRbyte);
	movf	(_IRbyte),w	;volatile
	movwf	(?_printf)
	clrf	(?_printf+1)
	movlw	((STR_2-__stringbase))&0ffh
	fcall	_printf
	line	69
;main.c: 69: switch(IRbyte)
	goto	l1946
	line	71
	
l1900:	
	movlw	1<<((40)&7)
	xorwf	((40)/8),f
	line	72
	
l1902:	
;main.c: 72: if (!RA0)
	btfsc	(40/8),(40)&7
	goto	u1061
	goto	u1060
u1061:
	goto	l1948
u1060:
	line	74
	
l1904:	
;main.c: 73: {
;main.c: 74: time = 0;
	clrf	(_time)	;volatile
	clrf	(_time+1)	;volatile
	clrf	(_time+2)	;volatile
	line	75
;main.c: 75: RA1 = 0;
	bcf	(41/8),(41)&7
	line	76
	
l1906:	
;main.c: 76: PORTB &= 0b00001111;
	movlw	(0Fh)
	andwf	(6),f	;volatile
	line	77
	
l1908:	
;main.c: 77: TMR2ON = 0; TMR2IE = 0;
	bcf	(146/8),(146)&7
	
l1910:	
	bsf	status, 5	;RP0=1, select bank1
	bcf	(1121/8)^080h,(1121)&7
	line	78
	
l1912:	
# 78 "W:\PIC\Fan_IR_Timer\main.c"
sleep ;#
psect	maintext
	goto	l1948
	line	81
	
l1914:	
	movlw	0
	subwf	(_time+2),w	;volatile
	skipz
	goto	u1075
	movlw	0
	subwf	(_time+1),w	;volatile
	skipz
	goto	u1075
	movlw	010h
	subwf	(_time),w	;volatile
u1075:
	skipnc
	goto	u1071
	goto	u1070
u1071:
	goto	l359
u1070:
	
l1916:	
	btfss	(40/8),(40)&7
	goto	u1081
	goto	u1080
u1081:
	goto	l359
u1080:
	line	83
	
l1918:	
;main.c: 82: {
;main.c: 83: RA1 = 1;
	bsf	(41/8),(41)&7
	line	84
	
l1920:	
;main.c: 84: time++;
	incf	(_time),f	;volatile
	skipnz
	incf	(_time+1),f	;volatile
	skipnz
	incf	(_time+2),f	;volatile
	line	85
	
l1922:	
;main.c: 85: unsigned char temp = time << 4;
	swapf	(_time),w	;volatile
	andlw	(0ffh shl 4) & 0ffh
	movwf	(main@temp)
	line	86
	
l1924:	
;main.c: 86: tickCount = delta*time;
	movf	(main@delta),w
	movwf	(?___tmul)
	movf	(main@delta+1),w
	movwf	(?___tmul+1)
	clrf	(?___tmul+2)
	movf	(_time),w	;volatile
	movwf	0+(?___tmul)+03h
	movf	(_time+1),w	;volatile
	movwf	1+(?___tmul)+03h
	movf	(_time+2),w	;volatile
	movwf	2+(?___tmul)+03h
	fcall	___tmul
	movf	(0+(?___tmul)),w
	movwf	(_tickCount)	;volatile
	movf	(1+(?___tmul)),w
	movwf	(_tickCount+1)	;volatile
	movf	(2+(?___tmul)),w
	movwf	(_tickCount+2)	;volatile
	line	87
	
l1926:	
;main.c: 87: printf("Tickcount %d", tickCount);
	movf	(_tickCount),w	;volatile
	movwf	(?_printf)
	movf	(_tickCount+1),w	;volatile
	movwf	(?_printf+1)
	movf	(_tickCount+2),w	;volatile
	movwf	(?_printf+2)
	movlw	((STR_3-__stringbase))&0ffh
	fcall	_printf
	line	88
	
l1928:	
;main.c: 88: PORTB &= 0b00001111;
	movlw	(0Fh)
	andwf	(6),f	;volatile
	line	89
	
l1930:	
;main.c: 89: PORTB |= temp;
	movf	(main@temp),w
	iorwf	(6),f	;volatile
	line	90
	
l1932:	
;main.c: 90: tick = 0;
	clrf	(_tick)	;volatile
	clrf	(_tick+1)	;volatile
	clrf	(_tick+2)	;volatile
	line	91
	
l1934:	
;main.c: 91: StartTimer2();
	fcall	_StartTimer2
	line	92
;main.c: 92: } else if (RA0)
	goto	l1948
	
l359:	
	btfss	(40/8),(40)&7
	goto	u1091
	goto	u1090
u1091:
	goto	l357
u1090:
	line	94
	
l1936:	
;main.c: 93: {
;main.c: 94: time = 0;
	clrf	(_time)	;volatile
	clrf	(_time+1)	;volatile
	clrf	(_time+2)	;volatile
	line	95
	
l1938:	
;main.c: 95: PORTB &= 0b00001111;
	movlw	(0Fh)
	andwf	(6),f	;volatile
	goto	l1948
	line	103
;main.c: 103: case 23 : if (RA0) RA1 = !RA1;
	
l363:	
	btfss	(40/8),(40)&7
	goto	u1101
	goto	u1100
u1101:
	goto	l364
u1100:
	
l1942:	
	movlw	1<<((41)&7)
	xorwf	((41)/8),f
	goto	l1948
	line	104
	
l364:	
;main.c: 104: else RA1 = 0;
	bcf	(41/8),(41)&7
	goto	l1948
	line	69
	
l1946:	
	movf	(_IRbyte),w	;volatile
	; Switch size 1, requested type "space"
; Number of cases is 4, Range of values is 15 to 37
; switch strategies available:
; Name         Bytes Cycles
; simple_byte    13     7 (average)
; direct_byte    39    16 (fixed)
;	Chosen strategy is simple_byte

	xorlw	15^0	; case 15
	skipnz
	goto	l1914
	xorlw	23^15	; case 23
	skipnz
	goto	l363
	xorlw	31^23	; case 31
	skipnz
	goto	l1900
	xorlw	37^31	; case 37
	skipnz
	goto	l1936
	goto	l1948

	line	108
	
l357:	
	line	110
	
l1948:	
;main.c: 109: }
;main.c: 110: IRbyte = 0;
	bcf	status, 5	;RP0=0, select bank0
	clrf	(_IRbyte)	;volatile
	line	111
;main.c: 111: bytecount=0;
	clrf	(_bytecount)	;volatile
	line	112
	
l1950:	
;main.c: 112: INTEDG = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	(1038/8)^080h,(1038)&7
	line	113
	
l1952:	
;main.c: 113: buffer = 0;
	bcf	(_buffer/8),(_buffer)&7
	line	114
	
l1954:	
;main.c: 114: oldtogglebit = togglebit;
	btfsc	(_togglebit/8),(_togglebit)&7
	goto	u1111
	goto	u1110
	
u1111:
	bsf	(_oldtogglebit/8),(_oldtogglebit)&7
	goto	u1124
u1110:
	bcf	(_oldtogglebit/8),(_oldtogglebit)&7
u1124:
	line	115
	
l1956:	
;main.c: 115: GIE = 1;
	bsf	(95/8),(95)&7
	line	119
	
l1958:	
;main.c: 116: }
;main.c: 119: if ((tick%delta == 0)&&(tick>0))
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(main@delta),w
	movwf	(?___ltmod)
	movf	(main@delta+1),w
	movwf	(?___ltmod+1)
	clrf	(?___ltmod+2)
	movf	(_tick),w	;volatile
	movwf	0+(?___ltmod)+03h
	movf	(_tick+1),w	;volatile
	movwf	1+(?___ltmod)+03h
	movf	(_tick+2),w	;volatile
	movwf	2+(?___ltmod)+03h
	fcall	___ltmod
	movf	(2+(?___ltmod)),w
	iorwf	(1+(?___ltmod)),w
	iorwf	(0+(?___ltmod)),w
	skipz
	goto	u1131
	goto	u1130
u1131:
	goto	l1892
u1130:
	
l1960:	
	movf	(_tick+2),w	;volatile
	iorwf	(_tick+1),w	;volatile
	iorwf	(_tick),w	;volatile
	skipnz
	goto	u1141
	goto	u1140
u1141:
	goto	l1892
u1140:
	line	121
	
l1962:	
;main.c: 120: {
;main.c: 121: unsigned char temp = (time - (tick/delta)) << 4;
	movf	(main@delta),w
	movwf	(?___ltdiv)
	movf	(main@delta+1),w
	movwf	(?___ltdiv+1)
	clrf	(?___ltdiv+2)
	movf	(_tick),w	;volatile
	movwf	0+(?___ltdiv)+03h
	movf	(_tick+1),w	;volatile
	movwf	1+(?___ltdiv)+03h
	movf	(_tick+2),w	;volatile
	movwf	2+(?___ltdiv)+03h
	fcall	___ltdiv
	movf	(0+(?___ltdiv)),w
	subwf	(_time),w	;volatile
	movwf	(??_main+0)+0
	swapf	(??_main+0)+0,w
	andlw	(0ffh shl 4) & 0ffh
	movwf	(main@temp_209)
	line	122
	
l1964:	
;main.c: 122: PORTB &= 0b00001111;
	movlw	(0Fh)
	andwf	(6),f	;volatile
	line	123
	
l1966:	
;main.c: 123: PORTB |= temp;
	movf	(main@temp_209),w
	iorwf	(6),f	;volatile
	goto	l1892
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

psect	maintext
	line	134
	signat	_main,88
	global	_printf
psect	text243,local,class=CODE,delta=2
global __ptext243
__ptext243:

;; *************** function _printf *****************
;; Defined at:
;;		line 461 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\lib\doprnt.c"
;; Parameters:    Size  Location     Type
;;  f               1    wreg     PTR const unsigned char 
;;		 -> STR_3(13), STR_2(23), STR_1(18), 
;; Auto vars:     Size  Location     Type
;;  f               1   14[BANK0 ] PTR const unsigned char 
;;		 -> STR_3(13), STR_2(23), STR_1(18), 
;;  _val            4   16[BANK0 ] struct .
;;  c               1   20[BANK0 ] char 
;;  prec            1   15[BANK0 ] char 
;;  flag            1   13[BANK0 ] unsigned char 
;;  ap              1   12[BANK0 ] PTR void [1]
;;		 -> ?_printf(2), 
;; Return value:  Size  Location     Type
;;                  2    7[BANK0 ] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 40/20
;;		On exit  : 60/0
;;		Unchanged: FFE9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       3       0       0
;;      Locals:         0       9       0       0
;;      Temps:          0       2       0       0
;;      Totals:         0      14       0       0
;;Total ram usage:       14 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_putch
;;		___lwdiv
;;		___lwmod
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text243
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\doprnt.c"
	line	461
	global	__size_of_printf
	__size_of_printf	equ	__end_of_printf-_printf
	
_printf:	
	opt	stack 7
; Regs used in _printf: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
;printf@f stored from wreg
	line	537
	bcf	status, 5	;RP0=0, select bank0
	movwf	(printf@f)
	
l1804:	
	movlw	(?_printf)&0ffh
	movwf	(printf@ap)
	line	540
	goto	l1850
	line	542
	
l1806:	
	movf	(printf@c),w
	xorlw	025h
	skipnz
	goto	u951
	goto	u950
u951:
	goto	l1810
u950:
	line	545
	
l1808:	
	movf	(printf@c),w
	fcall	_putch
	line	546
	goto	l1850
	line	552
	
l1810:	
	clrf	(printf@flag)
	line	638
	
l1814:	
	movf	(printf@f),w
	incf	(printf@f),f
	movwf	fsr0
	fcall	stringdir
	movwf	(printf@c)
	; Switch size 1, requested type "space"
; Number of cases is 3, Range of values is 0 to 105
; switch strategies available:
; Name         Bytes Cycles
; simple_byte    10     6 (average)
; direct_byte   119    13 (fixed)
;	Chosen strategy is simple_byte

	xorlw	0^0	; case 0
	skipnz
	goto	l755
	xorlw	100^0	; case 100
	skipnz
	goto	l1816
	xorlw	105^100	; case 105
	skipnz
	goto	l1816
	goto	l1850

	line	1254
	
l1816:	
	movf	(printf@ap),w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(printf@_val)
	incf	fsr0,f
	movf	indf,w
	movwf	(printf@_val+1)
	
l1818:	
	incf	(printf@ap),f
	incf	(printf@ap),f
	line	1256
	
l1820:	
	btfss	(printf@_val+1),7
	goto	u961
	goto	u960
u961:
	goto	l1826
u960:
	line	1257
	
l1822:	
	movlw	(03h)
	iorwf	(printf@flag),f
	line	1258
	
l1824:	
	comf	(printf@_val),f
	comf	(printf@_val+1),f
	incf	(printf@_val),f
	skipnz
	incf	(printf@_val+1),f
	line	1300
	
l1826:	
	clrf	(printf@c)
	incf	(printf@c),f
	line	1301
	
l1830:	
	clrc
	rlf	(printf@c),w
	addlw	low((_dpowers-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(??_printf+0)+0
	fcall	stringdir
	movwf	(??_printf+0)+0+1
	movf	1+(??_printf+0)+0,w
	subwf	(printf@_val+1),w
	skipz
	goto	u975
	movf	0+(??_printf+0)+0,w
	subwf	(printf@_val),w
u975:
	skipnc
	goto	u971
	goto	u970
u971:
	goto	l1834
u970:
	goto	l1838
	line	1300
	
l1834:	
	incf	(printf@c),f
	
l1836:	
	movf	(printf@c),w
	xorlw	05h
	skipz
	goto	u981
	goto	u980
u981:
	goto	l1830
u980:
	line	1433
	
l1838:	
	movf	(printf@flag),w
	andlw	03h
	btfsc	status,2
	goto	u991
	goto	u990
u991:
	goto	l1842
u990:
	line	1434
	
l1840:	
	movlw	(02Dh)
	fcall	_putch
	line	1467
	
l1842:	
	movf	(printf@c),w
	movwf	(printf@prec)
	line	1469
	goto	l1848
	line	1484
	
l1844:	
	movlw	0Ah
	movwf	(?___lwmod)
	clrf	(?___lwmod+1)
	clrc
	rlf	(printf@prec),w
	addlw	low((_dpowers-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(?___lwdiv)
	fcall	stringdir
	movwf	(?___lwdiv+1)
	movf	(printf@_val+1),w
	movwf	1+(?___lwdiv)+02h
	movf	(printf@_val),w
	movwf	0+(?___lwdiv)+02h
	fcall	___lwdiv
	movf	(1+(?___lwdiv)),w
	movwf	1+(?___lwmod)+02h
	movf	(0+(?___lwdiv)),w
	movwf	0+(?___lwmod)+02h
	fcall	___lwmod
	movf	(0+(?___lwmod)),w
	addlw	030h
	movwf	(printf@c)
	line	1516
	
l1846:	
	movf	(printf@c),w
	fcall	_putch
	line	1469
	
l1848:	
	decf	(printf@prec),f
	incf	((printf@prec)),w
	skipz
	goto	u1001
	goto	u1000
u1001:
	goto	l1844
u1000:
	line	540
	
l1850:	
	movf	(printf@f),w
	incf	(printf@f),f
	movwf	fsr0
	fcall	stringdir
	movwf	(printf@c)
	movf	((printf@c)),f
	skipz
	goto	u1011
	goto	u1010
u1011:
	goto	l1806
u1010:
	line	1533
	
l755:	
	return
	opt stack 0
GLOBAL	__end_of_printf
	__end_of_printf:
;; =============== function _printf ends ============

	signat	_printf,602
	global	___tmul
psect	text244,local,class=CODE,delta=2
global __ptext244
__ptext244:

;; *************** function ___tmul *****************
;; Defined at:
;;		line 3 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\sources\tmul.c"
;; Parameters:    Size  Location     Type
;;  multiplier      3    3[COMMON] unsigned um
;;  multiplicand    3    6[COMMON] unsigned um
;; Auto vars:     Size  Location     Type
;;  product         3    0[BANK0 ] unsigned um
;; Return value:  Size  Location     Type
;;                  3    3[COMMON] unsigned um
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         6       0       0       0
;;      Locals:         0       3       0       0
;;      Temps:          0       0       0       0
;;      Totals:         6       3       0       0
;;Total ram usage:        9 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text244
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\tmul.c"
	line	3
	global	__size_of___tmul
	__size_of___tmul	equ	__end_of___tmul-___tmul
	
___tmul:	
	opt	stack 7
; Regs used in ___tmul: [wreg+status,2+status,0]
	line	4
	
l1772:	
	clrf	(___tmul@product)
	clrf	(___tmul@product+1)
	clrf	(___tmul@product+2)
	line	6
	
l1090:	
	line	7
	btfss	(___tmul@multiplier),(0)&7
	goto	u921
	goto	u920
u921:
	goto	l1776
u920:
	line	8
	
l1774:	
	movf	(___tmul@multiplicand),w
	addwf	(___tmul@product),f
	movf	(___tmul@multiplicand+1),w
	clrz
	skipnc
	incf	(___tmul@multiplicand+1),w
	skipnz
	goto	u931
	addwf	(___tmul@product+1),f
u931:
	movf	(___tmul@multiplicand+2),w
	clrz
	skipnc
	incf	(___tmul@multiplicand+2),w
	skipnz
	goto	u932
	addwf	(___tmul@product+2),f
u932:

	line	9
	
l1776:	
	clrc
	rlf	(___tmul@multiplicand),f
	rlf	(___tmul@multiplicand+1),f
	rlf	(___tmul@multiplicand+2),f
	line	10
	
l1778:	
	clrc
	rrf	(___tmul@multiplier+2),f
	rrf	(___tmul@multiplier+1),f
	rrf	(___tmul@multiplier),f
	line	11
	movf	(___tmul@multiplier+2),w
	iorwf	(___tmul@multiplier+1),w
	iorwf	(___tmul@multiplier),w
	skipz
	goto	u941
	goto	u940
u941:
	goto	l1090
u940:
	line	12
	
l1780:	
	movf	(___tmul@product),w
	movwf	(?___tmul)
	movf	(___tmul@product+1),w
	movwf	(?___tmul+1)
	movf	(___tmul@product+2),w
	movwf	(?___tmul+2)
	line	13
	
l1093:	
	return
	opt stack 0
GLOBAL	__end_of___tmul
	__end_of___tmul:
;; =============== function ___tmul ends ============

	signat	___tmul,8315
	global	___ltmod
psect	text245,local,class=CODE,delta=2
global __ptext245
__ptext245:

;; *************** function ___ltmod *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\sources\ltmod.c"
;; Parameters:    Size  Location     Type
;;  divisor         3    3[COMMON] unsigned um
;;  dividend        3    6[COMMON] unsigned um
;; Auto vars:     Size  Location     Type
;;  counter         1    0[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  3    3[COMMON] unsigned um
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         6       0       0       0
;;      Locals:         0       1       0       0
;;      Temps:          0       0       0       0
;;      Totals:         6       1       0       0
;;Total ram usage:        7 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text245
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\ltmod.c"
	line	5
	global	__size_of___ltmod
	__size_of___ltmod	equ	__end_of___ltmod-___ltmod
	
___ltmod:	
	opt	stack 7
; Regs used in ___ltmod: [wreg+status,2+status,0]
	line	8
	
l1752:	
	movf	(___ltmod@divisor+2),w
	iorwf	(___ltmod@divisor+1),w
	iorwf	(___ltmod@divisor),w
	skipnz
	goto	u881
	goto	u880
u881:
	goto	l1768
u880:
	line	9
	
l1754:	
	clrf	(___ltmod@counter)
	incf	(___ltmod@counter),f
	line	10
	goto	l1758
	line	11
	
l1756:	
	clrc
	rlf	(___ltmod@divisor),f
	rlf	(___ltmod@divisor+1),f
	rlf	(___ltmod@divisor+2),f
	line	12
	incf	(___ltmod@counter),f
	line	10
	
l1758:	
	btfss	(___ltmod@divisor+2),(23)&7
	goto	u891
	goto	u890
u891:
	goto	l1756
u890:
	line	15
	
l1760:	
	movf	(___ltmod@divisor+2),w
	subwf	(___ltmod@dividend+2),w
	skipz
	goto	u905
	movf	(___ltmod@divisor+1),w
	subwf	(___ltmod@dividend+1),w
	skipz
	goto	u905
	movf	(___ltmod@divisor),w
	subwf	(___ltmod@dividend),w
u905:
	skipc
	goto	u901
	goto	u900
u901:
	goto	l1764
u900:
	line	16
	
l1762:	
	movf	(___ltmod@divisor),w
	subwf	(___ltmod@dividend),f
	movf	(___ltmod@divisor+1),w
	skipc
	incfsz	(___ltmod@divisor+1),w
	subwf	(___ltmod@dividend+1),f
	movf	(___ltmod@divisor+2),w
	skipc
	incf	(___ltmod@divisor+2),w
	subwf	(___ltmod@dividend+2),f
	line	17
	
l1764:	
	clrc
	rrf	(___ltmod@divisor+2),f
	rrf	(___ltmod@divisor+1),f
	rrf	(___ltmod@divisor),f
	line	18
	
l1766:	
	decfsz	(___ltmod@counter),f
	goto	u911
	goto	u910
u911:
	goto	l1760
u910:
	line	20
	
l1768:	
	movf	(___ltmod@dividend),w
	movwf	(?___ltmod)
	movf	(___ltmod@dividend+1),w
	movwf	(?___ltmod+1)
	movf	(___ltmod@dividend+2),w
	movwf	(?___ltmod+2)
	line	21
	
l1087:	
	return
	opt stack 0
GLOBAL	__end_of___ltmod
	__end_of___ltmod:
;; =============== function ___ltmod ends ============

	signat	___ltmod,8315
	global	___ltdiv
psect	text246,local,class=CODE,delta=2
global __ptext246
__ptext246:

;; *************** function ___ltdiv *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\sources\ltdiv.c"
;; Parameters:    Size  Location     Type
;;  divisor         3    3[COMMON] unsigned um
;;  dividend        3    6[COMMON] unsigned um
;; Auto vars:     Size  Location     Type
;;  quotient        3    0[BANK0 ] unsigned um
;;  counter         1    3[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  3    3[COMMON] unsigned um
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         6       0       0       0
;;      Locals:         0       4       0       0
;;      Temps:          0       0       0       0
;;      Totals:         6       4       0       0
;;Total ram usage:       10 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text246
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\ltdiv.c"
	line	5
	global	__size_of___ltdiv
	__size_of___ltdiv	equ	__end_of___ltdiv-___ltdiv
	
___ltdiv:	
	opt	stack 7
; Regs used in ___ltdiv: [wreg+status,2+status,0]
	line	9
	
l1726:	
	clrf	(___ltdiv@quotient)
	clrf	(___ltdiv@quotient+1)
	clrf	(___ltdiv@quotient+2)
	line	10
	
l1728:	
	movf	(___ltdiv@divisor+2),w
	iorwf	(___ltdiv@divisor+1),w
	iorwf	(___ltdiv@divisor),w
	skipnz
	goto	u841
	goto	u840
u841:
	goto	l1748
u840:
	line	11
	
l1730:	
	clrf	(___ltdiv@counter)
	incf	(___ltdiv@counter),f
	line	12
	goto	l1734
	line	13
	
l1732:	
	clrc
	rlf	(___ltdiv@divisor),f
	rlf	(___ltdiv@divisor+1),f
	rlf	(___ltdiv@divisor+2),f
	line	14
	incf	(___ltdiv@counter),f
	line	12
	
l1734:	
	btfss	(___ltdiv@divisor+2),(23)&7
	goto	u851
	goto	u850
u851:
	goto	l1732
u850:
	line	17
	
l1736:	
	clrc
	rlf	(___ltdiv@quotient),f
	rlf	(___ltdiv@quotient+1),f
	rlf	(___ltdiv@quotient+2),f
	line	18
	
l1738:	
	movf	(___ltdiv@divisor+2),w
	subwf	(___ltdiv@dividend+2),w
	skipz
	goto	u865
	movf	(___ltdiv@divisor+1),w
	subwf	(___ltdiv@dividend+1),w
	skipz
	goto	u865
	movf	(___ltdiv@divisor),w
	subwf	(___ltdiv@dividend),w
u865:
	skipc
	goto	u861
	goto	u860
u861:
	goto	l1744
u860:
	line	19
	
l1740:	
	movf	(___ltdiv@divisor),w
	subwf	(___ltdiv@dividend),f
	movf	(___ltdiv@divisor+1),w
	skipc
	incfsz	(___ltdiv@divisor+1),w
	subwf	(___ltdiv@dividend+1),f
	movf	(___ltdiv@divisor+2),w
	skipc
	incf	(___ltdiv@divisor+2),w
	subwf	(___ltdiv@dividend+2),f
	line	20
	
l1742:	
	bsf	(___ltdiv@quotient)+(0/8),(0)&7
	line	22
	
l1744:	
	clrc
	rrf	(___ltdiv@divisor+2),f
	rrf	(___ltdiv@divisor+1),f
	rrf	(___ltdiv@divisor),f
	line	23
	
l1746:	
	decfsz	(___ltdiv@counter),f
	goto	u871
	goto	u870
u871:
	goto	l1736
u870:
	line	25
	
l1748:	
	movf	(___ltdiv@quotient),w
	movwf	(?___ltdiv)
	movf	(___ltdiv@quotient+1),w
	movwf	(?___ltdiv+1)
	movf	(___ltdiv@quotient+2),w
	movwf	(?___ltdiv+2)
	line	26
	
l1077:	
	return
	opt stack 0
GLOBAL	__end_of___ltdiv
	__end_of___ltdiv:
;; =============== function ___ltdiv ends ============

	signat	___ltdiv,8315
	global	___lwmod
psect	text247,local,class=CODE,delta=2
global __ptext247
__ptext247:

;; *************** function ___lwmod *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lwmod.c"
;; Parameters:    Size  Location     Type
;;  divisor         2    3[BANK0 ] unsigned int 
;;  dividend        2    5[BANK0 ] unsigned int 
;; Auto vars:     Size  Location     Type
;;  counter         1    7[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    3[BANK0 ] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       4       0       0
;;      Locals:         1       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         1       4       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_printf
;; This function uses a non-reentrant model
;;
psect	text247
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lwmod.c"
	line	5
	global	__size_of___lwmod
	__size_of___lwmod	equ	__end_of___lwmod-___lwmod
	
___lwmod:	
	opt	stack 6
; Regs used in ___lwmod: [wreg+status,2+status,0]
	line	8
	
l1706:	
	movf	(___lwmod@divisor+1),w
	iorwf	(___lwmod@divisor),w
	skipnz
	goto	u801
	goto	u800
u801:
	goto	l1722
u800:
	line	9
	
l1708:	
	clrf	(___lwmod@counter)
	incf	(___lwmod@counter),f
	line	10
	goto	l1712
	line	11
	
l1710:	
	clrc
	rlf	(___lwmod@divisor),f
	rlf	(___lwmod@divisor+1),f
	line	12
	incf	(___lwmod@counter),f
	line	10
	
l1712:	
	btfss	(___lwmod@divisor+1),(15)&7
	goto	u811
	goto	u810
u811:
	goto	l1710
u810:
	line	15
	
l1714:	
	movf	(___lwmod@divisor+1),w
	subwf	(___lwmod@dividend+1),w
	skipz
	goto	u825
	movf	(___lwmod@divisor),w
	subwf	(___lwmod@dividend),w
u825:
	skipc
	goto	u821
	goto	u820
u821:
	goto	l1718
u820:
	line	16
	
l1716:	
	movf	(___lwmod@divisor),w
	subwf	(___lwmod@dividend),f
	movf	(___lwmod@divisor+1),w
	skipc
	decf	(___lwmod@dividend+1),f
	subwf	(___lwmod@dividend+1),f
	line	17
	
l1718:	
	clrc
	rrf	(___lwmod@divisor+1),f
	rrf	(___lwmod@divisor),f
	line	18
	
l1720:	
	decfsz	(___lwmod@counter),f
	goto	u831
	goto	u830
u831:
	goto	l1714
u830:
	line	20
	
l1722:	
	movf	(___lwmod@dividend+1),w
	movwf	(?___lwmod+1)
	movf	(___lwmod@dividend),w
	movwf	(?___lwmod)
	line	21
	
l787:	
	return
	opt stack 0
GLOBAL	__end_of___lwmod
	__end_of___lwmod:
;; =============== function ___lwmod ends ============

	signat	___lwmod,8314
	global	___lwdiv
psect	text248,local,class=CODE,delta=2
global __ptext248
__ptext248:

;; *************** function ___lwdiv *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lwdiv.c"
;; Parameters:    Size  Location     Type
;;  divisor         2    3[COMMON] unsigned int 
;;  dividend        2    5[COMMON] unsigned int 
;; Auto vars:     Size  Location     Type
;;  quotient        2    0[BANK0 ] unsigned int 
;;  counter         1    2[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    3[COMMON] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         4       0       0       0
;;      Locals:         0       3       0       0
;;      Temps:          0       0       0       0
;;      Totals:         4       3       0       0
;;Total ram usage:        7 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_printf
;; This function uses a non-reentrant model
;;
psect	text248
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lwdiv.c"
	line	5
	global	__size_of___lwdiv
	__size_of___lwdiv	equ	__end_of___lwdiv-___lwdiv
	
___lwdiv:	
	opt	stack 6
; Regs used in ___lwdiv: [wreg+status,2+status,0]
	line	9
	
l1680:	
	clrf	(___lwdiv@quotient)
	clrf	(___lwdiv@quotient+1)
	line	10
	
l1682:	
	movf	(___lwdiv@divisor+1),w
	iorwf	(___lwdiv@divisor),w
	skipnz
	goto	u761
	goto	u760
u761:
	goto	l1702
u760:
	line	11
	
l1684:	
	clrf	(___lwdiv@counter)
	incf	(___lwdiv@counter),f
	line	12
	goto	l1688
	line	13
	
l1686:	
	clrc
	rlf	(___lwdiv@divisor),f
	rlf	(___lwdiv@divisor+1),f
	line	14
	incf	(___lwdiv@counter),f
	line	12
	
l1688:	
	btfss	(___lwdiv@divisor+1),(15)&7
	goto	u771
	goto	u770
u771:
	goto	l1686
u770:
	line	17
	
l1690:	
	clrc
	rlf	(___lwdiv@quotient),f
	rlf	(___lwdiv@quotient+1),f
	line	18
	
l1692:	
	movf	(___lwdiv@divisor+1),w
	subwf	(___lwdiv@dividend+1),w
	skipz
	goto	u785
	movf	(___lwdiv@divisor),w
	subwf	(___lwdiv@dividend),w
u785:
	skipc
	goto	u781
	goto	u780
u781:
	goto	l1698
u780:
	line	19
	
l1694:	
	movf	(___lwdiv@divisor),w
	subwf	(___lwdiv@dividend),f
	movf	(___lwdiv@divisor+1),w
	skipc
	decf	(___lwdiv@dividend+1),f
	subwf	(___lwdiv@dividend+1),f
	line	20
	
l1696:	
	bsf	(___lwdiv@quotient)+(0/8),(0)&7
	line	22
	
l1698:	
	clrc
	rrf	(___lwdiv@divisor+1),f
	rrf	(___lwdiv@divisor),f
	line	23
	
l1700:	
	decfsz	(___lwdiv@counter),f
	goto	u791
	goto	u790
u791:
	goto	l1690
u790:
	line	25
	
l1702:	
	movf	(___lwdiv@quotient+1),w
	movwf	(?___lwdiv+1)
	movf	(___lwdiv@quotient),w
	movwf	(?___lwdiv)
	line	26
	
l777:	
	return
	opt stack 0
GLOBAL	__end_of___lwdiv
	__end_of___lwdiv:
;; =============== function ___lwdiv ends ============

	signat	___lwdiv,8314
	global	_putch
psect	text249,local,class=CODE,delta=2
global __ptext249
__ptext249:

;; *************** function _putch *****************
;; Defined at:
;;		line 7 in file "W:\PIC\Fan_IR_Timer\usart.c"
;; Parameters:    Size  Location     Type
;;  byte            1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  byte            1    3[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         1       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_printf
;; This function uses a non-reentrant model
;;
psect	text249
	file	"W:\PIC\Fan_IR_Timer\usart.c"
	line	7
	global	__size_of_putch
	__size_of_putch	equ	__end_of_putch-_putch
	
_putch:	
	opt	stack 6
; Regs used in _putch: [wreg]
;putch@byte stored from wreg
	movwf	(putch@byte)
	line	9
	
l1676:	
	line	10
;usart.c: 9: while(!TXIF)
	
l713:	
	line	9
	btfss	(100/8),(100)&7
	goto	u751
	goto	u750
u751:
	goto	l713
u750:
	line	11
	
l1678:	
;usart.c: 11: TXREG = byte;
	movf	(putch@byte),w
	movwf	(25)	;volatile
	line	12
	
l716:	
	return
	opt stack 0
GLOBAL	__end_of_putch
	__end_of_putch:
;; =============== function _putch ends ============

	signat	_putch,4216
	global	_StartTimer2
psect	text250,local,class=CODE,delta=2
global __ptext250
__ptext250:

;; *************** function _StartTimer2 *****************
;; Defined at:
;;		line 242 in file "W:\PIC\Fan_IR_Timer\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text250
	file	"W:\PIC\Fan_IR_Timer\main.c"
	line	242
	global	__size_of_StartTimer2
	__size_of_StartTimer2	equ	__end_of_StartTimer2-_StartTimer2
	
_StartTimer2:	
	opt	stack 7
; Regs used in _StartTimer2: [wreg+status,2]
	line	244
	
l1598:	
;main.c: 244: TMR2 = 0x00;
	clrf	(17)	;volatile
	line	245
	
l1600:	
;main.c: 245: PR2 = 0xFF;
	movlw	(0FFh)
	bsf	status, 5	;RP0=1, select bank1
	movwf	(146)^080h	;volatile
	line	246
	
l1602:	
;main.c: 246: T2CKPS0 = 1; T2CKPS1 = 1;
	bcf	status, 5	;RP0=0, select bank0
	bsf	(144/8),(144)&7
	
l1604:	
	bsf	(145/8),(145)&7
	line	247
	
l1606:	
;main.c: 247: TOUTPS2 = 1; TOUTPS1 = 1; TOUTPS3 = 1; TOUTPS0 = 1;
	bsf	(149/8),(149)&7
	
l1608:	
	bsf	(148/8),(148)&7
	
l1610:	
	bsf	(150/8),(150)&7
	
l1612:	
	bsf	(147/8),(147)&7
	line	248
	
l1614:	
;main.c: 248: TMR2IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bsf	(1121/8)^080h,(1121)&7
	line	249
	
l1616:	
;main.c: 249: TMR2ON = 1;
	bcf	status, 5	;RP0=0, select bank0
	bsf	(146/8),(146)&7
	line	250
	
l388:	
	return
	opt stack 0
GLOBAL	__end_of_StartTimer2
	__end_of_StartTimer2:
;; =============== function _StartTimer2 ends ============

	signat	_StartTimer2,88
	global	_isr
psect	text251,local,class=CODE,delta=2
global __ptext251
__ptext251:

;; *************** function _isr *****************
;; Defined at:
;;		line 138 in file "W:\PIC\Fan_IR_Timer\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  2  370[COMMON] int 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 60/0
;;		Unchanged: FFE00/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          3       0       0       0
;;      Totals:         3       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_StartTimer
;;		i1_StartTimer2
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	text251
	file	"W:\PIC\Fan_IR_Timer\main.c"
	line	138
	global	__size_of_isr
	__size_of_isr	equ	__end_of_isr-_isr
	
_isr:	
	opt	stack 5
; Regs used in _isr: [wreg+status,2+status,0+pclath+cstack]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+0
	movwf	saved_w
	movf	status,w
	movwf	(??_isr+1)
	movf	pclath,w
	movwf	(??_isr+2)
	ljmp	_isr
psect	text251
	line	141
	
i1l1618:	
;main.c: 141: if (INTF)
	btfss	(89/8),(89)&7
	goto	u58_21
	goto	u58_20
u58_21:
	goto	i1l371
u58_20:
	line	144
	
i1l1620:	
;main.c: 142: {
;main.c: 144: if (bytecount<1)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(_bytecount),f
	skipz	;volatile
	goto	u59_21
	goto	u59_20
u59_21:
	goto	i1l1628
u59_20:
	line	146
	
i1l1622:	
;main.c: 145: {
;main.c: 146: direction = 1;
	bsf	(_direction/8),(_direction)&7
	line	147
;main.c: 147: INTEDG = 1;
	bsf	status, 5	;RP0=1, select bank1
	bsf	(1038/8)^080h,(1038)&7
	line	148
	
i1l1624:	
;main.c: 148: StartTimer();
	fcall	_StartTimer
	line	149
	
i1l1626:	
;main.c: 149: bytecount++;
	incf	(_bytecount),f	;volatile
	line	152
;main.c: 152: } else
	goto	i1l1636
	line	154
	
i1l1628:	
;main.c: 153: {
;main.c: 154: direction = !direction;
	movlw	1<<((_direction)&7)
	xorwf	((_direction)/8),f
	line	157
;main.c: 157: if (direction==buffer)
	btfsc	(_buffer/8),(_buffer)&7
	goto	u60_21
	goto	u60_20
u60_21:
	movlw	1
	goto	u60_22
u60_20:
	movlw	0
u60_22:
	movwf	(??_isr+0)+0
	btfsc	(_direction/8),(_direction)&7
	goto	u61_21
	goto	u61_20
u61_21:
	movlw	1
	goto	u61_22
u61_20:
	movlw	0
u61_22:
	xorwf	(??_isr+0)+0,w
	skipz
	goto	u62_21
	goto	u62_20
u62_21:
	goto	i1l1634
u62_20:
	line	159
	
i1l1630:	
;main.c: 158: {
;main.c: 159: StartTimer();
	fcall	_StartTimer
	line	160
	
i1l1632:	
;main.c: 160: bytecount++;
	incf	(_bytecount),f	;volatile
	line	164
	
i1l1634:	
;main.c: 162: }
;main.c: 164: INTEDG = direction;
	btfsc	(_direction/8),(_direction)&7
	goto	u63_21
	goto	u63_20
	
u63_21:
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1038/8)^080h,(1038)&7
	goto	u64_24
u63_20:
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1038/8)^080h,(1038)&7
u64_24:
	line	168
	
i1l1636:	
;main.c: 167: }
;main.c: 168: INTF = 0;
	bcf	(89/8),(89)&7
	line	170
;main.c: 170: }
	goto	i1l1652
	line	171
	
i1l371:	
	line	172
;main.c: 171: else {
;main.c: 172: if (TMR1IF)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(96/8),(96)&7
	goto	u65_21
	goto	u65_20
u65_21:
	goto	i1l1652
u65_20:
	line	175
	
i1l1638:	
;main.c: 173: {
;main.c: 175: buffer = RB0;
	btfsc	(48/8),(48)&7
	goto	u66_21
	goto	u66_20
	
u66_21:
	bsf	(_buffer/8),(_buffer)&7
	goto	u67_24
u66_20:
	bcf	(_buffer/8),(_buffer)&7
u67_24:
	line	176
;main.c: 176: TMR1ON = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(128/8),(128)&7
	line	177
;main.c: 177: TMR1IF = 0;
	bcf	(96/8),(96)&7
	line	178
	
i1l1640:	
;main.c: 178: if (bytecount==2) togglebit = buffer;
	movf	(_bytecount),w	;volatile
	xorlw	02h
	skipz
	goto	u68_21
	goto	u68_20
u68_21:
	goto	i1l1644
u68_20:
	
i1l1642:	
	btfsc	(_buffer/8),(_buffer)&7
	goto	u69_21
	goto	u69_20
	
u69_21:
	bsf	(_togglebit/8),(_togglebit)&7
	goto	u70_24
u69_20:
	bcf	(_togglebit/8),(_togglebit)&7
u70_24:
	line	179
	
i1l1644:	
;main.c: 179: if ((bytecount>7)&&(bytecount<14))
	movlw	(08h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	subwf	(_bytecount),w	;volatile
	skipc
	goto	u71_21
	goto	u71_20
u71_21:
	goto	i1l1652
u71_20:
	
i1l1646:	
	movlw	(0Eh)
	subwf	(_bytecount),w	;volatile
	skipnc
	goto	u72_21
	goto	u72_20
u72_21:
	goto	i1l1652
u72_20:
	line	181
	
i1l1648:	
;main.c: 180: {
;main.c: 181: IRbyte <<= 1;
	clrc
	rlf	(_IRbyte),f	;volatile
	line	182
	
i1l1650:	
;main.c: 182: IRbyte |= buffer;
	movlw	0
	btfsc	(_buffer/8),(_buffer)&7
	movlw	1
	iorwf	(_IRbyte),f	;volatile
	line	191
	
i1l1652:	
;main.c: 183: }
;main.c: 187: }
;main.c: 188: }
;main.c: 191: if (TMR2IF)
	bcf	status, 5	;RP0=0, select bank0
	btfss	(97/8),(97)&7
	goto	u73_21
	goto	u73_20
u73_21:
	goto	i1l382
u73_20:
	line	193
	
i1l1654:	
;main.c: 192: {
;main.c: 193: GIE = 0;
	bcf	(95/8),(95)&7
	line	195
	
i1l1656:	
;main.c: 195: if (tick < tickCount)
	movf	(_tickCount+2),w	;volatile
	subwf	(_tick+2),w	;volatile
	skipz
	goto	u74_25
	movf	(_tickCount+1),w	;volatile
	subwf	(_tick+1),w	;volatile
	skipz
	goto	u74_25
	movf	(_tickCount),w	;volatile
	subwf	(_tick),w	;volatile
u74_25:
	skipnc
	goto	u74_21
	goto	u74_20
u74_21:
	goto	i1l380
u74_20:
	line	197
	
i1l1658:	
;main.c: 196: {
;main.c: 197: tick++;
	incf	(_tick),f	;volatile
	skipnz
	incf	(_tick+1),f	;volatile
	skipnz
	incf	(_tick+2),f	;volatile
	line	200
	
i1l1660:	
;main.c: 200: TMR2IF = 0;
	bcf	(97/8),(97)&7
	line	201
	
i1l1662:	
;main.c: 201: StartTimer2();
	fcall	i1_StartTimer2
	line	202
;main.c: 202: }
	goto	i1l1666
	line	203
	
i1l380:	
	line	205
;main.c: 203: else
;main.c: 204: {
;main.c: 205: tick = 0;
	clrf	(_tick)	;volatile
	clrf	(_tick+1)	;volatile
	clrf	(_tick+2)	;volatile
	line	206
;main.c: 206: RA1 = 0;
	bcf	(41/8),(41)&7
	line	207
;main.c: 207: RA0 = 0;
	bcf	(40/8),(40)&7
	line	208
;main.c: 208: TMR2ON = 0;
	bcf	(146/8),(146)&7
	line	209
;main.c: 209: TMR2IE = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	(1121/8)^080h,(1121)&7
	line	210
;main.c: 210: tickCount = 0;
	bcf	status, 5	;RP0=0, select bank0
	clrf	(_tickCount)	;volatile
	clrf	(_tickCount+1)	;volatile
	clrf	(_tickCount+2)	;volatile
	line	211
;main.c: 211: time = 0;
	clrf	(_time)	;volatile
	clrf	(_time+1)	;volatile
	clrf	(_time+2)	;volatile
	line	212
	
i1l1664:	
;main.c: 212: PORTB &= 0b00001111;
	movlw	(0Fh)
	andwf	(6),f	;volatile
	line	215
	
i1l1666:	
;main.c: 214: }
;main.c: 215: TMR2IF = 0;
	bcf	(97/8),(97)&7
	line	216
	
i1l1668:	
;main.c: 216: GIE = 1;
	bsf	(95/8),(95)&7
	line	221
	
i1l382:	
	movf	(??_isr+2),w
	movwf	pclath
	movf	(??_isr+1),w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_isr
	__end_of_isr:
;; =============== function _isr ends ============

	signat	_isr,90
	global	i1_StartTimer2
psect	text252,local,class=CODE,delta=2
global __ptext252
__ptext252:

;; *************** function i1_StartTimer2 *****************
;; Defined at:
;;		line 242 in file "W:\PIC\Fan_IR_Timer\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_isr
;; This function uses a non-reentrant model
;;
psect	text252
	file	"W:\PIC\Fan_IR_Timer\main.c"
	line	242
	global	__size_ofi1_StartTimer2
	__size_ofi1_StartTimer2	equ	__end_ofi1_StartTimer2-i1_StartTimer2
	
i1_StartTimer2:	
	opt	stack 4
; Regs used in i1_StartTimer2: [wreg+status,2]
	line	244
	
i1l1784:	
;main.c: 244: TMR2 = 0x00;
	clrf	(17)	;volatile
	line	245
	
i1l1786:	
;main.c: 245: PR2 = 0xFF;
	movlw	(0FFh)
	bsf	status, 5	;RP0=1, select bank1
	movwf	(146)^080h	;volatile
	line	246
	
i1l1788:	
;main.c: 246: T2CKPS0 = 1; T2CKPS1 = 1;
	bcf	status, 5	;RP0=0, select bank0
	bsf	(144/8),(144)&7
	
i1l1790:	
	bsf	(145/8),(145)&7
	line	247
	
i1l1792:	
;main.c: 247: TOUTPS2 = 1; TOUTPS1 = 1; TOUTPS3 = 1; TOUTPS0 = 1;
	bsf	(149/8),(149)&7
	
i1l1794:	
	bsf	(148/8),(148)&7
	
i1l1796:	
	bsf	(150/8),(150)&7
	
i1l1798:	
	bsf	(147/8),(147)&7
	line	248
	
i1l1800:	
;main.c: 248: TMR2IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bsf	(1121/8)^080h,(1121)&7
	line	249
	
i1l1802:	
;main.c: 249: TMR2ON = 1;
	bcf	status, 5	;RP0=0, select bank0
	bsf	(146/8),(146)&7
	line	250
	
i1l388:	
	return
	opt stack 0
GLOBAL	__end_ofi1_StartTimer2
	__end_ofi1_StartTimer2:
;; =============== function i1_StartTimer2 ends ============

	signat	i1_StartTimer2,88
	global	_StartTimer
psect	text253,local,class=CODE,delta=2
global __ptext253
__ptext253:

;; *************** function _StartTimer *****************
;; Defined at:
;;		line 229 in file "W:\PIC\Fan_IR_Timer\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 40/20
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_isr
;; This function uses a non-reentrant model
;;
psect	text253
	file	"W:\PIC\Fan_IR_Timer\main.c"
	line	229
	global	__size_of_StartTimer
	__size_of_StartTimer	equ	__end_of_StartTimer-_StartTimer
	
_StartTimer:	
	opt	stack 4
; Regs used in _StartTimer: [wreg]
	line	230
	
i1l1670:	
;main.c: 230: T1CKPS1 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	(133/8),(133)&7
	line	231
;main.c: 231: T1CKPS0 = 0;
	bcf	(132/8),(132)&7
	line	233
;main.c: 233: T1OSCEN = 0;
	bcf	(131/8),(131)&7
	line	234
;main.c: 234: TMR1CS = 0;
	bcf	(129/8),(129)&7
	line	235
;main.c: 235: TMR1IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bsf	(1120/8)^080h,(1120)&7
	line	236
	
i1l1672:	
;main.c: 236: TMR1H = 0b11111011; TMR1L = 0b00010110;
	movlw	(0FBh)
	bcf	status, 5	;RP0=0, select bank0
	movwf	(15)	;volatile
	movlw	(016h)
	movwf	(14)	;volatile
	line	238
	
i1l1674:	
;main.c: 238: TMR1ON = 1;
	bsf	(128/8),(128)&7
	line	239
	
i1l385:	
	return
	opt stack 0
GLOBAL	__end_of_StartTimer
	__end_of_StartTimer:
;; =============== function _StartTimer ends ============

	signat	_StartTimer,88
psect	text254,local,class=CODE,delta=2
global __ptext254
__ptext254:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
