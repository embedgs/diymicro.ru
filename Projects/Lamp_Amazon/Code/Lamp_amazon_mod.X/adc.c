#include <xc.h>

void InitADC()
{
	//Initialiation of the ADC module
	VCFG0 = 0; //VDD is a reference
	//Configure the Tad to 8Tosc (for Fosc=8MHz) - 1us
	ADCS2 = 0;
	ADCS1 = 0;
	ADCS0 = 1;
	//Configure the acqusition time (should be more than 2.5us for 2.5Kohm source), with 1us we could make 2us or 4us
	ACQT2 = 0;
	ACQT1 = 1;
	ACQT0 = 0;
    ADFM = 1; //right justification
	ADON = 1;
	ADIE = 1;
	ADIF = 0;
}

void adcSelChan(char mux_byte)
{
	/*
		Select of the ADC channel
		00 - AN0 (RA0)
		01 - AN1 (RA1)
		10 - AN2 (RA4)
		11 - AN3 (RA6)
	*/
	CHS1 = (mux_byte&0x02)>>1;
	CHS0 = mux_byte&0x01;
}

void adcAN0digitalOne()
{
	//Make AN0 channel digital output 1
		PCFG0 = 1;
		TRISA0 = 0;
		PORTAbits.RA0 = 1;
}

void adcAN0digitalZero()
{
	//Make AN0 channel digital output 1
		PCFG0 = 1;
		TRISA0 = 0;
		PORTAbits.RA0 = 0;
}

void adcAN1digitalOne()
{
	//Make AN1 channel digital output 1
		PCFG1 = 1;
		TRISA1 = 0;
		PORTAbits.RA1 = 1;
}

void adcAN1digitalZero()
{
	//Make AN1 channel digital output 1
		PCFG1 = 1;
		TRISA1 = 0;
		PORTAbits.RA1 = 0;
}


void adcAN2digitalOne()
{
	//Make AN2 channel digital output 1
		PCFG2 = 1;
		TRISA4 = 0;
		PORTAbits.RA4 = 1;
}

void adcAN2digitalZero()
{
	//Make AN2 channel digital output 1
		PCFG2 = 1;
		TRISA4 = 0;
		PORTAbits.RA4 = 0;
}

void adcAN3digitalOne()
{
	//Make AN3 channel digital output 1
		PCFG3 = 1;
		TRISA6 = 0;
		PORTAbits.RA6 = 1;
}

void adcAN0analogIn()
{
	PCFG0 = 0;
	TRISA0 = 1;
}

void adcAN1analogIn()
{
	PCFG1 = 0;
	TRISA1 = 1;
}

void adcAN2analogIn()
{
	PCFG2 = 0;
	TRISA4 = 1;
}

void adcAN3analogIn()
{
	PCFG3 = 0;
	TRISA6 = 1;
}

void adcCH0Pol()
{
	adcSelChan(0);
    adcAN0digitalOne();      
    adcAN0analogIn();  
}

void adcCH1Pol()
{
	adcSelChan(1);
    adcAN1digitalOne();      
    adcAN1analogIn();  
}

void adcCH2Pol()
{
	adcSelChan(2);
    adcAN2digitalOne();      
    adcAN2analogIn();  
}