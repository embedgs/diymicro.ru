#include <xc.h>
#include "uart.h"
#include "adc.h"

#define _XTAL_FREQ 8000000 //The speed of your internal(or)external oscillator


// PIC18F1230 Configuration Bit Settings

// 'C' source line config statements

// CONFIG1H
#pragma config OSC = INTIO2     // Oscillator (Internal oscillator, port function on RA6 and RA7)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config IESO = OFF       // Internal/External Oscillator Switchover bit (Oscillator Switchover mode disabled)

// CONFIG2L
#pragma config PWRT = OFF       // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOR = OFF       // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware only (SBOREN is disabled))
#pragma config BORV = 3         // Brown-out Reset Voltage bits (Minimum setting)

// CONFIG2H
#pragma config WDT = OFF        // Watchdog Timer Enable bit (WDT disabled (control is placed on the SWDTEN bit))
#pragma config WDTPS = 256    // Watchdog Timer Postscale Select bits (1:32768)

// CONFIG3L
#pragma config PWMPIN = OFF     // PWM Output Pins Reset State Control bit (PWM outputs disabled upon Reset)
#pragma config LPOL = HIGH      // Low-Side Transistors Polarity bit (Even PWM Output Polarity Control bit) (PWM0, PWM2 and PWM4 are active-high (default))
#pragma config HPOL = HIGH      // High Side Transistors Polarity bit (Odd PWM Output Polarity Control bit) (PWM1, PWM3 and PWM5 are active-high (default))

// CONFIG3H
#pragma config FLTAMX = RA5     // FLTA Mux bit (FLTA input is muxed onto RA5)
#pragma config T1OSCMX = LOW    // T1OSO/T1CKI MUX bit (T1OSO/T1CKI pin resides on RB2)
#pragma config MCLRE = ON       // Master Clear Enable bit (MCLR pin enabled, RA5 input pin disabled)

// CONFIG4L
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable bit (Reset on stack overflow/underflow enabled)
#pragma config BBSIZ = BB256    // Boot Block Size Select bits (256 Words (512 Bytes) Boot Block size)
#pragma config XINST = OFF      // Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled)

// CONFIG5L
#pragma config CP0 = OFF        // Code Protection bit Block 0 (000400-0007FF) (Block 0 is not code-protected)
#pragma config CP1 = OFF        // Code Protection bit Block 1 (000800-000FFF) (Block 1 is not code-protected)

// CONFIG5H
#pragma config CPB = OFF        // Code Protection bit (Boot Block Memory Area) (Boot Block is not code-protected)
#pragma config CPD = OFF        // Code Protection bit (Data EEPROM) (Data EEPROM is not code-protected)

// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection bit Block 0 (000400-0007FF) (Block 0 is not write-protected)
#pragma config WRT1 = OFF       // Write Protection bit Block 1 (000800-000FFF) (Block 1 is not write-protected)

// CONFIG6H
#pragma config WRTC = OFF       // Write Protection bit (Configuration Registers) (Configuration registers are not write-protected)
#pragma config WRTB = OFF       // Write Protection bit (Boot Block Memory Area) (Boot Block is not write-protected)
#pragma config WRTD = OFF       // Write Protection bit (Data EEPROM) (Data EEPROM is not write-protected)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection bit Block 0 (000400-0007FF) (Block 0 is not protected from table reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection bit Block 1 (000800-000FFF) (Block 1 is not protected from table reads executed in other blocks)

// CONFIG7H
#pragma config EBTRB = OFF      // Table Read Protection bit (Boot Block Memory Area) (Boot Block is not protected from table reads executed in other blocks)



volatile __bit timer0_event = 0;
volatile __bit single_sensor = 0;
volatile __bit adc_conversion_end = 0;
const int sensor1_ref[16]= {1019, 638, 497, 388, 303, 237, 185, 145, 114, 89, 69, 55, 43, 34, 27, 21};
const int sensor2_ref[16]= {1019, 611, 466, 356, 272, 208, 158, 121, 92, 70, 53, 41, 30, 22, 17, 13};
const int sensor3_ref[16]= {1018, 661, 523, 413, 326, 258, 203, 160, 127, 99, 78, 61, 47, 37, 29, 21};
const char threshold_delta_lamp = 6;

unsigned char timer_counter = 0; 
unsigned char samples_count = 0;
char temp0[16];
char temp1[16]; 
char temp2[16];
char temp3[16]; 
char temp4[16];
char temp5[16]; 
int adc_global_data[16];
char sensor1_touch_counts = 0;
char pulses_counter = 0;

int adc_data_temp = 0;
char i =0;
//unsigned int ref_sensor1[16][16];
//unsigned int ref_sensor2[16][16];

void timer0_1ms();
void timer0_33ms();
void timer0_1s();
void timer0_4s();
__bit IsSensorTouched(int ref_data[16],int adc_data[16], char threshold_delta);
void ADCtoGlobal(char adc_msb[16], char adc_lsb[16]);

void main() 
{
//INTOSC TO 8MHz
 IRCF2 = 1;
 IRCF1 = 1;
 IRCF0 = 1;
 SCS1 = 1;
//INTOSC setup    

 InitUart();
 writeDataUart((char *) "\r\ndiymicro.org -- Lamp fix project\r\n");
 InitADC();
 ADIE = 0;
 

 
 IPEN = 0;
 RCIE = 0; //dont need uart rx interrupts
 GIE = 1;
 PEIE = 1;

 single_sensor = 1;     //starting from the single sensor mode
 timer0_1s();//initiallization of the first run

 while(1)
 {
    
    if (timer0_event)   
    {
        
         writeDataUart((char *) "\r\nSensor 1: "); 
       
        ADIE = 0;    
        if (timer_counter == 0)
        {
            adcCH0Pol();
            timer_counter = 1;
        } //if counter 0
            else
            {
                if (timer_counter == 1)
                {
                    adcCH1Pol();
                    timer_counter = 2;
                }    //if counter 1
                    else
                    {
                        if (timer_counter == 2)
                        {
                            adcCH2Pol();    
                            timer_counter = 3;
                        }    //if counter 1
                    }//else 2
            }//else 1

             
            if(timer_counter==4)            //when all 3 are finished
              {
                writeDataUart((char *) "\033c");
                writeDataUart((char *) "Printing the data from ADC\r\n");
               
              writeDataUart((char *) "\r\nSensor 1: ");
              for(i=0;i<16;i++)
              {
                adc_data_temp = temp0[i];
                adc_data_temp = (adc_data_temp<<8) | temp1[i]; 
                NumToUart(adc_data_temp);
                writeDataUart((char *) " ");
              } 
              writeDataUart((char *) "\r\nSensor 2: ");
             for(i=0;i<16;i++)
              {
                adc_data_temp = temp2[i];
                adc_data_temp = (adc_data_temp<<8) | temp3[i]; 
                NumToUart(adc_data_temp);
                writeDataUart((char *) " ");
              } 
              writeDataUart((char *) "\r\nSensor 3: ");
              for(i=0;i<16;i++)
              {
                adc_data_temp = temp4[i];
                adc_data_temp = (adc_data_temp<<8) | temp5[i]; 
                NumToUart(adc_data_temp);
                writeDataUart((char *) " ");
              } 
              timer_counter = 0;
              __delay_ms(200);__delay_ms(200);__delay_ms(200);__delay_ms(200);__delay_ms(200);
              __delay_ms(200);__delay_ms(200);__delay_ms(200);__delay_ms(200);__delay_ms(200);
              __delay_ms(200);__delay_ms(200);__delay_ms(200);__delay_ms(200);__delay_ms(200);
              __delay_ms(200);__delay_ms(200);__delay_ms(200);__delay_ms(200);__delay_ms(200);

          }


        TMR0IE = 1;
        TMR0ON = 1;
        
        timer0_1s();
                ADIE = 1;
        
        timer0_event = 0;
        GO = 1;
    }//if timer0

    //adc conversion ended, analizing outputs and restarting timer


 }//while(1)

}//main()


void timer0_1ms()       //timer0 for 1ms
{
 //launching the timer for reference calibration (~1ms)
 T0CS = 0;
 //T016BIT = 0; //16 bits counter
 T016BIT = 1; //8 bits counter
 PSA = 0;       //prescaler is used
 T0PS2 = 0;
 T0PS1 = 1;
 T0PS0 = 0;
 TMR0ON = 1;
 TMR0IF = 0;
 TMR0IE = 1;
 //end of timer configuration
}//timer0


void timer0_33ms()       //timer0 for ~32.768ms
{
 //launching the timer for reference calibration (~1ms)
 T0CS = 0;
 //T016BIT = 0; //16 bits counter
 T016BIT = 1; //8 bits counter
 PSA = 0;       //prescaler is used
 T0PS2 = 1;
 T0PS1 = 1;
 T0PS0 = 1;
 TMR0ON = 1;
 TMR0IF = 0;
 TMR0IE = 1;
 //end of timer configuration
}//timer0


void timer0_1s()       //timer0 for ~1s
{
 //launching the timer for reference calibration (~1ms)
 T0CS = 0;
 T016BIT = 0; //16 bits counter
 //T016BIT = 1; //8 bits counter
 PSA = 0;       //prescaler is used
 T0PS2 = 1;
 T0PS1 = 0;
 T0PS0 = 0;
 TMR0ON = 1;
 TMR0IF = 0;
 TMR0IE = 1;
 //end of timer configuration
}//timer0

void timer0_4s()       //timer0 for ~4s
{
 //launching the timer for reference calibration (~1ms)
 T0CS = 0;
 T016BIT = 0; //16 bits counter
 //T016BIT = 1; //8 bits counter
 PSA = 0;       //prescaler is used
 T0PS2 = 1;
 T0PS1 = 1;
 T0PS0 = 0;
 TMR0ON = 1;
 TMR0IF = 0;
 TMR0IE = 1;
 //end of timer configuration
}//timer0

//post processing adc data to determine if sensor is being touched
__bit IsSensorTouched(int ref_data[16],int adc_data[16], char threshold_delta)
{
    char i = 0;
    char positive_count = 0;

    for (i=0;i<16;i++)
    {
        if (adc_data[i]>(ref_data[i]+threshold_delta))
            positive_count++;
    }//

    if (positive_count>=10)
        return 0x01;
    else
        return 0x00;
}

void ADCtoGlobal(char adc_msb[16], char adc_lsb[16])
{
    char i = 0;

    for(i=0;i<16;i++)
    {
        adc_global_data[i]= adc_msb[i];
        adc_global_data[i] = (adc_global_data[i]<<8) | adc_lsb[i]; 
    }
}

void __interrupt(high_priority) HighISR(void)
{

   
    if (TMR0IF)
    {
        //adcAN0digitalOne();
        //adcAN0analogIn();
        timer0_event = 1;
        TMR0IF = 0;
        TMR0IE = 0;
        TMR0ON = 0;
    }

    if (ADIF)
    {  
         
        if(timer_counter==1)
        {
            temp0[samples_count] = ADRESH;
            temp1[samples_count] = ADRESL;    
        }
            else
            {
                if (timer_counter == 2)
                {
                    temp2[samples_count] = ADRESH;
                    temp3[samples_count] = ADRESL;   
                }
                else
                {
                   if (timer_counter == 3)
                   {
                    temp4[samples_count] = ADRESH;
                    temp5[samples_count] = ADRESL;  
                   }
                }
            }    

          if (samples_count>=15)
          {
              samples_count=0;
              if (timer_counter == 3)
                    timer_counter = 4;

          }else
          {
              samples_count++;
              GO = 1;
          }
        
        
        ADIF = 0;       
    }//adif    
    
}//high priority interrupts