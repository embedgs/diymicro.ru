#include <xc.h>

void InitUart()  //Initialization of UART 
{
    SPEN = 1;
    TRISA3 = 1;
    TRISA2 = 1;
    
    SYNC = 0;
    BRG16 = 0;
    BRGH = 0;
    SPBRG = 12; //9615 baudrate with 8MHz
    TX9 = 0; //no need to use 9th bit
    TXEN = 1;

    //Reciever part
    RCIE = 1; //enable RX interrupts
    RX9 = 0;
    CREN = 1;

}//InitUart

char BusyUSART(void)
{
  if(!TXSTAbits.TRMT)  // Is the transmit shift register empty
    return 1;          // No, return FALSE
  return 0;            // Return TRUE
}


void writeByteUart(char tx_data)  //Writing of a single byte
{
  TXREG = tx_data;      // Write the data byte to the USART
}//writeByteUart

void writeDataUart( char *data)
{
  do
  {  // Transmit a byte
    while(BusyUSART());
    writeByteUart(*data);
  } while( *data++ );
}

void NumToUart(unsigned int Num)                                                //Число в уарт
{

  unsigned int bignum = 10000;
  unsigned char numtemp = 5;

  if (!Num)
  {
      writeByteUart('0');         //Выталкиеваем все разряды - от старшего к младшему
      while(BusyUSART());                                                       //Ждем пока освободится модуль иначе будут прострелы
  }
  else 
  {
	  while(numtemp>0)                                                             //Определяем сколько разрядов имеет наше число
	  {
	    if (Num/bignum)
	        break;
	    numtemp--;
	    bignum = bignum / 10;  
	  }  
	
	
	
	  for (unsigned char i = numtemp; i>0; i--)
	    {
	      writeByteUart( (Num - (Num/(bignum*10))*bignum*10 )/bignum + '0');         //Выталкиеваем все разряды - от старшего к младшему
	      while(BusyUSART());                                                       //Ждем пока освободится модуль иначе будут прострелы
	      bignum = bignum/10;
	    }
   } 
}


char ReadUart()
{
    char data;   // Holds received data
    data = RCREG;                      // Read data

  return (data);                     // Return the received data

}