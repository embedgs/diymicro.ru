#include <xc.h>
#include "uart.h"
#include "adc.h"

#define _XTAL_FREQ 8000000 //The speed of your internal(or)external oscillator


// PIC18F1230 Configuration Bit Settings

// 'C' source line config statements

// CONFIG1H
#pragma config OSC = INTIO1     // Oscillator (Internal oscillator, port function on RA6 and RA7)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config IESO = OFF       // Internal/External Oscillator Switchover bit (Oscillator Switchover mode disabled)

// CONFIG2L
#pragma config PWRT = OFF       // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOR = BOHW       // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware only (SBOREN is disabled))
#pragma config BORV = 3         // Brown-out Reset Voltage bits (Minimum setting)

// CONFIG2H
#pragma config WDT = OFF        // Watchdog Timer Enable bit (WDT disabled (control is placed on the SWDTEN bit))
#pragma config WDTPS = 32768    // Watchdog Timer Postscale Select bits (1:32768)

// CONFIG3L
#pragma config PWMPIN = OFF     // PWM Output Pins Reset State Control bit (PWM outputs disabled upon Reset)
#pragma config LPOL = HIGH      // Low-Side Transistors Polarity bit (Even PWM Output Polarity Control bit) (PWM0, PWM2 and PWM4 are active-high (default))
#pragma config HPOL = HIGH      // High Side Transistors Polarity bit (Odd PWM Output Polarity Control bit) (PWM1, PWM3 and PWM5 are active-high (default))

// CONFIG3H
#pragma config FLTAMX = RA5     // FLTA Mux bit (FLTA input is muxed onto RA5)
#pragma config T1OSCMX = LOW    // T1OSO/T1CKI MUX bit (T1OSO/T1CKI pin resides on RB2)
#pragma config MCLRE = ON       // Master Clear Enable bit (MCLR pin enabled, RA5 input pin disabled)

// CONFIG4L
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable bit (Reset on stack overflow/underflow enabled)
#pragma config BBSIZ = BB256    // Boot Block Size Select bits (256 Words (512 Bytes) Boot Block size)
#pragma config XINST = OFF      // Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled)

// CONFIG5L
#pragma config CP0 = OFF        // Code Protection bit Block 0 (000400-0007FF) (Block 0 is not code-protected)
#pragma config CP1 = OFF        // Code Protection bit Block 1 (000800-000FFF) (Block 1 is not code-protected)

// CONFIG5H
#pragma config CPB = OFF        // Code Protection bit (Boot Block Memory Area) (Boot Block is not code-protected)
#pragma config CPD = OFF        // Code Protection bit (Data EEPROM) (Data EEPROM is not code-protected)

// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection bit Block 0 (000400-0007FF) (Block 0 is not write-protected)
#pragma config WRT1 = OFF       // Write Protection bit Block 1 (000800-000FFF) (Block 1 is not write-protected)

// CONFIG6H
#pragma config WRTC = OFF       // Write Protection bit (Configuration Registers) (Configuration registers are not write-protected)
#pragma config WRTB = OFF       // Write Protection bit (Boot Block Memory Area) (Boot Block is not write-protected)
#pragma config WRTD = OFF       // Write Protection bit (Data EEPROM) (Data EEPROM is not write-protected)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection bit Block 0 (000400-0007FF) (Block 0 is not protected from table reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection bit Block 1 (000800-000FFF) (Block 1 is not protected from table reads executed in other blocks)

// CONFIG7H
#pragma config EBTRB = OFF      // Table Read Protection bit (Boot Block Memory Area) (Boot Block is not protected from table reads executed in other blocks)



volatile __bit timer0_event = 0;
volatile __bit timer0_launched = 0;
volatile __bit single_sensor = 0;
volatile __bit adc_conversion_end = 0;
volatile char sensor_in_work = 0;
const int sensor1_ref[16]= {1019, 638, 497, 388, 303, 237, 185, 145, 114, 89, 69, 55, 43, 34, 27, 21};
const int sensor2_ref[16]= {1019, 611, 466, 356, 272, 208, 158, 121, 92, 70, 53, 41, 30, 22, 17, 13};
const int sensor3_ref[16]= {1018, 661, 523, 413, 326, 258, 203, 160, 127, 99, 78, 61, 47, 37, 29, 21};
const char threshold_delta_lamp = 10;

unsigned char timer_counter = 0; 
unsigned char samples_count = 0;
char temp0[16];
char temp1[16]; 
char temp2[16];
char temp3[16]; 
char temp4[16];
char temp5[16]; 
int adc_global_data[16];
char sensor1_touch_counts = 0;
char sensor2_touch_counts = 0;
char sensor3_touch_counts = 0;
char pulses_counter = 0;

int adc_data_temp = 0;
char i =0;
//unsigned int ref_sensor1[16][16];
//unsigned int ref_sensor2[16][16];

void timer0_1ms();
void timer0_4ms();
void timer0_8ms();
void timer0_33ms();
void timer0_250ms();
void timer0_500ms(); 
void timer0_1s();
void timer0_4s();
__bit IsSensorTouched(int ref_data[16],int adc_data[16], char threshold_delta);
void ADCtoGlobal(char adc_msb[16], char adc_lsb[16]);

void main() 
{
//INTOSC TO 8MHz
 IRCF2 = 1;
 IRCF1 = 1;
 IRCF0 = 1;
 SCS1 = 1;
//INTOSC setup    

 InitUart();
 writeDataUart((char *) "\r\ndiymicro.org -- Lamp fix project\r\n");
 InitADC();
 ADIE = 0;
 

 
 IPEN = 0;
 RCIE = 0; //dont need uart rx interrupts
 GIE = 1;
 PEIE = 1;

 single_sensor = 0;     //starting from the single sensor mode
 timer0_1s();//initiallization of the first run

 while(1)
 {
    
  //Waking up - working with a single sensor AN0
    if (single_sensor)
    {

        //Forming 6 pulses

                if ((pulses_counter==0)&&(timer0_event)&&(!adc_conversion_end))//first pulse is about to go
                {
                     pulses_counter = 1;
                     timer0_event = 0;
                     timer0_33ms();
                     adcCH0Pol();
                     ADIE = 1;
                     GO = 1;                
                } //if pulses counter = 0
                    else
                    {
                          if ((pulses_counter>0)&&(timer0_event)&&(!adc_conversion_end))
                          {
                                if (pulses_counter>5)   //time to wait a longer time
                                {
                                    pulses_counter = 0; //reset a pulse counter
                                    timer0_event = 0;   //reset a timer0 flag just in case
                                    timer0_4s();        //wait about 4 seconds
                                    //writeDataUart((char *) "debug: we are in the 6th pulse area\r\n");
                                }   else
                                        {
                                           pulses_counter++;
                                           timer0_event = 0;    //rest a timer0 flag
                                           timer0_33ms();
                                           adcCH0Pol();
                                           ADIE = 1;
                                           GO = 1;     
                                        }//else

                          } //if for next pulses 
                    }//else


        //End of Forming 6 pulses
        
        //Processing of the data from ADC
        if ((adc_conversion_end)&&(!timer0_event))    
        {
                 ADCtoGlobal(temp0,temp1);
                 if (IsSensorTouched(sensor1_ref,adc_global_data,threshold_delta_lamp))
                 {
                     sensor1_touch_counts++;
                 }
                 adc_conversion_end = 0;
                 if (pulses_counter==6)         //last pulse in a pack
                 {
                    if (sensor1_touch_counts>2)
                        writeDataUart((char *) "Power on sensor has been touched!\r\n");
                    else
                        writeDataUart((char *) "nope,nobody touched the sensor!\r\n");
                    sensor1_touch_counts = 0;
                 }

        }   //if (adc_conversion_end) 


    }//if single sensor
    //END of waking up procedure and a single sensor AN0

    //Three sensor launching and processing

    if (!single_sensor)
    {
      if (!adc_conversion_end)
      {  
        if (pulses_counter==0)//check if we dont have the timer launched yet
        {
            timer0_250ms();
            pulses_counter = 1;
            sensor_in_work = 0;
            //writeDataUart((char *) "debug: first stage - time 1 s\r\n");
        } 
            else
            {
                 if (timer0_event)//first pulse is about to go
                {
                     
                     timer0_event = 0;
                     //writeDataUart((char *) "debug: second stage - entering to the polling\r\n");    

                    switch (sensor_in_work)
                    {
                        case 0 :
                        {
                           // writeDataUart((char *) "debug: sensor 0 stage\r\n");  
                            sensor_in_work = 1;
                            timer0_4ms();
                            adcCH0Pol();
                            ADIE = 1;
                            GO = 1;
                            break;    
                        }
                        case 1 :
                        {
                           // writeDataUart((char *) "debug: sensor 1 stage\r\n");  
                            sensor_in_work = 2;
                            timer0_4ms();
                            adcCH1Pol();
                            ADIE = 1;
                            GO = 1;
                            break;    
                        }
                        case 2 :
                        {
                            //writeDataUart((char *) "debug: sensor 2 stage\r\n");  
                            sensor_in_work = 3;
                            timer0_4ms();
                            adcCH2Pol();
                            ADIE = 1;
                            GO = 1;
                            break;    
                        }
                        case 3 :
                        {
                                                sensor_in_work = 0;
                                                //writeDataUart((char *) "debug: end of the sensor sweep\r\n");
                                                if (pulses_counter<9)
                                                    {
                                                        pulses_counter++;
                                                        ADIE = 0;
                                                        timer0_33ms();
                                                    }    
                                                else
                                                {
                                                    pulses_counter = 0;
                                                    timer0_250ms();    //another round is coming
                                                }
                        }                                                                        

                       

                    }//switch

                     
                } //else  

            }//else
        } //adc conversion ended    

        

        //Processing of the data from ADC
        if ((adc_conversion_end)&&(!timer0_event))    
        {
                 ADCtoGlobal(temp0,temp1);
                 if (IsSensorTouched(sensor1_ref,adc_global_data,threshold_delta_lamp))
                    sensor1_touch_counts++;
                 
                 ADCtoGlobal(temp2,temp3);
                 if (IsSensorTouched(sensor2_ref,adc_global_data,threshold_delta_lamp))
                    sensor2_touch_counts++;
                 
                 ADCtoGlobal(temp4,temp5);
                 if (IsSensorTouched(sensor3_ref,adc_global_data,threshold_delta_lamp))
                    sensor3_touch_counts++;


                 adc_conversion_end = 0;
                 if (pulses_counter==9)         //last pulse in a pack
                 {
                    
                    if (((sensor1_touch_counts)>(sensor2_touch_counts))&&((sensor1_touch_counts)>(sensor3_touch_counts)))
                    {
                        if (sensor1_touch_counts>4)
                            writeDataUart((char *) "sensor 1 has been touched!\r\n");
                    }
                
                    if (((sensor2_touch_counts)>(sensor1_touch_counts))&&((sensor2_touch_counts)>(sensor3_touch_counts)))
                    {
                        if (sensor2_touch_counts>4)
                            writeDataUart((char *) "sensor 2 has been touched!\r\n");
                    }

                    if (((sensor3_touch_counts)>(sensor1_touch_counts))&&((sensor3_touch_counts)>(sensor2_touch_counts)))
                    {
                        if (sensor3_touch_counts>4)
                            writeDataUart((char *) "sensor 3 has been touched!\r\n");
                    }

                    sensor1_touch_counts = 0;
                    sensor2_touch_counts = 0;
                    sensor3_touch_counts = 0;

                 }

        }   //if (adc_conversion_end)     



    }//if (!single_sensor)

    //End of three sensors processing



 }//while(1)

}//main()


void timer0_1ms()       //timer0 for 1ms
{
 //launching the timer for reference calibration (~1ms)
 timer0_launched = 1;
 T0CS = 0;
 //T016BIT = 0; //16 bits counter
 T016BIT = 1; //8 bits counter
 PSA = 0;       //prescaler is used
 T0PS2 = 0;
 T0PS1 = 1;
 T0PS0 = 0;
 TMR0ON = 1;
 TMR0IF = 0;
 TMR0IE = 1;
 //end of timer configuration
}//timer0

void timer0_4ms()       //timer0 for 8ms
{
 //launching the timer for reference calibration (~1ms)
 timer0_launched = 1;
 T0CS = 0;
 //T016BIT = 0; //16 bits counter
 T016BIT = 1; //8 bits counter
 PSA = 0;       //prescaler is used
 T0PS2 = 1;
 T0PS1 = 0;
 T0PS0 = 0;
 TMR0ON = 1;
 TMR0IF = 0;
 TMR0IE = 1;
 //end of timer configuration
}//timer0

void timer0_8ms()       //timer0 for 8ms
{
 //launching the timer for reference calibration (~1ms)
 timer0_launched = 1;
 T0CS = 0;
 //T016BIT = 0; //16 bits counter
 T016BIT = 1; //8 bits counter
 PSA = 0;       //prescaler is used
 T0PS2 = 1;
 T0PS1 = 0;
 T0PS0 = 1;
 TMR0ON = 1;
 TMR0IF = 0;
 TMR0IE = 1;
 //end of timer configuration
}//timer0


void timer0_33ms()       //timer0 for ~32.768ms
{
 //launching the timer for reference calibration (~1ms)
 timer0_launched = 1;
 T0CS = 0;
 //T016BIT = 0; //16 bits counter
 T016BIT = 1; //8 bits counter
 PSA = 0;       //prescaler is used
 T0PS2 = 1;
 T0PS1 = 1;
 T0PS0 = 1;
 TMR0ON = 1;
 TMR0IF = 0;
 TMR0IE = 1;
 //end of timer configuration
}//timer0


void timer0_250ms()       //timer0 for ~0.25s
{
 //launching the timer for reference calibration (~1ms)
 timer0_launched = 1;
 T0CS = 0;
 T016BIT = 0; //16 bits counter
 //T016BIT = 1; //8 bits counter
 PSA = 0;       //prescaler is used
 T0PS2 = 0;
 T0PS1 = 1;
 T0PS0 = 0;
 TMR0ON = 1;
 TMR0IF = 0;
 TMR0IE = 1;
 //end of timer configuration
}//timer0

void timer0_500ms()       //timer0 for ~0.5s
{
 //launching the timer for reference calibration (~1ms)
 timer0_launched = 1;
 T0CS = 0;
 T016BIT = 0; //16 bits counter
 //T016BIT = 1; //8 bits counter
 PSA = 0;       //prescaler is used
 T0PS2 = 0;
 T0PS1 = 1;
 T0PS0 = 1;
 TMR0ON = 1;
 TMR0IF = 0;
 TMR0IE = 1;
 //end of timer configuration
}//timer0

void timer0_1s()       //timer0 for ~1s
{
 //launching the timer for reference calibration (~1ms)
 timer0_launched = 1;
 T0CS = 0;
 T016BIT = 0; //16 bits counter
 //T016BIT = 1; //8 bits counter
 PSA = 0;       //prescaler is used
 T0PS2 = 1;
 T0PS1 = 0;
 T0PS0 = 0;
 TMR0ON = 1;
 TMR0IF = 0;
 TMR0IE = 1;
 //end of timer configuration
}//timer0

void timer0_4s()       //timer0 for ~4s
{
 //launching the timer for reference calibration (~1ms)
 timer0_launched = 1;
 T0CS = 0;
 T016BIT = 0; //16 bits counter
 //T016BIT = 1; //8 bits counter
 PSA = 0;       //prescaler is used
 T0PS2 = 1;
 T0PS1 = 1;
 T0PS0 = 0;
 TMR0ON = 1;
 TMR0IF = 0;
 TMR0IE = 1;
 //end of timer configuration
}//timer0

//post processing adc data to determine if sensor is being touched
__bit IsSensorTouched(int ref_data[16],int adc_data[16], char threshold_delta)
{
    char i = 0;
    char positive_count = 0;

    for (i=0;i<16;i++)
    {
        if (adc_data[i]>(ref_data[i]+threshold_delta))
            positive_count++;
    }//

    if (positive_count>=10)
        return 0x01;
    else
        return 0x00;
}

void ADCtoGlobal(char adc_msb[16], char adc_lsb[16])
{
    char i = 0;

    for(i=0;i<16;i++)
    {
        adc_global_data[i]= adc_msb[i];
        adc_global_data[i] = (adc_global_data[i]<<8) | adc_lsb[i]; 
    }
}

void __interrupt(high_priority) HighISR(void)
{

   
    if (TMR0IF)
    {
        //adcAN0digitalOne();
        //adcAN0analogIn();
        timer0_event = 1;
        timer0_launched =0;
        TMR0IF = 0;
        TMR0IE = 0;
        TMR0ON = 0;
    }

    if (ADIF)
    {  
         
        
        if (single_sensor)          //running adc for a single sensor
        {
            temp0[samples_count] = ADRESH;
            temp1[samples_count] = ADRESL;    
 

          if (samples_count>=15)
          {
            ADIE = 0;
            adc_conversion_end = 1;
            samples_count = 0;

          }else
          {
              samples_count++;
              GO = 1;
          }
          

        }//if single sensor      
            else
            {
                if (sensor_in_work == 1)
                {
                    temp0[samples_count] = ADRESH;
                    temp1[samples_count] = ADRESL;                      
                }   
                    else
                    {
                        if (sensor_in_work == 2)
                        {
                            temp2[samples_count] = ADRESH;
                            temp3[samples_count] = ADRESL;  
                        }
                            else
                            {
                                if (sensor_in_work == 3)
                                {
                                    temp4[samples_count] = ADRESH;
                                    temp5[samples_count] = ADRESL;  
                                }
                            }
                    }
             if (samples_count>=15)
               {
                 ADIE = 0;
                 samples_count = 0;
        
                }
                        else
                            {
                                samples_count++;
                                GO = 1;
                            } //samples count management   

             if ((samples_count>=15)&&(sensor_in_work==3))
             {
                ADIE = 0;
                adc_conversion_end = 1;
             }               

            }//if multi sensor



        ADIF = 0;
    }//adif    
}//high priority interrupts