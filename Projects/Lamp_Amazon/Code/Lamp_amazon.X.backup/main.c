#include <xc.h>
#include "uart.h"
#include "adc.h"

#define _XTAL_FREQ 8000000 //The speed of your internal(or)external oscillator


// PIC18F1230 Configuration Bit Settings

// 'C' source line config statements

// CONFIG1H
#pragma config OSC = INTIO1     // Oscillator (Internal oscillator, port function on RA6 and RA7)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config IESO = OFF       // Internal/External Oscillator Switchover bit (Oscillator Switchover mode disabled)

// CONFIG2L
#pragma config PWRT = OFF       // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOR = BOHW       // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware only (SBOREN is disabled))
#pragma config BORV = 3         // Brown-out Reset Voltage bits (Minimum setting)

// CONFIG2H
#pragma config WDT = OFF        // Watchdog Timer Enable bit (WDT disabled (control is placed on the SWDTEN bit))
#pragma config WDTPS = 32768    // Watchdog Timer Postscale Select bits (1:32768)

// CONFIG3L
#pragma config PWMPIN = OFF     // PWM Output Pins Reset State Control bit (PWM outputs disabled upon Reset)
#pragma config LPOL = HIGH      // Low-Side Transistors Polarity bit (Even PWM Output Polarity Control bit) (PWM0, PWM2 and PWM4 are active-high (default))
#pragma config HPOL = HIGH      // High Side Transistors Polarity bit (Odd PWM Output Polarity Control bit) (PWM1, PWM3 and PWM5 are active-high (default))

// CONFIG3H
#pragma config FLTAMX = RA5     // FLTA Mux bit (FLTA input is muxed onto RA5)
#pragma config T1OSCMX = LOW    // T1OSO/T1CKI MUX bit (T1OSO/T1CKI pin resides on RB2)
#pragma config MCLRE = ON       // Master Clear Enable bit (MCLR pin enabled, RA5 input pin disabled)

// CONFIG4L
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable bit (Reset on stack overflow/underflow enabled)
#pragma config BBSIZ = BB256    // Boot Block Size Select bits (256 Words (512 Bytes) Boot Block size)
#pragma config XINST = OFF      // Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled)

// CONFIG5L
#pragma config CP0 = OFF        // Code Protection bit Block 0 (000400-0007FF) (Block 0 is not code-protected)
#pragma config CP1 = OFF        // Code Protection bit Block 1 (000800-000FFF) (Block 1 is not code-protected)

// CONFIG5H
#pragma config CPB = OFF        // Code Protection bit (Boot Block Memory Area) (Boot Block is not code-protected)
#pragma config CPD = OFF        // Code Protection bit (Data EEPROM) (Data EEPROM is not code-protected)

// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection bit Block 0 (000400-0007FF) (Block 0 is not write-protected)
#pragma config WRT1 = OFF       // Write Protection bit Block 1 (000800-000FFF) (Block 1 is not write-protected)

// CONFIG6H
#pragma config WRTC = OFF       // Write Protection bit (Configuration Registers) (Configuration registers are not write-protected)
#pragma config WRTB = OFF       // Write Protection bit (Boot Block Memory Area) (Boot Block is not write-protected)
#pragma config WRTD = OFF       // Write Protection bit (Data EEPROM) (Data EEPROM is not write-protected)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection bit Block 0 (000400-0007FF) (Block 0 is not protected from table reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection bit Block 1 (000800-000FFF) (Block 1 is not protected from table reads executed in other blocks)

// CONFIG7H
#pragma config EBTRB = OFF      // Table Read Protection bit (Boot Block Memory Area) (Boot Block is not protected from table reads executed in other blocks)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

char rx_data_recieved = 0;
char tx_data_temp = 0;
char tmr0flag = 0;
char i = 0;
int adc_data[20];
char adc_conversion_start = 0;
char adc_conversion_ended = 0;


void main() 
{
//INTOSC TO 8MHz
 IRCF2 = 1;
 IRCF1 = 1;
 IRCF0 = 1;
 
 SCS1 = 1;
//INTOSC setup    
 
    //Switch all analog outputs to digital mode
    //ADCON1 = 0x07;
    //CMCON = 0x07;
    
 TRISB7 = 0; //TX pin set as output
 TRISB4 = 0;
 PORTBbits.RB7 = 1;
 PORTBbits.RB4 = 1;


 T0CS = 0;
 T016BIT = 1;
 PSA = 0;
 T0PS2 = 1;
 T0PS1 = 1;
 T0PS0 = 1;
 TMR0ON = 0;
 TMR0IF = 0;
 TMR0IE = 0;

 
 //trying uart
 InitUart();
 writeDataUart((char *) "\r\ndiymicro.org\r\n");

 
 InitADC();  //Init adc
 

 adcAN1digitalOne();
 adcAN2digitalZero();
 adcSelChan(1);


 IPEN = 1;
 GIE = 1;
 PEIE = 1;

 while(1)
 {



    if (rx_data_recieved)  //some data recieved
    {

        writeByteUart(rx_data_recieved);
        while(BusyUSART());

        if (rx_data_recieved == 'r')
          {
            //writeDataUart((char *) "\r\nStarting the ADC conversion\r\n");
            adc_conversion_start = 1;
            rx_data_recieved = 0;
            i=0;
            adcAN2analogIn();
            adcSelChan(2);
            ADON = 1;
            GO = 1;
          }  
        else
        {
            rx_data_recieved = 0;
            CREN = 0;
            CREN = 1;
        }
    }

    if ((adc_conversion_start)&&(adc_conversion_ended))
    {
        if (i<20)
        {
            adc_data[i] = (ADRESH << 2) | (ADRESL>>6);
            i++;
            GO = 1;
        }      

        if (i>=20)
        {
            GO = 0;
            ADON = 0;
            adc_conversion_ended = 0;
            for (i=0;i<20;i++)
            {  NumToUart(adc_data[i]);
                writeDataUart((char *)"\r\n");
            }    
            adcAN1digitalOne();
            adcAN2digitalZero();
            adcSelChan(1);
   

         }  
        adc_conversion_ended = 0;
    }

 }//while(1)

}//main()


void low_priority interrupt LowIsr(void)
{

    

}

void high_priority interrupt HighIsr(void)
{

    if (RCIF)
    {
       rx_data_recieved = ReadUart();
    }
    
    if (ADIF)
    {
        adc_conversion_ended = 1;
        ADIF = 0;
    }
    
}