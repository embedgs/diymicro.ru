#include <htc.h>
#define _XTAL_FREQ 4000000 //
#define LED GPIO4
// Config: ext reset, no code protect, no watchdog
__CONFIG(MCLRDIS & UNPROTECT & WDTEN & INTIO);

volatile bit CompFlag=0;
volatile bit flag2=0;
volatile bit currentState = 0;
volatile unsigned char CompCount = 0;
volatile unsigned int timeCount = 0;
volatile unsigned int oldTimeCount = 0;
volatile unsigned int OFFtimeCount = 0;
unsigned char i;  

void main() {
    PSA = 1;                  //Watchdog setup
    PS2 = 1;
    PS1 = 1;
    PS0 = 1;
    CLRWDT();
    for (i=0; i<20; i++)
    {
       __delay_ms(200);
       CLRWDT();
    }  
    
    CMCON=0b00000010;
    TRISIO = 0x00;   
    GPIO2 = 0;   
    LED = 0;
    T1CKPS1 = 1;
    T1CKPS0 = 1; 
     
    T1OSCEN = 0; 
    TMR1CS = 0; // Fosc/4
    GIE = 1;   
    PEIE = 1;  
    TMR1ON = 1; 
    CMIE = 1;


    CLRWDT();
        for (;;)  
        {     
            LED = currentState;
            if ((currentState)&&( (timeCount+oldTimeCount >= 600)))                  //From ON to OFF
            {
              CLRWDT();
              currentState = 0;
              CompCount = 0;
              timeCount = 0;
             
            }

            if ((!currentState)&&(OFFtimeCount>=30))                                //From OFF to ON
            {
              CMIE = 0;
              CLRWDT();
              if (CompCount>15)
                currentState = 1;
 
              CompCount = 0;
              OFFtimeCount = 0;
              CMIE = 1;

            }  

        }
 
}
 
void interrupt isr()
{
if (CMIF)
 {
    TMR1IE = 0;
    CLRWDT();
    if (COUT)
    {
    if (currentState)
    {
         oldTimeCount = timeCount;
         timeCount = 0;
         TMR1L = 0x00; TMR1H = 0x00;

         
    } else
          {
            CompCount++;
          }     
  }//if(cOUT)        
  GPIO5 = COUT;
  TMR1IE = 1;
  CMIF = 0;
 }//cmif

 if (TMR1IF)
 {
  CMIE = 0;
  CLRWDT();
  TMR1L = 0x00; TMR1H = 0x00;
  if (currentState)
  {
  timeCount++;
  if (timeCount >= 65500)
    timeCount = 65500;
 
  } 
  else 
    {
        if (CompCount>0)
        {
            if (OFFtimeCount <= 20)
            {
            OFFtimeCount++;
            CompCount = 1;
            } else
                OFFtimeCount++;
        }    
    }
  TMR1IF = 0;
  CMIE = 1;
 }

}//isr