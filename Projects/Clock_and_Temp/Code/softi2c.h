#define _XTAL_FREQ 4000000



#define SCL TRISA0
#define SDA TRISA1
#define SCL_IN RA0
#define SDA_IN RA1

void i2c_delay (void);


void i2c_start(void);

void i2c_stop (void);

bit i2c_tx(unsigned char d);

unsigned char i2c_rx(unsigned char ack);
