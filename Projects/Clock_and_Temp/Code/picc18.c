#include <18F14K50.h>
 
#FUSES NOWDT                    //��� ����������� �������
#FUSES WDT128                   //�������� ����������� ������� 1:128
#FUSES HS                       //���������������� ����� (���� 4 ���)
#FUSES NOFCMEN                  //Fail-safe clock monitor disabled
#FUSES BROWNOUT                 //����� ��� ������� �������� ���������� �������
#FUSES BORV19                
#FUSES PUT                      //������ �������� ��������� �������
#FUSES NOCPD                    //No EE protection
#FUSES STVREN                   //������������� �� ��� ������������ �����
#FUSES NODEBUG                  //No Debug mode for ICD
#FUSES NOXINST                  //Extended set extension and Indexed Addressing mode disabled (Legacy mode)
#FUSES NOWRT                    //������ �������� �� �������� �� ������
#FUSES NOWRTD                   //Data EEPROM not write protected
#FUSES NOWRTC                   //configuration not registers write protected
#FUSES IESO                     //���������� ��������� ������� �������
#FUSES NOEBTR                   //Memory not protected from table reads
#FUSES NOEBTRB                  //Boot block not protected from table reads
#FUSES NOMCLR                   //Master Clear pin used for I/O
#FUSES NOPROTECT                //Code not protected from reading
#FUSES NOCPB                    //No Boot Block code protection
#FUSES NOWRTB                   //Boot block not write protected
#FUSES NOLVP                    //����� �������������� ���������������� ��������
#FUSES NOHFOFST              
#FUSES NOWRT0                
#FUSES NOWRT1                
#FUSES USBDIV1              
#FUSES PCLKEN                
#FUSES BBSIZ2K                  //2K words Boot Block size
#FUSES PLLEN                
#FUSES CPUDIV1                  //No System Clock Postscaler
 
#use delay(clock=12M,RESTART_WDT)
