#include <htc.h>
#include "softi2c.h"

void i2c_delay (void) //�������� speed/18
{
__delay_us(5);
}

void i2c_start(void)  //����� ��������
{
SCL = 1;
SDA = 1;
i2c_delay();
SDA = 0;
i2c_delay();
SCL = 0;
}

void i2c_stop (void)
{
i2c_delay();
SDA = 0;
SCL = 0;
i2c_delay();
SCL = 1;
i2c_delay();
SDA = 1;

}

bit i2c_tx(unsigned char d)
{
char x;
static bit b;
   for (x=0; x<8; x++) {
       i2c_delay();
       if (d&0x80) SDA = 1;
       else SDA = 0;
       i2c_delay();
       SCL = 1;
       d <<= 1;
       i2c_delay();
       SCL = 0;
   }
i2c_delay();
i2c_delay();
SDA = 1;
SCL = 1;
i2c_delay();

b = SDA_IN;
SCL = 0;
return b;

}

unsigned char i2c_rx(unsigned char ack)
{
unsigned char x,d=0;
    SDA = 1;
    for (x=0; x<8;x++) {
                        d <<= 1;
                               do {
                                   i2c_delay();
                                   SCL = 1;
                                   } while (SCL_IN==0);
                        i2c_delay();
                        if (SDA_IN) d |= 1;
                        SCL = 0;
    }
    i2c_delay();
    if (ack) SDA = 0;
                   else SDA = 1;
    SCL = 1;
    i2c_delay();
    SCL = 0;
	
    return d;   

}

