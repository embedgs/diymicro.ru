#include <stdio.h>
#include <htc.h>
#define _XTAL_FREQ 4000000
#include "usart.h"

#define STATE TRISA2
#define PIN RA2

char temperature;
unsigned char temp_drob;
unsigned char sign;

__CONFIG(LVPDIS & WDTDIS & MCLREN & UNPROTECT & HS);

static bit INIT(void){
static bit b;
STATE = 1;
STATE = 0;
__delay_us(500);
STATE = 1;
__delay_us(65);
b = PIN;
__delay_us(450);

return b;

}

void TX(unsigned char cmd){
unsigned char temp = 0;
unsigned char i = 0;
temp = cmd;
for (i=0;i<8;i++) {
                   if (temp&0x01) {
										STATE = 0;
										__delay_us(5);
										STATE = 1;				   
										__delay_us(70);
									  } else {
								         		STATE = 0;
												__delay_us(70);
												STATE = 1;
												__delay_us(5);
											
											  }
					temp >>= 1;						  
				   }

}

unsigned char RX() {
unsigned char d = 0;
unsigned char i = 0;
for (i=0;i<8;i++){
                  STATE = 0;
				 __delay_us(6);
				 STATE = 1;
				 __delay_us(4);
				 d>>=1;
				 if (PIN == 1) d |= 0x80; 
				//printf("\r\n %d", d);
				 __delay_us(60);	
				 
                
                 	 
				 }
				 
return d;				 

}

void get_temp() {
static bit init;
unsigned char temp1;
unsigned char temp2;
init = INIT();
if (!init) {
printf("init");
TX(0xCC);
TX(0x44);
__delay_ms(150);
__delay_ms(150);
__delay_ms(150);
__delay_ms(150);
__delay_ms(150); 
printf("init");
} else printf("bug");
init = INIT();
if (!init) {
TX(0xCC);
TX(0xBE);

temp1 = RX();
temp2 = RX();

}
temp_drob = temp1 & 0b00001111;    //���������� ������� ����� � ��������� ����������
temp_drob = ((temp_drob*6)+2)/10;              //��������� � ������ ������� �����
temp1 >>= 4;
sign = temp2 & 0x80;
temp2 <<= 4;
temp2 &= 0b01110000;
temp2 |= temp1;

if (sign) { temperature = 127-temp2; 
            temp_drob = 10 - temp_drob; 
           }   else temperature = temp2;


}


void main() {
unsigned char input = 0;
init_comms();
CMCON = 0x07;

get_temp();
printf("\r\ntemperatura -- ");
if (sign) printf("-"); else printf("+");
printf("%d", temperature);
printf(".%d", temp_drob);

while(1){
input = getch();

if (input == 50) {
get_temp();
printf("\r\ntemperatura -- ");
if (sign) printf("-"); else printf("+");
printf("%d", temperature);
printf(".%d", temp_drob);
}

}

}
