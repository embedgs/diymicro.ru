opt subtitle "HI-TECH Software Omniscient Code Generator (PRO mode) build 6446"

opt pagewidth 120

	opt pm

	processor	16F628A
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 24 "X:\Hi-tech\Clock_and_temp\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 24 "X:\Hi-tech\Clock_and_temp\main.c"
	dw 0x3F7F & 0x3FFB & 0x3FFF & 0x3FFF & 0x3FEE ;#
	FNCALL	_main,_get_temp
	FNCALL	_main,_InitIO
	FNCALL	_main,_ReadHour
	FNCALL	_main,_ReadMin
	FNCALL	_main,_DispAll
	FNCALL	_main,_SetHour
	FNCALL	_main,_SetMin
	FNCALL	_main,_SetSeconds
	FNCALL	_DispAll,___lbdiv
	FNCALL	_DispAll,_Display
	FNCALL	_DispAll,___bmul
	FNCALL	_DispAll,_Displaypoint
	FNCALL	_DispAll,_Displaypointminus
	FNCALL	_Displaypointminus,_i2c_start
	FNCALL	_Displaypointminus,_i2c_tx
	FNCALL	_Displaypointminus,_i2c_stop
	FNCALL	_Displaypoint,_i2c_start
	FNCALL	_Displaypoint,_i2c_tx
	FNCALL	_Displaypoint,_i2c_stop
	FNCALL	_Display,_i2c_start
	FNCALL	_Display,_i2c_tx
	FNCALL	_Display,_i2c_stop
	FNCALL	_SetSeconds,_i2c_start
	FNCALL	_SetSeconds,_i2c_tx
	FNCALL	_SetSeconds,_DCBconv
	FNCALL	_SetSeconds,_i2c_stop
	FNCALL	_SetMin,_i2c_start
	FNCALL	_SetMin,_i2c_tx
	FNCALL	_SetMin,_DCBconv
	FNCALL	_SetMin,_i2c_stop
	FNCALL	_SetHour,_i2c_start
	FNCALL	_SetHour,_i2c_tx
	FNCALL	_SetHour,_DCBconv
	FNCALL	_SetHour,_i2c_stop
	FNCALL	_ReadMin,_i2c_start
	FNCALL	_ReadMin,_i2c_tx
	FNCALL	_ReadMin,_i2c_rx
	FNCALL	_ReadMin,_i2c_stop
	FNCALL	_ReadMin,_BCDconv
	FNCALL	_ReadHour,_i2c_start
	FNCALL	_ReadHour,_i2c_tx
	FNCALL	_ReadHour,_i2c_rx
	FNCALL	_ReadHour,_i2c_stop
	FNCALL	_ReadHour,_BCDconv
	FNCALL	_InitIO,_i2c_start
	FNCALL	_InitIO,_i2c_tx
	FNCALL	_InitIO,_i2c_stop
	FNCALL	_i2c_rx,_i2c_delay
	FNCALL	_i2c_stop,_i2c_delay
	FNCALL	_i2c_tx,_i2c_delay
	FNCALL	_i2c_start,_i2c_delay
	FNCALL	_get_temp,_INIT
	FNCALL	_get_temp,_TX
	FNCALL	_get_temp,_printf
	FNCALL	_get_temp,_RX
	FNCALL	_get_temp,___wmul
	FNCALL	_get_temp,___awdiv
	FNCALL	_DCBconv,___lbdiv
	FNCALL	_DCBconv,___bmul
	FNCALL	_printf,_putch
	FNCALL	_printf,___lwdiv
	FNCALL	_printf,___lwmod
	FNROOT	_main
	FNCALL	intlevel1,_isr
	global	intlevel1
	FNROOT	intlevel1
	global	_tempdigit
psect	stringtext,class=STRCODE,delta=2,reloc=256
global __pstringtext
__pstringtext:
;	global	stringtab,__stringbase
stringtab:
;	String table - string pointers are 2 bytes each
	btfsc	(btemp+1),7
	ljmp	stringcode
	bcf	status,7
	btfsc	(btemp+1),0
	bsf	status,7
	movf	indf,w
	incf fsr
skipnz
incf btemp+1
	return
stringcode:
	movf btemp+1,w
andlw 7Fh
movwf	pclath
	movf	fsr,w
incf fsr
skipnz
incf btemp+1
	movwf pc
__stringbase:
	file	"X:\Hi-tech\Clock_and_temp\main.c"
	line	36
_tempdigit:
	retlw	0B7h
	retlw	081h
	retlw	03Dh
	retlw	0ADh
	retlw	08Bh
	retlw	0AEh
	retlw	0BEh
	retlw	085h
	retlw	0BFh
	retlw	0AFh
	retlw	08h
	global	_dpowers
psect	stringtext
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\lib\doprnt.c"
	line	350
_dpowers:
	retlw	01h
	retlw	0

	retlw	0Ah
	retlw	0

	retlw	064h
	retlw	0

	retlw	0E8h
	retlw	03h

	retlw	010h
	retlw	027h

	global	_tempdigitvspoint
psect	stringtext
	file	"X:\Hi-tech\Clock_and_temp\main.c"
	line	51
_tempdigitvspoint:
	retlw	0F7h
	retlw	0C1h
	retlw	07Dh
	retlw	0EDh
	retlw	0CBh
	retlw	0EEh
	retlw	0FEh
	retlw	0C5h
	retlw	0FFh
	retlw	0EFh
	global	_timedigit
psect	stringtext
	file	"X:\Hi-tech\Clock_and_temp\main.c"
	line	65
_timedigit:
	retlw	0F5h
	retlw	024h
	retlw	0B9h
	retlw	0ADh
	retlw	06Ch
	retlw	0CDh
	retlw	0DDh
	retlw	0A4h
	retlw	0FDh
	retlw	0EDh
	global	_timedigitvspoint
psect	stringtext
	file	"X:\Hi-tech\Clock_and_temp\main.c"
	line	79
_timedigitvspoint:
	retlw	0F7h
	retlw	026h
	retlw	0BBh
	retlw	0AFh
	retlw	06Eh
	retlw	0CFh
	retlw	0DFh
	retlw	0A6h
	retlw	0FFh
	retlw	0EFh
	global	_tempdigit
	global	_dpowers
	global	_tempdigitvspoint
	global	_timedigit
	global	_timedigitvspoint
	global	_button
	global	_flag
	global	_sign
	global	_temp_drob
	global	_temperature
	global	_temphour
	global	_tempmin
	global	INIT@b
	global	get_temp@init
psect	bitnvCOMMON,class=COMMON,bit,space=1
global __pbitnvCOMMON
__pbitnvCOMMON:
get_temp@init:
       ds      1

	global	_CMCON
_CMCON	set	31
	global	_TMR1H
_TMR1H	set	15
	global	_TMR1L
_TMR1L	set	14
	global	_TXREG
_TXREG	set	25
	global	_GIE
_GIE	set	95
	global	_INTE
_INTE	set	92
	global	_PEIE
_PEIE	set	94
	global	_RA0
_RA0	set	40
	global	_RA1
_RA1	set	41
	global	_RA2
_RA2	set	42
	global	_RA3
_RA3	set	43
	global	_RA4
_RA4	set	44
	global	_RB0
_RB0	set	48
	global	_RB1
_RB1	set	49
	global	_RB2
_RB2	set	50
	global	_RB3
_RB3	set	51
	global	_RB4
_RB4	set	52
	global	_RB5
_RB5	set	53
	global	_RB6
_RB6	set	54
	global	_RB7
_RB7	set	55
	global	_T1CKPS0
_T1CKPS0	set	132
	global	_T1CKPS1
_T1CKPS1	set	133
	global	_T1OSCEN
_T1OSCEN	set	131
	global	_TMR1CS
_TMR1CS	set	129
	global	_TMR1IF
_TMR1IF	set	96
	global	_TMR1ON
_TMR1ON	set	128
	global	_TXIF
_TXIF	set	100
	global	_TRISB
_TRISB	set	134
	global	_TMR1IE
_TMR1IE	set	1120
	global	_TRISA0
_TRISA0	set	1064
	global	_TRISA1
_TRISA1	set	1065
	global	_TRISA2
_TRISA2	set	1066
	global	_TRISA3
_TRISA3	set	1067
	global	_TRISA4
_TRISA4	set	1068
	global	_TRISB0
_TRISB0	set	1072
psect	stringtext
	
STR_10:	
	retlw	13
	retlw	10
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	42	;'*'
	retlw	0
psect	stringtext
	
STR_12:	
	retlw	13
	retlw	10
	retlw	32	;' '
	retlw	49	;'1'
	retlw	32	;' '
	retlw	45	;'-'
	retlw	32	;' '
	retlw	82	;'R'
	retlw	101	;'e'
	retlw	108	;'l'
	retlw	111	;'o'
	retlw	97	;'a'
	retlw	100	;'d'
	retlw	32	;' '
	retlw	116	;'t'
	retlw	105	;'i'
	retlw	109	;'m'
	retlw	101	;'e'
	retlw	0
psect	stringtext
	
STR_14:	
	retlw	13
	retlw	10
	retlw	32	;' '
	retlw	51	;'3'
	retlw	32	;' '
	retlw	45	;'-'
	retlw	32	;' '
	retlw	82	;'R'
	retlw	101	;'e'
	retlw	108	;'l'
	retlw	111	;'o'
	retlw	97	;'a'
	retlw	100	;'d'
	retlw	32	;' '
	retlw	116	;'t'
	retlw	101	;'e'
	retlw	109	;'m'
	retlw	112	;'p'
	retlw	0
psect	stringtext
	
STR_21:	
	retlw	12
	retlw	69	;'E'
	retlw	110	;'n'
	retlw	116	;'t'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	32	;' '
	retlw	115	;'s'
	retlw	101	;'e'
	retlw	99	;'c'
	retlw	111	;'o'
	retlw	110	;'n'
	retlw	100	;'d'
	retlw	115	;'s'
	retlw	32	;' '
	retlw	45	;'-'
	retlw	32	;' '
	retlw	0
psect	stringtext
	
STR_18:	
	retlw	12
	retlw	69	;'E'
	retlw	110	;'n'
	retlw	116	;'t'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	32	;' '
	retlw	109	;'m'
	retlw	105	;'i'
	retlw	110	;'n'
	retlw	117	;'u'
	retlw	116	;'t'
	retlw	101	;'e'
	retlw	115	;'s'
	retlw	32	;' '
	retlw	45	;'-'
	retlw	32	;' '
	retlw	0
psect	stringtext
	
STR_5:	
	retlw	13
	retlw	10
	retlw	116	;'t'
	retlw	101	;'e'
	retlw	109	;'m'
	retlw	112	;'p'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	97	;'a'
	retlw	116	;'t'
	retlw	117	;'u'
	retlw	114	;'r'
	retlw	97	;'a'
	retlw	32	;' '
	retlw	45	;'-'
	retlw	45	;'-'
	retlw	32	;' '
	retlw	0
psect	stringtext
	
STR_13:	
	retlw	13
	retlw	10
	retlw	32	;' '
	retlw	50	;'2'
	retlw	32	;' '
	retlw	45	;'-'
	retlw	32	;' '
	retlw	83	;'S'
	retlw	101	;'e'
	retlw	116	;'t'
	retlw	117	;'u'
	retlw	112	;'p'
	retlw	32	;' '
	retlw	116	;'t'
	retlw	105	;'i'
	retlw	109	;'m'
	retlw	101	;'e'
	retlw	0
psect	stringtext
	
STR_15:	
	retlw	12
	retlw	69	;'E'
	retlw	110	;'n'
	retlw	116	;'t'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	32	;' '
	retlw	104	;'h'
	retlw	111	;'o'
	retlw	117	;'u'
	retlw	114	;'r'
	retlw	115	;'s'
	retlw	32	;' '
	retlw	45	;'-'
	retlw	32	;' '
	retlw	0
psect	stringtext
	
STR_2:	
	retlw	12
	retlw	84	;'T'
	retlw	105	;'i'
	retlw	109	;'m'
	retlw	101	;'e'
	retlw	32	;' '
	retlw	45	;'-'
	retlw	32	;' '
	retlw	37	;'%'
	retlw	100	;'d'
	retlw	32	;' '
	retlw	58	;':'
	retlw	0
psect	stringtext
	
STR_11:	
	retlw	13
	retlw	10
	retlw	32	;' '
	retlw	77	;'M'
	retlw	101	;'e'
	retlw	110	;'n'
	retlw	117	;'u'
	retlw	0
psect	stringtext
	
STR_4:	
	retlw	32	;' '
	retlw	37	;'%'
	retlw	100	;'d'
	retlw	32	;' '
	retlw	0
psect	stringtext
	
STR_16:	
	retlw	32	;' '
	retlw	37	;'%'
	retlw	99	;'c'
	retlw	0
psect	stringtext
	
STR_9:	
	retlw	46	;'.'
	retlw	37	;'%'
	retlw	100	;'d'
	retlw	0
psect	stringtext
	
STR_1:	
	retlw	98	;'b'
	retlw	117	;'u'
	retlw	103	;'g'
	retlw	0
psect	stringtext
	
STR_7:	
	retlw	43	;'+'
	retlw	0
psect	stringtext
	
STR_6:	
	retlw	45	;'-'
	retlw	0
psect	stringtext
STR_8	equ	STR_9+1
STR_19	equ	STR_16+0
STR_22	equ	STR_16+0
STR_17	equ	STR_16+1
STR_20	equ	STR_16+1
STR_23	equ	STR_16+1
STR_3	equ	STR_2+7
	file	"clock_vs_temp.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bitbssCOMMON,class=COMMON,bit,space=1
global __pbitbssCOMMON
__pbitbssCOMMON:
INIT@b:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_button:
       ds      1

_flag:
       ds      1

_sign:
       ds      1

_temp_drob:
       ds      1

_temperature:
       ds      1

_temphour:
       ds      1

_tempmin:
       ds      1

; Clear objects allocated to BITCOMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbitbssCOMMON/8)+0)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
	clrf	((__pbssBANK0)+1)&07Fh
	clrf	((__pbssBANK0)+2)&07Fh
	clrf	((__pbssBANK0)+3)&07Fh
	clrf	((__pbssBANK0)+4)&07Fh
	clrf	((__pbssBANK0)+5)&07Fh
	clrf	((__pbssBANK0)+6)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_get_temp
?_get_temp:	; 0 bytes @ 0x0
	global	?_InitIO
?_InitIO:	; 0 bytes @ 0x0
	global	?_SetHour
?_SetHour:	; 0 bytes @ 0x0
	global	?_SetMin
?_SetMin:	; 0 bytes @ 0x0
	global	?_SetSeconds
?_SetSeconds:	; 0 bytes @ 0x0
	global	?_i2c_start
?_i2c_start:	; 0 bytes @ 0x0
	global	?_i2c_tx
?_i2c_tx:	; 1 bit 
	global	?_i2c_stop
?_i2c_stop:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 0 bytes @ 0x0
	global	??_isr
??_isr:	; 0 bytes @ 0x0
	global	?_INIT
?_INIT:	; 1 bit 
	global	?_TX
?_TX:	; 0 bytes @ 0x0
	global	?_i2c_delay
?_i2c_delay:	; 0 bytes @ 0x0
	global	?_putch
?_putch:	; 0 bytes @ 0x0
	global	?_ReadHour
?_ReadHour:	; 1 bytes @ 0x0
	global	?_ReadMin
?_ReadMin:	; 1 bytes @ 0x0
	global	?_i2c_rx
?_i2c_rx:	; 1 bytes @ 0x0
	global	?_RX
?_RX:	; 1 bytes @ 0x0
	global	?_BCDconv
?_BCDconv:	; 1 bytes @ 0x0
	global	?_DCBconv
?_DCBconv:	; 1 bytes @ 0x0
	global	?_isr
?_isr:	; 2 bytes @ 0x0
	ds	2
	global	??_i2c_start
??_i2c_start:	; 0 bytes @ 0x2
	global	??_i2c_tx
??_i2c_tx:	; 0 bytes @ 0x2
	global	??_i2c_stop
??_i2c_stop:	; 0 bytes @ 0x2
	global	??_i2c_rx
??_i2c_rx:	; 0 bytes @ 0x2
	global	??_INIT
??_INIT:	; 0 bytes @ 0x2
	global	??_TX
??_TX:	; 0 bytes @ 0x2
	global	??_RX
??_RX:	; 0 bytes @ 0x2
	global	??_BCDconv
??_BCDconv:	; 0 bytes @ 0x2
	global	??_i2c_delay
??_i2c_delay:	; 0 bytes @ 0x2
	global	??_putch
??_putch:	; 0 bytes @ 0x2
	global	?___bmul
?___bmul:	; 1 bytes @ 0x2
	global	?___lbdiv
?___lbdiv:	; 1 bytes @ 0x2
	global	?___wmul
?___wmul:	; 2 bytes @ 0x2
	global	?___lwdiv
?___lwdiv:	; 2 bytes @ 0x2
	global	BCDconv@temp_min
BCDconv@temp_min:	; 1 bytes @ 0x2
	global	i2c_tx@d
i2c_tx@d:	; 1 bytes @ 0x2
	global	i2c_rx@ack
i2c_rx@ack:	; 1 bytes @ 0x2
	global	putch@byte
putch@byte:	; 1 bytes @ 0x2
	global	___bmul@multiplicand
___bmul@multiplicand:	; 1 bytes @ 0x2
	global	___lbdiv@divisor
___lbdiv@divisor:	; 1 bytes @ 0x2
	global	___wmul@multiplier
___wmul@multiplier:	; 2 bytes @ 0x2
	global	___lwdiv@divisor
___lwdiv@divisor:	; 2 bytes @ 0x2
	ds	1
	global	??___bmul
??___bmul:	; 0 bytes @ 0x3
	global	??___lbdiv
??___lbdiv:	; 0 bytes @ 0x3
	global	TX@cmd
TX@cmd:	; 1 bytes @ 0x3
	global	RX@d
RX@d:	; 1 bytes @ 0x3
	global	BCDconv@temp_maj
BCDconv@temp_maj:	; 1 bytes @ 0x3
	global	i2c_tx@x
i2c_tx@x:	; 1 bytes @ 0x3
	global	i2c_rx@d
i2c_rx@d:	; 1 bytes @ 0x3
	global	___bmul@product
___bmul@product:	; 1 bytes @ 0x3
	global	___lbdiv@dividend
___lbdiv@dividend:	; 1 bytes @ 0x3
	ds	1
	global	??_InitIO
??_InitIO:	; 0 bytes @ 0x4
	global	?_Display
?_Display:	; 0 bytes @ 0x4
	global	?_Displaypoint
?_Displaypoint:	; 0 bytes @ 0x4
	global	?_Displaypointminus
?_Displaypointminus:	; 0 bytes @ 0x4
	global	TX@temp
TX@temp:	; 1 bytes @ 0x4
	global	RX@i
RX@i:	; 1 bytes @ 0x4
	global	BCDconv@source
BCDconv@source:	; 1 bytes @ 0x4
	global	Display@temperaturet
Display@temperaturet:	; 1 bytes @ 0x4
	global	Displaypoint@temperaturet
Displaypoint@temperaturet:	; 1 bytes @ 0x4
	global	Displaypointminus@temperaturet
Displaypointminus@temperaturet:	; 1 bytes @ 0x4
	global	i2c_rx@x
i2c_rx@x:	; 1 bytes @ 0x4
	global	___bmul@multiplier
___bmul@multiplier:	; 1 bytes @ 0x4
	global	___lbdiv@counter
___lbdiv@counter:	; 1 bytes @ 0x4
	global	___wmul@multiplicand
___wmul@multiplicand:	; 2 bytes @ 0x4
	global	___lwdiv@dividend
___lwdiv@dividend:	; 2 bytes @ 0x4
	ds	1
	global	??_ReadHour
??_ReadHour:	; 0 bytes @ 0x5
	global	??_ReadMin
??_ReadMin:	; 0 bytes @ 0x5
	global	??_Display
??_Display:	; 0 bytes @ 0x5
	global	??_Displaypoint
??_Displaypoint:	; 0 bytes @ 0x5
	global	??_Displaypointminus
??_Displaypointminus:	; 0 bytes @ 0x5
	global	TX@i
TX@i:	; 1 bytes @ 0x5
	global	ReadMin@temp
ReadMin@temp:	; 1 bytes @ 0x5
	global	ReadHour@temp
ReadHour@temp:	; 1 bytes @ 0x5
	global	Display@time
Display@time:	; 1 bytes @ 0x5
	global	Displaypoint@time
Displaypoint@time:	; 1 bytes @ 0x5
	global	Displaypointminus@time
Displaypointminus@time:	; 1 bytes @ 0x5
	global	___lbdiv@quotient
___lbdiv@quotient:	; 1 bytes @ 0x5
	ds	1
	global	??_DCBconv
??_DCBconv:	; 0 bytes @ 0x6
	global	??___wmul
??___wmul:	; 0 bytes @ 0x6
	global	??___lwdiv
??___lwdiv:	; 0 bytes @ 0x6
	global	Display@timetemp
Display@timetemp:	; 1 bytes @ 0x6
	global	Displaypoint@timetemp
Displaypoint@timetemp:	; 1 bytes @ 0x6
	global	Displaypointminus@timetemp
Displaypointminus@timetemp:	; 1 bytes @ 0x6
	global	___wmul@product
___wmul@product:	; 2 bytes @ 0x6
	global	___lwdiv@quotient
___lwdiv@quotient:	; 2 bytes @ 0x6
	ds	1
	global	DCBconv@temp_min
DCBconv@temp_min:	; 1 bytes @ 0x7
	global	Display@temptemp
Display@temptemp:	; 1 bytes @ 0x7
	global	Displaypoint@temptemp
Displaypoint@temptemp:	; 1 bytes @ 0x7
	global	Displaypointminus@temptemp
Displaypointminus@temptemp:	; 1 bytes @ 0x7
	ds	1
	global	?_DispAll
?_DispAll:	; 0 bytes @ 0x8
	global	??___awdiv
??___awdiv:	; 0 bytes @ 0x8
	global	DCBconv@source
DCBconv@source:	; 1 bytes @ 0x8
	global	DispAll@min
DispAll@min:	; 1 bytes @ 0x8
	global	___lwdiv@counter
___lwdiv@counter:	; 1 bytes @ 0x8
	ds	1
	global	??___lwmod
??___lwmod:	; 0 bytes @ 0x9
	global	DCBconv@temp_maj
DCBconv@temp_maj:	; 1 bytes @ 0x9
	global	DispAll@temperaturet
DispAll@temperaturet:	; 1 bytes @ 0x9
	global	___lwmod@counter
___lwmod@counter:	; 1 bytes @ 0x9
	ds	1
	global	??_SetHour
??_SetHour:	; 0 bytes @ 0xA
	global	??_SetMin
??_SetMin:	; 0 bytes @ 0xA
	global	??_SetSeconds
??_SetSeconds:	; 0 bytes @ 0xA
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	??_DispAll
??_DispAll:	; 0 bytes @ 0x0
	global	?___lwmod
?___lwmod:	; 2 bytes @ 0x0
	global	?___awdiv
?___awdiv:	; 2 bytes @ 0x0
	global	SetMin@minutes
SetMin@minutes:	; 1 bytes @ 0x0
	global	SetHour@hours
SetHour@hours:	; 1 bytes @ 0x0
	global	SetSeconds@seconds
SetSeconds@seconds:	; 1 bytes @ 0x0
	global	___lwmod@divisor
___lwmod@divisor:	; 2 bytes @ 0x0
	global	___awdiv@divisor
___awdiv@divisor:	; 2 bytes @ 0x0
	ds	1
	global	DispAll@hour
DispAll@hour:	; 1 bytes @ 0x1
	ds	1
	global	DispAll@buf
DispAll@buf:	; 1 bytes @ 0x2
	global	___lwmod@dividend
___lwmod@dividend:	; 2 bytes @ 0x2
	global	___awdiv@dividend
___awdiv@dividend:	; 2 bytes @ 0x2
	ds	1
	global	DispAll@tem
DispAll@tem:	; 1 bytes @ 0x3
	ds	1
	global	?_printf
?_printf:	; 2 bytes @ 0x4
	global	DispAll@tim
DispAll@tim:	; 1 bytes @ 0x4
	global	___awdiv@counter
___awdiv@counter:	; 1 bytes @ 0x4
	global	printf@f
printf@f:	; 2 bytes @ 0x4
	ds	1
	global	___awdiv@sign
___awdiv@sign:	; 1 bytes @ 0x5
	ds	1
	global	??_printf
??_printf:	; 0 bytes @ 0x6
	global	___awdiv@quotient
___awdiv@quotient:	; 2 bytes @ 0x6
	ds	2
	global	printf@flag
printf@flag:	; 1 bytes @ 0x8
	ds	1
	global	printf@prec
printf@prec:	; 1 bytes @ 0x9
	ds	1
	global	printf@ap
printf@ap:	; 1 bytes @ 0xA
	ds	1
	global	printf@_val
printf@_val:	; 4 bytes @ 0xB
	ds	4
	global	printf@c
printf@c:	; 1 bytes @ 0xF
	ds	1
	global	??_get_temp
??_get_temp:	; 0 bytes @ 0x10
	ds	2
	global	get_temp@temp1
get_temp@temp1:	; 1 bytes @ 0x12
	ds	1
	global	get_temp@temp2
get_temp@temp2:	; 1 bytes @ 0x13
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0x14
	ds	2
	global	main@i
main@i:	; 1 bytes @ 0x16
	ds	1
;;Data sizes: Strings 192, constant 51, data 0, bss 7, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14     10      12
;; BANK0           80     23      30
;; BANK1           80      0       0
;; BANK2           48      0       0

;;
;; Pointer list with targets:

;; ?___lwdiv	unsigned int  size(1) Largest target is 0
;;
;; ?___lwmod	unsigned int  size(1) Largest target is 0
;;
;; ?___wmul	unsigned int  size(1) Largest target is 0
;;
;; ?___awdiv	int  size(1) Largest target is 0
;;
;; printf@f	PTR const unsigned char  size(2) Largest target is 24
;;		 -> STR_23(CODE[3]), STR_22(CODE[4]), STR_21(CODE[18]), STR_20(CODE[3]), 
;;		 -> STR_19(CODE[4]), STR_18(CODE[18]), STR_17(CODE[3]), STR_16(CODE[4]), 
;;		 -> STR_15(CODE[16]), STR_14(CODE[19]), STR_13(CODE[18]), STR_12(CODE[19]), 
;;		 -> STR_11(CODE[8]), STR_10(CODE[24]), STR_9(CODE[4]), STR_8(CODE[3]), 
;;		 -> STR_7(CODE[2]), STR_6(CODE[2]), STR_5(CODE[18]), STR_4(CODE[5]), 
;;		 -> STR_3(CODE[6]), STR_2(CODE[13]), STR_1(CODE[4]), 
;;
;; ?_printf	int  size(1) Largest target is 0
;;
;; printf@ap	PTR void [1] size(1) Largest target is 2
;;		 -> ?_printf(BANK0[2]), 
;;
;; S708$_cp	PTR const unsigned char  size(1) Largest target is 0
;;
;; _val._str._cp	PTR const unsigned char  size(1) Largest target is 0
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   _main->_DispAll
;;   _DispAll->_Display
;;   _DispAll->_Displaypoint
;;   _DispAll->_Displaypointminus
;;   _Displaypointminus->_i2c_tx
;;   _Displaypoint->_i2c_tx
;;   _Display->_i2c_tx
;;   _SetSeconds->_DCBconv
;;   _SetMin->_DCBconv
;;   _SetHour->_DCBconv
;;   _ReadMin->_i2c_rx
;;   _ReadMin->_BCDconv
;;   _ReadHour->_i2c_rx
;;   _ReadHour->_BCDconv
;;   _InitIO->_i2c_tx
;;   _DCBconv->___lbdiv
;;   _printf->___lwmod
;;   ___awdiv->___wmul
;;   ___lwmod->___lwdiv
;;
;; Critical Paths under _isr in COMMON
;;
;;   None.
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_get_temp
;;   _get_temp->_printf
;;   _printf->___lwmod
;;
;; Critical Paths under _isr in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _isr in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.
;;
;; Critical Paths under _isr in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 2, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 4     4      0    7108
;;                                             20 BANK0      3     3      0
;;                           _get_temp
;;                             _InitIO
;;                           _ReadHour
;;                            _ReadMin
;;                            _DispAll
;;                            _SetHour
;;                             _SetMin
;;                         _SetSeconds
;; ---------------------------------------------------------------------------------
;; (1) _DispAll                                              7     5      2    2332
;;                                              8 COMMON     2     0      2
;;                                              0 BANK0      5     5      0
;;                            ___lbdiv
;;                            _Display
;;                             ___bmul
;;                       _Displaypoint
;;                  _Displaypointminus
;; ---------------------------------------------------------------------------------
;; (2) _Displaypointminus                                    4     3      1     238
;;                                              4 COMMON     4     3      1
;;                          _i2c_start
;;                             _i2c_tx
;;                           _i2c_stop
;; ---------------------------------------------------------------------------------
;; (2) _Displaypoint                                         4     3      1     238
;;                                              4 COMMON     4     3      1
;;                          _i2c_start
;;                             _i2c_tx
;;                           _i2c_stop
;; ---------------------------------------------------------------------------------
;; (2) _Display                                              4     3      1     238
;;                                              4 COMMON     4     3      1
;;                          _i2c_start
;;                             _i2c_tx
;;                           _i2c_stop
;; ---------------------------------------------------------------------------------
;; (1) _SetSeconds                                           1     1      0     680
;;                                              0 BANK0      1     1      0
;;                          _i2c_start
;;                             _i2c_tx
;;                            _DCBconv
;;                           _i2c_stop
;; ---------------------------------------------------------------------------------
;; (1) _SetMin                                               1     1      0     680
;;                                              0 BANK0      1     1      0
;;                          _i2c_start
;;                             _i2c_tx
;;                            _DCBconv
;;                           _i2c_stop
;; ---------------------------------------------------------------------------------
;; (1) _SetHour                                              1     1      0     680
;;                                              0 BANK0      1     1      0
;;                          _i2c_start
;;                             _i2c_tx
;;                            _DCBconv
;;                           _i2c_stop
;; ---------------------------------------------------------------------------------
;; (1) _ReadMin                                              1     1      0     372
;;                                              5 COMMON     1     1      0
;;                          _i2c_start
;;                             _i2c_tx
;;                             _i2c_rx
;;                           _i2c_stop
;;                            _BCDconv
;; ---------------------------------------------------------------------------------
;; (1) _ReadHour                                             2     2      0     372
;;                                              5 COMMON     1     1      0
;;                          _i2c_start
;;                             _i2c_tx
;;                             _i2c_rx
;;                           _i2c_stop
;;                            _BCDconv
;; ---------------------------------------------------------------------------------
;; (1) _InitIO                                               0     0      0     102
;;                          _i2c_start
;;                             _i2c_tx
;;                           _i2c_stop
;; ---------------------------------------------------------------------------------
;; (2) _i2c_rx                                               3     3      0     139
;;                                              2 COMMON     3     3      0
;;                          _i2c_delay
;; ---------------------------------------------------------------------------------
;; (2) _i2c_stop                                             0     0      0       0
;;                          _i2c_delay
;; ---------------------------------------------------------------------------------
;; (2) _i2c_tx                                               2     2      0     102
;;                                              2 COMMON     2     2      0
;;                          _i2c_delay
;; ---------------------------------------------------------------------------------
;; (2) _i2c_start                                            0     0      0       0
;;                          _i2c_delay
;; ---------------------------------------------------------------------------------
;; (1) _get_temp                                             4     4      0    1850
;;                                             16 BANK0      4     4      0
;;                               _INIT
;;                                 _TX
;;                             _printf
;;                                 _RX
;;                             ___wmul
;;                            ___awdiv
;; ---------------------------------------------------------------------------------
;; (3) _i2c_delay                                            0     0      0       0
;; ---------------------------------------------------------------------------------
;; (2) _DCBconv                                              4     4      0     547
;;                                              6 COMMON     4     4      0
;;                            ___lbdiv
;;                             ___bmul
;; ---------------------------------------------------------------------------------
;; (2) _printf                                              12    10      2     974
;;                                              4 BANK0     12    10      2
;;                              _putch
;;                            ___lwdiv
;;                            ___lwmod
;; ---------------------------------------------------------------------------------
;; (2) _RX                                                   3     3      0      72
;;                                              2 COMMON     3     3      0
;; ---------------------------------------------------------------------------------
;; (2) _TX                                                   4     4      0      94
;;                                              2 COMMON     4     4      0
;; ---------------------------------------------------------------------------------
;; (2) _INIT                                                 1     1      0       0
;;                                              2 COMMON     1     1      0
;; ---------------------------------------------------------------------------------
;; (2) ___awdiv                                              8     4      4     445
;;                                              0 BANK0      8     4      4
;;                             ___wmul (ARG)
;; ---------------------------------------------------------------------------------
;; (3) ___lbdiv                                              4     3      1     241
;;                                              2 COMMON     4     3      1
;; ---------------------------------------------------------------------------------
;; (3) ___lwmod                                              5     1      4     159
;;                                              9 COMMON     1     1      0
;;                                              0 BANK0      4     0      4
;;                            ___lwdiv (ARG)
;; ---------------------------------------------------------------------------------
;; (3) ___lwdiv                                              7     3      4     162
;;                                              2 COMMON     7     3      4
;; ---------------------------------------------------------------------------------
;; (2) ___wmul                                               6     2      4      92
;;                                              2 COMMON     6     2      4
;; ---------------------------------------------------------------------------------
;; (3) ___bmul                                               3     2      1     136
;;                                              2 COMMON     3     2      1
;; ---------------------------------------------------------------------------------
;; (3) _putch                                                1     1      0      22
;;                                              2 COMMON     1     1      0
;; ---------------------------------------------------------------------------------
;; (2) _BCDconv                                              3     3      0      94
;;                                              2 COMMON     3     3      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 3
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (5) _isr                                                  2     2      0       0
;;                                              0 COMMON     2     2      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 5
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _get_temp
;;     _INIT
;;     _TX
;;     _printf
;;       _putch
;;       ___lwdiv
;;       ___lwmod
;;         ___lwdiv (ARG)
;;     _RX
;;     ___wmul
;;     ___awdiv
;;       ___wmul (ARG)
;;   _InitIO
;;     _i2c_start
;;       _i2c_delay
;;     _i2c_tx
;;       _i2c_delay
;;     _i2c_stop
;;       _i2c_delay
;;   _ReadHour
;;     _i2c_start
;;       _i2c_delay
;;     _i2c_tx
;;       _i2c_delay
;;     _i2c_rx
;;       _i2c_delay
;;     _i2c_stop
;;       _i2c_delay
;;     _BCDconv
;;   _ReadMin
;;     _i2c_start
;;       _i2c_delay
;;     _i2c_tx
;;       _i2c_delay
;;     _i2c_rx
;;       _i2c_delay
;;     _i2c_stop
;;       _i2c_delay
;;     _BCDconv
;;   _DispAll
;;     ___lbdiv
;;     _Display
;;       _i2c_start
;;         _i2c_delay
;;       _i2c_tx
;;         _i2c_delay
;;       _i2c_stop
;;         _i2c_delay
;;     ___bmul
;;     _Displaypoint
;;       _i2c_start
;;         _i2c_delay
;;       _i2c_tx
;;         _i2c_delay
;;       _i2c_stop
;;         _i2c_delay
;;     _Displaypointminus
;;       _i2c_start
;;         _i2c_delay
;;       _i2c_tx
;;         _i2c_delay
;;       _i2c_stop
;;         _i2c_delay
;;   _SetHour
;;     _i2c_start
;;       _i2c_delay
;;     _i2c_tx
;;       _i2c_delay
;;     _DCBconv
;;       ___lbdiv
;;       ___bmul
;;     _i2c_stop
;;       _i2c_delay
;;   _SetMin
;;     _i2c_start
;;       _i2c_delay
;;     _i2c_tx
;;       _i2c_delay
;;     _DCBconv
;;       ___lbdiv
;;       ___bmul
;;     _i2c_stop
;;       _i2c_delay
;;   _SetSeconds
;;     _i2c_start
;;       _i2c_delay
;;     _i2c_tx
;;       _i2c_delay
;;     _DCBconv
;;       ___lbdiv
;;       ___bmul
;;     _i2c_stop
;;       _i2c_delay
;;
;; _isr (ROOT)
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       2       0       14.3%
;;EEDATA              80      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      A       C       1       85.7%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       4       2        0.0%
;;BANK0               50     17      1E       3       37.5%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;ABS                  0      0      2A       4        0.0%
;;BITBANK0            50      0       0       5        0.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK2            30      0       0       8        0.0%
;;BANK2               30      0       0       9        0.0%
;;DATA                 0      0      2E      10        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 115 in file "X:\Hi-tech\Clock_and_temp\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1   22[BANK0 ] unsigned char 
;;  input           1    0        unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       1       0       0
;;      Temps:          0       2       0       0
;;      Totals:         0       3       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels required when called:    5
;; This function calls:
;;		_get_temp
;;		_InitIO
;;		_ReadHour
;;		_ReadMin
;;		_DispAll
;;		_SetHour
;;		_SetMin
;;		_SetSeconds
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"X:\Hi-tech\Clock_and_temp\main.c"
	line	115
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 8
; Regs used in _main: [wreg-fsr0h+status,2-btemp+1+pclath+cstack]
	line	117
	
l4652:	
;main.c: 116: unsigned char input;
;main.c: 117: unsigned char i=0;
	clrf	(main@i)
	line	119
	
l4654:	
;main.c: 119: CMCON = 0x07;
	movlw	(07h)
	movwf	(31)	;volatile
	line	121
	
l4656:	
;main.c: 121: TRISB = 0x00;
	bsf	status, 5	;RP0=1, select bank1
	clrf	(134)^080h	;volatile
	line	122
	
l4658:	
;main.c: 122: TRISB0 = 1;
	bsf	(1072/8)^080h,(1072)&7
	line	123
	
l4660:	
;main.c: 123: TRISA3 = 1;
	bsf	(1067/8)^080h,(1067)&7
	line	124
	
l4662:	
;main.c: 124: TRISA4 = 1;
	bsf	(1068/8)^080h,(1068)&7
	line	125
	
l4664:	
;main.c: 125: get_temp();
	fcall	_get_temp
	line	137
	
l4666:	
;main.c: 137: InitIO();
	fcall	_InitIO
	line	139
	
l4668:	
;main.c: 139: T1CKPS1 = 1;
	bcf	status, 5	;RP0=0, select bank0
	bsf	(133/8),(133)&7
	line	140
	
l4670:	
;main.c: 140: T1CKPS0 = 1;
	bsf	(132/8),(132)&7
	line	142
	
l4672:	
;main.c: 142: T1OSCEN = 0;
	bcf	(131/8),(131)&7
	line	143
	
l4674:	
;main.c: 143: TMR1CS = 0;
	bcf	(129/8),(129)&7
	line	144
	
l4676:	
;main.c: 144: GIE = 1;
	bsf	(95/8),(95)&7
	line	145
	
l4678:	
;main.c: 145: PEIE = 1;
	bsf	(94/8),(94)&7
	line	146
	
l4680:	
;main.c: 146: TMR1IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bsf	(1120/8)^080h,(1120)&7
	line	147
	
l4682:	
;main.c: 147: TMR1ON = 1;
	bcf	status, 5	;RP0=0, select bank0
	bsf	(128/8),(128)&7
	line	149
	
l4684:	
;main.c: 149: TMR1H = 0b10110000; TMR1L = 0x00;
	movlw	(0B0h)
	movwf	(15)	;volatile
	
l4686:	
	clrf	(14)	;volatile
	line	150
	
l4688:	
;main.c: 150: INTE = 0;
	bcf	(92/8),(92)&7
	line	156
	
l4690:	
;main.c: 156: temphour = ReadHour();
	fcall	_ReadHour
	bcf	status, 5	;RP0=0, select bank0
	movwf	(_temphour)
	line	157
	
l4692:	
;main.c: 157: tempmin = ReadMin();
	fcall	_ReadMin
	bcf	status, 5	;RP0=0, select bank0
	movwf	(_tempmin)
	line	162
	
l4694:	
;main.c: 162: DispAll(temphour, tempmin , temperature);
	bcf	status, 5	;RP0=0, select bank0
	movf	(_tempmin),w
	movwf	(?_DispAll)
	movf	(_temperature),w
	movwf	0+(?_DispAll)+01h
	movf	(_temphour),w
	fcall	_DispAll
	line	164
	
l4696:	
;main.c: 164: if (flag == 20) {
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(_flag),w	;volatile
	xorlw	014h
	skipz
	goto	u1531
	goto	u1530
u1531:
	goto	l4704
u1530:
	line	165
	
l4698:	
;main.c: 165: temphour = ReadHour();
	fcall	_ReadHour
	bcf	status, 5	;RP0=0, select bank0
	movwf	(_temphour)
	line	166
;main.c: 166: tempmin = ReadMin();
	fcall	_ReadMin
	bcf	status, 5	;RP0=0, select bank0
	movwf	(_tempmin)
	line	167
	
l4700:	
;main.c: 167: i++;
	incf	(main@i),f
	line	168
	
l4702:	
;main.c: 168: flag = 0;
	clrf	(_flag)	;volatile
	line	170
	
l4704:	
;main.c: 169: }
;main.c: 170: if (i == 100) { TMR1IE = 0;
	movf	(main@i),w
	xorlw	064h
	skipz
	goto	u1541
	goto	u1540
u1541:
	goto	l4714
u1540:
	
l4706:	
	bsf	status, 5	;RP0=1, select bank1
	bcf	(1120/8)^080h,(1120)&7
	line	171
	
l4708:	
;main.c: 171: get_temp();
	fcall	_get_temp
	line	172
	
l4710:	
;main.c: 172: i = 0;
	clrf	(main@i)
	line	173
	
l4712:	
;main.c: 173: TMR1IE = 1; }
	bsf	status, 5	;RP0=1, select bank1
	bsf	(1120/8)^080h,(1120)&7
	line	176
	
l4714:	
;main.c: 176: if (!RA4) {
	bcf	status, 5	;RP0=0, select bank0
	btfsc	(44/8),(44)&7
	goto	u1551
	goto	u1550
u1551:
	goto	l4732
u1550:
	line	177
	
l4716:	
;main.c: 177: TMR1IE = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	(1120/8)^080h,(1120)&7
	line	178
	
l4718:	
;main.c: 178: _delay((unsigned long)((100)*(4000000/4000.0)));
	opt asmopt_off
movlw	130
	bcf	status, 5	;RP0=0, select bank0
movwf	((??_main+0)+0+1),f
	movlw	221
movwf	((??_main+0)+0),f
u1637:
	decfsz	((??_main+0)+0),f
	goto	u1637
	decfsz	((??_main+0)+0+1),f
	goto	u1637
	nop2
opt asmopt_on

	line	179
	
l4720:	
;main.c: 179: if (!RA4) {
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfsc	(44/8),(44)&7
	goto	u1561
	goto	u1560
u1561:
	goto	l4730
u1560:
	line	181
	
l4722:	
;main.c: 181: if (temphour<24) {
	movlw	(018h)
	subwf	(_temphour),w
	skipnc
	goto	u1571
	goto	u1570
u1571:
	goto	l382
u1570:
	line	182
	
l4724:	
;main.c: 182: temphour++;
	incf	(_temphour),f
	line	183
	
l4726:	
;main.c: 183: SetHour(temphour);
	movf	(_temphour),w
	fcall	_SetHour
	line	184
;main.c: 184: } else {
	goto	l4730
	
l382:	
	line	185
;main.c: 185: temphour = 1;
	clrf	(_temphour)
	incf	(_temphour),f
	line	186
	
l4728:	
;main.c: 186: SetHour(temphour);
	movlw	(01h)
	fcall	_SetHour
	line	191
	
l4730:	
;main.c: 187: }
;main.c: 190: }
;main.c: 191: TMR1IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bsf	(1120/8)^080h,(1120)&7
	line	193
	
l4732:	
;main.c: 192: }
;main.c: 193: if (!RA3) {
	bcf	status, 5	;RP0=0, select bank0
	btfsc	(43/8),(43)&7
	goto	u1581
	goto	u1580
u1581:
	goto	l4750
u1580:
	line	194
	
l4734:	
;main.c: 194: TMR1IE = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	(1120/8)^080h,(1120)&7
	line	195
	
l4736:	
;main.c: 195: _delay((unsigned long)((100)*(4000000/4000.0)));
	opt asmopt_off
movlw	130
	bcf	status, 5	;RP0=0, select bank0
movwf	((??_main+0)+0+1),f
	movlw	221
movwf	((??_main+0)+0),f
u1647:
	decfsz	((??_main+0)+0),f
	goto	u1647
	decfsz	((??_main+0)+0+1),f
	goto	u1647
	nop2
opt asmopt_on

	line	196
	
l4738:	
;main.c: 196: if (!RA3) {
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfsc	(43/8),(43)&7
	goto	u1591
	goto	u1590
u1591:
	goto	l4748
u1590:
	line	197
	
l4740:	
;main.c: 197: if (tempmin<60) {
	movlw	(03Ch)
	subwf	(_tempmin),w
	skipnc
	goto	u1601
	goto	u1600
u1601:
	goto	l386
u1600:
	line	198
	
l4742:	
;main.c: 198: tempmin++;
	incf	(_tempmin),f
	line	199
	
l4744:	
;main.c: 199: SetMin(tempmin);
	movf	(_tempmin),w
	fcall	_SetMin
	line	200
;main.c: 200: } else {
	goto	l4748
	
l386:	
	line	201
;main.c: 201: tempmin = 1;
	clrf	(_tempmin)
	incf	(_tempmin),f
	line	202
	
l4746:	
;main.c: 202: SetMin(tempmin);
	movlw	(01h)
	fcall	_SetMin
	line	207
	
l4748:	
;main.c: 203: }
;main.c: 206: }
;main.c: 207: TMR1IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bsf	(1120/8)^080h,(1120)&7
	line	210
	
l4750:	
;main.c: 208: }
;main.c: 210: if (!RB0) {
	bcf	status, 5	;RP0=0, select bank0
	btfsc	(48/8),(48)&7
	goto	u1611
	goto	u1610
u1611:
	goto	l4694
u1610:
	line	211
	
l4752:	
;main.c: 211: TMR1IE = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	(1120/8)^080h,(1120)&7
	line	212
	
l4754:	
;main.c: 212: _delay((unsigned long)((100)*(4000000/4000.0)));
	opt asmopt_off
movlw	130
	bcf	status, 5	;RP0=0, select bank0
movwf	((??_main+0)+0+1),f
	movlw	221
movwf	((??_main+0)+0),f
u1657:
	decfsz	((??_main+0)+0),f
	goto	u1657
	decfsz	((??_main+0)+0+1),f
	goto	u1657
	nop2
opt asmopt_on

	line	213
	
l4756:	
;main.c: 213: if (!RB0) {
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfsc	(48/8),(48)&7
	goto	u1621
	goto	u1620
u1621:
	goto	l4760
u1620:
	line	214
	
l4758:	
;main.c: 214: SetSeconds(0);
	movlw	(0)
	fcall	_SetSeconds
	line	217
	
l4760:	
;main.c: 216: }
;main.c: 217: TMR1IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bsf	(1120/8)^080h,(1120)&7
	goto	l4694
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

psect	maintext
	line	241
	signat	_main,88
	global	_DispAll
psect	text921,local,class=CODE,delta=2
global __ptext921
__ptext921:

;; *************** function _DispAll *****************
;; Defined at:
;;		line 589 in file "X:\Hi-tech\Clock_and_temp\main.c"
;; Parameters:    Size  Location     Type
;;  hour            1    wreg     unsigned char 
;;  min             1    8[COMMON] unsigned char 
;;  temperaturet    1    9[COMMON] unsigned char 
;; Auto vars:     Size  Location     Type
;;  hour            1    1[BANK0 ] unsigned char 
;;  tim             1    4[BANK0 ] unsigned char 
;;  tem             1    3[BANK0 ] unsigned char 
;;  buf             1    2[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         2       0       0       0
;;      Locals:         0       4       0       0
;;      Temps:          0       1       0       0
;;      Totals:         2       5       0       0
;;Total ram usage:        7 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    4
;; This function calls:
;;		___lbdiv
;;		_Display
;;		___bmul
;;		_Displaypoint
;;		_Displaypointminus
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text921
	file	"X:\Hi-tech\Clock_and_temp\main.c"
	line	589
	global	__size_of_DispAll
	__size_of_DispAll	equ	__end_of_DispAll-_DispAll
	
_DispAll:	
	opt	stack 7
; Regs used in _DispAll: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
;DispAll@hour stored from wreg
	line	591
	movwf	(DispAll@hour)
	line	590
	
l4354:	
	line	591
;main.c: 591: unsigned char tem = 0;
	clrf	(DispAll@tem)
	line	592
;main.c: 592: unsigned char buf = 0;
	clrf	(DispAll@buf)
	line	594
	
l4356:	
;main.c: 594: if (!sign) {
	movf	(_sign),f
	skipz
	goto	u1521
	goto	u1520
u1521:
	goto	l485
u1520:
	line	595
	
l4358:	
;main.c: 595: RB7 = 0; RB6 = 0; RB4 = 0; RB5 = 0; RB3 = 0; RB2 = 0; RB1 = 0;
	bcf	(55/8),(55)&7
	bcf	(54/8),(54)&7
	bcf	(52/8),(52)&7
	bcf	(53/8),(53)&7
	bcf	(51/8),(51)&7
	bcf	(50/8),(50)&7
	bcf	(49/8),(49)&7
	line	596
	
l4360:	
;main.c: 596: tim = hour/10; tem = temperaturet/10;
	movlw	(0Ah)
	movwf	(?___lbdiv)
	movf	(DispAll@hour),w
	fcall	___lbdiv
	movwf	(DispAll@tim)
	movlw	(0Ah)
	movwf	(?___lbdiv)
	movf	(DispAll@temperaturet),w
	fcall	___lbdiv
	movwf	(DispAll@tem)
	line	597
	
l4362:	
;main.c: 597: Display(tim, tem);
	movf	(DispAll@tem),w
	movwf	(?_Display)
	movf	(DispAll@tim),w
	fcall	_Display
	line	598
	
l4364:	
;main.c: 598: RB7 = 1; RB6 = 0; RB4 = 0; RB5 = 0; RB3 = 1; RB2 = 0; RB1 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bsf	(55/8),(55)&7
	
l4366:	
	bcf	(54/8),(54)&7
	
l4368:	
	bcf	(52/8),(52)&7
	
l4370:	
	bcf	(53/8),(53)&7
	
l4372:	
	bsf	(51/8),(51)&7
	
l4374:	
	bcf	(50/8),(50)&7
	
l4376:	
	bcf	(49/8),(49)&7
	line	599
	
l4378:	
;main.c: 599: _delay((unsigned long)((500)*(4000000/4000000.0)));
	opt asmopt_off
movlw	166
movwf	(??_DispAll+0)+0,f
u1667:
decfsz	(??_DispAll+0)+0,f
	goto	u1667
	clrwdt
opt asmopt_on

	line	600
	
l4380:	
;main.c: 600: RB7 = 0; RB6 = 0; RB4 = 0; RB5 = 0; RB3 = 0; RB2 = 0; RB1 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(55/8),(55)&7
	
l4382:	
	bcf	(54/8),(54)&7
	
l4384:	
	bcf	(52/8),(52)&7
	
l4386:	
	bcf	(53/8),(53)&7
	
l4388:	
	bcf	(51/8),(51)&7
	
l4390:	
	bcf	(50/8),(50)&7
	
l4392:	
	bcf	(49/8),(49)&7
	line	601
	
l4394:	
;main.c: 601: buf = tim*10;
	movlw	(0Ah)
	movwf	(?___bmul)
	movf	(DispAll@tim),w
	fcall	___bmul
	movwf	(DispAll@buf)
	line	602
	
l4396:	
;main.c: 602: tim = hour - buf;
	movf	(DispAll@buf),w
	subwf	(DispAll@hour),w
	movwf	(DispAll@tim)
	line	603
	
l4398:	
;main.c: 603: tem = temperaturet - tem*10;
	movlw	(0F6h)
	movwf	(?___bmul)
	movf	(DispAll@tem),w
	fcall	___bmul
	movwf	(??_DispAll+0)+0
	movf	(DispAll@temperaturet),w
	addwf	0+(??_DispAll+0)+0,w
	movwf	(DispAll@tem)
	line	604
	
l4400:	
;main.c: 604: Displaypoint(tim, tem);
	movf	(DispAll@tem),w
	movwf	(?_Displaypoint)
	movf	(DispAll@tim),w
	fcall	_Displaypoint
	line	605
	
l4402:	
;main.c: 605: RB7 = 0; RB6 = 1; RB4 = 0; RB5 = 0; RB3 = 0; RB2 = 1; RB1 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	(55/8),(55)&7
	
l4404:	
	bsf	(54/8),(54)&7
	
l4406:	
	bcf	(52/8),(52)&7
	
l4408:	
	bcf	(53/8),(53)&7
	
l4410:	
	bcf	(51/8),(51)&7
	
l4412:	
	bsf	(50/8),(50)&7
	
l4414:	
	bcf	(49/8),(49)&7
	line	606
	
l4416:	
;main.c: 606: _delay((unsigned long)((500)*(4000000/4000000.0)));
	opt asmopt_off
movlw	166
movwf	(??_DispAll+0)+0,f
u1677:
decfsz	(??_DispAll+0)+0,f
	goto	u1677
	clrwdt
opt asmopt_on

	line	609
	
l4418:	
;main.c: 609: RB7 = 0; RB6 = 0; RB4 = 0; RB5 = 0; RB3 = 0; RB2 = 0; RB1 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(55/8),(55)&7
	
l4420:	
	bcf	(54/8),(54)&7
	
l4422:	
	bcf	(52/8),(52)&7
	
l4424:	
	bcf	(53/8),(53)&7
	
l4426:	
	bcf	(51/8),(51)&7
	
l4428:	
	bcf	(50/8),(50)&7
	
l4430:	
	bcf	(49/8),(49)&7
	line	610
	
l4432:	
;main.c: 610: tim = min/10; tem = temp_drob;
	movlw	(0Ah)
	movwf	(?___lbdiv)
	movf	(DispAll@min),w
	fcall	___lbdiv
	movwf	(DispAll@tim)
	
l4434:	
	movf	(_temp_drob),w
	movwf	(DispAll@tem)
	line	611
	
l4436:	
;main.c: 611: Display(tim, tem);
	movf	(DispAll@tem),w
	movwf	(?_Display)
	movf	(DispAll@tim),w
	fcall	_Display
	line	612
	
l4438:	
;main.c: 612: RB7 = 0; RB6 = 0; RB4 = 1; RB5 = 0; RB3 = 0; RB2 = 0; RB1 = 1;
	bcf	status, 5	;RP0=0, select bank0
	bcf	(55/8),(55)&7
	
l4440:	
	bcf	(54/8),(54)&7
	
l4442:	
	bsf	(52/8),(52)&7
	
l4444:	
	bcf	(53/8),(53)&7
	
l4446:	
	bcf	(51/8),(51)&7
	
l4448:	
	bcf	(50/8),(50)&7
	
l4450:	
	bsf	(49/8),(49)&7
	line	613
	
l4452:	
;main.c: 613: _delay((unsigned long)((500)*(4000000/4000000.0)));
	opt asmopt_off
movlw	166
movwf	(??_DispAll+0)+0,f
u1687:
decfsz	(??_DispAll+0)+0,f
	goto	u1687
	clrwdt
opt asmopt_on

	line	614
	
l4454:	
;main.c: 614: RB7 = 0; RB6 = 0; RB4 = 0; RB5 = 0; RB3 = 0; RB2 = 0; RB1 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(55/8),(55)&7
	
l4456:	
	bcf	(54/8),(54)&7
	
l4458:	
	bcf	(52/8),(52)&7
	
l4460:	
	bcf	(53/8),(53)&7
	
l4462:	
	bcf	(51/8),(51)&7
	
l4464:	
	bcf	(50/8),(50)&7
	
l4466:	
	bcf	(49/8),(49)&7
	line	615
	
l4468:	
;main.c: 615: buf = tim*10;
	movlw	(0Ah)
	movwf	(?___bmul)
	movf	(DispAll@tim),w
	fcall	___bmul
	movwf	(DispAll@buf)
	line	616
	
l4470:	
;main.c: 616: tim = min-buf;
	movf	(DispAll@buf),w
	subwf	(DispAll@min),w
	movwf	(DispAll@tim)
	line	617
	
l4472:	
;main.c: 617: Display(tim, tem);
	movf	(DispAll@tem),w
	movwf	(?_Display)
	movf	(DispAll@tim),w
	fcall	_Display
	line	618
	
l4474:	
;main.c: 618: RB7 = 0; RB6 = 0; RB4 = 0; RB5 = 1; RB3 = 0; RB2 = 0; RB1 = 1;
	bcf	status, 5	;RP0=0, select bank0
	bcf	(55/8),(55)&7
	
l4476:	
	bcf	(54/8),(54)&7
	
l4478:	
	bcf	(52/8),(52)&7
	
l4480:	
	bsf	(53/8),(53)&7
	
l4482:	
	bcf	(51/8),(51)&7
	
l4484:	
	bcf	(50/8),(50)&7
	
l4486:	
	bsf	(49/8),(49)&7
	line	619
	
l4488:	
;main.c: 619: _delay((unsigned long)((500)*(4000000/4000000.0)));
	opt asmopt_off
movlw	166
movwf	(??_DispAll+0)+0,f
u1697:
decfsz	(??_DispAll+0)+0,f
	goto	u1697
	clrwdt
opt asmopt_on

	line	620
	
l4490:	
;main.c: 620: RB7 = 0; RB6 = 0; RB4 = 0; RB5 = 0; RB3 = 0; RB2 = 0; RB1 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(55/8),(55)&7
	
l4492:	
	bcf	(54/8),(54)&7
	
l4494:	
	bcf	(52/8),(52)&7
	
l4496:	
	bcf	(53/8),(53)&7
	
l4498:	
	bcf	(51/8),(51)&7
	
l4500:	
	bcf	(50/8),(50)&7
	
l4502:	
	bcf	(49/8),(49)&7
	line	621
	
l4504:	
;main.c: 621: _delay((unsigned long)((500)*(4000000/4000000.0)));
	opt asmopt_off
movlw	166
movwf	(??_DispAll+0)+0,f
u1707:
decfsz	(??_DispAll+0)+0,f
	goto	u1707
	clrwdt
opt asmopt_on

	line	622
;main.c: 622: } else
	goto	l487
	
l485:	
	line	624
;main.c: 623: {
;main.c: 624: RB7 = 0; RB6 = 0; RB4 = 0; RB5 = 0; RB3 = 0; RB2 = 0; RB1 = 0;
	bcf	(55/8),(55)&7
	bcf	(54/8),(54)&7
	bcf	(52/8),(52)&7
	bcf	(53/8),(53)&7
	bcf	(51/8),(51)&7
	bcf	(50/8),(50)&7
	bcf	(49/8),(49)&7
	line	625
	
l4506:	
;main.c: 625: tim = hour/10; tem = 10;
	movlw	(0Ah)
	movwf	(?___lbdiv)
	movf	(DispAll@hour),w
	fcall	___lbdiv
	movwf	(DispAll@tim)
	
l4508:	
	line	626
	
l4510:	
;main.c: 626: Display(tim, tem);
	movlw	(0Ah)
	movwf	(?_Display)
	movf	(DispAll@tim),w
	fcall	_Display
	line	627
	
l4512:	
;main.c: 627: RB7 = 1; RB6 = 0; RB4 = 0; RB5 = 0; RB3 = 1; RB2 = 0; RB1 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bsf	(55/8),(55)&7
	
l4514:	
	bcf	(54/8),(54)&7
	
l4516:	
	bcf	(52/8),(52)&7
	
l4518:	
	bcf	(53/8),(53)&7
	
l4520:	
	bsf	(51/8),(51)&7
	
l4522:	
	bcf	(50/8),(50)&7
	
l4524:	
	bcf	(49/8),(49)&7
	line	628
	
l4526:	
;main.c: 628: _delay((unsigned long)((1)*(4000000/4000.0)));
	opt asmopt_off
movlw	249
movwf	(??_DispAll+0)+0,f
u1717:
	clrwdt
decfsz	(??_DispAll+0)+0,f
	goto	u1717
	nop2	;nop
	clrwdt
opt asmopt_on

	line	629
	
l4528:	
;main.c: 629: RB7 = 0; RB6 = 0; RB4 = 0; RB5 = 0; RB3 = 0; RB2 = 0; RB1 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(55/8),(55)&7
	
l4530:	
	bcf	(54/8),(54)&7
	
l4532:	
	bcf	(52/8),(52)&7
	
l4534:	
	bcf	(53/8),(53)&7
	
l4536:	
	bcf	(51/8),(51)&7
	
l4538:	
	bcf	(50/8),(50)&7
	
l4540:	
	bcf	(49/8),(49)&7
	line	630
	
l4542:	
;main.c: 630: buf = tim*10;
	movlw	(0Ah)
	movwf	(?___bmul)
	movf	(DispAll@tim),w
	fcall	___bmul
	movwf	(DispAll@buf)
	line	631
	
l4544:	
;main.c: 631: tim = hour - buf;
	movf	(DispAll@buf),w
	subwf	(DispAll@hour),w
	movwf	(DispAll@tim)
	line	632
	
l4546:	
;main.c: 632: tem = temperaturet/10;
	movlw	(0Ah)
	movwf	(?___lbdiv)
	movf	(DispAll@temperaturet),w
	fcall	___lbdiv
	movwf	(DispAll@tem)
	line	633
	
l4548:	
;main.c: 633: Displaypointminus(tim, tem);
	movf	(DispAll@tem),w
	movwf	(?_Displaypointminus)
	movf	(DispAll@tim),w
	fcall	_Displaypointminus
	line	634
	
l4550:	
;main.c: 634: RB7 = 0; RB6 = 1; RB4 = 0; RB5 = 0; RB3 = 0; RB2 = 1; RB1 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	(55/8),(55)&7
	
l4552:	
	bsf	(54/8),(54)&7
	
l4554:	
	bcf	(52/8),(52)&7
	
l4556:	
	bcf	(53/8),(53)&7
	
l4558:	
	bcf	(51/8),(51)&7
	
l4560:	
	bsf	(50/8),(50)&7
	
l4562:	
	bcf	(49/8),(49)&7
	line	635
	
l4564:	
;main.c: 635: _delay((unsigned long)((1)*(4000000/4000.0)));
	opt asmopt_off
movlw	249
movwf	(??_DispAll+0)+0,f
u1727:
	clrwdt
decfsz	(??_DispAll+0)+0,f
	goto	u1727
	nop2	;nop
	clrwdt
opt asmopt_on

	line	638
	
l4566:	
;main.c: 638: RB7 = 0; RB6 = 0; RB4 = 0; RB5 = 0; RB3 = 0; RB2 = 0; RB1 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(55/8),(55)&7
	
l4568:	
	bcf	(54/8),(54)&7
	
l4570:	
	bcf	(52/8),(52)&7
	
l4572:	
	bcf	(53/8),(53)&7
	
l4574:	
	bcf	(51/8),(51)&7
	
l4576:	
	bcf	(50/8),(50)&7
	
l4578:	
	bcf	(49/8),(49)&7
	line	639
	
l4580:	
;main.c: 639: tim = min/10; tem = temperaturet - tem*10;
	movlw	(0Ah)
	movwf	(?___lbdiv)
	movf	(DispAll@min),w
	fcall	___lbdiv
	movwf	(DispAll@tim)
	
l4582:	
	movlw	(0F6h)
	movwf	(?___bmul)
	movf	(DispAll@tem),w
	fcall	___bmul
	movwf	(??_DispAll+0)+0
	movf	(DispAll@temperaturet),w
	addwf	0+(??_DispAll+0)+0,w
	movwf	(DispAll@tem)
	line	640
	
l4584:	
;main.c: 640: Display(tim, tem);
	movf	(DispAll@tem),w
	movwf	(?_Display)
	movf	(DispAll@tim),w
	fcall	_Display
	line	641
	
l4586:	
;main.c: 641: RB7 = 0; RB6 = 0; RB4 = 1; RB5 = 0; RB3 = 0; RB2 = 0; RB1 = 1;
	bcf	status, 5	;RP0=0, select bank0
	bcf	(55/8),(55)&7
	
l4588:	
	bcf	(54/8),(54)&7
	
l4590:	
	bsf	(52/8),(52)&7
	
l4592:	
	bcf	(53/8),(53)&7
	
l4594:	
	bcf	(51/8),(51)&7
	
l4596:	
	bcf	(50/8),(50)&7
	
l4598:	
	bsf	(49/8),(49)&7
	line	642
	
l4600:	
;main.c: 642: _delay((unsigned long)((1)*(4000000/4000.0)));
	opt asmopt_off
movlw	249
movwf	(??_DispAll+0)+0,f
u1737:
	clrwdt
decfsz	(??_DispAll+0)+0,f
	goto	u1737
	nop2	;nop
	clrwdt
opt asmopt_on

	line	643
	
l4602:	
;main.c: 643: RB7 = 0; RB6 = 0; RB4 = 0; RB5 = 0; RB3 = 0; RB2 = 0; RB1 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(55/8),(55)&7
	
l4604:	
	bcf	(54/8),(54)&7
	
l4606:	
	bcf	(52/8),(52)&7
	
l4608:	
	bcf	(53/8),(53)&7
	
l4610:	
	bcf	(51/8),(51)&7
	
l4612:	
	bcf	(50/8),(50)&7
	
l4614:	
	bcf	(49/8),(49)&7
	line	644
	
l4616:	
;main.c: 644: buf = tim*10;
	movlw	(0Ah)
	movwf	(?___bmul)
	movf	(DispAll@tim),w
	fcall	___bmul
	movwf	(DispAll@buf)
	line	645
	
l4618:	
;main.c: 645: tim = min-buf;
	movf	(DispAll@buf),w
	subwf	(DispAll@min),w
	movwf	(DispAll@tim)
	line	646
;main.c: 646: Display(tim, tem);
	movf	(DispAll@tem),w
	movwf	(?_Display)
	movf	(DispAll@tim),w
	fcall	_Display
	line	647
	
l4620:	
;main.c: 647: RB7 = 0; RB6 = 0; RB4 = 0; RB5 = 1; RB3 = 0; RB2 = 0; RB1 = 1;
	bcf	status, 5	;RP0=0, select bank0
	bcf	(55/8),(55)&7
	
l4622:	
	bcf	(54/8),(54)&7
	
l4624:	
	bcf	(52/8),(52)&7
	
l4626:	
	bsf	(53/8),(53)&7
	
l4628:	
	bcf	(51/8),(51)&7
	
l4630:	
	bcf	(50/8),(50)&7
	
l4632:	
	bsf	(49/8),(49)&7
	line	648
	
l4634:	
;main.c: 648: _delay((unsigned long)((1)*(4000000/4000.0)));
	opt asmopt_off
movlw	249
movwf	(??_DispAll+0)+0,f
u1747:
	clrwdt
decfsz	(??_DispAll+0)+0,f
	goto	u1747
	nop2	;nop
	clrwdt
opt asmopt_on

	line	649
	
l4636:	
;main.c: 649: RB7 = 0; RB6 = 0; RB4 = 0; RB5 = 0; RB3 = 0; RB2 = 0; RB1 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(55/8),(55)&7
	
l4638:	
	bcf	(54/8),(54)&7
	
l4640:	
	bcf	(52/8),(52)&7
	
l4642:	
	bcf	(53/8),(53)&7
	
l4644:	
	bcf	(51/8),(51)&7
	
l4646:	
	bcf	(50/8),(50)&7
	
l4648:	
	bcf	(49/8),(49)&7
	line	650
	
l4650:	
;main.c: 650: _delay((unsigned long)((1)*(4000000/4000.0)));
	opt asmopt_off
movlw	249
movwf	(??_DispAll+0)+0,f
u1757:
	clrwdt
decfsz	(??_DispAll+0)+0,f
	goto	u1757
	nop2	;nop
	clrwdt
opt asmopt_on

	line	652
	
l487:	
	return
	opt stack 0
GLOBAL	__end_of_DispAll
	__end_of_DispAll:
;; =============== function _DispAll ends ============

	signat	_DispAll,12408
	global	_Displaypointminus
psect	text922,local,class=CODE,delta=2
global __ptext922
__ptext922:

;; *************** function _Displaypointminus *****************
;; Defined at:
;;		line 574 in file "X:\Hi-tech\Clock_and_temp\main.c"
;; Parameters:    Size  Location     Type
;;  time            1    wreg     unsigned char 
;;  temperaturet    1    4[COMMON] unsigned char 
;; Auto vars:     Size  Location     Type
;;  time            1    5[COMMON] unsigned char 
;;  temptemp        1    7[COMMON] unsigned char 
;;  timetemp        1    6[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         1       0       0       0
;;      Locals:         3       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_i2c_start
;;		_i2c_tx
;;		_i2c_stop
;; This function is called by:
;;		_DispAll
;; This function uses a non-reentrant model
;;
psect	text922
	file	"X:\Hi-tech\Clock_and_temp\main.c"
	line	574
	global	__size_of_Displaypointminus
	__size_of_Displaypointminus	equ	__end_of_Displaypointminus-_Displaypointminus
	
_Displaypointminus:	
	opt	stack 6
; Regs used in _Displaypointminus: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
;Displaypointminus@time stored from wreg
	line	576
	movwf	(Displaypointminus@time)
	line	575
	
l4336:	
	line	576
;main.c: 576: unsigned char temptemp = 0;
	clrf	(Displaypointminus@temptemp)
	line	577
	
l4338:	
;main.c: 577: timetemp = time;
	movf	(Displaypointminus@time),w
	movwf	(Displaypointminus@timetemp)
	line	578
	
l4340:	
;main.c: 578: temptemp = temperaturet;
	movf	(Displaypointminus@temperaturet),w
	movwf	(Displaypointminus@temptemp)
	line	579
	
l4342:	
;main.c: 579: i2c_start();
	fcall	_i2c_start
	line	580
	
l4344:	
;main.c: 580: i2c_tx(0b11101000);
	movlw	(0E8h)
	fcall	_i2c_tx
	line	581
	
l4346:	
;main.c: 581: i2c_tx(0b00000010);
	movlw	(02h)
	fcall	_i2c_tx
	line	582
	
l4348:	
;main.c: 582: i2c_tx(timedigitvspoint[timetemp]);
	movf	(Displaypointminus@timetemp),w
	addlw	low(_timedigitvspoint|8000h)
	movwf	fsr0
	movlw	high(_timedigitvspoint|8000h)
	skipnc
	addlw	1
	movwf	btemp+1
	fcall	stringtab
	fcall	_i2c_tx
	line	583
	
l4350:	
;main.c: 583: i2c_tx(tempdigit[temptemp]);
	movf	(Displaypointminus@temptemp),w
	addlw	low(_tempdigit|8000h)
	movwf	fsr0
	movlw	high(_tempdigit|8000h)
	skipnc
	addlw	1
	movwf	btemp+1
	fcall	stringtab
	fcall	_i2c_tx
	line	584
	
l4352:	
;main.c: 584: i2c_stop();
	fcall	_i2c_stop
	line	586
	
l482:	
	return
	opt stack 0
GLOBAL	__end_of_Displaypointminus
	__end_of_Displaypointminus:
;; =============== function _Displaypointminus ends ============

	signat	_Displaypointminus,8312
	global	_Displaypoint
psect	text923,local,class=CODE,delta=2
global __ptext923
__ptext923:

;; *************** function _Displaypoint *****************
;; Defined at:
;;		line 560 in file "X:\Hi-tech\Clock_and_temp\main.c"
;; Parameters:    Size  Location     Type
;;  time            1    wreg     unsigned char 
;;  temperaturet    1    4[COMMON] unsigned char 
;; Auto vars:     Size  Location     Type
;;  time            1    5[COMMON] unsigned char 
;;  temptemp        1    7[COMMON] unsigned char 
;;  timetemp        1    6[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         1       0       0       0
;;      Locals:         3       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_i2c_start
;;		_i2c_tx
;;		_i2c_stop
;; This function is called by:
;;		_DispAll
;; This function uses a non-reentrant model
;;
psect	text923
	file	"X:\Hi-tech\Clock_and_temp\main.c"
	line	560
	global	__size_of_Displaypoint
	__size_of_Displaypoint	equ	__end_of_Displaypoint-_Displaypoint
	
_Displaypoint:	
	opt	stack 6
; Regs used in _Displaypoint: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
;Displaypoint@time stored from wreg
	line	562
	movwf	(Displaypoint@time)
	line	561
	
l4318:	
	line	562
;main.c: 562: unsigned char temptemp = 0;
	clrf	(Displaypoint@temptemp)
	line	563
	
l4320:	
;main.c: 563: timetemp = time;
	movf	(Displaypoint@time),w
	movwf	(Displaypoint@timetemp)
	line	564
	
l4322:	
;main.c: 564: temptemp = temperaturet;
	movf	(Displaypoint@temperaturet),w
	movwf	(Displaypoint@temptemp)
	line	565
	
l4324:	
;main.c: 565: i2c_start();
	fcall	_i2c_start
	line	566
	
l4326:	
;main.c: 566: i2c_tx(0b11101000);
	movlw	(0E8h)
	fcall	_i2c_tx
	line	567
	
l4328:	
;main.c: 567: i2c_tx(0b00000010);
	movlw	(02h)
	fcall	_i2c_tx
	line	568
	
l4330:	
;main.c: 568: i2c_tx(timedigitvspoint[timetemp]);
	movf	(Displaypoint@timetemp),w
	addlw	low(_timedigitvspoint|8000h)
	movwf	fsr0
	movlw	high(_timedigitvspoint|8000h)
	skipnc
	addlw	1
	movwf	btemp+1
	fcall	stringtab
	fcall	_i2c_tx
	line	569
	
l4332:	
;main.c: 569: i2c_tx(tempdigitvspoint[temptemp]);
	movf	(Displaypoint@temptemp),w
	addlw	low(_tempdigitvspoint|8000h)
	movwf	fsr0
	movlw	high(_tempdigitvspoint|8000h)
	skipnc
	addlw	1
	movwf	btemp+1
	fcall	stringtab
	fcall	_i2c_tx
	line	570
	
l4334:	
;main.c: 570: i2c_stop();
	fcall	_i2c_stop
	line	572
	
l479:	
	return
	opt stack 0
GLOBAL	__end_of_Displaypoint
	__end_of_Displaypoint:
;; =============== function _Displaypoint ends ============

	signat	_Displaypoint,8312
	global	_Display
psect	text924,local,class=CODE,delta=2
global __ptext924
__ptext924:

;; *************** function _Display *****************
;; Defined at:
;;		line 545 in file "X:\Hi-tech\Clock_and_temp\main.c"
;; Parameters:    Size  Location     Type
;;  time            1    wreg     unsigned char 
;;  temperaturet    1    4[COMMON] unsigned char 
;; Auto vars:     Size  Location     Type
;;  time            1    5[COMMON] unsigned char 
;;  temptemp        1    7[COMMON] unsigned char 
;;  timetemp        1    6[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         1       0       0       0
;;      Locals:         3       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_i2c_start
;;		_i2c_tx
;;		_i2c_stop
;; This function is called by:
;;		_DispAll
;; This function uses a non-reentrant model
;;
psect	text924
	file	"X:\Hi-tech\Clock_and_temp\main.c"
	line	545
	global	__size_of_Display
	__size_of_Display	equ	__end_of_Display-_Display
	
_Display:	
	opt	stack 6
; Regs used in _Display: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
;Display@time stored from wreg
	line	547
	movwf	(Display@time)
	line	546
	
l4300:	
	line	547
;main.c: 547: unsigned char temptemp = 0;
	clrf	(Display@temptemp)
	line	548
	
l4302:	
;main.c: 548: timetemp = time;
	movf	(Display@time),w
	movwf	(Display@timetemp)
	line	549
	
l4304:	
;main.c: 549: temptemp = temperaturet;
	movf	(Display@temperaturet),w
	movwf	(Display@temptemp)
	line	550
	
l4306:	
;main.c: 550: i2c_start();
	fcall	_i2c_start
	line	551
	
l4308:	
;main.c: 551: i2c_tx(0b11101000);
	movlw	(0E8h)
	fcall	_i2c_tx
	line	552
	
l4310:	
;main.c: 552: i2c_tx(0b00000010);
	movlw	(02h)
	fcall	_i2c_tx
	line	553
	
l4312:	
;main.c: 553: i2c_tx(timedigit[timetemp]);
	movf	(Display@timetemp),w
	addlw	low(_timedigit|8000h)
	movwf	fsr0
	movlw	high(_timedigit|8000h)
	skipnc
	addlw	1
	movwf	btemp+1
	fcall	stringtab
	fcall	_i2c_tx
	line	554
	
l4314:	
;main.c: 554: i2c_tx(tempdigit[temptemp]);
	movf	(Display@temptemp),w
	addlw	low(_tempdigit|8000h)
	movwf	fsr0
	movlw	high(_tempdigit|8000h)
	skipnc
	addlw	1
	movwf	btemp+1
	fcall	stringtab
	fcall	_i2c_tx
	line	555
	
l4316:	
;main.c: 555: i2c_stop();
	fcall	_i2c_stop
	line	557
	
l476:	
	return
	opt stack 0
GLOBAL	__end_of_Display
	__end_of_Display:
;; =============== function _Display ends ============

	signat	_Display,8312
	global	_SetSeconds
psect	text925,local,class=CODE,delta=2
global __ptext925
__ptext925:

;; *************** function _SetSeconds *****************
;; Defined at:
;;		line 431 in file "X:\Hi-tech\Clock_and_temp\main.c"
;; Parameters:    Size  Location     Type
;;  seconds         1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  seconds         1    0[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       1       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       1       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_i2c_start
;;		_i2c_tx
;;		_DCBconv
;;		_i2c_stop
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text925
	file	"X:\Hi-tech\Clock_and_temp\main.c"
	line	431
	global	__size_of_SetSeconds
	__size_of_SetSeconds	equ	__end_of_SetSeconds-_SetSeconds
	
_SetSeconds:	
	opt	stack 7
; Regs used in _SetSeconds: [wreg+status,2+status,0+pclath+cstack]
;SetSeconds@seconds stored from wreg
	movwf	(SetSeconds@seconds)
	line	432
	
l4290:	
;main.c: 432: i2c_start();
	fcall	_i2c_start
	line	433
	
l4292:	
;main.c: 433: i2c_tx(0b11010000);
	movlw	(0D0h)
	fcall	_i2c_tx
	line	434
	
l4294:	
;main.c: 434: i2c_tx(0x00);
	movlw	(0)
	fcall	_i2c_tx
	line	435
	
l4296:	
;main.c: 435: i2c_tx(DCBconv(seconds));
	bcf	status, 5	;RP0=0, select bank0
	movf	(SetSeconds@seconds),w
	fcall	_DCBconv
	fcall	_i2c_tx
	line	436
	
l4298:	
;main.c: 436: i2c_stop();
	fcall	_i2c_stop
	line	437
	
l444:	
	return
	opt stack 0
GLOBAL	__end_of_SetSeconds
	__end_of_SetSeconds:
;; =============== function _SetSeconds ends ============

	signat	_SetSeconds,4216
	global	_SetMin
psect	text926,local,class=CODE,delta=2
global __ptext926
__ptext926:

;; *************** function _SetMin *****************
;; Defined at:
;;		line 383 in file "X:\Hi-tech\Clock_and_temp\main.c"
;; Parameters:    Size  Location     Type
;;  minutes         1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  minutes         1    0[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       1       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       1       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_i2c_start
;;		_i2c_tx
;;		_DCBconv
;;		_i2c_stop
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text926
	file	"X:\Hi-tech\Clock_and_temp\main.c"
	line	383
	global	__size_of_SetMin
	__size_of_SetMin	equ	__end_of_SetMin-_SetMin
	
_SetMin:	
	opt	stack 7
; Regs used in _SetMin: [wreg+status,2+status,0+pclath+cstack]
;SetMin@minutes stored from wreg
	movwf	(SetMin@minutes)
	line	384
	
l4280:	
;main.c: 384: i2c_start();
	fcall	_i2c_start
	line	385
	
l4282:	
;main.c: 385: i2c_tx(0b11010000);
	movlw	(0D0h)
	fcall	_i2c_tx
	line	386
	
l4284:	
;main.c: 386: i2c_tx(0x01);
	movlw	(01h)
	fcall	_i2c_tx
	line	387
	
l4286:	
;main.c: 387: i2c_tx(DCBconv(minutes));
	bcf	status, 5	;RP0=0, select bank0
	movf	(SetMin@minutes),w
	fcall	_DCBconv
	fcall	_i2c_tx
	line	388
	
l4288:	
;main.c: 388: i2c_stop();
	fcall	_i2c_stop
	line	389
	
l432:	
	return
	opt stack 0
GLOBAL	__end_of_SetMin
	__end_of_SetMin:
;; =============== function _SetMin ends ============

	signat	_SetMin,4216
	global	_SetHour
psect	text927,local,class=CODE,delta=2
global __ptext927
__ptext927:

;; *************** function _SetHour *****************
;; Defined at:
;;		line 404 in file "X:\Hi-tech\Clock_and_temp\main.c"
;; Parameters:    Size  Location     Type
;;  hours           1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  hours           1    0[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       1       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       1       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_i2c_start
;;		_i2c_tx
;;		_DCBconv
;;		_i2c_stop
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text927
	file	"X:\Hi-tech\Clock_and_temp\main.c"
	line	404
	global	__size_of_SetHour
	__size_of_SetHour	equ	__end_of_SetHour-_SetHour
	
_SetHour:	
	opt	stack 7
; Regs used in _SetHour: [wreg+status,2+status,0+pclath+cstack]
;SetHour@hours stored from wreg
	movwf	(SetHour@hours)
	line	405
	
l4270:	
;main.c: 405: i2c_start();
	fcall	_i2c_start
	line	406
	
l4272:	
;main.c: 406: i2c_tx(0b11010000);
	movlw	(0D0h)
	fcall	_i2c_tx
	line	407
	
l4274:	
;main.c: 407: i2c_tx(0x02);
	movlw	(02h)
	fcall	_i2c_tx
	line	408
	
l4276:	
;main.c: 408: i2c_tx(DCBconv(hours));
	bcf	status, 5	;RP0=0, select bank0
	movf	(SetHour@hours),w
	fcall	_DCBconv
	fcall	_i2c_tx
	line	409
	
l4278:	
;main.c: 409: i2c_stop();
	fcall	_i2c_stop
	line	410
	
l438:	
	return
	opt stack 0
GLOBAL	__end_of_SetHour
	__end_of_SetHour:
;; =============== function _SetHour ends ============

	signat	_SetHour,4216
	global	_ReadMin
psect	text928,local,class=CODE,delta=2
global __ptext928
__ptext928:

;; *************** function _ReadMin *****************
;; Defined at:
;;		line 391 in file "X:\Hi-tech\Clock_and_temp\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  temp            1    5[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         1       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_i2c_start
;;		_i2c_tx
;;		_i2c_rx
;;		_i2c_stop
;;		_BCDconv
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text928
	file	"X:\Hi-tech\Clock_and_temp\main.c"
	line	391
	global	__size_of_ReadMin
	__size_of_ReadMin	equ	__end_of_ReadMin-_ReadMin
	
_ReadMin:	
	opt	stack 7
; Regs used in _ReadMin: [wreg+status,2+status,0+pclath+cstack]
	line	392
	
l4250:	
	line	393
	
l4252:	
;main.c: 393: i2c_start();
	fcall	_i2c_start
	line	394
	
l4254:	
;main.c: 394: i2c_tx(0b11010000);
	movlw	(0D0h)
	fcall	_i2c_tx
	line	395
	
l4256:	
;main.c: 395: i2c_tx(0x01);
	movlw	(01h)
	fcall	_i2c_tx
	line	396
	
l4258:	
;main.c: 396: i2c_start();
	fcall	_i2c_start
	line	397
	
l4260:	
;main.c: 397: i2c_tx(0b11010001);
	movlw	(0D1h)
	fcall	_i2c_tx
	line	398
	
l4262:	
;main.c: 398: temp = i2c_rx(0);
	movlw	(0)
	fcall	_i2c_rx
	movwf	(ReadMin@temp)
	line	399
	
l4264:	
;main.c: 399: i2c_stop();
	fcall	_i2c_stop
	line	401
	
l4266:	
;main.c: 401: return BCDconv(temp);
	movf	(ReadMin@temp),w
	fcall	_BCDconv
	line	402
	
l435:	
	return
	opt stack 0
GLOBAL	__end_of_ReadMin
	__end_of_ReadMin:
;; =============== function _ReadMin ends ============

	signat	_ReadMin,89
	global	_ReadHour
psect	text929,local,class=CODE,delta=2
global __ptext929
__ptext929:

;; *************** function _ReadHour *****************
;; Defined at:
;;		line 412 in file "X:\Hi-tech\Clock_and_temp\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  temp            1    5[COMMON] unsigned char 
;;  temp2           1    0        unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         1       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_i2c_start
;;		_i2c_tx
;;		_i2c_rx
;;		_i2c_stop
;;		_BCDconv
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text929
	file	"X:\Hi-tech\Clock_and_temp\main.c"
	line	412
	global	__size_of_ReadHour
	__size_of_ReadHour	equ	__end_of_ReadHour-_ReadHour
	
_ReadHour:	
	opt	stack 7
; Regs used in _ReadHour: [wreg+status,2+status,0+pclath+cstack]
	line	413
	
l4230:	
	line	415
	
l4232:	
;main.c: 415: i2c_start();
	fcall	_i2c_start
	line	416
	
l4234:	
;main.c: 416: i2c_tx(0b11010000);
	movlw	(0D0h)
	fcall	_i2c_tx
	line	417
	
l4236:	
;main.c: 417: i2c_tx(0x02);
	movlw	(02h)
	fcall	_i2c_tx
	line	418
	
l4238:	
;main.c: 418: i2c_start();
	fcall	_i2c_start
	line	419
	
l4240:	
;main.c: 419: i2c_tx(0b11010001);
	movlw	(0D1h)
	fcall	_i2c_tx
	line	420
	
l4242:	
;main.c: 420: temp = i2c_rx(0);
	movlw	(0)
	fcall	_i2c_rx
	movwf	(ReadHour@temp)
	line	421
	
l4244:	
;main.c: 421: i2c_stop();
	fcall	_i2c_stop
	line	428
	
l4246:	
;main.c: 428: return BCDconv(temp);
	movf	(ReadHour@temp),w
	fcall	_BCDconv
	line	429
	
l441:	
	return
	opt stack 0
GLOBAL	__end_of_ReadHour
	__end_of_ReadHour:
;; =============== function _ReadHour ends ============

	signat	_ReadHour,89
	global	_InitIO
psect	text930,local,class=CODE,delta=2
global __ptext930
__ptext930:

;; *************** function _InitIO *****************
;; Defined at:
;;		line 536 in file "X:\Hi-tech\Clock_and_temp\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_i2c_start
;;		_i2c_tx
;;		_i2c_stop
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text930
	file	"X:\Hi-tech\Clock_and_temp\main.c"
	line	536
	global	__size_of_InitIO
	__size_of_InitIO	equ	__end_of_InitIO-_InitIO
	
_InitIO:	
	opt	stack 7
; Regs used in _InitIO: [wreg+status,2+status,0+pclath+cstack]
	line	537
	
l4220:	
;main.c: 537: i2c_start();
	fcall	_i2c_start
	line	538
	
l4222:	
;main.c: 538: i2c_tx(0b11101000);
	movlw	(0E8h)
	fcall	_i2c_tx
	line	539
	
l4224:	
;main.c: 539: i2c_tx(0b00000110);
	movlw	(06h)
	fcall	_i2c_tx
	line	540
	
l4226:	
;main.c: 540: i2c_tx(0x00);
	movlw	(0)
	fcall	_i2c_tx
	line	541
	
l4228:	
;main.c: 541: i2c_tx(0x00);
	movlw	(0)
	fcall	_i2c_tx
	line	542
;main.c: 542: i2c_stop();
	fcall	_i2c_stop
	line	543
	
l473:	
	return
	opt stack 0
GLOBAL	__end_of_InitIO
	__end_of_InitIO:
;; =============== function _InitIO ends ============

	signat	_InitIO,88
	global	_i2c_rx
psect	text931,local,class=CODE,delta=2
global __ptext931
__ptext931:

;; *************** function _i2c_rx *****************
;; Defined at:
;;		line 58 in file "X:\Hi-tech\Clock_and_temp\softi2c.c"
;; Parameters:    Size  Location     Type
;;  ack             1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  ack             1    2[COMMON] unsigned char 
;;  x               1    4[COMMON] unsigned char 
;;  d               1    3[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/20
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         3       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         3       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_i2c_delay
;; This function is called by:
;;		_ReadMin
;;		_ReadHour
;; This function uses a non-reentrant model
;;
psect	text931
	file	"X:\Hi-tech\Clock_and_temp\softi2c.c"
	line	58
	global	__size_of_i2c_rx
	__size_of_i2c_rx	equ	__end_of_i2c_rx-_i2c_rx
	
_i2c_rx:	
	opt	stack 6
; Regs used in _i2c_rx: [wreg+status,2+status,0+pclath+cstack]
;i2c_rx@ack stored from wreg
	movwf	(i2c_rx@ack)
	line	59
	
l4182:	
;softi2c.c: 59: unsigned char x,d=0;
	clrf	(i2c_rx@d)
	line	60
	
l4184:	
;softi2c.c: 60: TRISA1 = 1;
	bsf	(1065/8)^080h,(1065)&7
	line	61
;softi2c.c: 61: for (x=0; x<8;x++) {
	clrf	(i2c_rx@x)
	
l832:	
	line	62
;softi2c.c: 62: d <<= 1;
	clrc
	rlf	(i2c_rx@d),f
	line	64
	
l4190:	
;softi2c.c: 64: i2c_delay();
	fcall	_i2c_delay
	line	65
	
l4192:	
;softi2c.c: 65: TRISA0 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1064/8)^080h,(1064)&7
	line	66
	
l4194:	
;softi2c.c: 66: } while (RA0==0);
	bcf	status, 5	;RP0=0, select bank0
	btfss	(40/8),(40)&7
	goto	u1481
	goto	u1480
u1481:
	goto	l4190
u1480:
	line	67
	
l4196:	
;softi2c.c: 67: i2c_delay();
	fcall	_i2c_delay
	line	68
	
l4198:	
;softi2c.c: 68: if (RA1) d |= 1;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(41/8),(41)&7
	goto	u1491
	goto	u1490
u1491:
	goto	l836
u1490:
	
l4200:	
	bsf	(i2c_rx@d)+(0/8),(0)&7
	
l836:	
	line	69
;softi2c.c: 69: TRISA0 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	(1064/8)^080h,(1064)&7
	line	61
	
l4202:	
	incf	(i2c_rx@x),f
	
l4204:	
	movlw	(08h)
	subwf	(i2c_rx@x),w
	skipc
	goto	u1501
	goto	u1500
u1501:
	goto	l832
u1500:
	line	71
	
l4206:	
;softi2c.c: 70: }
;softi2c.c: 71: i2c_delay();
	fcall	_i2c_delay
	line	72
	
l4208:	
;softi2c.c: 72: if (ack) TRISA1 = 0;
	movf	(i2c_rx@ack),w
	skipz
	goto	u1510
	goto	l837
u1510:
	
l4210:	
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1065/8)^080h,(1065)&7
	goto	l838
	line	73
	
l837:	
;softi2c.c: 73: else TRISA1 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1065/8)^080h,(1065)&7
	
l838:	
	line	74
;softi2c.c: 74: TRISA0 = 1;
	bsf	(1064/8)^080h,(1064)&7
	line	75
	
l4212:	
;softi2c.c: 75: i2c_delay();
	fcall	_i2c_delay
	line	76
	
l4214:	
;softi2c.c: 76: TRISA0 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1064/8)^080h,(1064)&7
	line	78
	
l4216:	
;softi2c.c: 78: return d;
	movf	(i2c_rx@d),w
	line	80
	
l839:	
	return
	opt stack 0
GLOBAL	__end_of_i2c_rx
	__end_of_i2c_rx:
;; =============== function _i2c_rx ends ============

	signat	_i2c_rx,4217
	global	_i2c_stop
psect	text932,local,class=CODE,delta=2
global __ptext932
__ptext932:

;; *************** function _i2c_stop *****************
;; Defined at:
;;		line 20 in file "X:\Hi-tech\Clock_and_temp\softi2c.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/20
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_i2c_delay
;; This function is called by:
;;		_SetMin
;;		_ReadMin
;;		_SetHour
;;		_ReadHour
;;		_SetSeconds
;;		_InitIO
;;		_Display
;;		_Displaypoint
;;		_Displaypointminus
;; This function uses a non-reentrant model
;;
psect	text932
	file	"X:\Hi-tech\Clock_and_temp\softi2c.c"
	line	20
	global	__size_of_i2c_stop
	__size_of_i2c_stop	equ	__end_of_i2c_stop-_i2c_stop
	
_i2c_stop:	
	opt	stack 6
; Regs used in _i2c_stop: [status,2+status,0+pclath+cstack]
	line	21
	
l4172:	
;softi2c.c: 21: i2c_delay();
	fcall	_i2c_delay
	line	22
	
l4174:	
;softi2c.c: 22: TRISA1 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1065/8)^080h,(1065)&7
	line	23
	
l4176:	
;softi2c.c: 23: TRISA0 = 0;
	bcf	(1064/8)^080h,(1064)&7
	line	24
;softi2c.c: 24: i2c_delay();
	fcall	_i2c_delay
	line	25
	
l4178:	
;softi2c.c: 25: TRISA0 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1064/8)^080h,(1064)&7
	line	26
;softi2c.c: 26: i2c_delay();
	fcall	_i2c_delay
	line	27
	
l4180:	
;softi2c.c: 27: TRISA1 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1065/8)^080h,(1065)&7
	line	29
	
l820:	
	return
	opt stack 0
GLOBAL	__end_of_i2c_stop
	__end_of_i2c_stop:
;; =============== function _i2c_stop ends ============

	signat	_i2c_stop,88
	global	_i2c_tx
psect	text933,local,class=CODE,delta=2
global __ptext933
__ptext933:

;; *************** function _i2c_tx *****************
;; Defined at:
;;		line 32 in file "X:\Hi-tech\Clock_and_temp\softi2c.c"
;; Parameters:    Size  Location     Type
;;  d               1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  d               1    2[COMMON] unsigned char 
;;  x               1    3[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 40/20
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         2       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         2       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_i2c_delay
;; This function is called by:
;;		_SetMin
;;		_ReadMin
;;		_SetHour
;;		_ReadHour
;;		_SetSeconds
;;		_InitIO
;;		_Display
;;		_Displaypoint
;;		_Displaypointminus
;; This function uses a non-reentrant model
;;
psect	text933
	file	"X:\Hi-tech\Clock_and_temp\softi2c.c"
	line	32
	global	__size_of_i2c_tx
	__size_of_i2c_tx	equ	__end_of_i2c_tx-_i2c_tx
	
_i2c_tx:	
	opt	stack 6
; Regs used in _i2c_tx: [wreg+status,2+status,0+pclath+cstack]
;i2c_tx@d stored from wreg
	line	35
	movwf	(i2c_tx@d)
	
l4136:	
;softi2c.c: 33: char x;
;softi2c.c: 34: static bit b;
;softi2c.c: 35: for (x=0; x<8; x++) {
	clrf	(i2c_tx@x)
	line	36
	
l4142:	
;softi2c.c: 36: i2c_delay();
	fcall	_i2c_delay
	line	37
	
l4144:	
;softi2c.c: 37: if (d&0x80) TRISA1 = 1;
	btfss	(i2c_tx@d),(7)&7
	goto	u1461
	goto	u1460
u1461:
	goto	l827
u1460:
	
l4146:	
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1065/8)^080h,(1065)&7
	goto	l4148
	line	38
	
l827:	
;softi2c.c: 38: else TRISA1 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1065/8)^080h,(1065)&7
	line	39
	
l4148:	
;softi2c.c: 39: i2c_delay();
	fcall	_i2c_delay
	line	40
	
l4150:	
;softi2c.c: 40: TRISA0 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1064/8)^080h,(1064)&7
	line	41
	
l4152:	
;softi2c.c: 41: d <<= 1;
	clrc
	rlf	(i2c_tx@d),f
	line	42
;softi2c.c: 42: i2c_delay();
	fcall	_i2c_delay
	line	43
	
l4154:	
;softi2c.c: 43: TRISA0 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1064/8)^080h,(1064)&7
	line	35
	
l4156:	
	incf	(i2c_tx@x),f
	
l4158:	
	movlw	(08h)
	subwf	(i2c_tx@x),w
	skipc
	goto	u1471
	goto	u1470
u1471:
	goto	l4142
u1470:
	line	45
	
l4160:	
;softi2c.c: 44: }
;softi2c.c: 45: i2c_delay();
	fcall	_i2c_delay
	line	46
;softi2c.c: 46: i2c_delay();
	fcall	_i2c_delay
	line	47
	
l4162:	
;softi2c.c: 47: TRISA1 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1065/8)^080h,(1065)&7
	line	48
	
l4164:	
;softi2c.c: 48: TRISA0 = 1;
	bsf	(1064/8)^080h,(1064)&7
	line	49
;softi2c.c: 49: i2c_delay();
	fcall	_i2c_delay
	line	51
	
l4166:	
;softi2c.c: 51: b = RA1;
	line	52
	
l4168:	
;softi2c.c: 52: TRISA0 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1064/8)^080h,(1064)&7
	line	55
	
l829:	
	return
	opt stack 0
GLOBAL	__end_of_i2c_tx
	__end_of_i2c_tx:
;; =============== function _i2c_tx ends ============

	signat	_i2c_tx,4216
	global	_i2c_start
psect	text934,local,class=CODE,delta=2
global __ptext934
__ptext934:

;; *************** function _i2c_start *****************
;; Defined at:
;;		line 10 in file "X:\Hi-tech\Clock_and_temp\softi2c.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 40/0
;;		On exit  : 60/20
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_i2c_delay
;; This function is called by:
;;		_SetMin
;;		_ReadMin
;;		_SetHour
;;		_ReadHour
;;		_SetSeconds
;;		_InitIO
;;		_Display
;;		_Displaypoint
;;		_Displaypointminus
;; This function uses a non-reentrant model
;;
psect	text934
	file	"X:\Hi-tech\Clock_and_temp\softi2c.c"
	line	10
	global	__size_of_i2c_start
	__size_of_i2c_start	equ	__end_of_i2c_start-_i2c_start
	
_i2c_start:	
	opt	stack 6
; Regs used in _i2c_start: [status,2+status,0+pclath+cstack]
	line	11
	
l4128:	
;softi2c.c: 11: TRISA0 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bsf	(1064/8)^080h,(1064)&7
	line	12
;softi2c.c: 12: TRISA1 = 1;
	bsf	(1065/8)^080h,(1065)&7
	line	13
	
l4130:	
;softi2c.c: 13: i2c_delay();
	fcall	_i2c_delay
	line	14
	
l4132:	
;softi2c.c: 14: TRISA1 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1065/8)^080h,(1065)&7
	line	15
;softi2c.c: 15: i2c_delay();
	fcall	_i2c_delay
	line	16
	
l4134:	
;softi2c.c: 16: TRISA0 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1064/8)^080h,(1064)&7
	line	17
	
l817:	
	return
	opt stack 0
GLOBAL	__end_of_i2c_start
	__end_of_i2c_start:
;; =============== function _i2c_start ends ============

	signat	_i2c_start,88
	global	_get_temp
psect	text935,local,class=CODE,delta=2
global __ptext935
__ptext935:

;; *************** function _get_temp *****************
;; Defined at:
;;		line 314 in file "X:\Hi-tech\Clock_and_temp\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  temp2           1   19[BANK0 ] unsigned char 
;;  temp1           1   18[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 60/20
;;		On exit  : 60/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       2       0       0
;;      Temps:          0       2       0       0
;;      Totals:         0       4       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_INIT
;;		_TX
;;		_printf
;;		_RX
;;		___wmul
;;		___awdiv
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text935
	file	"X:\Hi-tech\Clock_and_temp\main.c"
	line	314
	global	__size_of_get_temp
	__size_of_get_temp	equ	__end_of_get_temp-_get_temp
	
_get_temp:	
	opt	stack 7
; Regs used in _get_temp: [wreg-fsr0h+status,2-btemp+1+pclath+cstack]
	line	318
	
l4084:	
;main.c: 315: static bit init;
;main.c: 316: unsigned char temp1;
;main.c: 317: unsigned char temp2;
;main.c: 318: init = INIT();
	fcall	_INIT
	bcf	(get_temp@init/8),(get_temp@init)&7
	btfss	status,0
	goto	u1415
	bsf	(get_temp@init/8),(get_temp@init)&7
u1415:

	line	319
	
l4086:	
;main.c: 319: if (!init) {
	btfsc	(get_temp@init/8),(get_temp@init)&7
	goto	u1421
	goto	u1420
u1421:
	goto	l4100
u1420:
	line	320
	
l4088:	
;main.c: 320: TX(0xCC);
	movlw	(0CCh)
	fcall	_TX
	line	321
;main.c: 321: TX(0x44);
	movlw	(044h)
	fcall	_TX
	line	322
	
l4090:	
;main.c: 322: _delay((unsigned long)((150)*(4000000/4000.0)));
	opt asmopt_off
movlw	195
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
movwf	((??_get_temp+0)+0+1),f
	movlw	205
movwf	((??_get_temp+0)+0),f
u1767:
	decfsz	((??_get_temp+0)+0),f
	goto	u1767
	decfsz	((??_get_temp+0)+0+1),f
	goto	u1767
opt asmopt_on

	line	323
	
l4092:	
;main.c: 323: _delay((unsigned long)((150)*(4000000/4000.0)));
	opt asmopt_off
movlw	195
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
movwf	((??_get_temp+0)+0+1),f
	movlw	205
movwf	((??_get_temp+0)+0),f
u1777:
	decfsz	((??_get_temp+0)+0),f
	goto	u1777
	decfsz	((??_get_temp+0)+0+1),f
	goto	u1777
opt asmopt_on

	line	324
	
l4094:	
;main.c: 324: _delay((unsigned long)((150)*(4000000/4000.0)));
	opt asmopt_off
movlw	195
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
movwf	((??_get_temp+0)+0+1),f
	movlw	205
movwf	((??_get_temp+0)+0),f
u1787:
	decfsz	((??_get_temp+0)+0),f
	goto	u1787
	decfsz	((??_get_temp+0)+0+1),f
	goto	u1787
opt asmopt_on

	line	325
	
l4096:	
;main.c: 325: _delay((unsigned long)((150)*(4000000/4000.0)));
	opt asmopt_off
movlw	195
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
movwf	((??_get_temp+0)+0+1),f
	movlw	205
movwf	((??_get_temp+0)+0),f
u1797:
	decfsz	((??_get_temp+0)+0),f
	goto	u1797
	decfsz	((??_get_temp+0)+0+1),f
	goto	u1797
opt asmopt_on

	line	326
	
l4098:	
;main.c: 326: _delay((unsigned long)((150)*(4000000/4000.0)));
	opt asmopt_off
movlw	195
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
movwf	((??_get_temp+0)+0+1),f
	movlw	205
movwf	((??_get_temp+0)+0),f
u1807:
	decfsz	((??_get_temp+0)+0),f
	goto	u1807
	decfsz	((??_get_temp+0)+0+1),f
	goto	u1807
opt asmopt_on

	line	327
;main.c: 327: } else printf("bug");
	goto	l4102
	
l4100:	
	movlw	low(STR_1|8000h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?_printf)
	movlw	high(STR_1|8000h)
	movwf	((?_printf))+1
	fcall	_printf
	line	328
	
l4102:	
;main.c: 328: init = INIT();
	fcall	_INIT
	bcf	(get_temp@init/8),(get_temp@init)&7
	btfss	status,0
	goto	u1435
	bsf	(get_temp@init/8),(get_temp@init)&7
u1435:

	line	329
	
l4104:	
;main.c: 329: if (!init) {
	btfsc	(get_temp@init/8),(get_temp@init)&7
	goto	u1441
	goto	u1440
u1441:
	goto	l4108
u1440:
	line	330
	
l4106:	
;main.c: 330: TX(0xCC);
	movlw	(0CCh)
	fcall	_TX
	line	331
;main.c: 331: TX(0xBE);
	movlw	(0BEh)
	fcall	_TX
	line	333
;main.c: 333: temp1 = RX();
	fcall	_RX
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(get_temp@temp1)
	line	334
;main.c: 334: temp2 = RX();
	fcall	_RX
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(get_temp@temp2)
	line	337
	
l4108:	
;main.c: 336: }
;main.c: 337: temp_drob = temp1 & 0b00001111;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(get_temp@temp1),w
	movwf	(_temp_drob)
	
l4110:	
	movlw	(0Fh)
	andwf	(_temp_drob),f
	line	338
	
l4112:	
;main.c: 338: temp_drob = ((temp_drob*6)+2)/10;
	movlw	0Ah
	movwf	(?___awdiv)
	clrf	(?___awdiv+1)
	movf	(_temp_drob),w
	movwf	(?___wmul)
	clrf	(?___wmul+1)
	movlw	06h
	movwf	0+(?___wmul)+02h
	clrf	1+(?___wmul)+02h
	fcall	___wmul
	movf	(0+(?___wmul)),w
	addlw	low(02h)
	movwf	0+(?___awdiv)+02h
	movf	(1+(?___wmul)),w
	skipnc
	addlw	1
	addlw	high(02h)
	movwf	1+0+(?___awdiv)+02h
	fcall	___awdiv
	movf	(0+(?___awdiv)),w
	movwf	(_temp_drob)
	line	339
	
l4114:	
;main.c: 339: temp1 >>= 4;
swapf	(get_temp@temp1),f
	movlw	15
	andwf	(get_temp@temp1),f

	line	340
	
l4116:	
;main.c: 340: sign = temp2 & 0x80;
	movf	(get_temp@temp2),w
	movwf	(_sign)
	
l4118:	
	movlw	(080h)
	andwf	(_sign),f
	line	341
;main.c: 341: temp2 <<= 4;
swapf	(get_temp@temp2),f
	movlw	240
	andwf	(get_temp@temp2),f

	line	342
	
l4120:	
;main.c: 342: temp2 &= 0b01110000;
	movlw	(070h)
	andwf	(get_temp@temp2),f
	line	343
	
l4122:	
;main.c: 343: temp2 |= temp1;
	movf	(get_temp@temp1),w
	iorwf	(get_temp@temp2),f
	line	345
;main.c: 345: if (sign) { temperature = 127-temp2;
	movf	(_sign),w
	skipz
	goto	u1450
	goto	l4126
u1450:
	
l4124:	
	movf	(get_temp@temp2),w
	sublw	07Fh
	movwf	(_temperature)
	line	346
;main.c: 346: temp_drob = 10 - temp_drob;
	movf	(_temp_drob),w
	sublw	0Ah
	movwf	(_temp_drob)
	line	347
;main.c: 347: } else temperature = temp2;
	goto	l423
	
l4126:	
	movf	(get_temp@temp2),w
	movwf	(_temperature)
	line	350
	
l423:	
	return
	opt stack 0
GLOBAL	__end_of_get_temp
	__end_of_get_temp:
;; =============== function _get_temp ends ============

	signat	_get_temp,88
	global	_i2c_delay
psect	text936,local,class=CODE,delta=2
global __ptext936
__ptext936:

;; *************** function _i2c_delay *****************
;; Defined at:
;;		line 5 in file "X:\Hi-tech\Clock_and_temp\softi2c.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		None
;; Tracked objects:
;;		On entry : 0/20
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_i2c_start
;;		_i2c_stop
;;		_i2c_tx
;;		_i2c_rx
;; This function uses a non-reentrant model
;;
psect	text936
	file	"X:\Hi-tech\Clock_and_temp\softi2c.c"
	line	5
	global	__size_of_i2c_delay
	__size_of_i2c_delay	equ	__end_of_i2c_delay-_i2c_delay
	
_i2c_delay:	
	opt	stack 5
; Regs used in _i2c_delay: []
	line	6
	
l4082:	
;softi2c.c: 6: _delay((unsigned long)((5)*(4000000/4000000.0)));
		opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on
	opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on
	opt asmopt_off
	clrwdt
	opt asmopt_on

	line	7
	
l814:	
	return
	opt stack 0
GLOBAL	__end_of_i2c_delay
	__end_of_i2c_delay:
;; =============== function _i2c_delay ends ============

	signat	_i2c_delay,88
	global	_DCBconv
psect	text937,local,class=CODE,delta=2
global __ptext937
__ptext937:

;; *************** function _DCBconv *****************
;; Defined at:
;;		line 372 in file "X:\Hi-tech\Clock_and_temp\main.c"
;; Parameters:    Size  Location     Type
;;  source          1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  source          1    8[COMMON] unsigned char 
;;  temp_maj        1    9[COMMON] unsigned char 
;;  temp_min        1    7[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         3       0       0       0
;;      Temps:          1       0       0       0
;;      Totals:         4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		___lbdiv
;;		___bmul
;; This function is called by:
;;		_SetMin
;;		_SetHour
;;		_SetSeconds
;; This function uses a non-reentrant model
;;
psect	text937
	file	"X:\Hi-tech\Clock_and_temp\main.c"
	line	372
	global	__size_of_DCBconv
	__size_of_DCBconv	equ	__end_of_DCBconv-_DCBconv
	
_DCBconv:	
	opt	stack 6
; Regs used in _DCBconv: [wreg+status,2+status,0+pclath+cstack]
;DCBconv@source stored from wreg
	line	374
	movwf	(DCBconv@source)
	line	373
	
l4070:	
	line	374
;main.c: 374: unsigned char temp_maj=0;
	clrf	(DCBconv@temp_maj)
	line	375
	
l4072:	
;main.c: 375: temp_maj = source/10 ;
	movlw	(0Ah)
	movwf	(?___lbdiv)
	movf	(DCBconv@source),w
	fcall	___lbdiv
	movwf	(DCBconv@temp_maj)
	line	376
	
l4074:	
;main.c: 376: temp_min = source - temp_maj*10;
	movlw	(0F6h)
	movwf	(?___bmul)
	movf	(DCBconv@temp_maj),w
	fcall	___bmul
	movwf	(??_DCBconv+0)+0
	movf	(DCBconv@source),w
	addwf	0+(??_DCBconv+0)+0,w
	movwf	(DCBconv@temp_min)
	line	377
	
l4076:	
;main.c: 377: temp_maj <<= 4;
swapf	(DCBconv@temp_maj),f
	movlw	240
	andwf	(DCBconv@temp_maj),f

	line	379
	
l4078:	
;main.c: 379: return temp_maj+temp_min;
	movf	(DCBconv@temp_min),w
	addwf	(DCBconv@temp_maj),w
	line	381
	
l429:	
	return
	opt stack 0
GLOBAL	__end_of_DCBconv
	__end_of_DCBconv:
;; =============== function _DCBconv ends ============

	signat	_DCBconv,4217
	global	_printf
psect	text938,local,class=CODE,delta=2
global __ptext938
__ptext938:

;; *************** function _printf *****************
;; Defined at:
;;		line 461 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\lib\doprnt.c"
;; Parameters:    Size  Location     Type
;;  f               2    4[BANK0 ] PTR const unsigned char 
;;		 -> STR_23(3), STR_22(4), STR_21(18), STR_20(3), 
;;		 -> STR_19(4), STR_18(18), STR_17(3), STR_16(4), 
;;		 -> STR_15(16), STR_14(19), STR_13(18), STR_12(19), 
;;		 -> STR_11(8), STR_10(24), STR_9(4), STR_8(3), 
;;		 -> STR_7(2), STR_6(2), STR_5(18), STR_4(5), 
;;		 -> STR_3(6), STR_2(13), STR_1(4), 
;; Auto vars:     Size  Location     Type
;;  _val            4   11[BANK0 ] struct .
;;  c               1   15[BANK0 ] char 
;;  ap              1   10[BANK0 ] PTR void [1]
;;		 -> ?_printf(2), 
;;  prec            1    9[BANK0 ] char 
;;  flag            1    8[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    4[BANK0 ] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFE9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       2       0       0
;;      Locals:         0       8       0       0
;;      Temps:          0       2       0       0
;;      Totals:         0      12       0       0
;;Total ram usage:       12 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_putch
;;		___lwdiv
;;		___lwmod
;; This function is called by:
;;		_get_temp
;; This function uses a non-reentrant model
;;
psect	text938
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\doprnt.c"
	line	461
	global	__size_of_printf
	__size_of_printf	equ	__end_of_printf-_printf
	
_printf:	
	opt	stack 6
; Regs used in _printf: [wreg-fsr0h+status,2-btemp+1+pclath+cstack]
	line	537
	
l4016:	
	movlw	(?_printf+02h)&0ffh
	movwf	(printf@ap)
	line	540
	goto	l4068
	line	542
	
l4018:	
	movf	(printf@c),w
	xorlw	025h
	skipnz
	goto	u1341
	goto	u1340
u1341:
	goto	l4022
u1340:
	line	545
	
l4020:	
	movf	(printf@c),w
	fcall	_putch
	line	546
	goto	l4068
	line	552
	
l4022:	
	clrf	(printf@flag)
	line	638
	goto	l4032
	line	802
	
l4024:	
	movf	(printf@ap),w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(printf@c)
	
l4026:	
	incf	(printf@ap),f
	incf	(printf@ap),f
	line	812
	
l4028:	
	movf	(printf@c),w
	fcall	_putch
	line	813
	goto	l4068
	line	638
	
l4032:	
	movf	(printf@f+1),w
	movwf	(??_printf+0)+0+1
	movf	(printf@f),w
	movwf	(??_printf+0)+0
	incf	(printf@f),f
	skipnz
	incf	(printf@f+1),f
	movf	1+(??_printf+0)+0,w
	movwf	btemp+1
	movf	0+(??_printf+0)+0,w
	movwf	fsr0
	fcall	stringtab
	movwf	(printf@c)
	; Switch size 1, requested type "space"
; Number of cases is 4, Range of values is 0 to 105
; switch strategies available:
; Name         Bytes Cycles
; simple_byte    13     7 (average)
; direct_byte   119    13 (fixed)
;	Chosen strategy is simple_byte

	xorlw	0^0	; case 0
	skipnz
	goto	l1207
	xorlw	99^0	; case 99
	skipnz
	goto	l4024
	xorlw	100^99	; case 100
	skipnz
	goto	l4034
	xorlw	105^100	; case 105
	skipnz
	goto	l4034
	goto	l4028

	line	1254
	
l4034:	
	movf	(printf@ap),w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(printf@_val)
	incf	fsr0,f
	movf	indf,w
	movwf	(printf@_val+1)
	
l4036:	
	incf	(printf@ap),f
	incf	(printf@ap),f
	line	1256
	
l4038:	
	btfss	(printf@_val+1),7
	goto	u1351
	goto	u1350
u1351:
	goto	l4044
u1350:
	line	1257
	
l4040:	
	movlw	(03h)
	iorwf	(printf@flag),f
	line	1258
	
l4042:	
	comf	(printf@_val),f
	comf	(printf@_val+1),f
	incf	(printf@_val),f
	skipnz
	incf	(printf@_val+1),f
	line	1300
	
l4044:	
	clrf	(printf@c)
	incf	(printf@c),f
	line	1301
	
l4048:	
	clrc
	rlf	(printf@c),w
	addlw	low(_dpowers|8000h)
	movwf	fsr0
	movlw	high(_dpowers|8000h)
	skipnc
	addlw	1
	movwf	btemp+1
	fcall	stringtab
	movwf	(??_printf+0)+0
	fcall	stringtab
	movwf	(??_printf+0)+0+1
	movf	1+(??_printf+0)+0,w
	subwf	(printf@_val+1),w
	skipz
	goto	u1365
	movf	0+(??_printf+0)+0,w
	subwf	(printf@_val),w
u1365:
	skipnc
	goto	u1361
	goto	u1360
u1361:
	goto	l4052
u1360:
	goto	l4056
	line	1300
	
l4052:	
	incf	(printf@c),f
	
l4054:	
	movf	(printf@c),w
	xorlw	05h
	skipz
	goto	u1371
	goto	u1370
u1371:
	goto	l4048
u1370:
	line	1433
	
l4056:	
	movf	(printf@flag),w
	andlw	03h
	btfsc	status,2
	goto	u1381
	goto	u1380
u1381:
	goto	l4060
u1380:
	line	1434
	
l4058:	
	movlw	(02Dh)
	fcall	_putch
	line	1467
	
l4060:	
	movf	(printf@c),w
	movwf	(printf@prec)
	line	1469
	goto	l4066
	line	1484
	
l4062:	
	movlw	0Ah
	movwf	(?___lwmod)
	clrf	(?___lwmod+1)
	clrc
	rlf	(printf@prec),w
	addlw	low(_dpowers|8000h)
	movwf	fsr0
	movlw	high(_dpowers|8000h)
	skipnc
	addlw	1
	movwf	btemp+1
	fcall	stringtab
	movwf	(?___lwdiv)
	fcall	stringtab
	movwf	(?___lwdiv+1)
	movf	(printf@_val+1),w
	movwf	1+(?___lwdiv)+02h
	movf	(printf@_val),w
	movwf	0+(?___lwdiv)+02h
	fcall	___lwdiv
	movf	(1+(?___lwdiv)),w
	movwf	1+(?___lwmod)+02h
	movf	(0+(?___lwdiv)),w
	movwf	0+(?___lwmod)+02h
	fcall	___lwmod
	movf	(0+(?___lwmod)),w
	addlw	030h
	movwf	(printf@c)
	line	1516
	
l4064:	
	movf	(printf@c),w
	fcall	_putch
	line	1469
	
l4066:	
	decf	(printf@prec),f
	incf	((printf@prec)),w
	skipz
	goto	u1391
	goto	u1390
u1391:
	goto	l4062
u1390:
	line	540
	
l4068:	
	movf	(printf@f+1),w
	movwf	(??_printf+0)+0+1
	movf	(printf@f),w
	movwf	(??_printf+0)+0
	incf	(printf@f),f
	skipnz
	incf	(printf@f+1),f
	movf	1+(??_printf+0)+0,w
	movwf	btemp+1
	movf	0+(??_printf+0)+0,w
	movwf	fsr0
	fcall	stringtab
	movwf	(printf@c)
	movf	((printf@c)),f
	skipz
	goto	u1401
	goto	u1400
u1401:
	goto	l4018
u1400:
	line	1533
	
l1207:	
	return
	opt stack 0
GLOBAL	__end_of_printf
	__end_of_printf:
;; =============== function _printf ends ============

	signat	_printf,602
	global	_RX
psect	text939,local,class=CODE,delta=2
global __ptext939
__ptext939:

;; *************** function _RX *****************
;; Defined at:
;;		line 293 in file "X:\Hi-tech\Clock_and_temp\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1    4[COMMON] unsigned char 
;;  d               1    3[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/20
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         2       0       0       0
;;      Temps:          1       0       0       0
;;      Totals:         3       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_get_temp
;; This function uses a non-reentrant model
;;
psect	text939
	file	"X:\Hi-tech\Clock_and_temp\main.c"
	line	293
	global	__size_of_RX
	__size_of_RX	equ	__end_of_RX-_RX
	
_RX:	
	opt	stack 6
; Regs used in _RX: [wreg+status,2+status,0]
	line	294
	
l3132:	
;main.c: 294: unsigned char d = 0;
	clrf	(RX@d)
	line	295
;main.c: 295: unsigned char i = 0;
	clrf	(RX@i)
	line	296
;main.c: 296: for (i=0;i<8;i++){
	clrf	(RX@i)
	
l410:	
	line	297
;main.c: 297: TRISA2 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1066/8)^080h,(1066)&7
	line	298
;main.c: 298: _delay((unsigned long)((6)*(4000000/4000000.0)));
		opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on
	opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on
	opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on

	line	299
;main.c: 299: TRISA2 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1066/8)^080h,(1066)&7
	line	300
;main.c: 300: _delay((unsigned long)((4)*(4000000/4000000.0)));
		opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on
	opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on

	line	301
;main.c: 301: d>>=1;
	clrc
	rrf	(RX@d),f
	line	302
;main.c: 302: if (RA2 == 1) d |= 0x80;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(42/8),(42)&7
	goto	u901
	goto	u900
u901:
	goto	l3140
u900:
	
l3138:	
	bsf	(RX@d)+(7/8),(7)&7
	line	304
	
l3140:	
;main.c: 304: _delay((unsigned long)((60)*(4000000/4000000.0)));
	opt asmopt_off
movlw	19
movwf	(??_RX+0)+0,f
u1817:
decfsz	(??_RX+0)+0,f
	goto	u1817
	nop2	;nop
opt asmopt_on

	line	296
	
l3142:	
	incf	(RX@i),f
	
l3144:	
	movlw	(08h)
	subwf	(RX@i),w
	skipc
	goto	u911
	goto	u910
u911:
	goto	l410
u910:
	line	310
	
l3146:	
;main.c: 308: }
;main.c: 310: return d;
	movf	(RX@d),w
	line	312
	
l413:	
	return
	opt stack 0
GLOBAL	__end_of_RX
	__end_of_RX:
;; =============== function _RX ends ============

	signat	_RX,89
	global	_TX
psect	text940,local,class=CODE,delta=2
global __ptext940
__ptext940:

;; *************** function _TX *****************
;; Defined at:
;;		line 271 in file "X:\Hi-tech\Clock_and_temp\main.c"
;; Parameters:    Size  Location     Type
;;  cmd             1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  cmd             1    3[COMMON] unsigned char 
;;  i               1    5[COMMON] unsigned char 
;;  temp            1    4[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/20
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         3       0       0       0
;;      Temps:          1       0       0       0
;;      Totals:         4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_get_temp
;; This function uses a non-reentrant model
;;
psect	text940
	file	"X:\Hi-tech\Clock_and_temp\main.c"
	line	271
	global	__size_of_TX
	__size_of_TX	equ	__end_of_TX-_TX
	
_TX:	
	opt	stack 6
; Regs used in _TX: [wreg+status,2+status,0]
;TX@cmd stored from wreg
	line	273
	movwf	(TX@cmd)
	line	272
	
l3108:	
	line	273
;main.c: 273: unsigned char i = 0;
	clrf	(TX@i)
	line	274
	
l3110:	
;main.c: 274: temp = cmd;
	movf	(TX@cmd),w
	movwf	(TX@temp)
	line	275
	
l3112:	
;main.c: 275: for (i=0;i<8;i++) {
	clrf	(TX@i)
	
l403:	
	line	276
;main.c: 276: if (temp&0x01) {
	btfss	(TX@temp),(0)&7
	goto	u881
	goto	u880
u881:
	goto	l405
u880:
	line	277
	
l3116:	
;main.c: 277: TRISA2 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1066/8)^080h,(1066)&7
	line	278
;main.c: 278: _delay((unsigned long)((5)*(4000000/4000000.0)));
		opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on
	opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on
	opt asmopt_off
	clrwdt
	opt asmopt_on

	line	279
;main.c: 279: TRISA2 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1066/8)^080h,(1066)&7
	line	280
	
l3118:	
;main.c: 280: _delay((unsigned long)((70)*(4000000/4000000.0)));
	opt asmopt_off
movlw	23
movwf	(??_TX+0)+0,f
u1827:
decfsz	(??_TX+0)+0,f
	goto	u1827
opt asmopt_on

	line	281
;main.c: 281: } else {
	goto	l3126
	
l405:	
	line	282
;main.c: 282: TRISA2 = 0;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1066/8)^080h,(1066)&7
	line	283
	
l3120:	
;main.c: 283: _delay((unsigned long)((70)*(4000000/4000000.0)));
	opt asmopt_off
movlw	23
movwf	(??_TX+0)+0,f
u1837:
decfsz	(??_TX+0)+0,f
	goto	u1837
opt asmopt_on

	line	284
	
l3122:	
;main.c: 284: TRISA2 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1066/8)^080h,(1066)&7
	line	285
	
l3124:	
;main.c: 285: _delay((unsigned long)((5)*(4000000/4000000.0)));
		opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on
	opt asmopt_off
	nop2	;2 cycle nop
	opt asmopt_on
	opt asmopt_off
	clrwdt
	opt asmopt_on

	line	288
	
l3126:	
;main.c: 287: }
;main.c: 288: temp >>= 1;
	clrc
	rrf	(TX@temp),f
	line	275
	
l3128:	
	incf	(TX@i),f
	
l3130:	
	movlw	(08h)
	subwf	(TX@i),w
	skipc
	goto	u891
	goto	u890
u891:
	goto	l403
u890:
	line	291
	
l407:	
	return
	opt stack 0
GLOBAL	__end_of_TX
	__end_of_TX:
;; =============== function _TX ends ============

	signat	_TX,4216
	global	_INIT
psect	text941,local,class=CODE,delta=2
global __ptext941
__ptext941:

;; *************** function _INIT *****************
;; Defined at:
;;		line 257 in file "X:\Hi-tech\Clock_and_temp\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/20
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          1       0       0       0
;;      Totals:         1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_get_temp
;; This function uses a non-reentrant model
;;
psect	text941
	file	"X:\Hi-tech\Clock_and_temp\main.c"
	line	257
	global	__size_of_INIT
	__size_of_INIT	equ	__end_of_INIT-_INIT
	
_INIT:	
	opt	stack 6
; Regs used in _INIT: [wreg+status,2+status,0]
	line	259
	
l3092:	
;main.c: 258: static bit b;
;main.c: 259: TRISA2 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1066/8)^080h,(1066)&7
	line	260
;main.c: 260: TRISA2 = 0;
	bcf	(1066/8)^080h,(1066)&7
	line	261
	
l3094:	
;main.c: 261: _delay((unsigned long)((500)*(4000000/4000000.0)));
	opt asmopt_off
movlw	166
movwf	(??_INIT+0)+0,f
u1847:
decfsz	(??_INIT+0)+0,f
	goto	u1847
	clrwdt
opt asmopt_on

	line	262
	
l3096:	
;main.c: 262: TRISA2 = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1066/8)^080h,(1066)&7
	line	263
;main.c: 263: _delay((unsigned long)((65)*(4000000/4000000.0)));
	opt asmopt_off
movlw	21
movwf	(??_INIT+0)+0,f
u1857:
decfsz	(??_INIT+0)+0,f
	goto	u1857
	clrwdt
opt asmopt_on

	line	264
	
l3098:	
;main.c: 264: b = RA2;
	bcf	(INIT@b/8),(INIT@b)&7
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(42/8),(42)&7
	goto	u865
	bsf	(INIT@b/8),(INIT@b)&7
u865:

	line	265
;main.c: 265: _delay((unsigned long)((450)*(4000000/4000000.0)));
	opt asmopt_off
movlw	149
movwf	(??_INIT+0)+0,f
u1867:
decfsz	(??_INIT+0)+0,f
	goto	u1867
	nop2	;nop
opt asmopt_on

	line	267
	
l3100:	
;main.c: 267: return b;
	btfsc	(INIT@b/8),(INIT@b)&7
	goto	u871
	goto	u870
u871:
	goto	l3104
u870:
	
l3102:	
	clrc
	
	goto	l400
	
l3104:	
	setc
	
	line	269
	
l400:	
	return
	opt stack 0
GLOBAL	__end_of_INIT
	__end_of_INIT:
;; =============== function _INIT ends ============

	signat	_INIT,88
	global	___awdiv
psect	text942,local,class=CODE,delta=2
global __ptext942
__ptext942:

;; *************** function ___awdiv *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\sources\awdiv.c"
;; Parameters:    Size  Location     Type
;;  divisor         2    0[BANK0 ] int 
;;  dividend        2    2[BANK0 ] int 
;; Auto vars:     Size  Location     Type
;;  quotient        2    6[BANK0 ] int 
;;  sign            1    5[BANK0 ] unsigned char 
;;  counter         1    4[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       4       0       0
;;      Locals:         0       4       0       0
;;      Temps:          0       0       0       0
;;      Totals:         0       8       0       0
;;Total ram usage:        8 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_get_temp
;; This function uses a non-reentrant model
;;
psect	text942
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\awdiv.c"
	line	5
	global	__size_of___awdiv
	__size_of___awdiv	equ	__end_of___awdiv-___awdiv
	
___awdiv:	
	opt	stack 6
; Regs used in ___awdiv: [wreg+status,2+status,0]
	line	9
	
l3972:	
	clrf	(___awdiv@sign)
	line	10
	
l3974:	
	btfss	(___awdiv@divisor+1),7
	goto	u1271
	goto	u1270
u1271:
	goto	l3980
u1270:
	line	11
	
l3976:	
	comf	(___awdiv@divisor),f
	comf	(___awdiv@divisor+1),f
	incf	(___awdiv@divisor),f
	skipnz
	incf	(___awdiv@divisor+1),f
	line	12
	
l3978:	
	clrf	(___awdiv@sign)
	incf	(___awdiv@sign),f
	line	14
	
l3980:	
	btfss	(___awdiv@dividend+1),7
	goto	u1281
	goto	u1280
u1281:
	goto	l3986
u1280:
	line	15
	
l3982:	
	comf	(___awdiv@dividend),f
	comf	(___awdiv@dividend+1),f
	incf	(___awdiv@dividend),f
	skipnz
	incf	(___awdiv@dividend+1),f
	line	16
	
l3984:	
	movlw	(01h)
	xorwf	(___awdiv@sign),f
	line	18
	
l3986:	
	clrf	(___awdiv@quotient)
	clrf	(___awdiv@quotient+1)
	line	19
	
l3988:	
	movf	(___awdiv@divisor+1),w
	iorwf	(___awdiv@divisor),w
	skipnz
	goto	u1291
	goto	u1290
u1291:
	goto	l4008
u1290:
	line	20
	
l3990:	
	clrf	(___awdiv@counter)
	incf	(___awdiv@counter),f
	line	21
	goto	l3994
	line	22
	
l3992:	
	clrc
	rlf	(___awdiv@divisor),f
	rlf	(___awdiv@divisor+1),f
	line	23
	incf	(___awdiv@counter),f
	line	21
	
l3994:	
	btfss	(___awdiv@divisor+1),(15)&7
	goto	u1301
	goto	u1300
u1301:
	goto	l3992
u1300:
	line	26
	
l3996:	
	clrc
	rlf	(___awdiv@quotient),f
	rlf	(___awdiv@quotient+1),f
	line	27
	
l3998:	
	movf	(___awdiv@divisor+1),w
	subwf	(___awdiv@dividend+1),w
	skipz
	goto	u1315
	movf	(___awdiv@divisor),w
	subwf	(___awdiv@dividend),w
u1315:
	skipc
	goto	u1311
	goto	u1310
u1311:
	goto	l4004
u1310:
	line	28
	
l4000:	
	movf	(___awdiv@divisor),w
	subwf	(___awdiv@dividend),f
	movf	(___awdiv@divisor+1),w
	skipc
	decf	(___awdiv@dividend+1),f
	subwf	(___awdiv@dividend+1),f
	line	29
	
l4002:	
	bsf	(___awdiv@quotient)+(0/8),(0)&7
	line	31
	
l4004:	
	clrc
	rrf	(___awdiv@divisor+1),f
	rrf	(___awdiv@divisor),f
	line	32
	
l4006:	
	decfsz	(___awdiv@counter),f
	goto	u1321
	goto	u1320
u1321:
	goto	l3996
u1320:
	line	34
	
l4008:	
	movf	(___awdiv@sign),w
	skipz
	goto	u1330
	goto	l4012
u1330:
	line	35
	
l4010:	
	comf	(___awdiv@quotient),f
	comf	(___awdiv@quotient+1),f
	incf	(___awdiv@quotient),f
	skipnz
	incf	(___awdiv@quotient+1),f
	line	36
	
l4012:	
	movf	(___awdiv@quotient+1),w
	movwf	(?___awdiv+1)
	movf	(___awdiv@quotient),w
	movwf	(?___awdiv)
	line	37
	
l1366:	
	return
	opt stack 0
GLOBAL	__end_of___awdiv
	__end_of___awdiv:
;; =============== function ___awdiv ends ============

	signat	___awdiv,8314
	global	___lbdiv
psect	text943,local,class=CODE,delta=2
global __ptext943
__ptext943:

;; *************** function ___lbdiv *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lbdiv.c"
;; Parameters:    Size  Location     Type
;;  dividend        1    wreg     unsigned char 
;;  divisor         1    2[COMMON] unsigned char 
;; Auto vars:     Size  Location     Type
;;  dividend        1    3[COMMON] unsigned char 
;;  quotient        1    5[COMMON] unsigned char 
;;  counter         1    4[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         1       0       0       0
;;      Locals:         3       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_DCBconv
;;		_DispAll
;; This function uses a non-reentrant model
;;
psect	text943
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lbdiv.c"
	line	5
	global	__size_of___lbdiv
	__size_of___lbdiv	equ	__end_of___lbdiv-___lbdiv
	
___lbdiv:	
	opt	stack 5
; Regs used in ___lbdiv: [wreg+status,2+status,0]
;___lbdiv@dividend stored from wreg
	line	9
	movwf	(___lbdiv@dividend)
	
l3948:	
	clrf	(___lbdiv@quotient)
	line	10
	
l3950:	
	movf	(___lbdiv@divisor),w
	skipz
	goto	u1230
	goto	l3968
u1230:
	line	11
	
l3952:	
	clrf	(___lbdiv@counter)
	incf	(___lbdiv@counter),f
	line	12
	goto	l3956
	
l1244:	
	line	13
	clrc
	rlf	(___lbdiv@divisor),f
	line	14
	
l3954:	
	incf	(___lbdiv@counter),f
	line	12
	
l3956:	
	btfss	(___lbdiv@divisor),(7)&7
	goto	u1241
	goto	u1240
u1241:
	goto	l1244
u1240:
	line	16
	
l1246:	
	line	17
	clrc
	rlf	(___lbdiv@quotient),f
	line	18
	
l3958:	
	movf	(___lbdiv@divisor),w
	subwf	(___lbdiv@dividend),w
	skipc
	goto	u1251
	goto	u1250
u1251:
	goto	l3964
u1250:
	line	19
	
l3960:	
	movf	(___lbdiv@divisor),w
	subwf	(___lbdiv@dividend),f
	line	20
	
l3962:	
	bsf	(___lbdiv@quotient)+(0/8),(0)&7
	line	22
	
l3964:	
	clrc
	rrf	(___lbdiv@divisor),f
	line	23
	
l3966:	
	decfsz	(___lbdiv@counter),f
	goto	u1261
	goto	u1260
u1261:
	goto	l1246
u1260:
	line	25
	
l3968:	
	movf	(___lbdiv@quotient),w
	line	26
	
l1249:	
	return
	opt stack 0
GLOBAL	__end_of___lbdiv
	__end_of___lbdiv:
;; =============== function ___lbdiv ends ============

	signat	___lbdiv,8313
	global	___lwmod
psect	text944,local,class=CODE,delta=2
global __ptext944
__ptext944:

;; *************** function ___lwmod *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lwmod.c"
;; Parameters:    Size  Location     Type
;;  divisor         2    0[BANK0 ] unsigned int 
;;  dividend        2    2[BANK0 ] unsigned int 
;; Auto vars:     Size  Location     Type
;;  counter         1    9[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       4       0       0
;;      Locals:         1       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         1       4       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_printf
;; This function uses a non-reentrant model
;;
psect	text944
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lwmod.c"
	line	5
	global	__size_of___lwmod
	__size_of___lwmod	equ	__end_of___lwmod-___lwmod
	
___lwmod:	
	opt	stack 5
; Regs used in ___lwmod: [wreg+status,2+status,0]
	line	8
	
l3004:	
	movf	(___lwmod@divisor+1),w
	iorwf	(___lwmod@divisor),w
	skipnz
	goto	u711
	goto	u710
u711:
	goto	l3020
u710:
	line	9
	
l3006:	
	clrf	(___lwmod@counter)
	incf	(___lwmod@counter),f
	line	10
	goto	l3010
	line	11
	
l3008:	
	clrc
	rlf	(___lwmod@divisor),f
	rlf	(___lwmod@divisor+1),f
	line	12
	incf	(___lwmod@counter),f
	line	10
	
l3010:	
	btfss	(___lwmod@divisor+1),(15)&7
	goto	u721
	goto	u720
u721:
	goto	l3008
u720:
	line	15
	
l3012:	
	movf	(___lwmod@divisor+1),w
	subwf	(___lwmod@dividend+1),w
	skipz
	goto	u735
	movf	(___lwmod@divisor),w
	subwf	(___lwmod@dividend),w
u735:
	skipc
	goto	u731
	goto	u730
u731:
	goto	l3016
u730:
	line	16
	
l3014:	
	movf	(___lwmod@divisor),w
	subwf	(___lwmod@dividend),f
	movf	(___lwmod@divisor+1),w
	skipc
	decf	(___lwmod@dividend+1),f
	subwf	(___lwmod@dividend+1),f
	line	17
	
l3016:	
	clrc
	rrf	(___lwmod@divisor+1),f
	rrf	(___lwmod@divisor),f
	line	18
	
l3018:	
	decfsz	(___lwmod@counter),f
	goto	u741
	goto	u740
u741:
	goto	l3012
u740:
	line	20
	
l3020:	
	movf	(___lwmod@dividend+1),w
	movwf	(?___lwmod+1)
	movf	(___lwmod@dividend),w
	movwf	(?___lwmod)
	line	21
	
l1239:	
	return
	opt stack 0
GLOBAL	__end_of___lwmod
	__end_of___lwmod:
;; =============== function ___lwmod ends ============

	signat	___lwmod,8314
	global	___lwdiv
psect	text945,local,class=CODE,delta=2
global __ptext945
__ptext945:

;; *************** function ___lwdiv *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lwdiv.c"
;; Parameters:    Size  Location     Type
;;  divisor         2    2[COMMON] unsigned int 
;;  dividend        2    4[COMMON] unsigned int 
;; Auto vars:     Size  Location     Type
;;  quotient        2    6[COMMON] unsigned int 
;;  counter         1    8[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    2[COMMON] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         4       0       0       0
;;      Locals:         3       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         7       0       0       0
;;Total ram usage:        7 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_printf
;; This function uses a non-reentrant model
;;
psect	text945
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\lwdiv.c"
	line	5
	global	__size_of___lwdiv
	__size_of___lwdiv	equ	__end_of___lwdiv-___lwdiv
	
___lwdiv:	
	opt	stack 5
; Regs used in ___lwdiv: [wreg+status,2+status,0]
	line	9
	
l2978:	
	clrf	(___lwdiv@quotient)
	clrf	(___lwdiv@quotient+1)
	line	10
	
l2980:	
	movf	(___lwdiv@divisor+1),w
	iorwf	(___lwdiv@divisor),w
	skipnz
	goto	u671
	goto	u670
u671:
	goto	l3000
u670:
	line	11
	
l2982:	
	clrf	(___lwdiv@counter)
	incf	(___lwdiv@counter),f
	line	12
	goto	l2986
	line	13
	
l2984:	
	clrc
	rlf	(___lwdiv@divisor),f
	rlf	(___lwdiv@divisor+1),f
	line	14
	incf	(___lwdiv@counter),f
	line	12
	
l2986:	
	btfss	(___lwdiv@divisor+1),(15)&7
	goto	u681
	goto	u680
u681:
	goto	l2984
u680:
	line	17
	
l2988:	
	clrc
	rlf	(___lwdiv@quotient),f
	rlf	(___lwdiv@quotient+1),f
	line	18
	
l2990:	
	movf	(___lwdiv@divisor+1),w
	subwf	(___lwdiv@dividend+1),w
	skipz
	goto	u695
	movf	(___lwdiv@divisor),w
	subwf	(___lwdiv@dividend),w
u695:
	skipc
	goto	u691
	goto	u690
u691:
	goto	l2996
u690:
	line	19
	
l2992:	
	movf	(___lwdiv@divisor),w
	subwf	(___lwdiv@dividend),f
	movf	(___lwdiv@divisor+1),w
	skipc
	decf	(___lwdiv@dividend+1),f
	subwf	(___lwdiv@dividend+1),f
	line	20
	
l2994:	
	bsf	(___lwdiv@quotient)+(0/8),(0)&7
	line	22
	
l2996:	
	clrc
	rrf	(___lwdiv@divisor+1),f
	rrf	(___lwdiv@divisor),f
	line	23
	
l2998:	
	decfsz	(___lwdiv@counter),f
	goto	u701
	goto	u700
u701:
	goto	l2988
u700:
	line	25
	
l3000:	
	movf	(___lwdiv@quotient+1),w
	movwf	(?___lwdiv+1)
	movf	(___lwdiv@quotient),w
	movwf	(?___lwdiv)
	line	26
	
l1229:	
	return
	opt stack 0
GLOBAL	__end_of___lwdiv
	__end_of___lwdiv:
;; =============== function ___lwdiv ends ============

	signat	___lwdiv,8314
	global	___wmul
psect	text946,local,class=CODE,delta=2
global __ptext946
__ptext946:

;; *************** function ___wmul *****************
;; Defined at:
;;		line 3 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\sources\wmul.c"
;; Parameters:    Size  Location     Type
;;  multiplier      2    2[COMMON] unsigned int 
;;  multiplicand    2    4[COMMON] unsigned int 
;; Auto vars:     Size  Location     Type
;;  product         2    6[COMMON] unsigned int 
;; Return value:  Size  Location     Type
;;                  2    2[COMMON] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         4       0       0       0
;;      Locals:         2       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         6       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_get_temp
;; This function uses a non-reentrant model
;;
psect	text946
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\wmul.c"
	line	3
	global	__size_of___wmul
	__size_of___wmul	equ	__end_of___wmul-___wmul
	
___wmul:	
	opt	stack 6
; Regs used in ___wmul: [wreg+status,2+status,0]
	line	4
	
l2962:	
	clrf	(___wmul@product)
	clrf	(___wmul@product+1)
	line	7
	
l2964:	
	btfss	(___wmul@multiplier),(0)&7
	goto	u651
	goto	u650
u651:
	goto	l2968
u650:
	line	8
	
l2966:	
	movf	(___wmul@multiplicand),w
	addwf	(___wmul@product),f
	skipnc
	incf	(___wmul@product+1),f
	movf	(___wmul@multiplicand+1),w
	addwf	(___wmul@product+1),f
	line	9
	
l2968:	
	clrc
	rlf	(___wmul@multiplicand),f
	rlf	(___wmul@multiplicand+1),f
	line	10
	
l2970:	
	clrc
	rrf	(___wmul@multiplier+1),f
	rrf	(___wmul@multiplier),f
	line	11
	
l2972:	
	movf	((___wmul@multiplier+1)),w
	iorwf	((___wmul@multiplier)),w
	skipz
	goto	u661
	goto	u660
u661:
	goto	l2964
u660:
	line	12
	
l2974:	
	movf	(___wmul@product+1),w
	movwf	(?___wmul+1)
	movf	(___wmul@product),w
	movwf	(?___wmul)
	line	13
	
l1219:	
	return
	opt stack 0
GLOBAL	__end_of___wmul
	__end_of___wmul:
;; =============== function ___wmul ends ============

	signat	___wmul,8314
	global	___bmul
psect	text947,local,class=CODE,delta=2
global __ptext947
__ptext947:

;; *************** function ___bmul *****************
;; Defined at:
;;		line 3 in file "C:\Program Files\HI-TECH Software\PICC\9.71a\sources\bmul.c"
;; Parameters:    Size  Location     Type
;;  multiplier      1    wreg     unsigned char 
;;  multiplicand    1    2[COMMON] unsigned char 
;; Auto vars:     Size  Location     Type
;;  multiplier      1    4[COMMON] unsigned char 
;;  product         1    3[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         1       0       0       0
;;      Locals:         2       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         3       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_DCBconv
;;		_DispAll
;; This function uses a non-reentrant model
;;
psect	text947
	file	"C:\Program Files\HI-TECH Software\PICC\9.71a\sources\bmul.c"
	line	3
	global	__size_of___bmul
	__size_of___bmul	equ	__end_of___bmul-___bmul
	
___bmul:	
	opt	stack 5
; Regs used in ___bmul: [wreg+status,2+status,0]
;___bmul@multiplier stored from wreg
	movwf	(___bmul@multiplier)
	line	4
	
l3932:	
	clrf	(___bmul@product)
	line	7
	
l3934:	
	btfss	(___bmul@multiplier),(0)&7
	goto	u1211
	goto	u1210
u1211:
	goto	l3938
u1210:
	line	8
	
l3936:	
	movf	(___bmul@multiplicand),w
	addwf	(___bmul@product),f
	line	9
	
l3938:	
	clrc
	rlf	(___bmul@multiplicand),f
	line	10
	
l3940:	
	clrc
	rrf	(___bmul@multiplier),f
	line	11
	
l3942:	
	movf	(___bmul@multiplier),f
	skipz
	goto	u1221
	goto	u1220
u1221:
	goto	l3934
u1220:
	line	12
	
l3944:	
	movf	(___bmul@product),w
	line	13
	
l1213:	
	return
	opt stack 0
GLOBAL	__end_of___bmul
	__end_of___bmul:
;; =============== function ___bmul ends ============

	signat	___bmul,8313
	global	_putch
psect	text948,local,class=CODE,delta=2
global __ptext948
__ptext948:

;; *************** function _putch *****************
;; Defined at:
;;		line 7 in file "X:\Hi-tech\Clock_and_temp\usart.c"
;; Parameters:    Size  Location     Type
;;  byte            1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  byte            1    2[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 60/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         1       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_printf
;; This function uses a non-reentrant model
;;
psect	text948
	file	"X:\Hi-tech\Clock_and_temp\usart.c"
	line	7
	global	__size_of_putch
	__size_of_putch	equ	__end_of_putch-_putch
	
_putch:	
	opt	stack 5
; Regs used in _putch: [wreg]
;putch@byte stored from wreg
	movwf	(putch@byte)
	line	9
	
l2942:	
	line	10
;usart.c: 9: while(!TXIF)
	
l1164:	
	line	9
	btfss	(100/8),(100)&7
	goto	u621
	goto	u620
u621:
	goto	l1164
u620:
	line	11
	
l2944:	
;usart.c: 11: TXREG = byte;
	movf	(putch@byte),w
	movwf	(25)	;volatile
	line	12
	
l1167:	
	return
	opt stack 0
GLOBAL	__end_of_putch
	__end_of_putch:
;; =============== function _putch ends ============

	signat	_putch,4216
	global	_BCDconv
psect	text949,local,class=CODE,delta=2
global __ptext949
__ptext949:

;; *************** function _BCDconv *****************
;; Defined at:
;;		line 359 in file "X:\Hi-tech\Clock_and_temp\main.c"
;; Parameters:    Size  Location     Type
;;  source          1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  source          1    4[COMMON] unsigned char 
;;  temp_maj        1    3[COMMON] unsigned char 
;;  temp_min        1    2[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 60/20
;;		On exit  : 60/20
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         3       0       0       0
;;      Temps:          0       0       0       0
;;      Totals:         3       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_ReadMin
;;		_ReadHour
;; This function uses a non-reentrant model
;;
psect	text949
	file	"X:\Hi-tech\Clock_and_temp\main.c"
	line	359
	global	__size_of_BCDconv
	__size_of_BCDconv	equ	__end_of_BCDconv-_BCDconv
	
_BCDconv:	
	opt	stack 6
; Regs used in _BCDconv: [wreg+status,2+status,0]
;BCDconv@source stored from wreg
	line	361
	movwf	(BCDconv@source)
	line	360
	
l2928:	
	line	361
;main.c: 361: unsigned char temp_maj=0;
	clrf	(BCDconv@temp_maj)
	line	362
	
l2930:	
;main.c: 362: temp_min = source&15;
	movf	(BCDconv@source),w
	movwf	(BCDconv@temp_min)
	
l2932:	
	movlw	(0Fh)
	andwf	(BCDconv@temp_min),f
	line	363
	
l2934:	
;main.c: 363: temp_maj = source >> 4;
	swapf	(BCDconv@source),w
	andlw	(0ffh shr 4) & 0ffh
	movwf	(BCDconv@temp_maj)
	line	366
	
l2936:	
;main.c: 366: temp_maj *= 10;
	clrc
	rlf	(BCDconv@temp_maj),w
	addwf	(BCDconv@temp_maj),f
	addwf	(BCDconv@temp_maj),f
	clrc
	rlf	(BCDconv@temp_maj),f
	line	369
	
l2938:	
;main.c: 369: return temp_maj+temp_min;
	movf	(BCDconv@temp_min),w
	addwf	(BCDconv@temp_maj),w
	line	370
	
l426:	
	return
	opt stack 0
GLOBAL	__end_of_BCDconv
	__end_of_BCDconv:
;; =============== function _BCDconv ends ============

	signat	_BCDconv,4217
	global	_isr
psect	text950,local,class=CODE,delta=2
global __ptext950
__ptext950:

;; *************** function _isr *****************
;; Defined at:
;;		line 244 in file "X:\Hi-tech\Clock_and_temp\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  2  393[COMMON] int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 60/0
;;		Unchanged: FFF9F/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2
;;      Params:         0       0       0       0
;;      Locals:         0       0       0       0
;;      Temps:          2       0       0       0
;;      Totals:         2       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	text950
	file	"X:\Hi-tech\Clock_and_temp\main.c"
	line	244
	global	__size_of_isr
	__size_of_isr	equ	__end_of_isr-_isr
	
_isr:	
	opt	stack 3
; Regs used in _isr: [wreg+status,2+status,0]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+0
	movwf	saved_w
	movf	status,w
	movwf	(??_isr+0)
	movf	pclath,w
	movwf	(??_isr+1)
	ljmp	_isr
psect	text950
	line	246
	
i1l2918:	
;main.c: 246: if (TMR1IF) {
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(96/8),(96)&7
	goto	u61_21
	goto	u61_20
u61_21:
	goto	i1l395
u61_20:
	line	247
	
i1l2920:	
;main.c: 247: flag++;
	incf	(_flag),f	;volatile
	line	248
	
i1l2922:	
;main.c: 248: TMR1H = 0b10110000; TMR1L = 0x00;
	movlw	(0B0h)
	movwf	(15)	;volatile
	
i1l2924:	
	clrf	(14)	;volatile
	line	249
	
i1l2926:	
;main.c: 249: TMR1IF = 0;
	bcf	(96/8),(96)&7
	line	254
	
i1l395:	
	movf	(??_isr+1),w
	movwf	pclath
	movf	(??_isr+0),w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_isr
	__end_of_isr:
;; =============== function _isr ends ============

	signat	_isr,90
psect	text951,local,class=CODE,delta=2
global __ptext951
__ptext951:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
