Short description of what do I want:
*To connect my laptop power supply (DELL/ thinkpad) or USB cable or the power bank and generate wide range of high quality voltages from 0 to 15V [up to 2A] 
*To have possibility to measure voltage and current 
*To have screen indicating the measured/set voltage/current
*To have possibility to be connected to external control using uart/usb 
*To have possibility to manually set the Vout without using display (manual override)
*Split pcb into two pices - main board and indication/control. I have in mind some future uses of the core board, where the indication/control will be redundand
*Main board need to be designed in a such way that some components can be skipped: for instance, if an efficiency is not important, we could skip buck boost converter. Or if current measurement is not needed we can skip the sensor IC, or skip uC to use manual settings.

I do understand that there are tons of ready solutions, but I guess I just in the proper mood to design it. It gives some opportunities to learn new things and expand the home lab a bit. Plus, I will be using the modern ICs purchased from digikey and avoiding unpleasant debug of conterfeight IC with horrible reliability/perforamnce, at least this is the idea.







The lab power supply

The goal is to build a device which will be somewhat similar to RD000, and yes I understand that I also can buy it, but I want to have some fun with design. That what DIY is about, right? 
So I want a device, back of which will accepting all kinds of power supply (except obvious one - 110/220V :) ). Since I have tons of different laptop adapters, I think I don't need to go the transformer way and deal with high voltage at all. Ideally it would be good to have different ports at the back panel - as many as have in my house I think + 2 USB ports (usual one and USB-C). The front panel will have the display, knobs and ports - +/- (like most of lab supplies have) and I am also thinking to put an USB port as well, since, in the ideal case I will have a current measurements displayed it might be a nice way to see how much current USB device consumes. 

So long story short, let me describe functions I'd like to have:


Main board.
1. Obviously voltage regulation. I want to have it digitally controlled. The LM chip seems to be a great fit, there is a minimul level of external devices, it is costly (~10$ currently) but looks like a great product so far + it can provide up to 3A of the current.
In the datasheet they shown a way to regulate the voltage digitally - the DAC is connected to the non-inverting amplifier with gain = 4. We need that amplification since DAC likely will use a different voltage supply and it will be smaller than Vin. So, if I would want to set my Vout = 20, with gain = 4, I can have DAC output = 5V.
As far as I understood we just overpowering the error amplifier current source (50uA) and setting the voltage at the input of the amp. Technically, it is possible to use some digitally controlled resistor array, but I feel it will be more expensive in the end.
I think up to 20V should be enough for me.
note 1: it might be good to make it high efficient with step up dc-dc converter, although it will add another 10 bucks on top, on the other hand if I will be able to connect a single battery and still be able to regulate it nicely without cables...hm  	
IR38060MTRPBF looks like something we can use

Components: LM chip, DAC (hopefully I can find the microchip uC with good enough onboard DAC), non-inverting amp chip with gain>6 (I'm thinking to use 3.3V to power up the uC and other stuff, so DAC output can't be higher than that), capacitors for input and output (need to explore which one to choose).

2. Voltage measurements.
We likely need a buffer/amplifier with gain less than 1, since output is up to 20V, we need to attenuate it 6 times.
Obviously ADC will be required, with reference voltage equal 3V, 12 bit adc will give us around 700uV LSB. Let's say we choose an amp with gain = 1/7, then our LSB effectively x7, which is 4.9mV, doesn't sound too good... I would like to have at least 4 times better, which gives us 14bit ADC then.
Screw it, the Allegro devices looks like have nice solution out of shelf without resistance in the path (ACS37800). Not clear how to use it yet for the voltage measurement. 

Components: Amplifier (attenuator), ADC (at least 14bit).

3. Current measurements
If we will put a 25mOhm resistor in the voltage path then current passing thru it will generate a drop across it: 25mV for 1A and 2.5mV for 100mA. It is undesirable, and, technically there should be a way to feed the feedback signal to error amplifier after that resistor, maybe I should look for a different regulator? 
So, anyway, with such a resistor in a path we can hook up the amp across the resistor and route the output of the amplifier to the ADC input. Then the resulting voltage will be proportional to the current - again, with 14 bits adc we are speaking now about roughly 200uV LSB, which then converted to 8mA resolution... Well that is a screw up, I need something better.

Again, ACS37800 looks like a nice fit with 16 bit adc on-board. The datasheet says it has 2% error max, pricey but should do the trick and deliver quality I want.

4. Current limiting 
I want to be able to limit the max current and switch off the load in case if the current consumption is higher than a threshold. 
Currently, this aspect is not clear for me completely, an investigation is required.


5. Uart based control
For future possible usage and flexibility, an uart control is certainly something desired. 
We need to place usb compatible converter, is there anything from microchip family available to support usb uart directly or do I need to have separate module for that?

6. Manual control 
I'd like to have a possibility set voltage manually (without limiting current).
Should be some kind of a spst switch which override the control from remote to local manual, then we need a dip switch I suppose for like 12 bits (14 bits will be overkill I think for the manual control). A couple of switches to set voltage to frequently used levels: 0.9, 1.8V, 3.3V, 5V for example (so another 4 switches are needed).

7. Power from USB.
I want to have possibility to use USB as a power source (if there is no external voltage connected). 
The detection is technically easy - just put the pMOS transistor between usb and main power and hook up the gate of it to the main power (while it is connected to the ground through some resistor). In this case when there is no external power, the usb is used as Vin for voltage regulator, as soon as some voltage appearing at the main input it will switch off pMOS and USB supply from Vin.

8. Main spst switch
Hard cut off for both main and usb supplies

9. Voltage regulator for the uC and other chips - 3.3V supposedly, I don't care about linearity and noise of that one. Can be DC-DC converter.



The front panel board
1. OLED display


2. Switches for setting of most frequently used voltages (see p.6 of the main board)


3. Encoder


4. Main switch







Summary:

Questions/tasks:
1. How many bit for DAC is OK?
2. How many bit fof ADC is OK?
3. Opamp gains?
4. Is there a way to feed the feedback signal after the current measurement resistor?
5. Explore the other current sensing possibilities, I want to have uA resolution somehow. Maybe there are IC solutions already on the market. - Allegro systems have ACS37800, need to put more efforts into investigation - will 2% be enough for me?
6. What supply connectors should be used?
7. Can I put two pcb together and cut at home?
8. How exactly put the main switch to the pcb, what is that switch should look like
9. Is lt3083 a good choice for the project (price based mainly)
10. Explore can we use dc-dc converter to boost the voltage from a single battery and make the input of LDO high efficient (or two batteries)


Components:
Voltage regulator (main):
1. up to 20V outputs, up to 3A current
2. explicit feedback path, so remote sensing is possible
3. possibility to disconnect the load if current is higher than settled value

DC DC converter for uC: 



Current sensor ACS37800 (9)


uC:
?
DAC - 
ADC - 14bit min (depends on vref)
UART (if possible with direct USB suppor)
GPIO - 1 for manual switch, 12 bit for dip switch to set the voltage, 4 for preset to certain most frequently used voltages.
I2C - to control OLED and the current sensor IC 

Opamps:
#1 - for the DAC conversion, gain ~ 7
#2 - for the voltage measurements, gain ~ 1/7
#3 - for the current measurements, gain ?

USB to uart converted: (if there is no pic microcontroller with direct usb uart support)


Transistors:
pMOS to support about 1A of the current

Capacitors:


Resistors:


Connectors:
usb (mini or micro) for a main board
supply connectors (which type?)
external connectors to be compatible with laptop power adapters, usb-c, usb-b
external connectors for a fron panel to use with banana plug or alligators, or usual wires

Switches:
dip switch for manual bits (12 positions)
spst for pcb - manual mode override
spst for the main power delivery - should tolerate all the current


The central idea is to have two parts:
1. The main pcb with LM3383 (?) on the board for the 