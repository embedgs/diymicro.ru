#include <xc.h>
#include "lcd.h"

#define _XTAL_FREQ 12000000
#define State TRISB0
#define HumPin RB0

#define HumBut RB7
#define LightBut RB5

__CONFIG(WDTDIS & UNPROTECT & LVPDIS & HS & MCLREN & BORDIS);


const unsigned char digits[10] = {
                  0b00110000,            //0
				  0b00110001,            //1
				  0b00110010,            //2
				  0b00110011,            //3
				  0b00110100,            //4
				  0b00110101,            //5
				  0b00110110,            //6
				  0b00110111,            //7
				  0b00111000,            //8
				  0b00111001,            //9
				  };
unsigned char DHTbyte[3];

bit Humflag = 0;
volatile bit StartHumMeasure = 0;
volatile bit Tmr2Rel = 0;

volatile bit InteFlag = 0;


volatile unsigned char IsrCount = 0;
volatile unsigned char tempTmr0 = 0;

volatile unsigned int tempTmr1 = 0;

volatile unsigned int TickCount = 0;
			
volatile static bit RHStatus = 0;


display_digit(unsigned char digit, unsigned char pos);
void InitTimer0();
void GetRHandTemp();
void GetRHandTempRoutine();
void DisplayAll();
void InitTimer2();


void main() {
 
CMCON = 0x07;
TRISB7 = 1;
TRISB5 = 1;

__delay_ms(200);
__delay_ms(200);

lcd_init();
lcd_goto(0x00);
lcd_puts("Humidity =");

T0CS = 0;
PSA = 0;
GIE = 1;
PEIE = 1;
RBIE = 1;
TMR2IE = 1;
INTEDG = 0;
HumPin = 1;
IsrCount = 0;
Humflag = 0;

InitTimer2();
//GetRHandTempRoutine();
//DisplayAll();


for (;;)
{
 if (StartHumMeasure)
    {
            GetRHandTempRoutine();
            DisplayAll();
            StartHumMeasure = 0;
            SLEEP();
    }

 if (Tmr2Rel)
 	{
 		Tmr2Rel = 0;
 		if (TickCount >= 2000)
 		{
 			GetRHandTempRoutine();
            DisplayAll();
            TickCount = 0;
 		}
 		InitTimer2();
 	}   



}//while(1)


}

void interrupt isr()
{


if (INTF)
{
		tempTmr0 = TMR0;
		IsrCount++;
		InteFlag = 1;
        INTF = 0;
} //if (INTF)

if (RBIF)
{
   if (!HumBut)
    {
        __delay_ms(100);
        if (!HumBut)
        {
            StartHumMeasure = 1;

            //GetRHandTempRoutine();
           // DisplayAll();
           // SLEEP();
        }//second if
    }//fist if
   RBIF =0;

}


if (TMR2IF)
{
	TickCount++;
	Tmr2Rel = 1;
	TMR2IF = 0;
}

} //isr()


display_digit(unsigned char digit, unsigned char pos)
{
                                                        
    unsigned char TempDigit = 0;
    lcd_goto(pos);
    if (digit < 100)
    {
        lcd_goto(pos);
	    lcd_putch(digits[digit/10]);	
        digit = digit - ((digit/10)*10);
        lcd_putch(digits[digit]);
    } 

}

void InitTimer0()
{
	TMR0 = 0;
	OPTION_REG = 0b10010001;

}



void GetRHandTemp()
{
	

		INTE = 0;
		DHTbyte[0] = 0;
		DHTbyte[1] = 0;
		DHTbyte[2] = 0;
		Humflag = 0;
		IsrCount = 0;
		


		State = 1;
		State = 0;
		HumPin = 0;
		__delay_ms(20);
		State = 1;
		HumPin = 1;
		__delay_us(40);
		State = 1;
		INTEDG = 0;
		INTE = 1;
		
		unsigned char tmrcount = 0;

		while (!Humflag)
		{
		

			if (TMR0>254)									//попытка придумывания антизависаний
			{
				TMR0 = 0;
				tmrcount++;
				if (tmrcount>=254)
				{
					Humflag = 1;
					RHStatus = 0;
					
				}
			}	

			if (InteFlag)
			{
				INTE = 0;
				if (IsrCount==2) INTEDG = 1;

				if ((IsrCount > 2) && (IsrCount < 12))
				{
					//temp[IsrCount-3]=tempTmr0;
					DHTbyte[0]<<=1;
					if (tempTmr0>50) DHTbyte[0] |= 0b00000001;
					InitTimer0();
				}

				if ((IsrCount > 18) && (IsrCount < 28))
				{
					//temp[IsrCount-3]=tempTmr0;
					DHTbyte[1]<<=1;
					if (tempTmr0>50) DHTbyte[1] |= 0b00000001;
					InitTimer0();
				}

				if ((IsrCount > 34) && (IsrCount < 44))
				{
					//temp[IsrCount-3]=tempTmr0;
					DHTbyte[2]<<=1;
					if (tempTmr0>50) DHTbyte[2] |= 0b00000001;
					InitTimer0();
				}

				InteFlag = 0;
				INTE = 1;
			}//if (InteFlag)


			if (IsrCount>=43)
			{
				INTE = 0;
				Humflag = 1;
				if ((DHTbyte[2] == (DHTbyte[0]+DHTbyte[1]))&&(DHTbyte[2]))
				{
					//Humflag = 1;
					
					//Humflag = 1;
					IsrCount = 0;
					RHStatus = 1;
				
				}	
				//Humflag = 1;

			}


		}
}//GetRHandTemp


void GetRHandTempRoutine()
{

TMR2IE = 0;
TMR2ON = 0;

RHStatus = 0;
			
lcd_clear();
lcd_goto(0x00);
lcd_putch('*');
GetRHandTemp();										
unsigned char i = 1;


while (i<8)
{
if (!RHStatus)									//second check
{
		lcd_clear();	
		lcd_goto(i);
		lcd_putch('*');
		__delay_ms(200);
		__delay_ms(200);
		__delay_ms(200);
		__delay_ms(200);
		__delay_ms(200);
		
		GetRHandTemp();
		i++;
}//end of second check		
	else i = 10;	

}


if (i < 10)
	DHTbyte[0]	= 0;



TMR2IE = 1;
TMR2ON = 1;

}//GetRHandTempRoutine()



void DisplayAll()
{
		
	if (DHTbyte[0] > 0)
	{	
		lcd_clear();
		lcd_goto(0x00);
		lcd_puts("RH");
		lcd_goto(0x06);
		lcd_puts("Temp");
		display_digit(DHTbyte[0],0x40);
		display_digit(DHTbyte[1],0x46);
		lcd_goto(0x48);
		lcd_putch(0b11011111);								//??????
		lcd_putch(0b01000011);								//?
		lcd_goto(0x0D);
		lcd_putch('V');
		lcd_goto(0x0F);
		lcd_putch('T');
		lcd_goto(0x4D);
	} 
		else
		{
				lcd_clear();
				lcd_goto(0x00);
				lcd_puts("Error, call");
				lcd_goto(0x40);
				lcd_puts("+375297251214");		
		}

}

void InitTimer2()    										//~22 мс с кварцем 12 МГц
{
	TMR2 = 0x00; //стартуем с 0
	PR2 = 0xFF;  
	T2CKPS0 = 1; T2CKPS1 = 1; // /16
	TOUTPS2 = 1; TOUTPS1 = 1; TOUTPS3 = 1; TOUTPS0 = 1; //2 делитель делит на 16
	TMR2ON = 1; // Запуск таймера!
}