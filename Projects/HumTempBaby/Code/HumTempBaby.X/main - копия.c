#include <htc.h>
#include "lcd.h"

#define _XTAL_FREQ 12000000
#define State TRISB0
#define HumPin RB0

#define HumBut RB7
#define LightBut RB5

__CONFIG(WDTDIS & UNPROTECT & LVPDIS & HS & MCLREN);


const unsigned char digits[10] = {
                  0b00110000,            //0
				  0b00110001,            //1
				  0b00110010,            //2
				  0b00110011,            //3
				  0b00110100,            //4
				  0b00110101,            //5
				  0b00110110,            //6
				  0b00110111,            //7
				  0b00111000,            //8
				  0b00111001,            //9
				  };
unsigned char DHTbyte[3];

bit Humflag = 0;
bit StartHumMeasure = 0;

volatile bit InteFlag = 0;


volatile unsigned char IsrCount = 0;
volatile unsigned char tempTmr0 = 0;
volatile bit RBlowFlag = 0;
volatile bit RBhighFlag = 0;
volatile unsigned int tempTmr1 = 0;
unsigned int Range = 0;
unsigned char RHTrig;
unsigned char RangeTrig;
volatile unsigned int TickCount = 0;
unsigned char ManInToiCount = 0;
unsigned char ManInToiCountTrig = 10;					


volatile static bit RHStatus = 0;


display_digit(unsigned char digit, unsigned char pos);
void InitTimer0();
void GetRHandTemp();
void GetRHandTempRoutine();
void DisplayAll();

void main() {
 
CMCON = 0x07;
TRISB7 = 1;
TRISB5 = 1;

__delay_ms(200);
__delay_ms(200);

lcd_init();
lcd_goto(0x00);
lcd_puts("Humidity =");

T0CS = 0;
PSA = 0;
GIE = 1;
PEIE = 1;
RBIE = 1;
INTEDG = 0;
HumPin = 1;
IsrCount = 0;
Humflag = 0;

//GetRHandTempRoutine();
//DisplayAll();


for (;;)
{
 if (StartHumMeasure)
    {
            GetRHandTempRoutine();
            DisplayAll();
            StartHumMeasure = 0;
            //SLEEP();
    }



}//while(1)


}

void interrupt isr()
{


if (INTF)
{
		tempTmr0 = TMR0;
		IsrCount++;
		InteFlag = 1;
        INTF = 0;
} //if (INTF)

if (RBIF)
{
   if (!HumBut)
    {
        __delay_ms(100);
        if (!HumBut)
        {
            StartHumMeasure = 1;

            //GetRHandTempRoutine();
           // DisplayAll();
           // SLEEP();
        }//second if
    }//fist if
   RBIF =0;

}


} //isr()


display_digit(unsigned char digit, unsigned char pos)
{
                                                        
    unsigned char TempDigit = 0;
    lcd_goto(pos);
    if (digit < 100)
    {
        lcd_goto(pos);
	    lcd_putch(digits[digit/10]);	
        digit = digit - ((digit/10)*10);
        lcd_putch(digits[digit]);
    } 

}

void InitTimer0()
{
	TMR0 = 0;
	OPTION = 0b10010001; 

}

void GetRHandTemp()
{
	

		INTE = 0;
		DHTbyte[0] = 0;
		DHTbyte[1] = 0;
		DHTbyte[2] = 0;
		Humflag = 0;
		IsrCount = 0;
		


		State = 1;
		State = 0;
		HumPin = 0;
		__delay_ms(20);
		State = 1;
		HumPin = 1;
		__delay_us(40);
		State = 1;
		INTEDG = 0;
		INTE = 1;
		


		while (!Humflag)
		{
		
			if (InteFlag)
			{
				INTE = 0;
				if (IsrCount==2) INTEDG = 1;

				if ((IsrCount > 2) && (IsrCount < 12))
				{
					//temp[IsrCount-3]=tempTmr0;
					DHTbyte[0]<<=1;
					if (tempTmr0>50) DHTbyte[0] |= 0b00000001;
					InitTimer0();
				}

				if ((IsrCount > 18) && (IsrCount < 28))
				{
					//temp[IsrCount-3]=tempTmr0;
					DHTbyte[1]<<=1;
					if (tempTmr0>50) DHTbyte[1] |= 0b00000001;
					InitTimer0();
				}

				if ((IsrCount > 34) && (IsrCount < 44))
				{
					//temp[IsrCount-3]=tempTmr0;
					DHTbyte[2]<<=1;
					if (tempTmr0>50) DHTbyte[2] |= 0b00000001;
					InitTimer0();
				}

				InteFlag = 0;
				INTE = 1;
			}//if (InteFlag)


			if (IsrCount>=43)
			{
				INTE = 0;
				Humflag = 1;
				if ((DHTbyte[2] == (DHTbyte[0]+DHTbyte[1]))&&(DHTbyte[2]))
				{
					//Humflag = 1;
					
					//Humflag = 1;
					IsrCount = 0;
					RHStatus = 1;
				
				}	
				//Humflag = 1;

			}


		}
}//GetRHandTemp


void GetRHandTempRoutine()
{

TMR2IE = 0;
TMR2ON = 0;

RHStatus = 0;
			
lcd_clear();
lcd_goto(0x00);
lcd_putch('*');
GetRHandTemp();										//??????? 1

	if (!RHStatus)									//first if
	{
		lcd_clear();	
		lcd_goto(0x01);
		lcd_putch('*');
		__delay_ms(200);
		__delay_ms(200);
		__delay_ms(200);
		__delay_ms(200);
		__delay_ms(200);
		
		GetRHandTemp();
		
	else
		{		
		if (!RHStatus)								//??????? 3
		
		{
			lcd_clear();
			lcd_goto(0x00);
			lcd_putch('*');
			__delay_ms(200);
			__delay_ms(200);
			__delay_ms(200);
			__delay_ms(200);
			__delay_ms(200);
		
			GetRHandTemp();						


			if (!RHStatus)							//??????? 4
			
			{
				lcd_clear();
				lcd_goto(0x01);
				lcd_putch('*');
				__delay_ms(200);
				__delay_ms(200);
				__delay_ms(200);
				__delay_ms(200);
				__delay_ms(200);
		
				GetRHandTemp();	


				if (!RHStatus)						//??????? 5
			
				{
					lcd_clear();
					lcd_goto(0x00);
					lcd_putch('*');
					__delay_ms(200);
					__delay_ms(200);
					__delay_ms(200);
					__delay_ms(200);
					__delay_ms(200);
		
					GetRHandTemp();	

					if (!RHStatus)

					{	
						lcd_clear();
						lcd_goto(0x01);
						lcd_putch('*');
						__delay_ms(200);
						__delay_ms(200);
						__delay_ms(200);
						__delay_ms(200);
						__delay_ms(200);
			
						GetRHandTemp();	
	
						if (!RHStatus)

						{		
							lcd_clear();
							lcd_goto(0x00);
							lcd_putch('*');
							__delay_ms(200);
							__delay_ms(200);
							__delay_ms(200);
							__delay_ms(200);
							__delay_ms(200);
			
							GetRHandTemp();	
										if (!RHStatus)
										{
											DHTbyte[0]	= 0;

											lcd_clear();
											lcd_goto(0x00);
											lcd_puts("Error, call");
											lcd_goto(0x40);
											lcd_puts("+375297251214");

										}

						}				
					}					

				}	


			}	 
		} 
		} 

	}//end first if
}//GetRHandTempRoutine()



void DisplayAll()
{
		lcd_clear();
		lcd_goto(0x00);
		lcd_puts("RH");
		lcd_goto(0x06);
		lcd_puts("Temp");
		display_digit(DHTbyte[0],0x40);
		display_digit(DHTbyte[1],0x46);
		lcd_goto(0x48);
		lcd_putch(0b11011111);								//??????
		lcd_putch(0b01000011);								//?
		lcd_goto(0x0D);
		lcd_putch('V');
		lcd_goto(0x0F);
		lcd_putch('T');
		lcd_goto(0x4D);
		
}