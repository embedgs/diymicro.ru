/* 
 * File:   main.c
 * Author: sarge
 * diymicro.org
 * Created on April 7, 2024, 7:50 PM
 */

#include <stdio.h>
#include <stdlib.h>

#define _XTAL_FREQ 1000000 //The speed of your internal(or)external oscillator




// PIC16F18046 Configuration Bit Settings

// 'C' source line config statements

// CONFIG1
#pragma config FEXTOSC = OFF    // External Oscillator Selection bits (Oscillator not enabled)
#pragma config RSTOSC = HFINTOSC_1MHz// Reset Oscillator Selection bits (HFINTOSC (1MHz))
#pragma config CLKOUTEN = ON    // Clock Out Enable bit (CLKOUT function is enabled; FOSC/4 clock appears at OSC2)
#pragma config VDDAR = HI       // VDD Range Analog Calibration Selection bit (Internal analog systems are calibrated for operation between VDD = 2.3 - 5.5V)

// CONFIG2
#pragma config MCLRE = EXTMCLR  // Master Clear Enable bit (If LVP = 0, MCLR pin is MCLR; If LVP = 1, RA3 pin function is MCLR)
#pragma config PWRTS = PWRT_OFF // Power-up Timer Selection bits (PWRT is disabled)
#pragma config WDTE = OFF       // WDT Operating Mode bits (WDT disabled; SEN is ignored)
#pragma config BOREN = ON       // Brown-out Reset Enable bits (Brown-out Reset enabled, SBOREN bit is ignored)
#pragma config DACAUTOEN = OFF  // DAC Buffer Automatic Range Select Enable bit (DAC Buffer reference range is determined by the REFRNG bit)
#pragma config BORV = LO        // Brown-out Reset Voltage Selection bit (Brown-out Reset Voltage (VBOR) set to 1.9V)
#pragma config ZCD = OFF        // ZCD Disable bit (ZCD module is disabled; ZCD can be enabled by setting the ZCDSEN bit of ZCDCON)
#pragma config PPS1WAY = OFF    // PPSLOCKED One-Way Set Enable bit (The PPSLOCKED bit can be set and cleared as needed (unlocking sequence is required))
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable bit (Stack Overflow or Underflow will cause a reset)

// CONFIG3

// CONFIG4
#pragma config BBSIZE = BB512   // Boot Block Size Selection bits (512 words boot block size)
#pragma config BBEN = OFF       // Boot Block Enable bit (Boot Block disabled)
#pragma config SAFEN = OFF      // Storage Area Flash (SAF) Enable bit (SAF disabled)
#pragma config WRTAPP = OFF     // Application Block Write Protection bit (Application Block is NOT write protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot Block is NOT write protected)
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration Register is NOT write protected)
#pragma config WRTD = OFF       // Data EEPROM Write-Protection bit (Data EEPROM is NOT write-protected)
#pragma config WRTSAF = OFF     // Storage Area Flash (SAF) Write Protection bit (SAF is NOT write protected)
#pragma config LVP = ON         // Low Voltage Programming Enable bit (Low Voltage programming enabled. MCLR/Vpp pin function is MCLR. MCLRE Configuration bit is ignored)

// CONFIG5
#pragma config CP = OFF         // Program Flash Memory Code Protection bit (Program Flash Memory code protection is disabled)
#pragma config CPD = OFF        // Data EEPROM Code Protection bit (EEPROM code protection is disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>



volatile __bit door_button = 0;
volatile __bit door_sensor = 0;
volatile __bit bottom_sensor = 0;
volatile __bit top_sensor = 0;
volatile __bit nothing_happening = 0;
volatile __bit cabin_position = 0;      //0 the elevator is at the bottom, 1 - at the top
volatile __bit door_position = 0;       //0 the door is closed, 1 - the door is opened

void all_motors_off(void);
void led_off(void);
void led_door_opened(void);
void led_cabin_moving(void);
void door_open(void);
void door_close(void);
void lift_up(void);
void lift_dn(void);



/*
 * 
 */
int main() {
    //disable all the analog functions (dont need them)
    ANSELA = 0;
    ANSELC = 0;
    nothing_happening = 1;      //initially nothing happening 

    //-------------buttons definitions ----------------
    //buttons/sensors set we have
    TRISA2 = 1;     //that will be our button, RA2 
    TRISC0 = 1;     //that will be the door sensor
    TRISC1 = 1;     //that will be the bottom sensor
    TRISC2 = 1;     //that will be the top sensor

    IOCAN2 = 1;     //looking for negative edge at RA2
    IOCCN0 = 1;     //looking for negative edge at RC0
    IOCCN1 = 1;     //looking for negative edge at RC1
    IOCCN2 = 1;     //looking for negative edge at RC2

    IOCCF = 0;
    IOCAF = 0;
    //-------------end of buttons definitions ---------

    
    //-------------LED outputs ------------------------
    TRISB5 = 0;     //LED1 RB5
    TRISB6 = 0;     //LED2 RB6
    LATB5 = 0;
    LATB6 = 0;
    led_off();

    //-------------end of LED outputs -----------------

    
    //-------------PWM motor outputs ------------------
    //RC3 - main motor up, for that mode we dont need any PWM
    //RC6 - main motor down -> no need here, rather we will slow down rc3
    //RC7 - door motor open
    //RB7 - door motor close

    //-------------End of PWM motor outputs -----------


    all_motors_off();   //by default everything is disabled

    GIE = 1;
    PEIE = 1;
    IOCIE = 1;
    
    
    
    //PWM3 setup
    //TRISB7 = 0;
    /*
    RB7PPS = 0x0B;  //RC3 as a PWM3 output now
    TRISB7 = 1; //briefly disable rc3 output
    PWM3CON = 0; //disable pwm3
    T2PR = 4;       //setting pwm period to 50KHz
    PWM3DCH = 1;
    PWM3DCL = 0;
    TMR2IF = 0;
    T2CLKCON = 0x01;
    T2CON = 128;  //prescaler to 1, timer enabled
    TRISB7 = 0;
    PWM3CONbits.EN = 1;
    */


    while (1)
    {
        //checking if the door button being pressed
        if (door_button)
        {
            __delay_ms(200);
            if (!PORTAbits.RA2) //debounce
            {
                __delay_ms(200);
                __delay_ms(200);
                __delay_ms(200);
                __delay_ms(200);
                __delay_ms(200);
                __delay_ms(200);
                if (!PORTAbits.RA2)     //long button press launches the lift/down
                {
                    if ((!cabin_position)&&(!door_position))
                    {
                        lift_up();
                        do{
                            
                        }while(PORTCbits.RC2);  //empty cycle till top sensor will trigger
                        door_open();        //opening the door to hold the cabin

                        do{
                            //empty cycle, waiting till door will fully open and block the cabin
                        }while(PORTCbits.RC0);
                        all_motors_off();
                        led_off();
                        door_position = 1;
                        cabin_position = 1;
                        led_off();
                    }   //if ((!cabin_position)&&(!door_position))
                        else
                        {                   //cabin moving to the bottom
                           
                           if ((cabin_position)&&(door_position))
                           {
                            lift_up();      //keep the cabin at the top
                            door_close();   //close the door
                            __delay_ms(200);
                            __delay_ms(200);
                            __delay_ms(200);
                            __delay_ms(200);
                            __delay_ms(200);
                            
                            lift_dn();

                            do
                            {

                            }while (PORTCbits.RC1);     //until the bottom side is reached
                            all_motors_off();
                            door_position = 0;
                            cabin_position = 0;
                            led_off();
                            }   //if ((!cabin_position)&&(door_position))
                        }
                }   //if long button press
                    else        //if the button press was a short time
                    {
                        if (!door_position)
                        {
                            door_open();
                             do{
                                 //empty cycle, waiting till door will fully open and block the cabin
                             }while(PORTCbits.RC0);
                            all_motors_off(); 
                            door_position = 1;
                        }
                            else
                            {
                                door_close();
                                __delay_ms(200);
                                __delay_ms(200);
                                __delay_ms(200);
                                __delay_ms(200);
                                __delay_ms(200);
                                all_motors_off();
                                                                
                                door_position = 0;
                            }
                    }//else of the short bottom press
                //all is clear, back to waiting     
                IOCAF2 =0;
                IOCIE = 1;
                nothing_happening =1;
                door_button = 0;    

            }//if debounce
                else
                {
                    //if it was a wrong push, just clear everything
                    IOCAF2 = 0;
                    nothing_happening = 1;
                    IOCIE = 1;
                    door_button = 0;
                }//else


        }
    }
    
    
}//main

void __interrupt() isr(void)
{
    if ((IOCAF2)&&(nothing_happening))
    {
       door_button = 1;
       nothing_happening = 0;       //somebody has pressed the button
       IOCIE = 0;   //disable all other buttons/sensors 
    }
}


void all_motors_off(void)
{
    //let the pcb pullup to work
    TRISC3 = 1;
    TRISC6 = 1;
    TRISC7 = 1;
    TRISB7 = 1;
    PWM3CON = 0;    //disable PWM3
    T2CON = 0;  //prescaler to 1, timer disabled
}

void led_off(void)      //switching off both leds
{
    PORTBbits.RB5 = 1;
    PORTBbits.RB6 = 1;
}

void led_door_opened(void)  //turning on led when door is opened
{
    PORTBbits.RB5 = 0;
    PORTBbits.RB6 = 1;
}

void led_cabin_moving(void) //other led when cabin is moving
{
    PORTBbits.RB5 = 1;
    PORTBbits.RB6 = 0;
}

void door_open(void)    //opening the door
{
    led_door_opened();
    RC7PPS = 0x0B;  //RC7 as a PWM3 output now
    
    TRISC7 = 1; //briefly disable rc7 output
    PWM3CON = 0; //disable pwm3
    T2PR = 4;       //setting pwm period to 50KHz
    PWM3DCH = 0;    
    PWM3DCL = 64;   //lowest duty cycle
    
    TMR2IF = 0;
    T2CLKCON = 0x01;
    T2CON = 128;  //prescaler to 1, timer enabled
    TRISC7 = 0;
    PWM3CONbits.EN = 1; //enable PWM

}


void door_close(void)      //closing the door
{
    led_off();
    RB7PPS = 0x0B;  //RB7 as a PWM3 output now
    
    TRISB7 = 1; //briefly disable rb7 output
    PWM3CON = 0; //disable pwm3
    T2PR = 4;       //setting pwm period to 50KHz
    PWM3DCH = 0;    
    PWM3DCL = 64;   //lowest duty cycle
    
    TMR2IF = 0;
    T2CLKCON = 0x01;
    T2CON = 128;  //prescaler to 1, timer enabled
    TRISB7 = 0;
    PWM3CONbits.EN = 1; //enable PWM
}


void lift_up(void)      //function to move the cabin to the top
{
    led_cabin_moving();
    TRISC3 = 0;
    PORTCbits.RC3 = 0;  //just full power, no pwm needed
}

void lift_dn(void)      //elevator down is basically just "breaking" the cabin to not let the gravity work too much
{
    led_cabin_moving();
    TRISB7 = 1;
    TRISC7 = 1;

    RC3PPS = 0x0B;  //RC3 as a PWM3 output now
    
    TRISC3 = 1; //briefly disable rc3 output
    PWM3CON = 0; //disable pwm3
    T2PR = 4;       //setting pwm period to 50KHz
    PWM3DCH = 0;    
    PWM3DCL = 64;   //lowest duty cycle
    
    TMR2IF = 0;
    T2CLKCON = 0x01;
    T2CON = 128;  //prescaler to 1, timer enabled
    TRISC3 = 0;
    PWM3CONbits.EN = 1; //enable PWM
}