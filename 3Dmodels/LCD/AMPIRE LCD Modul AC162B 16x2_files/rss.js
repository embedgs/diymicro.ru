﻿function AddFeedIcon(rssUrl)
{
    var hd = document.documentElement.firstChild;
    if(hd != null)
    {
        var lnk = document.createElement('link');
        lnk.setAttribute("rel","alternate");
        lnk.setAttribute("type","application/rss+xml");
        lnk.setAttribute("title","RSS");
        lnk.setAttribute("href",rssUrl);
        hd.appendChild(lnk);
    }
}