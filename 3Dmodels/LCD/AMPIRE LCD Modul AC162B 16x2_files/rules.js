﻿//var xmlHttp & GetXmlHttpObject Moved to base.js
var pVals = "";
var pNames = "";
var sConfig = "";
var s2DView = "";
var sCatalogId = "";
var bUseDfltCfg = true;
var sLoadingImagePath = "images/loadingAnimation.gif";
var cFORCE_DOWNLOAD_FILENAME = "FORCE_DOWNLOAD_FILENAME";
var bValidParams = true; //SPR 509328
var updatedPreview = false;
var isV6CompatibleFileName = false;
var downloadedModelFile = "";
var v6DownloadURL = "";

//var bForceReload = false;
//var sFormat = "3d"; Moved to download-model.aspx
//sPartname defined in download-model.aspx

function updateRule(el)
{
    var sColor = "";
    var bValid = true;
    if(el.options) sColor = el.options[el.selectedIndex].style.backgroundColor;
    if(sColor=="#dedede" || sColor!="" || sColor.indexOf('rgb')>0) bValid = false;
    if(bValid)
    {
        getCurrentParams();
        callRulesHanlder();
    }
    else
    {
        var s = "You have selected an invalid combination of data.\nClick OK to reset the data. Otherwise click Cancel.";
        if(typeof(sInvalidParam)!='undefined') s = sInvalidParam;
        
        var r = confirm(s);
        if(r==true)
        {
            //ok clicked
            pVals = URLencode(el.value);
            var id = el.id;
            pNames = URLencode(id.replace("ddParam_", ""));
            callRulesHanlder();
        }
        else
        {
            //set the field back to it's initial value
            el.selectedIndex = 0;
        }
    }
 }

function callRulesHanlder()
{
    var url= basePath() + "RulesHandler.aspx?vals=" + pVals + "&names=" + pNames + "&modelid=" + sModelId;
    
    //Ensure the request isn't cached by adding a random querystring
    url += "&sid=" + Math.random();
    
    //ensure the imgProcessingParams src is the animated gif
    if(document.getElementById("imgProcessingParams").src != basePath() + "images/indicator_small.gif")
        document.getElementById("imgProcessingParams").src = basePath() + "images/indicator_small.gif";
    document.getElementById("imgResolvingRules").style.display = '';
    document.getElementById("configHr").style.display = 'none';
    xmlHttp = GetXmlHttpObject(stateChanged);
    xmlHttp.open("GET", url, true);
    xmlHttp.setRequestHeader("Content-type", "text/html");
    isBusy = true;
    xmlHttp.send(null);
}
function updateConfigParams(el) {
    getCurrentConfig();

    var url = basePath() + "ConfigParamHandler.aspx?config=" + sConfig + "&modelid=" + sModelId;
    //Ensure the request isn't cached by adding a random querystring
    url += "&sid=" + Math.random();

    xmlHttp = GetXmlHttpObject(configState);
    xmlHttp.open("GET", url, true);
    isBusy = true;
    xmlHttp.send(null);
}

function updatePreview() {
    updatedPreview = true;

    var isSafari = false;
    if ((navigator.userAgent.indexOf("Safari/") >= 0) && (navigator.userAgent.indexOf("Chrome") == -1))
        isSafari = true;

    if (isSafari) {
        document.getElementById("divUpdatingPreview").style.display = '';
        var viewInedrawings = document.getElementById("btnDownloadForEdrawings_LoggedIn");
        if (viewInedrawings != null && viewInedrawings != 'undefined')
            viewInedrawings.disabled = true;
        var btndownload = document.getElementById("btnDownload");
        if (btndownload != null && btndownload!= 'undefined')
            btndownload.disabled = true;
    }
    
    // update GA download-model page
    pageTracker._trackPageview(basePath() + "download-model.aspx" ); 
    //allow downloading of parameter based configurations
    bUseDfltCfg = false;   
    //select the next tab if the image tab is first.
    setGeometryTab();
    
    getCurrentParams();
    getCurrentConfig();
    
    if (bValidParams) {
    //what is the selected tab
    var format = "3d";
        if (typeof (ModelTabs) != 'undefined') {
        var activeTab = ModelTabs.getActiveTab();
            if (activeTab != null) {
                if (activeTab.id.toLowerCase().indexOf("2d") > -1) {
                format = "2d";
            }
        }
    }
        
    //call the model preview handler
    var url= basePath() + "GetModelPreview.aspx?vals=" + pVals + "&names=" + pNames;
    url += "&type=" + format;
    url += "&catid=" + sCatalogId;
    url += "&modelid=" + sModelId;
    if(sConfig!="") url += "&config=" + sConfig;

    //Ensure the request isn't cached by adding a random querystring
    url += "&sid=" + Math.random();

        //SPR 517024 there is need of updating configuration for browser without 3DPreview support (FF,Chrome) 
        if (format == "3d" && eDraw == "True" && eDrawPreviewOnly == "True") {
            xmlHttp = GetXmlHttpObject(previewState);
            xmlHttp.open("GET", url, false);
            isBusy = true;
            xmlHttp.send(null);
            return;
        }


    //Ensure the download section is hidden
    if(typeof(dlg2)!='undefined') dlg2.hide();
    
    //loading animation
        if (format == "2d") {
        if(document.getElementById("PreviewFrame2D")!=null)
        document.getElementById("PreviewFrame2D").src = basePath() + sLoadingImagePath;
    }
        else {
            if (eDraw == "True" && document.getElementById("PreviewFrame3DED") != null) {
                document.getElementById("PreviewFrame3DED").src = basePath() + sLoadingImagePath;
            }
            if (eDraw == "True" && document.getElementById("PreviewFrame3DED") == null && document.getElementById("PreviewFrame3D") != null) {
            document.getElementById("PreviewFrame3D").src = basePath() + sLoadingImagePath;
    } 
        }

        xmlHttp = GetXmlHttpObject(previewState);
        xmlHttp.open("GET", url, false);
        isBusy = true;
        xmlHttp.send(null);
    }
    //SPR 687510
    document.getElementById("imgResolvingRules").style.display = 'none';
    document.getElementById("configHr").style.display = '';

    //SPR 698937: Processing wheel should stop after preview updated and if any error occured
    if (document.getElementById("PreviewFrame3D").style.height == "") {
        document.getElementById("PreviewFrame3D").style.height = "375px";
    }   
}

function pdfDownloadModel(arg, arg2) {
    sFormat = '3D';
    var sFormatName, sVersion, sUnits, sZip, bZip, bAllConfig;
    bZip = false;
    sFormatName = "pdfmodel";
    sVersion = "";
    sUnits = "";
    bAllConfig = false;
    var fd = arg;
    var fname = arg2;

    //hide the NextGen Button for PDF model
    var nextGenBtn = document.getElementById("nextGenUpload");
    if(nextGenBtn!=null && nextGenBtn!='undefined')
        nextGenBtn.style.display = 'none';

    str2 = strPdf;

    //don't use the parameters unless the preview has been clicked
    //at least once.
    if (!bUseDfltCfg) {
        getCurrentParams();
    }
    else {
        if (arg != 'swk') {
            pVals = "";
            pNames = "";
            get2DView();
        }
    }

    sendRatingReminderEmail(sCatalogId, sModelId);

    var url = basePath() + "GetModelDownload.aspx?";
    url += "format=" + sFormatName + "&version=" + sVersion;
    url += "&units=" + sUnits + "&zip=" + bZip + "&allconfig=" + bAllConfig;
    url += "&vals=" + pVals + "&names=" + pNames;
    url += "&type=" + sFormat; //2d or 3d
    url += "&catid=" + sCatalogId;
    url += "&view=" + s2DView;
    url += "&modelid=" + sModelId;
    if (sConfig != "") url += "&config=" + sConfig;

    //Ensure the request isn't cached by adding a random querystring
    url += "&sid=" + Math.random();

    if (arg != 'swk') {
        var link = document.getElementById("hlModelLink");
        link.style.display = "none";
        var wait = document.getElementById("imgDownloadWait");
        //wait.style.display = "inline";
        wait.src = basePath() + "images/indicator_small.gif";
        var lbl = document.getElementById("lblDownloadStatus");
        lbl.innerHTML = str1;
    }
    if (typeof (dlg2) != 'undefined') dlg2.show();
    
    if (arg != 'swk') 
    {
        xmlHttp = GetXmlHttpObject(downloadStatePDF0);
        xmlHttp.open("GET", url, true);
        isBusy = true;
        xmlHttp.send(null);  
        downloadStatePDF(fd,fname);
    }
    else 
    {
        xmlHttp = GetXmlHttpObject(downloadStateSWK);
    xmlHttp.open("GET", url , true);
    isBusy = true;
    xmlHttp.send(null);
}

}

function downloadStatePDF0() 
{
}

function downloadStatePDF(file,filename) {
    //if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
        //var url = xmlHttp.responseText;
        var url = file;
    //url = "C:\\Catalogs\\Madcap2\\Words\\Words.pdf";
        if (typeof (dlg2) != 'undefined') dlg2.hide();
        if (!urlHasError(url) && url != "") {
            isBusy = false;
            redirectCheck(url);
            var sFormatName = "pdf";
            var wait = document.getElementById("imgDownloadWait");
            //wait.style.display = "none";
            wait.src = basePath() + "images/icon_download.gif";
            var lbl = document.getElementById("lblDownloadStatus");
            lbl.innerHTML = str2;
            var link = document.getElementById("hlModelLink");
            link.href = url.replace(/#/g, '%23');
            //Force the download only for the following formats
            //so they don't open in the browser
            if (sFormatName == "edrw" || sFormatName == "pdf")// || sFormatName=="sldprt")
            {
                link.href = basePath() + "FileDownloadHandler.aspx?file=" + URLencode(url) + "&type=url&filename=" + filename;
                //link.setAttribute("target", "_blank");
            }
            link.style.display = "inline";
        }
        if (typeof (dlg2) != 'undefined') dlg2.show();
    //}
}

function downloadModel(arg) {
    var sFormatName, sVersion, sUnits, sZip, bZip, bAllConfig;
    
    //hide the NextGen Button when download operation starts
    var nextGenBtn = document.getElementById("nextGenUpload");
    if(nextGenBtn!=null && nextGenBtn!='undefined')
        nextGenBtn.style.display = 'none';
    
    bZip = false;
    sFormatName = document.getElementById("ddFormats").value;
    sVersion = document.getElementById("ddVersions").value;
    sUnits = document.getElementById("ddUnits").value;
    bZip = document.getElementById("cbZipped").checked;


    //Identify whether NextGen is running on client machine
    var bNextGenRunning = false;
    try {
        bNextGenRunning = isnextGenServiceRunning();
    }
    catch (err) {
        bNextGenRunning = false;
    }

    if (strModelType == 'pdf') {
        str2 = strPdf;
    }
    else if (sFormatName == '3dxml' || sFormatName == 'step' || sFormatName == 'sldprt') 
    {
        if (bNextGenRunning == true) 
            str2 = str3dxml;
        else
            str2 = strOtherFormats;
    }
    else {
        if (bNextGenRunning == true && sFormat.toLowerCase() == "3d")
            str2 = strNextGen;
        else 
            str2 = strOtherFormats;
    }

    bAllConfig = false;
    if(document.getElementById("cbDownloadAll")!=null)
    {
        bAllConfig = document.getElementById("cbDownloadAll").checked;
    }
	
	if(arg=='swk')
	{
	    bZip = true;
	    getCurrentParams();
        getCurrentConfig();
        if(sConfig=="" && pVals=="") sConfig = cFORCE_DOWNLOAD_FILENAME;
	}
	
    if(sUnits==null) sUnits = "";
    
    //don't use the parameters unless the preview has been clicked
    //at least once.
    if(!bUseDfltCfg) 
    {
        getCurrentParams();
    }
    else
    {
        if(arg!='swk')
	    {
	        pVals = "";
            pNames = "";
            get2DView();
        }
    }

    sendRatingReminderEmail(sCatalogId, sModelId);

    var url= basePath() + "GetModelDownload.aspx?";
    url += "format=" + sFormatName + "&version=" + sVersion;
    url += "&units=" + sUnits + "&zip=" + bZip + "&allconfig=" + bAllConfig;
    url += "&vals=" + pVals + "&names=" + pNames;
    url += "&type=" + sFormat; //2d or 3d
    url += "&catid=" + sCatalogId;
    url += "&view=" + s2DView;
    url += "&modelid=" + sModelId;
    url += "&bNextGenRunning=" + bNextGenRunning;
    if(sConfig!="") url += "&config=" + sConfig;
    if (typeof(sFeaturePos) != 'undefined' && sFeaturePos != "") url += "&featurePos=" + sFeaturePos;
    
    //Ensure the request isn't cached by adding a random querystring
    url += "&sid=" + Math.random();

   
	
	if(arg!='swk')
	{
        var link = document.getElementById("hlModelLink");
        link.style.display = "none";
        var wait = document.getElementById("imgDownloadWait");
        //wait.style.display = "inline";
        wait.src = basePath() + "images/indicator_small.gif";
        var lbl = document.getElementById("lblDownloadStatus");
        lbl.innerHTML = str1;
    }
    if (typeof (dlg2) != 'undefined') dlg2.show();

   
	
    if(arg!='swk')
	{
	    xmlHttp=GetXmlHttpObject(downloadState);
	} else {
	    xmlHttp=GetXmlHttpObject(downloadStateSWK);
	}

    xmlHttp.open("GET", url , true);
    isBusy = true;
    xmlHttp.send(null);
}

function previewState()
{
    if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
        if (xmlHttp.status == 200) {
        var partUrl = xmlHttp.responseText;
        isBusy = false;
            if (!urlHasError(partUrl)) {
                redirectCheck(partUrl);
           
            var isSafari = false;
            if ((navigator.userAgent.indexOf("Safari/") >= 0) && (navigator.userAgent.indexOf("Chrome") == -1))
                 isSafari = true;
            //what's the active tab
            var format = "3d";
                if (typeof (ModelTabs) != 'undefined') {
                var activeTab = ModelTabs.getActiveTab();
                    if (activeTab != null) {
                        if (activeTab.id.toLowerCase().indexOf("2d") > -1) {
                        format = "2d";
                    }
                }
            }
            
            var sNewUrl = basePath() + "Model-Preview.aspx?"; //partname is held in session
            sNewUrl += "format=" + format + "&updated=true" + "&catalogid=" + sCatalogId + "&modelid=" + sModelId;
            //Ensure the request isn't cached by adding a random querystring
            sNewUrl += "&sid=" + Math.random();
            if (eDraw == "True")
                sNewUrl += "&edraw=true";

            if (isSafari) {

                var IFrameObj = document.getElementById("SafariFrame");

                IFrameObj.setAttribute('src', sNewUrl);
                IFrameObj.style.display = "none";
            }
            else {
                if (format == "3d" && eDraw == "True" && document.getElementById("preview3DContainerED") != null) {
                    createPreviewIFrameED("preview3DContainerED", "PreviewFrame3DED", sNewUrl);
                }
                else if (format == "3d" && eDraw == "True" && document.getElementById("preview3DContainerED") == null && document.getElementById("preview3DContainer") != null) {
                    createPreviewIFrameED("preview3DContainer", "PreviewFrame3D", sNewUrl);
                }
                else if (format == "3d" && eDraw == "False") {
                    createPreviewIFrame(document.getElementById("preview3DContainer"), "PreviewFrame3D", sNewUrl);
                }
                else if (format == "2d") {
                    if (-1 == sNewUrl.search("catalogid=")) {
                        sNewUrl = sNewUrl + "&catalogid=" + sCatalogId
                    }
                    if (s2DView == "") {
                        // CHANGE DEFAULT TO 4 VIEW OR PREDEFINED
                        sNewUrl = sNewUrl + "&view=4";
                    }
                    else {
                        sNewUrl = sNewUrl + "&view=" + s2DView;
                    }
                    document.getElementById("PreviewFrame2D").src = sNewUrl;
                }
            }

        }
        else 
            {
            var isIE, sDisplay = "inline";
            navigator.userAgent.indexOf("MSIE")>=0? isIE = true: isIE = false;
            if(!isIE) sDisplay = "table";
            var link = document.getElementById("hlModelLink");
            link.style.display = "none";
            document.getElementById("downloadSection").style.display = sDisplay;
            var wait = document.getElementById("imgDownloadWait");
            //wait.style.display = "none";
            wait.src =  basePath() + "images/icon_alert.gif";
            var lbl = document.getElementById("lblDownloadStatus");
            
            //force any long nonbreaking string
            var re = new RegExp(/\\/g);
            var sReplace = "&shy;\\";
            if(!isIE) sReplace = "<wbr/>\\";
            lbl.innerHTML = partUrl.replace(re, sReplace);
            
            if(typeof(dlg2)!='undefined') dlg2.show();
            
            if (sFormat.toLowerCase() == "3d") 
            {
                //if (document.getElementById("PreviewFrame3D") != null)
                //document.getElementById("PreviewFrame3D").src = "about:blank";
                if (document.getElementById("PreviewFrame3DED") != null)    
                    document.getElementById("PreviewFrame3DED").src = "about:blank";
            }
                else if (sFormat.toLowerCase() == "2d") {
                document.getElementById("PreviewFrame2D").src = "about:blank";
            }
        }
    }
        else {
            handleUnspecError();
        }
}
}

function createPreviewIFrameED(elName, frameId, url) {
    var el = document.getElementById(elName);
    var IFrameObj = document.getElementById(frameId);
    var prevWidth = '400px';
    if (IFrameObj) {
        if (IFrameObj.width != null && IFrameObj.width > 0)
            prevWidth = IFrameObj.width + 'px';
        if (IFrameObj.style.width != null && IFrameObj.style.width != "")
            prevWidth = IFrameObj.style.width;
        el.removeChild(IFrameObj);
        IFrameObj = null;
    }
    if (document.createElement) {
        try {
            var tempIFrame = document.createElement('iframe');
            tempIFrame.setAttribute('id', frameId);
            tempIFrame.setAttribute('class', 'previewFrameED');
            tempIFrame.setAttribute('src', url);
            tempIFrame.setAttribute('scrolling', 'no');
            tempIFrame.setAttribute('frameBorder', '0');
            tempIFrame.style.border = '0px';
            tempIFrame.style.width = prevWidth;
            tempIFrame.style.height = '400px';

            IFrameObj = el.appendChild(tempIFrame);

            if (document.getElementById("preview3DContainerED") != null) 
                document.getElementById("preview3DContainerED").style.display = "";
            else if (document.getElementById("preview3DContainer") != null)
                document.getElementById("preview3DContainer").style.display = "";
        }
        catch (exception) {

        }
    }
}


function createPreviewIFrame (el, frameId, url) {

    var IFrameObj = document.getElementById(frameId);
    var previewDiv = document.getElementById("preview3DContainer");
    var cssClass = null;
    var prevWidth = '400px';
    if (IFrameObj) {
        cssClass = IFrameObj.getAttribute('class');
        if(IFrameObj.width != null && IFrameObj.width > 0)
            prevWidth = IFrameObj.width + 'px';
        if (IFrameObj.style.width != null && IFrameObj.style.width != "")
            prevWidth = IFrameObj.style.width;
        el.removeChild(IFrameObj);
        IFrameObj = null;
    }
  
    if (document.createElement) 
    {
        try 
        {
              var tempIFrame=document.createElement('iframe');
              tempIFrame.setAttribute('id', frameId);
              tempIFrame.setAttribute('class', 'previewFrame');
              if (cssClass) {
                  if (cssClass == 'previewFrame3DXML') {
                      tempIFrame.setAttribute('class', 'previewFrame3DXML');
                      tempIFrame.style.height = '432px';
                      if (previewDiv) {
                            previewDiv.setAttribute('class', 'previewContainer3DXML');
                            previewDiv.style.height = '432px';
                      }
                  }
                  else {
                      tempIFrame.setAttribute('class', 'previewFrame');
                      tempIFrame.style.height = '432px';
                  }
              }
                
              tempIFrame.setAttribute('src', url);
              tempIFrame.setAttribute('scrolling', 'no');
              tempIFrame.setAttribute('frameBorder', '0');
              tempIFrame.style.border='0px';
              tempIFrame.style.width = prevWidth;

              // tempIFrame.style.display = 'none';
              el.style.background = "transparent url('" + basePath() + "images/loadingAnimation.gif') top left)";
              IFrameObj = el.appendChild(tempIFrame);              
              
              if(window.navigator.appName.indexOf("Netscape") == -1)
                  document.getElementById("preview3DContainer").style.display = "";              
        } 
        catch(exception) 
        {

        }
    }
}

function downloadState()
{
    if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
    {
        if (xmlHttp.status != 200) {
            handleUnspecError();
            return;
        }

        var url = xmlHttp.responseText;

        if(typeof(dlg2)!='undefined') dlg2.hide();
        if (!urlHasError(url) && url != "") {
        
            var fileV6CompatibleString = url.substring(url.indexOf("&isFileV6Compatible"), url.length);
            if (fileV6CompatibleString.indexOf("true") != -1)
                isV6CompatibleFileName = "true";
            else
                isV6CompatibleFileName = "false";

            if (url.indexOf("&downloadedModelFile") != -1) {
                //SPR 691850 : Previously to get downloadedModelFile from downloadModelFileQueryString, last index of '=' was accessed.
                //In downloadModelFileQueryString it is observed that = is present in name of model file too. hence modified the code
                //to access downloadedModelFile by the length of &downloadedModelFile=
                var downloadModelFileQueryString = url.substring(url.indexOf("&downloadedModelFile"), url.length);
                //here 21 stands for total length of the string "&downloadedModelFile="
                downloadedModelFile = downloadModelFileQueryString.substring(21, downloadModelFileQueryString.length);
            }
            else
                downloadedModelFile = "";

            if (url.indexOf("&isFileV6Compatible") != -1) {
                url = url.substring(0, url.indexOf("&isFileV6Compatible"));
                v6DownloadURL = url;
            }
            else
                v6DownloadURL = "";
            isBusy = false;
            redirectCheck(url);
            var sFormatName = document.getElementById("ddFormats").value;
            var wait = document.getElementById("imgDownloadWait");
            //wait.style.display = "none";
            wait.src =  basePath() + "images/icon_download.gif";
            var lbl = document.getElementById("lblDownloadStatus");
            lbl.innerHTML = str2;
            var link = document.getElementById("hlModelLink");
            link.href = url.replace(/#/g, '%23');
            //Force the download only for the following formats
            //so they don't open in the browser
            if(sFormatName=="edrw" || sFormatName=="pdf")// || sFormatName=="sldprt")
            {
                link.href = basePath() + "FileDownloadHandler.aspx?file=" + URLencode(url);
            }
            link.style.display = "inline";
            //update the part number
            
            if(xmlHttp.statusText != null && typeof(xmlHttp.statusText) != "undefined")
            {
                if(xmlHttp.statusText!="" && xmlHttp.statusText.toLowerCase()!="ok")
                {
                    if (typeof (setPartNumber) != "undefined") {
                        setPartNumber(xmlHttp.statusText);
                    }
                    else {
                        top.setPartNumber(xmlHttp.statusText);
                    }
                }
            }
            //decide to hide or show the NextGen button, on the basis of download format
            try {
                hideShowNextGenButton();
            }
            catch (err) {
            }
        }
        else
        {
            xmlHttpDispose();
            var isIE;
            navigator.userAgent.indexOf("MSIE")>=0? isIE = true: isIE = false;
            
            var wait = document.getElementById("imgDownloadWait");
            //wait.style.display = "none";
            wait.src =  basePath() + "images/icon_alert.gif";
            var lbl = document.getElementById("lblDownloadStatus");
            //force any long nonbreaking string
            var re = new RegExp(/\\/g);
            var sReplace = "&shy;\\";
            if(!isIE) sReplace = "<wbr/>\\";
            lbl.innerHTML = url.replace(re, sReplace);
        }
        if(typeof(dlg2)!='undefined') dlg2.show();
    }
}

function downloadStateSWK()
{
    if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
    {
        if (xmlHttp.status != 200) {
            handleUnspecError();
            return;
        }
        var url = xmlHttp.responseText;
        isBusy = false;
        redirectCheck(url);
        if(!urlHasError(url))
        {
            var sFormatName = document.getElementById("ddFormats").value;
            var link = document.getElementById("ModelDownloadLink");
            if(link!=null) link.value = url;
            var elPNames = document.getElementById("ParamterNames");
            if(elPNames!=null) elPNames.value = pNames;
            var elPValues = document.getElementById("ParamterValues");
            if(elPValues!=null) elPValues.value = pVals;
            var elConfig = document.getElementById("NamedConfiguration");
            if(elConfig!=null)elConfig.value = sConfig;
        }
        else
        {
            var elError = document.getElementById("DownloadError");
            if(elError!=null) elError.value = url;
        }
    }
}

function stateChanged() {
    if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
        if (xmlHttp.status != 200) {
            handleUnspecError();
            return;
        }
        isBusy = false;
        if (!urlHasError(xmlHttp.responseText)) {
            //alert(xmlHttp.responseText);
            document.getElementById("imgResolvingRules").style.display = 'none';
            document.getElementById("configHr").style.display = '';
            var txt = xmlHttp.responseText;
            redirectCheck(txt);
            var div = document.getElementById("RuleParams");
            div.innerHTML = txt;
            setUpdateParams(false);
            bForceReload = true;
        }
        else {
            var el = document.getElementById("imgProcessingParams");
            if (el != null) {
                el.src = basePath() + "images/icon_alert_small.gif";
                el.title = xmlHttp.responseText;
            }
        }
    }
}

function configState() 
{ 
    if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
    {
        if (xmlHttp.status != 200) {
            handleUnspecError();
            return;
        }
        isBusy = false;
        if(!urlHasError(xmlHttp.responseText))
        {
            redirectCheck();
            var txt = xmlHttp.responseText;
            var div = document.getElementById("pnlConfigParams");
            div.innerHTML = txt;
        }
    }
}

function getCurrentConfig()
{
    sConfig = "";
    
    //if radio button was rendered check to see if user has selected it
    var radio = document.getElementById("rdioConfigs_0");
    if(radio == null || radio.checked)
    {
        var el = document.getElementById("presetConfig");
        if(el)
        {
            sConfig = encodeURIComponent(el.value).replace(/\?/, "%3F");
        }
    }
}

function sendRatingReminderEmail(sCatId, sPartId)
{
    var cb = document.getElementById("cbRatingReminder");
    if(cb!=null && cb!='undefined')
    {
        if(cb.checked==true)
        {
            var url= basePath() + "RatingReminderEmail.aspx?catid=" + sCatId + "&partid=" + sPartId;
            xmlHttp=GetXmlHttpObject(voidState);
            xmlHttp.open("GET", url , true);
            isBusy = true;
            xmlHttp.send(null);
        }
    }
    else
    {
        return;
    }
}

function fnSendReminderMail(sCatId,sPartId,sCType)
{   
    var cb = document.getElementById("cbRatingReminder");
    if(cb!=null && cb!='undefined')
    {
        if(cb.checked==true)
        {            
            var url= basePath() + "ContentRatingReminderEmail.aspx?catid=" + sCatId + "&partid=" + sPartId + "&etype=" + sCType ;               
            xmlHttp=GetXmlHttpObject(voidState);
            xmlHttp.open("GET", url , true);
            isBusy = true;
            xmlHttp.send(null);
           
        }
    }
    else
    {
        return;
    }    
}

function voidState()
{
    isBusy = false;
    return null;
}

function getCurrentParams()
{
    bValidParams = true;
    pVals = "";
    pNames = "";
    var sep = "";
    //if radio button was rendered check to see if user has selected it
    var radio = document.getElementById("rdioConfigs_1");
    if(radio == null || radio.checked)
    {
        var div = document.getElementById("RuleParams");
        if(div!=null)
        {
            var ddl = div.getElementsByTagName("select");
            for(i=0;i<ddl.length;i++)
            {
                if(i<(ddl.length-1)) sep = "||";
                //URlEncode found in base.js
                if (ddl[i].value != "") {
                    pVals += URLencode(ddl[i].value) + sep;
                    var id = ddl[i].id;
                    pNames += URLencode(id.replace("ddParam_", "")) + sep;
                }
                else {
                    //the empty string is only invalid if it's the value of
                    //the first item in the list that is selected
                    if (ddl[i].options[0].value == '') {
                        bValidParams = false;
                        //add a UI flag
                        var ind = document.createElement("span");
                        ind.id = "rfv";
                        ind.className = "red";
                        ind.innerHTML = "!";
                        ind.style.fontWeight = "bold";
                        ind.style.fontSize = "150%";
                        //test if an indicator is already present
                        if (ddl[i].parentNode.lastChild != 'undefined' ||
                        ddl[i].parentNode.lastChild != null) {
                            if (ddl[i].parentNode.lastChild.innerHTML != "!")
                                ddl[i].parentNode.appendChild(ind);
                        };
                    }
                }
                sep = "";
            }
            
            var txt = div.getElementsByTagName("input");
            for(i=0;i<txt.length;i++)
            {
                //if(i<(txt.length-1)) 
                sep = "||";
                var id = txt[i].id;
                //if(id)
                //{
                    if(id.indexOf('ddParam_') > -1)
                    {
                        if(txt[i].value!="")
                        {
                           pVals += sep + URLencode(txt[i].value);
                           pNames += sep + URLencode(id.replace("ddParam_", ""));
                        }
                        sep = "";
                    }
                    else if (id.indexOf('cbParam_') > -1) {
                        if (document.getElementById(id).checked) {
                            pVals += sep + "0";
                        }
                        else {
                            pVals += sep + "1";
                        }
                        pNames += sep + URLencode(id.replace("cbParam_", ""));
                    }
                //}
            }
            pVals = pVals.replace("#", "%23");
            pNames = pNames.replace("#", "%23");
        }
    }
    
    get2DView();
}

function get2DView()
{
    //set the 2D view
    if(sFormat.toLowerCase()=="2d")
    {
        var el = document.getElementById("dd2DViewSelect");
        if(el!=null) s2DView = el.value;
    }
}

function redirectCheck(s)
{
    if(s!=null)
    {
        if(s.toLowerCase()=="redirect")
        {
            //window.location.href = basePath() + "Default.aspx";SPR 468884            
            var url = document.location.href;
            var idx = url.indexOf('#');
            alert('Something went wrong. Please try again after the page is reloaded!');
            if (idx > -1) {
                document.location.href = url.substring(0, idx);
            }
            else {
                document.location.reload(); 
            }
        }
    }
}

function setGeometryTab()
{
    if(typeof(ModelTabs)!='undefined')
    {
        var tab = ModelTabs.getActiveTab();
        if(tab.id=="previewImg" || tab.id=="partInfo" || tab.id=="supplierInfo")
        {
            //try to activate the next tab, setting it's src to about:blank
            var nTab = ModelTabs.items.preview3D;
            if (nTab != null) 
            {
                if (document.getElementById("PreviewFrame3D") != null)
                    document.getElementById("PreviewFrame3D").src = "about:blank";
                if (document.getElementById("PreviewFrame3DED") != null)
                    document.getElementById("PreviewFrame3DED").src = "about:blank";
                ModelTabs.activate(nTab.id);
            }
            else
            {
                nTab = ModelTabs.items.preview2D;;
                if(nTab!=null) 
                {
                    document.getElementById("PreviewFrame2D").src = "about:blank";
                    ModelTabs.activate(nTab.id);
                }
            }
        }
    }
}

function setPartNumber(arg)
{
    var el = document.getElementById("ModelPartNumber");
    var el1 = document.getElementById("lblPartNumberID");
    if(el!=null && el1!=null)
    {
        // if arg is present, supplier part number
        // if not, use sConfig if available
        // if sConfig n/a, use part name itself
        if (arg != null && arg != "") 
        {   
            el1.innerHTML = sSupplierPartNumber;
            el.innerHTML = arg;
        }
        else 
        {
            if (el1!=null) 
            {
                el1.innerHTML = sSupplierModelDescription; 
                if (sConfig != null && sConfig != "") 
                {   
                    el.innerHTML = sConfig;
                }
                else 
                {
                    el.innerHTML = sPartname;
                }
            }
        }
        el.innerHTML = unescape(el.innerHTML);
    }
}

function getPartNumber()
{
    var s = "";
    var el = document.getElementById("ModelPartNumber");
    if(el!=null)
    {
        s = el.innerHTML;
    }
    return s;
}

function setUpdateParams(vis) 
{
    var btnPreview = document.getElementById("btnUpdatePreview");
    if(btnPreview!=null) btnPreview.disabled = vis;
    var sDisplay = "inline";
    if(!vis) sDisplay = "none";
    document.getElementById("resolveParams").style.display = sDisplay;
}

function toggleConfigOptions(arg)
{
    var el1 = document.getElementById("ConfigParams");
    var el2 = document.getElementById("RuleParams");
    switch(arg)
    {
        case 0:
            el1.style.display = '';
            el2.style.display = 'none';
            break;
        case 1:
            el1.style.display = 'none';
            el2.style.display = '';
            break;
    }
}

//sRange, sRangeLess, sRangeStep defined in Download-Model.aspx
function resolveRange(el)
{
    var isValid = true;
    var min = el.getAttribute('min');
    var max = el.getAttribute('max');
    var step = el.getAttribute('step');
    if (min != "none" && max != "none" && !isNaN(min))
    {								
	    min = verifyDigits(min); 
	    max = verifyDigits(max); 
	    step = verifyDigits(step);
	    var inputValue = verifyDigits(el.value);
    		
	    var temp = el.value;											
	    if (temp == "")
	    {
		    inputValue = min;	
	    }
	    else
	    {
		    if (isNaN(inputValue))									
			    inputValue = el.value;							
	    }						
	    el.value = inputValue;
    	
	    // to handle the indefinate max case						
	    if (!isNaN(max) && (inputValue > max || inputValue < min))
	    {
		    alert(sRangeOut);
		    el.focus();
		    isValid = false;
	    }
	    else if (isNaN(max) && inputValue < min)
	    {
		    alert(sRangeLess);
		    el.focus();
		    isValid = false;
	    } 
	    else
	    {
		    /*if (isNaN(step))  //new template may have the step in the fourth position
		    {
			    step = verifyDigits(document.outerForm.elements[j+3].value);
		    }*/
		    if (step > 0 && !isNaN(step))
		    {
			    result = (inputValue-min)%step;
			    if ((result < 0.00000000001) || ((step - result) < 0.00000000001))
				    isValid = true;
			    else								
			    {									
				    alert(sRangeStep);
				    el.focus();
				    isValid = false;
			    }
		    }
	    }
                
	    if(isValid) updateRule(el);
    }
}

function verifyDigits(str)
{
	if ((str != "0") && (str.indexOf(".") != -1))
	{
		while (str.substr(str.length-1, str.length) == "0")
		{
			str = str.substr(0, str.length-1);
		}
	}
	if (str.substr(0, 1) == ".")
		str = "0" + str;
	var multiple = "1";
	var numOfDigits = 0;
	var posOfDot = 0;			
	posOfDot = str.indexOf(".");
	if (posOfDot != -1)
		numOfDigits = str.substr(posOfDot+1, str.length).length;		
	var floatNumber = parseFloat(str);
	var temp = floatNumber + "";
	var posOfDotForFloat = temp.indexOf(".");
	var numOfDigitsForFloat = 0;
	if (posOfDotForFloat != -1)
		numOfDigitsForFloat = temp.substr(posOfDotForFloat+1, temp.length).length;
	
	if (posOfDot == -1 && posOfDotForFloat == -1)
	{
		return floatNumber;
	}	
	else
	{
		//for 2=1.99999997
		if (posOfDotForFloat != posOfDot)
		{				
			return Math.round(floatNumber);					
		}
		else
		{
			if ((numOfDigitsForFloat == numOfDigits) || (temp==str))
				return floatNumber;
			else
			{
				for (var i=0; i<numOfDigits; i++)
				{
					multiple = multiple + "0";
				}
				multiple = parseInt(multiple);
				return Math.round(floatNumber * multiple);
			}
		}	
	}	
}

function displayParamInfo(img, pType, sArg)
{
    if(typeof(pType)!='undefined')
    {
        if(typeof(sArg)!='undefined')
        {
            if(sArg!="")
            {
                switch(pType)
                {
                    case 1:
                        //a hyperlink, open a new window
                        var win = window.open(sArg,'paramInfo', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=1,scrollbars=1');
                        break;
                    case 0:
                        //text, display in a in-page window
                        var el = document.getElementById("ParamHelp_Content");
                        if(el!=null)
                        {
                            el.innerHTML = sArg;
                            if(typeof(dlgParamHelp)!='undefined')
                            {
                                var pos = YAHOO.util.Dom.getXY(img);
				                dlgParamHelp.cfg.setProperty("xy", pos);
                                dlgParamHelp.show();
                            }
                        }
                        break;
                }
            }
        }
    }
}
function urlHasError(sUrl)
{
    var r = false;
    sUrl = sUrl.toLowerCase();
    if(sUrl.indexOf("error") == 0 || sUrl.indexOf("file%20not%20found") >= 0 ||
        sUrl.indexOf("gateway%20timeout") >= 0)
    {
        r = true;
    }
    return r;
}

function handleUnspecError() {
    isBusy = false;
    var msg = "An unspecified error occurred. Please try again";
    setPageErrorMessage(msg);
    toggleSubForm('requestAlert', true);
}

function downloadForEdrawingsApp() {
    var hiddenCtrl = document.getElementById("hiddenCtrlEdrawingsURL");
    var edrawingsURL = hiddenCtrl.value;
    var modelurl = basePath() + "FileDownloadHandler.aspx?file=" + edrawingsURL;
    var windowObjectReference =
         window.open(modelurl, "EdrawingsDownload", "menubar=yes,location=no,resizable=yes,scrollbars=yes,status=yes");
}
