﻿// JScript File
var xmlHttp;
var isBusy = false;
var cookieTabsName = "3DCCTab";
var toolTipSrcEl;
var isPageUnloaded = false; //for detect onbeforeunload event

eBrowser = {
    CHROME: 'CHROME',
    FF: 'FF',
    IE: 'IE',
    OTHER: 'OTHER'
}
function GetBrowser() {
    var ua = navigator.userAgent.toUpperCase();

    if (ua.indexOf("CHROME") > -1) return eBrowser.CHROME;
    if (ua.indexOf("FIREFOX") > -1) return eBrowser.FF;
    if (ua.indexOf("MSIE") > -1) return eBrowser.IE;
    return eBrowser.OTHER;
}

/*Detect of current browser like server's method*/
eBrowserType={
    Chrome_v1:'Chrome_v1',
    Chrome_v2:'Chrome_v2',
	FF_v2:'FF_v2',
	FF_v3:'FF_v3',
	FF_v4:'FF_v4',
	FF_v5:'FF_v5',
	IE_v6:'IE_v6',
	IE_v7:'IE_v7',
	IE_v8:'IE_v8',
	IE_v9: 'IE_v9',
	IE_v10: 'IE_v10',
	Other:'Other'
}
function GetBrowserType()
{
	var ua=navigator.userAgent.toUpperCase();

	if (ua.indexOf("CHROME/1.") > -1) return eBrowserType.Chrome_v1;
	if (ua.indexOf("CHROME/2.") > -1) return eBrowserType.Chrome_v2;
    if (ua.indexOf("FIREFOX/2.")>-1) return eBrowserType.FF_v2;
    if (ua.indexOf("FIREFOX/3.")>-1) return eBrowserType.FF_v3;
    if (ua.indexOf("FIREFOX/4.") > -1) return eBrowserType.FF_v4;
    if (ua.indexOf("FIREFOX/5.") > -1) return eBrowserType.FF_v5;
    if (ua.indexOf("MSIE 6.")>-1) return eBrowserType.IE_v6;
    if (ua.indexOf("MSIE 7.")>-1) return eBrowserType.IE_v7;
    if (ua.indexOf("MSIE 8.")>-1) return eBrowserType.IE_v8;
    if (ua.indexOf("MSIE 9.") > -1) return eBrowserType.IE_v9;
    if (ua.indexOf("MSIE 10.") > -1) return eBrowserType.IE_v10;
    return eBrowserType.Other;
}
eBrowserVer = {
    FF_v3_6: 'FIREFOX/3.6',
    Other: 'Other'
}
function IsBrowserVer(browserVer) {
    var nav = navigator.userAgent.toUpperCase();
    if (nav.indexOf(browserVer.toUpperCase()) > -1) return true;
    return false;
}

//Detect the bit version of an operating system
eBitOprtSysType =
{
    WOW64: 'WOW64',
    x64: 'x64',
    x86: 'x86',
    Unknown: 'Unknown'
}

function GetBitOprtSysType() {
    var nav = navigator.userAgent.toUpperCase();
    if (nav.indexOf('WOW64') > -1) return eBitOprtSysType.WOW64;
    else {
        if (nav.indexOf('WIN64') > -1) return eBitOprtSysType.x64;
        else
            return eBitOprtSysType.x86;
    }
}

/*Advanced resizing images*/
function resizeImageKeepAspect(prevImgObj, fixedWidth, fixedHeight)
{
    var actImg = new Image();
    actImg.src = prevImgObj.src;
    var origWidth = actImg.width;
    var origHeight = actImg.height;
    var imgWidth = origWidth; //set original width by default
    var imgHeight = origHeight; //set original height by default
    var origAspect = origWidth / origHeight;
    var fixedAspect = fixedWidth / fixedHeight;
    if (origWidth > fixedWidth || origHeight > fixedHeight)
    {
        if (fixedAspect < origAspect)
        {
            imgWidth = fixedWidth;
            imgHeight = Math.round(origHeight / origWidth * fixedWidth);
        }
        else if (fixedAspect > origAspect)
        {
            imgHeight = fixedHeight;
            imgWidth = Math.round(origWidth / origHeight * fixedHeight);
        }
        else
        {
            imgWidth = fixedWidth;
            imgHeight = fixedHeight;
        }
    }
    //alert(imgWidth)
    // assign the calculated width and height to the preview image field WITH OVERRIDING css style/class and height/width attributes.
    if (imgWidth > 0 && imgHeight >0) {
        prevImgObj.width = imgWidth;
        prevImgObj.height = imgHeight;
        prevImgObj.style.width = imgWidth + "px";
        prevImgObj.style.height = imgHeight + "px";
    }
    prevImgObj.style.display = "inline";    
    prevImgObj.style.visibility = "visible";
}

function setImageSizes_TargetId_Class_MaxSizeWH(targetId, classN, maxW, maxH)
{
    var el;
    if (!targetId)
        el = document.body;
    else
        el = document.getElementById(targetId);

    if (el != null)
    {
        var arImg = el.getElementsByTagName("img");
        for (i = 0; i < arImg.length; i++)
        {
            if (arImg[i].className == classN)
            {
                resizeImageKeepAspect(arImg[i], maxW, maxH);
            }
        }
    }
}

/**/
function menuOver(el)
{
	if(el.className != "menuItemSelected") el.className = "menuItemOver";
}
function menuOut(el)
{
	if(el.className != "menuItemSelected") el.className = "menuItem";
}

function displayComment(id)
{
    var el = document.getElementById(id);
    if(el!=null)
    {
        document.getElementById("commentOverflow").innerHTML = el.value;
        document.getElementById("commentOverflowDlg").style.display = "";
        dlgOverflow.show();
    }
}

function checkUploadFormat(el)
{
    var div = document.getElementById("zipFile");
    if(el.value.toLowerCase()=="sldasm")
    {
        div.style.display = "block";
    }
    else
    {
        if(div.style.display == "block")
        {
            div.style.display = "none";
        }
    }
}

function cancelProfileEdit()
{
    history.go(-1);
}

function forgotPassword()
{
    
    var login2, login1;
    login1 = document.getElementById("login1");
    login2 = document.getElementById("login2");
    if (login1 && login2)
    {
        login1.style.display = "none";
        login2.style.display = "inline";
        var emptyFields = document.getElementById("emptyFields");
        if (emptyFields !=null)
             emptyFields.style.display ="none"
        var mismatch;
        mismatch = document.getElementById("mismatch");
        if (mismatch != null)
            mismatch.style.display= "none";
    }
}

function cancelPassword()
{
    var login2, login1;
    login1 = document.getElementById("login1");
    login2 = document.getElementById("login2");
    if (login1 && login2)
    {
        login1.style.display = "inline";
        login2.style.display = "none";
        var emptyFields = document.getElementById("emptyFields");
        if (emptyFields !=null)
             emptyFields.style.display ="none"
        var mismatch = document.getElementById("mismatch");
        if (mismatch != null)
            mismatch.style.display= "none";
             
    }
}
function selectCompanyName(name)
{
    var el = document.getElementById("txtSupplierName");
    if(el!=null) el.value = name;
    var o = document.getElementById("supplierMatches");
    if(o!=null) o.style.display = "none";
}
//End Request supplier page functions

function displaySection(show, hide)
{
    document.getElementById(hide).style.display = 'none';
    document.getElementById(show).style.display = '';
}

function changeElInnerHtml(id, arg1, arg2)
{
    var el = document.getElementById(id);
    el.innerHTML == arg1? el.innerHTML = arg2 : el.innerHTML = arg1;
}

function toggleElementVis(id)
{
    var el = document.getElementById(id);
    if(el!=null) el.style.display == 'none'? el.style.display = '' : el.style.display = 'none';
}

function toggleSection(evt, sectionBody, viewText, hideText)
{
    var el = getSourceElement(evt);
    el.innerHTML == viewText? el.innerHTML = hideText : el.innerHTML = viewText;
    toggleElementVis(sectionBody);
}

//moved from My3DCC. Uses Yahoo Animation
function toggleSubForm(id, bShow)
{
    if(bShow==null) bShow = false;
    var from = 0;
    var to = 1;
    var el = document.getElementById(id);
    el.style.visibility='visible';
    if(el.style.display == 'block')
    {
         from = 0.999999;
         to = 0;
    }
    else
    {
        from = 0;
        to = 1;
    }
    
    if(bShow) { from = 0; to = 1;}
    var anim = new YAHOO.util.Anim(id, { opacity: { to: to } }, from, YAHOO.util.Easing.easeOut);
    anim.animate();
    if(bShow)
    {
        YAHOO.util.Dom.setStyle([id], 'opacity', 0.01);
        el.style.display = 'block';
    }
    else
    {
        el.style.display == 'block'? el.style.display = 'none' : el.style.display = 'block';
    }
}
function addToFavorites(pageName) 
{
	if(window.sidebar)// Mozilla Firefox Bookmark
	{
	    window.sidebar.addPanel("3D ContentCentral - " + pageName, location.href,"");
	}
	else if (window.external) 
	{
	    var urlAddress = "";
	    if(typeof(window.external.AddFavorite) != "undefined")
		    window.external.AddFavorite(location.href, "3D ContentCentral - " + pageName);
    } 
    else 
    {
		alert("Sorry! Your browser doesn't support this function."); 
	} 
}

function msgHide(type, args, obj)
{
    var frame = "PreviewFrame2D";
    msgFrameHide(type, args, obj,frame);
}
function msgFrameHide(type, args, obj, frameId) 
{
    var frame = document.getElementById(frameId);
    if (frame != null) {
        if (frame.src != "about:blank") {
            var div = null;
            try 
            {
                if (frameId == "PreviewFrame2D")
                    div = top.PreviewFrame2D.document.getElementById("dialogSpacer");
                if (frameId == "PreviewFrame3DED")
                    div = top.PreviewFrame3DED.document.getElementById("dialogSpacer");
            }
            catch (Error) {
                div = (frame.contentDocument) ? frame.contentDocument.getElementById("dialogSpacer") : frame.contentWindow.document.getElementById("dialogSpacer");
            }
            if (div != null) {
                div.style.display = "none";
            }
        }
    }
}
function msgShow(type, args, obj)
{
    var frame = "PreviewFrame2D";
    msgFrameShow(type, args, obj, frame);
}
function msgFrameShow(type, args, obj, frameId) 
{
    var frame = document.getElementById(frameId);
    if (frame != null) {
        if (frame.src != "about:blank") {
            var div = null;
            try 
            {
                if (frameId == "PreviewFrame2D")
                    div = top.PreviewFrame2D.document.getElementById("dialogSpacer");
                if (frameId == "PreviewFrame3DED")
                    div = top.PreviewFrame3DED.document.getElementById("dialogSpacer");
            }
            catch (Error) {
                div = (frame.contentDocument) ? frame.contentDocument.getElementById("dialogSpacer") : frame.contentWindow.document.getElementById("dialogSpacer");
            }            
            if (div != null) {
                div.style.display = "block";
            }
        }
    }
}

var mailThisUrl_subj = "I thought this might interest you..."
var mailThisUrl_text = "Here is an interesting model from 3D ContentCentral: "
function mailThisUrl(modelName)
{
  var content = "mailto:"
             + ""
             + "?subject=" + mailThisUrl_subj
             + "&body="   + mailThisUrl_text + "%0d%0a"
             + modelName 
             + " (" + escape(location.href) + ")";
  window.location = content
}

var tellAFriend_subj = "I thought this site might interest you..."
var tellAFriend_text = "Here is an interesting site named 3D ContentCentral: "
function tellAFriend()
{
  var content = "mailto:"
             + ""
             + "?subject=" + tellAFriend_subj
             + "&body="   + tellAFriend_text + "%0d%0a"
             + " (" + escape(location.href) + ")";
  window.location = content
} 

function viewAllBMark(el)
{
    var bmark = el.value;
    location = "#" + bmark;
}

function populateRegions(el)
{
    //execute xmlhttp request
    var url="GetCountryRegions.aspx?arg=" + el.value;
    
    xmlHttp=GetXmlHttpObject(getRegionsState);
    xmlHttp.open("GET", url , true);
    xmlHttp.send(null);
}

function getRegionsState()
{
    if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
    {
        var results = xmlHttp.responseText;
        if(results!="")
        {
            //parse the results
            var arOptions = results.split("||");
            var el = document.getElementById("ddlStatesProvinces");
            //clear the dropdownlist
            el.options.length = 0;
            var i;
            for(i=0;i<arOptions.length;i++)
            {                
                var arOpt = arOptions[i].split(":");
                if(arOpt.length>1)
                {
                    el.options[el.options.length] = new Option(arOpt[1], arOpt[2]);
                }
            }
        }
    }
}

function GetXmlHttpObject(handler)
{ 
    var objXmlHttp=null;

    if (navigator.userAgent.indexOf("Chrome") >= 0) {
        objXmlHttp = new XMLHttpRequest();
        objXmlHttp.onload = handler;
        objXmlHttp.onerror = handler;
        return objXmlHttp;
    }
    if (navigator.userAgent.indexOf("Opera")>=0)
    {
        alert("This example doesn't work in Opera");
        return;
    }
    if (navigator.userAgent.indexOf("MSIE")>=0)
    { 
        var strName="Msxml2.XMLHTTP";
        if (navigator.appVersion.indexOf("MSIE 5.5")>=0)
        {
            strName="Microsoft.XMLHTTP";
        } 
        try
        { 
            objXmlHttp=new ActiveXObject(strName);
            objXmlHttp.onreadystatechange=handler; 
            return objXmlHttp;
        } 
        catch(e)
        { 
            alert("Error. Scripting for ActiveX might be disabled"); 
            return; 
        } 
    } 
    if (navigator.userAgent.indexOf("Mozilla")>=0)
    {
        objXmlHttp=new XMLHttpRequest();
        objXmlHttp.onload=handler;
        objXmlHttp.onerror=handler; 
        return objXmlHttp;
    }
 
}

//need this function since escape() ignore certain characters
function URLencode(sStr) {
    return encodeURI(sStr).
           replace(/&/g, '%26').
             replace(/\+/g, '%2B').
                replace(/\"/g,'%22').
                   replace(/\'/g, '%27').
                     replace(/\//g,'%2F').
                      replace(/#/g, '%23');
}

function createFriendlyUrlName(sStr)
{
    //like server method with same name
    return sStr.replace(/\./g, "").
                replace(/ - /g, "-").
                replace(/ /g, "-").
                replace(/\//g, " ").
                replace(/\'/g, "-").
                replace(/\+/g, "-").
                replace(/\&/g, "AND");
}

//cookie functions
function checkCookie(sName)
{
    if(sName==null) sName = cookieTabsName;
    var cTabs = readCookie(sName);
    if(cTabs!=null)
    {
        return cTabs;
    }
}

function createCookie(name,value,days)
{
	if (days)
	{
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name)
{
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++)
	{
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name)
{
	createCookie(name,"",-1);
}

//Search functions
document.onkeydown = focusKeyword;
var gT = "";
var gI = 0;
document.onmousedown = function (evt) {
    var sourceElement = getSourceElement(evt);
    if (sourceElement)
    {
        var o = document.getElementById("searchResults");
        if(o!=null)
        {
	        if(o.style.display = "block")
	        {
	            if((sourceElement.id!="searchResults")&&(sourceElement.className!="searchPopupLink"))
                {
                    keepSearchResults = false;
                    hideSearchResults();
                }
                else if(sourceElement.id=="searchResults")
                {
                    keepSearchResults = true;
                }
	        }
        }
    }
    if (sourceElement)
    {
        var o = document.getElementById("searchResults"+gT+gI);
        if(o!=null)
        {       
	        if(o.style.display == "block")
	        {         
	            if( (sourceElement.id.indexOf("searchResultsTitle") == -1) && (sourceElement.id.indexOf("searchLinksTitle") == -1)  
	            && (sourceElement.id.indexOf("searchResultsVendorName") == -1) && (sourceElement.id.indexOf("searchLinksVendorName") == -1)
	            && (sourceElement.id.indexOf("searchResultsVendorPartNo") == -1) && (sourceElement.id.indexOf("searchLinksVendorPartNo") == -1)
	            && (sourceElement.id.indexOf("searchResultsDescription") == -1) && (sourceElement.id.indexOf("searchLinksDescription") == -1)
	            )
              {
										setTimeout("hid('"+gT+"',"+gI+")",200);	
              }
              else if(sourceElement.id.indexOf("searchResultsTitle") == 0
              || sourceElement.id.indexOf("searchResultsVendorName") == 0
              || sourceElement.id.indexOf("searchResultsVendorPartNo") == 0
              || sourceElement.id.indexOf("searchResultsDescription") == 0
              )
              {
                    keepSearchResultsVendor = true;
              }
	        }
        }
    }   
}
function hid(tt,ii)
{
    keepSearchResultsVendor = false;
    hideSearchResultsVendor(tt,ii);
}


function getSourceElement(evt)
{
   if (window.event)
     return window.event.srcElement;
   else if (evt && evt.target) {
     if (typeof evt.target.nodeType == 'undefined')
       return evt.target;
     else if (evt.target.nodeType == 3)
       return evt.target.parentNode;
     else
       return evt.target;
   }
}

function searchKeyUp(e)
{
    var el = document.getElementById("txtSearchInputNav");
    //test for invalid character sequence '<.'
    if(!invalidCharSeq(el.value))
    {
        if((el.value!="")&&(!isEntKey(e)))
	    {
            //execute xmlhttp request
            var url="GetSearchSuggestions.aspx?arg=" + el.value + "&type=Keywords";
            xmlHttp=GetXmlHttpObject(searchState);
            xmlHttp.open("GET", url , true);
            xmlHttp.send(null);
        }
        else
        {
            hideSearchResults();
        }
    }
    else
    {
        var msg = "Invalid character sequence detected.\nYou may not enter '<' followed by any character.";
        if(sInvalidSearchCharMsg!=null) msg = sInvalidSearchCharMsg;
        alert(msg);
        el.value = "";
    }
}

function basePath()
{
    var index = 1;
    var b = isSiteInVirtualDir();
    if(!b)
        index = 0;
    //making some assumptions here
    var pathStr = document.location.protocol + "//" + document.location.hostname + "/";
    if(document.location.pathname.length > 0)
        var p = document.location.pathname.indexOf("/",index);
        if (p > 0)
            pathStr += document.location.pathname.substr(index,p);
    return pathStr;    
}
function isSiteInVirtualDir()
{
    var r = true;
    var cc = document.location.pathname.toLowerCase().indexOf("/3dcontentcentral");
    var sw = document.location.pathname.toLowerCase().indexOf("/swclient");
    if (cc < 0 && sw < 0) 
    {
        r = false;
    }
    
    return r;
}
function displaySearchMin(el)
{
    if(sSearchMinimum!=null)
    {
        if(sSearchMinimum!="") el.value = sSearchMinimum;
        return false;
    }
}

function executeSearch(inResults)
{
    var txtBox = document.getElementById("txtSearchInputNav");
    var sSearch = "";
    
    if(txtBox!=null)
    {
        var url;                      
        if(txtBox.value.length>0)
	    {
            //replace initial spaces if any
            var sTerm = txtBox.value.replace(/^\s*/, '');
            sTerm = URLencode(sTerm);
            if(sTerm!="")
            {
                if(inResults != null)
                {
                    url = document.location.href.replace("#", "");
                    //always start on first page for refinements
                    url = removeQueryStringParam(url, "page");                                       
                    url += "&in=" + sTerm;
                }                
                else
                {
                    //check for Visual Search page
                    if (location.href.toLowerCase().indexOf("gallery-view") > -1)
                    {
                        url = basePath() + "Search-Gallery-View.aspx";
                        url += "?arg=" + sTerm;
                    }
                    else
                    {
                        url = basePath() + "Search.aspx";
                        url += "?arg=" + sTerm;
                    }
                }                
            }
            else
            {
                displaySearchMin(txtBox);
                return false;
            }
        }
        else
        {
            displaySearchMin(txtBox);
            return false;
        }
        document.location.href = url.replace("https://", "http://");
        return true;
    }
}

function isValidSearch()
{
    var txtBox = document.getElementById("txtSearchInputNav");
    if(txtBox!=null)
    {
        if(txtBox.value.length>0)
	    {
            return true;
        }
        else
        {
            displaySearchMin(txtBox);
            return false;
        }
    }
}

function searchOnEnterKey(e, searchType)
{
    if (isEntKey(e)) executeSearchOld(searchType);
}
function isEntKey(e)
{
    var b = false;
    if(navigator.appName.toLowerCase() == "netscape") 
    {
        //enter key
        if(e.which == 13)
            b = true;
    }
    if(navigator.appName.toLowerCase() == "microsoft internet explorer")
    {
        if(event.keyCode == 13)
            b = true;
    }
    return b;
}

function invalidCharSeq(arg)
{
    var b = false;
    //test for 3 initial spaces
    var re = new RegExp(/.*?\<./);
    if(typeof(arg)!='undefined')
    {
        if(arg!="")
        {
            var m = arg.match(re);
            if(m)
            {
                //invalid char seq found
                return true;
            }
            else
            {
                //no match
                return false;
            }
        }
    }
    return b;
}

function cancelPostBack(e)
{
	if (!e) var e = window.event;
	e.cancelBubble = true;
	e.returnValue = false;
    e.cancel = true;
	if (e.stopPropagation) e.stopPropagation();
	return false;
}

function cumulativeOffset(element) {
  var valueT = 0, valueL = 0;
  do {
    valueT += element.offsetTop  || 0;
    valueL += element.offsetLeft || 0;
    element = element.offsetParent;
  } while (element);
  return [valueL, valueT];
}

function hideSearchResults()
{
	if (keepSearchResults)
	{
	    keepSearchResults = false;
	    return;
	}	
	var o = document.getElementById("searchResults");
	if(o!=null)
	{
		o.style.display = "none";
	}
}

function searchState()
{
    if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
    {
        var results = xmlHttp.responseText;
        if(results!="")
        {
            var el = document.getElementById("txtSearchInputNav");
            if(el!=null)
            {
                //var tgtPos = cumulativeOffset(el);
                var o = document.getElementById("searchResults");
                if(o!=null)
                {
                    if(o.style.display != "block")
                    {
                        o.style.display = "block";
                        //o.style.left = tgtPos[0] +'px';
                        //o.style.top =  (parseInt(tgtPos[1]) + 18) +'px';
                    }
                }
                var searchLinks = document.getElementById("searchLinks");
                if(searchLinks !=null) searchLinks.innerHTML = results;
                curKeywordSel = -1;
            }
        }
        else
        {
             hideSearchResults();
        }
    }
}

function focusKeyword(keypress) 
{
    var keycode;
    if(navigator.appName == "Netscape") 
        keycode = keypress.which;
    else if(navigator.appName == "Microsoft Internet Explorer")
        keycode = event.keyCode;
    else
        return; //which is safer to default to?
            
    switch(keycode)
    {
        case 9: //tab
            var el = getSelectedKeyword();
            if(el!=null) el.click();
            break;
        case 27: //esc
            keepSearchResults = false;
            var el = document.getElementById("txtSearchInputNav");
            if(el!=null) el.focus();
            break;
        case 38: //up arrow
            var el = document.getElementById("txtSearchInputNav");
            if(el!=null)
                if(el.value!="")
                    window.scrollTo(0,0);
            return selectKeyword("up");
            break;
        case 40: //down arrow
            return selectKeyword("down");
            break;
    }
}

var curKeywordSel = -1;
var keepSearchResults = false;
function selectKeyword(direction)
{
    var ret = true;
    var o = document.getElementById("searchResults");
	if(o!=null)
	{
		if(o.style.display != "none")
		{
		    var el = document.getElementById("searchLinks");
		    if(el!=null)
		    {
		        var links = o.getElementsByTagName("a");
		        if(links.length>0)
		        {
		            keepSearchResults = true;
		            switch(direction)
		            {
		                case "up":
		                    if(curKeywordSel==0)
		                    {
		                        curKeywordSel=-1
		                        var el = document.getElementById("txtSearchInputNav");
                                if(el!=null) el.focus(); 
		                    }
		                    else
		                    {
		                    if(curKeywordSel>0) curKeywordSel--;
		                    if(links[curKeywordSel])
    		                    links[curKeywordSel].focus();
    		                }
    		                break;
		                case "down":
		                    if(curKeywordSel<(links.length-1)) curKeywordSel++;
		                    if(links[curKeywordSel])
    		                    links[curKeywordSel].focus();
    		                if(curKeywordSel==0) return false;
    		                break;
		            }
		        }
		    }
		}
	}
	return ret;
}

function getSelectedKeyword()
{
    if (curKeywordSel < 0) return;    
    var o = document.getElementById("searchResults");
	if(o!=null)
	    if(o.style.display != "none")
		{
		    var el = document.getElementById("searchLinks");
		    if(el!=null)
		    {
		        var links = o.getElementsByTagName("a");
		        if(curKeywordSel < links.length)
		            return links[curKeywordSel]
		    }
		}    
}

function searchTermSelected(arg)
{
    var txtBox = document.getElementById("txtSearchInputNav");
    if(txtBox!=null)
    {
        txtBox.value = arg;
        var o = document.getElementById("searchResults");
	    if(o!=null) o.style.display = "none";
	    txtBox.focus();
    }
}
    
/* Bug Reporting */
function bugReport()
{
    var frm = frames['fBugReport'];
    if(frm!=null)
    {
        var doc = frm.document;
        if(doc!=null)
        {
            var p = doc.getElementById("referer_url");
            if(p!=null)
            {
                p.value = this.location.href;
                var subForm = doc.frmBugReport;
                if(subForm!=null) subForm.submit();
            }
        }
    }
}
function toggleHyperView()
{
    var el = self.PreviewFrame3D;
    if (el != null) {
        el.Hyper();
    }
    else {
        el =top.PreviewFrame3D;
        if (el != null) {
            el.Hyper(); 
        } 
    }
}
document.onkeyup = function (evt) {
    var sourceElement = getSourceElement(evt);
    if (sourceElement)
	{
	    if (sourceElement.id != "txtEmbed" && sourceElement.id != "txtEmbedED")
		{
		    if (invalidCharSeq(sourceElement.value))
		    {
			    alert(top.sInvalidSearchCharMsg);
			    sourceElement.value = "";
		    }
		}
	}
}

//PageRequestManager init moved to ctlBase.cs
var prm;

function AjaxRequestSetCursor(sender, args) {
    document.documentElement.style.cursor = "wait";
}
function AjaxRequestResetCursor() {
    document.documentElement.style.cursor = "default";    
}
function ClearErrorState() {
     $get(messageElem).innerHTML = '';
     ToggleAlertDiv('hidden');                     
}
function EndRequestHandler(sender, args)
{
   AjaxRequestResetCursor();
   if (typeof(args.get_error()) != 'undefined' && args.get_error() != null)
   {
       var errorMessage;
       //console.log(args.get_response().get_statusCode());
       //Status’ in the 200’s are okay; “200” is the best
       //if (args.get_response().get_statusCode() != '200')
       //{
           if (args.get_response().get_statusCode() == '404')
           {
               errorMessage = getRequestErrorArgs(args);
           }
           else
           {
               // Error occurred somewhere other than the server page.
               if (args.get_response().get_statusCode() != null && args.get_error().message != null)
                   errorMessage = 'An unspecified error occurred. ' + args.get_response().get_statusCode() + ' ' + args.get_error().message;
               else if (args.get_response().get_statusCode() != null)
                   errorMessage = 'An unspecified error occurred. ' + args.get_response().get_statusCode();
               else
                   errorMessage = 'An unspecified error occurred.';
           }
               
           args.set_errorHandled(true);
           if(isPageUnloaded && GetBrowserType()==eBrowserType.FF_v3)
           	   return;
           setPageErrorMessage(errorMessage);
           toggleSubForm('requestAlert', true);
       //}
   }
}
function getRequestErrorArgs(args)
{
    var errorMessage = 'An unspecified error occurred.';
    if(typeof(args.get_error())!="undefined" && args.get_error() != null)
        if(typeof(args.get_error().message)!= "undefined" && args.get_error().message != null)
            errorMessage = args.get_error().message;
    
    return errorMessage;
}
function setPageErrorMessage(msg)
{
    $get('requestAlertMessage').innerHTML = msg;
}

function focusTagAddFirst()
{
    if(prm!=null)
    {
        prm.add_endRequest(focusTagAddTxtBox);
    }
    __doPostBack('blTabs','2')
}
function focusTagAddTxtBox(sender, args)
{
    if(prm!=null)
    {
        prm.remove_endRequest(focusTagAddTxtBox);
    }
    __doPostBack('ucModelTags$lbAdd','');
}

//onbeforeunload is supported in IE and FF 0.9 & up
window.onbeforeunload = function() {
    isPageUnloaded=true;
    if(xmlHttp != null)
    {
        xmlHttpDispose();
    }
}

function xmlHttpDispose()
{
    if (isBusy && xmlHttp != null && typeof(xmlHttp) != "undefined")
    {
        xmlHttp.onreadystatechange = function () {}
        xmlHttp.abort();
        xmlHttp = null;
    }
}

function displayToolTip(title, body)
{
	var el = document.getElementById("toolTipPanel");//defined in ctlToolTipPanel
	document.getElementById("toolTipPanel_title").innerHTML = title;
	document.getElementById("toolTipPanel_body").innerHTML = body;
	var tgt = document.getElementById(toolTipSrcEl);
	var tgtPos = cumulativeOffset(tgt);
	el.style.left = tgtPos[0] +'px';
	el.style.top = (tgtPos[1]+22) +'px';
	el.style.display = "";
}    
 

function infoPopUpClose(popup)
{
    var o = document.getElementById(popup);  
    if(o!=null)
	{
	    if(o.style.display == "block")
		{   
		    o.style.display = "none";   
		}
	}	    
}


function TagsCorrect(tagsString)
{
	var tags = tagsString.split(",");	
        var newTagsString = "";
        for (i=0; i<tags.length; i++)
        {
           var tag = tags[i];
           if (tag.length > 50)
              tag = tag.substring(0,50);
           if (i>0)    
              newTagsString += ",";
           newTagsString += tag;
        }
        return newTagsString;
}


function applyAlphaFilter(o) {
     for(var i = 1; i<o.src.length; i++)
     {
        if(o.src.charCodeAt(i)>255)
        {
            return;
        }
    }
     var t= basePath()+"images/1x1.gif";
     if( o.src != t ) 
     {
         var s=o.src;
         o.src = t;
         var Prefix = "";
         if(s.indexOf("//")!=-1)
         {
            Prefix = s.substring(0, s.indexOf("//") + 2);
         }
         var URIEnd = s.substring(Prefix.length);
         var EscapedURIEnd = escape(URIEnd);
         var EscapedURI = Prefix + EscapedURIEnd;
         o.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + EscapedURI + "', sizingMethod='scale')";
     }
 }
 
 function executeAsyncSearch(searchType,inResults) {
     var url = basePath() + "SearchType.axd?searchType=" + searchType + "&r=" + Math.random();
     xmlHttp = GetXmlHttpObject(function() {
         if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
             isBusy = false;
             document.body.style.cursor = "pointer";
             if (xmlHttp.status == 200) {
                 executeSearch(inResults);
             }
             else {
                 setPageErrorMessage('An unspecified error occurred. Please try again');
                 toggleSubForm('requestAlert', true);
             }
         }
     });
     xmlHttp.open("GET", url, true);
     isBusy = true;
     document.body.style.cursor = "wait";
     xmlHttp.send(null);
 }

 function executeFeatureSearch(txtBoxId, isVisual) {
     var sSearch = "";
     var txtBox = document.getElementById(txtBoxId);
     if (txtBox != null) {
         if (txtBox.value.length > 0) {
             //replace initial spaces if any
             var sTerm = txtBox.value.replace(/^\s*/, '');
             sTerm = URLencode(sTerm);
             if (sTerm != "") {

                 var url = basePath() + "SearchType.axd?searchType=LearningSamples&r=" + Math.random();
                 xmlHttp = GetXmlHttpObject(function() {
                     if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
                         isBusy = false;

                         document.body.style.cursor = "pointer";
                         if (xmlHttp.status == 200) {
                             if (isVisual) {
                                 document.location.href = basePath() + 'Feature-Search-Gallery-View.aspx?arg=' + sTerm;
                             }
                             else {
                                 document.location.href = basePath() + 'Feature-Search.aspx?arg=' + sTerm; 
                             }
                         }
                         else {
                             setPageErrorMessage('An unspecified error occurred. Please try again');
                             toggleSubForm('requestAlert', true);
                         }
                     }
                 });
                 xmlHttp.open("GET", url, true);
                 isBusy = true;
                 document.body.style.cursor = "wait";
                 xmlHttp.send(null);
             }
             else {
                 displaySearchMin(txtBox);
                 return false;
             }
         }
         else {
             displaySearchMin(txtBox);
             return false;
         }
         return true;
     }
 }

 function getPageSize() {
     var size = new Object();
     size.height = 0;
     size.width = 0;
     try {
         if (self.innerHeight) {
             size.height = self.innerHeight;
             size.width = self.innerWidth;
             return size;
         }
         if (document.documentElement && document.documentElement.clientHeight) {
             size.height = document.documentElement.clientHeight;
             size.width = document.documentElement.clientWidth;
             return size;
         }
         size.height = document.body.clientHeight;
         size.width = document.body.clientWidth;
     }
     catch (error) {
     }
     return size;
 }

function removeQueryStringParam(url, param) {
    var start = url.indexOf("&" + param + "=");
    var end = url.indexOf("&", start + 1);
    if (start > 0) {
        if (end > 0) {
            url = url.substring(0, start) + url.substring(end);
        }
        else {
            url = url.substring(0, start);
        }
    }
    return url;
}