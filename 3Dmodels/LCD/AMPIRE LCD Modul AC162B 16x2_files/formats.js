﻿// JScript File
var ddFormats, ddVersions, ddUnits;
var formats3D = new Array();
var formats2D = new Array();
//Moved to the Download-Model.aspx page
//var currentFormatAr = formats3D;

function populateFormats()
{
    try
    {
	    ddFormats = document.getElementById("ddFormats");
	    ddVersions = document.getElementById("ddVersions");
	    ddUnits = document.getElementById("ddUnits");

	    populate();
	    updateFormatInfo(0);
	}
	catch(err)
	{
	    window.status = err.description;
	}
}
function updateFormatInfo(idx)
{   
	ddVersions.options.length = 0;
	ddUnits.options.length = 0;
	var i, ii;
	if(currentFormatAr!=null)
	{
	    if(currentFormatAr[idx].versions!=null)
	    {
	        for(i in currentFormatAr[idx].versions)
	        {
		        ddVersions.options.add(new Option(currentFormatAr[idx].versions[i], currentFormatAr[idx].versions[i]));
	        }
	    }
	    if(currentFormatAr[idx].units!=null)
	    {
	        for(ii in currentFormatAr[idx].units)
	        {
	            if(currentFormatAr[idx].formatString != "Pro/E Part/Assembly")
	            {
	             ddUnits.options.add(new Option(currentFormatAr[idx].units[ii], currentFormatAr[idx].units[ii]));
	            }
	            else
	            {
	              ddUnits.style.display = "none";
	              var lbl = document.getElementById("pnlUnits");
	              if(lbl!=null) lbl.style.display = "none";
	            }
	        }
	    }
	}
	//hide the controls if there are no new options
	if(ddVersions.options.length == 0)
	{
	    ddVersions.style.display = "none";
	    var lbl = document.getElementById("lblVersion");
	    if(lbl!=null)
	    {
	        lbl.style.display = "none";
	        var lbl2 = document.getElementById("imgVersionInfo");
	        if(lbl2!=null)
	        {
	            lbl2.style.display = "none";
	        }
	    }    
	}
	else
	{
	    ddVersions.style.display = "";
	    var lbl = document.getElementById("lblVersion");
	    if(lbl!=null) 
	    {
	        lbl.style.display = "";
	        var lbl2 = document.getElementById("imgVersionInfo");
	        if(lbl2!=null)
	        {
	            lbl2.style.display = "";
	        }
	    }    
	}
	if(ddUnits.options.length == 0)
	{
	    ddUnits.style.display = "none";
	    var lbl = document.getElementById("pnlUnits");
	    if(lbl!=null) lbl.style.display = "none";
	}
	else
	{
	    ddUnits.style.display = "";
	    var lbl = document.getElementById("pnlUnits");
	    if(lbl!=null) lbl.style.display = "";
	}
	
	//hide the download all config checkbox if not sldprt
	setConfigAll(idx);
}


function updateVersionInfo(el)
{   
    var elDDFormats = document.getElementById("ddFormats");
        
    if(elDDFormats!=null)
    {
        var sVal = elDDFormats.value;
        var curSel = ddVersions.selectedIndex;
        if(sVal!=null && sVal!="")
        {
            if(sVal.toLowerCase()=="sldprt")
            {
                ddVersions.options.length = 0;
                var i = 0;
                if(currentFormatAr!=null)
                {
                    if(currentFormatAr[elDDFormats.selectedIndex].versions!=null)
                    {
                        for(i in currentFormatAr[elDDFormats.selectedIndex].versions)
                        {
	                        ddVersions.options.add(new Option(currentFormatAr[elDDFormats.selectedIndex].versions[i], currentFormatAr[elDDFormats.selectedIndex].versions[i]));
	                        if((document.getElementById("cbDownloadAll").checked == true) && currentFormatAr[elDDFormats.selectedIndex].versions[i] == sModelVersion)
	                            break;
                        }
                    }
                }
	            
                if(ddVersions.options.length == 0)
                {
                    ddVersions.style.display = "none";
                    var lbl = document.getElementById("lblVersion");
                    if(lbl!=null) lbl.style.display = "none";
                }
                else
                {
                    ddVersions.style.display = "";
                    var lbl = document.getElementById("lblVersion");
                    if (curSel < ddVersions.options.length)
                        ddVersions.selectedIndex = curSel;
                    if(lbl!=null) lbl.style.display = "";
                }
            }
         }
     }
}

function formatInfo(formatdb, format, formatstring, versions, units)
{
    this.formatdb = formatdb;
	this.format = format;
	this.formatString = formatstring;
	this.versions = versions;
	this.units = units;
}
function populate()
{
    ddFormats.options.length = 0;
    var i;
    if (currentFormatAr != null) 
    {
        for (i in currentFormatAr) 
        {
            var val, text;
            val = currentFormatAr[i].formatdb;
            text = currentFormatAr[i].formatString + " (*." + currentFormatAr[i].format + ")";
            ddFormats.options.add(new Option(text, val));
        }
    }
}
function changeFormat(el) {
    if (el != null && el.id != null) {
        switch (el.id.toLowerCase()) 
        {
        case "rdioformat2d":
            if (formats2D != null)
                currentFormatAr = formats2D;
            populateFormats();
            sFormat = "2d";
            if(bIsPartnerWS ==  false) toggleElementVis("pnl2DDrawingView"); //method from base.js
            toggleElementVis("pnl2DDrawingViewOrig"); //method from base.js
            if (typeof (changeOtherViewControl) != 'undefined') {
                changeOtherViewControl("ChangeViewTabTo2D");
            }
            break;
        case "rdioformat3d":
            if (formats3D != null)
                currentFormatAr = formats3D;
            populateFormats();
            sFormat = "3d";
            if (bIsPartnerWS == false) {
                if (document.getElementById("pnl2DDrawingView") != null)
                    document.getElementById("pnl2DDrawingView").style.display = "none";
            }
            //toggleElementVis("pnl2DDrawingView"); //method from base.js
            if (document.getElementById("pnl2DDrawingViewOrig") != null)
                document.getElementById("pnl2DDrawingViewOrig").style.display = "none";
            //toggleElementVis("pnl2DDrawingViewOrig"); //method from base.js
            if (typeof (changeOtherViewControl) != 'undefined') {
                changeOtherViewControl("ChangeViewTabTo3D");
            }
            break;
    }
  }
}
function setConfigAll(idx)
{
    var elDDFormats = document.getElementById("ddFormats");
    if(elDDFormats!=null)
    {
        var sVal = elDDFormats.value;
        if(sVal!=null && sVal!="")
        {
            if(sVal.toLowerCase()=="sldprt")
            {
                if(document.getElementById('pnlAllConfig')!=null)
                    document.getElementById('pnlAllConfig').style.display = 'block';
                if(document.getElementById("cbDownloadAll")!=null)
                    document.getElementById("cbDownloadAll").checked = false;
            }
            else
            {
                if(document.getElementById('pnlAllConfig')!=null)
                    document.getElementById('pnlAllConfig').style.display = 'none';
                if(document.getElementById("cbDownloadAll")!=null)
                    document.getElementById("cbDownloadAll").checked = false;
            }
        }
    }
}

//ensures the download options are populated when switching tabs on the download page(s)
//prm declated in base.js
window.onload = function (evt) {
    if(prm==null) {
        prm = Sys.WebForms.PageRequestManager.getInstance();
    }
    if(prm != null) {
        prm.add_endRequest(populateFormats);
    }
}