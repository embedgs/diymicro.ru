var djApplicationEventManager = new Object();
djApplicationEventManager.fireEvent =  function (objectID, eventId) {
    var djEventObject = this[objectID];
    if ( typeof djEventObject != "undefined" ) {
        return djEventObject.events[eventId](arguments[2], arguments[3], arguments[4]);
    }
}
djApplicationEventManager.registerEvent = djRegisterEvent;
djApplicationEventManager.unregisterEvent = djUnRegisterEvent;


djObjectExtendCollection.SWXIAApplication = djApplicationExtend;

function djApplicationExtend(dsObj) {
   dsObj.releaseAll = function () { return djProcessCommand(this, 'releaseAll', arguments); };
   dsObj.get_Version = function () { return djProcessCommand(this, 'get_Version', arguments); };
   dsObj.RegisterEvent =  function(eventId, eventFunction) { djApplicationEventManager.registerEvent(this, eventId, eventFunction); return djProcessCommand(this, 'RegisterEvent', arguments); };
   dsObj.UnRegisterEvent =  function(eventId) { djApplicationEventManager.unregisterEvent(this, eventId); return djProcessCommand(this, 'UnRegisterEvent', arguments); };
   dsObj.Import3DXMLModel = function (iPathOrURL) { return djProcessCommand(this, 'Import3DXMLModel', arguments); };
   dsObj.ImportSolidWorksModel = function (iPathOrURL) { return djProcessCommand(this, 'ImportSolidWorksModel', arguments); };
   dsObj.ImportProEModel = function (iPathOrURL) { return djProcessCommand(this, 'ImportProEModel', arguments); };
   dsObj.ImportIGESModel = function (iPathOrURL) { return djProcessCommand(this, 'ImportIGESModel', arguments); };
   dsObj.ImportSTEPModel = function (iPathOrURL) { return djProcessCommand(this, 'ImportSTEPModel', arguments); };
   dsObj.ImportAcisModel = function (iPathOrURL) { return djProcessCommand(this, 'ImportAcisModel', arguments); };
   dsObj.ImportParasolidModel = function (iPathOrURL) { return djProcessCommand(this, 'ImportParasolidModel', arguments); };
}

