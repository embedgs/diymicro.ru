﻿
function isnextGenServiceRunning() {
    try {
        var mScript = new djScriptManager();
        var mApp = mScript.getApplication();
        mApp.release();
        return true;
    }
    catch (err) {
        return false;
    }
}
function getNextGenVersionForModel(downloadFormat) {
    return nextGenVersions[downloadFormat];
}

function hideShowNextGenButton() {
    var sFormatName = document.getElementById("ddFormats").value;
    var nextGenBtn = document.getElementById("nextGenUpload");
    var currentNextGenVersion = null;
    var formatVersion = document.getElementById("ddVersions").value;
    var isVersionSupported = false;
    
    if (isnextGenServiceRunning()) {
        var mScript = new djScriptManager();
        var mApp = mScript.getApplication();
        currentNextGenVersion = mApp.get_Version();
        mApp.release();
    }

    var key = sFormatName + "_" + formatVersion;
    var nextGenSupportedVersionForModel = null;
    
    if(sFormat.toLowerCase() == "3d")
        nextGenSupportedVersionForModel = getNextGenVersionForModel(key);

    if (nextGenSupportedVersionForModel != null && nextGenSupportedVersionForModel.toString().length > 0) {
        /*SPR 712686 : If current Nextgen version is greater than or equal to NextgenVersion supported by format,
                       set isVersionSupported to true.  */
        if (currentNextGenVersion >= nextGenSupportedVersionForModel)
            isVersionSupported = true;
    }

    if (strModelType == 'pdf')
        nextGenBtn.style.display = 'none';

    else if (sFormatName == '3dxml' || sFormatName == 'step' || sFormatName == 'sldprt')
    {
        if (isnextGenServiceRunning() && isVersionSupported == true) 
                  nextGenBtn.style.display = '';
        else
            nextGenBtn.style.display = 'none';    
    }
    else
        nextGenBtn.style.display = 'none';

}

function nextGenUpload_onclick() {
    if (isnextGenServiceRunning()) {
        var url = v6DownloadURL;
        //Remove the query string from the URL, SPR 684617
        url = url.split("?")[0];
        redirectCheck(downloadedModelFile);
        var sFormatName = document.getElementById("ddFormats").value;
        var mScript = new djScriptManager();
        var mApp = mScript.getApplication();

        //if file in url is not V6 compatible, get the file from temp folder
        if (isV6CompatibleFileName == "false") {
            var zipfileName = getFileName(url);
            var zipFileIndex = url.indexOf(zipfileName, 0);
            var filepath = url.substring(0, zipFileIndex - 1);
            url = filepath + "/TempNextGen" + "/" + downloadedModelFile + ".zip";
        }
        
        if (sFormatName == '3dxml')
            mApp.Import3DXMLModel(url);
        else if (sFormatName == 'sldprt')
            mApp.ImportSolidWorksModel(url);
        else if (sFormatName == 'step')
            mApp.ImportSTEPModel(url);

        mApp.releaseAll();
      }
  }

  function getFileName() {
      //this gets the full url
      var filename = v6DownloadURL;
      //this removes the anchor at the end, if there is one
      filename = filename.substring(0, (filename.indexOf("#") == -1) ? filename.length : filename.indexOf("#"));
      //this removes the query after the file name, if there is one
      filename = filename.substring(0, (filename.indexOf("?") == -1) ? filename.length : filename.indexOf("?"));
      //this removes everything before the last slash in the path
      filename = filename.substring(filename.lastIndexOf("/") + 1, filename.length);
      //return
      return filename;
  }

  function redirectCheck(s) {
      if (s != null) {
          if (s.toLowerCase() == "redirect") {
              //window.location.href = basePath() + "Default.aspx";SPR 468884            
              var url = document.location.href;
              var idx = url.indexOf('#');
              alert('Something went wrong. Please try again after the page is reloaded!');
              if (idx > -1) {
                  document.location.href = url.substring(0, idx);
              }
              else {
                  document.location.reload();
              }
          }
      }
  }

   