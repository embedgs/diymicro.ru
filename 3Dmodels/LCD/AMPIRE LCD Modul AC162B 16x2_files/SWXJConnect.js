//========================================================================//
//                        SOURCE CODE LICENSE                             //
//  Copyright 2012 Dassault Syst�mes SolidWorks Corporation               //
//  All rights reserved.                                                  //
//  This software and related documentation are proprietary to            // 
//  Dassault Syst�mes SolidWorks Corporation                              //
//========================================================================//
// djConnect.js, JavaScript API djConnect interface
//

var djServerAddress = new Object();
djServerAddress.Host = "localhost";
djServerAddress.Port = 7756;
djServerAddress.SetAddress = function(HostIn, PortIn) {
    this.Host = HostIn;
    this.Port = PortIn;
}; 

djServerAddress.GetHTTPAddress = function() {
    return "http://" + this.Host + ":" + this.Port;
}


var djObjectExtendCollection = new Object();

function djError(errMsg) {
    var jsError = new Object();
    jsError.name = "dsAPIError";
    jsError.message = errMsg + "\n";
    return jsError;
}

function djErrorString(errObj) {
    var jsError;
    if ( typeof errObj.name == "undefined" ) {
       jsError = "JavaScriptError"; 
    }
    else {
       jsError = errObj.name;
    }
    jsError +=  ": ";
    if ( typeof errObj.message == "undefined" ) {
       jsError += "Unknown Error"; 
    }
    else {
       jsError += errObj.message;
    }
    if ( jsError.charAt(jsError.length - 1) != "\n" ) {
       jsError += "\n";
    }
    return jsError;
}


function djScriptManager () {
    this.type = 'jsScriptManager';
    this.getApplication =  function () { return djProcessCommand(this, 'getApplication', arguments); };
}


function djObjectExtend(dsObj) {
   dsObj.release =  function() { return djProcessCommand(this, 'release', arguments); };
   dsObj.getDsType =  function() { return this.type + 'Type'; }
}


function djExtendCass(dsObj) {
    if ( dsObj == null ) {
       return;
    }
    if ( dsObj instanceof Array ) {
        for (var ii = 0; ii < dsObj.length; ii++) {
            djExtendCass(dsObj[ii]);
        }
    }
    if ( typeof dsObj.type === "undefined" ) {
       return;
    }
    var extFuncion = djObjectExtendCollection[dsObj.type];
    if ( typeof extFuncion  != "function" ) {
        throw djError("Missing library header " + dsObj.type + ".js!");
    }
    extFuncion(dsObj);
    djObjectExtend(dsObj);
}


function djProcessReturn(retStr) {
    if ( !retStr || 0 === retStr.length) {
       throw djError("dsAPIHttpService connection error!");
    }
    var retObj = JSON.parse(retStr);
    if ( retObj == null ) {
       return;
    }
    if ( typeof retObj.type === "undefined" ) {
        for ( var member in retObj )
            djExtendCass(retObj[member]);
    }
    else {
        djExtendCass(retObj);
    }
    return retObj;
}

function djGetXMLHttpRequest() {
    var req;
    if (typeof XMLHttpRequest != "undefined") {
       req = new  XMLHttpRequest();
    }
    if ( typeof req == "undefined" ) {
       req = new ActiveXObject("Msxml2.XMLHTTP");
    }
    if ( typeof req == "undefined" ) {
       req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    return req;
}


function djSendCommand(command) {
    var isIE = false;
    if ( typeof navigator != "undefined" ) {
      isIE = navigator.appName == 'Microsoft Internet Explorer';
    }
    else if (typeof WScript != "undefined" ) {
      isIE = true;
    }
    var req = djGetXMLHttpRequest();
    req.open("POST", djServerAddress.GetHTTPAddress(), false);
    req.setRequestHeader("Content-Type", "text/json;charset=UTF-8");

    if ( isIE ) {
        req.setRequestHeader("Content-dsCommand", "fromIE");
    }

    req.send(command);

    var retStr = req.responseText;
    return djProcessReturn(retStr);
}


function djProcessCommand(owner, funcName, args) {
    var fncObj = new Object();
    fncObj.language = "dsJavaScript";
    fncObj.owner = owner;
    fncObj.functionName = funcName;
    fncObj.args = new Array();
    for (var ii = 0; ii < args.length; ii++) {
        fncObj.args[ii] = args[ii];
    }
    var jsCmdStr = JSON.stringify(fncObj);
    var jsResult = djSendCommand(jsCmdStr);

    if ( jsResult != null && typeof jsResult.name !== "undefined" ) {
        
        throw jsResult;
    }    
    
    return jsResult;
}

function djRegisterEvent (djObject, eventId, eventFunction ) {
   var djEventObject = this[djObject.id];
   if ( typeof djEventObject == "undefined" ) {
      this[djObject.id] = djEventObject = new Object();
      djEventObject.object = djObject;
   }
   if ( typeof djEventObject.events == "undefined" ) {
       djEventObject.events = new Array();
   }
   djEventObject.events[eventId] = eventFunction;
}

function djUnRegisterEvent (djObject, eventId ) {
   var djEventObject = this[djObject.id];
   if ( typeof djEventObject != "undefined" ) {
      djEventObject.object = djObject;
      delete djEventObject.events[eventId];
   }
}

