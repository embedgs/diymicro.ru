tx_phases = zeros(1,4);

tx_phases(1) = 0; %TX0 phase is a reference
[c,lags]=xcorr(out.tx1.signals.values,out.tx0.signals.values);
[peak,lag_index]=max(abs(c));
step_local_ps = 1e12*lags(lag_index)*stoptime/total_n_samples;
step_local_deg = (fin*360)*step_local_ps*1e-12;
tx_phases(2)= step_local_ps;


[c,lags]=xcorr(out.tx2.signals.values,out.tx0.signals.values);
[peak,lag_index]=max(abs(c));
step_local_ps = 1e12*lags(lag_index)*stoptime/total_n_samples;
tx_phases(3)= step_local_ps;


[c,lags]=xcorr(out.tx3.signals.values,out.tx0.signals.values);
[peak,lag_index]=max(abs(c));
step_local_ps = 1e12*lags(lag_index)*stoptime/total_n_samples;
tx_phases(4)= step_local_ps;

hold on;
grid on;

plot(tx_phases,LineWidth=2,marker='x',color="green");
xlabel("TX channel");
ylabel("Delay w.r.t TX0, ps");
legend("Delays between channels");