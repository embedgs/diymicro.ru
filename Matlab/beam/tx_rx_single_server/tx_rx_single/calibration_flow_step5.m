clear;
clf;

rng("shuffle");
mdl = 'tx_calibration.slx';

input_source_setup;		%call for input source setup

%------------------end of mult configuration-------------------

number_of_runs = 8;
phases_error_initial = zeros(1,number_of_runs);
phases_error = zeros(1,number_of_runs);

%------------------mismatches section -------------------------
mc_tx_en = 1;                       %random vga gains or not
mc_tx_mult_gain_en = 1;                 %mult gain k mismatches
mc_tx_mult_delay_en = 1;                %mult delay errors 
%------------------end of mismatches enable/disable section----



for (run=1:1:number_of_runs)
	disp("MC sweep "+ run);
	calibration_step5;
    phases_error(run)=postcalibration_phases_error;
    phases_error_initial(run)=initial_phases_error;
end	

subplot(1,2,1);
histogram(phases_error_initial);
hold on;
xlabel("PRE-CAL MAX delays mismatch");
title("mean="+mean(phases_error_initial)+", std. dev="+std(phases_error_initial));
legend();

subplot(1,2,2);
histogram(phases_error);
hold on;
xlabel("POST-CAL MAX delays mismatch");
title("mean="+mean(phases_error)+", std. dev="+std(phases_error));
legend();