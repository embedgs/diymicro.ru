clear;
clf;

%input source setup
fin = 10e9;

phase_in0 = 0;
phase_in1 = 0;
nsamples = 512; %per period
nperiods = 8;  %more better for delay measurements    
total_n_samples = nsamples*nperiods;
stoptime = nperiods/fin;
vin0 = 0.2;
vin1 = 0.2;

mdl = 'single_mult.slx';

mult0_b0 = 0;
mult0_b1 = 0;
mult0_k = 0.2;



%adding non-idealities
mult0_offset = 0.005;
mult0_delay_error = 5e-12;


mult0_delay_error_compensation = 0;


% out = sim(mdl);
% 
% t = out.tout;
% in0 = out.in0.signals.values;
% in1 = out.in1.signals.values;
% product = out.product0.signals.values;
% 
% subplot(2,1,1);
% plot(t,in0);
% xlabel('t, s');
% ylabel('V, V');
% hold on;
% grid on;
% plot(t,in1,color='blue');
% subplot(2,1,2);
% plot(t,product,color='red',DisplayName="-100mV");
% xlabel('t, s');
% ylabel('Product, V');
% hold on;
% grid on;

% mult_avg_values=[];
% phase1_values = [];
% phase1_values = [phase1_values,phase_in1];
% out=sim(mdl);
% mult0_average = out.mult0_avg.signals.values(length(out.mult0_avg.signals.values));             %saving the averaged result
% 
% mult_avg_values=[mult_avg_values,mult0_average];
% 
% 
% 
% for (i=1:1:64)
%     display("Entering the sweep...");
%     display(i);
%     phase_in1 = (-360/64)*i;
%     phase1_values = [phase1_values,abs(phase_in1)];
%     out=sim(mdl);
%     mult0_average = out.mult0_avg.signals.values(length(out.mult0_avg.signals.values));             %saving the averaged result
% 
%     mult_avg_values=[mult_avg_values,mult0_average];
% 
% end    
% 
% plot(phase1_values,mult_avg_values,linewidth=4,DisplayName="simulated");
% hold on;
% grid on;
% xlabel('phase difference, deg');
% ylabel('average value of mult, v');
% 
% ideal_mult_phase_response = load("ideal_mult_phase_response.mat");
% plot(phase1_values,ideal_mult_phase_response.mult_avg_values,linewidth=4,DisplayName="ideal response");
% legend();


%sweep to get response across delay error compensation steps
mult_avg_values=[];
mult_delay_compensation_values=[];
for (i=1:1:16)
    display("Entering the sweep...");
    display(i);
    out=sim(mdl);
    mult0_average = out.mult0_avg.signals.values(length(out.mult0_avg.signals.values));             %saving the averaged result
    mult_avg_values=[mult_avg_values,mult0_average];
    mult_delay_compensation_values = [mult_delay_compensation_values,mult0_delay_error_compensation];

    mult0_delay_error_compensation = mult0_delay_error_compensation + 0.5e-12;

end    

plot(mult_delay_compensation_values,mult_avg_values,linewidth=4,DisplayName="sweep of delay error compensation");
grid on;
xlabel("Delay error compensation value, ps");
ylabel("Product, V");