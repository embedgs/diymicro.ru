%===================================step 3 redo - changing vga codes to have the same amplitudes ======================

%configure mult inputs tx*tx, we analyzing amplitudes
mult0_b0 = 1;
mult0_b1 = 1;
mult1_b0 = -1;
mult1_b1 = -1;
mult2_b0 = -1;
mult2_b1 = -1;
mult3_b0 = -1;
mult3_b1 = -1;
%------------------end of mult configuration-------------------

display("Step 3 - aligning the output amplitudes");
mult_setup;				%mult setup
tx_initial_setup;		%initial settings of tx
out = sim(mdl);

get_mult_averages_no_plot;
%==============OUR REFERENCE VALUE==============================
ref_mult_avg = tx_mult_averages(1);

get_amplitudes_no_plot;
get_phases_no_plot;
initial_delays_array = tx_phases;
initial_amplitudes_error = max(tx_amplitudes)- min(tx_amplitudes);
initial_phases_error = max(tx_phases) - min(tx_phases);


current_avg_error_1 = abs(ref_mult_avg - tx_mult_averages(2));
current_avg_error_2 = abs(ref_mult_avg - tx_mult_averages(3));
current_avg_error_3 = abs(ref_mult_avg - tx_mult_averages(4));

current_vga_code = 24;
calibrated_vga_code_1 = current_vga_code;
calibrated_vga_code_2 = current_vga_code;
calibrated_vga_code_3 = current_vga_code;

num_runs_for_step3 = 4;


mult_avg_values_1 = zeros(1,num_runs_for_step3);
mult_avg_values_2 = zeros(1,num_runs_for_step3);
mult_avg_values_3 = zeros(1,num_runs_for_step3);



for (i=1:1:num_runs_for_step3)
	display("sweeping vga gain, step" + i);
	%tx0_gain_vga = tx0_gain_vga_values(15+i);
    tx1_gain_vga = tx1_gain_vga_values(current_vga_code - num_runs_for_step3/2 +i);
	tx2_gain_vga = tx2_gain_vga_values(current_vga_code - num_runs_for_step3/2 +i);
	tx3_gain_vga = tx3_gain_vga_values(current_vga_code - num_runs_for_step3/2 +i);

	out=sim(mdl);
    get_mult_averages_no_plot;

	temp_avg_error_1 = abs(ref_mult_avg - tx_mult_averages(2));
	temp_avg_error_2 = abs(ref_mult_avg - tx_mult_averages(3));
	temp_avg_error_3 = abs(ref_mult_avg - tx_mult_averages(4));
   

    mult_avg_values_0(i) = tx_mult_averages(1);
    mult_avg_values_1(i) = tx_mult_averages(2);
    mult_avg_values_2(i) = tx_mult_averages(3);
    mult_avg_values_3(i) = tx_mult_averages(4);
    
	if (temp_avg_error_1<current_avg_error_1)
		current_avg_error_1 = temp_avg_error_1;
		calibrated_vga_code_1 = current_vga_code - num_runs_for_step3/2 +i;
	end	


	if (temp_avg_error_2<current_avg_error_2)
		current_avg_error_2 = temp_avg_error_2;
		calibrated_vga_code_2 = current_vga_code - num_runs_for_step3/2 +i;
	end	


	if (temp_avg_error_3<current_avg_error_3)
		current_avg_error_3 = temp_avg_error_3;
		calibrated_vga_code_3 = current_vga_code - num_runs_for_step3/2 +i;
	end		

end	

% %===================one more run with calibrated vga codes============

display("Step 3 is done, we have the next vga codes for #1 "+calibrated_vga_code_1);
display("Step 3 is done, we have the next vga codes for #2 "+calibrated_vga_code_2);
display("Step 3 is done, we have the next vga codes for #3 "+calibrated_vga_code_3);

tx1_gain_vga = tx1_gain_vga_values(calibrated_vga_code_1);
tx2_gain_vga = tx2_gain_vga_values(calibrated_vga_code_2);
tx3_gain_vga = tx3_gain_vga_values(calibrated_vga_code_3);

out = sim(mdl);
get_amplitudes_no_plot;
postcalibration_amplitudes_error = max(tx_amplitudes)- min(tx_amplitudes);



%========================================================================
%=====================entering step 5: make the proper mult setup;=======
%======= 0 -> 1==============
mult0_b0 = 1;
mult0_b1 = -1;

mult1_b0 = 1;
mult1_b1 = -1;

%======================we can sweep now vga1 and vga3, 0-1 and 0-3 are chosen


display("Step 5 - getting ready to sweep phases");

num_runs_for_step5 = 8;
tx_ps_delay_code_ref = 48;

tx_mult_avg_1 = zeros(1,num_runs_for_step5);
tx_mult_avg_2 = zeros(1,num_runs_for_step5);
tx_mult_avg_3 = zeros(1,num_runs_for_step5);


for (i=1:1:num_runs_for_step5)
	display("sweeping ps phases and getting mult averages for 0-1 and 0-3 " + i);
	tx1_delay_ps = tx1_delay_ps_values(tx_ps_delay_code_ref - num_runs_for_step5/2 + i);
	tx3_delay_ps = tx3_delay_ps_values(tx_ps_delay_code_ref - num_runs_for_step5/2 + i);
	out = sim(mdl);
	get_phases_no_plot;
    get_mult_averages_no_plot;
	tx_mult_avg_1(i) = tx_mult_averages(2);
	tx_mult_avg_3(i) = tx_mult_averages(1);
end

[temp_max, calibrated_ps_code_1] = max(tx_mult_avg_1);
[temp_max, calibrated_ps_code_3] = max(tx_mult_avg_3);

tx1_delay_ps = tx1_delay_ps_values(tx_ps_delay_code_ref - num_runs_for_step5/2+calibrated_ps_code_1);
tx3_delay_ps = tx3_delay_ps_values(tx_ps_delay_code_ref - num_runs_for_step5/2+calibrated_ps_code_3);


%==================== enabling 1-2 phase check now =========================================
mult2_b0 = 1;
mult2_b1 = -1;


for (i=1:1:num_runs_for_step5)
	display("sweeping ps phases and getting mult averages for 1-2 " + i);
	tx2_delay_ps = tx2_delay_ps_values(tx_ps_delay_code_ref - num_runs_for_step5/2 + i);
	out = sim(mdl);
	get_mult_averages_no_plot;
	tx_mult_avg_2(i) = tx_mult_averages(3);
end

[temp_max, calibrated_ps_code_2] = max(tx_mult_avg_2);
tx2_delay_ps = tx2_delay_ps_values(tx_ps_delay_code_ref - num_runs_for_step5/2+calibrated_ps_code_2);

out = sim(mdl);
get_phases_no_plot;
postcalibration_phases_error = max(tx_phases) - min(tx_phases);

