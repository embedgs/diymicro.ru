clear;
%clf;

rng("shuffle");
mdl = 'tx_calibration.slx';

input_source_setup;		%call for input source setup

%------------------end of mult configuration-------------------

number_of_runs = 1;
amplitudes_error_initial = zeros(1,number_of_runs);
amplitudes_error = zeros(1,number_of_runs);

%------------------mismatches section -------------------------
mc_tx_en = 1;                       %random vga gains or not
mc_tx_mult_gain_en = 1;                 %mult gain k mismatches
mc_tx_mult_delay_en = 1;                %mult delay errors 
%------------------end of mismatches enable/disable section----



for (run=1:1:number_of_runs)
	display("MC sweep "+ run);
	calibration_step4;
    amplitudes_error(run)=postcalibration_amplitudes_error;
    amplitudes_error_initial(run)=initial_amplitudes_error;
end	

% subplot(1,2,1);
% histogram(amplitudes_error_initial);
% hold on;
% xlabel("PRE-CAL MAX amplitude mismatch");
% title("mean="+mean(amplitudes_error_initial)+", std. dev="+std(amplitudes_error_initial));
% legend();
% 
% subplot(1,2,2);
% histogram(amplitudes_error);
% hold on;
% xlabel("POST-CAL MAX amplitude mismatch");
% title("mean="+mean(amplitudes_error)+", std. dev="+std(amplitudes_error));
% legend();