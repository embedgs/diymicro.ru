%input source setup
fin = 12e9;

phase_in= 0;
nsamples = 1024; %per period
nperiods = 24;  %more better for delay measurements    
total_n_samples = nsamples*nperiods;
stoptime = nperiods/fin;
vin = 0.02;
tin = 1/fin;
%end of input source setup