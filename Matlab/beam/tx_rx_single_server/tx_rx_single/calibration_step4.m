
%configure mult inputs tx*tx, we analyzing amplitudes
mult0_b0 = 1;
mult0_b1 = 1;
mult1_b0 = -1;
mult1_b1 = -1;
mult2_b0 = -1;
mult2_b1 = -1;
mult3_b0 = -1;
mult3_b1 = -1;
%------------------end of mult configuration-------------------

display("Step 4 - aligning the output amplitude steps");
mult_setup;				%mult setup
tx_initial_setup;		%initial settings of tx
out = sim(mdl);

get_mult_averages_no_plot;
%==============OUR REFERENCE VALUE==============================
ref_mult_avg = tx_mult_averages(1);
%================================================================


get_amplitudes_no_plot;
initial_amplitudes_error = max(tx_amplitudes)- min(tx_amplitudes);


current_avg_error_1 = abs(ref_mult_avg - tx_mult_averages(2));
current_avg_error_2 = abs(ref_mult_avg - tx_mult_averages(3));
current_avg_error_3 = abs(ref_mult_avg - tx_mult_averages(4));

calibrated_vga_code_0 = 24;	%dummy value
calibrated_vga_code_1 = 24;
calibrated_vga_code_2 = 24;
calibrated_vga_code_3 = 24;

mult_avg_values_0 = zeros(1,16);
mult_avg_values_1 = zeros(1,16);
mult_avg_values_2 = zeros(1,16);
mult_avg_values_3 = zeros(1,16);

amplitudes_values_0 = zeros(1,16);
amplitudes_values_1 = zeros(1,16);
amplitudes_values_2 = zeros(1,16);
amplitudes_values_3 = zeros(1,16);

%---------------------------building arrayrs with the data-----------------------

for (i=1:1:16)
	display("sweeping vga gain, step" + i);
	%---------sweeping all 4 vgas------------
	tx0_gain_vga = tx0_gain_vga_values(15+i);
    tx1_gain_vga = tx1_gain_vga_values(15+i);
	tx2_gain_vga = tx2_gain_vga_values(15+i);
	tx3_gain_vga = tx3_gain_vga_values(15+i);

	out=sim(mdl);
    get_mult_averages_no_plot;
    get_amplitudes_no_plot;

    mult_avg_values_0(i) = tx_mult_averages(1);
    mult_avg_values_1(i) = tx_mult_averages(2);
    mult_avg_values_2(i) = tx_mult_averages(3);
    mult_avg_values_3(i) = tx_mult_averages(4);


    amplitudes_values_0(i) = tx_amplitudes(1);
    amplitudes_values_1(i) = tx_amplitudes(2);
    amplitudes_values_2(i) = tx_amplitudes(3);
    amplitudes_values_3(i) = tx_amplitudes(4);
 

end	

%-------------------------end building arrayrs with the data---------------------



for (i=1:1:16)
	display("Checking for a new mid code...");
    %================ determining the closest to ref code
	temp_avg_error_1 = abs(ref_mult_avg - mult_avg_values_1(i));
	temp_avg_error_2 = abs(ref_mult_avg - mult_avg_values_2(i));
	temp_avg_error_3 = abs(ref_mult_avg - mult_avg_values_3(i));
   


    
	if (temp_avg_error_1<current_avg_error_1)
		current_avg_error_1 = temp_avg_error_1;
		calibrated_vga_code_1 = 18+i;
	end	


	if (temp_avg_error_2<current_avg_error_2)
		current_avg_error_2 = temp_avg_error_2;
		calibrated_vga_code_2 = 18+i;
	end	


	if (temp_avg_error_3<current_avg_error_3)
		current_avg_error_3 = temp_avg_error_3;
		calibrated_vga_code_3 = 18+i;
	end		
end	%of the new ref code generation


%=======================determine post-calibration error



% %===================one more run with calibrated vga codes============
tx0_gain_vga = tx0_gain_vga_values(24);
tx1_gain_vga = tx1_gain_vga_values(calibrated_vga_code_1);
tx2_gain_vga = tx2_gain_vga_values(calibrated_vga_code_2);
tx3_gain_vga = tx3_gain_vga_values(calibrated_vga_code_3);
out = sim(mdl);
get_amplitudes_no_plot;
postcalibration_amplitudes_error = max(tx_amplitudes)- min(tx_amplitudes);



%======================re-shuffling of the vga gains: goal to get 2dB step =========

vga_gain_ideal_step_value = 10^(2/20); %from db to times

%----------------------------temp values needed to work


calibrated_vga_codes_0 = zeros(1,8);
calibrated_vga_codes_1 = zeros(1,8);
calibrated_vga_codes_2 = zeros(1,8);
calibrated_vga_codes_3 = zeros(1,8);

calibrated_vga_codes_0(1) = 1;
calibrated_vga_codes_1(1) = 1;
calibrated_vga_codes_2(1) = 1;
calibrated_vga_codes_3(1) = 1;


%=======================TX0 shuffling===========================================
current_iteration = 1;
temp_current_iteration = current_iteration;    

while (current_iteration<8)

%%display('shuffle, shuffle...tx0');    
mult0_gain_step_for_test = 1000;
	for (i=temp_current_iteration:1:15)
				temp_mult0_gain_step_for_test = sqrt(mult_avg_values_0(i+1)/mult_avg_values_0(temp_current_iteration));
				abs(temp_mult0_gain_step_for_test-vga_gain_ideal_step_value);
                if (abs(temp_mult0_gain_step_for_test-vga_gain_ideal_step_value)<mult0_gain_step_for_test)
						mult0_gain_step_for_test = abs(temp_mult0_gain_step_for_test-vga_gain_ideal_step_value);
                        saved_iter_number = i+1;
	    		end	%if
            
	end %for
    temp_current_iteration = saved_iter_number;
    calibrated_vga_codes_0(current_iteration+1)=temp_current_iteration;
	current_iteration = current_iteration + 1;
end


%=======================TX1 shuffling===========================================
current_iteration = 1;
temp_current_iteration = current_iteration;    

while (current_iteration<8)

%display('shuffle, shuffle...tx1');    
mult1_gain_step_for_test = 1000;
	for (i=temp_current_iteration:1:15)
				temp_mult1_gain_step_for_test = sqrt(mult_avg_values_1(i+1)/mult_avg_values_1(temp_current_iteration));
				if (abs(temp_mult1_gain_step_for_test-vga_gain_ideal_step_value)<mult1_gain_step_for_test)
						mult1_gain_step_for_test = abs(temp_mult1_gain_step_for_test-vga_gain_ideal_step_value);
                        saved_iter_number = i+1;
	    		end	%if
            
	end %for
    temp_current_iteration = saved_iter_number;
    calibrated_vga_codes_1(current_iteration+1)=temp_current_iteration;
	current_iteration = current_iteration + 1;
end


%=======================TX2 shuffling===========================================
current_iteration = 1;
temp_current_iteration = current_iteration;    

while (current_iteration<8)

%display('shuffle, shuffle...tx1');    
mult2_gain_step_for_test = 1000;
	for (i=temp_current_iteration:1:15)
				temp_mult2_gain_step_for_test = sqrt(mult_avg_values_2(i+1)/mult_avg_values_2(temp_current_iteration));
				if (abs(temp_mult2_gain_step_for_test-vga_gain_ideal_step_value)<mult2_gain_step_for_test)
						mult2_gain_step_for_test = abs(temp_mult2_gain_step_for_test-vga_gain_ideal_step_value);
                        saved_iter_number = i+1;
	    		end	%if
            
	end %for
    temp_current_iteration = saved_iter_number;
    calibrated_vga_codes_2(current_iteration+1)=temp_current_iteration;
	current_iteration = current_iteration + 1;
end


%=======================TX3 shuffling===========================================
current_iteration = 1;
temp_current_iteration = current_iteration;    

while (current_iteration<8)

%display('shuffle, shuffle...tx1');    
mult3_gain_step_for_test = 1000;
	for (i=temp_current_iteration:1:15)
				temp_mult3_gain_step_for_test = sqrt(mult_avg_values_3(i+1)/mult_avg_values_3(temp_current_iteration));
				if (abs(temp_mult3_gain_step_for_test-vga_gain_ideal_step_value)<mult3_gain_step_for_test)
						mult3_gain_step_for_test = abs(temp_mult3_gain_step_for_test-vga_gain_ideal_step_value);
                        saved_iter_number = i+1;
	    		end	%if
            
	end %for
    temp_current_iteration = saved_iter_number;
    calibrated_vga_codes_3(current_iteration+1)=temp_current_iteration;
	current_iteration = current_iteration + 1;
end




%=======================check the shuffling impact on the amplitudes now=================================
tx_vga_gains_0 = zeros(1,7);
tx_vga_gains_1 = zeros(1,7);
tx_vga_gains_2 = zeros(1,7);
tx_vga_gains_3 = zeros(1,7);

for (i=1:1:7)
	tx_vga_gains_0(i) = convert_to_dB(amplitudes_values_0(calibrated_vga_codes_0(i+1))/amplitudes_values_0(calibrated_vga_codes_0(i)));
	tx_vga_gains_1(i) = convert_to_dB(amplitudes_values_1(calibrated_vga_codes_1(i+1))/amplitudes_values_1(calibrated_vga_codes_1(i)));
	tx_vga_gains_2(i) = convert_to_dB(amplitudes_values_2(calibrated_vga_codes_2(i+1))/amplitudes_values_2(calibrated_vga_codes_2(i)));
	tx_vga_gains_3(i) = convert_to_dB(amplitudes_values_3(calibrated_vga_codes_3(i+1))/amplitudes_values_3(calibrated_vga_codes_3(i)));

end


%plot gain and phase steps

plot(tx_vga_gains_0-2,LineWidth=2,DisplayName="TX0");
hold on;
grid on;
plot(tx_vga_gains_1-2,LineWidth=2,DisplayName="TX1");
plot(tx_vga_gains_2-2,LineWidth=2,DisplayName="TX2");
plot(tx_vga_gains_3-2,LineWidth=2,DisplayName="TX3");
xlabel("vga code");
ylabel("Gain - 2dB,dB");
%legend();



function T = convert_to_dB(x)
    T = 20*log10(x);
end    