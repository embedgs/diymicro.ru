t = out.tout;
tstart_index = total_n_samples - nsamples;
tstop_index = length(t);
tplot = t(tstart_index:tstop_index);


plot(tplot,out.tx0.signals.values(tstart_index:tstop_index),DisplayName="TX0",LineWidth=2);
hold on;
grid on;
plot(tplot,out.tx1.signals.values(tstart_index:tstop_index),DisplayName="TX1",LineWidth=2);
plot(tplot,out.tx2.signals.values(tstart_index:tstop_index),DisplayName="TX2",LineWidth=2);
plot(tplot,out.tx3.signals.values(tstart_index:tstop_index),DisplayName="TX3",LineWidth=2);
xlabel("t,s");
ylabel("A, V");
legend();