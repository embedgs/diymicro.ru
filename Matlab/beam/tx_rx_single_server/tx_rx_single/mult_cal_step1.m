% clear;
% clf;
% 
% %input source setup
% fin = 11.3e9;
% 
% phase_in= 0;
% nsamples = 1024; %per period
% nperiods = 8;  %more better for delay measurements    
% total_n_samples = nsamples*nperiods;
% stoptime = nperiods/fin;
% vin = 0.02;
% %end of input source setup
% 
% rng("shuffle");
% 
% mdl = 'mult_calibration.slx';
% 
% 
% %%configure mult inputs
% mult0_b0 = 0;
% mult0_b1 = 0;
% mult1_b0 = 1;
% mult1_b1 = 1;
% mult2_b0 = 1;
% mult2_b1 = 1;
% mult3_b0 = 1;
% mult3_b1 = 1;
% %%------------------end of mult configuration-------------------
% 
% 
% %------------------mismatches section -------------------------
% mc_tx_vga_en = 1;                       %random vga gains or not
% mc_tx_mult_gain_en = 1;                 %mult gain k mismatches
% mc_tx_mult_delay_en = 1;                %mult delay errors 
%------------------end of mismatches enable/disable section----




%------------gains of tx paths - irrelevant for the current setup
tx_gain_vga = dBconversion(20);         %nominal vga gain
tx0_gain_vga = tx_gain_vga + tx_gain_vga*mc_tx_vga_en*rand_1(0.2);
tx1_gain_vga = tx_gain_vga + tx_gain_vga*mc_tx_vga_en*rand_1(0.2);
tx2_gain_vga = tx_gain_vga + tx_gain_vga*mc_tx_vga_en*rand_1(0.2);
tx3_gain_vga = tx_gain_vga + tx_gain_vga*mc_tx_vga_en*rand_1(0.2);
%--------------------end of tx paths gains settings

%-----------------------------multiplier gains-----------------
tx_mult_gain_min = 0.1;     %minimum possible k of the mult
tx_mult_gain_step = 0.025;  %typical adjustment step for the mult

%assume we have 3 bits to adjust mult k
mult0_k_values=[];
mult1_k_values=[];
mult2_k_values=[];
mult3_k_values=[];

for (i=1:1:8)
    tx_mult_gain_min1 = tx_mult_gain_min+tx_mult_gain_min*rand_1(0.2)*mc_tx_mult_gain_en;
    tx_mult_gain_step1 = tx_mult_gain_step*(i-1)+tx_mult_gain_step*rand_1(0.2)*mc_tx_mult_gain_en;
    temp1 = tx_mult_gain_min1 + tx_mult_gain_step1;
    
    tx_mult_gain_min2 = tx_mult_gain_min+tx_mult_gain_min*rand_1(0.2)*mc_tx_mult_gain_en;
    tx_mult_gain_step2 = tx_mult_gain_step*(i-1)+tx_mult_gain_step*rand_1(0.2)*mc_tx_mult_gain_en;
    temp2 = tx_mult_gain_min2 + tx_mult_gain_step2;    
    
    tx_mult_gain_min3 = tx_mult_gain_min+tx_mult_gain_min*rand_1(0.2)*mc_tx_mult_gain_en;
    tx_mult_gain_step3 = tx_mult_gain_step*(i-1)+tx_mult_gain_step*rand_1(0.2)*mc_tx_mult_gain_en;
    temp3 = tx_mult_gain_min3 + tx_mult_gain_step3;    
    
    tx_mult_gain_min4 = tx_mult_gain_min+tx_mult_gain_min*rand_1(0.2)*mc_tx_mult_gain_en;
    tx_mult_gain_step4 = tx_mult_gain_step*(i-1)+tx_mult_gain_step*rand_1(0.2)*mc_tx_mult_gain_en;
    temp4 = tx_mult_gain_min4 + tx_mult_gain_step4;   

    mult0_k_values=[mult0_k_values,temp1];
    mult1_k_values=[mult1_k_values,temp2];
    mult2_k_values=[mult2_k_values,temp3];
    mult3_k_values=[mult3_k_values,temp4];

end

mult0_k = mult0_k_values(5);
mult1_k = mult1_k_values(5);
mult2_k = mult2_k_values(5);
mult3_k = mult3_k_values(5);
%------------------end of multiplier gains-------------------------

%------------------------------------------------------------------
%------------------multiplier delay error-------------------------
mult_delay_error_max = 5e-12;           %we expect up to 5ps
mult0_delay_error = rand(1)*mult_delay_error_max*mc_tx_mult_delay_en;
mult1_delay_error = rand(1)*mult_delay_error_max*mc_tx_mult_delay_en;
mult2_delay_error = rand(1)*mult_delay_error_max*mc_tx_mult_delay_en;
mult3_delay_error = rand(1)*mult_delay_error_max*mc_tx_mult_delay_en;
%------------------end of multiplier delay error introduction----


%------------------multiplier delay error calibration------------------------
mult0_del_calib_values=[];
mult1_del_calib_values=[];
mult2_del_calib_values=[];
mult3_del_calib_values=[];

mult_delay_calibration_step = 1e-12;            %the correction of the phase step

%------------------processing the very first code to avoid the negative delay---------------------------------------------------------------------

    %first adjustment value either 0, either positive mc 
    mult0_del_calib_values=[mult0_del_calib_values,(0+abs(mult_delay_calibration_step*mc_tx_mult_delay_en*rand_1(0.5)))];
    mult1_del_calib_values=[mult1_del_calib_values,(0+abs(mult_delay_calibration_step*mc_tx_mult_delay_en*rand_1(0.5)))];
    mult2_del_calib_values=[mult2_del_calib_values,(0+abs(mult_delay_calibration_step*mc_tx_mult_delay_en*rand_1(0.5)))];
    mult3_del_calib_values=[mult3_del_calib_values,(0+abs(mult_delay_calibration_step*mc_tx_mult_delay_en*rand_1(0.5)))];

for (i=2:1:8)
    %(i-1)*mult_delay_calibration_step+mult_delay_calibration_step*mc_tx_mult_delay_en*rand_1(0.6)
    mult0_del_calib_values=[mult0_del_calib_values,((i-1)*mult_delay_calibration_step+mult_delay_calibration_step*mc_tx_mult_delay_en*rand_1(0.5))];
    mult1_del_calib_values=[mult1_del_calib_values,((i-1)*mult_delay_calibration_step+mult_delay_calibration_step*mc_tx_mult_delay_en*rand_1(0.5))];
    mult2_del_calib_values=[mult2_del_calib_values,((i-1)*mult_delay_calibration_step+mult_delay_calibration_step*mc_tx_mult_delay_en*rand_1(0.5))];
    mult3_del_calib_values=[mult3_del_calib_values,((i-1)*mult_delay_calibration_step+mult_delay_calibration_step*mc_tx_mult_delay_en*rand_1(0.5))];
end


%by default we don't have any correction introduced
mult0_delay_calibration = mult0_del_calib_values(1);
mult1_delay_calibration = mult1_del_calib_values(1);
mult2_delay_calibration = mult2_del_calib_values(1);
mult3_delay_calibration = mult3_del_calib_values(1);
%-----------------end of multiplier delay error calibration adjustment-----------------------------------------------------------------------------


%------------singe sim-------------------------
%out = sim(mdl);




%-----------------sweep simulation----------------------------------
mult0_avg_values=[];
mult1_avg_values=[];
mult2_avg_values=[];
mult3_avg_values=[];

for (i=1:1:8)
   display("Step of sweeping");
   display(i);

   %------delay correction----------------------------
   mult0_delay_calibration = mult0_del_calib_values(i);
   mult1_delay_calibration = mult1_del_calib_values(i);
   mult2_delay_calibration = mult2_del_calib_values(i);
   mult3_delay_calibration = mult3_del_calib_values(i);


   out=sim(mdl);

   mult0_avg_value = out.mult0_avg.signals.values(length(out.mult0_avg.signals.values));
   mult1_avg_value = out.mult1_avg.signals.values(length(out.mult1_avg.signals.values));
   mult2_avg_value = out.mult2_avg.signals.values(length(out.mult2_avg.signals.values));
   mult3_avg_value = out.mult3_avg.signals.values(length(out.mult3_avg.signals.values));

   mult0_avg_values=[mult0_avg_values,mult0_avg_value];
   mult1_avg_values=[mult1_avg_values,mult1_avg_value];
   mult2_avg_values=[mult2_avg_values,mult2_avg_value];
   mult3_avg_values=[mult3_avg_values,mult3_avg_value];

end    

% plot(mult0_avg_values,linewidth=2,DisplayName="mult0 avg");
% hold on;
% grid on;
% plot(mult1_avg_values,linewidth=2,DisplayName="mult1 avg"); hold on;
% plot(mult2_avg_values,linewidth=2,DisplayName="mult2 avg"); hold on;
% plot(mult3_avg_values,linewidth=2,DisplayName="mult3 avg"); hold on;
% xlabel("correction code");
% ylabel("Product, V");
% legend();

%---------------------------end of sweep simulation----------------


%---------------------defining the proper correction code----------
[max_avg0,phase_correction0] = max(mult0_avg_values);
[max_avg1,phase_correction1] = max(mult1_avg_values);
[max_avg2,phase_correction2] = max(mult2_avg_values);
[max_avg3,phase_correction3] = max(mult3_avg_values); 


mult0_delay_calibration = mult0_del_calib_values(phase_correction0);
mult1_delay_calibration = mult1_del_calib_values(phase_correction1);
mult2_delay_calibration = mult2_del_calib_values(phase_correction2);
mult3_delay_calibration = mult3_del_calib_values(phase_correction3);


%-------------------determine the resulting error in fs-------------
 phase0_error = (mult0_delay_error - mult0_delay_calibration)*1e+15;
 phase1_error = (mult1_delay_error - mult1_delay_calibration)*1e+15;
 phase2_error = (mult2_delay_error - mult2_delay_calibration)*1e+15;
 phase3_error = (mult3_delay_error - mult3_delay_calibration)*1e+15;
%-------------------end determine the resulting error in fs---------


function K = dBconversion(x)
    K = 10^(x/20);
end

function Y = rand_1(x)
    Y = (rand(1)-0.5)*x;
end    