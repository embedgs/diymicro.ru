kfix = 1/0.004;
mult_values = kfix*mult_avg_values/2 +0.5;
plot(phase1_values,mult_values, LineWidth=2);
xlabel("Phase, degrees", FontSize=20)
ylabel("Multiplier output [normalized],V", FontSize=20)
hold on;
grid on;