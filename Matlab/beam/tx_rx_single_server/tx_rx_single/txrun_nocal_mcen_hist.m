clear;
clf;

rng("shuffle");
mdl = 'tx_calibration.slx';

input_source_setup;		%call for input source setup




%configure mult inputs
mult0_b0 = 1;
mult0_b1 = 1;
mult1_b0 = -1;
mult1_b1 = -1;
mult2_b0 = -1;
mult2_b1 = -1;
mult3_b0 = -1;
mult3_b1 = -1;
%------------------end of mult configuration-------------------

number_of_runs = 128;


%------------------mismatches section -------------------------
mc_tx_en = 1;                       %random vga gains or not
mc_tx_mult_gain_en = 1;                 %mult gain k mismatches
mc_tx_mult_delay_en = 1;                %mult delay errors 
%------------------end of mismatches enable/disable section----

mult_setup;				%mult setup

amplitudes_error = zeros(1,number_of_runs);
delay_errors = zeros(1,number_of_runs);


for (run=1:1:number_of_runs)
	display("MC sweep "+ run);
    tx_initial_setup;		%initial settings of tx
	 out=sim(mdl);
	 get_amplitudes_no_plot;
	 temp_max = max(tx_amplitudes);
     temp_min = min(tx_amplitudes);
     amplitudes_error(run) = temp_max - temp_min;
	 get_phases_no_plot;
	 delay_errors(run) = max(tx_phases)-min(tx_phases);

end


subplot(1,2,1);
histogram(amplitudes_error);
hold on;
xlabel("MAX amplitude mismatch");
title("mean="+mean(amplitudes_error)+", std. dev="+std(amplitudes_error));
legend();

subplot(1,2,2);
histogram(delay_errors);
hold on;
xlabel("MAX delay mismatch");
title("mean="+mean(delay_errors)+", std. dev="+std(delay_errors));
legend();