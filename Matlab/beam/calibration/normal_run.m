clear;
clf;

%simulator setup
Pdbm = -50;   %more convinient to have reference in dbm
fin = 10e9;
phase_in = 0;
nsamples = 512; %per period
nperiods = 32;
total_n_samples = nsamples*nperiods;
stoptime = nperiods/fin;
vin = sqrt(100*10^((Pdbm-30)/10));
mdl = 'amp_simple.slx';

lna_gain = 16;  %typical lna gain in db
%vga_gain = 32;  %typical vga gain in db
ps_gain = -6;   %typical ps gain in db

vga_gain=zeros(1,16);
for i=1:1:16
    vga_gain(i)=20+1*i;
end

ps_const_delay = 10e-12;
ps_phases=zeros(1,64);
for i = 1:1:64
    ps_phases(i)=ps_const_delay+(i-1)/(64*fin);
end


%parameter setup
rx0_lna_gain = dBconversion(lna_gain);
rx0_vga_gain = dBconversion(vga_gain(10));
rx0_ps_gain = dBconversion(ps_gain);
rx0_lna_delay = 15e-12;
rx0_vga_delay = 30e-12;
rx0_ps_delay = ps_phases(1);

%out = sim(mdl);
% rx0_out=[];
% disp("Entering to vga gain swepp...")
% for i=1:1:16
%     disp(i);
%     rx0_vga_gain = dBconversion(vga_gain(i));
%     out = sim(mdl);
%     rx0_out= [rx0_out,out.rx0.signals.values];
% end
% 
% t = out.tout;

%rx0_out = out.rx0.signals.values;

%plot(t,rx0_out);
%hold on;
%grid on;


function K = dBconversion(x)
    K = 10^(x/20);
end