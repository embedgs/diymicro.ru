subplot(1,2,1);
hold on;
grid on;
xlabel('t,s');
ylabel('Amplitude, V');
disp("Plotting the rx data...")
for i=1:1:16
    disp(i);
    subplot(1,2,1);
    hold on;
    grid on;
    plot(t,rx0_out(:,i));
    rx_gain(i)=20*log10(max(rx0_out(:,i))/vin);
end

subplot(1,2,2);
xlabel("gain code");
ylabel("Gain, dB");
hold on;
grid on;
plot(rx_gain,marker="o",color="red");