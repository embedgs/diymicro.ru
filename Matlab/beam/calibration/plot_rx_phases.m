tstart_index = total_n_samples - nsamples;
tstop_index = length(t);
tplot = t(tstart_index:tstop_index);


subplot(1,2,1);
hold on;
grid on;
xlabel('t,s');
ylabel('Amplitude, V');
disp("Plotting the rx data...")
step_ps=[];
step_ps_accumulated=[];
step_ps_accumulated=[step_ps_accumulated,0];
step_deg_accumulated=[];
step_deg_accumulated=[step_deg_accumulated,0];
for i=1:1:N
    disp(i);
    plot(tplot,rx0_out(tstart_index:tstop_index,i));
    hold on;
    grid on;
    if (i>1)
        [c,lags]=xcorr(rx0_out(:,i),rx0_out(:,(i-1)));
        [peak,lag_index]=max(abs(c));
        step_local_ps = 1e12*lags(lag_index)*stoptime/total_n_samples;
        step_local_deg = (fin*360)*(i-1)*step_local_ps*1e-12;
        step_deg_accumulated = [step_deg_accumulated,step_local_deg];
        step_ps=[step_ps,step_local_ps];
        step_ps_accumulated=[step_ps_accumulated,(i-1)*step_local_ps];
    end
end



subplot(1,2,2);
xlabel("phase interpolator code");
ylabel("Phase, deg");
hold on;
grid on;
plot(step_deg_accumulated,marker='o',color='red');





% plot(rx_gain,marker="o",color="red");

%[c,lags]=xcorr(rx0_out(:,2),rx0_out(:,1));
%plot(t,rx0_out(:,1));
%hold on;
%plot(t,rx0_out(:,2));
%[m,i] = max(abs(c));
%lags(i)*stoptime/total_n_samples