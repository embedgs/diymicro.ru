clear;
clf;

rng("shuffle");
mdl = 'tx_calibration.slx';

input_source_setup;		%call for input source setup

%------------------end of mult configuration-------------------

number_of_runs = 8;
amplitudes_error = zeros(1,number_of_runs);

%------------------mismatches section -------------------------
mc_tx_en = 1;                       %random vga gains or not
mc_tx_mult_gain_en = 1;                 %mult gain k mismatches
mc_tx_mult_delay_en = 1;                %mult delay errors 
%------------------end of mismatches enable/disable section----



for (run=1:1:number_of_runs)
	display("MC sweep "+ run);
	calibration_step3;
    amplitudes_error(run)=postcalibration_amplitudes_error;
end	


histogram(amplitudes_error);
hold on;
xlabel("MAX amplitude mismatch");
title("mean="+mean(amplitudes_error)+", std. dev="+std(amplitudes_error));
legend();