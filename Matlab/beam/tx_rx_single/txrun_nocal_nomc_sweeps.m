clear;
clf;

rng("shuffle");
mdl = 'tx_calibration.slx';

input_source_setup;		%call for input source setup




%configure mult inputs
mult0_b0 = 0;
mult0_b1 = 0;
mult1_b0 = 1;
mult1_b1 = 1;
mult2_b0 = 1;
mult2_b1 = 1;
mult3_b0 = 1;
mult3_b1 = 1;
%------------------end of mult configuration-------------------

number_of_runs = 1;


%------------------mismatches section -------------------------
mc_tx_en = 0;                       %random vga gains or not
mc_tx_mult_gain_en = 0;                 %mult gain k mismatches
mc_tx_mult_delay_en = 0;                %mult delay errors 
%------------------end of mismatches enable/disable section----

mult_setup;				%mult setup
tx_initial_setup;		%initial settings of tx


 %getting ideal vga gain curves
 tx0_gains = zeros(1,8);
 tx1_gains = zeros(1,8);
 tx2_gains = zeros(1,8);
 tx3_gains = zeros(1,8);
 


tx0_gain_vga = tx0_gain_vga_values(16);
tx1_gain_vga = tx1_gain_vga_values(16);
tx2_gain_vga = tx2_gain_vga_values(16);
tx3_gain_vga = tx3_gain_vga_values(16);

out = sim(mdl);
get_amplitudes_no_plot;
tx0_ref_amplitude = tx_amplitudes(1);
tx1_ref_amplitude = tx_amplitudes(2);
tx2_ref_amplitude = tx_amplitudes(3);
tx3_ref_amplitude = tx_amplitudes(4);

tx0_gains(1) = 0;
tx1_gains(1) = 0;
tx2_gains(1) = 0;
tx3_gains(1) = 0;

 for (i=1:1:7)
     display("sweeping amplitudes");
     tx0_gain_vga = tx0_gain_vga_values(16+2*i);
     tx1_gain_vga = tx1_gain_vga_values(16+2*i);
     tx2_gain_vga = tx2_gain_vga_values(16+2*i);
     tx3_gain_vga = tx3_gain_vga_values(16+2*i);
 
     out = sim(mdl);
     get_amplitudes_no_plot;
     tx0_gains(i+1) = convert_to_dB(tx_amplitudes(1)/tx0_ref_amplitude);
     tx1_gains(i+1) = convert_to_dB(tx_amplitudes(2)/tx1_ref_amplitude);
     tx2_gains(i+1) = convert_to_dB(tx_amplitudes(3)/tx2_ref_amplitude);
     tx3_gains(i+1) = convert_to_dB(tx_amplitudes(4)/tx3_ref_amplitude);
 
 end    
     % back to mid settings of the vga
     tx0_gain_vga = tx0_gain_vga_values(24);
     tx1_gain_vga = tx1_gain_vga_values(24);
     tx2_gain_vga = tx2_gain_vga_values(24);
     tx3_gain_vga = tx3_gain_vga_values(24);

%getting ideal ps delay steps
tx0_ps_steps = zeros(1,8);
tx1_ps_steps = zeros(1,8);
tx2_ps_steps = zeros(1,8);
tx3_ps_steps = zeros(1,8);


tx0_ps_steps(1)=0;
tx1_ps_steps(1)=0;
tx2_ps_steps(1)=0;
tx3_ps_steps(1)=0;


tx0_delay_ps = tx0_delay_ps_values(48);
tx1_delay_ps = tx1_delay_ps_values(48);
tx2_delay_ps = tx2_delay_ps_values(48);
tx3_delay_ps = tx3_delay_ps_values(48);

out = sim(mdl);

tx0_ref = out.tx0.signals.values;
tx1_ref = out.tx1.signals.values;
tx2_ref = out.tx2.signals.values;
tx3_ref = out.tx3.signals.values;


for (i=1:1:7)
    display("sweeping phases");
    tx0_delay_ps = tx0_delay_ps_values(48+2*i);
    tx1_delay_ps = tx1_delay_ps_values(48+2*i);
    tx2_delay_ps = tx2_delay_ps_values(48+2*i);
    tx3_delay_ps = tx3_delay_ps_values(48+2*i);
    
    out = sim(mdl);

    tx0_ps_steps(i+1) = measure_delay_ps(tx0_ref,out.tx0.signals.values,stoptime,total_n_samples);
    tx1_ps_steps(i+1) = measure_delay_ps(tx1_ref,out.tx1.signals.values,stoptime,total_n_samples);
    tx2_ps_steps(i+1) = measure_delay_ps(tx2_ref,out.tx2.signals.values,stoptime,total_n_samples);
    tx3_ps_steps(i+1) = measure_delay_ps(tx3_ref,out.tx3.signals.values,stoptime,total_n_samples);

end  

%plot gain and phase steps
subplot(1,2,1);
plot(tx0_gains,LineWidth=2,DisplayName="TX0");
hold on;
grid on;
plot(tx1_gains,LineWidth=2,DisplayName="TX1");
plot(tx2_gains,LineWidth=2,DisplayName="TX2");
plot(tx3_gains,LineWidth=2,DisplayName="TX3");
xlabel("vga code");
ylabel("Gain,dB");
legend();

subplot(1,2,2);
plot(tx0_ps_steps,LineWidth=2,DisplayName="TX0");
hold on;
grid on;
plot(tx1_ps_steps,LineWidth=2,DisplayName="TX1");
plot(tx2_ps_steps,LineWidth=2,DisplayName="TX2");
plot(tx3_ps_steps,LineWidth=2,DisplayName="TX3");
xlabel("PS code");
ylabel("PS step in ps");
legend();




function K = measure_delay_ps(x,y,tstop,nsamples)
    [c,lags]=xcorr(y,x);
    [peak,lag_index]=max(abs(c));
    K = 1e12*lags(lag_index)*tstop/nsamples;
end


function T = convert_to_dB(x)
    T = 20*log10(x);
end    