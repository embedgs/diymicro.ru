clear;
clf;

%input source setup
Pdbm = -10;
fin = 10e9;
phase_in = 0;
nsamples = 512; %per period
nperiods = 8;  %more better for delay measurements    
total_n_samples = nsamples*nperiods;
stoptime = nperiods/fin;
vin = sqrt(100*10^((Pdbm-30)/10));
mdl = 'tx_rx_single.slx';

tx0_mode = 1;

%constant delay setup
txrx_delay_switch_in = 20e-12;

tx_delay_ps = 20e-12;
tx_delay_vga = 20e-12;
tx_delay_pa = 20e-12;

rx_delay_lna = 20e-12;
rx_delay_ps = 20e-12;
rx_delay_vga = 20e-12;

%variable delay setup
rx_ps_phases=zeros(1,64);
for i = 1:1:64
    rx_ps_phases(i)=rx_delay_ps+(i-1)/(64*fin);
end

tx_ps_phases=zeros(1,64);
for i = 1:1:64
    tx_ps_phases(i)=tx_delay_ps+(i-1)/(64*fin);
end


%constant gain setup
tx_gain_ps = -6;
%tx0_gain_vga = 16;
tx_gain_pa = 10;

rx_gain_lna = 16;  %typical lna gain in db
%rx0_gain_vga = 32;  %typical vga gain in db
rx_gain_ps = -6;   %typical ps gain in db

rx_gain_vga=zeros(1,16);
tx_gain_vga=zeros(1,16);
for i=1:1:16
    rx_gain_vga(i)=10+1*i;
    tx_gain_vga(i)=-20+2.5*(i-1);
end


%passing settings to the simulink model
txrx0_delay_switch_in = txrx_delay_switch_in;

tx0_delay_ps = tx_ps_phases(1);
tx0_delay_vga = tx_delay_vga;
tx0_delay_pa = tx_delay_pa;

rx0_delay_lna = rx_delay_lna;
rx0_delay_ps = rx_ps_phases(1);
rx0_delay_vga = rx_delay_vga;

rx0_gain_lna = dBconversion(rx_gain_lna);
rx0_gain_vga = dBconversion(rx_gain_vga(8));
rx0_gain_ps = dBconversion(rx_gain_ps);

tx0_gain_pa = dBconversion(tx_gain_pa);
tx0_gain_vga = dBconversion(tx_gain_vga(1));
tx0_gain_ps = dBconversion(tx_gain_ps);




out = sim(mdl);

t = out.tout;
tx0_in_v = out.tx0_in.signals.values;
tx0_out_v = out.tx0_out.signals.values;
rx0_out_v = out.rx0_out.signals.values;

function K = dBconversion(x)
    K = 10^(x/20);
end