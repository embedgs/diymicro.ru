

%configure mult inputs tx*tx, we analyzing amplitudes
mult0_b0 = 1;
mult0_b1 = 1;
mult1_b0 = -1;
mult1_b1 = -1;
mult2_b0 = -1;
mult2_b1 = -1;
mult3_b0 = -1;
mult3_b1 = -1;
%------------------end of mult configuration-------------------

display("Step 3 - aligning the output amplitudes");
mult_setup;				%mult setup
tx_initial_setup;		%initial settings of tx
out = sim(mdl);

get_mult_averages_no_plot;
%==============OUR REFERENCE VALUE==============================
ref_mult_avg = tx_mult_averages(1);

get_amplitudes_no_plot;
initial_amplitudes_error = max(tx_amplitudes)- min(tx_amplitudes);

current_avg_error_1 = abs(ref_mult_avg - tx_mult_averages(2));
current_avg_error_2 = abs(ref_mult_avg - tx_mult_averages(3));
current_avg_error_3 = abs(ref_mult_avg - tx_mult_averages(4));

current_vga_code_1 = 24;
current_vga_code_2 = 24;
current_vga_code_3 = 24;

mult_avg_values_1 = zeros(1,16);
mult_avg_values_2 = zeros(1,16);
mult_avg_values_3 = zeros(1,16);



for (i=1:1:16)
	display("sweeping vga gain, step" + i);
	%tx0_gain_vga = tx0_gain_vga_values(15+i);
    tx1_gain_vga = tx1_gain_vga_values(15+i);
	tx2_gain_vga = tx2_gain_vga_values(15+i);
	tx3_gain_vga = tx3_gain_vga_values(15+i);

	out=sim(mdl);
    get_mult_averages_no_plot;

	temp_avg_error_1 = abs(ref_mult_avg - tx_mult_averages(2));
	temp_avg_error_2 = abs(ref_mult_avg - tx_mult_averages(3));
	temp_avg_error_3 = abs(ref_mult_avg - tx_mult_averages(4));
   

    mult_avg_values_0(i) = tx_mult_averages(1);
    mult_avg_values_1(i) = tx_mult_averages(2);
    mult_avg_values_2(i) = tx_mult_averages(3);
    mult_avg_values_3(i) = tx_mult_averages(4);
    
	if (temp_avg_error_1<=current_avg_error_1)
		current_avg_error_1 = temp_avg_error_1;
		current_vga_code_1 = 15+i;
	end	


	if (temp_avg_error_2<=current_avg_error_2)
		current_avg_error_2 = temp_avg_error_2;
		current_vga_code_2 = 15+i;
	end	


	if (temp_avg_error_3<=current_avg_error_3)
		current_avg_error_3 = temp_avg_error_3;
		current_vga_code_3 = 15+i;
	end		

end	

% %===================one more run with calibrated vga codes============
tx1_gain_vga = tx1_gain_vga_values(current_vga_code_1);
tx2_gain_vga = tx2_gain_vga_values(current_vga_code_2);
tx3_gain_vga = tx3_gain_vga_values(current_vga_code_3);

out = sim(mdl);
get_amplitudes_no_plot;
postcalibration_amplitudes_error = max(tx_amplitudes)- min(tx_amplitudes);