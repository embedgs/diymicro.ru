%==========================TX blocks setup====================================


%--------------------------Constant delays setup------------------------------
tx_delay_switch_in = 5e-12;
tx_delay_vga = 10e-12;
tx_delay_pa = 10e-12;

tx0_delay_switch_in = tx_delay_switch_in+mc_tx_en*rand_1(0.2)*tx_delay_switch_in;
tx1_delay_switch_in = tx_delay_switch_in+mc_tx_en*rand_1(0.2)*tx_delay_switch_in;
tx2_delay_switch_in = tx_delay_switch_in+mc_tx_en*rand_1(0.2)*tx_delay_switch_in;
tx3_delay_switch_in = tx_delay_switch_in+mc_tx_en*rand_1(0.2)*tx_delay_switch_in;

tx0_delay_vga = tx_delay_vga+mc_tx_en*rand_1(0.2)*tx_delay_vga;
tx1_delay_vga = tx_delay_vga+mc_tx_en*rand_1(0.2)*tx_delay_vga;
tx2_delay_vga = tx_delay_vga+mc_tx_en*rand_1(0.2)*tx_delay_vga;
tx3_delay_vga = tx_delay_vga+mc_tx_en*rand_1(0.2)*tx_delay_vga;

tx0_delay_pa = tx_delay_pa+mc_tx_en*rand_1(0.2)*tx_delay_pa;
tx1_delay_pa = tx_delay_pa+mc_tx_en*rand_1(0.2)*tx_delay_pa;
tx2_delay_pa = tx_delay_pa+mc_tx_en*rand_1(0.2)*tx_delay_pa;
tx3_delay_pa = tx_delay_pa+mc_tx_en*rand_1(0.2)*tx_delay_pa;
%--------------------------End of constant delays setup-------------------------

%--------------------------Constant gains setup---------------------------------
tx_gain_pa = 6;
tx_gain_ps = -2;

tx0_gain_pa = dBconversion(tx_gain_pa+mc_tx_en*rand_1(0.2)*tx_gain_pa);
tx1_gain_pa = dBconversion(tx_gain_pa+mc_tx_en*rand_1(0.2)*tx_gain_pa);
tx2_gain_pa = dBconversion(tx_gain_pa+mc_tx_en*rand_1(0.2)*tx_gain_pa);
tx3_gain_pa = dBconversion(tx_gain_pa+mc_tx_en*rand_1(0.2)*tx_gain_pa);

tx0_gain_ps = dBconversion(tx_gain_ps+mc_tx_en*rand_1(0.2)*tx_gain_ps);
tx1_gain_ps = dBconversion(tx_gain_ps+mc_tx_en*rand_1(0.2)*tx_gain_ps);
tx2_gain_ps = dBconversion(tx_gain_ps+mc_tx_en*rand_1(0.2)*tx_gain_ps);
tx3_gain_ps = dBconversion(tx_gain_ps+mc_tx_en*rand_1(0.2)*tx_gain_ps);
%-------------------------End of constant gain setup-----------------------------


%-------------------------PS steps setup-----------------------------------------
tx_delay_ps_error = 500e-15;
tx_delay_ps_step = tin/128;
tx_delay_ps = 10e-12;

tx0_delay_ps_values = zeros(1,128);
tx1_delay_ps_values = zeros(1,128);
tx2_delay_ps_values = zeros(1,128);
tx3_delay_ps_values = zeros(1,128);

for (i=1:1:128)
	tx0_delay_ps_values(i)= tx_delay_ps + tx_delay_ps_step*(i-1) + tx_delay_ps_error*rand_1(1)*mc_tx_en;
	tx1_delay_ps_values(i)= tx_delay_ps + tx_delay_ps_step*(i-1) + tx_delay_ps_error*rand_1(1)*mc_tx_en;
	tx2_delay_ps_values(i)= tx_delay_ps + tx_delay_ps_step*(i-1) + tx_delay_ps_error*rand_1(1)*mc_tx_en;
	tx3_delay_ps_values(i)= tx_delay_ps + tx_delay_ps_step*(i-1) + tx_delay_ps_error*rand_1(1)*mc_tx_en;
end

tx0_delay_ps = tx0_delay_ps_values(64);
tx1_delay_ps = tx1_delay_ps_values(64);
tx2_delay_ps = tx2_delay_ps_values(64);
tx3_delay_ps = tx3_delay_ps_values(64);

%------------------------end of PS steps setup-----------------------------------


%------------------------VGA gains setup-----------------------------------------
tx_gain_vga = -6;
tx_gain_vga_step = 1;
tx_gain_vga_error = 1;

tx0_gain_vga_values = zeros(1,48);
tx1_gain_vga_values = zeros(1,48);
tx2_gain_vga_values = zeros(1,48);
tx3_gain_vga_values = zeros(1,48);

for (i=1:1:48)
	tx0_gain_vga_values(i) = dBconversion(tx_gain_vga + tx_gain_vga_step*(i-1) + mc_tx_en*rand_1(1)*tx_gain_vga_error);
	tx1_gain_vga_values(i) = dBconversion(tx_gain_vga + tx_gain_vga_step*(i-1) + mc_tx_en*rand_1(1)*tx_gain_vga_error);
	tx2_gain_vga_values(i) = dBconversion(tx_gain_vga + tx_gain_vga_step*(i-1) + mc_tx_en*rand_1(1)*tx_gain_vga_error);
	tx3_gain_vga_values(i) = dBconversion(tx_gain_vga + tx_gain_vga_step*(i-1) + mc_tx_en*rand_1(1)*tx_gain_vga_error);
end

tx0_gain_vga = tx0_gain_vga_values(24);
tx1_gain_vga = tx1_gain_vga_values(24);
tx2_gain_vga = tx2_gain_vga_values(24);
tx3_gain_vga = tx3_gain_vga_values(24);


%------------------------end of VGA gain setup--------------------------------------


function K = dBconversion(x)
    K = 10^(x/20);
end

function Y = rand_1(x)
    Y = (rand(1)-0.5)*x;
end    