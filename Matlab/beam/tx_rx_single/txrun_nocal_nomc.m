clear;
clf;

rng("shuffle");
mdl = 'tx_calibration.slx';

input_source_setup;		%call for input source setup




%configure mult inputs
mult0_b0 = 0;
mult0_b1 = 0;
mult1_b0 = 1;
mult1_b1 = 1;
mult2_b0 = 1;
mult2_b1 = 1;
mult3_b0 = 1;
mult3_b1 = 1;
%------------------end of mult configuration-------------------

number_of_runs = 1;


%------------------mismatches section -------------------------
mc_tx_en = 0;                       %random vga gains or not
mc_tx_mult_gain_en = 0;                 %mult gain k mismatches
mc_tx_mult_delay_en = 0;                %mult delay errors 
%------------------end of mismatches enable/disable section----

mult_setup;				%mult setup
tx_initial_setup;		%initial settings of tx

out=sim(mdl);

%plot 4 graphs
subplot(2,2,1);
plot_4_tx;
subplot(2,2,2);
get_amplitudes;
subplot(2,2,3);
get_phases;
subplot(2,2,4);
get_mult_averages;