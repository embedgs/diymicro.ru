%==================MULTIPLIER SETUP with errors================================

%------------------------------------------------------------------------------
%------------------multiplier delay error from step 1--------------------------
%------------------we don't need correction coefficients-----------------------
mult0_delay_calibration = 5e-12;
mult1_delay_calibration = 5e-12;
mult2_delay_calibration = 5e-12;
mult3_delay_calibration = 5e-12;

mult0_delay_error = (+450e-15)*mc_tx_mult_delay_en+5e-12;
mult1_delay_error = (-0.92e-12)*mc_tx_mult_delay_en+5e-12;
mult2_delay_error = (-120e-15)*mc_tx_mult_delay_en+5e-12;
mult3_delay_error = (0.945e-12)*mc_tx_mult_delay_en+5e-12;
%------------------------------------------------------------------------------
%-----------------end of phase error introduction------------------------------


%------------------------------------------------------------------------------
%--------------------------multiplier gains from step 2------------------------
mult_k = 0.2;							%nominal gain
mult0_k = mult_k+0.005*mc_tx_mult_delay_en;
mult1_k = mult_k-0.021*mc_tx_mult_delay_en;
mult2_k = mult_k+0.021*mc_tx_mult_delay_en;
mult3_k = mult_k-0.007*mc_tx_mult_delay_en;
%--------------------------end of multiplier gain-------------------------------



