%------------------------------------------------------------------------------
%------------------multiplier delay error from step 1--------------------------
%------------------we don't need correction coefficients-----------------------
mult0_delay_calibration = 5e-12;
mult1_delay_calibration = 5e-12;
mult2_delay_calibration = 5e-12;
mult3_delay_calibration = 5e-12;

mult0_delay_error = (+450e-15)*mc_tx_mult_delay_en+5e-12;
mult1_delay_error = (-0.92e-12)*mc_tx_mult_delay_en+5e-12;
mult2_delay_error = (-120e-15)*mc_tx_mult_delay_en+5e-12;
mult3_delay_error = (0.945e-12)*mc_tx_mult_delay_en+5e-12;
%------------------------------------------------------------------------------
%-----------------end of phase error introduction------------------------------


%------------------------------------------------------------------------------
%-----------------TX VGA mismatches and gains----------------------------------
tx_gain_vga_min = dBconversion(15)+dBconversion(15)*mc_tx_vga_en*rand_1(0.2);         
tx_vga_gain_step = dBconversion(2);

tx0_vga_gain_values=[];
tx1_vga_gain_values=[];
tx2_vga_gain_values=[];
tx3_vga_gain_values=[];


for (i=1:1:8)
	temp0 = tx_gain_vga_min + tx_vga_gain_step*(i-1)+mc_tx_vga_en*tx_vga_gain_step*rand_1(0.2);  
	temp1 = tx_gain_vga_min + tx_vga_gain_step*(i-1)+mc_tx_vga_en*tx_vga_gain_step*rand_1(0.2);
	temp2 = tx_gain_vga_min + tx_vga_gain_step*(i-1)+mc_tx_vga_en*tx_vga_gain_step*rand_1(0.2);
	temp3 = tx_gain_vga_min + tx_vga_gain_step*(i-1)+mc_tx_vga_en*tx_vga_gain_step*rand_1(0.2);

	tx0_vga_gain_values=[tx0_vga_gain_values,temp0];
	tx1_vga_gain_values=[tx1_vga_gain_values,temp1];
	tx2_vga_gain_values=[tx2_vga_gain_values,temp2];
	tx3_vga_gain_values=[tx3_vga_gain_values,temp3];

end


tx0_gain_vga = tx0_vga_gain_values(3);
tx1_gain_vga = tx1_vga_gain_values(3);
tx2_gain_vga = tx2_vga_gain_values(3);
tx3_gain_vga = tx3_vga_gain_values(3);
%--------------------end of tx paths gains settings----------------------------
%------------------------------------------------------------------------------



%--------------------------multiplier gains------------------------------------
%------------------------------------------------------------------------------
tx_mult_gain_min = 0.16;     %minimum possible k of the mult
tx_mult_gain_step = 0.01;  %typical adjustment step for the mult
tx_mult_gain_reference = tx_mult_gain_min + 4*tx_mult_gain_step;

%assume we have 3 bits to adjust mult k
mult0_k_values=[];
mult1_k_values=[];
mult2_k_values=[];
mult3_k_values=[];

for (i=1:1:8)
    tx_mult_gain_min1 = tx_mult_gain_min+tx_mult_gain_min*rand_1(0.2)*mc_tx_mult_gain_en;
    tx_mult_gain_step1 = tx_mult_gain_step*(i-1)+tx_mult_gain_step*rand_1(0.2)*mc_tx_mult_gain_en;
    temp1 = tx_mult_gain_min1 + tx_mult_gain_step1;
    
    tx_mult_gain_min2 = tx_mult_gain_min+tx_mult_gain_min*rand_1(0.2)*mc_tx_mult_gain_en;
    tx_mult_gain_step2 = tx_mult_gain_step*(i-1)+tx_mult_gain_step*rand_1(0.2)*mc_tx_mult_gain_en;
    temp2 = tx_mult_gain_min2 + tx_mult_gain_step2;    
    
    tx_mult_gain_min3 = tx_mult_gain_min+tx_mult_gain_min*rand_1(0.2)*mc_tx_mult_gain_en;
    tx_mult_gain_step3 = tx_mult_gain_step*(i-1)+tx_mult_gain_step*rand_1(0.2)*mc_tx_mult_gain_en;
    temp3 = tx_mult_gain_min3 + tx_mult_gain_step3;    
    
    tx_mult_gain_min4 = tx_mult_gain_min+tx_mult_gain_min*rand_1(0.2)*mc_tx_mult_gain_en;
    tx_mult_gain_step4 = tx_mult_gain_step*(i-1)+tx_mult_gain_step*rand_1(0.2)*mc_tx_mult_gain_en;
    temp4 = tx_mult_gain_min4 + tx_mult_gain_step4;   

    mult0_k_values=[mult0_k_values,temp1];
    mult1_k_values=[mult1_k_values,temp2];
    mult2_k_values=[mult2_k_values,temp3];
    mult3_k_values=[mult3_k_values,temp4];

end

mult0_k = mult0_k_values(5);
mult1_k = mult1_k_values(5);
mult2_k = mult2_k_values(5);
mult3_k = mult3_k_values(5);
%------------------------------end of multiplier gains-------------------------
%------------------------------------------------------------------------------



%------------------------------mc sim w/o calibration--------------------------
%mult0_k_error = mult0_k - tx_mult_gain_reference;
%mult1_k_error = mult1_k - tx_mult_gain_reference;
%mult2_k_error = mult2_k - tx_mult_gain_reference;
%mult3_k_error = mult3_k - tx_mult_gain_reference;
%------------------------------------------------------------------------------

%===============================step 2 calibration=============================
%-------------------------------first run-------------------------------------
out=sim(mdl);
%-------------------------------getting reference value-----------------------
mult0_avg_value = out.mult0_avg.signals.values(length(out.mult0_avg.signals.values));
mult0_avg_value_ref = mult0_avg_value;


mult1_avg_values=[];
mult2_avg_values=[];
mult3_avg_values=[];


%------------------------------step 2.1--------------------------------------
display("Entering the mult1 k sweep");
mult1_b0 = 0;
mult1_b1 = 0;
for (i=1:1:8)
   
   mult1_k = mult1_k_values(i);
   out = sim(mdl);
   mult1_avg_value = out.mult1_avg.signals.values(length(out.mult1_avg.signals.values));
   mult1_avg_values=[mult1_avg_values,mult1_avg_value];
end

temp_avg=abs(mult1_avg_values - mult0_avg_value);
[temp_min,temp_index]=min(temp_avg)

%----------------------------getting mult1 k value---------------------------
mult1_k = mult1_k_values(temp_index);
mult1_avg_value_ref = mult1_avg_values(temp_index);


%------------------------------step 2.2: calibrate tx1 vga gain to get same mult1 avg-------------------------------------

display("Entering the TX1 vga gain sweep");
mult1_b0 = 1;
mult1_b1 = 1;
mult1_avg_values=[];
for (i=1:1:8)
    tx1_gain_vga = tx1_vga_gain_values(i);
    out=sim(mdl);
    mult1_avg_value = out.mult1_avg.signals.values(length(out.mult1_avg.signals.values));
    mult1_avg_values=[mult1_avg_values,mult1_avg_value];

end    

%-------------------------getting tx1 vga gain value-----------------------------------------
temp_avg=abs(mult1_avg_values - mult0_avg_value_ref);
[temp_min,temp_index]=min(temp_avg)
tx1_gain_vga = tx1_vga_gain_values(temp_index);

%-----------------------------step 2.3: calibrating of mult2 based on tx1 -------------------------------------
display("Entering the mult2 k sweep");
mult2_b0 = 0;
mult2_b1 = 0;
for (i=1:1:8)
   
   mult2_k = mult2_k_values(i);
   out = sim(mdl);
   mult2_avg_value = out.mult2_avg.signals.values(length(out.mult2_avg.signals.values));
   mult2_avg_values=[mult2_avg_values,mult2_avg_value];
end

temp_avg=abs(mult2_avg_values - mult0_avg_value_ref);
[temp_min,temp_index]=min(temp_avg)

%----------------------------getting mult2 k value---------------------------
mult2_k = mult2_k_values(temp_index);
mult2_avg_value_ref = mult2_avg_values(temp_index);


%------------------------------step 2.4: calibrate tx3 vga gain to get same mult0 avg-------------------------------------

display("Entering the TX3 vga gain sweep");
mult0_b0 = 1;
mult0_b1 = 1;
mult0_avg_values=[];
for (i=1:1:8)
    tx3_gain_vga = tx3_vga_gain_values(i);
    out=sim(mdl);
    mult0_avg_value = out.mult0_avg.signals.values(length(out.mult0_avg.signals.values));
    mult0_avg_values=[mult0_avg_values,mult0_avg_value];

end    

%-------------------------getting tx2 vga gain value-----------------------------------------
temp_avg=abs(mult0_avg_values - mult0_avg_value_ref);
[temp_min,temp_index]=min(temp_avg)
tx3_gain_vga = tx3_vga_gain_values(temp_index);


%-----------------------------step 2.5: calibrating of mult3 based on tx3 -------------------------------------
display("Entering the mult3 k sweep");
mult3_b0 = 1;
mult3_b1 = 1;
for (i=1:1:8)
   
   mult3_k = mult3_k_values(i);
   out = sim(mdl);
   mult3_avg_value = out.mult3_avg.signals.values(length(out.mult3_avg.signals.values));
   mult3_avg_values=[mult3_avg_values,mult3_avg_value];
end

temp_avg=abs(mult3_avg_values - mult0_avg_value_ref);
[temp_min,temp_index]=min(temp_avg)

%----------------------------getting mult2 k value---------------------------
mult3_k = mult3_k_values(temp_index);
mult3_avg_value_ref = mult3_avg_values(temp_index);


mult1_k_error = mult1_k - mult0_k;
mult2_k_error = mult2_k - mult0_k;
mult3_k_error = mult3_k - mult0_k;










function K = dBconversion(x)
    K = 10^(x/20);
end

function Y = rand_1(x)
    Y = (rand(1)-0.5)*x;
end    