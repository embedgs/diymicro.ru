tx_mult_averages = zeros(1,4);

tx_mult_averages(1) = out.mult0_avg.signals.values(length(out.mult0_avg.signals.values));
tx_mult_averages(2) = out.mult1_avg.signals.values(length(out.mult1_avg.signals.values));
tx_mult_averages(3) = out.mult2_avg.signals.values(length(out.mult2_avg.signals.values));
tx_mult_averages(4) = out.mult3_avg.signals.values(length(out.mult3_avg.signals.values));

