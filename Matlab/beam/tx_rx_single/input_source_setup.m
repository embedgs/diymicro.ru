%input source setup
fin = 11.3e9;

phase_in= 0;
nsamples = 512; %per period
nperiods = 24;  %more better for delay measurements    
total_n_samples = nsamples*nperiods;
stoptime = nperiods/fin;
vin = 0.02;
tin = 1/fin;
%end of input source setup