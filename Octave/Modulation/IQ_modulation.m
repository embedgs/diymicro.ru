clf;
clear;
grid on;
hold  on;

t = 0:0.1:20;
f = 0.1;
I = 1;
Q = 1;
phi = 0;

I = -1;
Q = 1;
fI0 = I*cos(2*pi*f*t + phi);
fQ0 = j*Q*sin(2*pi*f*t + phi);
f0 = fI0 + fQ0;
phase0 = angle(f0);
subplot(2,2,1)
hold on;
plot(t, fI0,'b','linewidth',2,'markersize',10);
plot(t, imag(fQ0),'r','linewidth',2,'markersize',10);
plot(t,phase0);
h = legend('I=0','Q=1');


subplot(2,2,2)
hold on;
I = 1;
Q = 1;
fI1 = I*cos(2*pi*f*t + phi);
fQ1 = j*Q*sin(2*pi*f*t + phi);
f1 = fI1 + fQ1;
phase1 = angle(f1);

plot(t, fI1,'b','linewidth',2,'markersize',10);
plot(t, imag(fQ1),'r','linewidth',2,'markersize',10);
plot(t,phase1);
h = legend('I=1','Q=1')


subplot(2,2,3)
hold on;
I = -1;
Q = -1;
fI2 = I*cos(2*pi*f*t + phi);
fQ2 = j*Q*sin(2*pi*f*t + phi);
f2 = fI2 + fQ2;
phase2 = angle(f2);
plot(t, fI2,'b','linewidth',2,'markersize',10);
plot(t, imag(fQ2),'r','linewidth',2,'markersize',10);
plot(t,phase2)
h = legend('I=0','Q=0')


subplot(2,2,4)
hold on;
I = 1;
Q = -1;
fI3 = I*cos(2*pi*f*t + phi);
fQ3 = j*Q*sin(2*pi*f*t + phi);
f3 = fI3 + fQ3;
phase3 = angle(f3);
plot(t, fI3,'b','linewidth',2,'markersize',10);
plot(t, imag(fQ3),'r','linewidth',2,'markersize',10);
plot(t,phase3)
h = legend('I=1','Q=0')



#h=legend('I              ','Q                 ');
#set(h,'fontsize',14,'fontname','FreeSans','fontweight','normal');
#set(gca, 'linewidth', 3, 'fontsize', 18,'fontname', 'FreeSans') #modify line and fontsize of x and y axes
