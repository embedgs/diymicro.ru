clf;
clear;



N_samples = 64; # input data samples
phi = 0;
Ig = 1;
Qg = 1;
bits = 4; 
f_bb = 1e6;
f_res = f_bb*128;
t = 0:1/f_res:(N_samples/f_bb-1/f_res);
ph_check = 29;

function retval = i_gen(icoef, f_bb, f_res, phi)
  #printf('tx is %.8f \n', tcur);
  i_arr = [];  
     for (tx = 0:1/f_res:(1/f_bb-1/f_res))
         i_arr(end+1) = icoef*cos(2*pi*f_bb*tx+phi);
     endfor  
  retval = i_arr;
  return;
      #q_arr(end+1) = j*qcoef*sin(2*pi*f_bb*tcur+phi);
endfunction  

function retval = q_gen(icoef, f_bb, f_res, phi)
  #printf('tx is %.8f \n', tcur);
  q_arr = [];  
     for (tx = 0:1/f_res:(1/f_bb-1/f_res))
         q_arr(end+1) = j*icoef*sin(2*pi*f_bb*tx+phi);
     endfor  
  retval = q_arr;
  return;
      #q_arr(end+1) = j*qcoef*sin(2*pi*f_bb*tcur+phi);
endfunction  


#Generate input data stream

for (i=1:1:N_samples)
  datastream(i) = randi([0,(bits-1)]);
endfor
#datastream(1) = 0;

#plot(datastream)

tstart = 0;
i_bb_data = [];
q_bb_data = [];
#parsing datastream and generating output
for (i=1:1:N_samples)
  switch (datastream(i))
    case 0
      i_bb_data = [i_bb_data, i_gen(-1,f_bb,f_res,phi)];
      q_bb_data = [q_bb_data, q_gen(-1,f_bb,f_res,phi)];
    case 1
      i_bb_data = [i_bb_data, i_gen(-1,f_bb,f_res,phi)];
      q_bb_data = [q_bb_data, q_gen(1,f_bb,f_res,phi)];
    case 2
      i_bb_data = [i_bb_data, i_gen(1,f_bb,f_res,phi)];
      q_bb_data = [q_bb_data, q_gen(-1,f_bb,f_res,phi)];
    case 3
      i_bb_data = [i_bb_data, i_gen(1,f_bb,f_res,phi)];
      q_bb_data = [q_bb_data, q_gen(1,f_bb,f_res,phi)];
  endswitch 
endfor
#plot(t,i_bb_data)
#hold on;
#plot(t,imag(q_bb_data))
iq_bb_data = i_bb_data + q_bb_data;
phase_bb = angle(iq_bb_data);
phase_arr= [];

for(i=ph_check:128:(N_samples*128-ph_check))
  data_iq_const(end+1)=iq_bb_data(i);
endfor

grid on
hold on
scatterplot(data_iq_const,1,0,'r+')

