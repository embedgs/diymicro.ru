clf
clear
#input data variables
step = 1/256;
tstop = 2;
f1 = 7;

t = 0:step:tstop-step;

N = 16;
harm = 0:1:(N-1);
y = 2+sin(2*pi*f1*t)+4*sin(2*pi*(f1+1)*t);

#ytry=e.^(j*2*pi*1*t);
#y_product1 = y.*ytry;

#plot(t,y)
##hold on
#grid on


for i=0:1:(N-1)
  ytry=e.^(j*2*pi*i*t);
  y_product = y.*ytry;
 
  correlation_real(i+1) = sum(real(y_product));
  correlation_imag(i+1) = sum(imag(y_product));
  correlation_abs(i+1) = correlation_real(i+1) + correlation_imag(i+1);
  #plot(i,correlation_abs(i+1),'marker','o');
  #hold on;  
  #grid on;
  
  
endfor

bar(harm,correlation_abs)
grid on;
hold on;