clf
clear


#input data variables
step = 1/256;
tstop = 2;
f1 = 1;

t = 0:step:tstop-step;

for i = 1:length(t)
  rand_coeff1(i) = (rand()-0.5:0.5)/3;
  rand_coeff2(i) = (rand()-0.5:0.5)/3;
end
y = sin(2*pi*f1*t);
y1 = y+rand_coeff1;
y2 = -y+rand_coeff2;
y3 = sin(2*pi*13*f1*t);

correlation1 = sum(y.*y1);
correlation2 = sum(y.*y2);
correlation3 = sum(y.*y3);
subplot(3,1,1)
hold on
grid on
title('Correlation ~ 254');
plot(t,y,'r');
plot(t,y1,'b');
legend("original","positive correlation",'location','eastoutside');
subplot(3,1,2)
hold on
grid on
title('Correlation ~ -254');
plot(t,y,'r');
plot(t,y2,'b');
legend("original","negative correlation",'location','eastoutside');
subplot(3,1,3)
hold on
grid on
title('Correlation ~ -->0');
plot(t,y,'r');
plot(t,y3,'b');
legend("original","no correlation            ",'location','eastoutside');
