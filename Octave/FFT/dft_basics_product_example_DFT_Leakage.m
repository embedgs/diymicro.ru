clf;
clear;
grid on;
hold  on;
N = 64;
Fs = 8;
ts = 1/Fs;
#f_spectrum = 0:Fs/N:(Fs*(N-1))/N;
f_spectrum = 0:Fs/N:Fs/2;

tsample = 0:ts:(N-1)*ts;
nsample = 0:1:N;
y_sampled = 0 + 3*cos(2*pi*tsample*3.3);

#for i=1:1:(N)
#  ywindowed(i) = 0.5*(1-cos(2*pi*i/N))*(y_sampled(i));
#endfor

  ywindowed = y_sampled.*hanning(N)';




plot(tsample,y_sampled,'b','linewidth',2,'markersize',10);
hold on;
plot(tsample,ywindowed,'r','linewidth',2,'markersize',10);

h=legend('original              ','windowed                 ');
set(h,'fontsize',14,'fontname','FreeSans','fontweight','normal');
set(gca, 'linewidth', 3, 'fontsize', 18,'fontname', 'FreeSans') #modify line and fontsize of x and y axes
#xlabel('N');
#ylabel('Coefficient');

for i=0:1:(N-1)
  sum_harm = 0;
  for k=0:1:(N-1)
      sum_harm = sum_harm + ywindowed(k+1)*e^(-j*2*pi*i*k/N);
  endfor
    f(i+1) = sqrt(real(sum_harm)^2+imag(sum_harm)^2);
 
endfor

#normalization procedure

for i=1:1:((N/2)+1)
    if ((i==1)||(i==(N/2 + 1)))
      f_norm(i) = f(i)/N;
    else
      f_norm(i) = f(i)*2/N;
    endif
endfor

#bar(f_spectrum,f_norm,0.1)
xlabel('f, Hz');
ylabel('Amplitude, V');