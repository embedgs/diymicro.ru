clf;
clear;
grid on;
hold  on;
N = 64
Fs = 8;
ts = 1/Fs;
#f_spectrum = 0:Fs/N:(Fs*(N-1))/N;
f_spectrum = 0:Fs/N:Fs/2;

tsample = 0:ts:(N-1)*ts;
y_sampled = 2 + 5*cos(2*pi*tsample*2)+3*sin(2*pi*tsample*3) + 2*cos(2*pi*tsample*4);
#plot(tsample,y_sampled,'b','marker','o','linewidth',1,'markersize',20);
hold on
#plot(tsample,ywindowed,'r','marker','o','linewidth',1,'markersize',20);

h=legend('original              ','sampled                 ');
set(h,'fontsize',14,'fontname','FreeSans','fontweight','normal');
set(gca, 'linewidth', 3, 'fontsize', 18,'fontname', 'FreeSans') #modify line and fontsize of x and y axes
#xlabel('t, s');
#ylabel('Amplitude, V');

for i=0:1:(N-1)
  sum_harm = 0;
  for k=0:1:(N-1)
      sum_harm = sum_harm + y_sampled(k+1)*e^(-j*2*pi*i*k/N);
  endfor
    f(i+1) = sqrt(real(sum_harm)^2+imag(sum_harm)^2);
 
endfor

#normalization procedure

for i=1:1:((N/2)+1)
    if ((i==1)||(i==(N/2 + 1)))
      f_norm(i) = f(i)/N;
    else
      f_norm(i) = f(i)*2/N;
    endif
endfor


bar(f_spectrum,f_norm,0.2)
#bar(f_spectrum,f_win,0.1)
xlabel('f, Hz');
ylabel('Amplitude, V');