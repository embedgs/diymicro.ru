clf;
clear;

range = 9000;
tstep_last = 100;

solution_for_N = range/tstep_last;

N = 1:1:1000;
equation = log(N)./log(N./(N-1));

Nfit = lookup(equation, solution_for_N);

n = 1:1:Nfit;
tstep = tstep_last/log(Nfit/(Nfit-1));
func = log(n).*tstep;
mult_factor = range/func(length(func));
func_adjusted = mult_factor.*func;

for i=2:1:length(func)
  step_array(i)=func(i)-func(i-1);
  step_array_adjusted(i) = func_adjusted(i)-func_adjusted(i-1);
endfor  

subplot(2,1,1);
hold on;
grid on;
plot(n,func);
plot(n,func_adjusted);
subplot(2,1,2);
hold on;
plot(n,step_array);
plot(n,step_array_adjusted);
grid on;

