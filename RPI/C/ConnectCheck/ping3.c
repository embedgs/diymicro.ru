#include <stdio.h>
#include "/usr/include/string.h"
#include "/usr/include/stdlib.h"
#include <sys/time.h>
#include <wiringPi.h>
 
int main ()
{
wiringPiSetup();		//Init wiringPi
pinMode (0, OUTPUT);
digitalWrite (0, LOW);

FILE * fp;
fp = fopen ("/home/pi/Log/ConnectionTest.txt", "a+");

double t1, t2;
unsigned char flag = 0;
struct timeval tim;  
gettimeofday(&tim, NULL);  
t1=tim.tv_sec+(tim.tv_usec/1000000.0);  //Время старта в секундах
FILE *cmd = popen ( "ping -c 4 ya.ru", "r" ); //ping ya.ru 4 раза
char *s = malloc ( sizeof ( char ) * 200 );
do
{
  fgets ( s, sizeof ( char )*200, cmd );
  //printf ( "%s", s);//show outcome
  gettimeofday(&tim, NULL);  
  t2 = tim.tv_sec+(tim.tv_usec/1000000.0);  //Текущее время работы в секундах   
  if ( strstr ( s, "ms" ) != 0 )    //проверяем допинговались ли мы куда-нибудь вообще, если да то прерываем цикл
  {
  		flag = 1;
        break;
  }              
}while((t2-t1)<60);		//Если висим слишком долго то пора делать ребут

time_t t;				//Время для логов
time(&t);

if (!flag) 				//Обрабатываем отсутствие коннекта
{
	digitalWrite (0, HIGH);
	delay(1000);
	delay(1000);
	digitalWrite (0, LOW);

    fprintf( fp, "Test of connection is failed at %s\n", ctime( &t) );
} else
	fprintf( fp, "Test of connection is passed at %s\n", ctime( &t) );	

 
pclose ( cmd );
fclose(fp);
return 0;
}
